#!/usr/bin/perl -w
use strict;
use warnings;
use HTTP::Request;
use LWP::UserAgent;
use Data::Dumper;
use Encode;
use URI::Escape;
use URI::URL;

my $url = "http://api.infobip.com/api/v3/sendsms/plain";
my $sender = "1.1";
my $user = "PdSMS";
my $pass = "1*SM*a1";

my $msisdn = '';
my $text = '';
my $is_unicode = 0;
if (scalar(@ARGV) >= 2) {
	$msisdn = $ARGV[0];
	$text = decode('cp1251', $ARGV[1]);
	$is_unicode = 1 if (bytes::length($ARGV[1]) != bytes::length($text));
} else {
	print "Run: $0 msisdn text\n";
	exit;
}

$url = url($url);
my %req = (
	user => $user,
	password => $pass,
	sender => $sender,
	SMSText => $text,
	GSM => $msisdn,
);
$req{'DataCoding'} = 8 if ($is_unicode);
$url->query_form(%req);

# GET
my $ua = LWP::UserAgent->new;
$ua->timeout(20); # ������� ����� (���)
my $response = $ua->get($url);
#print $response->code."\n";
#print $response->as_string();

__END__
