#!/usr/bin/perl
use strict;
use warnings;
use locale;
use lib '../lib/';
use Disp::DaemonManager;

Disp::DaemonManager::run_monitor(\&init,
								 kind => 'billing',
								 status_file => '../log/monitor-billing.status',
								 pid_file => '../var/run/monitor-billing.pid',
								 config_file => '../etc/asg.conf',
								 log_config => '../etc/log.conf');

sub init {
    my ( $dm ) = @_;
    $dm->perm_daemons( 'permanent_billing',
                       yell => [ 'bin', './yell.pl' ],
                       cyka => [ 'bin', './cuka2.php' ],
                       fastcgi_stat => [ 'cgi-bin' , './stat.pl'],
                       credit_etl => [ 'bin', 'perl credit-etl.pl' ],
                       ELT_external_custom => [ 'bin', './ELT_external_custom.php' ],
                       long_tasks => [ 'bin', './tasks_process.pl' ]);
}
