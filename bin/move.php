#!/usr/local/bin/php
<?php

// vim: tabstop=4

class Replication
{
	function __construct($FROMDB, $TODB, $mails)
	{
		$this->fromdb = new rdb($FROMDB);
		$this->todb = new rdb($TODB);
		$this->system_root = $FROMDB[system_root];
		$this->log_file = "$FROMDB[system_root]/log/out_replication.log";
		$this->err_log_file = "$FROMDB[system_root]/log/err_replication.log";
		$this->mails = $mails;
		$this->mail_title = "Content-Type: text/plain; charset=windows-1251\nContent-Transfer-Encoding: 8bit\nFrom: replication@dt3.mobile4smile.ru\n";
	}
	function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}
	//! \brief Error output.
	function errorExit($s)
	{
	    error_log (date("H:i:s")." EXIT: $s\n",3,$this->log_file);
	    exit;
	}	
	//! \biref Standart messages output.
	function log($s)
	{
		error_log(date("H:i:s")." $s\n",3,$this->log_file);
	}
	//! \brief Warnings output.
	function w($s)
	{
		error_log("$s\n",3,$this->err_log_file);
	}
	//! \brief ������ ����������.
	function run()
	{
		$this->log("STARTING...");
		while (1)
		{
			//
			// Example:
			//
			// --------------------------------------------------------------------------
			// | <id>   | <tbl>    | <op>   | <cond>                                    |
			// --------------------------------------------------------------------------
			// | 125140 | billingf | INSERT | inbox_id=12018487 and outbox_id=12089926' |
			// --------------------------------------------------------------------------
			$query = "SELECT id, tbl, op, cond FROM tr_replication ORDER BY id LIMIT 100";
			$this->records = array();
			$this->records = ($this->indb).selectAll($query);
			if ($this->records)
			{
				$this->log("MOVING CUSTOMS:".count($this->records));
				$start_timestamp = $this->getmicrotime();
				foreach($this->records as $record)
				{
					// ����������� ������������� ������
					$query = "SELECT * FROM {$record[tbl]} WHERE {$record[cond]}";
					$in = ($this->indb).selectAll($query);
					if (!$in)
					{
						// �� ������� ������������� ������.
						// ����� � ��������� ��� � �������� ������ � ������������.
						// ��� ��� DELETE ���, �� ��� ��� ������� ��������������.
						$err_description = ($this->indb).dberror();
						$this->w("[ERROR {$record[id]}]\n".
								 "QUERY: {$query}\n".
								 "MYSQL ERROR: {$err_description}\n");
						mail($this->mails, 
							 "REPLICATION WARNING: RECORD FOR REPLICATION DIDN'T FOUNDED IN {$record[tbl]}.",
							 "[ERROR {$record[id]}]\nQUERY: {$query}\nMYSQL ERROR: {$err_description}\n", 
							 $this->mail_title);
						continue;
					}

					////////////////
					// ���������� //
					////////////////
					($this->outdb).startTransaction();
					($this->indb).startTransaction();

					// ������
					$query = "DELETE FROM tr_replacation WHERE id='{$record[id]}'";
					$res = ($this->indb).query($query);
					if (!$res)
					{
						//
						// ������ ����� ��� ����� � DB, �������!
						//
						$err_description = ($this->indb).dberror();
						$this->w("[ERROR {$record[id]}]\n".
								 "QUERY: {$query}\n".
								 "MYSQL ERROR: {$err_description}\n");
						//$this->outdb->rollbackTransaction();
						//$this->indb->rollbackTransaction();
						mail($this->mails, 
							 "REPLICATION ERROR: DELETE FROM tr_replication FAILED.",
							 "[ERROR {$record[id]}]\nQUERY: {$query}\nMYSQL ERROR: {$err_description}\n", 
							 $this->mail_title);
						sleep(5);
						exit;
					}

					// ������� � ������� ����
					$query = "REPLACE INTO {$record[tbl]} SET ";
					$queryAttr = '';
					foreach ($in as $name=>$value)
					{
						if ('' == $queryAttr) $query .= "{$name}='{$value}'";
						else $query .= ",{$name}='{$value}'";
					}
					$query .= $queryAttr;
					$res = ($this->outdb).query($query);
					if (!$res)
					{
						//
						// ������ ����� ��� ����� � ��.
						//
						$err_description = ($this->indb).dberror();
						$this->w("[ERROR {$record[id]}]\n".
								 "QUERY: {$query}\n".
								 "MYSQL ERROR: {$err_description}\n");
						mail($this->mails, 
							 "REPLICATION ERROR: INSERT IN {$record[tbl]} FAILED.",
							 "[ERROR {$record[id]}]\nQUERY: {$query}\nMYSQL ERROR: {$err_description}\n", 
							 $this->mail_title);
						sleep(5);
						exit;
					}
					
					($this->outdb).commitTransaction();
					($this->indb).commitTransaction();
				}
				$execute_time = $this->getmicrotime() - $start_timestamp;
				$this->log("����� ���������� ����������: $execute_time.");
			}
			else
			{
				sleep(1);	
				$this->log("WAITING...");
			}
		}
	}	
};	

//include_once('../www/config.php');
include_once('../www/cdb.php'); 

// Mail adresses:
$mails = 'e.tihonov@alt1.ru;a.belyaev@alt1.ru;v.islamov@alt1.ru';

$cp = new Replication($INDB, $OUTDB, $mails);
$cp->run();
?>
