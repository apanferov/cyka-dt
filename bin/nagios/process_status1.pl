#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Getopt::Std;
use Net::Domain qw(hostfqdn);

eval {
	my %opts;
	getopt('vP', \%opts);

	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
	my $db = new Disp::AltDB();
	my $process_name = uc($opts{P});
	my $current_fqdn = hostfqdn();

	$process_name =~ m/^[a-zA-Z_]*(\d+)$/i;
	my $smsc_id = $1;
	my ($max_inactive_time,$fqdn,$out_queue_limit,$out_queue_limit_time) = $db->{db}->selectrow_array("select max_inactive_time,run_on_host,out_queue_limit,out_queue_limit_time from set_smsc_t where id=? limit 1",undef,$smsc_id);
#	my ($out_queue_limit,$out_queue_limit_time) = (0,5);
	die "Unknown connection $smsc_id." unless ($fqdn);
	
	if ($fqdn ne $current_fqdn) {
		print "Connection established on another host.";
		exit 0;
	}

# OVERALL STATUS
# ������� �������� ��������� ����
	my $prs = $db->{db}->selectall_hashref("select pr_name,time_to_sec(timediff(now(),pr_heartbeat)) as heartbeat_delay, time_to_sec(timediff(now(),last_received_sms_time)) as last_received_sms_delay from tr_process_heartbeat where pr_name like '%\\_$smsc_id' order by pr_name",'pr_name',undef);
	#print Dumper($prs);
	my $status = 0;
	my @status;
	my $last_received_sms_delay = 1000000;
	for (keys(%$prs)) {
		my $name = $_;
		my $heartbeat_delay = $prs->{$name}->{heartbeat_delay};
		if ($heartbeat_delay > 900) {
			my $heartbeat_delay_min = int($heartbeat_delay/60);
			push(@status,"$name stopped ($heartbeat_delay_min min)");
			$status = 1 if (!$status);
		}
		$status = 2 if ($heartbeat_delay > 3600);
		$last_received_sms_delay = $prs->{$name}->{last_received_sms_delay} if (defined($prs->{$name}->{last_received_sms_delay}) and ($last_received_sms_delay > $prs->{$name}->{last_received_sms_delay}));
	}
	my $status_text = ($status) ? join(', ',@status) : 'OK';
	my $last_received_sms_delay_min = int($last_received_sms_delay/60);

# QUEUES	
	my ($in_queue_text,$out_queue_text);
	if (!$status) {
		# INQUEUE	
		$in_queue_text = "InQueue: OK";
		if ($max_inactive_time) {
			if ($last_received_sms_delay_min > $max_inactive_time) {
				$in_queue_text = "InQueue: No msgs ($last_received_sms_delay_min min)";
				$status = 1;
			}
		}
		# OUTQUEUE
		$out_queue_text = "OutQueue: OK";
		if ($out_queue_limit_time) {
			my $out_queue_count = $db->get_value('select count(*) from tr_outsms where smsc_id=? and date_sub(now(),interval ? minute)>date',undef,$smsc_id,$out_queue_limit_time);
			if ($out_queue_count > $out_queue_limit) {
				$out_queue_text = "OutQueue: $out_queue_count msgs stuck";
				$status = 1;
			}
		}
	}

	$status_text .= ", $in_queue_text" if ($in_queue_text);
	$status_text .= ", $out_queue_text" if ($out_queue_text);
	print "Status: $status_text";
	exit $status;
};

if ($@) {
	print $@;
	exit 3;
}



