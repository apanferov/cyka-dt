#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Translit qw (translit_text);

Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();

my $all_smsc = $db->{db}->selectall_hashref('select * from set_smsc','id', undef);

open my $fh1, ">", 'dt1_local.txt';
open my $fh2, ">", 'dt3_local.txt';
open my $fh9, ">", 'nrpe.txt';

print $fh1 "### BEGIN SMSC monitoring\n";
# print $fh2 "### BEGIN SMSC monitoring\n";
print $fh9 "### BEGIN SMSC monitoring\n";

foreach (sort {$a <=> $b} keys(%$all_smsc)) {
	my $smsc = $all_smsc->{$_};
	my $name = translit_text($smsc->{fullname});
	$name =~ s/\'//ig;
	$name =~ s/\"//ig;
	$name =~ s/\(//ig;
	$name =~ s/\)//ig;
	my $id = $smsc->{id};
	$name = "[$id] $name";

	if ($smsc->{run_on_host} eq 'dt1.local') {
	       print $fh1 <<EOF;
define service{
	use                             common_service,cyka_connection_check,cyka_admins
	host_name                       cyka-dt1
	service_description             $name
	check_command                   check_nrpe_command!dt_mon_smsc_$id
}

EOF
	}

	if ($smsc->{run_on_host} eq 'dt3.local') {
		print $fh2 <<EOF;
define service{
	use                             common_service,cyka_connection_check,cyka_admins
	host_name                       cyka-dt3
	service_description             $name
	check_command                   check_nrpe_command!dt_mon_smsc_$id
}

EOF
	}

	if ($smsc->{run_on_host} eq 'dt1.local' || $smsc->{run_on_host} eq 'dt3.local') {
		print $fh9 "command[dt_mon_smsc_$id]=/gate/asg5/bin/nagios/process_status.sh -P $id\n";
	}
}

print $fh1 "### END SMSC monitoring\n";
print $fh2 "### END SMSC monitoring\n";
print $fh9 "### END SMSC monitoring\n";

exit 0;




