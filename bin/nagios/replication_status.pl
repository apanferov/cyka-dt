#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);

eval {
	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
	my $db = new Disp::AltDB();
	
	my $count = $db->get_value("select count(*) from tr_replication");
	print "$count actions in replication queue";
	exit 2 if ($count > 10000);
};

if ($@) {
	print $@;
	exit 3;
}

exit 0;




