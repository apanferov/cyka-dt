#!/bin/bash
zabbix_sender -z zabbix.a1a.local -s web -k kpk_messages_queue -o `echo "select count(*) from tr_credit_messages where cyka_processed = 0" | mysql --skip-column-names alt5` &> /dev/null
zabbix_sender -z zabbix.a1a.local -s web -k kpk_lines_queue -o `echo "select count(*) from tr_credit_lines where next_action_date < now()" | mysql --skip-column-names alt5` &> /dev/null
zabbix_sender -z zabbix.a1a.local -s web -k kpk_notifies_queue -o `echo "select count(*) from tr_credit_notifies" | mysql --skip-column-names alt5` &> /dev/null
