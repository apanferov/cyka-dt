#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use Disp::Config qw(param);
use Config::Tiny;
use Redis;

eval {
	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);

	my $r = Redis->new(server => Config->param('server','redis'), encoding => undef);
	my $qlen = $r->llen('dr_queue');
	print "Queue: $qlen";
	exit 2 if ($qlen > 1000000);
	exit 1 if ($qlen > 1000);
	exit 0;
};

if ($@) {
	print $@;
	exit 3;
}

exit 0;




