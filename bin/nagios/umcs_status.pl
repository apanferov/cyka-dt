#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;

my $check_interval = "interval 1 day";
my $minimal_requests_count = 10;
my $minimal_success_percentage = 50;

sub main {
	Log::Log4perl->init("$FindBin::RealBin/../../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../../etc/asg.conf");

	my $sth = Config->dbh->prepare("select our_status, count(*) as cnt from tr_umcs_requests where reserve_date > date_sub(now(), $check_interval) group by our_status");
	$sth->execute();

	my $total = 0;
	my $success_count = 0;

	while (my ( $status, $count ) = $sth->fetchrow_array) {
		$total += $count;
		# Покупка - прошла, Покупка - мы отказались, Явный отказ абонента от покупки
		if ($status == 8 or $status == 9 or $status == 13) {
			$success_count += $count;
		}
	}

	my $status_msg = "Total reserved: $total, total success: $success_count. ";
	my $failure = 0;

	if ($total < $minimal_requests_count) {
		$status_msg .= "To few requests in $check_interval. ";
		$failure = 1;
	}

	if ( 100 * $success_count / $total < $minimal_success_percentage  ) {
		$status_msg .= "Success rate to low. ";
		$failure = 1;
	}

	print $status_msg;
	exit $failure ? 2 : 0;


}

main();
