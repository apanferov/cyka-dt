#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Getopt::Std;
use Net::Domain qw(hostfqdn);

# my $current_fqdn = hostfqdn();
# print "$current_fqdn\n";

use URI::URL;

use constant queue_conf_tab => {
    insms  => { process_limit =>  50, stuck_limit =>   5, stuck_time =>  5, datefield => 'date' },
    inbox  => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date' },
    outbox => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date' },
    outsms => { process_limit => 100, stuck_limit =>   5, stuck_time =>  5, datefield => 'date' },
    errsms => { err_limit =>  50, err_time =>  100, datefield => 'date' },
};
my $url = "https://asg5.alt1.ru";
my $err_rows_limit = 50;

Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();
my $queue = 'errsms';
my $queue_conf = queue_conf_tab->{$queue};
$queue_conf->{$_} = Config->param($queue.'_'.$_,'monitoring') || $queue_conf->{$_} foreach(keys(%$queue_conf));
my $datefield = $queue_conf->{datefield}; 
my $err_time = $queue_conf->{err_time}; 
$queue = 'inboxa';

my $dbh = $db->{db};
my $query = "select id as inbox_id from tr_$queue where adddate($datefield, INTERVAL $err_time MINUTE) > now() order by date desc limit $err_rows_limit";
print "$query\n";
my $res = $dbh->selectall_arrayref($query, { Slice => {} }) or die $!; 
foreach my $row (@$res) {
	my $q = url($url);
	my %req = (
		module => 'findmsg',
		action => 'inboxinfo',
		inbox_id => $row->{inbox_id},
	);
	$q->query_form(%req);
	print $q->as_string."\n";
}

__END__
