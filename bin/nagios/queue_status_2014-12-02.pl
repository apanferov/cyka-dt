#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Getopt::Std;

use constant queue_conf_tab => {
    insms  => { process_limit =>  50, stuck_limit =>   5, stuck_time =>  5, datefield => 'date' },
    inbox  => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date' },
    outbox => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date' },
    outsms => { process_limit => 100, stuck_limit =>   5, stuck_time =>  5, datefield => 'date' },
    errsms => { err_limit =>  50, err_time =>  10, datefield => 'date' },
};


eval {
	my %opts;
	getopt('vQ', \%opts);

	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
	my $db = new Disp::AltDB();
	
	my $queue = lc($opts{Q});
	die "Unknown queue ''." unless (defined $queue);
	die "Unknown queue '$queue'." unless (defined queue_conf_tab->{$queue});
	my $queue_conf = queue_conf_tab->{$queue};
	$queue_conf->{$_} = Config->param($queue.'_'.$_,'monitoring') || $queue_conf->{$_}
		foreach(keys(%$queue_conf));
	
	if ($queue eq 'errsms') {
		my $datefield = $queue_conf->{datefield}; 
		my $err_time = $queue_conf->{err_time}; 
		my $err_msg_count = $db->get_value("select count(*) from tr_$queue where adddate($datefield,INTERVAL ? MINUTE) > now()",
														undef,$err_time);
	
		my $status_msg = "Queue:".uc($queue).". $err_msg_count errors in last $err_time minutes.";
		print $status_msg;
		exit 2 if ($err_msg_count > $queue_conf->{err_limit});
	}
	else {
		my $process_msg_count = $db->get_value("select count(*) from tr_$queue");
		my $datefield = $queue_conf->{datefield}; 
		my $stuck_msg_count = $db->get_value("select count(*) from tr_$queue where adddate($datefield,INTERVAL ? MINUTE) < now()",
														undef,$queue_conf->{stuck_time});
		
		my $status_msg = "Queue:".uc($queue).". Process:$process_msg_count/$queue_conf->{process_limit}. Stuck:$stuck_msg_count/$queue_conf->{stuck_limit}.";
		print $status_msg;
		exit 2 if (($process_msg_count > $queue_conf->{process_limit})
	     or ($stuck_msg_count > $queue_conf->{stuck_limit}));
	}
};

if ($@) {
	print $@;
	exit 3;
}

exit 0;




