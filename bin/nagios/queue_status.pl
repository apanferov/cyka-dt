#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Getopt::Std;
#use URI::URL;
#use HTML::Entities;

use constant queue_conf_tab => {
    insms  => { process_limit =>  50, stuck_limit =>   5, stuck_time =>  5, datefield => 'date', inbox_id_field => 'id' },
    inbox  => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date', inbox_id_field => 'id' },
    outbox => { process_limit =>  50, stuck_limit =>   0, stuck_time =>  5, datefield => 'date', inbox_id_field => 'inbox_id' },
    outsms => { process_limit => 100, stuck_limit =>   5, stuck_time =>  5, datefield => 'date', inbox_id_field => 'inbox_id' },
    errsms => { err_limit =>  50, err_time =>  10, datefield => 'date', inbox_id_field => 'inbox_id' },
};
my $url = "https://asg5.alt1.ru";
my $rep_rows_limit = 50;

eval {
	my %opts;
	getopt('vQ', \%opts);

	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
	my $db = new Disp::AltDB();
	
	my $queue = lc($opts{Q});
	die "Unknown queue ''." unless (defined $queue);
	die "Unknown queue '$queue'." unless (defined queue_conf_tab->{$queue});
	my $queue_conf = queue_conf_tab->{$queue};
	$queue_conf->{$_} = Config->param($queue.'_'.$_,'monitoring') || $queue_conf->{$_} foreach(keys(%$queue_conf));
	my $inbox_id_field = $queue_conf->{inbox_id_field}; 
	
	if ($queue eq 'errsms') {
		my $datefield = $queue_conf->{datefield}; 
		my $err_time = $queue_conf->{err_time}; 
		my $err_msg_count = $db->get_value("select count(*) from tr_$queue where adddate($datefield,INTERVAL ? MINUTE) > now()", undef, $err_time);
	
		my $status_msg = "Queue:".uc($queue).". $err_msg_count errors in last $err_time minutes.";
		print $status_msg;
		if ($err_msg_count > $queue_conf->{err_limit}) {
			my $dbh = $db->{db};
			my $query = "select $inbox_id_field as inbox_id from tr_$queue where adddate($datefield, INTERVAL $err_time MINUTE) > now() order by date desc limit $rep_rows_limit";
			my $res = $dbh->selectall_arrayref($query, { Slice => {} }) or die $!;
			my $id_str = '';
			foreach my $row (@$res) {
				$id_str .= "," if ($id_str);
				$id_str .= $row->{inbox_id};
#				my $q = url($url);
#				my %req = (
#					module => 'findmsg',
#					action => 'inboxinfo',
#					inbox_id => $row->{inbox_id},
#				);
#				$q->query_form(%req);
#				print encode_entities($q->as_string)."<br>";
			}
			print " ".$id_str if ($id_str);

			exit 2;
		}
	} else {
		my $process_msg_count = $db->get_value("select count(*) from tr_$queue");
		my $datefield = $queue_conf->{datefield}; 
		my $stuck_msg_count = $db->get_value("select count(*) from tr_$queue where adddate($datefield,INTERVAL ? MINUTE) < now()", undef, $queue_conf->{stuck_time});
		
		my $status_msg = "Queue:".uc($queue).". Process:$process_msg_count/$queue_conf->{process_limit}. Stuck:$stuck_msg_count/$queue_conf->{stuck_limit}.";
		print $status_msg;
		if (
			($process_msg_count > $queue_conf->{process_limit}) or
	    	($stuck_msg_count > $queue_conf->{stuck_limit})
	    ) {
			my $dbh = $db->{db};
			my $query = "select $inbox_id_field as inbox_id from tr_$queue where adddate($datefield, INTERVAL $queue_conf->{stuck_time} MINUTE) < now() order by date desc limit $rep_rows_limit";
			my $res = $dbh->selectall_arrayref($query, { Slice => {} }) or die $!; 
			my $id_str = '';
			foreach my $row (@$res) {
#				if ($queue eq 'insms') {
#					print $row->{inbox_id}."\n"; # insms.id
#					print $row->{inbox_id}.",";
				$id_str .= "," if ($id_str);
				$id_str .= $row->{inbox_id};
#				} else {
#					my $q = url($url);
#					my %req = (
#						module => 'findmsg',
#						action => 'inboxinfo',
#						inbox_id => $row->{inbox_id},
#					);
#					$q->query_form(%req);
#					print encode_entities($q->as_string)."<br>";
#				}
			}
			print "insms.id:" if ($queue eq 'insms' && $id_str);
			print " ".$id_str if ($id_str);

			exit 2;
		}
	}
};

if ($@) {
	print $@;
	exit 3;
}

exit 0;

__END__

perl queue_status.pl -Qinsms
sh queue_status.sh -Qinbox
