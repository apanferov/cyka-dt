#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::Config;
use Disp::Utils;
use Getopt::Std;
use Time::Period;

eval {
	Config::init(config_file => "../../etc/asg.conf");

	my %opts;
	getopts('vs:t:n:p:', \%opts);

	my $time = $opts{t} || 30;

	unless ($time > 0 and $time < 60 * 24) {
		die "Time param must be between 1 and 1440";
	}

	my $smsc = $opts{s};

	unless (defined($smsc) and $smsc =~ /^\d+$/) {
		die "Invalid smsc_id";
	}

	my $numbers = $opts{n};
	unless (defined($numbers)) {
		die "Numbers must be given";
	}

	my $period = $opts{p} || 'wd {Mon-Fri} hr {9am-6pm}';

	my @numbers;
	@numbers = split /,/, $numbers;

	my $sth = Config->dbh->prepare( "select num, count(*) from tr_insmsa where smsc_id = ? and date > date_sub(now(), interval ? minute) and num in (".(join ",", ("?")x@numbers).") group by num" );

	$sth->execute($smsc, $time, @numbers);

	my $no_traffic = 0;
	my @count;

	my %not_seen = map { $_ => 1 } @numbers;

	while (my($num, $cnt) = $sth->fetchrow_array) {
		push @count, "$num($cnt)";
		delete $not_seen{$num};
	}

	my $cpart = "Smsc $smsc. Period $time minutes. ";
	$cpart .= "Seen numbers: ".join(", ", @count)
	  if @count;

	if (keys %not_seen) {
		print "No traffic for numbers: ".join(", ", keys %not_seen).". $cpart\n";
		exit (inPeriod(time(), $period) ? 1 : 0);
	} else {
		print "All numbers have traffic. $cpart\n";
		exit 0;
	}
};

if ($@) {
	print $@;
	exit 3;
}
