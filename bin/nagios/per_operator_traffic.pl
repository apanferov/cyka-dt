#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::Config;
use Disp::Utils;
use Getopt::Std;
use Net::Domain qw(hostfqdn);

eval {
	Config::init(config_file => "../../etc/asg.conf");

	my %opts;
	getopts('vo:t:', \%opts);

	my $time = $opts{t} || 60;

	unless ($time > 0 and $time < 60 * 24) {
		die "Time param must be between 1 and 1440";
	}

	my $operator = $opts{o};

	unless (defined($operator) and $operator =~ /^\d+$/) {
		die "Invalid operator_id";
	}

	my $current_fqdn = hostfqdn();
	my ($fqdn) = Config->dbh->selectrow_array("select s.run_on_host from set_smsc_t s, set_operator_t o where o.id=? and o.smsc_id=s.id limit 1",undef,$operator);
	die "No default connection for $operator." unless ($fqdn);
	
	if ($fqdn ne $current_fqdn) {
		print "Default operator connection established on another host.";
		exit 0;
	}

	my ( $count ) = Config->dbh->selectrow_array("select count(*) from tr_insmsa where operator_id = ? and date > date_sub(now(), interval ? minute)",
												 undef, $operator, $time);


	my ($op_name) = Config->dbh->selectrow_array("select short_name from set_operator where id = ?", undef, $operator);

	if ($count) {
		print "There was '$count' messages for operator '$op_name($operator)' in last '$time' minutes\n";
		exit 0;
	} else {
		print "There was no messages for operator '$op_name($operator)' in last '$time' minutes\n";
		exit 2;
	}
};

if ($@) {
	print $@;
	exit 3;
}
