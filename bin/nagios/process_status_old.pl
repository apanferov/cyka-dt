#!/usr/bin/perl
use strict;
use warnings;
use lib '../../lib';
use DBI;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Getopt::Std;
use Net::Domain qw(hostfqdn);

eval {
	my %opts;
	getopt('vP', \%opts);

	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
	my $db = new Disp::AltDB();
	my $process_name = uc($opts{P});
	my $current_fqdn = hostfqdn();

	$process_name =~ m/^(\w+)_(\d+)$/i;
	my $smsc_id = $2;
	my ($max_time,$fqdn) = $db->{db}->selectrow_array("select max_inactive_time,run_on_host from set_smsc where id=? limit 1",undef,$smsc_id);
	
	if ($fqdn ne $current_fqdn) {
		print "Process isn't running on this host.";
		exit 0;
	}

	my $current_state = $db->get_value("select pr_state from tr_process_state_log where pr_name='$process_name' and pr_state<>'HEARTBEAT' order by id desc limit 1");
	
	my ($heartbeat_delay,$last_received_sms_time) = $db->{db}->selectrow_array("select time_to_sec(timediff(now(),pr_heartbeat)),time_to_sec(timediff(now(),last_received_sms_time)) from tr_process_heartbeat where pr_name=? limit 1",undef,$process_name);
	die "Unknown process '$process_name'." unless (defined $heartbeat_delay);
	
	my $heartbeat_delay_min = int($heartbeat_delay/60);
	my $last_received_sms_time_min = int($last_received_sms_time/60);
	my $heartbeat = ($heartbeat_delay < 100)?'OK':"ABSENT($heartbeat_delay_min min)";
	
	my $traffic_info = '';
	my $traffic_fail;
	if ($heartbeat_delay <= 900) {
		if ($process_name =~ m/^(trn|rcv|smsc)_(\d+)$/i) {
			$traffic_info="InQueue: ";
			if ($max_time and ($last_received_sms_time_min > $max_time)) {
				$traffic_info.="No msgs ($last_received_sms_time_min min).";
				$traffic_fail = 1;
			}else{
				$traffic_info.="OK.";
			}
		}
	}

	my $status_msg = "Process: $process_name. State: $current_state. Heartbeat: $heartbeat. $traffic_info";
	print $status_msg;
	
	if ($heartbeat_delay > 3600) {
		exit 2;
	}
	elsif (($current_state eq 'LISTEN') or ($heartbeat_delay > 900) or $traffic_fail) {
		exit 1;
	}
	
};

if ($@) {
	print $@;
	exit 3;
}

exit 0;




