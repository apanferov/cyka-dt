#!/usr/bin/perl
#######################################################
#
#  DISTRIBUTOR
#
#######################################################

use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use IO::File;
use Log::Log4perl qw(get_logger);
use Disp::AltDB;
use Disp::Utils qw(yell);
use Disp::Config qw(param);

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $terminated = 0;
my ($log, $db);

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $process_name='DISTRIBUTOR';

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
				"log4perl.rootLogger"       => Config->param('log_level',lc($process_name)).', Logfile',
				"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
				"log4perl.appender.Logfile.recreate" => 1,
				"log4perl.appender.Logfile.recreate_check_interval" => 300,
				"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
				"log4perl.appender.Logfile.umask" => "0000",
				"log4perl.appender.Logfile.filename" => Config->param('log_filename',lc($process_name)),
				"log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
				"log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern',lc($process_name))
			   );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################

	$db = new Disp::AltDB();

#######################################################
#  Main loop
#######################################################

	my ($heartbeat,$distribution_count,$distribution_list_count) = (0,0,0);
	my ($distributions,$distribution_list);
	$log->info('Started.');

	while (!$terminated)	{

	 	if ((time() - $heartbeat) >= 60) {
			$db->heartbeat($process_name);
			$log->debug("HEARTBEAT.");
			$heartbeat = time();
			$distributions = $db->{db}->selectall_hashref("SELECT d.id,d.name,d.msg,d.msg_per_sec FROM tr_distribution d, tr_distribution_time t WHERE d.id=t.distribution_id AND now() BETWEEN t.from_date AND t.to_date",'id');
			$distribution_count = scalar keys(%$distributions);
			$log->debug("Configuration loaded. $distribution_count distribution(s) in process.");
		}

		if ($distribution_count) {
			foreach (keys(%$distributions)) {
				my $dist = $distributions->{$_};
				$log->debug("Start reading table.");
				$distribution_list=$db->{db}->selectall_hashref("SELECT * FROM tr_distribution_list WHERE (sended=0) AND (distribution_id=?) limit $dist->{msg_per_sec}",'abonent',undef,$dist->{id});
				$log->debug("Finish reading table.");
				$distribution_list_count = scalar keys(%$distribution_list);

				if ($distribution_list_count) {
					foreach (keys(%$distribution_list)) {
						my $msg = $distribution_list->{$_};
						$msg->{distribution_id} = $dist->{id};
						$msg->{msg} = $dist->{msg} unless ($msg->{msg});
						my $id;
						eval {
							$id = send_msg($db,$msg);
						};
						if ($@) {
							$log->warn($@);
							$db->{db}->rollback();
							$id = send_msg($db,$msg);
						}
						$log->info("Message MSG$id was distributed. From:$msg->{num} To:$msg->{abonent} Text:$msg->{msg}");
					}
				}
			}
		}
		sleep 1;
	}
};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();


########### DO_EXIT ###########################

sub send_msg {
	my ($db,$msg) = @_;
	$db->{db}->begin_work();
	$db->{db}->do("INSERT INTO tr_outboxa (abonent,smsc_id,operator_id,num,msg,push_id,service_id,content_type,date) VALUES (?,?,?,?,?,100,100,'text/plain',now())",
				  undef,$msg->{abonent},$msg->{smsc_id},$msg->{operator_id},$msg->{num},$msg->{msg});
	my $id = $db->get_last_insert_id();
	$db->{db}->do("INSERT INTO tr_outbox select * from tr_outboxa where id=?",undef,$id);
	$db->{db}->do("INSERT INTO tr_billing(inbox_id, outbox_id, date,  service_id, abonent, smsc_id, operator_id, num, insms_count, outsms_count, dir, test_flag, cyka_processed, is_fraud) values (0, ?, now(), 100, ?, ?, ?, ?, 0, 0, 2, 0, 1, 0)", undef,
				  $id, $msg->{abonent}, $msg->{smsc_id}, $msg->{operator_id}, $msg->{num});
	$db->{db}->do('UPDATE tr_distribution_list SET sended=1, outbox_id=? WHERE id=?',undef,$id,$msg->{id});
	$db->{db}->commit();
	return $id;
}



sub do_stop {
	if ($db)   {
		$db->finish();   }
	$log->info("Stopped.\n\n\n");
}

sub do_die {
	my ($str)=@_;
	$log->fatal($str);
	yell($str,code => 'FATAL',process =>$process_name,
		 header => "$process_name: $str",
		 ignore => { log4perl => 1  });
	do_stop;
	exit(0);
}
