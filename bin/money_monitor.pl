#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use Disp::Config;
use Disp::Utils;
use Disp::Operator;
use Disp::ServiceManager;
use Text::CSV;
use Translit qw/translit_text/;
use Iterator_Utils ':all';
use ConfigTinyWrapper;
use File::Basename;

sub db_connect {
	my ( $cf ) = @_;
        my $dbconf = Config->param($cf);
        $dbconf = (dirname(Config->param('original_config')) . "/$dbconf") unless -f $dbconf;
        my $tmp = ConfigTinyWrapper->read( $dbconf );
        die $ConfigTinyWrapper::errstr unless $tmp;
        $tmp = $tmp->{_};

        my $dbh = DBI->connect("DBI:mysql:$tmp->{name};host=$tmp->{host}",
                               $tmp->{username}, $tmp->{password},
                               { AutoCommit => 1,
                                 RaiseError => 1 });
        $dbh->do("set names cp1251");
        return $dbh;
}

sub main {
        Log::Log4perl->init("../etc/log.conf");
        Config::init(config_file => "../etc/asg.conf",
                     skip_db_connect => 1);

        $| = 1;

        my $dbh = db_connect('db_bill_config');
		my $src_dbh = db_connect('db_transport_config');

        my ($usd_course) = $dbh->selectrow_array("select price from set_exchange_rate where currency = 'usd' and date = date(now()) limit 1");

        $dbh->do("drop table if exists disp_price_loader");
        $dbh->do("create temporary table disp_price_loader as select operator_id, num, max(date_start) as date_start from set_num_price where date_start <= date(now()) group by  operator_id, num");

        my %prices;

        my $tmp = $dbh->selectall_arrayref("select n.operator_id as operator_id, n.num as num, n.price_in * (select r.price from set_exchange_rate r where r.currency = n.currency and r.date = date(now()) ) as price_in from disp_price_loader d join set_num_price n on n.num = d.num and n.operator_id = d.operator_id and n.date_start = d.date_start", {Slice => {}});
        foreach (@$tmp) {
            $prices{$_->{operator_id}}{$_->{num}} = { price => $_->{price_in} / $usd_course };
        }

        my ( $max_inbox ) = $src_dbh->selectrow_array("select max(id) from tr_inboxa");
        my ( $max_inbox_counted ) = $dbh->selectrow_array("select max(last_inbox_id) from abonent_money_fetch where host = ?",
														  undef,
														  'alt1-dt3.alt1.ru');
		my $max_inbox_seen;

        $max_inbox_counted ||= 0;

        # my $sth = $dbh->prepare("select id, abonent, num, msg, transport_count, operator_id, week(date, 7) + extract(year from date) * 100 as week, date from tr_inboxa where date > date_sub(now(), interval 4 week) and id > ?");
        # my $sth = $dbh->prepare("select id, abonent, num, msg, transport_count, operator_id, week(date_sub(date, interval weekday(date) day), 5) + extract(year from date_sub(date, interval weekday(date) day)) * 100 as week, date from tr_inboxa where date > date_sub(now(), interval 4 week) and id > ?");
        my $sth = $src_dbh->prepare("select id, abonent, num, msg, transport_count, operator_id, date_format(date_sub(date, interval (weekday(date)) day), '\%Y\%m\%d') as week, date from tr_inboxa where date >= date_sub(now(), interval 16 day) and id > ?");

        $sth->execute($max_inbox_counted);

		my %weeks_seen;
		my %series_seen;


		my %abonent_cache;
		my %series_age_data;

        my $hr;
		my $i = 0;

        while (defined($hr=$sth->fetchrow_hashref)) {

			if ($hr->{id} > $max_inbox_seen) {
				$max_inbox_seen = $hr->{id};
			}

            my $usd = $prices{$hr->{operator_id}}{$hr->{num}}{price} || 0;

			$dbh->begin_work;

			unless ($abonent_cache{$hr->{abonent}}{$hr->{week}}) {
				my $t = {};

				($t->{diff}) = $dbh->selectrow_array("select unix_timestamp(max(date)) - unix_timestamp(min(date)) from tr_inboxa where abonent = ? and date between STR_TO_DATE(?, '%Y%m%d') and date_add(STR_TO_DATE(?, '\%Y\%m\%d'), interval 7 day)",
													  undef, $hr->{abonent}, $hr->{week}, $hr->{week});

				($t->{age}) = $dbh->selectrow_array("select unix_timestamp(now()) - unix_timestamp(min(date)) from tr_inboxa where abonent = ?",
													undef, $hr->{abonent});

				($t->{series} = $hr->{abonent}) =~ s/...$//;

				$abonent_cache{$hr->{abonent}}{$hr->{week}} = $t;
			}

			if (my $c = $abonent_cache{$hr->{abonent}}{$hr->{week}}) {
				$series_age_data{$c->{series}}{$hr->{week}}{age}  ||= 1000000000;
				$series_age_data{$c->{series}}{$hr->{week}}{diff} ||= 1000000000;

				my $oa = $series_age_data{$c->{series}}{$hr->{week}}{age};
				my $od = $series_age_data{$c->{series}}{$hr->{week}}{diff};

				my $na = $c->{age};
				my $nd = $c->{diff};

				$series_age_data{$c->{series}}{$hr->{week}}{age} = $na if $na < $oa;
				$series_age_data{$c->{series}}{$hr->{week}}{diff} = $nd if $nd > 0 and $nd < $od;
			}



			if ($usd > 0) {
				$dbh->do(<<EOF, undef, $hr->{abonent}, $hr->{date}, $usd, $hr->{operator_id}, $usd);
insert into abonent_money_daily
SET abonent = ?,
    date = date(?),
    usd = ?,
    operator_id = ?
ON DUPLICATE KEY UPDATE
    usd = usd + ?
EOF
			}

			if ( $usd > 2.5 ) {
				$dbh->do("
insert into abonent_money_weekly
SET abonent = ?,
    week = ?,
    usd = ?,
    last_inbox_id = ?,
    flag = 0
ON DUPLICATE KEY UPDATE
    usd = usd + ?,
    last_inbox_id = ?", undef,
						 $hr->{abonent}, $hr->{week}, $usd, $hr->{id}, $usd, $hr->{id});

				my $series = $hr->{abonent};
				$series =~ s/...$//;

				$dbh->do("
insert into abonent_series_weekly
SET series = ?,
    week = ?,
    usd = ?,
    cnt = 1,
    operator_id = ?,
    flag = 0,
    uniq = 0,
    last_inbox_id = ?
ON DUPLICATE KEY UPDATE
    last_inbox_id = ?,
    usd = usd + ?,
    cnt = cnt + 1", undef,
						 $series, $hr->{week}, $usd, $hr->{operator_id}, $hr->{id}, $hr->{id}, $usd);

				$series_seen{$series}{$hr->{week}} = 1;
			}

			if ( $usd > 4.5 ) {

				my $prefix = lc(translit_text($hr->{msg}));

				$dbh->do("
insert into abonent_prefix_weekly
SET abonent = ?,
    week = ?,
    prefix = ?,
    cnt = ?
ON DUPLICATE KEY UPDATE
    cnt = cnt + ?", undef,
						 $hr->{abonent}, $hr->{week}, $prefix, $hr->{transport_count}, $hr->{transport_count});

			}

            print $i."\n" unless $i++ % 1000;

			$dbh->commit;
            $max_inbox_counted = $hr->{id};
			$weeks_seen{$hr->{week}} = 1;
        }
		$dbh->do("insert into abonent_money_fetch set date = now(), host = ?, last_inbox_id = ?", undef,
				 'alt1-dt3.alt1.ru', $max_inbox_seen);
        print "UPTO: $max_inbox_counted\n";

		my @weeks = keys %weeks_seen;
		if ( @weeks ) {
			my $week_ph = join(',', ('?') x @weeks);
			print "PREFIX REGEN: ".join(",", @weeks).", '$week_ph'\n";
			$dbh->do("delete from abonent_prefix_furl where week in ($week_ph)",
					 undef, @weeks);
			$dbh->do("insert into abonent_prefix_furl SELECT prefix, week, count(distinct abonent) uniq, sum(cnt) sumc from abonent_prefix_weekly where week in ($week_ph) group by prefix, week having sumc > 1 and uniq > 1",
					 undef, @weeks);
		}

		$i = 0;
		print "SERIES UPDATE: ".scalar(%series_seen)."\n";
		while (my($k,$v) = each %series_seen) {
			my @series_weeks = keys %$v;
			my $week_ph = join(", ", ("?")x@series_weeks);

			my $sth = $dbh->prepare("select week, count(distinct abonent) as uniq from abonent_money_weekly where abonent like ? and week in ($week_ph) group by week");
			$sth->execute($k.'%', @series_weeks);
			while ( my($week, $uniq) = $sth->fetchrow_array) {
				$dbh->do("update abonent_series_weekly set uniq = ?, min_age = if(min_age > ?, ?, min_age), min_diff = if(min_diff > ?, ?, min_diff) where week = ? and series = ?", undef,
						 $uniq, $series_age_data{$k}{$week}{age}, $series_age_data{$k}{$week}{age}, $series_age_data{$k}{$week}{diff}, $series_age_data{$k}{$week}{diff}, $week, $k);
			}
            print $i."\n" unless $i++ % 100;
		}

		# print Dumper(\%series_age_data);
}

eval {
        main();
};

if ($@) {
        die "Unhandled: $@";
}

__END__

=pod

CREATE TABLE abonent_prefix_weekly
(abonent VARCHAR(20) not null,
 week integer not null,
 prefix varchar(100) not null,
 cnt integer not null default 0,
 PRIMARY KEY (abonent, week, prefix)
);

CREATE TABLE abonent_prefix_furl
(prefix varchar(100) not null,
 week integer not null,
 uniq integer not null default 0,
 sumc integer not null default 0,
 PRIMARY KEY (prefix, week),
 KEY (week)
);

CREATE TABLE abonent_series_weekly
( series VARCHAR(20) not null,
  week integer not null,
  usd float not null,
  cnt integer not null default 0,
  flag integer not null default 0,
  PRIMARY KEY(series, week)
);

SELECT prefix, count(distinct abonent), sum(cnt) from abonent_prefix_weekly where week = 200737 group by prefix;

SELECT series, sum(usd)
FROM abonent_money_weekly
WHERE week = ?
GROUP BY series

DROP TABLE if exists abonent_money_daily;
CREATE TABLE abonent_money_daily(
abonent varchar(20) not null,
operator_id integer not null,
date date not null,
usd float not null,
primary key (abonent, date, operator_id));

CREATE TABLE abonent_service_daily(
abonent varchar(20) not null,
date date not null,
service_id integer unsigned not null,
count integer not null default 0,
primary key (abonent, date));

CREATE TABLE abonent_money_fetch(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  host varchar(100) not null,
  last_inbox_id integer unsigned not null);

CREATE INDEX abonent_money_fetch___host__last_inbox_id___idx ON abonent_money_fetch(host, last_inbox_id);


=cut


