#!/usr/bin/php
<?php

// vim: tabstop=4

class Worker
{
	function __construct($INDB, $OUTDB, $mails)
	{
		$this->indb = new rdb($INDB);
		$this->outdb = new rdb($OUTDB);
		$this->system_root = $INDB[system_root];
		$this->log_file = "$INDB[system_root]/log/cuka2_bonus_log.log";
		$this->err_log_file = "$INDB[system_root]/log/cuka2_bonus_errors.log";
		$this->mails = $mails;
		$this->mail_title = array("From"=>"cuka_bonus_daemon");
		$this->log("Constructing completed.");
		$this->plugins = array();
		$this->allrates = array();
	}
	public static function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}
	//! \brief Error output.
	public function errorExit($s)
	{
	    error_log (date("[Y-m-d H:i:s] ")." EXIT: $s\n",3,$this->log_file);
	    exit;
	}	
	//! \biref Standart messages output.
	public function log($s)
	{
		error_log(date("[Y-m-d H:i:s] ")." $s\n",3,$this->log_file);
	}
	//! \brief Warnings output.
	public function w($s)
	{
		error_log(date("[Y-m-d H:i:s] ")."$s\n",3,$this->err_log_file);
	}
	//! \brief GetRates
	private function getRates($date)
	{
		$dt = substr($date,0,10);
		if(!$this->allrates[$dt]) $this->allrates[$dt] = $this->indb->selectAll2("SELECT * FROM set_exchange_rate WHERE date='$dt'",'currency');
		
		$err_description = $this->indb->dberror();
		if ($err_description || !$this->allrates)
		{
			$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
			$this->w($err_msg);
			/*mail($this->mails,
				"cuka_daemon_bonus ERROR: exchange rates wasn't finding",
				$err_msg,
				$this->mail_title);*/
			asgYell($err_msg, "cuka_daemon_bonus ERROR: exchange rates wasn't finding", $this->mails, $this->mail_title);
			exit;
		}
		return $this->allrates[$dt];
	}
	/** \brief Recount money from any currency to rubles.
	 *
	 *  \param currency - Currency for money.
	 *  \param money - Money in concrete currency.
	 *  \param rates - All rates.
	 */
	private function to_rur($currency, $money, $rates)
	{
		$x = $rates[$currency][price];
		if ((0 == $x) || ('' == $x)) return 0;
		return ($money * $x);
	}
	//
	//!
	//
	public function OutDBMapEscape($in)
	{
		// escape
		$tmp = array();
		foreach ($in as $k=>$v)
		{
			$tmp[$k] = $this->outdb->escape($v);
		}
		return $tmp;
	}
	//
	//! \brief Retrieving concrete plugin
	//
	private function GetPlugin($operator_id, $num, $service_id, $partner_id, $procent_category = null)
	{
		$c = 1;
		do
		{
			//
			// ���� plugin, �������� ��� ��������.
			// 
			if ($procent_category) $res = $this->plugins[$operator_id][$num][$service_id][$partner_id][$procent_category];
			else $res = $this->plugins[$operator_id][$num][$service_id][$partner_id];
			if (!$c) return $res;

			if (!$res)
			{
				//
				// �������� ��������� ����������.
				//
				$query = "select * 
						  from set_cyka_plugin_t 
						  where active>0 and 
					   			operator_id='$operator_id' and 
								num='$num' and 
								service_id='$service_id' and 
								partner_id='$partner_id'";
				$tmp = $this->indb->selectAll($query);
				//$this->w($query);
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					/*mail($this->mails,
						"cuka_daemon_bonus ERROR: database source error",
						$err_msg,
						$this->mail_title);*/
					asgYell($err_msg, "cuka_daemon_bonus ERROR: database source error", $this->mails, $this->mail_title);
					exit;
				}
				if ($tmp)
				foreach($tmp as $cur)
				{
					$this->plugins[$cur[operator_id]][$cur[num]][$cur[service_id]][$cur[partner_id]][$cur[procent_category]] = $cur;
				}
			} // END OF : if (!res)
		} // END OF : while (--$c)
		while($c--);

		return $res;
	}
	//
	//! \brief ��������� ���������� � �����, ��� �������� ����������� �����.
	//
	private function GetCustom($record)
	{
		$query = "SELECT *  
				  FROM cyka_payment 
				  WHERE inbox_id='{$record['transaction_id']}' and transport_type='{$record['transport_type']}'";
		$custom = $this->indb->selectRow($query);
		// work
		$err_description = $this->indb->dberror();
		if ($err_description)
		{
			//
			// ���� ������, �� ��������� � �������
			//
			$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
			$this->w($err_msg);
			/*mail($this->mails,
				"cuka_daemon_bonus ERROR: database source error",
				$err_msg,
				$this->mail_title);*/
			asgYell($err_msg, "cuka_daemon_bonus ERROR: database source error", $this->mails, $this->mail_title);
			exit;
		}
		return $custom;
	}
	//
	//! \brief Retrieving downloads
	//
	private function GetCountDownloads($record)
	{
		$query = "SELECT count(*) as num 
				  FROM cyka_downloads 
				  WHERE transaction_id='{$record['transaction_id']}' and transport_type='{$record['transport_type']}'";
		$count = $this->indb->selectField($query, 'num');
		// work
		$err_description = $this->indb->dberror();
		if ($err_description)
		{
			//
			// ���� ������, �� ��������� � �������
			//
			$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
			$this->w($err_msg);
			/*mail($this->mails,
				"cuka_daemon_bonus ERROR: database source error",
				$err_msg,
				$this->mail_title);*/
			asgYell($err_msg, "cuka_daemon_bonus ERROR: database source error", $this->mails, $this->mail_title);
			exit;
		}
		return $count;
	}
	//! \brief ���������
	private function Handling($custom)
	{
		$count = $this->GetCountDownloads($record);
		if ($count)
		{
			//
			// ������� �����
			//
			$custom = $this->GetCustom($record);
			if (!$custom)
			{
				$err_msg = print_r( $custom , true );
				$this->w( $err_msg );
				/*mail( $this->mails,
					  "cuka_daemon_bonus ERROR: ����� �� ������",
					  $err_msg,
					  $this->mail_title
					);*/
				asgYell($err_msg, "cuka_daemon_bonus ERROR: ����� �� ������", $this->mails, $this->mail_title);
				return null;
			}
			else $custom = $this->OutDBMapEscape($custom);

			//
			// ������� plugin
			//
			$plugin = $this->GetPlugin($custom[operator_id], $custom[num], $custom[service_id], $custom[partner_id], $record[procent_category]);
			if (!$plugin)
			{
				$err_msg = print_r( $custom , true );
				$this->w( $err_msg );
				/*mail( $this->mails,
					  "cuka_daemon_bonus ERROR: plugin �� ������",
					  $err_msg,
					  $this->mail_title
					);*/
				asgYell($err_msg, "cuka_daemon_bonus ERROR: plugin �� ������", $this->mails, $this->mail_title);
				return null;
			}
			else $plugin = $this->OutDBMapEscape($plugin);
			
			//
			// ��������� �������� ������!
			//
			$query = "REPLACE INTO cyka_payment_ext ".
					 "SET id=null,".
						 "transport_type=6,".
						 "date=now(),".
						 "service_id='{$custom['service_id']}',";
			if ($custom['subservice_id']) 
			{
				$query .="subservice_id='{$custom['subservice_id']}',";
			}
			//
			// Convert to RUR.
			//
			if ($plugin['currency'] != 'rur') $partner_income = $plugin['b_price_fixed_bonus_p'];
			else $partner_income = $this->to_rur($plugin['currency'], $plugin['b_price_fixed_bonus_p'], $this->GetRates(date("Y-m-d H:i:s")));
			//
			// Create .
			//
			$query .= 	 "abonent='{$custom['abonent']}',".
						 "operator_id='{$custom['operator_id']}',".
						 "num='{$custom['num']}',".
						 "msg=null,".
						 "test_flag='{$custom['test_flag']}',".
						 "cyka_processed=1,".
						 "is_error=0,".
						 "is_ignore=1,".
						 "partner_id='{$custom['partner_id']}',".
						 "price_abonent=0,".
						 "price_rest=0,".
						 "a1_income=0,".
						 "a1_income_after_custom_handling=0,".
						 "partner_income='{$partner_income}'".
						 "partner_income_ex='{$plugin['b_price_fixed_bonus_p']}'".
						 "partner_currency='{$plugin['currency']}'";
				
			$this->outdb->query($query);
			$err_description = $this->outdb->dberror();
			if ($err_description)
			{
				$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
				$this->w($err_msg);
				/*mail($this->mails,
					 "ELT_external_custom WARNING: database sync error",
					 $err_msg, 
					 $this->mail_title);*/
				asgYell($err_msg, "ELT_external_custom WARNING: database sync error", $this->mails, $this->mail_title);
				sleep( $this->sleep_time );
				exit;
			}
		}
		//
		// update source
		//
		$count = ($count) ? 1 : 0;
		$payment_id = ($payment_id) ? $payment_id : 'null';
		$query = "UPDATE cyka_payment_bonus 
				  SET processed=1,
				  	  is_download_exist='$count',
					  bonus_payment_id=$payment_id
				  WHERE id={$record['id']}";
		$this->indb->query($query);
		$err_description = $this->indb->dberror();
		if ($err_description)
		{
			$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
				$this->w($err_msg);
				/*mail($this->mails,
					 "cuka_bonus_daemon ERROR: database sync error",
					 $err_msg,
					 $this->mail_title);*/
				asgYell($err_msg, "cuka_bonus_daemon ERROR: database sync error", $this->mails, $this->mail_title);
				sleep( $this->sleep_time );
				exit;
		}
	}
	//! \brief ������
	public function run()
	{
		$this->log("STARTING...");
		$start_time = $this->getmicrotime();
		
		// executing
		$start_timestamp = $this->getmicrotime();
			
		$this->records = array();
		$query = "SELECT id, transaction_id, transport_type, date, procent_category 
				  FROM cyka_payment_bonus 
				  WHERE processed=0 and date<date_sub( date(now()) , interval 2 day)";
		$this->records = $this->indb->selectAll($query);
		// log
		$execute_time = $this->getmicrotime() - $start_timestamp;
		// work
		if (!($this->records))
		{
			//
			// ���� ������, �� ��������� � �������
			//
			$err_description = $this->indb->dberror();
			if ($err_description)
			{
				$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
				$this->w($err_msg);
				/*mail($this->mails,
					"cuka_daemon_bonus ERROR: database source error",
					$err_msg,
					$this->mail_title);*/
				asgYell($err_msg, "cuka_daemon_bonus ERROR: database source error", $this->mails, $this->mail_title);
				exit;
			}
			continue;
		}
		else 
		{
			$this->log("RETRIVING TIME FOR ".count($this->records)." EXTERNAL CUSTOMS IS $execute_time.");
		}
			
		$this->log("\nHANDLING RECORDS:".count($this->records));
		$start_timestamp = $this->getmicrotime();
		foreach($this->records as $record)
		{
			$record = $this->OutDBMapEscape($record);
			$this->Handling($record);
		} // foreach($this->records as $record)

		$execute_time = $this->getmicrotime() - $start_timestamp;
	
	} // END OF : Worker::run()
};	

include_once('../htdocss/config.php');
include_once('../htdocss/rdb.php'); 

// Mail adresses:
$mails = '';

$cp = new Worker($DB, $DB, $mails);
$cp->run();
?>
