#!/usr/bin/php -q
<?php

// vim: tabstop=4

chdir("/home/asg5/htdocss");
include_once('config.php');
include_once('db.php');

function notify_about_the_service_termination_of_validity($email_title, $target_emails)
{
	//////////////////////////////////////////////////////
	//                                                  //
	// ����������� �� ��������� ����� �������� �������. //
	//                                                  //
	//////////////////////////////////////////////////////
	$query = "SELECT *, if (date(now()) > date(offtime),1,0) as check_
			  FROM set_service
			  WHERE (offtime<>'0000-00-00 00:00:00') and (date(now())>=date(offtime)) and (active<>0)";
	$res = db::selectAll($query);

	//error_log(date("H:i:s")." {$query}\n",3,'service_offtime_notify.log');
	if ($res)
	{
	    foreach ($res as $row)
	    {
			$manager_email = db::selectField("SELECT email FROM set_cyka_manager WHERE id={$row[manager_id]}",'email');
			$target_emails_ = $target_emails;
			if ($manager_email) $target_emails_ .= ",$manager_email";
			if (!($row[check_]))
			{
				// service must be switched off now
				asgYell(
					"������ [{$row[id]}]: {$row[name]}\n���� � ����� ����������: {$row[offtime]}",
					"�� ������� ������������� ���������� �������!",
					$target_emails_,
					$email_title,
					true);
			}
			else
			{
				// service must be switched off before
				asgYell(
					"������ [{$row[id]}]: {$row[name]}\n���� � ����� ����������: {$row[offtime]}",
					"������������ ���������� �������!",
					$target_emails_,
					$email_title,
					true);
			}
		}
	}
}

function notify_about_the_keyword_termination_of_validity($email_title, $target_emails)
{
	//////////////////////////////////////////////////////////////
	//                                                          //
	// ����������� �� ��������� ����� �������� ��������� �����. //
	//                                                          //
	//////////////////////////////////////////////////////////////
	$query = "SELECT *, if (date(now()) > date(date),1,0) as check_
			  FROM set_keyword
			  WHERE (date<>'0000-00-00') and (date(now())>=date(date))";
	$res = db::selectAll($query);

	//error_log(date("H:i:s")." {$query}\n",3,'service_offtime_notify.log');
	if ($res)
	{
	    foreach ($res as $row)
	    {
			//$partner = db::selectRow("select * from set_cyka_partner where id='{$row[partner_id]}'");
			$partner = db::selectRow("select * from set_cyka_money_consumer_t where id='{$row[partner_id]}'");
			$manager_id = $partner[manager_id];
			$manager_email = db::selectField("SELECT email FROM set_cyka_manager WHERE id='{$manager_id}'",'email');
			$target_emails_ = $target_emails;
			if ($manager_email) $target_emails_ .= ",$manager_email";
			if (!($row[check_]))
			{
				// keyword must be switched off now
				asgYell(
					"�����: '{$row[num]}'\n".
					"�������� �����: '{$row[keyword]}'\n".
					"����� ������: '{$row[text]}'\n".
					"������� [{$row[partner_id]}]: '{$partner[name]}'\n".
					"��� ��������� ����� (0 - ������� ������, 1 - �������) : '{$row[keyword_type]}'\n".
					"���� ����������: '{$row[date]}'\n",
					"������� ����������� ���� �������� ��������� �����!",
					$target_emails_,
					$email_title,
					true);
			}
			else
			{
				// keyword must be switched off before
				asgYell(
					"�����: '{$row[num]}'\n".
					"�������� �����: '{$row[keyword]}'\n".
					"����� ������: '{$row[text]}'\n".
					"������� [{$row[partner_id]}]: '{$partner[name]}'\n".
					"��� ��������� ����� (0 - ������� ������, 1 - �������) : '{$row[keyword_type]}'\n".
					"���� ����������: '{$row[date]}'\n",
					"���� �������� ��������� ����� ���������!",
					$target_emails_,
					$email_title,
					true);
			}
		}
	}
}

function notify_about_the_detail_traffic_everyday_changing($email_title, $target_emails)
{
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
    GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
    GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_service_t op,
  set_sgroup_t sg
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.service_id and
  op.sgroup_id=sg.id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT sg.name as service_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>���������� ����� �� ���������� ������� � ������� �� ������� ��������<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(������ �������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>������ ��������</td>
					<td>% �� ����</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ��������� � ����, %</td>
					<td>��������� ������ �1 � ����, %</td>
					<td>��������� ������ �1 � ���� � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ��������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, "���������� ����� �� ���������� ������� � ������� �� ������� ��������.", $target_emails, $email_title, true);
}

function notify_about_the_traffic_everyday_changing($email_title, $target_emails)
{
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
    GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
    GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_operator_t op
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.operator_id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT op.name as operator_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>���������� ����� �� ���������� ������� � �������<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(��������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>��������</td>
					<td>% �� ����</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ���������, %</td>
					<td>��������� ������ �1, %</td>
					<td>��������� ������ �1 � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ����������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, "���������� ����� �� ���������� ������� � �������.", $target_emails, $email_title, true);
}


function notify_about_the_detail_traffic_everyday_changing_without_micropayments($email_title, $target_emails, $report_without_group_services)
{
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
	  ) t11
	JOIN set_service_t s1 ON s1.id=t11.service_id ";
	if ($report_without_group_services) $query .= "and not s1.sgroup_id in ($report_without_group_services) ";
	$query .= "GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
	  ) t22
	JOIN set_service_t s2 ON s2.id=t22.service_id ";
	if ($report_without_group_services) $query .= "and not s2.sgroup_id in ($report_without_group_services) ";
	$query .= "GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_service_t op,
  set_sgroup_t sg
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.service_id and
  op.sgroup_id=sg.id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT sg.name as service_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>���������� ����� �� ���������� ������� � ������� (��� �������������) �� ������� ��������<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(������ �������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>������ ��������</td>
					<td>% �� ���� (��� �������������)</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ��������� � ����, %</td>
					<td>��������� ������ �1 � ����, %</td>
					<td>��������� ������ �1 � ���� � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ��������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, "���������� ����� �� ���������� ������� � ������� (��� �������������) �� ������� ��������.", $target_emails, $email_title, true);
}

function notify_about_the_traffic_everyday_changing_without_micropayments($email_title, $target_emails, $report_without_group_services)
{
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
	JOIN set_service_t s1 ON s1.id=t11.service_id ";
	if ($report_without_group_services) $query .= "and not s1.sgroup_id in ($report_without_group_services) ";
	$query .= "GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
	JOIN set_service_t s2 ON s2.id=t22.service_id ";
	if ($report_without_group_services) $query .= "and not s2.sgroup_id in ($report_without_group_services) ";
	$query .= "GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_operator_t op
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.operator_id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT op.name as operator_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>���������� ����� �� ���������� ������� � ������� (��� �������������)<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(��������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>��������</td>
					<td>% �� ���� (��� �������������)</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ���������, %</td>
					<td>��������� ������ �1, %</td>
					<td>��������� ������ �1 � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ����������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, "���������� ����� �� ���������� ������� � ������� (��� �������������)", $target_emails, $email_title, true);
}

$db = new db($DB);
$report_without_group_services = "15,16,19,21";

$email_title   = array("Content-Type"=>"text/plain; charset=windows-1251", "Content-Transfer-Encoding"=>"8bit", "X-Priority"=> 1, "From"=>"cyka_info@alt1.ru");
$everyday_traffic_changing_email_title = array("Content-Type"=>"text/html; charset=windows-1251", "Content-Transfer-Encoding"=>"8bit", "X-Priority"=>1, "From"=>"cyka_info@alt1.ru");
$target_emails = "m.fokin@alt1.ru";
//$target_emails = "";
$everyday_traffic_changing_emails = "st@port102.org,vt@dom2010.com,ks@gjxnt.com,alex@gjxnt.com,td@gjxnt.com";
$everyday_traffic_service_changing_emails =
	"st@port102.org,vt@dom2010.com,ks@gjxnt.com,alex@gjxnt.com,td@gjxnt.com";
$everyday_traffic_changing_emails_without_micropayments = "st@port102.org,vt@dom2010.com,ks@gjxnt.com,alex@gjxnt.com,td@gjxnt.com";

//////#notify_about_the_service_termination_of_validity($email_title, $target_emails);
//////#notify_about_the_keyword_termination_of_validity($email_title, $target_emails);
//$everyday_traffic_changing_emails = "a.arhipov@alt1.ru";
//$everyday_traffic_service_changing_emails = "a.arhipov@alt1.ru";
//$everyday_traffic_changing_emails_without_micropayments = "a.arhipov@alt1.ru";
notify_about_the_traffic_everyday_changing
    ($everyday_traffic_changing_email_title, $everyday_traffic_changing_emails);
notify_about_the_detail_traffic_everyday_changing
    ($everyday_traffic_changing_email_title, $everyday_traffic_service_changing_emails);

// not need
#notify_about_the_traffic_everyday_changing_without_micropayments
#    ($everyday_traffic_changing_email_title, $everyday_traffic_changing_emails_without_micropayments, $report_without_group_services);
#notify_about_the_detail_traffic_everyday_changing_without_micropayments
#    ($everyday_traffic_changing_email_title, $everyday_traffic_changing_emails_without_micropayments, $report_without_group_services);

?>
