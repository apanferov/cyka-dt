#!/usr/bin/perl
#######################################################
#  
#  Encoder
#  
#######################################################

use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use IO::File;
use IO::Select;
use Log::Log4perl qw(get_logger);
use Disp::Recode qw(encode_sms);
use Disp::AltDB;
use Disp::Utils qw(yell);
use Disp::Config qw(param);

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $terminated = 0;
my ($log, $db);

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
   "log4perl.rootLogger"       => Config->param('log_level','encoder').', Logfile',
   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
	"log4perl.appender.Logfile.recreate" => 1,
	"log4perl.appender.Logfile.recreate_check_interval" => 300,
	"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
	"log4perl.appender.Logfile.umask" => "0000",
   "log4perl.appender.Logfile.filename" => Config->param('log_filename','encoder'),
   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
   "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','encoder')
    );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################

	$db = new Disp::AltDB();
	my $smsc_to_use7bit = $db->get_smsc_use7bit();
	my $smsc_to_use1251 = $db->get_smsc_use1251();
	my $smsc_to_cut_type = $db->get_smsc_cut_type();
	my $smsc_to_client_type = $db->get_smsc_client_type();

#######################################################
#  Main loop
#######################################################

	$log->info('Started.');
	while (!$terminated)	{
		$log->debug("Getting messages.");
		my $msgs = $db->get_sms_ready_to_encode();
		my $msg_count = scalar keys(%$msgs);
		if ($msg_count) {
			$log->info("$msg_count message(s) fetched.");
			foreach (sort {$a <=> $b} keys(%$msgs)) {
				my $msg = $msgs->{$_};
				$log->info("Encoding MSG$msg->{id} ...");
				$msg->{use7bit} = $smsc_to_use7bit->{$msg->{smsc_id}} & 0x7f;
				$msg->{gsm_alphabet} = $smsc_to_use7bit->{$msg->{smsc_id}} & 0x80;
				$msg->{use1251} = $smsc_to_use1251->{$msg->{smsc_id}};
				$msg->{cut_type} = $smsc_to_cut_type->{$msg->{smsc_id}};
				$msg->{client_type} = $smsc_to_client_type->{$msg->{smsc_id}};
				if ($msg->{registered_delivery} and $msg->{inbox_id}) {
					my ($smsc_id,$insms_ids) = $db->{db}->selectrow_array('select smsc_id,transport_ids from tr_inboxa where id=?',undef,$msg->{inbox_id});
					if ($smsc_to_client_type->{$smsc_id} eq 'smsc') {
						my ($insms_id) = split(/\s*,\s*/,$insms_ids || '');
						$msg->{insms_id} = $insms_id;
					}
				}
				my ($retval,$retmsg) = (0,'');
				eval { ($retval,$retmsg) = encode_sms($msg);};
				if ((!$@) and $retval) {
					$log->debug("Used method: $retmsg");
					$log->debug(Dumper($msg));
					my @ids = $db->move_outbox_to_outsms($msg);
					$log->info("Message MSG$msg->{id}(".join(',',@ids).") was encoded. Method: $retmsg From:$msg->{num} To:$msg->{abonent} Text:$msg->{msg}");
				}
				else {
					$log->debug("Used method: $retval");
					$log->error($@);
					$log->error(Dumper($msg));
					yell(Dumper($msg),code => '',process => 'ENCODER',
						header => "ENCODER: Message MSG$msg->{id} wasn't encoded.",
						ignore => { log4perl => 1  });
					$db->move_outbox_to_outsms($msg);
					$log->error("Message MSG$msg->{id} wasn't encoded. Method: $retmsg / $retval From:$msg->{num} To:$msg->{abonent} Text:$msg->{msg}");
				}
			}
		}
		else {
			sleep 1;
		}
	}

};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();


########### DO_EXIT ###########################

sub do_stop
{
   if ($db)   {
      $db->finish();   }

   $log->info("Stopped.\n\n\n");
}

sub do_die
{
   my ($str)=@_;
   $log->fatal($str);
	yell($str,code => 'FATAL',process => 'ENCODER',
		header => "ENCODER: $str",
		ignore => { log4perl => 1  });
   do_stop;
   exit(0);
}



