#!/usr/bin/php
<?php

// vim: tabstop=4

chdir("/home/asg5/htdocss");
include_once('config.php');
include_once('db.php'); 

function increase_managers_premium($current_date)
{
	$rt = db::selectAll("SELECT * FROM set_cyka_increase_managers_premium_t");

	db::beginTransaction();

	db::query("DELETE FROM cyka_increase_managers_premium_t 
			   WHERE year=year(date_sub('$current_date',INTERVAL 1 month)) and month=month(date_sub('$current_date',INTERVAL 1 month))");
	if ($rt && is_array($rt))
	foreach ($rt as $cur)
	{
		$service_id = $cur[service_id];
		$manager_id = $cur[manager_id];
		$leader_id  = $cur[leader_id];
		
		$query = "SELECT sum(cp.a1_income) as a1_income 
				   FROM cyka_payment_furl cp
				   WHERE test_flag=0 and 
				   		 (date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
						 			   last_day(date_sub('$current_date', INTERVAL 1 month))) and service_id='$service_id'";
		$a1_income = db::selectField($query, 'a1_income');
		
		$query = "SELECT sum(cp.a1_income) as a1_income 
				   FROM cyka_payment_furl cp
				   WHERE test_flag=0 and 
				   		 (date between date_format(date_sub('$current_date', INTERVAL 2 month),'%Y-%m-01') and 
						 			   last_day(date_sub('$current_date', INTERVAL 2 month))) and service_id='$service_id'";
		$previous_a1_income = db::selectField($query, 'a1_income');

		$query = "SELECT sum(if (c.type=0, -1 * c.money * ex.price, c.money * ex.price)) as price
				  FROM set_cyka_expense c 
				  LEFT JOIN set_exchange_rate ex ON ex.currency=c.currency and ex.date=date(c.timestamp)
				  WHERE c.service_id='$service_id' and 
				  		c.timestamp between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
											date_format(last_day(date_sub('$current_date', INTERVAL 1 month)),'%Y-%m-%d 23:59:59')";
		$expence_projects = db::selectField($query, 'price');
		
		$query = "SELECT sum(if (c.type=0, -1 * c.money * ex.price, c.money * ex.price)) as price
				  FROM set_cyka_expense c 
				  LEFT JOIN set_exchange_rate ex ON ex.currency=c.currency and ex.date=date(c.timestamp)
				  WHERE c.service_id='$service_id' and 
				  		c.timestamp between date_format(date_sub('$current_date', INTERVAL 2 month),'%Y-%m-01') and 
											date_format(last_day(date_sub('$current_date', INTERVAL 2 month)),'%Y-%m-%d 23:59:59')";
		$previous_expence_projects = db::selectField($query, 'price');

		//
		//
		//
$previous_expence_projects = 0;
		$delta = (($a1_income + $expence_projects) - ($previous_a1_income + $previous_expence_projects));
		$manager_premium = $delta * ($cur[manager_procent] / 100);
		$leader_premium  = $delta * ($cur[leader_procent]  / 100);
		$sql = "INSERT INTO cyka_increase_managers_premium_t 
				SET year=YEAR(date_sub('$current_date', interval 1 month)),
					month=month(date_sub('$current_date',INTERVAL 1 month)),
					service_id='$service_id',
					manager_id='$manager_id',
					manager_procent='$cur[manager_procent]',
					a1_income='$delta',
					manager_premium='$manager_premium',
					leader_id='$leader_id',
					leader_procent='$cur[leader_procent]',
					leader_premium='$leader_premium'";
		//echo $sql;
		db::query($sql);
		if (db::dberror()!='')
		{
			print_r(db::dberror());
			db::rollbackTransaction();
			exit;
		}
	}
	
	db::commitTransaction();
	return 1;
}


function managers_premium($current_date)
{

	$rt = db::selectAll("SELECT * FROM set_cyka_managers_premium_t");
	$check = db::selectField("SELECT if(month(date_format('$current_date','%Y-%m-01'))=7, 1, 0) as checking", 'checking');
	db::beginTransaction();

	db::query("DELETE FROM cyka_managers_premium_t 
			   WHERE year=year(date_sub('$current_date',INTERVAL 1 month)) and month=month(date_sub('$current_date',INTERVAL 1 month))");
	if ($rt && is_array($rt))
	foreach ($rt as $cur)
	{
		$service_id = $cur[service_id];
		$manager_id = $cur[manager_id];
		$leader_id  = $cur[leader_id];
		
		$query = "SELECT sum(ifnull(a1_clean_income__rur_without_nds,0)) as a1_clean_income__rur_without_nds 
				  FROM traffic_furl_price 
				  WHERE date between date_format(date_sub('$current_date',INTERVAL 1 month),'%Y-%m-01') and 
				  					 last_day(date_sub('$current_date', INTERVAL 1 month)) and service_id=$service_id";
		$premium = db::selectField($query, 'a1_clean_income__rur_without_nds');

		$query = "SELECT sum(ifnull(a1_clean_income__rur_without_nds,0)) as a1_clean_income__rur_without_nds 
				  FROM traffic_ext_furl_price 
				  WHERE date between date_format(date_sub('$current_date',INTERVAL 1 month),'%Y-%m-01') and 
				  					 last_day(date_sub('$current_date', INTERVAL 1 month)) and service_id=$service_id";
		$premium_ext = db::selectField($query, 'a1_clean_income__rur_without_nds');

		$query2 = "SELECT sum(cp.price_rest - cp.a1_income) as expence 
				   FROM cyka_payment_furl cp 
				   WHERE test_flag=0 and 
				   		 (date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
						 			   last_day(date_sub('$current_date', INTERVAL 1 month))) and service_id='$service_id'";
		$expence  = db::selectField($query2, 'expence');
/*
		$query2 = "SELECT sum(cp.a1_income) as expence 
				   FROM cyka_payment_furl cp 
				   WHERE test_flag=0 and 
				   		 (date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
						 			   last_day(date_sub('$current_date', INTERVAL 1 month))) and service_id='$service_id'";
		$expence  = db::selectField($query2, 'expence');
*/
//
		// �������!
		//
		// ��� �� ���� ������ �������� ������ � ������ ������ �� ��������� �������� � ������� �� ���� 2007.
		// �������� ������ ���: �������� �., �������� �., ��� �., �������� �.
		// ID: 1329,2667,1003,2740
//if (!(2667==$manager_id or 2740==$manager_id)) continue;
/*
		if ($check && (1329==$manager_id ||
					   2667==$manager_id ||
					   1003==$manager_id ||
					   2740==$manager_id))
		{
			// ������ ��������� ��� �� ������� �� ���� ����������� �� ������� � ������� �� ���� �� ������� �������
			$query = "SELECT sum(if (c.type=0, -1 * c.money * ex.price, c.money * ex.price)) as price
					  FROM set_cyka_expense c 
					  LEFT JOIN set_exchange_rate ex ON ex.currency=c.currency and ex.date=date(c.timestamp)
					  WHERE c.service_id='$service_id' and c.timestamp between '2007-02-01' and '2007-06-30 23:59:59' and 
					  		not c.id in (428,431)";
		}
		else
		{
			$query = "SELECT sum(if (c.type=0, -1 * c.money * ex.price, c.money * ex.price)) as price
					  FROM set_cyka_expense c 
					  LEFT JOIN set_exchange_rate ex ON ex.currency=c.currency and ex.date=date(c.timestamp)
					  WHERE c.service_id='$service_id' and 
					  		c.timestamp between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
												date_format(last_day(date_sub('$current_date', INTERVAL 1 month)),'%Y-%m-%d %H:%i:%s')";
		}
 */
/*
		$query = "SELECT sum(cp.a1_income) as a1_income 
				   FROM cyka_payment_furl cp
				   WHERE test_flag=0 and 
				   		 (date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
						 			   last_day(date_sub('$current_date', INTERVAL 1 month))) and service_id='$service_id'";
		$a1_income = db::selectField($query, 'a1_income');
*/
//		$expence_projects = db::selectField($query, 'price');
//		print_r($expence_projects);

		//
		// .
		//
//print_r("manager_id=$manager_id, service_id=$service_id -> $premium_ext + $premium - $expence - $expence_projects\n");
//if ($manager_id = 1043) print_r("manager_id=$manager_id, service_id=$service_id -> $expence + $expence_projects\n");
		$a1_income = /*$a1_income + $expence_projects;*/ $premium_ext + $premium;// - $expence + $expence_projects;
		$manager_premium = $a1_income * ($cur[manager_procent] / 100);
		$leader_premium  = $a1_income * ($cur[leader_procent]  / 100);
		$sql = "INSERT INTO cyka_managers_premium_t 
				SET year=YEAR(date_sub('$current_date', interval 1 month)),
					month=month(date_sub('$current_date',INTERVAL 1 month)),
					service_id='$service_id',
					manager_id='$manager_id',
					manager_procent='$cur[manager_procent]',
					a1_income='$a1_income',
					manager_premium='$manager_premium',
					leader_id='$leader_id',
					leader_procent='$cur[leader_procent]',
					leader_premium='$leader_premium'";
		//echo $sql;
		db::query($sql);
		#if (db::dberror()!='') db::rollbackTransaction();
	}
	
	db::commitTransaction();
	return 1;
}


function top_managers_premium($current_date)
{
	$rt    = db::selectAll("SELECT * FROM set_cyka_top_managers_premium_t");

	//
	// ������������ ������ �� ��������:
	// --------------------------------
	// ������ N 431 - MPAY
	// ������ N 479 - ��������
	// ������ N 301 - �������� GAT
	// ������ N 426 - Papa Racot
	// # 692 - SMSProxy!!!
	//
	$service_ignore = " (not service_id in (692)) and ";

	db::beginTransaction();
	
	$res   = db::selectRow("select year(date_sub('$current_date', INTERVAL 1 MONTH)) as year, month(date_sub('$current_date', INTERVAL 1 MONTH)) as month");
	$year  = $res[year];
	$month = $res[month];
	db::query("DELETE FROM cyka_top_managers_premium_t WHERE year=$year and month=$month");
	
	foreach($rt as $cur)
	{
		$sql1 = "SELECT sum(a1_income)
				 FROM cyka_payment_furl c
				 WHERE $service_ignore date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and last_day(date_sub('$current_date', INTERVAL 1 month))";
//print_r($sql1);
		$sql2 = "SELECT sum(a1_clean_income__rur_without_nds) 
				 FROM traffic_furl_price c 
				 WHERE $service_ignore date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and last_day(date_sub('$current_date', INTERVAL 1 month))";
//print_r($sql2);
		$sql3 = "SELECT sum(a1_clean_income__rur_without_nds) 
				 FROM traffic_ext_furl_price c 
				 WHERE $service_ignore date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and last_day(date_sub('$current_date', INTERVAL 1 month))";
//print_r($sql3);
		if ($cur[source] == 1) 
		{
			$money = db::selectField($sql1);
		}
		else
		{
			$money = db::selectField($sql2) + db::selectField($sql3);
		}
//		return 1;
		$sql = "INSERT INTO cyka_top_managers_premium_t 
				SET year=$year,
					month=$month,
					manager_id=$cur[manager_id],
					manager_procent=$cur[manager_procent],
					manager_premium=".(($money*$cur[manager_procent])/100).",
					source=$cur[source];";
		db::query($sql);
	}
	db::commitTransaction();
		
	return 1;
}


function dro_managers_premium($current_date)
{
	// ���������� ����� ��������� � ���������� ��� ����� �������� ���!
	//
	// ��������: ���� �� ����������� ������ �� wap-cpa (transport_type=2) � 
	// ����������� mt (transport_type=3). ������, ��� ���� ������ ������������ 
	// ������� �������� � ��������, ������� ��� �� ����� ��������� ��.
	//
	// � ��������� ���� ����������� ��������� ������ ����������� � ������.
	//
	/*$query ="select	m.id as manager_id,
					l.id as leader_id,
					sum(ifnull(tr.a1_clean_income__rur_without_nds,0)) as a1_clean_income__rur_without_nds,
					sum(ifnull(((tr.a1_clean_income__rur_without_nds * dro.manager_procent) / 100),0)) AS manager_premium__rur_without_nds,
					sum(ifnull(((tr.a1_clean_income__rur_without_nds * dro.leader_procent) / 100),0)) AS leader_premium__rur_without_nds
			 from traffic_furl_price tr
			 join set_cyka_manager sm on sm.id=tr.service_manager_id and sm.group_id!=5
			 JOIN set_cyka_dro_premium_t dro on dro.operator_id=tr.operator_id and dro.manager_id=tr.operator_manager_id
			 join set_cyka_manager m on m.id=tr.operator_manager_id and m.group_id=5
			 join set_cyka_manager l on l.id=m.main_manager_id and m.group_id=5
			 WHERE (date BETWEEN date_format(CONCAT(year(date_sub(date(now()),interval 1 month)),'-',month(date_sub(date(now()), interval 1 month)),'-01'),'%Y-%m-%d') and LAST_DAY(date_sub(now(), interval 1 month))) 
			 group by m.id,l.id
			 order by tr.operator_name";
	$premiums = db::selectAll($query);
	if (!$premium)
	{
		if (db::dberror()) return 0;
	}
	//
	// ����������� ������ � �� ������� ��������� � ��� ����� � 
	// ����������� ������ ����� �� ������ ��������� � ������� ������ 
	// � ���������.
	//
	$query2="select m.id as manager_id,
					sum(ifnull(cp.price_rest - cp.a1_income,0)) as expence,
					sum(ifnull((cp.price_rest - cp.a1_income) * dro.manager_procent / 100, 0)) as manager_expence,
					sum(ifnull((cp.price_rest - cp.a1_income) * dro.leader_procent / 100, 0)) as leader_expence
			 from cyka_payment_furl cp
			 join set_service s on s.id=cp.service_id
			 join set_cyka_manager sm on sm.id=s.manager_id and sm.group_id!=5
			 join set_operator o on o.id=cp.operator_id
			 join set_cyka_dro_premium_t dro on dro.operator_id=cp.operator_id and dro.manager_id=o.manager_id
			 join set_cyka_manager m on m.id=o.manager_id and m.group_id=5
			 join set_cyka_manager l on l.id=m.main_manager_id and m.group_id=5
			 WHERE test_flag=0 and (date BETWEEN date_format(CONCAT(year(date_sub(date(now()),interval 1 month)),'-',month(date_sub(date(now()), interval 1 month)),'-01'),'%Y-%m-%d') and LAST_DAY(date_sub(now(), interval 1 month))) 
			 group by m.id";
	$expences = db::selectAll2($query2, 'manager_id');
	if (!$expences)
	{
		if (db::dberror()) return 0;
	}
	//
	// ��������� ���������.
	//
	if ($premiums)
	{
//		db::beginTransaction();
		
		$res = db::selectRow("select year(date_sub(now(), INTERVAL 1 MONTH)) as year, month(date_sub(now(), INTERVAL 1 MONTH)) as month");
		$year  = $res[year];
		$month = $res[month];
		db::query("DELETE FROM cyka_dro_premium_t WHERE year=$year and month=$month");
		
		foreach ($premiums as $premium)
		{
			$a1_clean_income = $premium[a1_clean_income__rur_without_nds];
			$a1_expence      = $expences[$premium[manager_id]][expence];
			$manager_premium = $premium[manager_premium__rur_without_nds] - $expences[$premium[manager_id]][manager_expence];
			$leader_premium  = $premium[leader_premium__rur_without_nds] - $expences[$premium[manager_id]][leader_expence];
			
			$query = "INSERT INTO cyka_dro_premium_t 
					  SET year='$year',
					  	  month='$month',
						  manager_id='{$premium[manager_id]}',
						  leader_id='{$premium[leader_id]}',
						  a1_clean_income__rur_without_nds='$a1_clean_income',
						  a1_expence__rur_without_nds='$a1_expence',
						  manager_premium__rur_without_nds='$manager_premium',
						  leader_premium__rur_without_nds='$leader_premium',
						  currency='rur'";
			db::query($query);
		}
		//if (count($premiums) != db::selectField("SELECT count(*) as count FROM cyka_dro_premium_t WHERE year=$year, month=$month", 'count'))
		//{
		//	db::rollbackTransaction();
		//	return 0;
		//}

//		db::commitTransaction();
	}*/
	
	$rt=db::selectAll("SELECT * FROM set_cyka_dro_premium_t");
	$res = db::selectRow("select year(date_sub('$current_date', INTERVAL 1 MONTH)) as year, month(date_sub('$current_date', INTERVAL 1 MONTH)) as month");
	$year  = $res[year];
	$month = $res[month];
	
	db::beginTransaction();
	
	db::query("DELETE FROM cyka_dro_premium_t WHERE year=$year and month=$month");

	if ($rt && is_array($rt))
	foreach($rt as $cur)
	{
		// SMS traffic
		$sql1 = "
		SELECT			
			sum(ifnull(tr.a1_clean_income__rur_without_nds,0)) as a1_clean_income__rur_without_nds,
			sum(ifnull((tr.a1_clean_income__rur_without_nds * $cur[manager_procent] / 100),0)) AS manager_premium__rur_without_nds,
			sum(ifnull((tr.a1_clean_income__rur_without_nds * $cur[leader_procent] / 100),0)) AS leader_premium__rur_without_nds
		from traffic_furl_price tr
		join set_cyka_manager sm on sm.id=tr.service_manager_id and sm.group_id!=5
		JOIN set_cyka_dro_premium_t dro on dro.operator_id=tr.operator_id and dro.manager_id=tr.operator_manager_id
		join set_cyka_manager m on m.id=tr.operator_manager_id and m.group_id=5
		join set_cyka_manager l on l.id=m.main_manager_id and m.group_id=5
		WHERE
			(date BETWEEN date_format(CONCAT(year(date_sub(date('$current_date'),interval 1 month)),
											 '-',
											 month(date_sub(date('$current_date'), interval 1 month)),
											 '-01'),'%Y-%m-%d') and 
						  LAST_DAY(date_sub('$current_date', interval 1 month))) 
			and
			tr.operator_id='$cur[operator_id]'
			and
			tr.operator_manager_id='$cur[manager_id]' 
			and
			(not tr.service_id in (431,479,301,426))";
		$rt1 = db::selectRow($sql1);
		// not SMS traffic
		$sql3 = "
		SELECT			
			sum(ifnull(tr.a1_clean_income__rur_without_nds,0)) as a1_clean_income__rur_without_nds,
			sum(ifnull((tr.a1_clean_income__rur_without_nds * $cur[manager_procent] / 100),0)) AS manager_premium__rur_without_nds,
			sum(ifnull((tr.a1_clean_income__rur_without_nds * $cur[leader_procent] / 100),0)) AS leader_premium__rur_without_nds
		from traffic_ext_furl_price tr
		join set_cyka_manager sm on sm.id=tr.service_manager_id and sm.group_id!=5
		JOIN set_cyka_dro_premium_t dro on dro.operator_id=tr.operator_id and dro.manager_id=tr.operator_manager_id
		join set_cyka_manager m on m.id=tr.operator_manager_id and m.group_id=5
		join set_cyka_manager l on l.id=m.main_manager_id and m.group_id=5
		WHERE
			(date BETWEEN date_format(CONCAT(year(date_sub(date('$current_date'),interval 1 month)),
											 '-',
											 month(date_sub(date('$current_date'), interval 1 month)),
											 '-01'),'%Y-%m-%d') and 
						  LAST_DAY(date_sub('$current_date', interval 1 month))) 
			and
			tr.operator_id='$cur[operator_id]'
			and
			tr.operator_manager_id='$cur[manager_id]' 
			and
			(not tr.service_id in (431,479,301,426))";
		$rt3 = db::selectRow($sql3);
		
		$sql2="
		SELECT
			sum(ifnull(cp.price_rest - cp.a1_income,0)) as expence,
			sum(ifnull((cp.price_rest - cp.a1_income) * $cur[manager_procent] / 100, 0)) as manager_expence,
			sum(ifnull((cp.price_rest - cp.a1_income) * $cur[leader_procent] / 100, 0)) as leader_expence
		FROM cyka_payment_furl cp
		join set_service s on s.id=cp.service_id
		join set_cyka_manager sm on sm.id=s.manager_id and sm.group_id!=5
		join set_operator o on o.id=cp.operator_id
		join set_cyka_dro_premium_t dro on dro.operator_id=cp.operator_id and dro.manager_id=o.manager_id
		join set_cyka_manager m on m.id=o.manager_id and m.group_id=5
		join set_cyka_manager l on l.id=m.main_manager_id and m.group_id=5
		WHERE
			test_flag=0 
			and 
			(date BETWEEN date_format(CONCAT(year(date_sub(date('$current_date'),interval 1 month)),
											 '-',
											 month(date_sub(date('$current_date'), interval 1 month)),
											 '-01'),'%Y-%m-%d') and 
						  LAST_DAY(date_sub('$current_date', interval 1 month))) 
			and
			cp.operator_id='$cur[operator_id]'
			and
			m.id='$cur[manager_id]' 
			and
			(not cp.service_id in (431,479,301,426))";	
		$rt2 = db::selectRow($sql2);
		
		$a1_clean_income = $rt1[a1_clean_income__rur_without_nds] + $rt3[a1_clean_income__rur_without_nds];
		$a1_expense 	 = $rt2[expence];
		$manager_premium = $rt1[manager_premium__rur_without_nds] + $rt3[manager_premium__rur_without_nds] - $rt2[manager_expence];
		$leader_premium  = $rt1[leader_premium__rur_without_nds] + $rt3[leader_premium__rur_without_nds] - $rt2[leader_expence];
		
		$query = "INSERT INTO cyka_dro_premium_t 
				  SET year='$year',
				  	  month='$month',
					  manager_id='{$cur[manager_id]}',
					  leader_id='{$cur[leader_id]}',
					  a1_clean_income__rur_without_nds='$a1_clean_income',
					  a1_expence__rur_without_nds='$a1_expense',
					  manager_premium__rur_without_nds='$manager_premium',
					  leader_premium__rur_without_nds='$leader_premium',
					  operator_id='$cur[operator_id]',
					  currency='rur'";
		db::query($query);	

		db::commitTransaction();
	}
	return 1;
}

function manager_lo_premium($current_date)
{
	$res = db::selectRow("select year(date_sub('$current_date', INTERVAL 1 MONTH)) as year, month(date_sub('$current_date', INTERVAL 1 MONTH)) as month");
	$year  = $res[year];
	$month = $res[month];
	
	$rt = db::selectAll("SELECT * FROM set_cyka_dk_premium_t");
	
	if ($rt && is_array($rt))
	{
		db::beginTransaction();
		db::query("DELETE FROM cyka_dk_premium_t 
				   WHERE year=year(date_sub('$current_date',INTERVAL 1 month)) and month=month(date_sub('$current_date',INTERVAL 1 month))");	
		foreach ($rt as $cur)
		{
			$leader_id = db::selectField("SELECT main_manager_id FROM set_cyka_manager WHERE id=$cur[manager_id]");		
			$manager_id = db::escape($cur[manager_id]);
			$manager_procent = $cur[manager_procent];
			$leader_procent = $cur[leader_procent];

			$query = "SELECT sum(cpf.a1_income) 
					  FROM cyka_payment_lo_furl cpf
					  JOIN set_cyka_legal_owner l ON l.id=cpf.legal_owner_id 
					  JOIN set_cyka_manager m ON m.id=l.manager_id
					  WHERE cpf.date between date_format(date_sub('$current_date', INTERVAL 1 month),'%Y-%m-01') and 
					  		last_day(date_sub('$current_date', INTERVAL 1 month)) and
							m.id='$manager_id'";
			$a1_income = db::selectField($query);
			$manager_premium = ($a1_income * $manager_procent) /100;
			$leader_premium = ($a1_income * $leader_procent) /100;

			$sql = "INSERT INTO cyka_dk_premium_t 
					SET year='$year',
						month='$month',
						manager_id='$manager_id',
						leader_id='$leader_id',
						manager_procent='$manager_procent',
						leader_procent='$leader_procent',
						a1_income='$a1_income',
						manager_premium='$manager_premium',
						leader_premium='$leader_premium'";
			db::query($sql);
			if (mysql_error()) 
			{
				db::rollbackTransaction();
				return 0;
			}
		}
		db::commitTransaction();
	}
	return 1;
}

$db = new db($DB);

$current_date = date("Y-m-d H:i:s");
//
// MUST BE FIRST DAY OF NEXT MONTH
//
//$current_date = date("2008-10-01 00:00:01");

managers_premium($current_date);
//dro_managers_premium($current_date);
//top_managers_premium($current_date);
//manager_lo_premium($current_date);
//increase_managers_premium($current_date);
?>
