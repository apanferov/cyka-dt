#!/usr/bin/perl

my $root;
BEGIN {
  $root = '/gate/asg5';
}

use strict;
use locale;
use lib "$root/lib";
use Disp::MonitorCtl;
use Data::Dumper;
use Getopt::Long;

eval {
	my $mc = new_aggregated Disp::MonitorCtl
	  ( { pid => "$root/var/run/monitor-transport.pid",
		  status => "$root/var/log/monitor-transport.status" },
		{ pid => "$root/var/run/monitor-billing.pid",
		  status => "$root/var/log/monitor-billing.status" } );

	my ($get_list, $reload, $restart_child, $long_list);

	my $result = GetOptions( "list" => \$get_list,
							 "reload" => \$reload,
							 "full-list" => \$long_list,
							 "kill-child=s" => \$restart_child);


	if ($get_list) {
		print map { $_->{name} ."\n" } @{$mc->list};
		exit;
	}

	if ($long_list) {
		$mc->print_long_list(\*STDOUT);
		exit;
	}

	if ($reload) {
		$mc->reload;
		print "All monitors signalled";
		exit;
	}

	if ($restart_child) {
		$restart_child =~ /^(.*)$/;
		$restart_child = $1;
		if ($mc->kill_child($restart_child)) {
			print "Child $restart_child signalled\n";
		} else {
			print "Child $restart_child signalling failed\n";
		}
		exit;
	}

	print "UNKNOWN COMMAND\n";
};


if ( $@ ) {
	print $@;
}


