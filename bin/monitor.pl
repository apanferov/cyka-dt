#!/usr/bin/perl
use strict;
use warnings;
use locale;
use lib '../lib/';
use POSIX ":sys_wait_h";
use Proc::Daemon;
use Sys::Syslog;

use Disp::Config;
use Disp::DaemonManager;
use Disp::DaemonManager::Child;
use Disp::DaemonManager::PermanentSource;
use Disp::DaemonManager::TranscieverSource;
use Disp::Utils;
use Data::Dumper;
use Log::Log4perl qw/get_logger/;

sub main {
	Config::init(skip_db_connect => 1, config_file => '../etc/asg.conf');
	Log::Log4perl::init('../etc/log.conf');
	yell("Starting monitor",
		process => 'MONITOR',
		header => "MONITOR: Starting monitor",
		ignore => { database => 1 });

	my $perm_daemons = [ map { new Disp::DaemonManager::Child($_) }
						 (
						  { id => 'dispatcher',
							active => 1,
							subdir => Config->param('system_root').'/bin',
							execute => 'perl ./dispatcher.pl',
						  },
						  { id => 'distributor',
							active => 1,
							subdir => Config->param('system_root').'/bin',
							execute => 'perl ./distributor.pl',
						  },
						  { id => 'replication',
							active => 1,
							subdir => Config->param('system_root').'/bin',
							execute => './replication.php',
						  },

						  { id => 'encoder',
							active => 1,
							subdir => Config->param('system_root').'/bin',
							execute => 'perl ./encoder.pl',
						  },
						  { id => 'decoder',
							active => 1,
							subdir => Config->param('system_root').'/bin',
							execute => 'perl ./decoder.pl',
						  }) ];

	my $dm = new Disp::DaemonManager( status_file => '../log/monitor.status',
									  pid_file => '../var/run/monitor.pid' );

	my $ts = new Disp::DaemonManager::TranscieverSource( subdir => Config->param('system_root').'/bin',
														 execute => 'perl ./sms_client.pl',
														 dbname => Config->param(qw/name database/),
														 dbuser => Config->param(qw/username database/),
														 dbhost => Config->param(qw/host database/),
														 dbpassword => Config->param(qw/password database/),
													   );

	$dm->add_data_source(source => Disp::DaemonManager::PermanentSource->new($perm_daemons),
						 prefix => 'permanent');

	$dm->add_data_source(source => $ts, prefix => 'smpp');
	$dm->run;
	yell("Monitor finished",
		process => 'MONITOR',
		header => "MONITOR: Monitor finished");
}

eval {
	main();
};
if ($@) {
	yell("ABNORMAL MONITOR TERMINATION: $@",
		code => 'FATAL_ERROR',
		process => 'MONITOR',
		header => "MONITOR: ABNORMAL MONITOR TERMINATION",
		ignore => { database => 1 });
}
