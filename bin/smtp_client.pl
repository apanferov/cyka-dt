#!/usr/bin/perl
use strict;
use locale;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::MonitoredDaemon;
use Disp::EmailSender;
use Disp::EmailReciever;
use Disp::Utils;

eval {
    if ($ENV{SMPP_MODE} == 11) {
	Disp::MonitoredDaemon
	    ->new( stub => Disp::EmailSender->new(),
		   smsc_id => $ENV{SMPP_SMSC_ID},
		   idle_sleep => 5,
		   debug_level => $ENV{SMPP_DEBUG_LEVEL},
		   client_type => 'smtp',
		   kind => 'snd',
		 )
	      ->run();
    } elsif ($ENV{SMPP_MODE} == 12) {
	Disp::MonitoredDaemon
	    ->new( stub => Disp::EmailReciever->new(),
		   smsc_id => $ENV{SMPP_SMSC_ID},
		   idle_sleep => 30,
		   debug_level => $ENV{SMPP_DEBUG_LEVEL},
		   client_type => 'smtp',
		   kind => 'rcv')
	      ->run();
    }
};

if ($@) {
    yell("Died with :\n".$@,
	 code => 'ERROR',
	 process => "SMTP_$ENV{SMPP_SMSC_ID} MODE_$ENV{SMPP_MODE}",
	 header => "SMTP_$ENV{SMPP_SMSC_ID} MODE_$ENV{SMPP_MODE} abnormal termination"
	);
}

