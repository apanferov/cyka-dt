#!/usr/bin/perl
use lib '../lib';
use strict;
use Time::HiRes qw(gettimeofday tv_interval);
use Disp::Utils qw(raw_sendmail);
use Disp::Config;

$SIG{TERM} = $SIG{INT} = sub { exit; };
$SIG{USR1} = 'IGNORE'; # log detach signal

Config::init(config_file => "../etc/asg.conf",
	skip_db_connect => 1);

my $PIDFILE = '../var/run/yell.pid';
my $LOGFILE = Config->safe_param(yell_file => '$ENV{HOME}/yell.log');
my $POSFILE = "${LOGFILE}.pos";

my $pid = PidFile->new($PIDFILE);
my $log = LogFile->new($LOGFILE, PosFile->new($POSFILE));

my $elapsed = 0;
my $number  = 0;

while (1)
{
	my $t0  = [gettimeofday];
	my $msg = $log->fetch(newer => 259200, nodup => 1);

	$elapsed += tv_interval($t0);
	$number++;

	if ($number > 100) {
		select(undef,undef,undef, 60.0 - $elapsed) if ($elapsed < 60);
		$number = $elapsed = 0;
	}
	raw_sendmail $msg;
	$log->commit_pos;
}






BEGIN {

package PidFile;
use strict;
use Carp;
use IO::File;

sub new
{
	my($class, $fname) = @_;

	if (my $pidf = IO::File->new($fname, 'r')) {
		kill(0, int $pidf->getline) && croak "Pidfile $fname is locked";
		$pidf = undef;
	}
	my $fh = IO::File->new($fname, 'w') or croak "Cannot open pidfile: $fname";
        $fh->autoflush(1);
	$fh->print($$);

	bless { fname => $fname },
		(ref $class||$class);
}

sub DESTROY
{
	unlink $_[0]->{fname};
}






package PosFile;
use strict;
use Carp;
use IO::File;

sub new
{
	my($class, $fname) = @_;
	my $fh = IO::File->new($fname, 'a+') or croak "Cannot open posfile: $fname";
	$fh->seek(0,0);
	$fh->autoflush;

	bless {
		fh    => $fh,
		pos   => int $fh->getline
		},
		(ref $class||$class);
}

sub commit
{
	my $self = shift;
	$self->{fh}->truncate(0);
	$self->{fh}->seek(0,0);
	$self->{fh}->print($self->{pos});
}

sub get
{
	$_[0]->{pos};
}

sub set
{
	my($self, $pos) = @_;
	$self->{pos} = $pos;
	$self;
}






package LogFile;
use strict;
use Carp;
use IO::File;
use Date::Parse qw/str2time/;

sub new
{
	my($class, $fname, $pos) = @_;
	my $fh = IO::File->new($fname, 'r') or croak "Cannot open logfile: $fname";

	bless {
		fname => $fname,
		fh    => $fh,
		pos   => $pos,
		prev  => ''
		},
		(ref $class||$class);
}

sub fetch
{
	my($self, %params) = @_;
	local($/) = "========================================\n";

	while (1)
	{
		if (my @a = $self->{fh}->stat and my @b = stat($self->{fname})) {
			if ($a[1] != $b[1]) {
				$self->{fh}->close;
				sleep(1) while ($self->{fh}->open($self->{fname}, 'r'));
			}
		}
		$self->{fh}->seek($self->{pos}->get, 0);
		$self->{pos}->set($self->{fh}->tell);

		my $msg = $self->{fh}->getline;
		if (!$msg) {
			#warn "Empty message was read. Sleeping for 1 second.";
			sleep(1);
			next;
		}

		if($params{nodup} && $msg eq $self->{prev}) {
			#warn "Skipping duplicate message.";
			$self->commit_pos;
			next;
		}

		$self->{prev} = $msg;
		my($date) = $msg =~ /^Date:\s(.+?)$/m;

		if (!$date) {
			#warn "Date not found. Message too old.";
			$self->commit_pos;
			next;
		}
		if ($params{newer} && str2time($date) < time - $params{newer}) {
			#warn "Message too old ($date).";
			$self->commit_pos;
			next;
		}
		return $msg;
	}
}

sub commit_pos
{
	my($self) = @_;
	$self->{pos}->set($self->{fh}->tell)->commit;
}

}#BEGIN
