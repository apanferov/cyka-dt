#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char **argv) {

  char **new_argv = malloc(sizeof(char*)*(argc+3));
  int i;

  new_argv[0] = "/usr/bin/perl";
  new_argv[1] = "/gate/asg5/bin/monitorctl.pl";
  for ( i = 1; i < argc; i++ ) {
	new_argv[1+i] = argv[i];
  }
  new_argv[argc + 1] = 0;
  execv("/usr/bin/perl", new_argv);
}
