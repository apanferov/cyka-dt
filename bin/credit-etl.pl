#!/usr/bin/perl
# -*- tab-width: 4 -*-
package main;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use English '-no_match_vars';
use Carp;
use utf8;

use Data::Dumper;
use Log::Log4perl qw/get_logger/;

use Disp::Utils qw/yell with_transaction/;
use Disp::Config;
use Disp::CreditFSM;
use Disp::CreditFSMWorld;

sub main {
    my $dbh = Config->dbh();
    my $world = Disp::CreditFSMWorld->new($dbh);

    my $quit_flag;
    my $quit_sub = sub { $quit_flag = 1 };
    $SIG{$_} = $quit_sub for qw/INT TERM QUIT/;

    my $log = get_logger('credit-etl.main-loop');

    while (not $quit_flag) {
        my $done_something;
        with_transaction {
            if (my $message = $world->fetch_unprocessed_message) {
                $done_something = 1;
                $world->mark_message_processed($message);
                $log->debug("Found unprocessed message $message->{id}");
                if (my $event = Disp::CreditFSM::credit_message_to_event($world, $message)) {
                    $log->debug("Unprocessed message $message->{id} resulted in $event->{event} event");
                    my $line = $world->load_line($message->{credit_line_id});
                    $line ||= Disp::CreditFSM::initialize_line($world, $message);
                    process_event($world, $line, $event);
                }
            }
            elsif (my $line = $world->fetch_unprocessed_line) {
                $done_something = 1;
                $log->debug("Found scheduled line $line->{credit_line_id}");
                my $event = Disp::CreditFSM::credit_line_to_event($world, $line);
                if ($event) {
                    process_event($world, $line, $event);
                } else {
                    $line->{next_action_date} = Disp::CreditFSM::next_action_date($world, $line);
                    $world->save_line($line);
                }
            }
        } $dbh;
        sleep($world->param(idle_timeout => 'credit')) unless $done_something;
    }
}

sub process_event {
    my ( $world, $line, $event ) = @_;

    my $old_status = $line->{status};

    my $messages = $world->line_messages($line);

    Disp::CreditFSM::run({
        event     => $event,
        world     => $world,
        line      => $line,
        messages  => $messages,
    });

    $line->{next_action_date} = Disp::CreditFSM::next_action_date($world, $line);

    if ($line->{status} ne $old_status) {
        $line->{last_status_change} = $world->now;
    }

    $world->save_line($line);
}

Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
Config::init(config_file => ( $ENV{DEBUG_CONFIG_FILE} || "$FindBin::RealBin/../etc/asg.conf" ), skip_db_connect => 0);
eval {
    main();
};
if ($EVAL_ERROR) {
    my $err = $EVAL_ERROR;
    yell(
        $EVAL_ERROR,
        code => 'PROC_STOP',
        process => 'CREDIT ETL',
        header => 'Unexpected credit-etl.pl termination',
    );
    die $err;
}




