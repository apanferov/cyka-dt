#!/usr/bin/php
<?php

// vim: tabstop=4

/** \brief Main CYKA handler class.
 *
 *  Customs and downloads handling.
 */
class cyka2
{
	private $allrates;
	
	function __construct($DB, $admin_mails, $mails, $mails2)
	{
		$this->db = new db($DB);
		$this->system_root = $DB[system_root];
		$this->customs_log_file = "$DB[system_root]/log/cyka2_customs_handling.log";
		$this->downloads_log_file = "$DB[system_root]/log/cyka2_downloads_handling.log";
		$this->log_dir = "$DB[system_root]/log/cyka/";
		$this->admin_mails  = $admin_mails;
		$this->mails  = $mails;
		$this->mails2 = $mails2;
		$thid->precision = '6'; // ���������� ��� ����������� �������� ��� �����������, �� � ���� ������ �� �������!
		$this->sendOnlyAdmin = true;
		//if(!$this->db->link) $this->errorExit("database not available");
	}
	
	private function getRates($date)
	{
		$dt = substr($date,0,10);
		if(!$this->allrates[$dt]) $this->allrates[$dt] = db::selectAll2("SELECT * FROM set_exchange_rate WHERE date='$dt'",'currency');
		return $this->allrates[$dt];
	}
	
	private function GetPlugin($operator_id, $num, $service_id, $partner_id, $procent_category = null)
	{
		$c = 1;
		do
		{
			//
			// ���� plugin, �������� ��� ��������.
			// 
			if ($procent_category) $res = $this->info[plugin][$operator_id][$num][$service_id][$partner_id][$procent_category];
			else $res = $this->info[plugin][$operator_id][$num][$service_id][$partner_id];
			if (!$c) return $res;

			if (!$res)
			{
				//
				// �������� ��������� ����������.
				//
				$query = "select * 
						  from set_cyka_plugin_t 
						  where active>0 and 
					   			operator_id='$operator_id' and 
								num='$num' and 
								service_id='$service_id' and 
								partner_id='$partner_id'";
				$tmp = db::selectAll($query);
				//$this->w($query);
				if ($tmp)
				foreach($tmp as $cur)
				{
					$this->info[plugin][$cur[operator_id]][$cur[num]][$cur[service_id]][$cur[partner_id]][$cur[procent_category]] = $cur;
				}
			} // END OF : if (!res)
		} // END OF : while (--$c)
		while($c--);

		return $res;
	}
	
	private function Normalize($in)
	{
		return sprintf("%.{$this->precision}f", $in);
	}
	
	// ������� ����������� �� ������� ��������� ������.
	private function csendmail($title, $body, $only_admin = false, $manager_id = null)
	{
		if (!$only_admin) 
		{
			$this->sendmail($this->mails, $title, $body, $only_admin, $manager_id);
		}
		else
		{
			$this->sendmail($this->admin_mails, $title, $body, $only_admin, $manager_id);
		}
	}
	
	// ������� ����������� �� ������� ��������� �������.
	private function dsendmail($title, $body, $only_admin = false, $manager_id = null)
	{
		if (!$only_admin) 
		{
			$this->sendmail($this->mails2, $title, $body, $only_admin, $manager_id);
		}
		else
		{
			$this->sendmail($this->admin_mails, $title, $body, $only_admin, $manager_id);
		}
	}

	// ������� ����������� �� ������� ���������
	private function sendmail($target, $title, $body, $only_admin, $manager_id)
	{
		if (!$only_admin && $manager_id)
		{
			if ($target != null && $target != '') 
			{
				$target .= ",".$this->info['managers'][$manager_id]['email'];
			}
			else
			{
				$target = $this->info['managers'][$manager_id]['email'];
			}
		}
		asgYell($body, $title, $target);
	}

	private function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}

	//! \brief Error output.
	private function errorExit($s)
	{
		$_date = date("Y-m-d H:i:s");
	    error_log("[$date] EXIT: $s\n", 3, $this->customs_log_file   );
	    error_log("[$date] EXIT: $s\n", 3, $this->downloads_log_file );
	    exit;
	}	

	//! \biref Standart messages output.
	private function log($s, $isDownload = false)
	{
		$_date = date("Y-m-d H:i:s");
		if (true == $isDownload) 
			error_log("[$_date] $s\n", 3, $this->downloads_log_file);
		else 
			error_log("[$_date] $s\n", 3, $this->customs_log_file);
	}

	//! \brief Warnings output.
	private function w($s, $isDownload=false)
	{
	    if (true == $isDownload) 
			error_log("$s\n",3,$this->downloads_log_file);
	    else 
			error_log("$s\n",3,$this->customs_log_file);
	}

	//! \brief Retrieving supplemental information.
	private function load_info()
	{
		$this->info[default_partner] 		= db::selectRow( "select * from set_cyka_partner where is_def limit 1");
		$this->info[default_partner_agent] 	= db::selectRow( "select * from set_cyka_partner_agent where is_def limit 1");
		$this->info[default_manager] 		= db::selectRow( "select * from set_cyka_manager where is_def limit 1");
		$this->info[default_project]		= db::selectRow( "select project_id as id, project_header as name, is_def as is_def 
															  from media.project where is_def limit 1");
		$this->info[default_country]		= db::selectRow( "select * from media.countries where is_def limit 1");
		$this->info[operators] 				= db::selectAll2("select * from set_operator",'id');
		$this->info[countries]				= db::selectAll2("select * from media.countries", 'id');
		$this->info[services] 				= db::selectAll2("select * from set_service",'id');
		$this->info[subservices]			= db::selectAll2("select subservice_id as id, service_id, subservice_name as name from set_subservice",'id');
		$this->info[partners] 				= db::selectAll2("select * from set_cyka_partner",'id');
		$this->info[partner_agents] 		= db::selectAll2("select * from set_cyka_partner_agent",'id');
		$this->info[operator_agents] 		= db::selectAll2("select * from set_cyka_operator_agent",'id');
		foreach ($this->info[operator_agents] as $k=>$agent) $this->info[operator_agents_grupped][$agent[operator_id]][$k] = $agent;
		$this->info[procent_categories] 	= db::selectAll2("select * from set_cyka_procent_category", 'id');
		$this->info[projects]				= db::selectAll2("select project_id as id, project_header as name, is_def as is_def from media.project",'id');
		$this->info[legal_owners] 			= db::selectAll2("select * from set_cyka_legal_owner",'id');
		$this->info[legal_owner_agents] 	= db::selectAll2("select * from set_cyka_legal_owner_agent_t",'id');
		$this->info[managers] 				= db::selectAll2("select * from set_cyka_manager",'id');
		
		$this->w($this->stat);
		if (mysql_error() || ('' == $this->info[default_partner]))
		{
			$this->w("��� ���������� � ��!");
			$this->dsendmail("��� ���������� � ��.", "[".mysql_errno()."]".mysql_error(),$this->sendOnlyAdmin);
			sleep(5);
			exit;
		}
	}	

	/** \brief Recount money from any currency to rubles.
	 *
	 *  \param currency - Currency for money.
	 *  \param money - Money in concrete currency.
	 *  \param rates - All rates.
	 */
	private function to_rur($currency, $money, $rates)
	{
		$x = $rates[$currency][price];
		if ((0 == $x) || ('' == $x)) return 0;
		return ($money * $x);
	}

	/** \brief Recount money from rubles to any currency.
	 *
	 *  \param currency - Currency for money.
	 *  \param money - Money in rubles.
	 *  \param rates - All rates.
	 */
	private function from_rur($currency, $money, $rates)
	{
		$x = $rates[$currency][price];
		if ((0 == $x) || ('' == $x)) return 0;
		return ($money / $x);
	}

	/** \brief Custom updating.
	 *
	 *  ��������� ���������� � ������: �������� ��������, ������������ ������ �1, 
	 *  �������� ������ �1, ������ � ���������� ��������, ������ � ����������
	 *  ���������� �������� � ������ � ���������� ��������� �1 � ��� ����������.
	 */
	private function custom_updating($custom)
	{
		if (('' == $custom[partner_id])       || ('0' == $custom[partner_id])      ) $custom[partner_id]       = $this->info[default_partner][id]; 
		if (('' == $custom[transport_type])   || (is_null($custom[partner_id]))    ) $custom[transport_type]   = 0; 
		if (('' == $custom[partner_agent_id]) || ('0' == $custom[partner_agent_id])) $custom[partner_agent_id] = $this->info[default_partner_agent][id];
		if (('' == $custom[manager_id])       || ('0' == $custom[manager_id])      ) $custom[manager_id]       = $this->info[default_manager][id];
		if (('' == $custom[leader_id])        || ('0' == $custom[leader_id])       ) $custom[leader_id]        = $this->info[default_manager][id];

        if ('' == $custom[partner_currency]) $custom[partner_currency] = 'rur';
        if ('' == $custom[partner_agent_currency]) $custom[partner_agent_currency] = 'rur';
        if ('' == $custom[manager_currency]) $custom[manager_currency] = 'rur';

		$error_msg = db::escape($custom[error_msg]);
		$comment = db::escape($custom[comment]);
	$msg = db::escape($custom[msg]);
        $query="replace into cyka_payment
					set inbox_id = '{$custom[inbox_id]}',
					transport_type='{$custom[transport_type]}',
					date='{$custom[date]}',
					service_id='{$custom[service_id]}',
					subservice_id='{$custom[subservice_id]}',
					abonent='{$custom[abonent]}',
					operator_id='{$custom[operator_id]}',
					num='{$custom[num]}',
					msg='{$msg}',
					in_count='{$custom[in_count]}',
					out_count='{$custom[out_count]}',
					measure='{$custom[measure]}',
					price_abonent = '{$custom[price_abonent]}',
					price_rest = '{$custom[price_rest]}',
					a1_income = '{$custom[a1_income]}',
					a1_income_after_custom_handling = '{$custom[a1_income_after_custom_handling]}',
					cyka_processed = '{$custom[cyka_processed]}',
					is_error = '{$custom[is_error]}',
					partner_id = '{$custom[partner_id]}',
					partner_k_price_abonent = '{$custom[partner_k_price_abonent]}',
					partner_k_price_rest = '{$custom[partner_k_price_rest]}',
					partner_b_price_fixed = '{$custom[partner_b_price_fixed]}',
					partner_income = '{$custom[partner_income]}',
					partner_income_ex = '{$custom[partner_income_ex]}',
					partner_currency = '{$custom[partner_currency]}',
					partner_agent_id = '{$custom[partner_agent_id]}',
					partner_agent_k_price_abonent = '{$custom[partner_agent_k_price_abonent]}',
					partner_agent_k_price_rest = '{$custom[partner_agent_k_price_rest]}',
					partner_agent_b_price_fixed = '{$custom[partner_agent_b_price_fixed]}',
					partner_agent_income = '{$custom[partner_agent_income]}',
					partner_agent_income_ex = '{$custom[partner_agent_income_ex]}',
					partner_agent_currency = '{$custom[partner_agent_currency]}',
					manager_id = '{$custom[manager_id]}',
					manager_k_price_abonent = '{$custom[manager_k_price_abonent]}',
					manager_k_price_rest = '{$custom[manager_k_price_rest]}',
					manager_b_price_fixed = '{$custom[manager_b_price_fixed]}',
					manager_income = '{$custom[manager_income]}',
					manager_income_ex = '{$custom[manager_income_ex]}',
					manager_currency = '{$custom[manager_currency]}',
					leader_id = '{$custom[leader_id]}',
					leader_k_price_abonent = '{$custom[leader_k_price_abonent]}',
					leader_k_price_rest = '{$custom[leader_k_price_rest]}',
					leader_b_price_fixed = '{$custom[leader_b_price_fixed]}',
					leader_income = '{$custom[leader_income]}',
					leader_income_ex = '{$custom[leader_income_ex]}',
					leader_currency = '{$custom[leader_currency]}',
					error_msg = '{$error_msg}',
					is_ignore = '{$custom[is_ignore]}',
					comment = null,
					is_fraud = '{$custom[is_fraud]}'";
		$res = db::query($query, false);
		if (!$res)
	   	{
			$this->w($query);
			$this->w(mysql_error());
			return 0;
		} else {
			db::query("delete from cyka_payment_not_processed
				where inbox_id = '{$custom[inbox_id]}'
				and transport_type='{$custom[transport_type]}'
				and in_count='{$custom[in_count]}'");
		}

		//
		// BONUS for 4m and 4m with WAP-PUSH
		//
		if ((30 == $custom[service_id]) || (119 == $custom[service_id]))
		{
			$query = "REPLACE INTO cyka_payment_bonus 
					  SET id=null,
					  	  transaction_id='{$custom['inbox_id']}',
						  transport_type='{$custom['transport_type']}',
					  	  date='{$custom['date']}',
						  processed=0,
						  is_download_exist=0,
						  procent_category='{$custom['procent_category']}'";
			$res = db::query($query, false);
			if (!$res)
		   	{
				$this->w($query);
				$this->w(mysql_error());
				return 0;
			}
		}
		return 1;
	}

	/** \brief Updating cyka_downloads record for concrete download.
	 */
	private function download_updating($download)
	{
		$this->w($download[comment], true);

		$error_msg = db::escape($download[error_msg]);
		$comment = db::escape($download[comment]);
		$content_name = db::escape($download[content_name]);
		$content_title = db::escape($download[content_title]);
		$content_singer = db::escape($download[content_singer]);
		$content_author_words = db::escape($download[content_author_words]);
		$content_author_music = db::escape($download[content_author_music]);
		$content_owner_code = db::escape($download[content_owner_code]);
		$query = "UPDATE cyka_downloads 
				  SET cyka_processed='{$download[cyka_processed]}',
				 	  is_error='{$download[is_error]}',
					  error_msg='$error_msg',
					  comment='$comment',
					  content_name='{$content_name}',
					  content_category='{$download[content_category]}',
					  content_cost_category='{$download[content_cost_category]}',
					  content_title='{$content_title}',
					  content_singer='{$content_singer}',
					  content_author_words='{$content_author_words}',
					  content_author_music='{$content_author_music}',
					  content_owner_code='{$content_owner_code}'
				  WHERE id='{$download[id]}'";
		if (!(db::query($query)))
		{
			$this->w("ERROR QUERY: ".$query,true);
			$this->w("ERROR MYSQL: ".mysql_error(),true);
			return 0;
		}
		else
		{
			return 1;
		}
	}

	/** \brief Create new record in cpayment_lo table for concrete pay for legal owner.
	 *
	 *  ������ ������ � ������� cpayment_lo � ������� ���������� ��������������� �
	 *  ���������� ���������������.
	 *  \return 1(ok) if all modification operation is success, otherwise 0(error).
	 */
	private function create_cpayment_lo_record($cpayment_lo_record)
	{
		if (!$cpayment_lo_record[legal_owner_id]) $cpayment_lo_record[legal_owner_id] = '0';
		if (!$cpayment_lo_record[legal_owner_agent_id]) $cpayment_lo_record[legal_owner_agent_id] = '0';

        if (!$cpayment_lo_record[legal_owner_currency]) $cpayment_lo_record[legal_owner_currency] = 'rur';
        if (!$cpayment_lo_record[legal_owner_agent_currency]) $cpayment_lo_record[legal_owner_agent_currency] = 'rur';

		if ('' == $cpayment_lo_record[legal_owner_type]) $cpayment_lo_record[legal_owner_type] = 1; // copyright

		//
		// � ������, ���� ��������������� ��� ����� ������������� ��� ������ ������� 
		// (�������� �� ������� ����������� � �������� �������), �� ��������� ����������,
		// ������ � ��� �����������, ��������� ���� ����� ����������!
		//
		$record = db::selectRow(
					"SELECT * 
					FROM cyka_payment_lo 
					WHERE inbox_id = '{$cpayment_lo_record[inbox_id]}' and
						  download_id = '{$cpayment_lo_record[download_id]}' and 
						  transport_type = '{$cpayment_lo_record[transport_type]}' and 
						  legal_owner_id = '{$cpayment_lo_record[legal_owner_id]}'");
		if ($record)
		{
			// ��������� legal_owner_income_ex � ������ ���������� ������!
			if ($record[legal_owner_currency] != $cpayment_lo_record[legal_owner_currency])
			{
				$cpayment_lo_record[legal_owner_income_ex] = 
					$this->from_rur( $record[legal_owner_currency], 
									 $cpayment_lo_record[legal_owner_income], 
									 $this->getRates($record[date]) );
			}
	        $query = "UPDATE cyka_payment_lo ".
				 	 "SET legal_owner_income = legal_owner_income + '{$cpayment_lo_record[legal_owner_income]}',
				 		  legal_owner_income_ex = legal_owner_income_ex + '{$cpayment_lo_record[legal_owner_income_ex]}'
					  WHERE inbox_id = '{$cpayment_lo_record[inbox_id]}' and
					 	    download_id = '{$cpayment_lo_record[download_id]}' and
						    transport_type = '{$cpayment_lo_record[transport_type]}' and
							legal_owner_id = '{$cpayment_lo_record[legal_owner_id]}'";
		}
		else
		{
	        $query = "INSERT INTO cyka_payment_lo ".
				 	 "SET inbox_id = '{$cpayment_lo_record[inbox_id]}',
						  download_id = '{$cpayment_lo_record[download_id]}',
  						  order_number = '{$cpayment_lo_record[order_number]}',
						  transport_type = '{$cpayment_lo_record[transport_type]}',
						  date = '{$cpayment_lo_record[date]}',
						  legal_owner_id = '{$cpayment_lo_record[legal_owner_id]}',
						  legal_owner_k_price_abonent = '{$cpayment_lo_record[legal_owner_k_price_abonent]}',
						  legal_owner_k_price_rest = '{$cpayment_lo_record[legal_owner_k_price_rest]}',
						  legal_owner_b_price_fixed = '{$cpayment_lo_record[legal_owner_b_price_fixed]}',
					 	  legal_owner_income = '{$cpayment_lo_record[legal_owner_income]}',
				 		  legal_owner_income_ex = '{$cpayment_lo_record[legal_owner_income_ex]}',
					 	  legal_owner_currency = '{$cpayment_lo_record[legal_owner_currency]}',
						  legal_owner_type = '{$cpayment_lo_record[legal_owner_type]}'";
		}
		$res = db::query($query);
		if (mysql_error())
	   	{
			$this->w("ERROR QUERY: ".$query,true);
			$this->w("ERROR MYSQL: ".mysql_error(),true);
			return 0;
		}
		return 1;
	}
	
	/** \brief Make new payment record for operator agent.
	 *
	 *  ���� ����� �� �������, ����� �������� ����������� ������ � ������� ����������� ������
	 *  � ��������� �1. ������ ���������� �������� ���������� � ������������ ����2.
	 *
	 *  \param custom - Current custom parameters.
	 *  \param operator_agent - Current operator agent properties.
	 *  \param rates - Exchange rates.
	 *  \param order - ���������� ����� ���������� ��������� ����� ���� ������ �����������.
	 *  \return If queries were finished success then 1, otherwise 0.
	 */
	private function make_oa_payment($custom, $operator_agent, $rates, $order)
	{
	 	$operator_agent_income = $operator_agent[k_price_abonent_po] * $custom[price_abonent] + 
								 $operator_agent[k_price_rest_po] * $custom[price_rest] + 
								 $this->to_rur($operator_agent[currency_po], $operator_agent[b_price_fixed_po], $rates);
		// income to concrete currency
	 	$operator_agent_income_ex = $this->from_rur($operator_agent[currency_po], $operator_agent_income, $rates);
		// LOG
		$k1 = (int)($operator_agent[k_price_abonent_po] * 100);
		$k2 = (int)($operator_agent[k_price_rest_po] * 100);

		$custom[comment] .= "[{$operator_agent[id]}][{$operator_agent[name]}]  ".
							"$k1% * $custom[price_abonent] ���. + ".
							"$k2% * $custom[price_rest] ���. + ".
							"$operator_agent[b_price_fixed_po] $operator_agent[currency_po] = ".
							"$operator_agent_income ���.\n";
		$operator_agent_income    = $this->Normalize($operator_agent_income);
		$operator_agent_income_ex = $this->Normalize($operator_agent_income_ex);
		if ($operator_agent_income > $custom[a1_income])
		{
			$custom[comment] .= "������: ����� $operator_agent_income ���. ���������� ��������� �������� ��������� ������!\n";
			//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: ����� ���������� ��������� �������� ��������� ������.",$custom[comment]);
			// ����������� �������, ������ � �����!
		}

		//
		// Clear ALL old record with current inbox
		//
		$res = db::query("DELETE FROM cyka_payment_oa WHERE inbox_id='{$custom[inbox_id]}' and transport_type='{$custom[transport_type]}'",false);
		if (!$res)
		{
			$custom[comment] .= "QUERY: ".$query."\n";
			$custom[comment] .= "ERROR: ".mysql_error()."\n";
			$custom[error_msg] .= "ERROR: ".mysql_error();
			$custom[is_error] = 1;
			return $custom;
		}
		
		//
		// Insert new record
		//
		$query="INSERT INTO cyka_payment_oa 
				SET inbox_id='{$custom[inbox_id]}',
					transport_type = '{$custom[transport_type]}',
					order_number='{$order}',
				   	date='{$custom[date]}', 
					money_consumer_id='{$operator_agent[id]}', 
					k_price_abonent='{$operator_agent[k_price_abonent_po]}', 
					k_price_rest='{$operator_agent[k_price_rest_po]}', 
					b_price_fixed='{$operator_agent[b_price_fixed_po]}', 
					income='{$operator_agent_income}',
					income_ex='{$operator_agent_income_ex}',
					currency='{$operator_agent[currency_po]}'
					";
		$res = db::query($query,false);
		if (!$res)
		{
			$custom[comment] .= "QUERY: ".$query."\n";
			$custom[comment] .= "ERROR: ".mysql_error()."\n";
			$custom[error_msg] .= "ERROR: ".mysql_error();
			$custom[is_error] = 1;
			return $custom;
		}

		$custom[a1_income] -= $operator_agent_income;
		return $custom;
	}

	////////////////////////////////////
	///! \brief ���������� ���������. //
	////////////////////////////////////
	private function fill_managers_payments($custom, $partner, $service, $rates)
	{
		$managers   = $this->info[managers];
		$manager_id = $custom[manager_id];
		$leader_id  = $custom[leader_id];

		if (($custom[manager_id] || $custom[leader_id]) && ($partner[vip] < 1))
		{
			// ���� ������������� ����� �1 � ������ �� VIP, ����� ������� � ���������!
			if ($custom[a1_income]<0)
			{
    	        $custom[manager_income] = $custom[a1_income];
                $custom[manager_income_ex] = $this->from_rur($custom[manager_currency], $custom[manager_income], $rates);
			    $log .= "� ��������� �1 [$manager_id] '{$managers[$manager_id][name]}' ����� {$custom[a1_income]} ���.".
						"������������ ��������� [$leader_id] '{$managers[$leader_id][name]}' ������ �� ���������.\n";
				//XXX
			}
			else
			{
	        	// income in rubles
    	        $custom[manager_income] = $custom[manager_k_price_abonent] * $custom[price_abonent] + 
        	                              $custom[manager_k_price_rest] * $custom[a1_income] + 
            	                          $this->to_rur($custom[manager_currency], $custom[manager_b_price_fixed], $rates);
    	        $custom[manager_income_ex] = $this->from_rur($custom[manager_currency], $custom[manager_income], $rates);
	            // LOG
    	        $k1 = (double)($custom[manager_k_price_abonent] * 100);
        	    $k2 = (double)($custom[manager_k_price_rest] * 100);
		    	$log .= "�������� �1 [{$custom[manager_id]}][{$this->info[managers][$custom[manager_id]][name]}] ".
						"$k1% * $custom[price_abonent] ���. + ".
						"$k2% * $custom[a1_income] ���. + ".
						"$custom[manager_b_price_fixed] $custom[manager_currency] = ".
						"$custom[manager_income] ���.\n";
		    	// income in rubles
    	        $custom[leader_income] = $custom[leader_k_price_abonent] * $custom[price_abonent] + 
        	                              $custom[leader_k_price_rest] * $custom[a1_income] + 
            	                          $this->to_rur($custom[leader_currency], $custom[leader_b_price_fixed], $rates);
    	        $custom[leader_income_ex] = $this->from_rur($custom[leader_currency], $custom[leader_income], $rates);
	            // LOG
    	        $k1 = (double)($custom[leader_k_price_abonent] * 100);
        	    $k2 = (double)($custom[leader_k_price_rest] * 100);
				$log .= "������������ [{$custom[leader_id]}][{$this->info[managers][$custom[leader_id]][name]}] ".
						"$k1% * $custom[price_abonent] ���. + ".
						"$k2% * $custom[a1_income] ���. + ".
						"$custom[leader_b_price_fixed] $custom[leader_currency] = ".
						"$custom[leader_income] ���.\n";
			}
            //
            // ���� ��� ���������� �� ������� �����, ���������� ����������� � ������� ������.
            // �������� = 6 ������ ����� ������� (�� ���������).
            //
            $custom[manager_income] 	= $this->Normalize($custom[manager_income]);
            $custom[manager_income_ex] 	= $this->Normalize($custom[manager_income_ex]);
            $custom[leader_income] 		= $this->Normalize($custom[leader_income]);
            $custom[leader_income_ex] 	= $this->Normalize($custom[leader_income_ex]);
            
			if ($custom[a1_income]<0)
            {
				$log .= "������: ������������� ����� �1. � ��������� �1 [$manager_id] '{$managers[$manager_id][name]}'".
										" ����� {$custom[a1_income]} ���.\n";
				//$this->csendmail($title_log, $custom[comment]);
				// ����������� �������!
			}
			else // ����� �������!
			{
				$custom[a1_income] -= $custom[manager_income];
				$custom[a1_income] -= $custom[leader_income];
			}
			
			$custom[target_log] .= $log;
		}
	    else
		{
			if ($partner[vip]<1) $custom[target_log] .= "�������� �1 ��� ������������ �� �������.\n";
			else $custom[target_log] .= "��������� �1 � VIP ������� ������ �� �����������!\n";
		}
		
		return $custom;
	}

	/** \brief Customs handling.
	 *
	 *  ������������ �� ������������ ������ �� ������� cyka_payment.
	 *  ������ ����������� ������ �� ����������� ����������� ���������� (������� cpayment_oa).
	 *  �������� ������� ������ � ������� 'cyka_payment', �������� ���������� � ��������, 
	 *  ���������� �������� � ��������� �1, � ����� �������� ��������, ������������ ������� �1 
	 *  � �������� ������� �1.
	 *  ���� ���������� �����-���� ������, �� � ��� ���������� �������, �������� ������ � ���.
	 *
	 *  � ������� � ������������ ������ ����� � tr_billing ���������� ������� �� �������� ��-��������� � 
	 *  ���������� �������� (� ���������� ��������) ������ �� �����������, ��� � ��������� � �������������.
	 *  ��������� ��������� 
	 *  -------------------------------------------------------
	 *  ��� ���������� ������ �������� ��������� �������������:
	 *  -------------------------------------------------------
	 *  1. �������, ��������� � �������� �1 ����� ���� � ������������ ���������� ���
	 *     ������� ������. ���� ��� �� ���, ����� ��������� ������ ��� �����!
	 *  2. ����������� ��������� ����� ���� �����.
	 *  \return 1(ok) if all modification operation is success, otherwise 0(error).
	 */
	private function custom_handling($custom)
	{
		$info = $this->info;
		$rates = $this->getRates($custom[date]);	
		$operator = $inf[operators][$custom[operator_id]];

		$custom[comment] =  "-----------------------------------\n".
							"����� N $custom[inbox_id]\n".
							"-----------------------------------\n".
							"��� ����������: {$custom[transport_type]}\n".
							"����: {$custom[date]}\n".
							"������ [{$custom[service_id]}]: '{$info[services][$custom[service_id]][name]}'\n".
							"���������: [{$custom[subservice_id]}]: '{$info[subservices][$custom[subservice_id]][name]}'\n".
							"�������: {$custom[abonent]}\n".
							"�������� [{$custom[operator_id]}]: {$info[operators][$custom[operator_id]][name]}\n".
							"�����: {$custom[num]}\n".
							"������� [{$custom[partner_id]}]: {$info[partners][$custom[partner_id]][name]}\n".
							"��������� �������� [{$custom[partner_agent_id]}]: {$info[partner_agents][$custom[partner_agent_id]][name]}\n".
							"test_flag: '{$custom[test_flag]}'\n".
							"���������: '{$custom[msg]}'\n".
							"-----------------------------------\n";

		if (0 != $custom['is_ignore'] and 6 == $custom['transport_type'])
		{
			$custom[partner_id] = $info[default_partner][id];
			$custom[comment] .= "����� ������������. ����� � ������ ������� ����������.\n";
			$custom[comment] .= "����� �1 ����� ���� ����������: {$custom[a1_income]} ���.\n";
			$this->w($custom[comment]);
			$custom[cyka_processed] = 1;
			$this->csendmail("CYKA INFORMATION: CUSTOM HANDLING: ignore",$custom[comment].'\n\n'.print_r($custom,true));
			return $this->custom_updating($custom);
		}		

		////////////////////////////////////////////////////////////////////////
		// �������� ��������� �� ������� ����������. ��� �������� ����������, //
		// ����� ��������� ��������� ��������� ����� ������������� ������.    //
		////////////////////////////////////////////////////////////////////////
		// general
	 	$custom[price_abonent] = 0;
	 	$custom[price_rest] = 0;
	 	$custom[cyka_processed] = 1;
	 	$custom[is_error] = 0;
		$custom[a1_income_after_custom_handling] = 0;
		// partner information
	 	$custom[partner_k_price_abonent] = 0;
	 	$custom[partner_k_price_rest] = 0;
	 	$custom[partner_b_price_fixed] = 0;
	 	$custom[partner_income] = 0;
	 	$custom[partner_income_ex] = 0;
	 	$custom[partner_currency] = 'rur';
		// partner agent information
		$custom[partner_agent_id] = 0;
	 	$custom[partner_agent_k_price_abonent] = 0;
	 	$custom[partner_agent_k_price_rest] = 0;
	 	$custom[partner_agent_b_price_fixed] = 0;
	 	$custom[partner_agent_income] = 0;
	 	$custom[partner_agent_income_ex] = 0;
	 	$custom[partner_agent_currency] = 'rur';
		//manager information
		$custom[manager_id] = 0;
	 	$custom[manager_k_price_abonent] = 0;
	 	$custom[manager_k_price_rest] = 0;
	 	$custom[manager_b_price_fixed] = 0;
	 	$custom[manager_income] = 0;
	 	$custom[manager_income_ex] = 0;
	 	$custom[manager_currency] = 'rur';
		//leader information
		$custom[leader_id] = 0;
	 	$custom[leader_k_price_abonent] = 0;
	 	$custom[leader_k_price_rest] = 0;
	 	$custom[leader_b_price_fixed] = 0;
	 	$custom[leader_income] = 0;
	 	$custom[leader_income_ex] = 0;
	 	$custom[leader_currency] = 'rur';

		/////////////////////////////////////////////////////////////////
		// ���������� ���� ������ ��� �������� � ������������ ����� �1 //
		/////////////////////////////////////////////////////////////////
		$query="SELECT e.price * if (n.nds, n.price_in / 1.18, n.price_in) AS price_abonent,
					   e.price * if (n.nds, n.estimated_income / 1.18, n.estimated_income) AS price_rest 
				FROM set_num_price n 
				LEFT JOIN set_exchange_rate e ON e.currency=n.currency AND e.date=date('$custom[date]') 
				WHERE n.date_start<=date('$custom[date]') AND n.operator_id='$custom[operator_id]' AND n.num='$custom[num]' 
				ORDER BY n.date_start DESC LIMIT 1";
		$tmp = db::selectAll($query);
		if ($tmp)
		{
			foreach ($tmp as $cur)
			{
		 		$custom[price_abonent] = $cur[price_abonent];
				//
				// �������� = 6 ������ (�� ���������)
				// ���������� ����� � ���������� �������� �����-���� ������ ��� �����������!
				// ����� ���������� �������� � ������� �����, ��-�� ������� ��������� ������!
				//
				$cur[price_abonent]    = $this->Normalize($cur[price_abonent]);
				$cur[price_rest]       = $this->Normalize($cur[price_rest]);
				
				$custom[price_abonent] = $cur[price_abonent];
				$custom[price_rest]    = $cur[price_rest]; // ������������ ����� �1 � ������ (������ const)
			 	$custom[a1_income]     = $cur[price_rest]; // ����� A1, ����������� ��� �����������
				$custom[a1_income_after_custom_handling] = $cur[price_rest];
			}
		}
		else
		{
			$custom[comment] .= "WARNING: ���� ��� ��������� [{$custom[operator_id]}] ".
								"{$info[operators][$custom[operator_id]][name]} � ������ '{$custom[num]}' �� �����������!\n";
		}
		$custom[comment] .= "���� ��� ��������: $custom[price_abonent] ���.\n"; 
		$custom[comment] .= "����� �1: $custom[price_rest] ���.\n";
		if (
			(0  >  $custom[price_abonent]) || 
			('' == $custom[price_abonent]) ||
			('' == $custom[price_rest])    ||
			(0  >  $custom[price_rest])
		   )
		{
			//
			// ���� ����������� ������ ���� (��� �� ������ ������, ���,
			// ��������, ��� Simperia), �� �� �� ���������� ����� � �������.
			// � ������� 'cpayment' ���������� ��� ����� ��������� ���� � 
			// ������� (cyka_procesed=1 & is_error=1)
			//
			$custom[error_msg] = "ERROR: ����������� ������ ����!\n";
			$custom[comment] .= $custom[error_msg];
		 	$custom[is_error] = 1;
			$this->w($custom[comment]);
			return $this->custom_updating($custom);
		}
		//////////////////////
		// ��������� FRAUD� //
		//////////////////////
		if (0 != $custom['is_fraud'])
		{
			//$query = "SELECT is_fraud FROM tr_billing WHERE inbox_id='{$custom[inbox_id]}' and outbox_id=0 LIMIT 1";
			//$is_fraud = db::selectField($query, 'is_fraud');
			//if ($is_fraud)
			//{
				$custom[partner_id] = $info[default_partner][id];
				$custom[comment] .= "����� �������� ������. ���� ����� � ������ ���������� � ����� �1 (�������� �� ���������).\n";
				$custom[comment] .= "����� �1 ����� ���� ����������: {$custom[a1_income]} ���.\n";
				$this->w($custom[comment]);
				//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: ����.", $custom[comment], $this->sendOnlyAdmin);
				return $this->custom_updating($custom);
			//}
		}		
		///////////////////////////////////////////////////////
		// ���� ����������� ��������� � ��������� �� ������! //
		///////////////////////////////////////////////////////
		$operator_agents = $info[operator_agents_grupped][$custom[operator_id]];
		if ($operator_agents)
		{
			db::beginTransaction();
			$n = count($operator_agents);
			$custom[comment] .= "���������� ����������� ���������: $n\n";
			$order = 0;
			foreach($operator_agents as $id=>$agent)
			{
				$order++;
				$custom = $this->make_oa_payment($custom, $agent, $rates, $order);
				if ($custom[is_error])
				{
					//$custom[is_error] = 1;
					$custom[a1_income] = $custom[price_rest];

					db::rollbackTransaction();
					$this->w($custom[comment]);
					return $this->custom_updating($custom);
				}
			}
		}
		else
		{
			$custom[comment] .= "���������� ����������� ���������: 0\n";
		}
		///////////////////////////////////////////////////////////////////////////////
		// ���� ��������. ���� ������� ������, ����� ����� �������� ����� ���������� //
		// �������� � ��������� �1. ����, ��� ��� ������, ������������ ����������.   //
		// ���������, ��� �������, ��������� � �������� �1 ����� ���� � ������������ //
		// ���������� ��� ������� ������. ���� ��� �� ���, ����� ��������� ������    //
		// ��� �����!                                                                //
		///////////////////////////////////////////////////////////////////////////////
		$partner = $info[partners][$custom[partner_id]];
		if (!$partner)
		{
			// 
			// ���� �������� ��� � ������ ���������, ����� � ���� ������ �������� �����������
			// � ����������� �������� �� ���������!
			//
			$partner = $info[default_partner];
			$partner[vip] = 0;
			$custom[comment] .= "�������� '[{$custom[partner_id]}]' ��� � ������ ���������. ����������� �������� �� ���������.\n";
		}
		/////////////////////////////////////////////////////////////////////////////////
		// ���� ���������� ��������� ��� ��������, ���������� �������� � ��������� �1. //
		/////////////////////////////////////////////////////////////////////////////////
		//
		// ������� ���� ������ ��� ���� ����������, ���� �� �����, ����� ��� �����������!
		//
		do // ��������� ����!
		{
			$pluginc = $this->GetPlugin('1',$custom[num],$custom[service_id],$partner[id],'1');
			if (!$pluginc)
			{
				$pluginc = $this->GetPlugin($custom[operator_id],$custom[num],$custom[service_id],$partner[id],'1');
				if (!$pluginc) // Possible 4m service and 4m service with WAP-PUSH.
				{
					//
					// service_id = 30  <<<<< 4m - ����� �����-�������� �� ����.
					// service_id = 119 <<<<< 4m - ����� �����-�������� �� ���� � WAP-PUSH.
					//
					if ((30 == $custom[service_id]) || (119 == $custom[service_id]))
					{
						//
						// �������� content_id ��� ������� ������!
						// 
						// ���� content_id �� ������, ����� � �������� �������� ���������� '�������� �� ���������'
						// � ������ pluginc (���������� content_id ��������, ���� ������� ������ � �������� ��� 
						// �� �� ��� �����. ��� � ����� ������� wap-�������, �� �������� �� ����� ������� ������� 
						// �� ���� ������, �� ������� �� �������� ���. �������� ������ �� ����������� � ������ ������).
						// ������ �� �����������!
						//
						$query = "SELECT * FROM cyka_downloads_wap WHERE transaction_id={$custom[inbox_id]}";
						$cyka_content_info = db::selectRow($query);
						if (!$cyka_content_info)
						{
							//$custom[error_msg]= "������ ������ �������� �� ������ ������������� ����������� ��������.\n";
							$custom[comment] .= "������ ['{$custom[service_id]}'] '{$info[services][$custom[service_id]][name]}' ".
												"�� ������ ������������� ��������. ����������� �������� �� ��������� ������ ".
												"�������� � ���������� ��������� ������.\n";
							//$custom[is_error] = 1;
							//$this->w($custom[comment]);
							//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: {$custom[error_msg]}", $custom[comment]);
							//return $this->custom_updating($custom);
							$partner = $info[default_partner];
							$custom[partner_id] = $partner[id];
							$pluginc = null;
							break;
						}
						else
						{
							$custom[comment] .= "�������������� ����� �������� '{$cyka_content_info[content_id]}'".
												" �� ������� '{$cyka_content_info[project_id]}'.\n";
						}

						//
						// �������� ���� ��� ���ר�� ���������� ����������������!
						//
						$query_etalon = "SELECT content_id,owner_cyka_id as owner_id,owner_type,owner_alloc,owner_fix,owner_procent,lower(curr) as curr 
										 FROM media.cprices ";
						do // ��������� ����
						{
							if ($cyka_content_info[project_id])
							{
								//
								// ������� ����� ��������� ���������� ��� content_id, ������ � �������, ���� ������� ������ ��������.
								//
								$query = $query_etalon."WHERE content_id='{$cyka_content_info[content_id]}' and 
															  country_id='{$info[operators][$custom[operator_id]][country_id]}' and 
															  project_id='{$cyka_content_info[project_id]}'";
								$log =  "��� �������� '{$cyka_content_info[content_id]}', ".
										"������� LMJ '{$info[projects][$cyka_content_info[project_id]][name]}' � ".
										"������ '{$info[countries][$info[operators][$custom[operator_id]][country_id]][name]}' ";
								if ($content_prices = db::selectAll($query))
								{
									$custom[comment] .= $log."������� ��������� ����������.\n";
									break;
								}
								else $custom[comment].= $log."�� ������� ��������� ����������.\n";
							}
							//
							// ������� ����� ��������� ���������� ��� content_id, ������ � ������� �� ���������
							//
							$query = $query_etalon."WHERE content_id='{$cyka_content_info[content_id]}' and 
														  country_id='{$info[operators][$custom[operator_id]][country_id]}' and 
														  project_id='{$info[default_project][id]}'";
							$log =  "��� �������� '{$cyka_content_info[content_id]}', ".
									"������� LMJ '{$info[default_project][name]}' � ".
									"������ '{$info[countries][$info[operators][$custom[operator_id]][country_id]][name]}' ";
							if ($content_prices = db::selectAll($query))
							{
								$custom[comment] .= $log."������� ��������� ����������.\n";
								break;
							}
							else $custom[comment] .= $log."�� ������� ��������� ����������.\n";
							//
							// ������� ����� ��������� ���������� ��� content_id, ������ �� ��������� � ������� �� ���������
							//
							$query = $query_etalon."WHERE content_id='{$cyka_content_info[content_id]}' and 
														  country_id='{$info[default_country][id]}' and 
														  project_id='{$info[default_project][id]}'";
							$log =  "��� �������� '{$cyka_content_info[content_id]}', ".
									"������� LMJ '{$info[default_project][name]}' � ".
									"������ '{$info[default_country][name]}' ";
							if ($content_prices = db::selectAll($query))
							{
								$custom[comment] .= $log."������� ��������� ����������.\n";
								break;
							}
							else $custom[comment] .= $log."�� ������� ��������� ����������.\n";
						}
						while (0);

						if (!$content_prices)
				        {
							$custom[error_msg] = "������� ������ �������� ��� �������� ���������� (��������������� �� ��������).";
							$custom[comment]  .= "��� �������� � ��������������� {$cyka_content_info[content_id]} �� ������� ��������� ����������.\n";
							$this->w($custom[comment]);
							//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: {$custom[error_msg]}",$custom[comment]);
							$this->log("CYKA ERROR: CUSTOM HANDLING: {$custom[error_msg]}\n".$custom[comment]);
						 	$custom[is_error] = 1;
							return $this->custom_updating($custom);
						}

						//
						// ��������� ���������� ����������������.
						//
						$legal_owners_income = 0;
						//
						// � ������, ���� ��������������� ��� ����� ������������� ��� ������ ������� 
						// (�������� �� ������� ����������� � �������� �������), �� ��������� ����������,
						// ������ � ��� �����������, ��������� ���� ����� ����������!
						//
						foreach ($content_prices as $content_owner_info)
						{
							//
							// �������� ����� �������������� ������������ ��� ��������� �� ���� ��� ��������.
							// (�������� ��������!)
							//
						 	$lo_income = $content_owner_info[owner_alloc] * $custom[price_abonent] / 10000;
							$lo_fix = $this->to_rur($content_owner_info[curr], $content_owner_info[owner_fix] / 1000, $rates);
							$lo_fix_used = 0;
							if ($lo_income < $lo_fix)
							{
								$lo_fix_used = 1;
								$lo_income = $lo_fix;
							}

							// ��������� ������� ��������!
							// ����� �� 10000 � ����������� � �������������� ������ � lmj
							$lo_income *= $content_owner_info[owner_procent] / 10000;
							$legal_owners_income += $lo_income;
					
							// LOG
							$k1 = (int)($content_owner_info[owner_alloc] / 100);
							$k2 = 0;
							$custom[comment] .= "���������������: [{$content_owner_info[owner_id]}]".
												"[{$info[legal_owners][$content_owner_info[owner_id]][name]}] ".
												"$k1% * $custom[price_abonent] ���. ";
							if ($lo_fix_used) $custom[comment] .= "< ";
							else $custom[comment] .= "> ";
							$custom[comment] .= ($content_owner_info[owner_fix] / 1000)." ".$content_owner_info[curr]." => ";

							if ($lo_fix_used) $custom[comment] .= ($content_owner_info[owner_fix] / 1000)." ".$content_owner_info[curr]." ";
							else $custom[comment] .= "$k1% * $custom[price_abonent] ";
							$custom[comment] .= "* ".($content_owner_info[owner_procent] / 10000)." % = {$lo_income} ���.\n";
						}

						//
						// ���������� ����� ���������������� � % � �� ���������� ������ � ������� �������.
						//
						if ($custom[price_abonent]) $legal_owners_procent = ceil( ($legal_owners_income * 100 ) / $custom[price_abonent] );
						else $legal_owners_procent = 0;
						$custom[comment] .= "������� ���������� ���������������� �� ������ �1 �������� $legal_owners_procent.\n";
					
						//
						// ���� ������ ���������� plugin, ��� ������� �������� ���������� ����������������!
						//
						$plugins = (array)$this->GetPlugin($custom[operator_id], $custom[num], $custom[service_id], $partner[id]) 
								 + (array)$this->GetPlugin('1', $custom[num], $custom[service_id], $partner[id]);
						if ($plugins)
						{
							foreach ($plugins as $procent_category_id=>$plugin)
							{
								$procent_category = $info[procent_categories][$procent_category_id];
								if ('' == $procent_category)
								{
									$custom[error_msg] = "����������� ���������� ��������� � ��������������� '$procent_category_id'.\n";
									$custom[comment] .= $custom[error_msg];
									$custom[is_error]=1;
									$this->w($custom[comment]);
									return $this->custom_update($custom);
								}
								if (($procent_category[procent_min] <= $legal_owners_procent) and 
									($procent_category[procent_max] >= $legal_owners_procent))
								{
									$pluginc = $plugin;
									$custom[comment] .= "������ ���������� plugin [{$pluginc[id]}] � procent_category = '$procent_category[name]'\n";
								}
							}
						}
						else $pluginc = null;
					
						if (!$pluginc)
						{
							//$custom[error_msg] = "�� ������ plugin ��� �������.";
							$custom[comment] .= "�� ������ plugin ��� ������� [{$custom[service_id]}}] '{$info[services][$custom[service_id]][name]}'";
							//$custom[is_error] = 1;
							//$this->w($custom[comment]);
							//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: {$custom[error_msg]}", $custom[comment]);
							//return $this->custom_updating($custom);
						}
					} // END OF: if ((30 == $custom[service_id]) || (119 == $custom[service_id]))
				}
				else
				{
					$custom[comment] .= "������ plugin N{$pluginc[id]} � ����������� ��� �������� ��������� � procent_category N1 [0% - 100%].\n";
				}
			}
			else
			{
				$custom[comment] .= "������ plugin N{$pluginc[id]} � ����������� ��� ���� ���������� � procent_category N1 [0% - 100%].\n";
			}
		}
		while (0);

		$custom[procent_category] = $pluginc[procent_category];
		
		if ($pluginc)
		{
			$custom[partner_k_price_abonent] 		= 0;//$pluginc[k_price_abonent_p];
	 		$custom[partner_k_price_rest] 			= 0;//$pluginc[k_price_rest_p];
		 	$custom[partner_b_price_fixed] 			= $pluginc[b_price_fixed_p];
		 	$custom[partner_currency]				= $pluginc[currency];
		
			$custom[partner_agent_id]				= $info[partners][$custom[partner_id]][partner_agent_id];
		 	$custom[partner_agent_k_price_abonent] 	= $pluginc[k_price_abonent_pp];
	 		$custom[partner_agent_k_price_rest] 	= $pluginc[k_price_rest_pp];
	 		$custom[partner_agent_b_price_fixed] 	= $pluginc[b_price_fixed_pp];
			$custom[partner_agent_currency]			= $info[partner_agents][$custom[partner_agent_id]][currency];//$pluginc[currency];
		}
		else
		{
	 		$custom[partner_k_price_abonent] 		= 0;
		 	$custom[partner_k_price_rest] 			= 0;
		 	$custom[partner_b_price_fixed] 			= 0;
		 	$custom[partner_currency]				= $info[partners][$custom[partner_id]][currency];
		
			$custom[partner_agent_id]				= $info[partners][$custom[partner_id]][partner_agent_id];
	 		$custom[partner_agent_k_price_abonent] 	= 0;
		 	$custom[partner_agent_k_price_rest] 	= 0;
	 		$custom[partner_agent_b_price_fixed] 	= 0;
			$custom[partner_agent_currency]			= $info[partner_agents][$custom[partner_agent_id]][currency];//$pluginc[currency];
		}

		$custom[manager_id] = $info[partners][$custom[partner_id]][manager_id];
		$service = $info[services][$custom[service_id]];
		$manager = $info[managers][$custom[manager_id]];

		$custom[manager_k_price_abonent] = 0;
		$custom[manager_k_price_rest]	 = ($manager[is_def]) ? 0 : $service[manager_procent]/100.0;
		$custom[manager_b_price_fixed]	 = 0;
		$custom[manager_currency]		 = 'rur';

		$custom[leader_id] = $manager[main_manager_id];
		$leader = $info[managers][$manager[main_manager_id]];
		

		$custom[leader_k_price_abonent] = 0;
		$custom[leader_k_price_rest]	 = ($leader[is_def]) ? 0 : $service[superviser_procent]/100.0;
		$custom[leader_b_price_fixed]	 = 0;
		$custom[leader_currency]		 = 'rur';
		
		
		#$this->w(var_export($service,true));
		#$this->w(var_export($custom,true));
	
		//////////////////////////////////////
		// ��������� �������� �� ������ �1! //
		//////////////////////////////////////
		// income in rubles
	 	$custom[partner_income] = $custom[partner_k_price_abonent] * $custom[price_abonent] + 
								  $custom[partner_k_price_rest] * $custom[price_rest] + 
								  $this->to_rur($custom[partner_currency], $custom[partner_b_price_fixed], $rates);
		// income to concrete currency
	 	$custom[partner_income_ex] = $this->from_rur($custom[partner_currency], $custom[partner_income], $rates);
		// LOG
		$k1 = (int)($custom[partner_k_price_abonent] * 100);
		$k2 = (int)($custom[partner_k_price_rest] * 100);
		$custom[comment] .= "�������: [{$partner[id]}][{$partner[name]}]  ".
							"$k1% * $custom[price_abonent] ���. + ".
							"$k2% * $custom[price_rest] ���. + ".
							"$custom[partner_b_price_fixed] $custom[partner_currency] = ".
							"$custom[partner_income] ���.\n";
		//
		// ���� ��� ���������� �� ������� ����� � ������ �� �������� VIP, ����� � ����������� 
		// ����������� ������ ����� ����� �� ����� ��������� �1.
		// �������� �������� = 6 ������ ����� ������� (�� ���������).
		//
		$custom[partner_income]    = $this->Normalize($custom[partner_income]);
		$custom[partner_income_ex] = $this->Normalize($custom[partner_income_ex]);
		if (($custom[partner_income] > $custom[a1_income]) && ($custom[partner_income] != 0) && ($partner[vip]<1))
		{
			$custom[comment] .= "������: $custom[partner_income] ���. - ������� ������� �����!\n";
		}	
		$custom[a1_income] -= $custom[partner_income];
		
		/////////////////////////////////////////////////
		// ��������� ���������� ��������.              //
	   	// ����� ���������� �������� ������� �� ������ //
		// ��������, �� ���������� ��� ������������ �� //
		// ������ �1!                                  //
		/////////////////////////////////////////////////
		// income in rubles
        if ($custom[partner_agent_id])
        {
            $custom[partner_agent_income] = $custom[partner_agent_k_price_abonent] * $custom[price_abonent] + 
                                            $custom[partner_agent_k_price_rest] * $custom[partner_income] + 
                                            $this->to_rur($custom[partner_agent_currency], $custom[partner_agent_b_price_fixed], $rates);
            // income to concrete currency
            $custom[partner_agent_income_ex] = $this->from_rur($custom[partner_agent_currency], $custom[partner_agent_income], $rates);
            // LOG
            $k1 = (int)($custom[partner_agent_k_price_abonent] * 100);
            $k2 = (int)($custom[partner_agent_k_price_rest] * 100);
		    $custom[comment] .= "��������� �������� [{$custom[partner_agent_id]}][{$info[partner_agents][$custom[partner_agent_id]][name]}]  ".
								"$k1% * $custom[price_abonent] ���. + ".
								"$k2% * $custom[partner_income] ���. + ".
								"$custom[partner_agent_b_price_fixed] $custom[partner_agent_currency] = ".
								"$custom[partner_agent_income] ���.\n";
            //
            // ���� ��� ���������� �� ������� �����, ����� ���� (����� �1) ����������� �������
            // ���������� � ���������� ����������� � ���, ��� �������� ����������� �������.
            // ������ � ���������� ������� ��������� � �������!
            // �������� = 6 ������ ����� ������� (�� ���������).
            //
            $custom[partner_agent_income]    = $this->Normalize($custom[partner_agent_income]);
            $custom[partner_agent_income_ex] = $this->Normalize($custom[partner_agent_income_ex]);
            if (($custom[partner_agent_income] > $custom[a1_income]) && ($custom[partner_agent_income]!=0) && ($partner[vip]<1))
            {
				$custom[comment] .= "����� ���������� �������� $custom[partner_agent_income] ���. �������� ��������� ������.\n";
            }	
       	    $custom[a1_income] -= $custom[partner_agent_income];
		}
		else
		{
			$custom[comment] .= "��������� �������� �� ������.\n";
		}

		//////////////////////////////////////////////////////
		// ��������� ����� �1 ����� ���� ����������, �� ��  //
		// ��������� ���� ���������.                        //
		//////////////////////////////////////////////////////
		$custom[a1_income_after_custom_handling] = $custom[a1_income];
		
		//////////////////////////////////////////////////////
		// ��������� ��������� �1 �� ������ �1, ����������� //
		// ����� ���������� ����������� ���������, �������� //
		// � ����������	��������.���� ��������, ��� ���� �  //
		// ���������� �� ����� ������, ����� �����������    //
		// ����� ��������� �������.                         //
		// ���� �� ����� ������, ����� ��� ����� �����      //
		// �����������, ���� ����� �1 ��������� (�.�.       //
		// ���� ������ �������� ��������, � �� �����������) //
		// � ������, ���� ����� �1 ������������� ��� ������ //
		// �������������, ����� ��� �������� ��������� �    //
		// ���������.                                       //
		//////////////////////////////////////////////////////
		$custom[target_log] = null;
		$custom = $this->fill_managers_payments($custom, $partner, $service, $rates);
		$custom[comment] .= $custom[target_log];
		$custom[comment] .= "����� �1 ����� ���� ����������: {$custom[a1_income]} ���.\n";
		
		//////////////////////////////////////
		// ���� � ����� �� �� vip ��������? //
		//////////////////////////////////////
		if (0 == $info[partners][$custom[partner_id]][vip])
		{
			if ($custom[a1_income] < 0)
			{
				//$this->csendmail("CYKA ERROR: CUSTOM HANDLING: ������������� ����� �1 ����� ���������� �� ������.", 
				//				 $custom[comment], 
				//				 false, 
				//				 $custom[manager_id]
				//				);
				$this->log("CYKA ERROR: CUSTOM HANDLING: ������������� ����� �1 ����� ���������� �� ������.\n".$custom[comment]);
			}
		}
		
		//
	   	// ���� ����������� ��������� ������, ����� ��������� � ���� ���� 
		// ������ � ���������� �� ���� ������, � ���������, �������������� 
		// � ������ $ret = $this->custom_updating() ������ � 
		// ������� �������������� (��� �� ������, ����� ����������).
		//
		$this->w($custom[comment]);
		$ret = $this->custom_updating($custom);
		if ($operator_agents)
		{
			if ($ret) db::commitTransaction();
			else      db::rollbackTransaction();
		}
		return $ret;
	}

	/** \brief The download handling.
	 *
	 *  \param download - Array of parameters. Must be following parameters:
	 *                      - id - Download id inside table 'downloads'
	 *                      - date - Download date!
	 *                      - transaction_id
	 *                      - content_id
	 *                      - content_name
	 *                      - content_type_id
	 *                      - price_category
	 *                      - content_owner
	 *  \return The 1(true) if all success, otherwise 0(false).
	 */
	private function download_handling($download)
	{
		$info = $this->info;
		// LOG
		$download[comment] = "-------------------------\n".
							 "DOWNLOAD N $download[id]\n".
							 "-------------------------\n".
							 "date: {$download[date]}\n".
							 "transaction_id: {$download[transaction_id]}\n".
							 "transport_type: {$download[transport_type]}\n".
							 "content_id: {$download[content_id]}\n\n";
		/////////////////////////////////////////////////////////
		// ���������� �������, ��� ���������� ����� � �������, //
		// � � ����� �������� ����������, ��� ��� ������.      //
		// ������� ��� ����, ��� ���� ��������� ����� ����,    //
		// �� �� ����� ������������. �� ���� ����� �������     //
		// �������������� 1 ���.                               //
		/////////////////////////////////////////////////////////
		//$query = "UPDATE cyka_downloads SET cyka_processed=1,is_error=1 WHERE id='{$download[id]}'";
		//if (!db::query($query))
		//{
		//	$this->w("ERROR QUERY: ".$query,true);
		//	$this->w("ERROR MYSQL: ".mysql_error(),true);
		//	return 0;
		//}

		//////////////////////////////////////////////////////////////////////
		// ������������ ������� ���������� ����� � ��������� ��� ���������. //
		//////////////////////////////////////////////////////////////////////
		$custom = db::selectRow("SELECT * FROM cyka_payment WHERE inbox_id='$download[transaction_id]' and transport_type='$download[transport_type]'");
		if (!$custom)
		{
			$download[cyka_processed] = 1;
			$download[is_error]  = 1;
			$download[error_msg] = "����� ��� ������� �� ������.\n";
			$download[comment]  .= $download[error_msg];
			//$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}", $download[comment]);
			$this->log("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}\n".$download[comment]);
			return $this->download_updating($download); 
		}
		//
		// ���� ����� ��������� � �������, ����� ��������� ����������� � ����� � �������!
		//
		if ($custom[is_error]!=0)
		{
			$download[cyka_processed] = 1;
			$download[is_error]  = 1;
			$download[error_msg] = "����� ��� ������� ��������� � �������\n";
			$download[comment] .= $download[error_msg].$custom[comment]."\n-----------------------------------\n";
			$download[comment] .= "���� ��� ��������: '{$custom[price_abonent]}'\n".
								  "����� �1 ��� ����: '{$custom[price_rest]}'\n".
								  "�������� ����� �1 �� ���������� ���������� � ������: '{$custom[a1_income_after_custom_handling]}'\n".
								  "������������� ����� �1: '{$custom[a1_income]}'\n";
			//$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}.", $download[comment]);
			$this->log("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}.\n".$download[comment]);
			return $this->download_updating($download); 
		}
		$download[comment] .= "���� ��� ��������: '{$custom[price_abonent]}'\n".
							  "����� �1 ��� ����: '{$custom[price_rest]}'\n".
							  "�������� ����� �1 �� ���������� ���������� � ������: '{$custom[a1_income_after_custom_handling]}'\n".
							  "������������� ����� �1 ����� ���������� ���������� � ������ ���������������� (���� ����): '{$custom[a1_income]}'\n";

		////////////////////////////////////////////////////////////////////////
		// ���������� ���� ������ �� ���� ������!                             //
		// ��� ���������� ��������������� ����� ������������� �� ����� �����. //
		////////////////////////////////////////////////////////////////////////
		#$rates = db::selectAll2("select * from set_exchange_rate where date(date)=date('$custom[date]')",'currency');
		$rates = $this->getRates($custom[date]);	
		if (!$rates)
		{
			$download[cyka_processed] = 1;
			$download[is_error] = 1;
			$download[error_msg] = "���c� ����� �� '{$custom[date]}' �� �������!.\n";
			$download[comment] .= $download[error_msg];
			$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}.", $download[comment]);
			return $this->download_updating($download); 
		}

		/////////////////////////////////////////////
		// ���� � ��������� ���������� � ��������  //
		// ���������� �� ����������� � ������      //
		// �������� �������, ��� ��� ��� ��������� //
		// �����������                             //
		/////////////////////////////////////////////
		$query = "select * from media.content_info where content_id='{$download[content_id]}'";
		$content_info = db::selectRow($query);
		if (!$content_info)
		{
			$download[cyka_processed] = 1;
			$download[is_error] = 1;
			$download[error_msg] = "��� �������� � ��������������� '{$download[content_id]}' �� ������� �������������� ����������.\n";
			$download[comment] .= $download[error_msg];
			$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: ����������� �������.", $download[comment]);
			return $this->download_updating($download); 
		}
		
		////////////////////////////////////////////////////////////
		// �������� ���� ��� ���ר�� ���������� ����������������! //
		////////////////////////////////////////////////////////////
		$query_etalon = "SELECT content_id,owner_cyka_id as owner_id,owner_type,owner_alloc,owner_fix,owner_procent,lower(curr) as curr 
						 FROM media.cprices ";
		do // ��������� ����
		{
			if ($download[project_id])
			{
				//
				// ������� ����� ��������� ���������� ��� content_id, ������ � �������, ���� ������� ������ ��������.
				//
				$query = $query_etalon."WHERE content_id='{$download[content_id]}' and 
											  country_id='{$info[operators][$custom[operator_id]][country_id]}' and 
											  project_id='{$download[project_id]}'";
				$log =  "��� �������� '{$download[content_id]}', ".
						"������� LMJ '{$info[projects][$cyka_content_info[project_id]][name]}' � ".
						"������ '{$info[countries][$info[operators][$custom[operator_id]][country_id]][name]}' ";
				
				$query = $query_etalon."WHERE content_id='{$download[content_id]}' and 
											  country_id='{$info[operators][$custom[operator_id]][country_id]}' and 
											  project_id='{$download[project_id]}'";
				if ($content_prices = db::selectAll($query))
				{
					$download[comment] .= $log."������� ��������� ����������.\n";
					break;
				}
				else $download[comment] .= $log."�� ������� ��������� ����������.\n";
			}
			//
			// ������� ����� ��������� ���������� ��� content_id, ������ � ������� �� ���������
			//
			$query = $query_etalon."WHERE content_id='{$download[content_id]}' and 
										  country_id='{$info[operators][$custom[operator_id]][country_id]}' and 
										  project_id='{$info[default_project][id]}'";
			$log =  "��� �������� '{$download[content_id]}', ".
					"������� LMJ '{$info[default_project][name]}' � ".
					"������ '{$info[countries][$info[operators][$custom[operator_id]][country_id]][name]}' ";
			if ($content_prices = db::selectAll($query))
			{
				$download[comment] .= $log."������� ��������� ����������.\n";
				break;
			}
			else $download[comment] .= $log."�� ������� ��������� ����������.\n";
			
			//
			// ������� ����� ��������� ���������� ��� content_id, ������ �� ��������� � ������� �� ���������
			//
			$query = $query_etalon."WHERE content_id='{$download[content_id]}' and 
										  country_id='{$info[default_country][id]}' and 
										  project_id='{$info[default_project][id]}'";
			$log =  "��� �������� '{$download[content_id]}', ".
					"������� LMJ '{$info[default_project][name]}' � ".
					"������ '{$info[default_country][name]}' ";
			if ($content_prices = db::selectAll($query))
			{
				$download[comment] .= $log."������� ��������� ����������.\n";
				break;
			}
			else $download[comment] .= $log."�� ������� ��������� ����������.\n";
		}	
		while (0);

		if (!$content_prices)
        {
			$download[cyka_processed] = 1;
			$download[is_error] = 1;
			$download[error_msg] = "��� �������� � ��������������� {$download[content_id]} �� ������� ��������� ���������� ����������������.\n";
			$download[comment] .= $download[error_msg];
			$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: ������� ������������ ���������������.", $download[comment]);
			return $this->download_updating($download);
		}
		
		$download[content_name]			 = $content_info[content_name];
		$download[content_category] 	 = $content_info[category_name];
		$download[content_cost_category] = $content_info[cost_category];
		$download[content_title]		 = $content_info[content_title];
		$download[content_singer]		 = $content_info[singer];
		$download[content_author_words]  = $content_info[author_words];
		$download[content_author_music]  = $content_info[author_music];
		$download[content_owner_code]	 = $content_info[owner_code];

		// 1. ���� a1_income_after_custom_handling �������� ����� �1 ����� ���� 
		//    ���������� ����������� ���������, �������� � ���������� ��������, ��
		//    �� ��������� ���� ���������!
		// 2. ������������ ������� ���������������� � ������������ ������� ���������� 
		//    ��������������� (������ �� ��������).
		// 4. ���������� ����� ����� ��������� � ������. ��������� ����� �1.
		// 5. ������� ���������� � ������ ��������������� � ���� ��������� ������ � 
		//    ������� cpayment_lo. ���������, ��� � ��� ��� ������� � ������� �������.
		//    ���� ��� �� ��� (���������� �������), �� ������������ �� �������� ������ 
		//    ������� ��������� ����, ��� ����� ����������� �������. � ��������� �����
		//    �� �������� ������, ��� ���������� ����� ����������, ��� ��������� �������
		//    (����� ���� ��������� ������� ��� ������ ������)
		
		/////////////
		// [��� 1] //
		/////////////
		$custom[a1_income] 			= $custom[a1_income_after_custom_handling];
		$custom[manager_income] 	= 0;
		$custom[manager_income_ex] 	= 0;
		$custom[leader_income] 		= 0;
		$custom[leader_income_ex] 	= 0;

		//
		// ���� ������� ��� �������� ������.
		//
		$query = "SELECT sum(legal_owner_income) 
				  FROM cyka_payment_lo 
				  WHERE inbox_id={$custom[inbox_id]} and transport_type={$custom[transport_type]} and download_id!={$download[id]}";
		$lo_sum = db::selectField($query);
		if ('' == $lo_sum) $lo_sum = 0;

		$custom[a1_income] -= $lo_sum;

		/////////////
		// [��� 2] //
		/////////////
		db::beginTransaction();
		
		//
		// ������� ������ ������
		//
		$query="DELETE FROM cyka_payment_lo WHERE download_id='{$download[id]}'";
		if (!db::query($query))
		{
			db::rollbackTransaction();
			return 0;
		}
		//
		// If other downloads for current custom exist then fix a1_income (without current download).
		//
		$query = "SELECT sum(legal_owner_income + legal_owner_agent_income) AS summ  
				 FROM cyka_payment_lo 
				 WHERE download_id<>'{$download[id]}' AND inbox_id='{$download[transaction_id]}' and transport_type='{$download[transport_type]}'";
		$lo_payments_from_other_downloads = db::selectField($query, 'summ');
		if ($lo_payments_from_other_downloads)
		{
			$custom[a1_income] -= $lo_payments_from_other_downloads;
		}
		//
		// ��������� �� ������ ���������������� � ���������� ����������
		//
		$order_number = 0;
		$lo_count = count($content_prices);
		$download[comment] .= "���������� ����������������: '{$lo_count}'.\n";
		foreach ($content_prices as $content_price)
		{
			////////////////////////////////////////////
			// ��������� ���������� � ��������������� //
			////////////////////////////////////////////
			$legal_owner = $info[legal_owners][$content_price[owner_id]];
			if (!$legal_owner)
			{
				$download[error_msg] = "��������������� [{$content_price[owner_id]}] �� ������.\n";
				$download[comment]   .= $download[error_msg];
				db::rollbackTransaction();
				$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: {$download[error_msg]}", $download[comment]);
				return $this->download_updating($download);
			}
			$order_number++;
			
			$lo_payment[inbox_id] = $download[transaction_id];
			$lo_payment[download_id] = $download[id];
			$lo_payment[transport_type] = $download[transport_type];
			$lo_payment[order_number] = $order_number;
			$lo_payment[date] = $download[date];
			$lo_payment[legal_owner_id] = $content_price[owner_id];
			$lo_payment[legal_owner_k_price_abonent] = $content_price[owner_alloc]/10000.0; // ����� �� 10000 � ����������� � �������������� ������ � lmj
			$lo_payment[legal_owner_k_price_rest] = 0;
			$lo_payment[legal_owner_b_price_fixed] = $content_price[owner_fix]/1000.0; // ����� �� 1000 � ����������� � �������������� ������ � lmj
			$lo_payment[legal_owner_income] = 0;
			$lo_payment[legal_owner_income_ex] = 0;
			$lo_payment[legal_owner_currency] = $content_price[curr];
			$lo_payment[legal_owner_agent_id] = 0;
			$lo_payment[legal_owner_agent_k_price_abonent] = 0;
			$lo_payment[legal_owner_agent_k_price_rest] = 0;
			$lo_payment[legal_owner_agent_b_price_fixed] = 0;
			$lo_payment[legal_owner_agent_income] = 0;
			$lo_payment[legal_owner_agent_income_ex] = 0;
			$lo_payment[legal_owner_agent_currency] = 'rur';

			//
			// �������� ����� �������������� ������������ ��� ��������� �� ���� ��� ��������.
			// (�������� ��������!)
			//
		 	$lo_payment[legal_owner_income] = $lo_payment[legal_owner_k_price_abonent] * $custom[price_abonent];
			$lo_fix = $this->to_rur($lo_payment[legal_owner_currency], $lo_payment[legal_owner_b_price_fixed], $rates);
			$lo_fix_used = 0;
			if ($lo_payment[legal_owner_income] < $lo_fix)
			{
				$lo_fix_used = 1;
				$lo_payment[legal_owner_income] = $lo_fix;
			}
			// ��������� ������� ��������!
			// ����� �� 10000 � ����������� � �������������� ������ � lmj
			$lo_payment[legal_owner_income] *= $content_price[owner_procent] / 10000;
		 	$lo_payment[legal_owner_income_ex] = $this->from_rur($lo_payment[legal_owner_currency], 
																 $lo_payment[legal_owner_income], 
																 $rates);
			//
			// LOG
			//
			$k1 = (int)($lo_payment[legal_owner_k_price_abonent] * 100);
			$k2 = (int)($lo_payment[legal_owner_k_price_rest] * 100);
			$download[comment] .= "���������������: [{$legal_owner[id]}][{$legal_owner[name]}]  $k1% * $custom[price_abonent] ���. ";
			if ($lo_fix_used) $download[comment] .= "< ";
			else $download[comment] .= "> ";
			$download[comment] .= "$lo_payment[legal_owner_b_price_fixed] $lo_payment[legal_owner_currency] => ";
            if ($lo_fix_used) $download[comment] .= "$lo_payment[legal_owner_b_price_fixed] $lo_payment[legal_owner_currency] ";
			else $download[comment] .= "$k1% * $custom[price_abonent] ";
			$download[comment] .= "* {$content_price[owner_procent]}% = {$lo_payment[legal_owner_income]} ���.\n";
			
			//
			// ���� ��� ���������� �� ������� �����, ����� ��� ������ �������� 
			// � ���������, ���� ������� ������ �� VIP.
			// �������� = 6 ������ ����� ������� (�� ���������).
			//
			$lo_payment[legal_owner_income]    = $this->Normalize($lo_payment[legal_owner_income]);
			$lo_payment[legal_owner_income_ex] = $this->Normalize($lo_payment[legal_owner_income_ex]);
			if ($lo_payment[legal_owner_income] > $custom[a1_income])
			{
				$download[comment] .= "������: $lo_payment[legal_owner_income] ���. - ������� ������� �����!\n";
			}	
			$custom[a1_income] -= $lo_payment[legal_owner_income];
			
			//
			// INSERT INTO cpayment_lo
			//
			if (0 == $this->create_cpayment_lo_record($lo_payment))
			{
				db::rollbackTransaction();
				return 0;
			}
		}
	
		/////////////
		// [��� 4] //
		/////////////
		//////////////////////////////////////////////////////
		// ��������� ��������� �1 �� ������ �1, ����������� //
		// ����� ���������� ����������� ���������, ��������,//
		// ���������� ��������, ���������������� �          //
		// ����������� ����������������.                    //
		// � ������, ���� ����� �1 �������������, ����� ��� //
		// �������� ��������� � ���������.                  //
		//////////////////////////////////////////////////////
		$custom[target_log] = null;
		$custom = $this->fill_managers_payments($custom,
												$info[partners][$custom[partner_id]], 
												$info[services][$custom[service_id]], 
												$rates);
		$download[comment] .= $custom[target_log];
		$download[comment] .= "����� �1: '{$custom[a1_income]}' ���.\n";

		//////////////////////////////////////
		// ���� � ����� �� �� vip ��������? //
		//////////////////////////////////////
		if ($info[partners][$custom[partner_id]][vip] = 0)
		{
			if ($custom[a1_income] <= 0)
			{
				$this->dsendmail("CYKA ERROR: DOWNLOAD HANDLING: ������� ��� ������������� ����� �1 ����� ���������� �� �������.", $download[comment]);
			}
		}
		
		// LOG
		//$this->w($download[comment], true);
	
		// UPDATE cpayment
		if (!($this->custom_updating($custom)))
		{
			$this->w("CUSTOM IS SAVING ERROR ERROR.", true);
			db::rollbackTransaction();
			return 0;
		}
        
		$download[cyka_processed] = 1;
		$download[is_error] = 0;
		if (!($this->download_updating($download)))
		{
			$this->w("DOWNLOAD IS SAVING ERROR.", true);
			db::rollbackTransaction();
			return 0;
		}
		db::commitTransaction();
		return 1;
	}

	//! \brief Main CYKA process.
	public function run()
	{
		$this->log("STARTING...",false);
		$this->log("STARTING...",true);
		$this->load_info();
		$this->processing_customs   = 10000; // ���������� �������������� �� ��� �������.
		$this->processing_downloads = 1000; // ���������� �������������� �� ��� �������.

		$start_time_ = $this->getmicrotime();
		while (1)
		{
			$executing_time_ = $this->getmicrotime() - $start_time_;
			if ($executing_time_ >= 300) // 5 minutes
			{
				$this->errorExit("CYKA DAEMON RESTART (EVERY 5 MINUTES)");
			}
		
			$go_to_sleep = 0;

			//////////////////////
			// Customs handling //
			//////////////////////
			$query = "SELECT * FROM cyka_payment_not_processed LIMIT {$this->processing_customs}";
			$this->records = array();
			$start_timestamp = $this->getmicrotime();
			$this->records = db::selectAll($query);
			$execute_time = $this->getmicrotime() - $start_timestamp;
			$count = count($this->records);
			if ($count) $this->log("����� ������� ���������� � $count �������������� ������� ����� $execute_time.");
			if ($this->records)
			{
				foreach($this->records as $rec)
				{
					$start_timestamp = $this->getmicrotime();
					if (!$this->custom_handling($rec))
					{
						$this->log("CYKA CUSTOM TERMINATED!");
						$this->log("CYKA CUSTOM TERMINATED!",true);
						$this->csendmail("CYKA CUSTOM TERMINATED!", var_export($rec, true)."\n".mysql_error(),$this->sendOnlyAdmin);
						sleep(5);
						exit;
					}
					$execute_time = $this->getmicrotime() - $start_timestamp;
					$this->log("����� ��������� ������ {$rec[inbox_id]} ����� $execute_time.");
					$rec = array();
				}	
			}	
			else
			{
				$go_to_sleep = 1;
			}
			////////////////////////
			// Downloads handling //
			////////////////////////
			if ($count < $this->processing_customs)
			{
				$query="SELECT * 
						FROM cyka_downloads 
						WHERE cyka_processed=0 and date<date_sub(now(), interval 5 minute)  
						ORDER BY date 
						LIMIT {$this->processing_downloads}";
				$this->records = array();
				$start_timestamp = $this->getmicrotime();
				$this->records = db::selectAll($query);
				$execute_time = $this->getmicrotime() - $start_timestamp;
				$count = count($this->records);
				if ($count) $this->log("����� ������� ���������� � $count �������������� �������� ����� $execute_time.",true);
			}
			else
			{
				$this->log("������� ���������� �������. ���������������� ��������� �������!",true);
				$this->records = array();
			}
			if ($this->records)
			{
				foreach($this->records as $rec)
				{
					$start_timestamp = $this->getmicrotime();
					if (!$this->download_handling($rec))
					{
						$this->log("CYKA DOWNLOAD TERMINATED!");
						$this->log("CYKA DOWNLOAD TERMINATED!",true);
						$this->csendmail("CYKA DOWNLOAD TERMINATED!", var_export($rec, true)."\n".mysql_error(),$this->sendOnlyAdmin);
						sleep(5);
						exit;
					}
					$execute_time = $this->getmicrotime() - $start_timestamp;
					$this->log("����� ��������� ������� {$rec[inbox_id]} ����� $execute_time.",true);
					$rec = array();
				}	
			}	
			else
			{
				if ($go_to_sleep)
				{			
					$this->log("WAITING 1 sec ...");
					$this->log("WAITING 1 sec ...", true);
					sleep(1);	
				}
			}

			// GO TO NEXT CIRCLE
		}
	}	
};	

include_once('/home/asg5/htdocss/config.php');
include_once('/home/asg5/htdocss/db.php'); 

//sleep(1000000000000); exit;

// Mail adresses:

$mails2 = $mails = $admin_mails = '';

$cp = new cyka2($DB, $admin_mails, $mails, $mails2);
$cp->run();

?>
