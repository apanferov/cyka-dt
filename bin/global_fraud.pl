#!/usr/bin/perl
# -*- encoding: cp1251 -*-
use strict;
use warnings;
use lib '../lib';
use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use Disp::Config;
use Disp::Utils;
use Disp::Operator;
use Disp::Manager;

sub main {
    Log::Log4perl->init("../etc/log.conf");
    Config::init(config_file => "../etc/asg.conf");

    my $st = fetch_fraudulent_abonents();
    my $hr;
    while (defined($hr = $st->fetchrow_hashref)) {
	my $severity = 'NORMAL';
	$severity = 'CRITICAL' if ($hr->{cnt} > $hr->{thr} * 2);

	# Time, Severity
	my ( $old_t, $old_s ) = check_abonent($hr->{ab});

	my $severity_changed = (not $old_s or ($old_s ne $severity));
	my $notify_time_reached = (not $old_t or ( $old_t + 3600 * 24 < time ));

	if ($severity_changed or $notify_time_reached) {
	    notify_manager($hr, $severity);
	    update_notifier($hr->{ab}, $severity);
	}
    }
}
main();

# ABonent
sub check_abonent {
    my ( $ab ) = @_;
    return Config->dbh->selectrow_array("select last_notify_time, last_notify_severity from tr_fraud_notifies where abonent = ?", undef,
					$ab );
}

sub manager_email {
    my ( $operator_id ) = @_;
    my $op = Disp::Operator->load($operator_id);
    if ($op and $op->{manager_id}) {
	my $mg = Disp::Manager->load($op->{manager_id});
	if ($mg and $mg->{email}) {
	    return $mg->{email};
	}
    }
    return;
}

# HashRef, SEVerity
sub notify_manager {
    my ( $hr, $sev ) = @_;
    yell( <<MSG, code => '', process => 'ANOMAL_TRAFFIC', header => "Fraud for operator $hr->{op} - action required", cc => manager_email($hr->{op}), contact_group => 'fraud' );
��� ��������� ���������� ������ ��� �������� $hr->{ab}

�� 24 ���� ������ $hr->{cnt} ��������� ��� ������ ����������� $hr->{thr}

���������: $sev
MSG
}

# ABonent, SEVerity
sub update_notifier {
    my ( $ab, $sev ) = @_;

    Config->dbh->do(<<SQL, undef, $ab, time, $sev, time, $sev);
insert into tr_fraud_notifies SET abonent = ?, last_notify_time = ?, last_notify_severity = ?
ON DUPLICATE KEY UPDATE last_notify_time = ?, last_notify_severity = ?
SQL
}

sub fetch_fraudulent_abonents {
    my $dbh = Config->dbh;
    $dbh->do("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_price_loader ( operator_id INTEGER UNSIGNED NOT NULL, num VARCHAR(255) NOT NULL, date_start date NOT NULL)");
    $dbh->do("TRUNCATE tmp_price_loader");
    $dbh->do("insert into tmp_price_loader select operator_id, num, max(date_start) from set_num_price group by 1,2");
    my $sql = <<EOF;
SELECT i.abonent as ab, t.operator_id as op, t.num as num , n.per_abonent_throttle as thr, sum(transport_count) as cnt
FROM tmp_price_loader t
NATURAL JOIN set_num_price n
JOIN tr_inboxa i ON i.operator_id = t.operator_id and i.num = t.num
WHERE i.date > date_sub(now(), INTERVAL 1 DAY)
GROUP by 1,2,3,4
HAVING cnt >= n.per_abonent_throttle;
EOF
    my $st = $dbh->prepare($sql);
    $st->execute;
    return $st;
}

__END__

create table tr_fraud_notifies ( abonent VARCHAR(20) PRIMARY KEY,
 last_notify_time INTEGER,
 last_notify_severity VARCHAR(20) );
