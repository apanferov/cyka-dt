#!/usr/bin/perl
#######################################################
#
#  CHECKER
#
#######################################################

use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use IO::File;
use Log::Log4perl qw(get_logger);
use Disp::AltDB;
use Disp::Utils qw(yell);
use Disp::Config qw(param);
use LWP::UserAgent;
use URI;
use POSIX;

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $terminated = 0;
my ($log, $db);
my @db = ();

my %check_tab;
%check_tab = ( 'new_traffic_monitoring' =>
			   { error_code => 0,
				 check_interval => 300, last_check_time => 0,
				 notify_interval => 3600, last_notify_time => 0,
				 check_sub => sub{
					 my $settings = load_settings();
					 my $statistics = load_statistics();
					 my $notify_array = check_traffic($settings,$statistics);
					 my $result = ($notify_array) ? 1 : 0;
					 return $result,$notify_array;
				 },
				 notify_sub => sub {
					 my ($error_code,$notify_array) = @_;
					 notify_managers($notify_array,
									 $check_tab{'new_traffic_monitoring'}->{last_notify_array});
					 $check_tab{'new_traffic_monitoring'}->{last_notify_array} = $notify_array;
					 return 0,'OK.';
				 },
			   },
			   'safonov_monitoring' =>
			   { error_code => 0,
				 check_interval => 300, last_check_time => 0,
				 notify_interval => 3600, last_notify_time => 0,
				 check_sub => sub{
					my $outbox_has_msg = 0;
					my $err_msg_count  = 0;
					while ( my($host, $attr) = each %{Config->section('db_transport')} )
					{
						my $db = Disp::AltDB->new($attr);
						$outbox_has_msg += $db->get_value("select id from tr_outboxa where subdate(now(),INTERVAL 5 MINUTE) < date and service_id=1010 limit 1")
							|| 0;
						$err_msg_count  += $db->get_value("select count(*) from tr_errsms where subdate(now(),INTERVAL 5 MINUTE) < date and service_id=1010 and errmessage rlike '[3]TH'")
							|| 0;
					}
					return ($err_msg_count || !$outbox_has_msg ? 1 : 0), $err_msg_count, $outbox_has_msg;
				 },
				 notify_sub => sub {
					 my ($error_code, $err_msg_count, $outbox_has_msg) = @_;
					 my $text = ($error_code) ? "Mob Platej RUR - CRITICAL." : "Mob Platej RUR - OK.";
					 $text .= "$text\nNo response messages during last 5 minutes." unless $outbox_has_msg;
					 $text .= "$text\n$err_msg_count messages failed on 3rd try during last 5 minutes." if $err_msg_count;
					 $text .= POSIX::strftime("\nDate: %D %T",localtime);
					 my $msg = {	plug => 'safonov_alert',
									operator_id => 204,
									abonent => 79856448822, #Safonov
									msg => $text,
							   };
					 send_msg($msg);
					 #$msg->{abonent} = 79269159871; #Fokin
					 #send_msg($msg);
					 #$msg->{abonent} = 79851354510; #Dronin
					 #send_msg($msg);
					 #$msg->{abonent} = 79851354534; #Islamov
					 #send_msg($msg);
				 },
			   },

			   'umcs_cert' =>
			   {
				  error_code => 0,

				  check_interval  => 20*60*60,
				  last_check_time => 0,

				  notify_interval  => 0,
				  last_notify_time => 0,

				  check_sub => sub
					  {
						  my $errcode = 0;
						  my @result  = ();

						  use Date::Parse qw(str2time);

						  foreach my $host (keys %{Config->section('db_transport')})
						  {
							  foreach my $cert ('/gate/asg5/etc/umcs_a1_alt.pfx')
							  {
								  my $na_date = `ssh -i ~/.ssh/roasg_id_dsa roasg\@$host openssl pkcs12 -password pass: -nodes -in $cert -clcerts 2>/dev/null | openssl x509 -noout -enddate`;
								  my @dt = $na_date =~ /notAfter=(.+)$/;
								  push @result, [$cert, @dt ? $dt[0] : '<<INVALID DATE>>', $host];
								  $errcode++ unless (@dt && str2time($dt[0]) > time() - 30*24*60*60);
							  }
						  }
						  ($errcode, @result);
					  },

				  notify_sub => sub
					  {
						  my($errcode, @result) = @_;

						  my $code = $errcode ? 'ALERT' : 'OK';
						  my $text = '';
						  $text .= sprintf("%s expires at %s on %s\n", @$_) for @result;

						  yell($text,
							  code    => $code,
							  process => 'CHECKER',
							  header  => sprintf('Mobile Commerce %s Certificate Expiration',
										  strftime('%d-%m-%Y %H:%M', localtime)),
							  #cc      => 'i.fedorov@alt1.ru',
							  ignore  => { log4perl => 1, file => 1, database => 1 });
						  (0, 'OK');
					  }
			   },

			   'umcs_monitoring' =>
			   {
				  error_code => 0,

				  check_interval  => 30*60,
				  last_check_time => 0,

				  notify_interval  => 30*60,
				  last_notify_time => 0,

				  our_status_msg => [
					  'UNKNOWN', #internal
					  'Request of reserve has failed',
					  'Request of reserve has been successful',
					  'Acknowledgement of reserve has failed', #internal
					  'Acknowledgement of reserve has been successful',
					  'Acknowledgement of reserve has failed on other side',
					  'Processing purchase',
					  'Purchase has failed',
					  'Purchase has been successful',
					  'Purchase rejecting has been successful',
					  'Purchase rejecting has failed on other side',
					  'Rejecting of reserve has been successful',
					  'Rejecting of reserve has failed on other side',
					  'UNKNOWN', #not implemented
					  'Request\'s timeout rollback has been successful',
					  'Request\'s timeout rollback has failed on other side'
					  ],
				  
				  check_sub => sub
					  {
						  my($outsmsN, $errsmsN) = (0, 0);
						  my @result = ();
						  while ( my($host, $attr) = each %{Config->section('db_transport')} )
						  {
							  my $db = Disp::AltDB->new($attr);

							  my $a = $db->{db}->selectall_arrayref(
							  	'select o.status,u.our_status,count(*) as n from tr_outsmsa o
								left join tr_umcs_requests u on o.msg_id=u.order_id
								where o.smsc_id=88 AND o.date between subdate(now(), interval 90 minute) and subdate(now(), interval 30 minute)
								group by o.status,u.our_status', { Slice => {} });

							  foreach my $r (@$a) {
							  	my $t;
							  	if (!$r->{our_status}) {
									$t = $r->{status} == 255 ? "Message wasn't delivered to smsc" : 'No reserve for purchase initiated by alt1';
								}
								else {
									$t = $check_tab{umcs_monitoring}->{our_status_msg}->[$r->{our_status}];
								}
								$r->{info} = $t;
							  	push @result, $r;warn Dumper($a);
								$outsmsN += $r->{n};
								$errsmsN += $r->{n} if ($r->{status} != 12);
							  }
						  }
						  my $percent = $outsmsN && int($errsmsN * 100 / $outsmsN + 0.5);warn "$outsmsN, $errsmsN, $percent";
						  ($percent > 15 || 0, $percent, $outsmsN, $errsmsN, \@result, time);
					  },

			  	  notify_sub => sub
					  {
						  my($errcode, $percent, $outsmsN, $errsmsN, $a, $t) = @_;
						  
						  my $code = $errcode ? ($percent <= 10 ? 'WARNING' : 'ALERT') : 'OK';
						  my $text = sprintf("Erroneous requests %d of %d (%%%d) between %s and %s\n\n",
						  	$errsmsN, $outsmsN, $percent, POSIX::ctime($t-90*60), POSIX::ctime($t-30*60));

						  my $dump = "\n\nstatus | our_status | count | comment\n";
						  $dump .= sprintf("   %3d |         %2d | %5d | %s\n",
						  	$_->{status}, $_->{our_status}, $_->{n}, $_->{info}) foreach (@$a);

						  yell($text . $dump,
							  code    => $code,
							  process => 'CHECKER',
							  header  => sprintf('Mobile Commerce %s',
										  strftime('%d-%m-%Y %H:%M', localtime)),
							  cc      => 'a.cherkashchenko@alt1.ru',
							  ignore  => { log4perl => 1, file => 1, database => 1 }, to=>'a.kurzhonkov@alt1.ru');

						  my $sms = {
							  plug => 'safonov_alert',
							  operator_id => 204,
							  abonent => 79851354601,
							  msg => $text
							  };
						  send_msg($sms);
					  }
			   },
# disabled
#			   'king_monitoring' =>
#			   { error_code => 0,
#				 check_interval => 300, last_check_time => 0,
#				 notify_interval => 3600, last_notify_time => 0,
#				 check_sub => sub{
#					 my $err_msg_count = $db->get_value('select count(*) from tr_errsms where adddate(date,INTERVAL 5 MINUTE) > now() and service_id=1023');
#					 my $result = ($err_msg_count > 1) ? 1 : 0;
#					 return $result,$err_msg_count;
#				 },
#				 notify_sub => sub {
#					 my ($error_code,$err_msg_count) = @_;
#					 my $msg = {	plug => 'safonov_alert',
#									operator_id => 204,
#									abonent => 79157895232,
#									msg => ($error_code) ? "Peredovie Tehnologii CRITICAL. $err_msg_count errors during last 5 munutes." : "Peredovie Tehnologii OK",
#							   };
#					 send_msg($msg);
#				 },
#			   },
#
			 );

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $process_name='CHECKER';

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
				"log4perl.rootLogger" => Config->param('log_level',lc($process_name)).', Logfile',
				"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
				"log4perl.appender.Logfile.recreate" => 1,
				"log4perl.appender.Logfile.recreate_check_interval" => 300,
				"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
				"log4perl.appender.Logfile.umask" => "0000",
				"log4perl.appender.Logfile.filename" => Config->param('log_filename',lc($process_name)),
				"log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
				"log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern',lc($process_name))
			   );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################
	
	$db = new Disp::AltDB();
	
#######################################################
#  Main loop
#######################################################

	$log->info('Started.');

	my $last_ping_time = 0;
	while (!$terminated)	{

		if ((time() - $last_ping_time) >= 60) {
			$db->ping();
			$last_ping_time = time();
		}

		foreach (keys(%check_tab)) {
			my $tab = $check_tab{$_};
			my $tab_name = $_;
			if ((time() - $tab->{last_check_time}) >= $tab->{check_interval}) {
				$log->info("$tab_name> Start checking");
				my @new_check_array = &{$tab->{check_sub}}($tab_name);
				my $new_error_code = $new_check_array[0];
				$log->info("$tab_name> Status: $new_error_code (was $tab->{error_code})");
				my $delta = time() - $tab->{last_notify_time};
				if (($new_error_code and($delta >= $tab->{notify_interval})) or
					($new_error_code!=$tab->{error_code})) {
					my $text = (ref($new_check_array[1]) eq 'HASH') ? Dumper($new_check_array[1]) : $new_check_array[1];
					$log->debug("$tab_name> $new_error_code - $text");
					my ($notify_status, $notify_text) = &{$tab->{notify_sub}}(@new_check_array);
					if ($notify_status) {
						$log->warn("$tab_name> Notify - $notify_text");
						yell("$tab_name> Notify - $notify_text",code => 'FATAL',process =>$process_name,
							 header => "$process_name: $notify_text",
							 ignore => { log4perl => 1  });
					}
					else {
						$log->info("$tab_name> Notify - $notify_text");
					}
					$tab->{last_notify_time} = time();
				};
				$tab->{error_code} = $new_error_code;
				$tab->{last_check_time} = time();
			}
		}
		sleep 1;
	}
};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();

#######################################################
#  Internal Functions
#######################################################

sub load_settings {
	my $tmp = $db->{db}->selectall_arrayref("select operator_id,num,check_period,is_active from set_traffic_monitor order by operator_id, num");
	my $settings;
	foreach (@$tmp) {
		$settings->{$_->[0]}->{$_->[1]} = ($_->[3]) ? $_->[2] : undef;
	}

	$tmp = $db->{db}->selectall_arrayref("select distinct p.operator_id,p.num,o.parent_id from set_plugin p inner join set_operator o on o.id=p.operator_id inner join set_num_price n on (p.operator_id=n.operator_id and n.num=p.num and n.cpa=0 and n.mt=0) where p.active>0");
	foreach (@$tmp) {
		my $operator_id = $_->[0];
		my $num = $_->[1];
		if (!exists($settings->{$operator_id}->{$num}) 
			and ($operator_id != 100) and ($_->[2] != 16) and ($_->[2] != 19)) {
			$settings->{$operator_id}->{$num} = 1;
			$db->{db}->do("insert into set_traffic_monitor (operator_id,num,check_period,is_active) values (?,?,6,1)",undef,$operator_id,$num);
			$log->info("Added new number: $operator_id, $num");
		}
	}
	return $settings;
}

sub load_statistics {
	my $tmp = $db->{db}->selectall_arrayref("select operator_id,num,max(hour) from tr_sms_statistics where count>0 and status='income' group by operator_id,num");
	my $statistics;
	$statistics->{$_->[0]}->{$_->[1]} = ($_->[2]) foreach (@$tmp);
	return $statistics;
}

sub check_traffic {
	my ($settings,$statistics) = @_;
	my $current_hour = int(time()/3600);
	my $result;
	foreach (keys(%{$settings})) {
		my $operator_id=$_;
		foreach (keys(%{$settings->{$operator_id}})) {
			my $num=$_;
			if ($settings->{$operator_id}->{$num}) {
				my $check_period = $settings->{$operator_id}->{$num};
				my $last_active_hour = $statistics->{$operator_id}->{$num} || 0;
#				$log->debug("$current_hour - $last_active_hour > $check_period");
				if (($current_hour - $last_active_hour) > $check_period) {
					$result->{$operator_id}->{$num} = $last_active_hour;
				}
			}
		}
	}
	return $result;
}

sub notify_managers {
	my ($notify_array,$last_notify_array) = @_;
	my $operators = $db->{db}->selectall_hashref("select o.id as id, o.name as name, m.email as email from set_operator o left join set_cyka_manager m on o.manager_id=m.id",'id');
	foreach (keys(%{$notify_array})) {
		my $operator_id=$_;
		my $operator_name = $operators->{$operator_id}->{name};
		my $text = "\n$operator_name [$operator_id]\n��� ������� �� ��������� �������:\n";
		foreach (keys(%{$notify_array->{$operator_id}})) {
			my $num = $_;
			my $hour = POSIX::strftime("%Y/%m/%d %H:%M",localtime(($notify_array->{$operator_id}->{$num}+1)*3600));
			$text .= "\t$num � $hour\n";
		}
		delete $last_notify_array->{$operator_id};
		yell($text,code => '',process => 'CHECKER',
			 header => "CHECKER: $operator_name - CRITICAL.",
			 contact_group => 'traffic_monitor', # cc => $operators->{$operator_id}->{email},
			 ignore => { log4perl => 1, file => 1, database => 1 });
	}
	foreach (keys(%{$last_notify_array})) {
		my $operator_id=$_;
		my $operator_name = $operators->{$operator_id}->{name};
		my $text = "\n$operator_name [$operator_id]\n�� ���� ������� ���� ������.";
		yell($text,code => '',process => 'CHECKER',
			 header => "CHECKER: $operator_name - OK.",
			 contact_group => 'traffic_monitor', # cc => $operators->{$operator_id}->{email},
			 ignore => { log4perl => 1, file => 1, database => 1 });
	}
}

sub send_msg {
	my ($msg) = @_;
	my $ua = LWP::UserAgent->new;
	$ua->timeout(10);

	my $u = new URI("http://gateasg.alt1.ru/cgi-bin/push.pl");
	$u->query_form(service_id => 100, msg => "status: notice\ndestination: $msg->{abonent}\nplug: $msg->{plug}\nforce-operator-id: $msg->{operator_id}\n\n$msg->{msg}");
	my $resp = $ua->get($u->as_string);

	if ($resp->content() =~ /^\d+$/) {
		return 0,'OK.';
	}
	else {
		$u = new URI("https://www.smstraffic.ru/multi.php");
		$u->query_form( login => 'vorobieva', password => 'pofikiha', phones => $msg->{abonent}, originator => '1121', message => $msg->{msg} );
		$resp = $ua->get($u->as_string);
		return 1,'OK. (PUSH notification failed)' if ($resp->is_success());
		return 1,'FAIL. (PUSH and SMStraffic notification failed)';
	}
}

########### DO_EXIT ###########################

sub do_stop
{
	if ($db)   {
		$db->finish();   }

	$log->info("Stopped.\n\n\n");
}

sub do_die
{
	my ($str)=@_;
	$log->fatal($str);
	yell($str,code => 'FATAL',process =>$process_name,
		 header => "$process_name: $str",
		 ignore => { log4perl => 1  });
	do_stop;
	exit(0);
}
