#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use GD;
use Digest::MD5 qw/md5_hex/;

use Log::Log4perl;
use Disp::Config;

sub gen_code {
    my ( $code_length ) = @_;

    return sprintf("\%0${code_length}d", int(rand(10**$code_length-1)));
}

sub update_db {
    my ( $service_id, $current_code, $prev_code, $update_period) = @_;

    Config->dbh->do("REPLACE INTO tr_service_antispam SET service_id = ?, current_code = ?, previous_code = ?, valid_through = from_unixtime(unix_timestamp(now()) + 60 * ? + 90)", undef,
		    $service_id, $current_code, $prev_code, $update_period );
}

sub crypt_service_id {
    my ( $service_id ) = @_;

    my $h_sid = sprintf '%05i', $service_id;
    my $md5 = md5_hex("1snt438gy".$h_sid."tnhnt3598ejbks3");
    return $h_sid.$md5;
}

sub static_text_method {
    my ( $service_id, $current_code ) = @_;

    my $dir = Config->param(static_dir => 'antispam');
    my $file = $dir . "/" . crypt_service_id($service_id) . ".txt";

    open(my $fh, ">", $file); # yell

    print $fh $current_code;
    close $fh;
}

sub static_image_method {
    my ( $service_id, $current_code ) = @_;

    my $img = new GD::Image(2+8*length($current_code), 16);

    my $white = $img->colorAllocate(255,255,255);
    my $black = $img->colorAllocate(0,0,0);

    $img->fill(1,1, $white);

    $img->string(GD::Font->Large, 1, 1, $current_code, $black);

    my $dir = Config->param(static_dir => 'antispam');
    my $basename = $dir . "/" . crypt_service_id($service_id);

    my $do_it = sub {
	my ( $generator, $ext ) = @_;
	open my $fh, ">", "$basename.$ext"; # yell
	print $fh $generator->();
	close $fh;
    };

    $do_it->(sub {$img->gif}, "gif");
    $do_it->(sub {$img->jpeg}, "jpg");
}

my %update_file_methods = ( image => \&static_image_method,
			    text => \&static_text_method );

sub update_file {
    my ( $service_id, $current_code, $allowed_methods ) = @_;

    foreach my $meth (split /,/, $allowed_methods) {
	my $handler = $update_file_methods{$meth};
	unless (ref($handler) eq 'CODE') {
	    # yell();
	    next;
	}
	$handler->($service_id, $current_code);
    }
}

sub main {
    	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg.conf");

	my $st = Config->dbh->prepare("select id,
	antispam_code_length, antispam_update_period,
	antispam_allowed_methods from set_service where
	antispam_code_length > 0 and antispam_update_period > 0 and
	length(antispam_allowed_methods) > 0");

	my $st_tr = Config->dbh->prepare("select current_code, previous_code, unix_timestamp(date) from tr_service_antispam where service_id = ?");

	$st->execute;

	while (my($id, $cl, $up, $am) = $st->fetchrow_array) {

	    $st_tr->execute($id);

	    my ( $cc, $pc, $dt ) = $st_tr->fetchrow_array;

	    if ( not defined $cc or time > $dt + $up * 60) {
		my $new_code = gen_code($cl);

		print "$new_code\n";
		update_db($id, $new_code, $cc || "", $up);
 		update_file($id, $new_code, $am);
	    }
	}
}
main();
