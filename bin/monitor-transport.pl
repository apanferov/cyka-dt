#!/usr/bin/perl
use strict;
use warnings;
use locale;
use lib '../lib/';
use Disp::DaemonManager;
use Disp::DaemonManager::TranscieverSource;

Disp::DaemonManager::run_monitor(\&init,
								 kind => 'transport',
								 status_file => '../log/monitor-transport.status',
								 pid_file => '../var/run/monitor-transport.pid',
								 config_file => '../etc/asg.conf',
								 log_config => '../etc/log.conf');

sub init {
	my ( $dm ) = @_;
	$dm->perm_daemons( 'permanent_transport',
					   yell			 => [ 'bin'	, './yell.pl' ],
					   fastcgi_info		 => [ 'cgi-bin'	, './info.pl' ],
					   fastcgi_int_info	 => [ 'cgi-bin'	, './int_info.pl' ],
  					   fastcgi_push		 => [ 'cgi-bin'	, './push.pl'],
  					   fastcgi_service	 => [ 'cgi-bin'	, './service.pl'],
  					   fastcgi_call_alert	 => [ 'cgi-bin'	, './call_alert.pl'],
 					   dispatcher		 => [ 'bin'	, 'perl ./dispatcher.pl'],
					   dr_dispatcher	 => [ 'bin'	, 'perl ./dr_dispatcher.pl'],
 					   distributor		 => [ 'bin'	, 'perl ./distributor.pl'],
					   replication		 => [ 'bin'	, 'perl ./replication.pl' ],
 					   encoder		 => [ 'bin'	, 'perl ./encoder.pl'],
					   decoder		 => [ 'bin'	, 'perl ./decoder.pl'],
					   redis2cpe		 => [ 'bin'	, 'perl ./redis2cpe.pl'],
	);

 	my $ts = new Disp::DaemonManager::TranscieverSource( subdir => Config->param('system_root').'/bin',
 														 execute => 'perl ./sms_client.pl',
 														 dbname => Config->param(qw/name database/),
 														 dbuser => Config->param(qw/username database/),
 														 dbhost => Config->param(qw/host database/),
 														 dbpassword => Config->param(qw/password database/) );
 	$dm->add_data_source(source => $ts, prefix => 'smpp');
}
