#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Disp::HTTP_SMSReceiver2;
use Log::Log4perl qw/get_logger/;

sub main {
	my ($smsc_id,$debug_level,$smpp_mode);

	$smsc_id = 91;
	$debug_level = 'DEBUG';
	$smpp_mode = 22;

	Disp::HTTP_SMSReceiver2->new($smsc_id, $debug_level, $smpp_mode)->run;
}

main();

# eval {
# 	main();
# };

# if ($@) {
# 	yell("ABNORMALLY TERMINATED DISPATCHER: $@",
# 		 process => 'DISPATCHER',
# 		 code => 'PROC_STOP',
# 		 header => 'Dispatcher abnormal termination' );
# }
