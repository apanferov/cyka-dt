#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl;
use XML::Simple;
use Data::Dumper;
use Disp::UmcsSOAP;

sub iso_time {
	my ( $time ) = @_;
	return $time unless $time =~ /^(\d{4}-\d{2}-\d{2})\s+(\d{2}:\d{2}:\d{2})$/;

	return "$1T$2+03:00";
}

sub main {
	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg.conf");

	foreach (Disp::UmcsSOAP::certificate_groups()) {
		make_feo_batches($_);
	}

	send_feo_batches();

}

sub make_feo_batches {
	my ( $certificate_group ) = @_;
	my $tries = 0;

	my @short_numbers = Disp::UmcsSOAP::certificate_numbers($certificate_group);
	my $short_numbers_ph = "(" . join(",", ("?")x@short_numbers) . ")";

	while (1) {
		my ($unassigned_count) = Config->dbh->selectrow_array("select count(*) from tr_umcs_requests where our_status = 8 and feo_code is null and num in $short_numbers_ph", undef, @short_numbers);

		last unless $unassigned_count;

		eval {
			Config->dbh->begin_work;

			Config->dbh->do("insert into tr_umcs_feo(code, certificate_group) values (null, ?)", undef, $certificate_group);
			my $feo_code = Config->dbh->{mysql_insertid};

			Config->dbh->do('select @a := 0');
			Config->dbh->do("update tr_umcs_requests set feo_code = ?, feo_seq_number = (\@a := \@a + 1) where feo_code is null and our_status = 8 and num in $short_numbers_ph limit 500",
							undef, $feo_code, @short_numbers);

			Config->dbh->commit;
		};

		if ( $@ ) {
			Config->dbh->rollback;
		}

		if ($tries++ > 100) {
			die "Too many tries($tries) in make_feo_batches";
		}
	}
}

sub send_feo_batches {
	my $sth = Config->dbh->prepare("select code, certificate_group from tr_umcs_feo where send_result is null");
	$sth->execute;
	while (my($feo_code, $certificate_group) = $sth->fetchrow_array) {

		Disp::UmcsSOAP::cert_conf($certificate_group);
		my $batch = make_batch_xml($feo_code);
		my $send_result;
		if ($batch) {
			$send_result = Disp::UmcsSOAP::Helper::TakeFileDailyReport
			  ('https://umcs.beeline.ru/', # "http://gw2.aqt.ru:86/UMCSShopInterface/UMCSShopInterfaceWS.asmx",
			   'http://www.estylesoft.com/umcs/shopinterface/',
			   $batch
						  );
		}
		Config->dbh->do("update tr_umcs_feo set send_result = ?, send_time = now() where code = ?", undef,
						$send_result, $feo_code);
	}
}

sub make_batch_xml {
	my ( $feo_code ) = @_;

	my ( $dateFrom, $dateTo ) = Config->dbh->selectrow_array("select date_sub(min(purchase_date), interval 90 minute), date_add(max(purchase_date), interval 90 minute) from tr_umcs_requests where feo_code = ?",undef,
															 $feo_code);
	my $sth = Config->dbh->prepare("select order_id, feo_seq_number, purchase_date, our_price from tr_umcs_requests where feo_code = ? order by feo_seq_number");
	$sth->execute($feo_code);

	my @lines;
	while (my ($order_id, $feo_seq_number, $purchase_date, $our_price) = $sth->fetchrow_array) {
		push @lines, { order => $order_id,
					   id => $feo_seq_number,
					   date => iso_time($purchase_date),
					   price => $our_price };
	}
	return unless @lines;
	my $xml = { feo => { code => $feo_code,
						 date => iso_time(today()),
						 dateFrom => iso_time($dateFrom),
						 dateTo => iso_time($dateTo),
						 allCount => 1,
						 count => 1,
						 item => \@lines
					   } };
	XMLout($xml, KeepRoot => 1, XMLDecl => 1);
}

eval {
	main();
};

if ($@) {
	yell("ABNORMALLY TERMINATED UMCS FEO SENDER: $@",
		 process => 'FEO_SENDER',
		 code => 'PROC_STOP',
		 header => 'Feo sender abnormal termination' );
}

__END__


ALTER TABLE tr_umcs_feo add column certificate_group integer not null;

