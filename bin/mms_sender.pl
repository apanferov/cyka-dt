#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';  


use Disp::Utils qw(yell);
use Log::Log4perl;
use Log::Log4perl qw(get_logger); 
use Log::Log4perl::Layout;
use Log::Log4perl::Level;
use Disp::Config qw(param); 
use Config::Tiny; 
use Data::Dumper;

use MMS::XML;
use MMS::Encoder;  
use MMS::http;

my $smsc_id;
my $log;
my $XMLwriter=MMS::XML->new();
my $MMSwriter=MMS::Encoder->new;
my $httpClient=MMS::http->new();
my $db;
my $terminated=0;
my($sleep_time, $network_error_timeout, $maxErrorCounter);
my %conection=();
my $conection_id=0;
my $TRACE=0;

eval{
 foreach (@ARGV)
 {
 	$TRACE=1 if (index($_,'TRACE')>=0);
};

print "trace is enabled\n" if($TRACE==1);



# ���������� ������� �� ����� �� ���������..	
$SIG{QUIT}=sub {$log->info('QUIT')if($log); $terminated=1;};
$SIG{INT} =sub {$log->info('INT')if($log);  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM')if($log); $terminated=1;}; 

{# �������������: ������  ����� ������������
   
  
$conection_id=$ENV{'SMPP_SMSC_ID'}if(defined($ENV{'SMPP_SMSC_ID'}));
	Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);

	
	#  Logging preparations
	
	my %log_conf = (
	   "log4perl.rootLogger"       													=> Config->param('log_level','mms_sender').", Logfile",
	   "log4perl.appender.Logfile"													=> "Log::Log4perl::Appender::File",
	   "log4perl.appender.Logfile.filename" 								=> "../log/msnd_$conection_id.log",# +smsc_t::id
	   "log4perl.appender.Logfile.recreate" => 1,
	   "log4perl.appender.Logfile.recreate_check_interval" => 300,
	   "log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
	   "log4perl.appender.Logfile.layout" 									=> "Log::Log4perl::Layout::PatternLayout",
	   "log4perl.appender.Logfile.layout.ConversionPattern" =>Config->param('log_pattern','mms_sender')	

    	);

    	$sleep_time=Config->param('sleep_time','mms_sender');
    	$network_error_timeout=Config->param('network_error_timeout','mms_sender');
    	$maxErrorCounter=Config->param('maxErrorCounter','mms_sender')	;

	Log::Log4perl->init( \%log_conf);
	$log=Log::Log4perl::get_logger("");
	$log->info("Starting...");
   die 'Not defined SMPP_SMSC_ID at GLOBAL ENVARIENTMENTS' if(!defined($ENV{'SMPP_SMSC_ID'}));




	
	Config::db_connect() or die "Couldn't connect to dataBase";
   $db = Config::dbh(); 
   ($conection{'IP'},$conection{'port'},$conection{'login'},$conection{'password'})=
   	$db->selectrow_array("SELECT host, port, system_id, password FROM set_smsc WHERE id=$conection_id");
   	if($TRACE==1){ print "$_=>$conection{$_}\n" foreach keys %conection;}
   	 
#������ �� ���� ���������� ����������


#����� �� ��������� � ��������� �������: �� ������� ��������� ����������?   
   
####################################

$httpClient->setLog($log);

$log->debug(Dumper(\%conection));
}	

do{#���� ������� ��������� �� �� � �� ��������
	
my ($newMessageCount)=$db->selectrow_array('SELECT COUNT(*) FROM tr_outmmsa WHERE status=0');
##### �������� �������� ������� ��� ������������, �� ������������ ��������� � ������ outmmsa
if($TRACE==1){print "The count of new messages is $newMessageCount\n";}

if($newMessageCount>0){#	��������� ���������
	


	my ($mms_id,$from,$to,$transaction_id,$msg,$inbox_id,$test_flag,$outbox_id)=$db->selectrow_array('SELECT id,  num,abonent, transaction_id, msg,inbox_id, test_flag, outbox_id  FROM tr_outmmsa WHERE status=0');
	die 'The table already cleaned...' if(!$mms_id);
	
# ����� ��������� ���������
{# ����� ������������ � ������� ��������� � ���������� ���������
####################################
{# ������������ XML ����� ���������
####################################
$XMLwriter->resetAddres();
$XMLwriter->setPushID($mms_id);#$push_id"4879683648"
$XMLwriter->addAddress("$to");
####################################
}

{# ������������ MMS ����� ���������
####################################

$MMSwriter->resetBody();

$MMSwriter->setOptions(undef,#'X-MMS-Message-Class'
											undef,#'X-MMS-Priority'
											undef,#'X-MMS-Read-Reply'
											);
$MMSwriter->setHeader($from,#From
											$to,#To
											 '',#SUBJECT
											 $mms_id,#'X-MMS-Transaction-ID'$transaction_id
											 );
####################################
#���������� ������  �� ���� ������
#$MMSwriter->addContent('text/plain','Hello World!');

my ($type,$content_ID,$filename,$data);	
$_=	$msg;
my @data_list= /\d+/g;;
if($TRACE==1){print "$_\n" foreach (@data_list);}
foreach (@data_list){
($type,$content_ID,$filename,$data)=$db->selectrow_array("SELECT content_type,content_id,filename,data FROM tr_outmmsa_data WHERE id=$_");
if($type&&$data){
if($TRACE==1){print "data_type=$type\ndata:\n$data\n";}
$MMSwriter->addContent($type,$content_ID,$filename,$data);
}else{
	if($TRACE==1){print "not found data with id=$_\n";}
	$log->warn("not found data with id=$_ for mms with outbox_id=$inbox_id")	;
}

}

####################################
}

{# ������������ � �������� http �������
####################################
$_=$conection{'port'};
my $port=join('',/\d+/g);
$httpClient->setHost("$conection{'IP'}:$port");#'212.44.140.187:10021'
$httpClient->setAuthorization($conection{'login'},$conection{'password'});#"441"_"rg8ev6qn"
#billing System Requered
#$httpClient->setBillingData("A1 Service; 0.5");
####################################
my ($XML,$MMS)=($XMLwriter->encodeXML(),$MMSwriter->encodeMMS());
my $errorCounter=-1;
my $message_id=0;
my $description="";
my $status=0;
if($test_flag!=2){
	$log->debug("Sending message with id=$mms_id ...\n\t*******************");
do{#�������� ��������� � ������ ��������� ������...
	    	
    	
    	
 	
sleep $network_error_timeout if (++$errorCounter>$maxErrorCounter);

$httpClient->sendRequest("http://".$conection{'IP'}.":".$conection{'port'}."/vas",#  ##    :$conection{'port'}
													$XMLwriter->encodeXML(),
													$MMSwriter->encodeMMS(),
													$XML,$MMS
													);
}until ($httpClient->Sended()||$errorCounter>$maxErrorCounter);#������� ������ � ������ �������
	


{# #������� ����� ������� � ��������� �������� � ������ ������ � ��������...	
	$message_id=0;
	$description="";
	$status=0;
	if($httpClient->Sended()){#��������� ��������� �������
	my %result=$httpClient->getAnswer();# ��������� ���� ���������� �������

		if($result{'code'}=='1000'){
				$status=1;
				$db->do('update tr_billing set outsms_count=outsms_count+1 where outbox_id=?',undef,$outbox_id);# unless ($ignore_billing); 
		   	    #$db->do('update billingf set outsms_count=outsms_count+1 where outbox_id=?',undef,$outbox_id);# unless ($ignore_billing); 
				
		}else{
				$status=2;	
				$description='Data Error: ['.$result{'code'}.'] '.$result{'desc'};#��������� ���� ��������� ������� ������
				$log->error("the message with id=$mms_id couldn't be sended:\n\t$description".(($log->is_debug)?(" \n".$httpClient->getBody()):('')));
				$status=2;
				#warning();#yell
		}
		
		$message_id=$result{'message-id'};
		$description=$result{'desc'};
	}else{#��������� ��������� ������� ������
			$description='NetWork Error: '.$httpClient->getError();#��������� ���� ��������� ������� ������
			$log->error("the message with id=$mms_id couldn't be sended:\n\t$description".(($log->is_debug)?(" \n".$httpClient->getBody()):('')));
			$status=2;
			#������ � ��� ��������� � ������������� �������� ��������� � ��������� ���������������
			#warning($description);#yell
	}

}
}else{
	$description='The test mesage';
	$status=1;
	$log->warn("the message with outbox_id=$outbox_id && id=$mms_id isn't sended (a test message");
}
my $query="UPDATE tr_outmmsa SET date=now(), status=$status, message_id=\"$message_id\", description=\"$description\" WHERE id=$mms_id";
$log->debug("The mms was sent with status=$status,\n\tmessage_id=\"$message_id\",\n\tdescription=\"$description\"\n\tid=$mms_id\n\t*******************\n\n");
if($TRACE==1){print "query=$query\n\n";}
	die $db->errstr if(!$db->do($query));
}

}

	
}else {# �������� ��� ���������� ����� ���������
	sleep $sleep_time;
	my $query="INSERT INTO ";
	$query.="tr_outmmsa (outbox_id,abonent,smsc_id,operator_id,num,msg, transaction_id,service_id,inbox_id,push_id, test_flag, date)";
	$query.="SELECT id,abonent,smsc_id,operator_id,num,msg, transaction_id,service_id,inbox_id,push_id, test_flag, date ";
	$query.="FROM tr_outbox WHERE smsc_id=$conection_id";
	
	die "Error while moving data at DB: ".$db->errstr if(!$db->do($query));#2 
	$query="DELETE FROM tr_outbox WHERE smsc_id=$conection_id";
	die $db->errstr if(!$db->do($query)); 
}

}until($terminated);	
								
};
if ($@){
	#  ��������� ��������� ��������� die � ����� ��������� �� ���� ����� �� yell
	if($TRACE==1){print "The script was fall down\n";
print "$@\n";}
	fatal_error($@);

}else{
 if($TRACE==1){
 	print "The script was fall down\n not known issues\n" if(!$terminated);
 	}
}

$log->info('The Script was stoped') if($log);



sub warning {
   my ($err) = @_;

   my $proc_name = ($conection_id!=0)?('mms_sender_'.$conection_id):('mms_sender');

	yell($err,code => 'Warning',process => 'mms_sender',
		header => "$proc_name",ignore => { log4perl => 1, email => 1 });
   return 1;
}

sub fatal_error {
   my ($err2) = @_;
   $err2 =~ /^(.*?)$/m;
   my $err = $1;
   if($log){
   $log->error($err2);
   $log->error("The process is stoped with Fatal Error");
  }
   my $proc_name = ($conection_id!=0)?('mms_sender_'.$conection_id):('mms_sender');

	yell($err2,code => 'FATAL_ERROR',process => 'mms_sender',
		header => "$proc_name: $err",ignore => { log4perl => 1, email => 1 });
   return 1;
} 

