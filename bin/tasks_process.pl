#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use MIME::Lite;
use IO::Scalar;
use URI::Escape;

my $terminated = 0;
$SIG{QUIT} = sub {$terminated = 1;};
$SIG{INT} = sub {$terminated = 1;};
$SIG{TERM} = sub {$terminated = 1;};

Config::init(config_file => "../etc/asg.conf");
my $db = new Disp::AltDB();
my $pi_last_update_time;
my $pi_last_update_str;
my $from_date;

while ($terminated == 0) {
	$db->reconnect;
	$from_date = POSIX::strftime("%Y-%m-%d", localtime(time()));

	# �������� ������ �� ������� �� ����������
  my $query = "
select
  t.id,
  t.email,
	t.description,
  t.task_id,
  t.data
from tr_task t
where
t.date_start is null 
order by id 
limit 1
";
	my $task = $db->{db}->selectrow_hashref($query);
	if ($task) {
		print "(task_id=$task->{id}) $task->{email}\n";
		eval {
			$task->{params} = get_post_params($task->{data});
			task_make($task);
		};
		if ($@) {
			set_task_err($task->{id}, $@);
		}
	}

  sleep(1);
	print "+\n";
}

print "Done\n";

sub get_post_params {
	my ($param_str) = @_;
	my $params = {};

	if ($param_str) {
		foreach (split ('&', $param_str)) {
	 		my ($k, $v) = split '=';
			$params->{$k} = uri_unescape($v);
		}
	}

	return $params;
}

sub task_make {
	my ($task) = @_;
	my $query;

	if ($task->{task_id} == 1) {
		# � 1 - ������� �� �.�. � ��������
		$db->{db}->do("update tr_task set date_start = now() where id = $task->{id}");

		# �������� �������� ������
		my $data = $task->{params}{data};
		$pi_last_update_time = undef;
		$pi_last_update_str = '';
		set_task_pi($task->{id}, '�������� ������');
		unless ($data) {
			set_task_err($task->{id}, "��� ������ ��� ��������� (POST:data - ����)");
			return;
		}
		my $line_cnt = 0;
		foreach (split ("\n", $data)) {
  		s/\x0D//g;
	  	my ($num, $abonent) = split(/\t/);

			$line_cnt++;

#			print "line_cnt=$line_cnt,num=$num,abonent=$abonent\n";

			next unless (defined($num) || defined($abonent));
			unless (int($abonent)) {
				set_task_err($task->{id}, "������: $line_cnt\nabonent='$abonent' - �� �����");
				return;
			}
			unless (length($num) > 3 && length($num) < 10) {
				set_task_err($task->{id}, "������: $line_cnt\nnum='$num' - ������������ ����� (3 < len(num) < 10)");
				return;
			}
#			print "num=$num, abonent=$abonent\n";
			exit if (check_task_terminated($task->{id}));
		}

		# ����������
		set_task_pi($task->{id}, '0%');
		my $res = '';
		my $line = 0;
		foreach (split ("\n", $data)) {
  		s/\x0D//g;
  		my ($num, $abonent) = split(/\t/);
			next unless (defined($num) || defined($abonent));
			$abonent =~ s/^7//;

#		  sleep(1);

			my $query = "
select cp.num, cp.abonent, group_concat(distinct p.name separator ',') as partners
from cyka_payment cp
left join set_cyka_partner p on p.id = cp.partner_id
where
cp.num like '$num%' and
cp.abonent = 7$abonent and
#cp.operator_id in (120) and
cp.partner_id <> 0
group by cp.num, cp.abonent
";
			my $row = $db->{db}->selectrow_hashref($query, { Slice => {} });
			# ������
			if ($row) {
				$res .= "$row->{num};$row->{abonent};$row->{partners}\n";
			} else {
				$res .= "$num;7$abonent;<null>\n";
			}

			$line++;
			set_task_pi($task->{id}, sprintf("%.2f", ($line / $line_cnt) * 100).'%');
			exit if (check_task_terminated($task->{id}));
		}

		# ������� �����
		$task->{email} = 'os@moscomsvjaz.ru' if ($task->{email} eq 'a.gusev@alt1.ru');

		# �������� ���������� ������
		set_task_pi($task->{id}, '�������� ������');
		my $msg = MIME::Lite->new(
			From => 'cyka_info@alt1.ru',
			To => $task->{email},
			#To => 'v.islamov@alt1.ru',
			Subject => "Long tasks: $task->{description}",
			Type => 'text/plain',
			Data => "Long tasks report"
		);
		$msg->attach(
			Type => 'application/vnd.ms-excel',
			Encoding => 'base64',
			Data => $res,
			Filename => "partners_$from_date.csv"
		);
		raw_sendmail($msg->as_string);

		$db->{db}->do("update tr_task set date_end = now(), progress_info = '���������', result = 'done' where id = $task->{id}");
	} else {
		$query = "
update tr_task
set
	date_start = now(),
	date_end = now(),
	progress_info = '���������',
	result = 'error',
	error_txt = '����������� ��� ������'
where
id = $task->{id}
";
		$db->{db}->do($query);
	}
}

sub set_task_pi {
	my ($id, $info_str) = @_;
	$info_str =~ s/'/''/g;

#	print "pi_last_update_time=$pi_last_update_time, time=".time.", pi_last_update_str=$pi_last_update_str, info_str=$info_str\n";

#	unless (defined($pi_last_update_time) && ($pi_last_update_time + 5 > time) && $pi_last_update_str eq $info_str) {
	unless (defined($pi_last_update_time) && ($pi_last_update_time + 5 > time)) {
		unless ($pi_last_update_str eq $info_str) {
			$db->{db}->do("update tr_task set progress_info = '$info_str' where id = $id");

			$pi_last_update_time = time;
			$pi_last_update_str = $info_str;
		}
#		print "u\n";
#	} else {
#		print "iu\n";
	}
}

sub set_task_err {
	my ($id, $err_str) = @_;
	$err_str =~ s/'/''/g;

	my $query = "
update tr_task
set
	date_end = now(),
	progress_info = '���������',
	result = 'error',
	error_txt = '$err_str'
where
id = $id
";
	$db->{db}->do($query);
}

sub check_task_terminated {
	my ($id) = @_;

	if ($terminated) {
		my $query = "
update tr_task
set
	date_start = null,
	date_end = null,
	result = 'error',
	error_txt = '������ ��������, ��������� ������������ �������.
������ ����� ������������ �������������.'
where
id = $id
";
		$db->{db}->do($query);
	}

	return $terminated;
}

__END__

foreach my $k (keys(%{$task->{params}})) {
	print "$k=$task->{params}{$k}\n";
}





