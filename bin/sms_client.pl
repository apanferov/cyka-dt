#!/usr/bin/perl
#######################################################
#
#  SMPP Client v2
#
#######################################################

use strict;
use warnings;
use lib '../lib';
use Disp::SMPP_SMSClient;
use Disp::HTTP_SMSSender;
use Channel::HTTP_SMSSender4;
use Disp::HTTP_SMSReceiver;
use Disp::HTTP_SMSReceiver3;
use Channel::HTTP_SMSReceiver4;
use Data::Dumper;

my $client;

if ($ENV{SMPP_MODE} == 21) {
        $client = new Disp::HTTP_SMSSender(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}
elsif ($ENV{SMPP_MODE} == 22) {
        $client = new Disp::HTTP_SMSReceiver(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}
elsif ($ENV{SMPP_MODE} == 23) {
        $client = new Disp::HTTP_SMSReceiver3(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}
elsif ($ENV{SMPP_MODE} == 31) {
        $client = new Disp::HTTP_SMSSender4(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}
elsif ($ENV{SMPP_MODE} == 32) {
        $client = new Disp::HTTP_SMSReceiver4(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}
else {
        $client = new Disp::SMPP_SMSClient(
                $ENV{SMPP_SMSC_ID},$ENV{SMPP_DEBUG_LEVEL},$ENV{SMPP_MODE});
}

$SIG{QUIT}=sub {$client->{log}->info('QUIT '.$client->{wait_resp_count}); $client->{terminated}=time();};
$SIG{INT} =sub {$client->{log}->info('INT '.$client->{wait_resp_count});  $client->{terminated}=time();};
$SIG{TERM}=sub {$client->{log}->info('TERM '.$client->{wait_resp_count}); $client->{terminated}=time();};
exit $client->run();
