#!/usr/bin/perl
#######################################################
#  
#  Decoder
#  
#######################################################

use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use IO::File;
use IO::Select;
use Log::Log4perl qw(get_logger);
use Disp::Recode qw(decode_sms);
use Disp::AltDB;
use Disp::Utils qw(yell);
use Disp::Config qw(param);
use Disp::ConcatList;

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $sms;
my $terminated = 0;
my $print_list = 0;
my $sended_count = 0;
my ($log, $db);
my ($sel_insms, $del_insms, $ins_inbox_arh, $ins_inbox);
my $list = new Disp::ConcatList();

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);

#######################################################
#  Logging preparations
#######################################################

my $notify_on_concat_error = Config->param('notify_on_concat_error','decoder');
my %log_conf = (
   "log4perl.rootLogger"       => Config->param('log_level','decoder').', Logfile',
   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
	"log4perl.appender.Logfile.recreate" => 1,
	"log4perl.appender.Logfile.recreate_check_interval" => 300,
	"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
	"log4perl.appender.Logfile.umask" => "0000",
   "log4perl.appender.Logfile.filename" => Config->param('log_filename','decoder'),
   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
   "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','decoder')
    );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};
$SIG{HUP}=sub {$log->info('HUP'); $print_list=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################

	$db = new Disp::AltDB();
	my $smsc_to_use7bit = $db->get_smsc_use7bit();
	my $smsc_to_ussd_type = $db->get_smsc_ussd_type();
	my $smsc_to_client_type = $db->get_smsc_client_type();

#######################################################
#  Main loop
#######################################################

	$log->info('Started.');
	my $max_id=0;

	while (!$terminated) {
		$log->debug("Getting messages.");
		my $after_id = ($sended_count) ? 0 : $max_id;
		my $smss = $db->get_sms_ready_to_decode($after_id);
   	
		$sended_count = 0;
		my $sms_count = scalar keys(%$smss);
		if ($sms_count) {
			$log->info("$sms_count message(s) fetched. (after $after_id)");
			foreach (sort {$a <=> $b} keys(%$smss)) {
				$sms = $smss->{$_};
				$max_id = $sms->{id} if ($sms->{id}>$max_id);
				$sms->{use7bit} = $smsc_to_use7bit->{$sms->{smsc_id}} & 0x7f;
				$sms->{gsm_alphabet} = $smsc_to_use7bit->{$sms->{smsc_id}} & 0x80;
				$sms->{ussd_type} = $smsc_to_ussd_type->{$sms->{smsc_id}};
				$sms->{client_type} = $smsc_to_client_type->{$sms->{smsc_id}};

				decode_sms($sms);
				if (($sms->{sar_segment_seqnum} > 0) and ($sms->{sar_total_segments} >= $sms->{sar_segment_seqnum})) {
					my $msg_id = $list->add($sms);
#					$log->debug(Dumper($list->{list})); ######################
					process_msg($msg_id) if ($list->is_ready($msg_id));
				}
				else {
						yell(Dumper($sms),
								code => 'ERROR', process => 'DECODER',
								header => "DECODER: Wrong concatenation parameters",
								ignore => {log4perl => 1});
						my $id = $db->move_insms_to_inbox_bad($sms);
						$log->error("Message MSG$sms->{id} wasn't decoded. From:$sms->{abonent} To:$sms->{num}\n".Dumper($sms));
				}
			}
		}
		foreach ($list->get_ready(180)) {
			my $msg_id = $_;
			$sms = $list->msg($msg_id);
			$list->msg($msg_id)->{msg_type} .= '+bad';
			my $msgdump = Dumper($list->msg($msg_id));
			my $id = process_msg($msg_id);
			yell(" https://asg.alt1.ru/?module=findmsg&action=inboxinfo&inbox_id=$id \n $msgdump",
					code => 'WARNING', process => 'DECODER',
					header => "DECODER: Concatenation error",
					ignore => {log4perl => 1, 'e-mail' => !$notify_on_concat_error}); #	if ($list->is_very_bad($msg_id));
		}
		if ($print_list) {
			$log->info("*****LIST*****\n".Dumper($list->{list})."\n*****LIST*****");
			$print_list = 0;
		}
		sleep 1 unless ($sended_count);
	}

};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();

###############################################

sub process_msg {
	my ($msg_id) = @_;
	$list->concat($msg_id);
	my $msg = $list->msg($msg_id);
	my $id = $db->move_insms_to_inbox($msg);
	$log->debug(Dumper($msg));
	$log->info("Message MSG$id was decoded. From:$msg->{1}{abonent} To:$msg->{1}{num} Text:$msg->{msg}");
	$list->del($msg_id);
	$sended_count++;
	return $id;
}

########### DO_EXIT ###########################

sub do_stop
{
   if ($db)   {
      $db->finish();   }

   $log->info("Stopped.\n\n\n");
}

sub do_die
{
   my ($str)=@_;
   $str .= "\n ".Dumper($sms) if (defined $sms);
   $log->fatal($str);
	yell($str,code => 'FATAL',process => 'DECODER',
		header => "DECODER: $str",
		ignore => { log4perl => 1  });
   do_stop;
   exit(0);
}



