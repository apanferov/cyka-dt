#!/usr/bin/php
<?php

// vim: tabstop=4

class Worker
{
	function __construct($INDB, $OUTDB, $mails)
	{
		$this->indb = new rdb($INDB);
		$this->outdb = new rdb($OUTDB);
		$this->system_root = $INDB[system_root];
		$this->log_file = "$INDB[system_root]/log/ELT_internal_custom_log.log";
		$this->err_log_file = "$INDB[system_root]/log/ELT_internal_custom_errors.log";
		$this->mails = $mails;
		$this->mail_title = array("From"=>"ELT_internal_custom");
		$this->log("Constructing completed.");
		$this->count_records_limit = 10000; // maximum records is retrieving from source database in one select query.
		$this->sleep_time = 2; // sleep time seconds!


		//
		// Loading num_prices
		//
		$query = "SELECT CONCAT(operator_id, num) as id, operator_id, num, mt FROM set_real_num_price";
		$this->arrMt = $this->indb->SelectAll2($query, 'id');
		if (!count($this->arrMt))
		{
			$err_description = $this->indb->dberror();
			if ($err_description)
			{
				$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
				$this->w($err_msg);
				asgYell($err_msg, "ELT_internal_custom ERROR: database source error", $this->mails, $this->mail_title);
				sleep($this->sleep_time);
				exit;
			}
		}
	}
	function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}
	//! \brief Error output.
	function errorExit($s)
	{
		//error_log (date("[Y-m-d H:i:s] ")." EXIT: $s\n",3,$this->log_file);
	    exit;
	}	
	//! \biref Standart messages output.
	function log($s)
	{
		error_log(date("[Y-m-d H:i:s] ")." $s\n",3,$this->log_file);
	}
	//! \brief Warnings output.
	function w($s)
	{
		error_log(date("[Y-m-d H:i:s] ")."$s\n",3,$this->err_log_file);
	}
	//! \brief ������ ����������.
	function run()
	{
		$this->log("STARTING...");
		//sleep(1000000000000000000);
		$start_time = $this->getmicrotime();
		while (1)
		{
			$executing_time = $this->getmicrotime() - $start_time;
			if ($executing_time >= 300) // 5 minutes
			{
				$this->errorExit("DAEMON RESTART");
			}

			//
			// ALGORITHM:
			// ----------
			// 1) (E)xtract (retrieving from source) external record.
			// 2) (L)oad record (loading into sync).
			// 3) (T)ransform data when loading.
			//
			// WARNING: all work in auto_transaction mode. It's not error! (but repeat might clear download payments)
			//

			//
			// (1)
			//
			$this->records = array();
			$query = "SELECT * FROM tr_billing WHERE cyka_processed=0 and date>'20111215' limit ".$this->count_records_limit;
			// executing
			$start_timestamp = $this->getmicrotime();
			$this->records = $this->indb->selectAll($query);
			// log
			$execute_time = $this->getmicrotime() - $start_timestamp;
			// work
			if (!($this->records))
			{
				$this->log("SLEEPING...");
				sleep($this->sleep_time);

				//
				// ���� ������, �� ��������� � �������
				//
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					/*mail($this->mails,
						 "ELT_internal_custom ERROR: database source error",
						 $err_msg,
						 $this->mail_title);*/
					asgYell($err_msg, "ELT_internal_custom ERROR: database source error", $this->mails, $this->mail_title);
					exit;
				}
				continue;
			}
			else 
			{
				$this->log("RETRIVING TIME FOR ".count($this->records)." EXTERNAL CUSTOMS IS $execute_time.");
			}
			
			//
			// (2)
			//
			$this->log("\nMOVING RECORDS:".count($this->records));
			$start_timestamp = $this->getmicrotime();
			foreach($this->records as $record)
			{
				// escape
				$tmp_record = array();
				foreach ($record as $k=>$v)
				{
					$tmp_record[$k] = $this->outdb->escape($v);
				}
				$record = $tmp_record;
				//
				// (3)
				//
				$mt = $this->arrMt[$record['operator_id'].$record['num']]['mt'];
				do
				{
					$operator_id = $record['operator_id'];
					$num         = $record['num'];

					if (
						10  != $record['test_flag'] and
						123 != $operator_id and // tatamia
						124 != $operator_id and // simperia
						194 != $operator_id and // mts-111
						202 != $operator_id and // ����� ���
                        !((1==$record['dir']) && $mt) and
						(
							(
								1   == $record['dir']         and
								0    < $record['insms_count']
							) 
							or
							(
								2   == $record['dir']         and
								0    < $record['outsms_count'] and
								0    < $mt
							)
                          )
					   )
					{
                        // �������� ��������������� ������ �� inbox/outbox
						if (1 == $record['dir']) 
						{
							$query = "SELECT msg FROM tr_inboxa WHERE id=".$record['inbox_id'];
						}
						else 
						{
							$query = "SELECT msg FROM tr_outboxa WHERE id=".$record['outbox_id'];
						}
						$inbox = $this->indb->SelectRow($query);
						if (0 == count($inbox))
						{
							$this->log("Inbox row ".$record['inbox_id']." wasn't finding. ");
							/*mail($this->mails,
								 "ELT_internal_custom ERROR: Inbox row wasn't finding.",
								 print_r($record, true), 
								 $this->mail_title);*/
							asgYell(print_r($record, true), "ELT_internal_custom ERROR: Inbox row wasn't finding.", $this->mails, $this->mail_title);
							break;
						}
						// ��������� ������ ��� ������ � �������
						$query = "REPLACE INTO cyka_payment ";
                        // INBOX_ID
						if ($mt)
						{
							if (0 == (int)$record['push_id'])
							{
								$query .= "SET inbox_id='{$record['outbox_id']}',";
							}
							else
							{
								$query .= "SET inbox_id='{$record['push_id']}',";
							}
						}
						else
						{
							$query .= "SET inbox_id='{$record['inbox_id']}',";
						}
                        // TRANSPORT_TYPE
						if (1 == $record['dir'])
						{
							$query .= "transport_type=0,";
						}
						else
						{
							if (0 == (int)$record['push_id'])
							{
								$query .= "transport_type=8,";
							}
							else
							{
								$query .= "transport_type=1,";
							}
						}
						$query .= 	 "date='{$record['date']}',".
									 "service_id='{$record['service_id']}',";
						if ($record['subservice_id'])
						{
							$query .="subservice_id='{$record['subservice_id']}',";
						}
						$query .= 	 "abonent='{$record['abonent']}',".
									 "operator_id='{$record['operator_id']}',".
									 "num='{$record['num']}',".                          // ������� ����� ���������� �������
									 "msg='".$this->outdb->escape($inbox['msg'])."',".   // �������!
									 "in_count=1,".
									 "out_count=0,".
									 "measure='sms',".
									 "test_flag='{$record['test_flag']}',".
									 "cyka_processed=0,".
									 "is_error=1,".                                      // �������� �� error_code=1
									 "is_ignore=0,";
                        // ���������� �������� ��� mt-��������� ��� push_id (������� ������������ $record['transport_type']==8 + �������=0 ?)
						if ((2 == $record['dir']) && $mt && (0 == (int)$record['push_id']) && (0 == (int)$record['partner_id']))
						{
							$q = "SELECT partner_id FROM tr_billing WHERE inbox_id='{$record['inbox_id']}' and outbox_id=0 and dir=1 LIMIT 1";
							$p_id = $this->indb->selectField($q, 'partner_id');
							$p_id = (int)$p_id;
							$query .= "partner_id='{$p_id}',";
						}
						else
						{
							$query .= "partner_id='{$record['partner_id']}',";
						}
						$query .=	 "is_fraud='{$record['is_fraud']}'";
						$this->outdb->query($query);
						$err_description = $this->outdb->dberror();
						if ($err_description)
						{
							$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
							$this->w($err_msg);
							asgYell($err_msg, "ELT_internal_custom WARNING: database sync error", $this->mails, $this->mail_title);
							sleep( $this->sleep_time );
							exit;
						}
					}
					else
					{
						$this->log("Skip record inbox_id='".$record['inbox_id']."' and outbox_id='".$record['outbox_id']."'");
					}
				}
				while (0);

				// update source
				$query = "UPDATE tr_billing 
						  SET cyka_processed=1 
						  WHERE inbox_id='{$record['inbox_id']}'
						  and outbox_id='{$record['outbox_id']}'
						  and insms_count='{$record['insms_count']}'
						  and outsms_count='{$record['outsms_count']}'";
				$this->indb->query($query);
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					/*mail($this->mails,
						 "ELT_internal_custom WARNING: database sync error",
						 $err_msg,
						 $this->mail_title);*/
					asgYell($err_msg, "ELT_internal_custom WARNING: database sync error", $this->mails, $this->mail_title);
					sleep( $this->sleep_time );
					exit;
				}
			} // END OF : foreach($this->records as $record)
			
			$execute_time = $this->getmicrotime() - $start_timestamp;
			$this->log("MOVING TIME FOR ".count($this->records)." EXTERNAL CUSTOMS IS $execute_time.");
		
		} // END OF : while(1)
	
	} // END OF : Worker::run()
};	

include_once('../htdocss/config.php');
include_once('../htdocss/rdb.php'); 

// Mail adresses:
$mails = 'v.islamov@alt1.ru,a.lebedeff@alt1.ru';

$cp = new Worker($DB, $DB, $mails);
$cp->run();
?>
