#!/usr/bin/php -q 
<?php

// vim: tabstop=4

chdir(realpath(dirname(__FILE__)."/../htdocss"));
include_once('config.php');
include_once('db.php');

if ($argc > 2) {
	print "Usage: php ".__FILE__." [date]\n";
	exit;
// ������ �� ��������� ����
} elseif ($argc == 2 && (is_numeric($now=$argv[1]) || $now=strtotime($argv[1]))) {
	$days_after=0;
// ������ �� ������� ����
} else {
	if (date('H') < 15)
		$now = time();
	else
		$now = time()+86400;
	$days_after=7;
}
print "Update by date ".date("Y-m-d", $now)."\n";

$db = new db($DB);
$names = array_keys(db::selectAll2("select distinct upper(currency) as currency from set_real_num_price;","currency"));
updateKurs($names, $now, $days_after);

$deltas = db::selectAll2("select upper(t1.currency) as currency, 
								 t1.price/t2.price as delta,
								 t1.price as price 
						  from set_exchange_rate t1, 
						  	   set_exchange_rate t2 
						  where t1.currency=t2.currency and 
--						  		t1.date=curdate() and 
--								t2.date=date_sub(curdate(),interval 1 day)
								t1.date=from_unixtime($now) and
								t2.date=date_sub(from_unixtime($now),interval 1 day)
						  having delta between 0.9 and 1.1","currency");

$res .= date("d.m.Y H:i")."\n";
$subj = "CURRENCY SETUP FROM CENTRAL BANK SITE";
foreach($names as $nm)
{
	if($d=$deltas[$nm]) 
	{
		$res.=sprintf("%s=%' 2.6f\tchange=%' 1.6f\tOK\n",$nm,$d[price],$d[delta]);
	}
	else 
	{
		//$res.= "$nm=$d[price]\t\tchange=$d[delta]\t\t\tmay be wrong\n";
		$res.=sprintf("%s=%' 2.6f\tchange=%' 1.6f\tOK\n",$nm,$d[price],$d[delta]);
		$subj='CURRENCY ALERT, PLEASE CHECK IT!';
	}
}
//print $res;

//asgYell($res,$subj);

// ----------------------------

function processBloomberg($rates, $names, $timestamp)
{
	// Open CURL session:
	$ch = curl_init('http://www.bloomberg.com/markets/currencies/currency-converter/');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($ch);
	curl_close($ch);
//	printf("%s\n",$data);
	foreach($names as $name) {
		if (!isset($rates[$name])) {
			$pattern = sprintf("/price\\['%s:CUR'\\] = ([\\.0-9]+);/",$name);
//			printf($pattern."\n");
			preg_match($pattern,$data,$matches);
//			print($matches[1]);
			$rates[$name] = $rates[USD]/$matches[1];
		}
	}
	return $rates;
}

function processOpen($rates, $names, $timestamp)
{
	// Required file, could be e.g. '/historical/2011-01-01.json' or 'currencies.json'
	$filename = "/historical/".date("Y-m-d", $timestamp-86400).".json";
	printf($filename."\n");

	// Open CURL session:
	$ch = curl_init('http://openexchangerates.org/' . $filename);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	// Get the data:
	$json = curl_exec($ch);
	curl_close($ch);

	// Decode JSON response:
	$exchangeRates = json_decode($json);

	// You can now access the rates inside the parsed object, like so:
	foreach($names as $name) {
		if (!isset($rates[$name])) {
			$rates[$name] = $rates[USD]/$exchangeRates->rates->{$name};
			printf("1 %s in %s: %s (as of %s)\n",$exchangeRates->base,$name,$exchangeRates->rates->{$name},date('h:i jS F, Y', $exchangeRates->timestamp));
		}
	}
	return $rates;
}

function processCbr($names, $timestamp)
{
//	print "processCbr\n";
	$rates = array();
	$date = date("d/m/Y", $timestamp);
	$doc = new DOMDocument("1.0");
	$doc->loadHTMLFile("http://www.cbr.ru/currency_base/D_print.asp?date_req={$date}");
	if ($doc) {
		$s = simplexml_import_dom($doc);
		//$trs = $s->xpath('//tr[@bgcolor="#ffffff"]');
		$trs = $s->xpath('//tr');
		foreach($trs as $node) {
			$name = substr( $node->td[1],-3 );
			if ( in_array($name,$names) ) {
				$price = (float)( str_replace(',','.',$node->td[4]) );
				$scale = (float)( $node->td[2] );
				$value = $price/$scale;
				$rates[$name] = $value;
//				print "$name=$value\n";
			}
		}
		
	}
	return $rates;
}

function updateKurs($names, $timestamp, $days_after)
{
	$rates = processCbr($names, $timestamp);
	$rates["RUR"] = 1;
	$rates = processBloomberg($rates, $names, $timestamp);
	print_r($rates);
	
	foreach($rates as $name => $rate) {
		if ($rate > 0) {
			for ($i=0;$i<=$days_after;++$i)	{
				db::query("REPLACE INTO set_exchange_rate SET currency=lower('$name'),price=$rate,date=date_add(FROM_UNIXTIME($timestamp), interval $i day)");
			}
		}
	}

}
