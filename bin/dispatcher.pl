#!/usr/bin/perl -w
use strict qw(vars);
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;
use Log::Log4perl;
use Disp::Config;
use Disp::Utils qw(yell);


Log::Log4perl->init("$FindBin::Bin/../etc/log.conf");
Config::init(config_file => "$FindBin::Bin/../etc/asg.conf");

my $Host = `hostname -f`;
(@ARGV && $ARGV[0] eq 'worker')
	? &worker(@ARGV)
	: &master();

###

sub worker
{
	eval
	{
		yell("Dispatcher worker [$$] is starting on $Host",
			code    => 'PROC_START',
			process => 'DISPATCHER WORKER',
			header  => "Dispatcher worker is starting on $Host"
			);

		require Disp::Daemon;
		Disp::Daemon
			->new(@_)
			->run;

		yell("Dispatcher worker [$$] is finished on $Host",
			code    => 'PROC_STOP',
			process => 'DISPATCHER WORKER',
			header => "Dispatcher worker is finished on $Host"
			);
	};
	if ($@)
	{
		yell($@,
			code    => 'PROC_STOP',
			process => 'DISPATCHER WORKER',
			header  => 'Dispatcher abnormal termination'
			);
	}
}

sub master
{
	eval
	{
		yell("Dispatcher master [$$] is starting on $Host",
			code    => 'PROC_START',
			process => 'DISPATCHER MASTER',
			header  => "Dispatcher master is starting on $Host"
			);

		require Disp::Daemon::Master;
		Disp::Daemon::Master->run;

		yell("Dispatcher master [$$] is finished on $Host",
			code    => 'PROC_STOP',
			process => 'DISPATCHER MASTER',
			header => "Dispatcher master is finished on $Host"
			);
	};
	if ($@)
	{
		yell($@,
			code    => 'PROC_STOP',
			process => 'DISPATCHER MASTER',
			header  => 'Dispatcher abnormal termination'
			);
	}
}

__END__
