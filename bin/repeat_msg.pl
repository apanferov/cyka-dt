#!/usr/bin/perl -w
use strict;
use warnings;
use Data::Dumper;

use lib '../lib';
use Disp::AltDB;
use Disp::Config qw(param);

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 0);
my $db = new Disp::AltDB();

#print Dumper($db);
#exit;

my @inbox_ids = @{$db->{db}->selectcol_arrayref(<<EOF)};
select id as inbox_id
from tr_inboxa
where
date >= '2016-11-25 06:45' and date < '2016-12-06 18:35 ' and
num = '2316'
EOF

my $line_num = 0;
foreach my $inbox_id (@inbox_ids) {
	$line_num++;
	print "$inbox_id ($line_num/".scalar(@inbox_ids).")\n";
	$db->{db}->do("select repeat_msg($inbox_id);");
	sleep 1;
}

print "Done\n";

__END__
