#!/usr/bin/perl
# -*- coding: utf-8 -*-
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Disp::AltDB;
use Replication::TailWithState;
use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use POSIX ":sys_wait_h";
use Time::HiRes qw(gettimeofday tv_interval usleep);

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $terminated = 0;

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");
my $process_name='REPLICATION';

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
				"log4perl.rootLogger" => Config->param('log_level',lc($process_name)).', Logfile',
				"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
				"log4perl.appender.Logfile.recreate" => 1,
				"log4perl.appender.Logfile.recreate_check_interval" => 300,
				"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
				"log4perl.appender.Logfile.umask" => "0000",
				"log4perl.appender.Logfile.filename" => Config->param('log_filename',lc($process_name)),
				"log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
				"log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern',lc($process_name))
			   );
Log::Log4perl->init( \%log_conf);
my $log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$terminated=1;};
$SIG{INT} =sub {$terminated=1;};
$SIG{TERM}=sub {$terminated=1;};

my $pids = {};
eval {
	my $source_host = Config->param('local_db_host');
	foreach my $dest_host (split(/\s*,\s*/,Config->param('destination_hosts','replication'))) {
		my $thread_conf = {source_db_conf => (Config->param($source_host,'db_transport') || Config->param($source_host,'db_billing')),
						   dest_db_conf => (Config->param($dest_host,'db_transport') || Config->param($dest_host,'db_billing')),
						   tables => Config->param($dest_host,'replication') };
		$thread_conf->{position} = new Replication::Position("$Replication::TailWithState::dir/$thread_conf->{dest_db_conf}{host}.pos");

		my $pid = run_thread($thread_conf);
		die "Can't fork" unless ($pid > 0);
		$pids->{$pid} = $thread_conf;
	}
	$log->info('Started.');
	while (!$terminated)	{
		my $kpid = waitpid(-1, WNOHANG);
		if ($kpid > 0) {
			my $thread_conf = $pids->{$kpid};
			delete($pids->{$kpid});
			my $pid = run_thread($thread_conf);
			die "Can't fork" unless ($pid > 0);
			$pids->{$pid} = $thread_conf;
		}
		else {
			select(undef,undef,undef,1);
			cleanup();
		}
	}
};
do_die($@) if ($@);
do_stop();

#######################################################
#  Internal Functions
#######################################################

sub run_thread {
	my ($conf) = @_;
	my $pid = fork;
	return $pid if $pid;

	my $tables = {};
	foreach my $table (split(/\s*,\s*/,$conf->{tables})) {
		$tables->{$table} = 1;
	}

	my ($source_db, $dest_db, $file);
	eval {
		$source_db = new Disp::AltDB($conf->{source_db_conf});
		$dest_db = new Disp::AltDB($conf->{dest_db_conf});
		my $last_request_time = time;

		$file = new Replication::TailWithState("$conf->{dest_db_conf}{host}.pos");
		my $pos = $file->pos;

		$log->info("Replication to $conf->{dest_db_conf}{host} started.");

		my $replication_tab = { INSERT => sub {
									my ($table,$action,$condition) = @_;
									my $row = $source_db->{db}->selectrow_hashref("select * from $table where $condition lock in share mode");
									if ($row) {
										$dest_db->do("replace into $table set ".join(",", map { "$_ = ?" } keys(%$row)),undef,values(%$row));
										return 0;
									}
									return "INSERT failed. Record not found.";
								},
								INSERTIGN => sub {
									my ($table,$action,$condition) = @_;
									my $row = $source_db->{db}->selectrow_hashref("select * from $table where $condition lock in share mode");
									if ($row) {
										####### HACK FOR A1Systems
										if (($table eq 'tr_num_map') and ($dest_db->{conf}{name} eq 'alt6')) {
											delete $row->{transport_type};
											delete $row->{smsc_id};
										};
										#######
										$dest_db->do("insert ignore into $table set ".join(",", map { "$_ = ?" } keys(%$row)),undef,values(%$row));
										return 0;
									}
									return "REPLACE failed. Record not found.";
								},
								REPLACE => sub {
									my ($table,$action,$condition) = @_;
									my $row = $source_db->{db}->selectrow_hashref("select * from $table where $condition lock in share mode");
									if ($row) {
										####### HACK FOR A1Systems
										if (($table eq 'tr_num_map') and ($dest_db->{conf}{name} eq 'alt6')) {
											delete $row->{transport_type};
											delete $row->{smsc_id};
										};
										#######
										$dest_db->do("replace into $table set ".join(",", map { "$_ = ?" } keys(%$row)),undef,values(%$row));
										return 0;
									}
									return "REPLACE failed. Record not found.";
								},
								DELETE => sub {
									my ($table,$action,$condition) = @_;
									my $row = $source_db->{db}->selectrow_hashref("select * from $table where $condition lock in share mode");
									unless ($row) {
										$dest_db->do("delete from $table where $condition");
										return 0;
									}
									return "DELETE failed. Record was not deleted.";
								}
							  };
		while (!$terminated) {
			$pos = $file->pos;
			while (my $line = $file->get_line) {
				$log->debug("$file->{repl_filename} ($pos) => $line");
				my ($date,$table,$action,$condition) = $line =~ /^(.*)\t(.*)\t(.*)\t(.*)$/;
				if ($tables->{$table} and $table and $action and $condition and defined $replication_tab->{uc($action)}) {
					my $res = &{$replication_tab->{uc($action)}}($table,$action,$condition);
					if ($res) {
						$log->warn("$file->{repl_filename}:$pos ($line) $res.");
					}
					else {
						$log->debug("$file->{repl_filename} ($pos) done.");
					}
				}
				else {
					$log->debug("$file->{repl_filename} ($pos) skipped.");
				}
				$pos = $file->save_position;
				last if $terminated;
			}
			select(undef,undef,undef,1);
			if ($last_request_time+60 < time) {
				$source_db->ping();
				$dest_db->ping();
				$last_request_time = time;
			}
		}
	};
	if ($@) {
		$log->fatal($@);
		$source_db->finish if $source_db;
		$dest_db->finish if $dest_db;
		$file->finish if $file;
	}
	$log->info("Replication to $conf->{dest_db_conf}{host} stopped.");
	exit(0);
}

sub cleanup {
	my @dates;
	my $min_date = 0xFFFFFFFF;
	foreach my $pid (keys(%$pids)) {
		my ($filename,$pos) = $pids->{$pid}{position}->load_position;
		my $date = Replication::TailWithState::filename_to_date($filename);
		$min_date = $date if ($min_date > $date);
	}
	my @files = glob("$Replication::TailWithState::dir/repl-????-??-??-GMT.log") ;
	foreach my $filename (@files) {
		my ($f) = $filename =~ /(repl-\d{4}-\d{2}-\d{2}-GMT\.log)/;
		if (Replication::TailWithState::filename_to_date($f) < $min_date) {
			rename($filename,"$filename.old");
			$log->info("Replication file $filename deleted.");
		}
	}
}

########### DO_EXIT ###########################

sub do_stop
{
	$log->info("Stopping...");
	kill('TERM', keys(%$pids));
	while (%$pids)	{
		my $kpid = waitpid(-1, WNOHANG);
		delete($pids->{$kpid}) if ($kpid > 0);
		select(undef,undef,undef,0.1);
	}
	$log->info("Stopped.\n\n\n");
}

sub do_die
{
	my ($str)=@_;
	$log->fatal($str);
#	yell($str,code => 'FATAL',process =>$process_name,
#		 header => "$process_name: $str",
#		 ignore => { log4perl => 1  });
	do_stop;
	exit(0);
}



1
