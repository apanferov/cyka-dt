#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use Disp::Config;
use Disp::Daemon;
use Disp::Utils;
use Disp::MTTLookup;
use Disp::NavisimStats;
use Iterator_Utils ':all';
use IO::Scalar;

sub main {
	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg.conf");
	my $f = new Disp::NavisimStats('83.222.6.227', 'stat', 'a1stat');
	$f->process_stats('/home/stat/stat', '');
}

eval {
	main();
};

if ($@) {
	die "Unhandled: $@";
}
