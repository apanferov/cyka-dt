#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use Log::Log4perl qw(get_logger);
use Disp::AltDB;
use Disp::Utils qw(yell dumper_load dumper_save);
use Disp::Config qw(param);
use Redis;
#use POSIX;

my $terminated = 0;
my ($log,$db,$r,$params);

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "../etc/asg.conf");

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
   "log4perl.rootLogger"       => Config->param('log_level','redis2cpe').', Logfile',
   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
   "log4perl.appender.Logfile.recreate" => 1,
   "log4perl.appender.Logfile.recreate_check_interval" => 300,
   "log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
   "log4perl.appender.Logfile.umask" => "0000",
   "log4perl.appender.Logfile.filename" => Config->param('log_filename','redis2cpe'),
   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
   "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','redis2cpe')
    );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 

$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################

#	$db = new Disp::AltDB();

	$r = Redis->new(server => Config->param('server','redis'), encoding => undef);

#######################################################
#  Main loop
#######################################################

	$log->info('Started.');
	while (!$terminated) {
		my $params_str = $r->rpop('subs_queue');
		if ($params_str) {
			my $params = dumper_load($params_str);
			$log->debug('REDIS DATA '.Dumper($params));
#			add_payment($params) if ($params->{partner_id});
			last unless ($params);
		} else {
      sleep 1;
    }
	}

};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();

sub add_payment {
	my ($params) = @_;

#	my $date = $params->{'time'} || POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
#	$date =~ s/\,/ /;
	my $data = {
		transport_type => ($params->{transport_type}) ? $params->{transport_type} : 3,
		date => $params->{date}, # $date
		service_id => $params->{service_id},
		abonent => $params->{abonent},
		operator_id => $params->{operator_id},
		num => $params->{num},
		partner_id => $params->{partner_id},
		cyka_processed => ($params->{cyka_processed}) ? $params->{cyka_processed} : 0,
		test_flag => ($params->{test_flag}) ? $params->{test_flag} : 0,
		comment => ($params->{comment}) ? $params->{comment} : '',
		is_error => ($params->{is_error}) ? $params->{is_error} : 1,
	};

	my @fields = qw/transport_type date service_id abonent operator_id num partner_id cyka_processed test_flag comment is_error/;
	my $query = "replace into cyka_payment_ext (".join(", ", @fields).") values (".join(", ", ('?') x (@fields)).")";
	my @values = map { $data->{$_} } @fields;
	$log->debug("CYKA SAVE ".Dumper($data));
	eval {
		Config->dbh->do($query, undef, @values);
		$log->debug("CYKA SAVE OK");
	};
	$log->warn("CYKA SAVE ERROR $@") if ($@);
}

########### DO_EXIT ###########################

sub do_stop
{
#	$db->finish if ($db);
	$r->quit if ($r);

	$log->info("Stopped.\n\n\n");
}

sub do_die
{
	my ($str)=@_;
	$log->fatal($str);
	yell(
		$str,
		code => 'FATAL',
		process => 'REDIS2CPE',
		header => "REDIS2CPE: $str",
		ignore => { log4perl => 1 }
	);
	do_stop;
	exit(0);
}

