#!/usr/bin/php
<?php

// vim: tabstop=4

class Worker
{
	function __construct($INDB, $OUTDB, $mails)
	{
		$this->indb = new rdb($INDB);
		$this->outdb = new rdb($OUTDB);
		$this->system_root = $INDB[system_root];
		$this->log_file = "$INDB[system_root]/log/ELT_external_custom_log.log";
		$this->err_log_file = "$INDB[system_root]/log/ELT_external_custom_errors.log";
		$this->mails = $mails;
		$this->mail_title = array("From"=>"ELT_external_custom");
		$this->log("Constructing completed.");
		$this->count_records_limit = 1000; // maximum records is retrieving from source database in one select query.
		$this->sleep_time = 2; // sleep time seconds!
	}
	
	function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}
	//! \brief Error output.
	function errorExit($s)
	{
		//error_log (date("[Y-m-d H:i:s] ")." EXIT: $s\n",3,$this->log_file);
	    exit;
	}	
	//! \biref Standart messages output.
	function log($s)
	{
		//error_log(date("[Y-m-d H:i:s] ")." $s\n",3,$this->log_file);
	}
	//! \brief Warnings output.
	function w($s)
	{
		error_log(date("[Y-m-d H:i:s] ")."$s\n",3,$this->err_log_file);
	}
	//! \brief ������ ����������.
	function run()
	{
		$this->log("STARTING...");
		//sleep(1000000000000000000);
		$start_time = $this->getmicrotime();
		while (1)
		{
			$executing_time = $this->getmicrotime() - $start_time;
			if ($executing_time >= 300) // 5 minutes
			{
				$this->errorExit("DAEMON RESTART");
			}

			//
			// ALGORITHM:
			// ----------
			// 1) (E)xtract (retrieving from source) external record.
			// 2) (L)oad record (loading into sync).
			// 3) (T)ransform data when loading.
			//
			// WARNING: all work in auto_transaction mode. It's not error! (but repeat might clear download payments)
			//

			//
			// (1)
			//
			$this->records = array();
			$query = "SELECT * FROM cyka_payment_ext WHERE cyka_processed=0 and date>date_sub(now(), interval 10 day) LIMIT {$this->count_records_limit}";
			// executing
			$start_timestamp = $this->getmicrotime();
			$this->records = $this->indb->selectAll($query);
			// log
			$execute_time = $this->getmicrotime() - $start_timestamp;
			// work
			if (!($this->records))
			{
				$this->log("SLEEPING...");
				sleep($this->sleep_time);

				//
				// ���� ������, �� ��������� � �������
				//
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					asgYell($err_msg, "ELT_external_custom ERROR: database source error", $this->mails, $this->mail_title);
					exit;
				}
				continue;
			}
			else 
			{
				$this->log("RETRIVING TIME FOR ".count($this->records)." EXTERNAL CUSTOMS IS $execute_time.");
			}
			
			//
			// (2)
			//
			$this->log("\nMOVING RECORDS:".count($this->records));
			$start_timestamp = $this->getmicrotime();
			foreach($this->records as $record)
			{
				// escape
				$tmp_record = array();
				foreach ($record as $k=>$v)
				{
					$tmp_record[$k] = $this->outdb->escape($v);
				}
				$record = $tmp_record;
				//
				// (3)
				//
				// Check transport_type
				if ($record['transport_type'] < 1)
				{
					$query = "UPDATE cyka_payment_ext 
							  SET cyka_processed=1,is_error=1,error_msg='Incorrect transport_type={$record['transport_type']}' 
							  WHERE id={$record['id']}"; 
					$this->indb->query($query); // Result checking might not use!
					
					// log
					$this->w("WARNING [{$record['id']}]: incorrect transport_type={$record['transport_type']}\n");
					asgYell(print_r($record, true), "ELT_external_custom WARNING [{$record['id']}]: incorrect transport_type={$record['transport_type']}", $this->mails, $this->mail_title);
					continue;
				}

				// check test_flag
				if (0/*0 != $record['test_flag']*/)
				{
					$query = "UPDATE cyka_payment_ext 
							  SET cyka_processed=1,is_error=0  
							  WHERE id={$record['id']}"; 
					$this->indb->query($query); // Result checking might not use!
					
					// log
					$this->log("INFORMATION [{$record['id']}]: test external custom\n");
					asgYell(print_r($record, true), "ELT_external_custom INFORMATION [{$record['id']}]: test external custom", $this->mails, $this->mail_title);
					continue;
				}

				if ($record['transport_type'] == 9)
				{
					$record['measure'] = 'kb';
				}
				else
				{
					$record['measure'] = 'sms';
				}
				
				// insert to sync
				$query = "REPLACE INTO cyka_payment_not_processed ".
						 "SET inbox_id='{$record['id']}',".
							 "transport_type='{$record['transport_type']}',".
							 "date='{$record['date']}',".
							 "service_id='{$record['service_id']}',";
				if ($record['subservice_id']) 
				{
					$query .="subservice_id='{$record['subservice_id']}',";
				}
				$query .= 	 "abonent='{$record['abonent']}',".
							 "operator_id='{$record['operator_id']}',".
							 "num='{$record['num']}',".
							 "msg='{$record['msg']}',".
							 "in_count=1,".
							 "out_count=0,".
							 "measure='{$record['measure']}',".
							 "test_flag='{$record['test_flag']}',".
							 "cyka_processed=0,".
							 "is_error=1,".
							 "is_ignore=0,".
							 "partner_id='{$record['partner_id']}'";
				
				$this->outdb->query($query);
				$err_description = $this->outdb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					asgYell($err_msg, "ELT_external_custom WARNING: database sync error", $this->mails, $this->mail_title);
					sleep( $this->sleep_time );
					exit;
				}

				// update source
				$query = "UPDATE cyka_payment_ext 
						  SET cyka_processed=1,is_error=0  
						  WHERE id={$record['id']}"; 
				$this->indb->query($query);
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					asgYell($err_msg, "ELT_external_custom WARNING: database sync error", $this->mails, $this->mail_title);
					sleep( $this->sleep_time );
					exit;
				}
			} // END OF : foreach($this->records as $record)
			
			$execute_time = $this->getmicrotime() - $start_timestamp;
			$this->log("MOVING TIME FOR ".count($this->records)." EXTERNAL CUSTOMS IS $execute_time.");
		
		} // END OF : while(1)
	
	} // END OF : Worker::run()
};	

include_once('../htdocss/config.php');
include_once('../htdocss/rdb.php'); 

// Mail adresses:
$mails = '';

$cp = new Worker($DB, $DB, $mails);
$cp->run();
?>
