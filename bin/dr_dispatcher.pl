#!/usr/bin/perl
#######################################################
#  
#  Encoder
#  
#######################################################

use strict;
use warnings;
use lib '../lib';
use DBI;
use Data::Dumper;
use Log::Log4perl qw(get_logger);
use Disp::AltDB;
use Disp::Utils qw(yell dumper_load dumper_save);
use Disp::Config qw(param);
use Redis;
use Disp::DownloadManager;

$| = 1;
STDOUT->autoflush;
STDERR->autoflush;

my $terminated = 0;
my ($log,$db,$r,$dm,$services);

#######################################################
#  Loading configuration
#######################################################

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);

#######################################################
#  Logging preparations
#######################################################

my %log_conf = (
   "log4perl.rootLogger"       => Config->param('log_level','dr_dispatcher').', Logfile',
   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
   "log4perl.appender.Logfile.recreate" => 1,
   "log4perl.appender.Logfile.recreate_check_interval" => 300,
   "log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
   "log4perl.appender.Logfile.umask" => "0000",
   "log4perl.appender.Logfile.filename" => Config->param('log_filename','dr_dispatcher'),
   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
   "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','dr_dispatcher')
    );
Log::Log4perl->init( \%log_conf);
$log=get_logger("");
$log->info('Starting...'); 
$SIG{QUIT}=sub {$log->info('QUIT'); $terminated=1;};
$SIG{INT} =sub {$log->info('INT');  $terminated=1;};
$SIG{TERM}=sub {$log->info('TERM'); $terminated=1;};

eval {

#######################################################
#  Connecting to DB
#######################################################

	$db = new Disp::AltDB();
	$services = $db->{db}->selectall_hashref("select * from set_service where dr_delivery>0",'id');
#	$db->finish();
#	$log->debug(Dumper($services));


	$r = Redis->new(server => Config->param('server','redis'), encoding => undef);

	$dm = Disp::DownloadManager->new(max_slots => 1000, tcp_timeout => 5, http_timeout => 20);
	$log->info(Dumper($dm));
	$dm->{success_cb} = \&handle_success;
	$dm->{error_cb} = \&handle_error;



#######################################################
#  Main loop
#######################################################

	$log->info('Started.');
	while (!$terminated or ($dm->{max_slots} != $dm->free_slots)) {
#		$log->debug("Getting DRs.");
		my $count = 0;
		while ($dm->free_slots and !$terminated) {
			my $dr;
#			eval { 
				my $dr_str = $r->rpop('dr_queue');
				if ($dr_str) {
					$dr->{body} = dumper_load($dr_str);
#					$log->debug(Dumper($dr));
					$log->debug("DR ready MSG$dr->{body}{outbox_id}");
					if (my $s = dr_allowed($dr)) {
						add_link($s,$dr);
						$r->hset('dr_queue_temp',"$dr->{body}{outbox_id}-$dr->{body}{status}",$dr_str);
					} else {
						$log->info("DR for MSG$dr->{body}{outbox_id} (status=$dr->{body}{status}) WAS SKIPPED");
					}
				}
#			};
#			if ($@) {
#				yell($@,code => 'FATAL',process => 'DR_DISPATCHER',
#					header => "DR_DISPATCHER: Error",
#					ignore => { log4perl => 1  });
#				last;
#			}
			last unless ($dr);
			$count++;
		}
#		$log->debug("Checking downloads");
		while ( $dm->has_downloads ) {
			$dm->poll(1);
		}
		sleep 1 unless($count);
	}

};
do_die($@) if ($@);
$log->info('Stopping...');
do_stop();

sub dr_allowed {
	my ($dr) = @_;
	my $service_id = $dr->{body}{service_id};
	unless ($service_id) {
		$log->warn("Loading service_id");
		$db->reconnect;
		$service_id = $db->get_value('select service_id from tr_outboxa where id=?',undef, $dr->{body}{outbox_id});
	}
	if (my $s = $services->{$service_id}) {
		return undef unless ($s->{dr_uri});
		return $s if ($s->{dr_delivery} == 1);
		return $s if (($s->{dr_delivery} == 2)and($dr->{body}{status} > 2));
	}
	return undef;
}

sub add_link {
	my ($s,$dr) = @_;
	$dr->{original_message}{id} = $dr->{body}{outbox_id}; # for DownloadManager
	$dr->{original_service}{id} = $s->{id}; # for DownloadManager
	$dr->{url} = $s->{dr_uri};
	$dr->{params} = {operation => 'dr',
		status => status_name($dr->{body}{status})};
	if ($dr->{body}{inbox_id}) {
		$dr->{params}{type} = 'inbox';
		$dr->{params}{inbox_id} = $dr->{body}{inbox_id};
		$dr->{params}{outbox_id} = $dr->{body}{outbox_id};
	} elsif ($dr->{body}{push_id}) {
		$dr->{params}{type} = 'push';
		$dr->{params}{push_id} = $dr->{body}{push_id};
	} else {
		$dr->{params}{type} = 'outbox';
		$dr->{params}{outbox_id} = $dr->{body}{outbox_id};
	}
#	$dr->{params}{push_id} = $dr->{body}{push_id} if ($dr->{params}{push_id});
#	$dr->{params}{paid} = (($dr->{params}{paid}) ? 1 : 0 ) if (defined $dr->{params}{paid});
#	$dr->{params}{transaction_id} = $dr->{body}{inbox_id} if ($dr->{params}{inbox_id});
	$dm->add($dr);
}

sub handle_success {
	my ( $p ) = @_;
	$log->debug(Dumper($p));
#	$log->debug(Dumper($p->{data}));
	$r->hdel('dr_queue_temp',"$p->{link}{body}{outbox_id}-$p->{link}{body}{status}");
	$log->info("DR for MSG$p->{link}{body}{outbox_id} (status=$p->{link}{params}{status}) WAS DELIVERED");
}

sub handle_error {
	my ( $p ) = @_;
	$log->debug(Dumper($p));
#	$log->debug(Dumper($p->{data}));
	$r->hdel('dr_queue_temp',"$p->{link}{body}{outbox_id}-$p->{link}{body}{status}");
	$log->warn("DR for MSG$p->{link}{body}{outbox_id} (status=$p->{link}{params}{status}) WAS NOT DELIVERED $p->{network_error} $p->{error_code} $p->{url}");
}

sub status_name {
	my ( $status ) = @_;
	my $status_map = { 	0 => "PROCESS",
				2 => "ACCEPTED",
				10 => "ACKNOWLEDGED",
				11 => "ACCEPTED",
				12 => "DELIVERED",
				13 => "UNDELIVERED",
				14 => "UNDELIVERED",
				15 => "UNDELIVERED",
				127 => "REJECTED",
				255 => "REJECTED"
		};
	my $status_text = $status_map->{$status} || "UNKNOWN";
	return $status_text;
}

########### DO_EXIT ###########################

sub do_stop
{
   $db->finish if ($db);
   $r->quit if ($r);


   $log->info("Stopped.\n\n\n");
}

sub do_die
{
   my ($str)=@_;
   $log->fatal($str);
	yell($str,code => 'FATAL',process => 'DR_DISPATCHER',
		header => "DR_DISPATCHER: $str",
		ignore => { log4perl => 1  });
   do_stop;
   exit(0);
}

