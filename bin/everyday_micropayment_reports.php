#!/usr/bin/php -q
<?php

// vim: tabstop=4

chdir(realpath(dirname(__FILE__).'/../htdocss'));
include_once('config.php');
include_once('db.php');

function notify_about_the_detail_traffic_everyday_changing($email_title, $target_emails, $report_group_services)
{
  $title = "���������� ����� �� ���������� ������� � ������� �� ������� �������� �������������";
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
	  ) t11
	JOIN set_service_t s1 ON s1.id=t11.service_id ";
	if ($report_group_services) $query .= "and s1.sgroup_id in ($report_group_services) ";
	$query .= "GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
	  ) t22
	JOIN set_service_t s2 ON s2.id=t22.service_id ";
	if ($report_group_services) $query .= "and s2.sgroup_id in ($report_group_services) ";
	$query .= "GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_service_t op,
  set_sgroup_t sg
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.service_id and
  op.sgroup_id=sg.id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT sg.name as service_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>$title<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(������ �������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>������ ��������</td>
					<td>% �� ���� (��� �������������)</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ��������� � ����, %</td>
					<td>��������� ������ �1 � ����, %</td>
					<td>��������� ������ �1 � ���� � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ��������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, $title, $target_emails, $email_title, true);
}

function notify_about_the_traffic_everyday_changing($email_title, $target_emails, $report_group_services)
{
  $title = "���������� ����� �� ���������� ������� � ������� �������������";
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
	JOIN set_service_t s1 ON s1.id=t11.service_id ";
	if ($report_group_services) $query .= "and s1.sgroup_id in ($report_group_services) ";
	$query .= "GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
	JOIN set_service_t s2 ON s2.id=t22.service_id ";
	if ($report_group_services) $query .= "and s2.sgroup_id in ($report_group_services) ";
	$query .= "GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_operator_t op
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.operator_id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	$query1 = "SELECT op.name as operator_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);
	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d1 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	$d2 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	$res .= "<h3>$title<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	$res .= "<h4>(��������� ����������� � ������� �������� ������ �1 � ���������� �� ��������� ����)</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>��������</td>
					<td>% �� ���� (��� �������������)</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ���������, %</td>
					<td>��������� ������ �1, %</td>
					<td>��������� ������ �1 � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>����� �� ���� ����������</b></td>
					<td><b>100.000</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";
		foreach ($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			foreach ($columns as $column=>$value)
			{
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				$res .= "<td $class><b>$value</b></td>";
			}
			$res .= "</tr>\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, $title, $target_emails, $email_title, true);
}

function createQuery ($report_group_services)
{
	$query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(floor(100 * ( sum(t1.insms_count) - sum(t2.insms_count) ) / sum(t2.insms_count)),0) as insms_percent,
  ifnull(floor(100 * ( sum(t1.outsms_count) - sum(t2.outsms_count) ) / sum(t2.outsms_count)),0) as outsms_percent,
  ifnull(floor(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds)),0) as operator_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds)),0) as a1_income_percent,
  ifnull(floor(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds)),0) as a1_clean_income_percent
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date between DATE_SUB(CURDATE() ,INTERVAL 7 DAY) and DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date between DATE_SUB(CURDATE() ,INTERVAL 7 DAY) and DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
	) t11
	JOIN set_service_t s1 ON s1.id=t11.service_id ";
	if ($report_group_services) $query .= "and s1.sgroup_id in ($report_group_services) ";
    $query .= "GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date between DATE_SUB(CURDATE() ,INTERVAL 14 DAY) and DATE_SUB(CURDATE() ,INTERVAL 8 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date between DATE_SUB(CURDATE() ,INTERVAL 14 DAY) and DATE_SUB(CURDATE() ,INTERVAL 8 DAY)
      ) t22
	JOIN set_service_t s2 ON s2.id=t22.service_id ";
	if ($report_group_services) $query .= "and s2.sgroup_id in ($report_group_services) ";
    $query .= "GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_service_t op,
  set_sgroup_t sg,
  set_operator_t oo
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 7 DAY) AND
  op.id=t1.service_id and
  op.sgroup_id=sg.id and
  t1.operator_id=t2.operator_id AND
  t1.operator_id=oo.id AND
  t1.service_id=t2.service_id";

	return $query;
}

function notify_about_the_detail_traffic_everyweek_changing_with_operators($email_title, $target_emails, $report_group_services)
{
  $title = "������������ ����� ����� �� ���������� �������� � ������� �� ������� �������� �������������";
	$query = createQuery($report_group_services);

	$query1 = "SELECT sg.id as sgroup_id, sg.name as sgroup_name,".$query." GROUP BY 2 ORDER BY 3 DESC";
	$query2 = "SELECT ".$query;
	$rt = db::selectAll($query1);
	$all = db::selectRow($query2);

	$res .="<html>
			<head>
				<title></title>
				<style>
					body
					{
						color: #000;
						font-family: Verdana, Arial, Helvetica, sans-serif;
						font-size: 10px;
					}
					.header
					{
						background-color:#ccc;
					}
				</style>
			</head>
			<body>";
	$d11 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-7, date("Y")));
	$d12 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));

	$d21 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-14, date("Y")));
	$d22 = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-8, date("Y")));

	$res .= "<h3>$title<h3><h4>�� ������ [$d11 ... $d12] � ��������� � [$d21 ... $d22]</h4>\n";
	$res .= "<table border=1>
				<tr class=header>
					<td>������ ��������</td>
					<td>��������</td>
					<td>% �� ���� (��� ����� �������������)</td>
					<td>��������� ��������� �������, %</td>
					<td>��������� ���������� �������, %</td>
					<td>��������� ������ ���������, %</td>
					<td>��������� ������ �1, %</td>
					<td>��������� ������ �1 � ������ ��������� �������, %</td>
			 	</tr>\n";
	if ($rt)
	{
		$res .= "<tr class='dark' align='right'>
					<td align='left'><b>�����</b></td>
					<td><b>-</b></td>
					<td><b>100</b></td>
					<td><b>$all[insms_percent]</b></td>
					<td><b>$all[outsms_percent]</b></td>
					<td><b>$all[operator_income_percent]</b></td>
					<td><b>$all[a1_income_percent]</b></td>
					<td><b>$all[a1_clean_income_percent]</b></td>
				</tr>";

		foreach($rt as $columns)
		{
			//if ($cur[percent] <= -50)    $class="style='background-color:#F00'";
			//elseif ($cur[percent] >= 50) $class="style='background-color:#00F'";
			//else $class="";
			$res .= "<tr align='right'>";
			$rowspan = 1;
			foreach ($columns as $column=>$value)
			{
				if ($column == 'sgroup_id')
				{
					// �����������
					$detail = null;
					$rt_detail = array();
					$all_detail = array();

					$query = createQuery($report_without_group_services, $value);
					$query1 = "SELECT oo.name as operator_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
					$rt_detail = db::selectAll($query1);

					$rowspan = count($rt_detail)+1;
					foreach ($rt_detail as $columns_detail)
					{
						$detail .= "<tr align='right'>";
						foreach ($columns_detail as $column_detail=>$value_detail)
						{
							if ($column_detail == 'a1_clean_income__rur_without_nds')
							{
								$detail .= "<td align='right'><b>".sprintf('%.3f',$value_detail * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
								continue;
							}
							if (('insms_percent' == $column_detail) ||
								('operator_income_percent' == $column_detail) ||
								('a1_income_percent' == $column_detail) ||
								('a1_clean_income_percent' == $column_detail)
							   )
							{
								if ($value_detail <= -50) $class="style='color:red'";
								elseif ($value_detail >= 50) $class="style='color:blue'";
								else $class="";
							}
							elseif ('outsms_percent' == $column_detail)
							{
								if ($value_detail <= -50) $class="style='color:blue'";
								elseif ($value_detail >= 50) $class="style='color:red'";
								else $class="";
							}
							else
							{
								$class = "align='left'";
							}
							if ($column_detail != 'sgroup_name')
							{
								$detail .= "<td $class><b>$value_detail</b></td>";
							}
							else
							{
								$detail .= "<td $class rowspan=$rowspan><b>$value_detail</b></td>";
							}
						}
						$detail .= "</tr>\n";
					}
//print_r($detail);
					continue;
				}
				if ($column == 'a1_clean_income__rur_without_nds')
				{
					if ($all['a1_clean_income__rur_without_nds'] == 0)
					{
						$res .= "<td align='right'><b>undef</b></td>";
					}
					else
					{
						$res .= "<td align='right'><b>".sprintf('%.3f',$value * 100 / $all['a1_clean_income__rur_without_nds'])."</b></td>";
					}
					continue;
				}
				if (('insms_percent' == $column) ||
					('operator_income_percent' == $column) ||
					('a1_income_percent' == $column) ||
					('a1_clean_income_percent' == $column)
				   )
				{
					if ($value <= -50) $class="style='color:red'";
					elseif ($value >= 50) $class="style='color:blue'";
					else $class="";
				}
				elseif ('outsms_percent' == $column)
				{
					if ($value <= -50) $class="style='color:blue'";
					elseif ($value >= 50) $class="style='color:red'";
					else $class="";
				}
				else
				{
					$class = "align='left'";
				}
				if ($column != 'sgroup_name')
				{
					$res .= "<td $class><b>$value</b></td>";
				}
				else
				{
					$res .= "<td $class rowspan=$rowspan><b>$value</b></td>";
					$res .= "<td $class><b>-</b></td>";
				}
			}
			$res .= "</tr>\n";
			$res .= $detail."\n";
		}
	}
	$res .= "</table></body>\n";

	asgYell($res, $title, $target_emails, $email_title, true);
}

$db = new db($DB);
$report_group_services = "15,16,19,21";

$email_title = array("Content-Type"=>"text/html; charset=windows-1251", "Content-Transfer-Encoding"=>"8bit", "X-Priority"=>1, "From"=>"scat@alt1.ru");

$target_emails = "st@port102.org,vt@dom2010.com,ks@gjxnt.com,alex@gjxnt.com,td@gjxnt.com";

//$target_emails = "a.arhipov@alt1.ru";

notify_about_the_traffic_everyday_changing
    ($email_title, $target_emails, $report_group_services);
notify_about_the_detail_traffic_everyday_changing
    ($email_title, $target_emails, $report_group_services);

// � �����������
if (date('w') == '1') {
  notify_about_the_detail_traffic_everyweek_changing_with_operators
    ($email_title, $target_emails, $report_group_services);
}
