#!/usr/bin/perl
# -*- encoding: utf-8; tab-width: 4 -*-
package main;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use English '-no_match_vars';
use Carp;
use utf8;

use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use POSIX qw/strftime/;
use Time::Local;

use Disp::Utils qw/yell with_transaction sendmail/;
use Disp::Config;
use Encode qw/encode decode/;

sub main {
    my $dbh = Config->dbh();

    calc_coefficients($dbh);
    calc_rollup($dbh);
    make_report($dbh, strftime('%Y-%m-%d', localtime(time-86400)));
}

sub main_ {
    my $dbh = Config->dbh();

    my $date = strftime('%Y-%m-%d', localtime(time-86400*$ARGV[0]));
    print "DATE: $date\n";
    remove_old_rollup_data($dbh, $date);
    calc_rollup_date($dbh, $date);
}



sub make_report {
    my ( $dbh, $date ) = @_;

    $date ||= strftime('%Y-%m-%d', localtime(time - 86400));

    my $report = <<EOF;
<html>
  <head>
    <title>"Контент по карману" - отчет</title>
  </head>
  <body>
   <h1>"Контент по карману" - отчет</h1>
EOF

    $report .= report_unhandled($dbh, $date);
    $report .= report_unbillable($dbh, $date);
    $report .= report_new_requests($dbh, $date);
    $report .= report_write_offs($dbh, $date);
    $report .= report_completed($dbh, $date);

    $report .= <<EOF;
  </body>
</html>
EOF

    sendmail(
        to => Config->param(report_email => 'credit'),
        from => Config->param(report_from => 'credit'),
        subject => 'Daily "Kontent po karmanu" report',
        body => encode('utf-8', $report),
        content_type => 'text/html; charset=utf-8',
    );
}

sub report_unhandled {
    my ( $dbh, $date ) = @_;

    my $result = "";

    my $data = $dbh->selectall_arrayref(
        "select operator_id, count(*) as cnt, sum(credit_remaining) as money from tr_credit_messages  where credit_type = ? and updated_at between ? and ? group by operator_id", {Slice=>{}},
        'initial', $date . " 00:00:00", $date . " 23:59:59",
    );

    if ($data and @$data) {
        $result .= "<h2>Проигнорированные сообщения</h2>\n";
        $result .= "<table border='1'>\n";
        $result .= "<tr><td>Оператор</td><td>Количество</td><td>Сумма</td></tr>\n";

        for my $op (@$data) {
            $result .= "<tr>\n";

            my $op_name;
            if ($op->{operator_id}) {
                ( $op_name ) = $dbh->selectrow_array("select name from set_operator_t where id = ?", undef, $op->{operator_id});
                $op_name = $op_name ? decode('cp1251', $op_name) : $op->{operator_id};
            } else {
                $op_name = "Сервис не определился";
            }

            my $money = sprintf('%.02f', $op->{money} / 100.0);

            $result .= "<td>$op_name</td>\n";
            $result .= "<td>$op->{cnt}</td>\n";
            $result .= "<td>$money</td>\n";

            $result .= "</tr>\n";
        }
        $result .= "</table>";
    }

    return $result;
}

sub report_unbillable {
    my ( $dbh, $date ) = @_;
    my $result = "";

    my $data = $dbh->selectall_arrayref(
        "select m.operator_id, count(*) as cnt from tr_credit_messages m left join operator_billing_coefficients c on m.operator_id = c.op_id where c.op_id is null and m.operator_id is not null and updated_at between ? and ? group by 1", {Slice=>{}},
        "$date 00:00:00", "$date 23:59:59",
    );

    if ($data and @$data) {
        $result .= "<h2>Невозможно рассчитать биллинговые коэффициенты</h2>\n";
        $result .= "<table border='1'>\n";
        $result .= "<tr><td>Оператор</td><td>Количество сообщений</td></tr>\n";
        for my $row (@$data) {
            $result .= "<tr>\n";

            my ( $op_name ) = $dbh->selectrow_array("select name from set_operator_t where id = ?", undef, $row->{operator_id});
            $op_name = $op_name ? decode('cp1251', $op_name) : $row->{operator_id};
            $result .= "<td>$op_name</td>\n";
            $result .= "<td>$row->{cnt}</td>\n";
            $result .= "</tr>\n";
        }
        $result .= "</table>\n";
    }

    return $result;
}

sub report_new_requests {
    my ( $dbh, $date ) = @_;
    my $result = "";

    my ($count, $sum, $total) = $dbh->selectrow_array(
        "select count(*), sum(credit_remaining), sum(credit_total) from tr_credit_messages where credit_type = ? and updated_at between ? and ?", undef,
        'initial_delivered',"$date 00:00:00", "$date 23:59:59",
    );


    if ($count) {

        $sum = sprintf('%.02f', $sum / 100);
        $total = sprintf('%.02f', $total / 100);

        $result .= "<h2>Новые запросы</h2>\n";
        $result .= "$count принятых запросов на сумму $total с недостачей $sum рублей";

    }

    return $result;
}

sub report_write_offs {
    my ( $dbh, $date ) = @_;
    my $result = "";

    my ($count, $sum) = $dbh->selectrow_array(
        "select count(*), sum(credit_installment) from tr_credit_messages where credit_type = ? and updated_at between ? and ?", undef,
        'write_off',"$date 00:00:00", "$date 23:59:59",
    );

    if ($count) {
        $sum = sprintf('%.02f', $sum / 100);
        $result .= "<h2>Успешные досписания</h2>\n";
        $result .= "$count успешных досписаний на сумму $sum рублей";
    }

    return $result;
}

sub report_completed {
    my ( $dbh, $date ) = @_;
    my $result = "";

    my ($count, $sum, $total) = $dbh->selectrow_array(
        "select count(*), sum(remaining), sum(total) from tr_credit_lines where status in ('complete', 'incomplete') and last_status_change between ? and ?", undef,
        "$date 00:00:00", "$date 23:59:59",
    );

    if ($count) {
        $sum = sprintf('%.02f', $sum / 100);
        $total = sprintf('%.02f', $total / 100);
        $result .= "<h2>Закончена обработка</h2>";
        $result .= "Закончена обработка $count кредитных линий на сумму $total, конечная недостача $sum\n";
    }

    return $result;
}

sub calc_coefficients {
    my ( $dbh ) = @_;

    my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
    my $this_month = sprintf('%04d%02d', $year + 1900, $mon + 1);
    if (--$mon < 0) {
        $year -= 1;
        $mon = 11;
    }
    my $prev_month = sprintf('%04d%02d', $year + 1900, $mon + 1);

    calc_coefficients_for_month($dbh, $prev_month);
    calc_coefficients_for_month($dbh, $this_month);
}

sub calc_coefficients_for_month {
    my ( $dbh, $month ) = @_;

    my @mts_operators = map { $_->[0] } @{$dbh->selectall_arrayref("select id from set_operator where parent_id = 5 or id = 492")};

    my $start = month_start_date($month);
    my $end = month_end_date($month);

    $dbh->begin_work;

    $dbh->do("delete from operator_billing_coefficients where month = ?", undef, $month);

    my $data = $dbh->selectall_arrayref(<<EOF, {Slice => {}}, @mts_operators, $start, $end);
select
   extract(year_month from date) as month,
   operator_id,
   sum(operator_income__rur_without_nds) as income
from traffic_furl_price
where operator_id in ( @{[join (',', ('?') x @mts_operators )]} )
  and date between ? and ?
group by 1,2
EOF

    my %coefs = map { $_ => 0.50 } @mts_operators;
    for my $row (@$data) {
        my $i = $row->{income};
        $coefs{$row->{operator_id}} =
            $i < 700000 ?  0.50 :
            $i < 1500000 ? 0.51 :
            $i < 3000000  ? 0.52 :
            $i < 6000000  ? 0.54 :
            $i < 9000000  ? 0.56 :
            $i < 15000000 ? 0.58 :
            $i < 20000000 ? 0.60 :
            $i < 30000000 ? 0.62 :
            $i < 50000000 ? 0.64 :
            $i < 200000000? 0.66 :
            $i < 300000000? 0.68 : 0.70;
    }

    while (my($k, $v) = each %coefs) {
        $dbh->do(
            'insert into operator_billing_coefficients set month = ?, op_id = ?, coefficient = ?', undef,
            $month, $k, $v,
        );
    }

    $dbh->commit;
}

sub month_start_date {
    my ( $month ) = @_;
    confess "Wrong month $month" unless $month =~ /^(\d{4})(\d{2})$/;
    return "$1-$2-01";
}

sub month_end_date {
    my ( $month ) = @_;
    confess "Wrong month $month" unless $month =~ /^(\d{4})(\d{2})$/;

    my ($m, $y) = ( $2, $1 );
    $m--, $y -= 1900;

    my ($nm, $ny) = ($m == 11) ? (0,$y+1) : ($m+1,$y);

    return strftime('%Y-%m-%d', localtime(timelocal(0,0,12, 1,$nm,$ny) - 86400));
}

sub calc_rollup {
    my ( $dbh ) = @_;

    my $today = strftime('%Y-%m-%d', localtime(time - 86400*2));
    my $yesterday = strftime('%Y-%m-%d', localtime(time - 24 * 60 * 60));

    for my $date ($yesterday, $today) {
        remove_old_rollup_data($dbh, $date);
        calc_rollup_date($dbh, $date);
    }
}

sub remove_old_rollup_data {
    my ( $dbh, $date ) = @_;
    $dbh->do("delete from credit_messages_rollup where date = ?", undef, $date);
}

sub calc_rollup_date {
    my ( $dbh, $date ) = @_;

    my $ds = $date . " 00:00:00";
    my $de = $date . " 23:59:59";

    $dbh->do(<<EOF, undef, $ds, $de);
insert into credit_messages_rollup
select date(c.updated_at) as dt,
       extract(year_month from c.updated_at) as month,
       c.operator_id,
       c.num,
       c.service_id,
       s.sgroup_id,
       sum(case c.credit_type
         when 'initial' then c.credit_total
         when 'initial_rejected' then c.credit_total
         when 'initial_delivered' then c.credit_remaining
         when 'write_off' then -c.credit_installment
         else 0
       end) as money_shortage
   from tr_credit_messages c
        join tr_billing b on b.inbox_id = c.inbox_id and b.dir = 1
        left join set_service_t s on s.id = c.service_id
   where c.updated_at between ? and ? and c.inbox_id is not null
   group by date(c.updated_at), c.operator_id, c.num, c.service_id,c.service_id
EOF
}

Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
Config::init(config_file => ( $ENV{DEBUG_CONFIG_FILE} || "$FindBin::RealBin/../etc/asg.conf" ), skip_db_connect => 0);
main();
