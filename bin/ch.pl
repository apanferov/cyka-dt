#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::Config;
use Disp::Utils;
use Data::Dumper;
use Getopt::Std;

my %opts;
getopt('p', \%opts);
my $pref = $opts{p};

exit 0 unless ($pref);

Config::init(config_file => '../etc/asg.conf');

my $res = Config->dbh->selectall_arrayref('select distinct pattern,service_id from set_plugin where active=1 order by 1');

foreach my $mm (@$res) {
print "$mm->[1]\t$mm->[0]\n" if ($pref =~ /$mm->[0]/i);

}
