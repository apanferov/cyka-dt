#!/usr/bin/perl
# -*- tab-width: 4 -*-
package main;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use English '-no_match_vars';
use Carp;
use utf8;

use Data::Dumper;
use POSIX qw/strftime/;
use List::Util qw/max/;

use Log::Log4perl qw/get_logger/;

use Disp::CreditFSMTest;

Log::Log4perl::init(\qq(
           log4perl.logger          = DEBUG, Logfile

           log4perl.appender.Logfile          = Log::Log4perl::Appender::File
           log4perl.appender.Logfile.filename = $FindBin::RealBin/../var/log/test-suite.log
           log4perl.appender.Logfile.layout   = Log::Log4perl::Layout::PatternLayout
           log4perl.appender.Logfile.layout.ConversionPattern = [%d] %F %L %m%n

         )
                );

Test::Class->runtests;


