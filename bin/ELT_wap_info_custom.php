#!/usr/bin/php
<?php

// vim: tabstop=4

class Worker
{
	function __construct($INDB, $OUTDB, $mails)
	{
		$this->indb = new rdb($INDB);
		$this->outdb = new rdb($OUTDB);
		$this->system_root = $INDB[system_root];
		$this->log_file = "$INDB[system_root]/log/ELT_wap_info_custom_log.log";
		$this->err_log_file = "$INDB[system_root]/log/ELT_wap_info_custom_errors.log";
		$this->mails = $mails;
		$this->mail_title = array("From"=>"ELT_wap_info_custom");
		$this->log("Constructing completed.");
		$this->count_records_limit = 1000; // maximum records is retrieving from source database in one select query.
		$this->sleep_time = 60; // sleep time seconds!
		
		//
		// Loading SMSMA partners
		//
		$query = "SELECT id FROM alt5.set_cyka_partner WHERE group_id in (2,15,16,23)";
		$this->arrP = $this->indb->SelectAll2($query, 'id');
		if (!count($this->arrP))
		{
			$err_description = $this->indb->dberror();
			if ($err_description)
			{
				$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
				$this->w($err_msg);
				asgYell($err_msg, "ELT_wap_info_custom ERROR: database source error", $this->mails, $this->mail_title);
				sleep($this->sleep_time);
				exit;
			}
		}
	}
	function getmicrotime()
	{
	    list($usec,$sec) = explode(" ",microtime());
	    return ((float)$usec + (float)$sec);
	}
	//! \brief Error output.
	function errorExit($s)
	{
		//error_log (date("[Y-m-d H:i:s] ")." EXIT: $s\n",3,$this->log_file);
	    exit;
	}	
	//! \biref Standart messages output.
	function log($s)
	{
		//error_log(date("[Y-m-d H:i:s] ")." $s\n",3,$this->log_file);
	}
	//! \brief Warnings output.
	function w($s)
	{
		error_log(date("[Y-m-d H:i:s] ")."$s\n",3,$this->err_log_file);
	}
	//! \brief ������ ����������.
	function run()
	{
		$this->log("STARTING...");
		//sleep(100000000000000); exit;
		$start_time = $this->getmicrotime();
		while (1)
		{
			$executing_time = $this->getmicrotime() - $start_time;
			if ($executing_time >= 300) // 5 minutes
			{
				$this->errorExit("DAEMON RESTART");
			}

			//
			// ALGORITHM:
			// ----------
			// 1) (E)xtract (retrieving from source) external record.
			// 2) (L)oad record (loading into sync).
			// 3) (T)ransform data when loading.
			//
			// WARNING: all work in auto_transaction mode. It's not error!
			//

			//
			// (1)
			//
			$this->records = array();
			$query = "SELECT inbox_id as transaction_id, 
							 transport_type as transaction_type, 
							 date as date, 
							 service_id as service_id, 
							 operator_id as operator_id, 
							 abonent as abonent, 
							 num as num, 
							 msg as msg, 
							 partner_id as partner_id, 
							 partner_income as partner_income, 
							 partner_income_ex as partner_income_ex, 
							 partner_currency as partner_currency 
					 FROM alt5.cyka_payment 
					 WHERE cyka_processed=1 and date>date_sub(now(), interval 60 minute) LIMIT ".$this->count_records_limit;
			// executing
			$start_timestamp = $this->getmicrotime();
			$this->records = $this->indb->selectAll($query);
			// log
			$execute_time = $this->getmicrotime() - $start_timestamp;
			// work
			if (!($this->records))
			{
				$this->log("SLEEPING...");
				sleep($this->sleep_time);

				//
				// ���� ������, �� ��������� � �������
				//
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					asgYell($err_msg, "ELT_wap_info_custom ERROR: database source error", $this->mails, $this->mail_title);
					exit;
				}
				continue;
			}
			else 
			{
				$this->log("RETRIVING TIME FOR ".count($this->records)." MOVED WAP CUSTOMS IS $execute_time.");
			}
			
			//
			// (2)
			//
			$this->log("\nMOVING RECORDS:".count($this->records));
			$start_timestamp = $this->getmicrotime();
			if ($this->records)
			foreach($this->records as $record)
			{
				// escape
				$tmp_record = array();
				if ($record)
				foreach ($record as $k=>$v)
				{
					$tmp_record[$k] = $this->outdb->escape($v);
				}
				$record = $tmp_record;
				//
				// (3)
				//
				do
				{
					if ($this->arrP[$record['partner_id']]['id'])
					{
						if (0 == $record['transaction_type'])
						{
							$query = "SELECT content_id FROM cyka_downloads_wap ".
									 "WHERE transaction_id='".$record['transaction_id']."' and transport_type=10 LIMIT 1";
							$content_id = $this->indb->SelectField($query, 'content_id');
						}
						else
						{
							$content_id = null;
						}

						if (0/*!$content_id*/)
						{
							$this->log("The content_id for inbox_id=".$record['transaction_id'].
									   " and transport_type=".$record['transaction_type']." wasn't finding.");
							asgYell(print_r($record, true), "ELT_wap_info_custom WARNING: content_id wasn't finding.", $this->mails, $this->mail_title);
							//break;
						}
						// insert to sync
						$query = "REPLACE INTO wap_info.payment_t ".
								 "SET transaction_id='{$record['transaction_id']}',".
								 	 "transaction_type='{$record['transaction_type']}',".
									 "date='{$record['date']}',".
									 "service_id='{$record['service_id']}',".
									 "operator_id='{$record['operator_id']}',".
									 "abonent='{$record['abonent']}',".
									 "num='{$record['num']}',".
									 "msg='{$record['msg']}',".
									 "media_code=null,";
						if ($content_id)
						{
							$query .= "content_id='".$this->outdb->escape($content_id)."',";
						}
						else
						{
							$query .= "content_id=null,";
						}
						$query .= 	 "partner_id='{$record['partner_id']}',".
									 "partner_income='{$record['partner_income']}',".
									 "partner_income_ex='{$record['partner_income_ex']}',".
									 "partner_currency='{$record['partner_currency']}',".
									 "processed=0";
						$this->outdb->query($query);
						$err_description = $this->outdb->dberror();
						if ($err_description)
						{
							$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
							$this->w($err_msg);
							asgYell($err_msg, "ELT_wap_info_custom WARNING: database sync error", $this->mails, $this->mail_title);
							sleep( $this->sleep_time );
							exit;
						}
					}
					else
					{
						$this->log("Skip record inbox_id='".$record['transaction_id']."' and transaction_type='".$record['outbox_id']."'");
					}
				}
				while (0);

				// update source
				$query = "UPDATE alt5.cyka_payment 
						  SET cyka_processed=2  
						  WHERE inbox_id='{$record['transaction_id']}' and transport_type='{$record['transaction_type']}'"; 
				$this->indb->query($query);
				$err_description = $this->indb->dberror();
				if ($err_description)
				{
					$err_msg = "QUERY: {$query}\nMYSQL ERROR: {$err_description}\n";
					$this->w($err_msg);
					asgYell($err_msg, "ELT_wap_info_custom WARNING: database sync error", $this->mails, $this->mail_title);
					sleep( $this->sleep_time );
					exit;
				}
			} // END OF : foreach($this->records as $record)
			
			$execute_time = $this->getmicrotime() - $start_timestamp;
			$this->log("MOVING TIME FOR ".count($this->records)." EXTERNAL WAP CUSTOMS IS $execute_time.");
		
		} // END OF : while(1)
	
	} // END OF : Worker::run()
};	

include_once('../htdocss/config.php');
include_once('../htdocss/rdb.php'); 

// Mail adresses:
$mails = '';

$cp = new Worker($DB, $DB, $mails);
$cp->run();
?>
