#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;

my $group_id = 5;
my $min_sum = 50;

sub main {
	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg.conf");

	my @ids;
	my $sth = Config->dbh->prepare("select id from set_operator where parent_id = 5");
	$sth->execute;

	while (my($id) = $sth->fetchrow_array) {
		push @ids, $id;
	}

	$sth = Config->dbh->prepare("select abonent, usd, date from abonent_money_daily where usd > ? and operator_id in (".join(',', ('?')x@ids).") and date = date(date_sub(now(), interval 1 day))");
	$sth->execute($min_sum, @ids);

	my @out;
	while (my ($abonent, $usd, $date) = $sth->fetchrow_array) {
		push @out, "$date\t$abonent\t$usd";
	}

	sendmail(to => Config->param("yell_mts_fraud"),
			 from => Config->param("yell_from"),
			 subject => 'Daily report of abonents with big spendings',
			 body => join("\n", @out));
}

main();
