# wrapper to ping appenders

BEGIN {
	map { /Log4perl\.pm$/ && delete $INC{$_} } keys %INC;
	local @INC = @INC;
	shift @INC;
	require Log::Log4perl;

	$SIG{ALRM} = sub {
		map { $_->file_switch($_->{filename}) if ($_->{watcher} && $_->{watcher}->file_has_moved()) }
			values %{Log::Log4perl::appenders()};
		alarm(600);
	};
}
alarm(600);

1;
