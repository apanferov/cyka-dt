#!/usr/bin/perl -w
package Proxy;
use strict;
use warnings;
use Disp::Config qw(param);
use Redis;
use Nginx;

my $redis;

sub init_worker {
	Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
	$redis = Redis->new(server => Config->param('server','redis'), encoding => undef);
	warn << "END";
Proxy is running
END
}

sub redis {
	return $redis;
}

sub get_args {
	my ($r) = @_;
	my @pairs = split(/&/, $r->args);
	my $args = {};
	foreach my $pair (@pairs) { my ($name, $value) = split(/=/, $pair); $args->{$name} = $r->unescape($value);}
	return $args;
}

1;
