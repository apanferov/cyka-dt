#!/usr/bin/perl -w
package Subscription::ICBProxy;
use strict;
use warnings;
no  warnings 'uninitialized';
use bytes;
use Nginx;
use Subscription::Proxy;
use Data::Dumper;
use Disp::Utils;
use Disp::Config qw(param);
use Redis;
use XML::Simple;

my $service_tab = {
	icube_anonimservice =>    {redirect => "/icb-to-platinote-real", partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	icube_loadnofilmru =>     {redirect => "/icb-to-7hlp-real", partner => 0},
	icube_portallesscom =>    {redirect => "/icb-to-7hlp-real", partner => 0},
	icube_safeguardservice => {redirect => "/icb-to-platinote-real", partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	icube_windefend =>        {redirect => "/icb-to-platinote-real", partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	filositecom =>            {redirect => "/icb-to-7hlp-real", partner => 0},
	moydostupcom =>           {redirect => "/icb-to-7hlp-real", partner => 0},
	combatwars_net =>         {redirect => "/icb-to-platinote-real", partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	vkmediagetcom =>          {redirect => "/icb-to-platinote-real", partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	volifile_ru =>            {redirect => "/icb-to-7hlp-real", partner => 0},
};

my $black_list = {};

sub handler {
	my ($r) = @_;
	if ($r->has_request_body(\&handler_post)) {
		return OK;
	}
	return &handler_post;
}

sub handler_post {
	my ($r) = @_;
	my $body = $r->request_body || '';
	$r->log_error(0,"i-cb-body:$body");
	my ($service_id) = $body =~ /\sid\s*=\s*\x22([^\x22]+)\x22/;
	my $service = $service_tab->{$service_id} || {};

	if ($service->{partner_id} and cyka_payment_save_shaded($r,$service)) {
		$service->{redirect} = undef;
	}

	if ($service->{redirect}) {
		$r->internal_redirect($service_tab->{$service_id}{redirect});
		return OK;
	} else {
		$r->main_count_inc;
	        $r->send_http_header('text/html');
		$r->send_special(NGX_HTTP_LAST);
		$r->finalize_request(NGX_OK);
		return NGX_DONE;
	}
}

sub cyka_payment_save_shaded {
	my ($r,$service) = @_;
	my $shaded = 0;
	my $params = XMLin($r->request_body, KeepRoot => 0);
	if ($params->{subscriber}{services}{service}{status} =~ /^([05]|5[12])$/) {
		my $date = $params->{subscriber}{services}{service}{updated_at} || POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
		$date =~ s/\,/ /;
		my $data = {
			transport_type => 3,
			date => $date,
			service_id => $service->{service_id},
			abonent => $params->{subscriber}{msisdn},
			operator_id => 516,
			num => $service->{num},
			partner_id => $service->{partner_id},
			cyka_processed => 0,
			test_flag => 0,
			comment => "$params->{subscriber}{services}{service}{status} $params->{subscriber}{services}{service}{uuid}",
			is_error => 1,
		};
		if ($service->{shade_partner} and (!$black_list->{$params->{msisdn}}) and (int(rand(100)) < $service->{shade_percent}) and ($params->{status} =~ /^prolong/)) {
			$data->{partner_id} = $service->{shade_partner};
			$shaded = 1;
#			$log->info("MF SHADED NOTIFY ($service->{shade_percent}\%) -> $service->{shade_partner}");
		}
		my $msg = dumper_save($data);
		Proxy->redis->lpush('subs_queue',$msg);
	}
	return $shaded;
}


1;
