#!/usr/bin/perl -w
package Subscription::MTSProxy;
use strict;
use warnings;
no  warnings 'uninitialized';
use bytes;
use Nginx;
use Subscription::Proxy;
use Data::Dumper;
use Disp::Utils;
use Disp::Config qw(param);
use Redis;

#my @handler_tab = (
#	sub {
#		my ($r) = @_;
#		my $uri = $r->uri;
#		return "" if ($uri eq '/mts/wb');
#		return undef;
#	},
#);

sub handler_sn_mts {
	my $r = shift;
	if ($r->has_request_body(\&access_handler_post_sn_mts)) {
		return OK;
	}
	return &access_handler_post_sn_mts;
}

sub handler_mts_sn {
	my $r = shift;
	if ($r->has_request_body(\&access_handler_post_mts_sn)) {
		return OK;
	}
	return &access_handler_post_sn_mts;
}


#sub access_handler_sn_mts {
#	my ($r) = @_;
#	return OK if $r->has_request_body ( \&access_handler_post );
#	return &access_handler_post;
#}

#sub access_handler_mts_sn {
#	my ($r) = @_;
#	return OK if $r->has_request_body ( \&access_handler_post );
#	return &access_handler_post;
#}

#sub access_handler_post_sn_mts {
#	my ($r) = @_;
#	$r->phase_handler_inc;
#	$r->core_run_phases;
#	return NGX_DONE;
#}

#sub access_handler_post_mts_sn {
#	my ($r) = @_;
#	$r->phase_handler_inc;
#	$r->core_run_phases;
#	return NGX_DONE;
#}

sub access_handler_post_sn_mts {
	my ($r) = @_;
	save_request($r);
	$r->internal_redirect("/sn-to-mts-real");
	return OK;
}

sub access_handler_post_mts_sn {
	my ($r) = @_;
	save_request($r);
	$r->internal_redirect("/mts-to-sn-real");
	return OK;
}

sub save_request {
	my ($r) = @_;
	my $msg = dumper_save({request => {
				args => $r->args,
                        	method => $r->request_method,
#                          remote_addr => $r->remote_addr,
#                          unparsed_uri => $r->unparsed_uri,
				uri => $r->uri,
				headers => $r->headers_in,
				body => $r->request_body,
			},
	});
	Proxy->redis->lpush('subs_queue',$msg);
}
1;
