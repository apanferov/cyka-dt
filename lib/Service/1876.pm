package Service::1876;
use LWP::UserAgent;
use URI;
use URI::URL;

use Digest::MD5 qw/md5_hex/;
use MIME::Base64 qw/encode_base64/;
use URI::Escape qw/uri_unescape/;


my $secret_word = 'ldFlrFFLD45';
my @urls = ('https://profile.dsp.beeline.ru/api/v1/billing_notification');
my @sys_urls = ('http://api.7hlp.org/cyka_create_beeline');

sub handle
{
	my $req = shift;
	my $result = "";

	my ($num) = split('-', $req->param('num'));
	$num = $req->param('num') unless $num;
	if ($req->param('msg') =~ /^(\Q$num\E)\d+\|/) {
		foreach (@sys_urls) {
			$result .= send_request($_, scalar $req->Vars);
		}
	}

	my ($amount) = ($req->param('msg') =~ /^\Q$num\E\d+\|5\|charged\s(\d+\.\d+)\srub/, 0);
	my $params = {
		id => $req->param('transaction_id'),
		datetime => $req->param('date'),
		shortcode => $num,
		msisdn => $req->param('user_id'),
		smstext => encode_base64($req->param('msg')),
		operator => $req->param('operator_id'),
	};
	$params->{amount} = $amount if ($amount),
	$params->{signature} = md5_hex($params->{id}.$params->{datetime}.$secret_word);
	foreach (@urls) {
		$result .= send_request($_, $params);
	}

	if ($req->param('msg') =~ /^test\Q$num\E/) {
		return $req->header(-type => 'text/plain', -charset => 'cp1251'),
		"status: reply\n".
		"\n".
		"�������� ������������. ��� ������ ".substr(md5_hex($req->param('user_id')), 0, 10).".\n";
	}

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: ignore\n".
	"\n".
	$result;
}

sub send_request {
	my ($uri, $params) = @_;
	my $ua = new LWP::UserAgent;
	$ua->timeout(5);

	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);
	return $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
}

1;

__END__

# my @urls = ('http://tssubs.bildev2.adviator.com/api/stop');
my @urls = ('http://tss.adviator.com/api/stop');

sub handle
{	
	my $req = shift;
	my $result = "";
	foreach (@urls) {
		$result .= send_request($_,scalar $req->Vars);
	}

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: reply\n".
	"\n".
	"�������� ���������. ����������� �� ����������� ���. 8-800-555-3115.\n".
	"\n\n".
	"status: ignore\n".
	"\n".
	$result;
}

sub send_request {
	my ($uri,$params) = @_;
	my $ua = new LWP::UserAgent;
	$ua->timeout(5);

	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);

	my $response;
	eval {
		$response = $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
	};
	if ($@) {
		$response = "error: $@";
	}

	return $response;
}

1;

__END__

package Service::1835;
use LWP::UserAgent;
use URI;
use URI::URL;
use Digest::MD5 qw/md5_hex/;
use MIME::Base64 qw/encode_base64/;
use URI::Escape qw/uri_unescape/;

my $secret_word = 'lAoGN6lkah';
my $num_url = {
        1899 => 'http://confirmation.gameloft.com/sms/facp_ru_1899.php',
        1161 => 'http://confirmation.gameloft.com/sms/facp_ru_1161.php',
        1131 => 'http://confirmation.gameloft.com/sms/facp_ru_1131.php',
        1151 => 'http://confirmation.gameloft.com/sms/facp_ru_1151.php',
        1003 => 'http://confirmation.gameloft.com/sms/facp_am_1003.php',
        1008 => 'http://confirmation.gameloft.com/sms/facp_am_1008.php',
        3008 => 'http://confirmation.gameloft.com/sms/facp_am_3008.php',
        4016 => 'http://confirmation.gameloft.com/sms/facp_ru_4016.php',
        5013 => 'http://confirmation.gameloft.com/sms/facp_ru_5013.php',
        7003 => 'http://confirmation.gameloft.com/sms/facp_am_7003.php',
};

sub handle
{
        my $req = shift;
        my $result = "";

        my $datetime = $req->param('date');
        $datetime = uri_unescape($datetime);
        $datetime =~ s/[^\d]//g;
        my $params = {
                id => $req->param('transaction_id'),
                msisdn => $req->param('user_id'),
                shortcode => $req->param('num'),
                datetime => $datetime,
                smstext => encode_base64($req->param('msg')),
                operator => $req->param('operator_id'),
        };
        $params->{skey} = md5_hex($params->{id}.$params->{datetime}.$secret_word);
        $result .= send_request($num_url->{$req->param('num')}, $params);

        $req->header(-type => 'text/plain', -charset => 'cp1251'),
        "status: ignore\n".
        "\n".
        $result;
}

sub send_request {
        my ($uri, $params) = @_;
        my $ua = new LWP::UserAgent;
        $ua->timeout(5);

        my $url = url($uri);
        $url->query_form(%$params);
        my $resp = $ua->get($url);
        return $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
}

1;

__END__

package Service::1860;
use LWP::UserAgent;
use URI;
use URI::URL;

my @urls = ('http://api.7hlp.org/cyka_create_beeline');

sub handle
{
        my $req = shift;
        my $result = "";
        foreach (@urls) {
                $result .= send_request($_, scalar $req->Vars);
        }

        my ($num) = split('-', $req->param('num'));
        my @data = split('\|', $req->param('msg').' ');
        $data[-1] = substr($data[-1], 0, -1);
        my $source_addr = (scalar @data > 1) ? $data[0] : $num;
        my $source_port = (scalar @data > 3) ? $data[1] : '';
        my $msg = (scalar @data > 1) ? $data[-2] : $req->param('msg');
        my $transaction_id = (scalar @data > 1) ? $data[-1] : '';

        $req->header(-type => 'text/plain', -charset => 'cp1251'),
        # "status: forward\n".
        # "destination: ".$req->param('user_id')."\n".
        # "content-type: text/plain\n".
        # "plug: 676:".$num."\n".
        # "p.forward: 1\n".
        # "p.source_port: ".$source_port."\n".
        # "p.source_addr: ".$source_addr."\n".
        # "p.transaction_id: ".$transaction_id."\n".
        # "\n".
        # $msg."\n".
        # "\n\n".
        "status: ignore\n".
        "\n".
        $result;
}

sub send_request {
        my ($uri, $params) = @_;
        my $ua = new LWP::UserAgent;
        $ua->timeout(5);

        my $url = url($uri);
        $url->query_form(%$params);
        my $resp = $ua->get($url);
#       if ($resp->is_success) {
                return $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
#       } else {
#               return $resp->status_line;
#       }
}

1;
__END__
