package Service::1837;

use constant SECOND => 60;
use constant MINUTE => SECOND * 60;

my $respons_msg = '��������� q � ����� �� ��� ���������';
# my $respons_msg1 = '�� �������� � ������.';
my $session_timeout = 10 * MINUTE;
my $keyword = 'sms_pseudo';

my $pattern = '^'.$keyword;
my $pattern_c = qr/$pattern/i; 

sub handle
{
	my @msg_ar;

	if ($_[0]->param('msg') =~ m/$pattern_c/gi) {
		@msg_ar = (
			# "status: reply",
			# "tariff: free",
			# "destination: ".$_[0]->param('user_id'),
			# "",
			# $respons_msg1
			"status: ignore",
			"",
			"ignore"
		);
	} else {
		@msg_ar = (
			"status: notice-pseudo",
			"plug: ".$_[0]->param('num'),
			"destination: ".$_[0]->param('user_id'),
			"keyword: $keyword",
			"timeout: $session_timeout", 
			"",
			$respons_msg
		);
	}

	$_[0]->header(-type => 'text/plain', -charset => 'cp1251'),
	join("\n", @msg_ar);
}

1;
__END__