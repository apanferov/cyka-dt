package Service::1878;
use HTTP::Request;
use LWP::UserAgent;
use URI::URL;
use JSON;

my $operator_tab = {
	mBEELINE => 536,
	mMTS => 675,
	mMEGAFON => 532,
	mTELE2 => 577,
};

sub handle {
	my $req = shift;
	my $result = '';

	my $abonent = $req->param('user_id');
	my $num = $req->param('num');
	my $msg = $req->param('msg');

	$msg = '-' if (length($msg) == 0);
	$msg =~ s/(\n|\r\n)/\\n/g;
	my $operator = get_operator($abonent);
	if (ref($operator) eq "HASH") {
		my $operator_id = $operator_tab->{$operator->{orgcode}} || 0;

		if ($operator_id) {
			return
				$req->header(-type => 'text/plain', -charset => 'cp1251'),
				"status: return\n".
				"destination: ".$abonent."\n".
				"content-type: text/plain\n".
				"plug: ".$num."\n".
				"force-operator-id: ".$operator_id."\n".
				"registered-delivery: 1\n".
				# "provider: mks\n".
				"\n".
				"$msg\n"
			;
		} else {
			# error
			$result = 'Orgcode "'.$operator->{orgcode}.'" not in \$operator_tab!';
		}
	} else {
		# error
		$result = $operator;
	}

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: ignore\n".
	"\n".
	$result;
}

sub get_operator {
	my ($abonent) = @_;
	my $url = "http://178.63.66.211:9998/get_operator";
	my $user = 'qqq';
	my $pass = 'qqq';

	if (length($abonent) == 10) {
		$abonent = "7".$abonent;
	} elsif (length($abonent) == 11) {
		$abonent = "7".substr($abonent, 1);
	} else {
		return 0;
	}

	$url = url($url);
	my %req = (
		number => $abonent,
		# parent_id => 0,
	);
	$url->query_form(%req);

	# GET
	my $ua = LWP::UserAgent->new;
	$ua->timeout(5); # Ожидаем ответ (сек)
	my $request = HTTP::Request->new;
	$request->method("GET");
	$request->uri($url);
	$request->authorization_basic($user, $pass);
	my $result = '';
	eval {
		my $response = $ua->request($request);
		$result .= $response->content()."\n";
		$result = decode_json($response->content());
	};
	if ($@) {
		$result .= $@;
	}

	return $result;
}

1;
__END__
