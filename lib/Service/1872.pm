package Service::1872;
use LWP::UserAgent;
use URI;
use URI::URL;

my @urls = ('http://api.7hlp.org/cyka_stop');

sub handle
{	
	my $req = shift;
	my $result = "";
	foreach (@urls) {
		$result .= send_request($_,scalar $req->Vars);
	}

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	# "status: reply\n".
	# "\n".
	# "�������� �� ����� ���������.\n".
	# "\n\n".
	"status: ignore\n".
	"\n".
	$result;
}

sub send_request {
	my ($uri,$params) = @_;
	my $ua = new LWP::UserAgent;
	$ua->timeout(5);

	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);
	return $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
}

1;
__END__
