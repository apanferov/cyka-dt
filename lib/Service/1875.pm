package Service::1875;
use LWP::UserAgent;
use URI;
use URI::URL;

# my @urls = ('http://tssubs.bildev2.adviator.com/api/stop');
my @urls = ('http://tss.adviator.com/api/stop');

sub handle
{	
	my $req = shift;
	my $result = "";
	foreach (@urls) {
		$result .= send_request($_,scalar $req->Vars);
	}

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: reply\n".
	"\n".
	"�������� ���������. ����������� �� ����������� ���. 8-800-555-3115.\n".
	"\n\n".
	"status: ignore\n".
	"\n".
	$result;
}

sub send_request {
	my ($uri,$params) = @_;
	my $ua = new LWP::UserAgent;
	$ua->timeout(5);

	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);

	my $response;
	eval {
		$response = $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
	};
	if ($@) {
		$response = "error: $@";
	}

	return $response;
}

1;
__END__
