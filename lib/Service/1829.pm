# echo
package Service::1829;

sub handle
{
	my $msg = $_[0]->param('msg');
	$msg='|' if (length($msg)==0);
	$msg =~ s/(\x0a|\x0d\x0a)/|/g;
	$_[0]->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: reply\n\n$msg";
}

1;
__END__
