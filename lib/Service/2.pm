package Service::2;
use Net::Domain qw(hostfqdn);

sub handle
{
	$_[0]->header(-type => 'text/plain', -charset => 'cp1251'),
	'{"result": "ok ('.hostfqdn().')"}';
}

1;
__END__
