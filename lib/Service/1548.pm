package Service::1548;
use strict;


sub handle
{
	$_[0]->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: reply\n"
	."destination: ".$_[0]->param('user_id')
	."\ntariff: free\n\n"
	.'Wir konnten Ihre Eingabe nicht auswerten. Haben Sie sich vertippt? Bitte versuchen Sie es noch einmal.';
}

1;
__END__
