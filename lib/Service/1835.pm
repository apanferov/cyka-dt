package Service::1835;
use LWP::UserAgent;
use URI;
use URI::URL;
use Digest::MD5 qw/md5_hex/;
use MIME::Base64 qw/encode_base64/;
use URI::Escape qw/uri_unescape/;

my $secret_word = 'lAoGN6lkah';
my $num_url = {
	1899 => 'http://confirmation.gameloft.com/sms/facp_ru_1899.php',
	1161 => 'http://confirmation.gameloft.com/sms/facp_ru_1161.php',
	1131 => 'http://confirmation.gameloft.com/sms/facp_ru_1131.php',
	1151 => 'http://confirmation.gameloft.com/sms/facp_ru_1151.php',
	1003 => 'http://confirmation.gameloft.com/sms/facp_am_1003.php',
	1008 => 'http://confirmation.gameloft.com/sms/facp_am_1008.php',
	3008 => 'http://confirmation.gameloft.com/sms/facp_am_3008.php',
	4016 => 'http://confirmation.gameloft.com/sms/facp_ru_4016.php',
	5013 => 'http://confirmation.gameloft.com/sms/facp_ru_5013.php',
	7003 => 'http://confirmation.gameloft.com/sms/facp_am_7003.php',
};

sub handle
{	
	my $req = shift;
	my $result = "";

	my $datetime = $req->param('date');
	$datetime = uri_unescape($datetime);
	$datetime =~ s/[^\d]//g;
	my $params = {
		id => $req->param('transaction_id'),
		msisdn => $req->param('user_id'),
		shortcode => $req->param('num'),
		datetime => $datetime,
		smstext => encode_base64($req->param('msg')),
		operator => $req->param('operator_id'),
	};
	$params->{skey} = md5_hex($params->{id}.$params->{datetime}.$secret_word);
	$result .= send_request($num_url->{$req->param('num')}, $params);

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: ignore\n".
	"\n".
	$result;
}

sub send_request {
	my ($uri, $params) = @_;
	my $ua = new LWP::UserAgent;
	$ua->timeout(5);

	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);
	return $url->as_string()."\n".$resp->code()." (".$resp->header("Client-Warning").")\n";
}

1;
__END__
