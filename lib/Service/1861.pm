package Service::1861;
use LWP::UserAgent;
use URI;
use URI::URL;

sub handle
{	
	my $req = shift;
	my $result = "";
	foreach (@urls) {
		$result .= send_request($_, scalar $req->Vars);
	}

	my ($num) = split('-', $req->param('num'));
	my @data = split('\|', $req->param('msg').' ');
	$data[-1] = substr($data[-1], 0, -1);
	my $source_addr = (scalar @data > 1) ? $data[0] : $num;
	my $source_port = (scalar @data > 3) ? $data[1] : '';
	my $msg = (scalar @data > 1) ? $data[-2] : $req->param('msg');
	my $transaction_id = (scalar @data > 1) ? $data[-1] : '';

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: forward\n".
	"destination: ".$req->param('user_id')."\n".
	"content-type: text/plain\n".
	"plug: 677:".$num."\n".
	"p.backward: 1\n".
	"p.source_port: ".$source_port."\n".
	"p.source_addr: ".$source_addr."\n".
	"p.transaction_id: ".$transaction_id."\n".
	"\n".
	$msg."\n".
	"\n\n".
	"status: ignore\n".
	"\n".
	"Done";
}

1;
__END__
