package Service::1827;
use Redis;

my $r = Redis->new(encoding => undef);

sub handle {
	my $req = shift;
	my $op = $r->hget('mks_2316', $req->param('user_id'));
	my $msg = $req->param('msg');
	$msg = '|' if (length($msg) == 0);
	$msg =~ s/(\x0a|\x0d\x0a)/|/g;

	$req->header(-type => 'text/plain', -charset => 'cp1251'),
	"status: return\n".
	"destination: ".$req->param('user_id')."\n".
	"content-type: text/plain\n".
	"plug: ".$req->param('num')."\n".
	"force-operator-id: $op\n".
	"registered-delivery: 1\n".
	"provider: mks\n".
	"\n".
	"$msg\n";
}

1;
__END__
