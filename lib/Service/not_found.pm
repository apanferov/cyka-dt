package Service::not_found;

sub handle
{
	$_[0]->header(-type => 'text/html', -charset => 'utf-8'),
	"Not found";
}

1;
__END__