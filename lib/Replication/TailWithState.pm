#!/usr/bin/perl
# -*- coding: utf-8 -*-
package Replication::TailWithState;
use base 'Exporter';
use Time::Local;
use Replication::Position;
use IO::File;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;

our $dir = '/gate/asg5/var/repl';

sub new {
    my ( $class, $pos_filename ) = @_;
	my $self = { log => get_logger('') };
	bless $self, $class;
	$self->{pos} = new Replication::Position("$dir/$pos_filename");
	my ($filename,$pos) = $self->load_position;
	$self->open_file($filename,$pos);
    return $self;
}

sub open_file {
	my ($self, $filename, $pos) = @_;
	$filename ||= $self->{repl_filename};
	$self->{log}->debug("Trying to open file $dir/$filename ...");
	$self->{fh}->close if $self->{fh};
	$self->{fh} = undef;
	$self->{repl_filename} = $filename;
	$self->{next_file_date} = filename_to_date($self->{repl_filename})+86400;
	return 0 unless (-f "$dir/$filename");
	$self->{fh} = new IO::File("$dir/$filename",'r') or die "Can't open file '$dir/$filename'";
	$self->{fh}->seek($pos,0) if $pos;
	$self->{log}->info("Replication file $dir/$filename opened.");
	return 1;
}

sub open_next_file {
	my ($self, $filename) = @_;
	$filename ||= $self->{repl_filename};
	while (time > filename_to_date($filename) + 86400) {
		$filename = get_next_filename($filename);
		last if $self->open_file($filename);
	}
}

sub can_open_next_file {
	my $self = shift;
	return 1 unless $self->{fh};
	return ($self->{fh}->eof and ($self->{next_file_date} < time));
}

sub load_position {
	my ($self,$position_filename) = @_;
	# Try load from position file
	my ($filename,$pos) = $self->{pos}->load_position;
	# Try find earliest replication file in directory
	unless ($filename and (-f "$dir/$filename")) {
		($filename) = sort {$a cmp $b} glob("$dir/repl-????-??-??-GMT.log") ;
		$filename ||= '';
		($filename) = $filename =~ /(repl-\d{4}-\d{2}-\d{2}-GMT\.log)/;
	}
	# Set defaults
	$filename ||= "repl-2008-10-01-GMT.log";
	return $filename,$pos;
}

sub save_position {
	my $self = shift;
	my $pos = $self->{fh}->tell;
	$self->{pos}->save_position($self->{repl_filename},$pos);
	return $pos;
}

sub get_line {
	my $self = shift;
	# Try to read line
	my $line = $self->{fh}->getline if ($self->{fh});
	return $line if $line;
	# Try to open current file
	$self->open_file unless ($self->{fh});
	# Try to open next files
	$self->open_next_file if $self->can_open_next_file;
	# Try to read line again
	$line = $self->{fh}->getline if ($self->{fh});
	return $line;
}

sub pos {
	my $self = shift;
	return $self->{fh}->tell if $self->{fh};
	return 0;
}

sub finish {
	my ($self) = @_;
	$self->{fh}->close if $self->{fh};
	$self->{fh} = undef;
}

#######################################################
#  Filename Functions
#######################################################

sub filename_to_date {
	my $filename = shift;
	my ($year,$month,$day) = $filename =~ /^repl-(\d{4})-(\d{2})-(\d{2})-GMT\.log$/;
	return 0 unless ($year and $month and $day);
	return timegm(0,0,0,$day,--$month,$year);
}

sub date_to_filename {
	my $time = shift;
	my ($sec,$min,$hour,$day,$month,$year,$wday,$yday,$isdst) = gmtime($time);
	$month++;
	$year+=1900;
	return sprintf("repl-%04d-%02d-%02d-GMT.log",$year,$month,$day);
}

sub get_next_filename {
	my $filename = shift;
	return date_to_filename(filename_to_date($filename)+86400);
}

sub compare_filenames {
	my ($filename1, $filename2) = @_;
	return filename_to_date($filename1) <=> filename_to_date($filename2);
}

sub test {
	my $a1 = 'repl-2008-10-31-GMT.log';
	my $d = filename_to_date($a1);
	my $a2 = date_to_filename($d);
	print "$a1 => $d => $a2\n";
	print "OK\n" if ($a1 eq $a2);
	my $a3 = get_next_filename($a1);
	print "next to $a1 => $a3\n";
	print "OK\n" if ($a3 eq 'repl-2008-11-01-GMT.log');
	my $a4 = get_next_filename('repl-2008-10-26-GMT.log');
	print "$a4\n";
	print "OK\n" if ($a4 eq 'repl-2008-10-27-GMT.log');
}

1
