#!/usr/bin/perl
# -*- coding: utf-8 -*-
package Replication::Position;
use strict;
use warnings;
use base 'Exporter';
use Data::Dumper;

sub new {
    my ( $class, $filename ) = @_;
	my $self = { pos_filename => $filename};
	bless $self, $class;
    return $self;
}

sub load_position {
	my $self = shift;
	my ($filename,$pos);
	# Try load from position file
	if (-f $self->{pos_filename}) {
		open(FH, '<', $self->{pos_filename});
		($filename,$pos) = <FH> =~ /^(.*)\t(\d*)/;
		close(FH);
	}
	$pos ||= 0;
	return $filename,$pos;
}

sub save_position {
	my ($self, $filename, $pos) = @_;
	open(FH, '>', $self->{pos_filename});
	print FH  "$filename\t$pos";
	close(FH);
}

1

