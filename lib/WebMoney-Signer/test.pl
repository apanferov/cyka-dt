#!/usr/bin/perl
use strict;
use warnings;
use lib qw(./blib/arch);
use lib qw(./blib/lib);
use Data::Dumper;

use WebMoney::Signer;

my $s = WebMoney::Signer->new("1234123412", "1234", "123.kwm");

unless ( $s->Sign("',.p',.p") ) {
  print $s->ErrorCode . "\n";
}

