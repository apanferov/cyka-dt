#ifdef __cplusplus 
extern "C" {
#endif

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "ppport.h"

#ifdef __cplusplus 
}
#endif

#include "signer.h"

class SignerWrapper {
  Signer signer_;
public:

  SignerWrapper(char *szLogin, char *szPassword, char *szKeyFileName) :
	signer_(szLogin, szPassword, szKeyFileName)
  {
  }

  char* Sign(char *szIn)
  {
	szptr szSign;
	if ( signer_.Sign(szIn, szSign) ) {
	  const char *result = szSign;
	  return strdup(result);
	} else {
	  return 0;
	}
  }

  int ErrorCode()
  {
	return signer_.ErrorCode();
  }
};

MODULE = WebMoney::Signer		PACKAGE = WebMoney::Signer		

SignerWrapper *
SignerWrapper::new(szLogin, szPassword, szKeyFileName)
  char *szLogin
  char *szPassword
  char *szKeyFileName

char *
SignerWrapper::Sign(szIn)
  char *szIn

int
SignerWrapper::ErrorCode()

