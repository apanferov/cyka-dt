#!/usr/bin/perl
package POE::Component::FastCGI_m;
use strict;
use warnings;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;

use POE qw/Component::Server::TCP
		   Wheel::ReadWrite
		   Driver::SysRW
		   Filter::FastCGI/;

sub new {
	my ( $class, %args ) = @_;

	my $session = POE::Session->create
	  ( inline_states => { _start => \&_start,
						   routines => \&_routines,
						   terminate => \&_shutdown },
		heap => \%args
	  );
}

sub _routines {
	my ( $heap, $kernel ) = @_[HEAP, KERNEL];

	$heap->{HandleRoutines}->();

	$kernel->alarm_set('routines', time + $heap->{RoutinesInterval} );
}

sub _start {
	my ( $session, $heap, $kernel ) = @_[SESSION, HEAP, KERNEL];

	print "_start: ".$session->ID."\n";

	$kernel->sig( 'TERM' => 'terminate' );
	$kernel->sig( 'INT' => 'terminate' );

	POE::Component::Server::TCP->new
		( Port => $heap->{Port},
		  Address => $heap->{Address},
		  Alias => 'fcgi_server',
		  ClientFilter => 'POE::Filter::FastCGI',
		  ClientInput => \&_input,
		  ClientConnected => \&_connect,
		  InlineStates => { _timer => \&_timer },
		  Args => [ $heap->{HandleRequest}, $heap->{HandleAlarm} ]
		);

	$kernel->alarm_set('routines', time + $heap->{RoutinesInterval} );
}

sub _shutdown {
	my ( $session, $heap, $kernel ) = @_[SESSION, HEAP, KERNEL];
	print "_shutdown: ".$session->ID."\n";

	if ($heap->{shutdown_start_time}) {
		# Don't ignore second termination signal
	} else {
		$heap->{shutdown_start_time} = time;
		$kernel->post(fcgi_server => 'shutdown');
		$kernel->alarm_remove_all();
		$kernel->sig_handled();
	}
}

sub _connect {
	my ( $session, $heap, $args ) = @_[SESSION, HEAP, ARG0];
	print "_connect: ".$session->ID."\n";
	$heap->{alarm_handler} = $args->[1];
	$heap->{request_handler} = $args->[0];
}

sub _input {
	my ( $kernel, $heap, $input, $session ) = @_[KERNEL, HEAP, ARG0, SESSION];

	$heap->{input} = $input;
	$heap->{client_heap} = {};

	print "_input: ".$session->ID."\n";

	eval {
		my $ret = $heap->{request_handler}->($session->ID, $heap->{client_heap}, fastcgi_input_to_cgi_like($input));
		return process_handler_result($ret, $heap, $kernel);
	};
	if ($@) {
		if ($heap->{HandleYell}) {
			eval {
				$heap->{HandleYell}->("fastcgi session - uncaught exeption in handle_request: $@");
			};
		}
		return process_handler_result(['Status: 500 Infernal server error\nContent-type: text/html\n\n<h1>Infernal server error</h1>Uncaught exception:'.$@]);
	}
}

sub _timer {
	my ( $kernel, $heap, $input, $session ) = @_[KERNEL, HEAP, ARG0, SESSION];
	print "_timer: ".$session->ID."\n";
	eval {
		my $ret = $heap->{alarm_handler}->($session->ID, $heap->{client_heap});
		return process_handler_result($ret, $heap, $kernel);
	};
	if ($@) {
		if ($heap->{HandleYell}) {
			eval {
				$heap->{HandleYell}->("fastcgi session - uncaught exeption in handle_alarm: $@");
			};
		}
		return process_handler_result('Status: 500 Infernal server error\nContent-type: text/html\n\n<h1>Infernal server error</h1>Uncaught exception');
	}
}

sub process_handler_result {
	my ( $ret, $heap, $kernel ) = @_;

	print "AAAA".Dumper($ret);
	if ((ref $ret eq 'ARRAY') and $heap->{client}) {
		print "RESULT\n";
		$heap->{client}->put({ content => join("\n", @$ret),
							   requestid => $heap->{input}->[0],
							   close => 1 });
		delete $heap->{client};
	} elsif (defined($ret) and (not ref $ret) and ($ret >= 0)) {
		print "TIMER: $ret\n";
		$kernel->alarm_set('_timer', time + $ret);
	} elsif ($heap->{client}) {
		print "FAILURE\n";
		$heap->{client}->put({ content => "Status: 500 Infernal server error\nContent-type: text/html\n\n<h1>500 Infernal server error</h1>callback returned incorrect data",
							   requestid => $heap->{input}->[0],
							   close => 1 });
		delete $heap->{client};
	}
}

sub fastcgi_input_to_cgi_like {
	my ( $input ) = @_;
	return POE::Component::FastCGI_m::FakeCGI->new($input);

}

package POE::Component::FastCGI_m::FakeCGI;
use URI;

sub new {
	# At a time - only GET is supported
	my ( $proto, $input ) = @_;

	$proto = ref($proto) || $proto;

	my $self = { input => $input };
	my $query = "";

	if ($input->[2]{REQUEST_METHOD} eq 'GET') {
		$query = $input->[2]{QUERY_STRING};
	} elsif ($input->[2]{REQUEST_METHOD} eq 'POST' and
			 $input->[2]{CONTENT_TYPE} eq 'application/x-www-form-urlencoded') {
		$query = $input->[1]{postdata};
	}

	my $u = new URI;

	$u->query($query);
	$self->{params} = {$u->query_form};

	bless $self, $proto;
	return $self;
}

sub Vars {
	my ( $self ) = @_;
	return unless defined wantarray;
	return %{$self->{params}} if wantarray;
	return {%{$self->{params}}};
}

sub param {
	my ( $self, $param ) = @_;
	return $self->{params}{$param};
}


1;
