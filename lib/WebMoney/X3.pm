#!/usr/bin/perl
package WebMoney::X3;

use strict;
use warnings;

use XML::Simple;

# reqn
# wmid
# purse
# datestart
# datefinish
# signer


sub make {
	my ( %p ) = @_;

	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime(time);
    my $reqn = sprintf '%04i%02i%02i%02i%02i%02i', $year+1900, $mon, $mday, $hour, $min, $sec;

	my $go = { purse => { content => $p{purse } },
			   datestart => { content => $p{datestart} },
			   datefinish => { content => $p{datefinish} } };

	my $req = { reqn => { content => $reqn },
				getoperations => {content => $go },
			  };

	if ($p{signer}) {
		my $sign = $p{signer}->Sign($p{purse}.$reqn);
		return unless $sign;
		$req->{sign} = { content => $sign };
		$req->{wmid} = { content => $p{wmid} };
	}

	my $xml = { 'w3s.request' => $req };

	return XMLout($xml, KeepRoot => 1, XMLDecl => 1);
}
1;
