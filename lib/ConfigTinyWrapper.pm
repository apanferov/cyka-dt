#!/usr/bin/perl
package ConfigTinyWrapper;
use strict;
use warnings;
use base 'Config::Tiny';


sub read {
    my ( $proto, $name ) = @_;
    my $self = Config::Tiny->read($name);

    foreach my $sect (values %{$self}) {
	foreach (values %{$sect}) {
	    s/^\s*"(.+)"\s*$/$1/;
	}
    }

    return $self;
}

sub errstr {
    my ( $proto ) = @_;
    return Config::Tiny->errstr;
}

1;