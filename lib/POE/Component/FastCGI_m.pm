#!/usr/bin/perl
package POE::Component::FastCGI_m;
use strict;
use warnings;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use IO::String;
use Carp qw/confess/;

use POE qw/Component::Server::TCP
		   Wheel::ReadWrite
		   Driver::SysRW
		   Filter::FastCGI/;

sub new {
	my ( $class, %args ) = @_;

	my $session = POE::Session->create
	  ( inline_states => { _start => \&_start,
						   routines => \&_routines,
						   terminate => \&_shutdown,
						   register_destructor => \&_register_destructor,
						 },
		heap => \%args
	  );
}

sub _register_destructor {
	my ( $heap, $kernel, $session, $event ) = @_[HEAP, KERNEL, ARG0, ARG1];

	print "destruction planned for '$session' with event '$event'\n";

	push @{$heap->{destruction_list}}, [ $session, $event ];
}

sub _routines {
	my ( $heap, $kernel ) = @_[HEAP, KERNEL];

	$heap->{HandleRoutines}->();

	$kernel->alarm_set('routines', time + $heap->{RoutinesInterval} );
}

sub _start {
	my ( $session, $heap, $kernel ) = @_[SESSION, HEAP, KERNEL];

	print "_start: ".$session->ID."\n";

	$kernel->alias_set( 'fastcgi' );

	$kernel->sig( 'TERM' => 'terminate' );
	$kernel->sig( 'INT' => 'terminate' );

	POE::Component::Server::TCP->new
		( Port => $heap->{Port},
		  Address => $heap->{Address},
		  Alias => 'fcgi_server',
		  ClientFilter => 'POE::Filter::FastCGI',
		  ClientInput => \&_input,
		  ClientConnected => \&_connect,
		  ClientDisconnected => \&_disconnect,
		  ClientError => \&_error,
		  InlineStates => { _timer => \&_timer, _result => \&_result },
		  Args => [ $heap->{HandleRequest}, $heap->{HandleAlarm}, $heap->{HandleResult} ]
		);

	$kernel->alarm_set('routines', time + $heap->{RoutinesInterval} );
}

sub _shutdown {
	my ( $session, $heap, $kernel ) = @_[SESSION, HEAP, KERNEL];
	print "_shutdown: ".$session->ID."\n";

	if ($heap->{shutdown_start_time}) {
		# Don't ignore second termination signal
	} else {
		$heap->{shutdown_start_time} = time;

		for (@{$heap->{destruction_list}}) {
			my ( $s, $e ) = @$_;
			my $sid = $kernel->alias_resolve($s);
			if ($sid) {
				$kernel->post($sid, $e);
			}
		}

		$kernel->post(fcgi_server => 'shutdown');
		$kernel->alarm_remove_all();
		$kernel->sig_handled();
	}
}

sub _error {
	my ( $session, $heap ) = @_[SESSION, HEAP];
	$heap->{client_heap}->{error} = [ @_[ARG0, ARG1, ARG2] ];
	warn '_error:'.Dumper($heap->{client_heap});
}

sub _result {
	my ( $session, $heap, $res ) = @_[SESSION, HEAP, ARG0];
	warn '_result:'.Dumper($heap->{client_heap});
#	$heap->{result_handler}->($session->ID, $heap->{client_heap}, $res);
}

sub _disconnect {
	my ( $session, $heap, $res ) = @_[SESSION, HEAP, ARG0];
	warn '_disconnect:'.Dumper($heap->{client_heap});
	#$heap->{result_handler}->($session->ID, $heap->{client_heap}, $heap->{client_response});
}

sub _connect {
	my ( $session, $heap, $arg0, $arg1, $arg2 ) = @_[SESSION, HEAP, ARG0, ARG1, ARG2];
	print "_connect: ".$session->ID."\n";
	$heap->{alarm_handler} = $arg1;
	$heap->{request_handler} = $arg0;
	$heap->{result_handler} = $arg2;
}

sub _input {
	my ( $kernel, $heap, $input, $session ) = @_[KERNEL, HEAP, ARG0, SESSION];

	$heap->{input} = $input;
	$heap->{client_heap} = { request_id => $input->[0] };

	print "_input: ".$session->ID."\n";

	eval {
		my $ret = $heap->{request_handler}->($session->ID, $heap->{client_heap}, fastcgi_input_to_cgi_like($input), $input);
		return process_handler_result($session, $ret, $heap, $kernel);
	};
	if ($@) {
		if ($heap->{HandleYell}) {
			eval {
				$heap->{HandleYell}->("fastcgi session - uncaught exeption in handle_request: $@");
			};
		}
		return process_handler_result($session, ['Status: 500 Infernal server error\nContent-type: text/html\n\n<h1>Infernal server error</h1>Uncaught exception:'.$@], $heap, $kernel);
	}
}

sub _timer {
	my ( $kernel, $heap, $input, $session ) = @_[KERNEL, HEAP, ARG0, SESSION];
	print "_timer: ".$session->ID."\n";
	eval {
		my $ret = $heap->{alarm_handler}->($session->ID, $heap->{client_heap});
		return process_handler_result($session, $ret, $heap, $kernel);
	};
	if ($@) {
		if ($heap->{HandleYell}) {
			eval {
				$heap->{HandleYell}->("fastcgi session - uncaught exeption in handle_alarm: $@");
			};
		}
		return process_handler_result($session, 'Status: 500 Infernal server error\nContent-type: text/html\n\n<h1>Infernal server error</h1>Uncaught exception');
	}
}

sub process_handler_result {
	my ( $session, $ret, $heap, $kernel ) = @_;

	print "CALLBACK RESULT ".Dumper($ret);
	my ($content,$timer);
	if (defined($ret) and (ref $ret eq 'HASH')) {
		print "HASH\n";
		$content = $ret->{content};
		$content = join("\n", @{$ret->{content}}) if (defined($ret->{content}) and (ref $ret->{content} eq 'ARRAY'));
		$timer = $ret->{timer};
	} elsif (defined($ret) and (ref $ret eq 'ARRAY')) {
		print "ARRAY\n";
		$content = join("\n", @$ret);
#		$heap->{client_response} = $ret;
	} elsif (defined($ret) and not ref $ret) {
		print "TIMER\n";
		$timer = $ret;
	} elsif ($heap->{client}) {
		print "FAILURE\n";
		$content = "Status: 500 Internal server error\nContent-type: text/html\n\n<h1>500 Internal server error</h1>callback returned incorrect data";
	}

	if (defined($content) and $heap->{client}) {
		print "RESULT $content\n";
		eval {
			$heap->{client}->put({ content => $content,
						requestid => $heap->{input}->[0],
						close => 1 });
		};
		if ( $@ ) {
			confess $@;
		}
#		delete $heap->{client};
	}
	if (defined($timer)) {
		print "TIMER: $timer\n";
		$kernel->alarm_set('_timer', time + $timer);
	}
	return undef;
}

sub fastcgi_input_to_cgi_like {
	my ( $input ) = @_;
	return POE::Component::FastCGI_m::FakeCGI->new($input);

}

package POE::Component::FastCGI_m::FakeCGI;
use URI;
use Data::Dumper;
require CGI;

sub new {
	# At a time - only GET is supported
	my ( $proto, $input ) = @_;

	local *STDIN;
	tie *STDIN, 'IO::String';
	${tied(*STDIN)->string_ref} = $input->[1]{postdata} || "";

	# Don't copy all keys
	$ENV{$_} = $input->[2]{$_} for qw/REMOTE_ADDR/;

	local %ENV = %ENV;
	$ENV{$_} = $input->[2]{$_} for keys %{$input->[2]};

	CGI::initialize_globals();
	my $real_q = new CGI;

	# $real_q->param(-name => 'POSTDATA', -value => $input->[1]{postdata});
	return $real_q;
}


1;
