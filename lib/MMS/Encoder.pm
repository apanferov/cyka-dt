package MMS::Encoder;

use strict;

sub new{# �����������
	my ($class) = @_;
   my $self = {};
   	$self->{dataBody}=""; 
	$self->{mmsBorder}='mms-border';
   $self->{Header}={
	'X-MMS-Message-Type'=>'m-send-req',
'X-MMS-Transaction-ID'=>'',
'X-MMS-MMS-Version'=>'1.0',
'From'=>undef,
'To'=>undef,
'Cc'=>undef,
'Subject'=>undef,
'X-MMS-Message-Class'=>undef,#'personal',
'X-MMS-Priority'=>undef,#'normal',
'X-MMS-Read_Reply'=>undef,#'No',
'Content-Type'=>'multipart/mixed;boundary='.$self->{mmsBorder}};

   
   bless $self, $class;
   return $self;
}
sub setHeader{# ������� "��������" ����� mms ��������� 
	my $self=shift;
	($self->{Header}->{From},
	$self->{Header}->{To},
	$self->{Header}->{Subject},
	$self->{Header}->{'X-MMS-Transaction-ID'},
	)=@_;
}
sub setOptions{ # ������� ����� ���������
		my $self=shift;
	($self->{Header}->{'X-MMS-Message-Class'},
	 $self->{Header}->{'X-MMS-Priority'},
	 $self->{Header}->{'X-MMS-Read-Reply'},
	 )	=@_;
}
sub addContent{ # ���������� ������ � ���� mms ($contentType,$contentBody)
	
	use MIME::Base64;
	use MIME::QuotedPrint;
	my $self = shift;


	my($contentType,$content_ID,$filename,$contentBody)=@_;#$contentLocation,
	my $encodedBody="";
	if((!$contentType)&&(!$contentBody)){return 0;}

	$self->{dataBody}.="\nContent-Type: ".$contentType;
	$self->{dataBody}.="; name=".$filename if($filename);
	$self->{dataBody}.="\nContent-ID: <".$content_ID.">" if($content_ID);
	$self->{dataBody}.="\nContent-transfer-encoding: base64\n\n";
	
	$encodedBody=encode_base64($contentBody);
$self->{dataBody}.=$encodedBody;

	$self->{dataBody}.="\n--".$self->{mmsBorder};
	
 	
}
sub encodeMMS{# ������ mms ���������
	my $self = shift;
	my $header="";
	my $headerValue=$self->{'Header'};
	my ($key,$value);
	
while (($key, $value) = each %$headerValue)
 {if($value){$header.=$key.': '.$value."\n";}}

my $body=$header."\n--".$self->{mmsBorder}.$self->{dataBody}."--\n";#$
	return $body;
}
sub resetBody{# �������� ������ � ����� ���������
	my $self=shift;
	$self->{dataBody}="";	
}


1;
