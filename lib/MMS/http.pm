package MMS::http;
 
use strict;
use Data::Dumper;

sub new{# �����������
  my ($class) = @_;
  my $self = {};
  $self->{url}=undef;
  $self->{body}="";
  $self->{Host}=undef;
  $self->{User}=undef;
  $self->{Password}=undef;
  $self->{BillingData}=undef;
  $self->{Answer}=undef;
  $self->{Status}=0;
  $self->{ErrorLine}="";
   
  
  $self->{log} = undef;
 
  bless $self, $class;
  return $self;
}
sub setLog{
	my $self=shift;
	($self->{log})=(@_);
}
sub setHost{# ������������� ���������� �������
	my $self=shift;
	($self->{Host})=(@_);
}
sub setAuthorization{
	my $self=shift;
	($self->{User},$self->{Password})=(@_);
}
sub setBillingData{
	my $self=shift;
	($self->{BillingData})=(@_);
}
sub sendRequest{# ����������� �������
	use LWP::UserAgent;
  use HTTP::Headers;
	
	my $self=shift;	
	my($url,$XMLbody,$MMSbody)=@_;
	
	my $papborder="pap-border";
	my $content="--$papborder\n$XMLbody\n--$papborder\n$MMSbody\n--$papborder--";
	$self->{Status}=0;
	$self->{ErrorLine}="";
		
	
###  ������������ ��������� http �������
	my $heads=new HTTP::Headers;
			$heads->push_header('X-MMSC-Billingdata'=> '"'.$self->{BillingData}.'"') if($self->{BillingData});	
			$heads->authorization_basic($self->{User},$self->{Password}) if( $self->{User}&&$self->{Password}); 
			$heads->push_header('Host'=>$self->{Host})if($self->{Host});
			$heads->content_length(length($content));
	

### ������������ � ���������� �������
	my $req = HTTP::Request->new('POST',
																$url,
																$heads,
																$content);
	
	$req->content_type('multipart/related; boundary=pap-border; type="application/xml"');
	
	$self->{body}=$req->as_string();

  
  my $ua = LWP::UserAgent->new;
  		$ua->agent("VAS/0.1 ");



   my $sended=1;
   
   if($sended){# ���� �������� ���������, �� ��������� ������
   	my $res=$ua->request($req);


  # Check the outcome of the response
  # ������/������������� ������
  if ($res->is_success) {# ��������� ��������� �������.
  	$self->{Answer}=$res->content;
  	$self->{Status}=1;
  	if($self->{log}->is_debug()){#������ ������� �� ������ �������
				$self->{log}->info( "The server reply is:\n".$self->{'Answer'}); #Dumper($res->content)  
			}      
  } else {# ��������� ������ ��� �������
  	#$self->{ErrorLine}=$res->status_line();
  		#if($self->{log}->is_debug()){
								$self->{ErrorLine}=$res->status_line()." Couldn't send http request."; #.$req->as_string()  
								#$self->{log}->error(
			#}else{
		 #     $self->{ErrorLine}=$res->status_line(). " Couldn't send http request"; #$self->{log}->error()
     		  $self->{Answer}=undef;
    #}
  }
}
else{# ��������� ��������� �������
	$self->{log}->warn("The request not sended (Test message mode)");
	$self->{Status}=1;
}
}
sub getAnswer{# ������ ����������� ������
	my $self=shift;	
	sub isCorrect($){
		my($string)=@_;
		my($res)=1;
		my(@key)=('push-id','code','desc');#,'message-id'
		foreach (@key)
		{$res=0 if(index($string,$_)<0);}
		return $res;
	}
# ����� �������� �����... ���� ��������� � ��������� �������
	sub getValue ($$){# ��������� �������� ����� �� ������
		my($string,$key)=@_;
		my $position=index($string,$key)+1;
		return '' if ($position==0);
			$string=substr($string,$position);
			$string=substr($string,index($string,"\"")+1);
			$string=substr($string,0,index($string,"\""));
		return $string;	
}	
	my($string)=@_;
	my %responce=('push-id'=>'000','code'=>'1000','desc'=>'1000:Test is Ok','message-id'=>'0000');
	my $key;
	if (!isCorrect($self->{Answer})){
		$responce{'desc'}='Incorrect Server Reply';
		$responce{'code'}='0000';
		return %responce;
	}
	while (($key,) = each %responce) {
		%responce->{$key}=getValue($self->{Answer},$key);
}	
  return %responce;# ���� � ������� ������ � ����������� ������� ������ �������

	
}
sub Sended{
	my $self=shift;
	return $self->{Status};
}
sub getError{
	my $self=shift;
	return $self->{ErrorLine};
}
sub getBody{
	my $self=shift;
	return $self->{body};	
}
1;