# -*- encoding: cp1251 -*-
package Translit;
require Exporter;
use Carp;
use Exporter;
use strict;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

@ISA = qw(Exporter);
@EXPORT = qw();
@EXPORT_OK = qw
    (
     translit_text
     utf8_text
     );
%EXPORT_TAGS =
    (
     'all'    => [@EXPORT_OK],
     );

no warnings;
# Traslit hash
my %translit_hash = qw(
� A
� B
� V
� G
� D
� E
� E
� Zh
� Z
� I
� J
� K
� L
� M
� N
� O
� P
� R
� S
� T
� U
� F
� H
� C
� Ch
� Sh
� Sch
� '
� Y
� '
� E
� Yu
� Ya
� a
� b
� v
� g
� d
� e
� e
� zh
� z
� i
� j
� k
� l
� m
� n
� o
� p
� r
� s
� t
� u
� f
� h
� c
� ch
� sh
� sch
� '
� y
� '
� e
� yu
� ya
\x84 "
\x8b <
\x91 '
\x92 '
\x93 "
\x94 "
\x96 -
\x97 -
\x9b >
\xb9 N
\xbb "
);

my %utf8_hash = qw(
� &#1040;
� &#1041;
� &#1042;
� &#1043;
� &#1044;
� &#1045;
� &#1045;
� &#1046;
� &#1047;
� &#1048;
� &#1049;
� &#1050;
� &#1051;
� &#1052;
� &#1053;
� &#1054;
� &#1055;
� &#1056;
� &#1057;
� &#1058;
� &#1059;
� &#1060;
� &#1061;
� &#1062;
� &#1063;
� &#1064;
� &#1065;
� &#1066;
� &#1067;
� &#1068;
� &#1069;
� &#1070;
� &#1071;
� &#1072;
� &#1073;
� &#1074;
� &#1075;
� &#1076;
� &#1077;
� &#1077;
� &#1078;
� &#1079;
� &#1080;
� &#1081;
� &#1082;
� &#1083;
� &#1084;
� &#1085;
� &#1086;
� &#1087;
� &#1088;
� &#1089;
� &#1090;
� &#1091;
� &#1092;
� &#1093;
� &#1094;
� &#1095;
� &#1096;
� &#1097;
� &#1098;
� &#1099;
� &#1100;
� &#1101;
� &#1102;
� &#1103;
);
use warnings;

#
# ������� �������� � �������� �� ��������� (��������)
#
sub translit_text
{
    my ($text) = @_;
    my ($translit_text) = '';
    foreach (split //, $text) {
        if (/[\x80-\xff]/) {
            $translit_text.= ($translit_hash{$_} || '?');
        } else {
            $translit_text.= $_;
        }
    }
    return $translit_text;
}

#
# ������� �������� � �������� �� ��������� (utf8)
#
sub utf8_text
{
    my ($text) = @_;
    my ($utf8_text) = '';
    #
    foreach (split //, $text) {
        if (/[�-��-���]/) {
            $utf8_text.= $utf8_hash{$_};
        } else {
            $utf8_text.= $_;
        }
    }
    return $utf8_text;
}

1;
