// -*- c++ -*-
#ifndef EASY_H__
#define EASY_H__

#include <string>
#include <boost/shared_ptr.hpp>
#include <curl/curl.h>

#include "charbuf.h"

class CurlMulti;
class CurlEasy 
{
public:
  static CurlEasy *to_object(CURL *curl);
  static size_t curl_write_function(void *ptr, size_t size, size_t nmemb, void *stream);
  static size_t curl_header_function(void *ptr, size_t size, size_t nmemb, void *stream);

private:
  friend class CurlMulti;
  CURL *curl_;
  boost::shared_ptr<CharBuffer> data_;
  std::string url_;
  enum { NONE, HEAD, BODY } state_;

public:
  int private_int_;
  int private_int2_;
  void *private_ptr_;

  CurlEasy(const char *url, int timeout, int connect_timeout);
  ~CurlEasy();
};



#endif // EASY_H__
