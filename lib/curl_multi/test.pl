#!/usr/bin/perl
use strict;
use locale;
use warnings;
use curl_multi;
use Data::Dumper;

my $c = new curl_multi::CurlMulti(128);

foreach (1..2) {
	$c->add_url("https://ozon.ru/", $_);
}

my $stress_test = 0;

my $i = 0;
while ($stress_test ? $i < 1000 :  $c->used_slots ) {
	$c->perform(10);
	my $r;
	while (exists (($r=$c->fetch_result())->{status})) {
		if ( $stress_test and $i ++ > 10 ) {
			last;
		}
		print "Iter: $i\n";
		print "Slot: ".$c->used_slots()."\n";
		print "Got back: $r->{status}\n";
		print "$r->{contents}\n";
		$c->add_url("http://192.168.252.14/", $i)
		  if $stress_test;
	}
}
