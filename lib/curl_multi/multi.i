%module curl_multi
%{
#include "multi.h"
%}

%typemap(out) FetchResult {
  HV* tmp = newHV();

  if ( $1.status ) {
	SV* t_status = newSVpv($1.status, 0);
	hv_store(tmp, "status", 6, t_status, 0);
  }

  SV* t_data = newSViv($1.private_data);
  hv_store(tmp, "data", 4, t_data, 0);

  if ( $1.contents and $1.contents->length() ) {
	SV* t_contents = newSVpvn($1.contents->buffer(), $1.contents->length());
	hv_store(tmp, "contents", 8, t_contents, 0);
  }

  $result = sv_2mortal(newRV_noinc((SV*) tmp));
  argvi++;
}

class CurlMulti {
public:
  CurlMulti(int initial_slots, int timeout, int connect_timeout);
  ~CurlMulti();

  void add_url(const char *url, int data);
  int perform(int timeout);
  int used_slots();
  FetchResult fetch_result();

};
