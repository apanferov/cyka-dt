#! /bin/sh
INSTALL=${INSTALL:-'/gate/asg5/lib'}

swig -perl -c++ -o multi_wrap.cxx multi.i
perl Makefile.PL
make
cp -pf ./blib/lib/curl_multi.pm $INSTALL
cp -rpf ./blib/arch/auto $INSTALL
