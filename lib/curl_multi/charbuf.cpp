#include <stdlib.h>
#include <string.h>
#include "charbuf.h"

CharBuffer::CharBuffer(int alloc) : allocated_len_(alloc), len_(0), data_(0)
{
  data_ = static_cast<char*>(malloc(allocated_len_));
}

CharBuffer::~CharBuffer()
{
  free(data_);
}

void CharBuffer::clear()
{
  len_ = 0;
}

void CharBuffer::swap(CharBuffer &other)
{

  char *tmpd = data_;
  int tmpl = len_;
  int tmpa = allocated_len_;

  data_ = other.data_;
  len_ = other.len_;
  allocated_len_ = other.allocated_len_;
  
  other.data_ = tmpd;
  other.len_ = tmpl;
  other.allocated_len_ = tmpa;
}

void CharBuffer::append(const char *data, int len) 
{
  if ( len_ + len > allocated_len_ ) {
	CharBuffer tmp( allocated_len_ * 2 > len_ + len ? allocated_len_ * 2 : (len_ + len) * 2 );
	tmp.append(data_, len_);
	tmp.swap(*this);
  }
  memcpy(data_+len_, data, len);
  len_ += len;
}

void CharBuffer::append(const char *data)
{
  append(data, strlen(data));
}

