// -*- c++ -*-
#ifndef MULTI_H__
#define MULTI_H__

#include "easy.h"
#include <vector>
#include <list>

#include <iostream>

struct FetchResult {
  const char *status;
  int private_data;
  boost::shared_ptr<CharBuffer> contents;

  FetchResult();

  FetchResult(const FetchResult& other)
	: status(other.status), private_data(other.private_data), contents(other.contents)
  {
	std::cerr << "FRCOPY: " << status << std::endl;
  }

};


class CurlMulti {
  static bool global_init_;

  CURLM *curlm_;
  std::vector<CurlEasy*> handles_;
  int used_slots_;
  std::list<int> free_handles_;
  int still_running_;
  int easy_timeout_, easy_connect_timeout_;

  int alloc_handle_slot();
  void free_slot(int slot);
  void init_slots(int initial_slots);

public:

  typedef struct FetchResult FetchResult;

  int still_running() { return still_running_; }
  int used_slots() { return used_slots_; }

  void add(CurlEasy* easy);
  void add_url(const char *url, int data);

  int perform(int timeout);
  FetchResult fetch_result();

  CurlMulti(int initial_slots, int timeout, int connect_timeout);
  ~CurlMulti();
};

#endif // MULTI_H__
