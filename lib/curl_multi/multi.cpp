#include "multi.h"

#include <iostream>
using std::cerr;
using std::endl;
using std::vector;

bool CurlMulti::global_init_ = false;

void CurlMulti::init_slots(int init_slots) 
{
  vector<CurlEasy*> tmp(init_slots, static_cast<CurlEasy*>(0));
  tmp.swap(handles_);
  for (int i = 0; i < handles_.size(); i++ ) {
	free_handles_.push_front(i);
  }
}

CurlMulti::CurlMulti(int initial_slots, int timeout, int connect_timeout)
  : used_slots_(0), easy_timeout_(timeout), easy_connect_timeout_(connect_timeout)
{
  if ( ! global_init_ ) {
	global_init_ = true;
	curl_global_init(CURL_GLOBAL_ALL);
  }
  curlm_ = curl_multi_init();
  init_slots(initial_slots);
}

CurlMulti::~CurlMulti() 
{
  curl_multi_cleanup(curlm_);
}

int CurlMulti::alloc_handle_slot () {
  if (free_handles_.empty()) {
	int orig_size = handles_.size();
	int new_size = orig_size * 2;
	handles_.resize(new_size);
	for (int i = orig_size; i < new_size; i++ ) {
	  free_handles_.push_front(i);
	}
  }
  int ret = free_handles_.front();
  free_handles_.pop_front();
  used_slots_++;
  return ret;
}

void CurlMulti::add(CurlEasy* easy)
{
  int slot = alloc_handle_slot();
  handles_[slot] = easy;
  easy->private_int_ = slot;
  curl_multi_add_handle(curlm_, easy->curl_);
}

void CurlMulti::add_url(const char *url, int data)
{
  CurlEasy *easy = new CurlEasy(url, easy_timeout_, easy_connect_timeout_);
  easy->private_int2_ = data;
  add(easy);
}

int CurlMulti::perform(int timeout)
{
  while(CURLM_CALL_MULTI_PERFORM ==
		curl_multi_perform(curlm_, &still_running_));

  if ( not still_running_ ) 
	return 1;

  struct timeval tv;
  int rc; /* select() return code */

  fd_set fdread;
  fd_set fdwrite;
  fd_set fdexcep;
  int maxfd;

  FD_ZERO(&fdread);
  FD_ZERO(&fdwrite);
  FD_ZERO(&fdexcep);

  /* set a suitable timeout to play around with */
  tv.tv_sec = timeout;
  tv.tv_usec = 0;
  curl_multi_fdset(curlm_, &fdread, &fdwrite, &fdexcep, &maxfd);

  rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &tv);

  switch(rc) {
  case -1:
	/* select error */
	return 0;
  case 0:
	return 1;
  default:
	while(CURLM_CALL_MULTI_PERFORM ==
		  curl_multi_perform(curlm_, &still_running_));
	return 1;
  }
}

CurlMulti::FetchResult CurlMulti::fetch_result()
{
  CURLMsg *msg;
  int msgs_left;
  FetchResult ret;
  if ((msg = curl_multi_info_read(curlm_, &msgs_left))) {
	if ( msg->msg == CURLMSG_DONE ) {
	  CurlEasy *e = CurlEasy::to_object(msg->easy_handle);
	  free_slot(e->private_int_);

	  ret.private_data = e->private_int2_;

	  if ( msg->data.result == CURLE_OK ) {
		// Transfer completed successfully
		ret.contents = e->data_;
		ret.status = "OK";
	  } else {
		// There was some error
		ret.status = curl_easy_strerror(msg->data.result);
	  }

	  curl_multi_remove_handle(curlm_, msg->easy_handle);
	  delete e;
	}
  }
  return ret;
}

void CurlMulti::free_slot(int slot)
{
  free_handles_.push_front(slot);
  handles_[slot] = 0;
  used_slots_--;
}

CurlMulti::FetchResult::FetchResult()
  : status(0), private_data(0)
{
}

