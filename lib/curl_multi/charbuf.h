// -*- c++ -*-

#ifndef CHARBUFFER_H__
#define CHARBUFFER_H__

#include <string>

class CharBuffer 
{
  int len_;
  int allocated_len_;
  char *data_;
public:
  CharBuffer(int alloc = 4096);
  ~CharBuffer();

  void swap(CharBuffer &other);
  int length()
  {
	return len_;
  }
  const char *buffer()
  {
	return data_;
  }

  std::string to_string()
  {
	return std::string(data_, data_ + len_);
  }


  void append(const char *data, int len);
  void append(const char *data);

  void clear();

};

#endif // CHARBUFFER_H__
