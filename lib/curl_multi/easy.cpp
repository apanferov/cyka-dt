#include "easy.h"

#include <cstring>
using std::strstr;

size_t CurlEasy::curl_write_function(void *ptr, size_t size, size_t nmemb, void *stream)
{
  CurlEasy *obj = reinterpret_cast<CurlEasy*>(stream);
  obj->data_->append(static_cast<char*>(ptr), size * nmemb);
  return size * nmemb;
}

size_t CurlEasy::curl_header_function(void *ptr, size_t size, size_t nmemb, void *stream)
{
  CurlEasy *obj = reinterpret_cast<CurlEasy*>(stream);
  char *header = static_cast<char*>(ptr);

  if ( strstr(header, "HTTP/1.") == header ) {
	obj->data_->clear();
  }

  obj->data_->append(header, size * nmemb);
  return size * nmemb;
}

CurlEasy *CurlEasy::to_object(CURL *curl)
{
  char *temp;
  curl_easy_getinfo(curl, CURLINFO_PRIVATE, &temp);
  return reinterpret_cast<CurlEasy*>(temp);
}

CurlEasy::CurlEasy(const char *url, int timeout, int connect_timeout)
  : url_(url), data_(new CharBuffer()), curl_(curl_easy_init()), state_(NONE)
{
  curl_easy_setopt(curl_, CURLOPT_PRIVATE, reinterpret_cast<char*>(this));
  curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, CurlEasy::curl_write_function);
  curl_easy_setopt(curl_, CURLOPT_WRITEDATA, reinterpret_cast<void*>(this));
  curl_easy_setopt(curl_, CURLOPT_URL, url);
  curl_easy_setopt(curl_, CURLOPT_FOLLOWLOCATION, 1);
  curl_easy_setopt(curl_, CURLOPT_MAXREDIRS, 2);
  curl_easy_setopt(curl_, CURLOPT_HEADERFUNCTION, CurlEasy::curl_header_function);
  curl_easy_setopt(curl_, CURLOPT_HEADERDATA, reinterpret_cast<void*>(this));
  curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYPEER, 0);
  curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYHOST, 0);
  curl_easy_setopt(curl_, CURLOPT_CONNECTTIMEOUT, connect_timeout);
  curl_easy_setopt(curl_, CURLOPT_TIMEOUT, timeout);
}

CurlEasy::~CurlEasy()
{
  curl_easy_cleanup(curl_);
}
