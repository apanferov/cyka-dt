#include <boost/shared_ptr.hpp>
#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <time.h>
#include "charbuf.h"

using std::cout;
using std::endl;
using std::cerr;
using boost::shared_ptr;
using std::vector;
using std::list;
using std::ostream_iterator;

#include "multi.h"

int main(int argc, char **argv)
{
  CurlMulti m(128);

  for (int i = 0; i < 100; i++) {
	m.add_url("http://192.168.252.14/", 0);
  }

  int i = 0;
  while ( i < 1000 ) {
	m.perform(10);
	CurlMulti::FetchResult r;
	while ( (r=m.fetch_result()).status ) {
	  if ( i++ > 1000 )
		break;
	  cerr << "Iter: " << i << endl;
	  cerr << "Used: " << m.used_slots() << endl;
	  cerr << "Status: " << r.status << endl;
// 	  copy(r.contents->buffer(), r.contents->buffer() + r.contents->length(),
// 		   ostream_iterator<char>(cout));
	  m.add_url("http://192.168.252.14/", 0);
	}
  }

}


