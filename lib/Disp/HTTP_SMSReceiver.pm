#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Disp::HTTP_SMSReceiver;
use strict;
use warnings;
use base 'Exporter';
our $VERSION    = '1.001';
our $SOAP_GLOBAL;

use DBI;
use Data::Dumper;
#use CGI qw/:standard/;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;

use SOAP::Lite +trace => qw/all/;
use SOAP::Transport::HTTP;
use SOAP::Transport::IO;
use XML::Simple;
use IO::Scalar;
use Encode qw(encode decode);
use POSIX ':sys_wait_h';

use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Disp::AltDB;
use Disp::UmcsSOAP;

use constant process_tab => {
    http_umcs => { pref => 'RCV_',
                   receive => sub {
                       my ( $self ) = @_;

					   # Rip em all
					   while ( waitpid(-1, WNOHANG) > 0 ) {
					   }

                       # HTTP_SMSClient �������� CGI::Vars, �������
                       # POST-������ ��� ����������. �������
                       # ���������� SOAP::Transport::IO ������
                       # SOAP::Tranport::HTTP::CGI.
                       my $data = $self->{query}->param("POSTDATA");
                       $self->{log}->debug($data);
                       my $out = "";
                       my $ofh = new IO::Scalar \$out;
                       my $fh = new IO::Scalar \$data;

                       # ���������� ����������. �� �����, ��� �
                       # SOAP::Lite �� ����������� � ����� ����������
                       # ����������.
                       local $SOAP_GLOBAL = { db => $self->{db},
                                              log => $self->{log},
                                              host => $self->{url},
                                              smsc_id => $self->{smsc_id},
											  operator_id => $self->{operator_id}
                                            };

                       # $self->{log}->info(Dumper($self), $SOAP_GLOBAL);

                       SOAP::Transport::IO::Server
                           -> new( in => $fh, out => $ofh )
                           -> dispatch_with({ 'http://www.estylesoft.com/umcs/shop/' => 'Disp::UmcsSOAP' })
                           -> handle;


                       # $self->{log}->debug(Dumper($SOAP_GLOBAL->{sms}));
                       my $sms = $SOAP_GLOBAL->{sms};
                       $self->{log}->info("Message $sms->{id} was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                       $self->{log}->debug("RESPONSE:\n".$out);

                       print $self->{query}->header(-type=>'text/xml', -charset=>'utf-8');
                       print $out;

                       return $sms;
                   },
                   create_resp => sub {
                       return;
                   },
                   create_fatal_resp => sub {
                       print header(-type=>'text/xml', -charset=>'utf-8');
                       print <<EOF;
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Body>
       <SOAP-ENV:Fault>
           <faultcode>SOAP-ENV:Server</faultcode>
           <faultstring>Uncaught exception while handling RPC request</faultstring>
           <detail>
               <e:myfaultdetails xmlns:e="http://alt1.ru/soap/fault">
                 <message>Nothing here, all details in log-file</message>
               </e:myfaultdetails>
           </detail>
       </SOAP-ENV:Fault>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF
                   }
                 },
    http_ivanovo => { pref => 'TRN_', # IVANOVO
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{dest},
                                                abonent => $params->{abonent},
                                                msg => $params->{sms},
                                                data_coding => 0,
                                                esm_class => 0,
                                        transaction_id => $params->{id},
                                        };
                                        return $sms unless ($sms->{abonent} =~ /\d+/);
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
           create_resp => sub {
               my ($self,$sms) = @_;
               my $smss;
               if ($sms->{abonent} =~ /^\d+$/) {
                   my $smc = 0; my $fc=0;
                   while ((!$smc) and ($fc < 24)) {
                       sleep 5; $fc++;
                       $self->{log}->debug('Fetching...');
                       $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$sms->{transaction_id});
                       $smc = scalar keys(%$smss);
                   }
               } else {
                   $self->{log}->debug('Fetching...');
                   $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},undef,$sms->{num});
               }
               my $sms_count = scalar keys(%$smss);
               my $resp = '';
               if ($sms_count) {
                   $self->{log}->debug("$sms_count message(s) fetched.");
                   foreach my $sms (sort {$a->{id} <=> $b->{id}} values(%$smss)) {

                       if ($sms->{esm_class} & 0x40) {
                           $resp .= $self->xml_hex_message($sms);
                       } else {
                           $resp .= $self->xml_concat_message($sms);
                       }

                       # XXX �� ������� �� ����, ��� ������ �� ���������� �������?
                       $self->{db}->set_sms_new_status($sms->{id},2,0,0,0);
                       $self->{db}->set_sms_delivered_status($sms->{id});
                       $self->{log}->info("Message SMS$sms->{id} was delivered to abonent.");
                   }
               }
               $resp = '<?xml version="1.0" encoding="windows-1251" ?>'."\n<content>\n$resp</content>";
               $self->{log}->debug("Response:\n $resp");
               print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
               print $resp;
           },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print '<?xml version="1.0" encoding="windows-1251" ?>';
                                        print "\n<content>\n</content>";
                                },
        },
    http_teleclick => {         pref => 'RCV_', # TELECLICK
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{dstaddr},
                                                abonent => $params->{srcaddr},
                                                msg => $params->{text},
                                                data_coding => 0,
                                                esm_class => 0,
                                        transaction_id => $params->{trans_id},
                                        link_id => $params->{linkid},
                                        };
                                        #($sms->{num},$sms->{transaction_id}) = split(/#/,$sms->{num});
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_ukraine2 => {  pref => 'RCV_', # UKRAINE OLD
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};
                                        my $msg= ( $params->{keyword}." ".$params->{text});
                                        $msg = encode('cp1251',decode('utf8',$msg));
                                        $msg =~ s/\+quot;/"'"/ieg;

                                        return undef if ($params->{action} eq 'delivery-report');
                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{'short-number'},
                                                abonent => $params->{phone},
                                                msg =>$msg,
                                                data_coding => 0,
                                                esm_class => 0,
                                                link_id => $params->{provider},
                                        transaction_id => $params->{'transaction-id'},
                                        };

                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html');
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_ukraine => {   pref => 'RCV_', # UKRAINE NIKITA NEW
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};
                                        my $msg= ( $params->{keyword}." ".$params->{text});
                                        $msg = encode('cp1251',decode('utf8',$msg));
                                        $msg =~ s/\+quot;/"'"/ieg;

                                        if (lc($params->{action}) eq 'deliver') {
                                        my $sms = {
                                                        smsc_id => $self->{smsc_id},
                                                        num => $params->{'short-number'},
                                                        abonent => $params->{phone},
                                                        msg =>$msg,
                                                        data_coding => 0,
                                                        esm_class => 0,
                                                        link_id => $params->{provider},
                                                transaction_id => $params->{'transaction-id'},
                                                };
                                                if ($params->{provider} =~ /^kstar/) {
                                                        $self->{log}->debug(Dumper($sms));
                                                        my $id = $self->{db}->insert_sms_into_insms_ignored($sms);
                                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                                        my $msg = {
                                                                abonent => $sms->{abonent},
                                                                smsc_id => $self->{smsc_id},
                                                                operator_id => 0,
                                                                num => $sms->{num},
                                                                data_coding => 0,
                                                                esm_class => 0,
                                                                transaction_id => $sms->{transaction_id},
                                                                test_flag => 20,
                                                                parts => ['�������. ����� �� 2-�� SMS']};
                                                        $self->{db}->move_outbox_to_outsms($msg);
                                                        $self->{log}->info("Testing SMS created.");
                                                        return $sms;
                                                } else {
                                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                                        $self->{log}->debug(Dumper($sms));
                                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                                        return $sms;
                                                }
                                        }
                                        elsif (lc($params->{action}) eq 'delivery-report') {
                                                my $transaction_id = $params->{'transaction-id'};
                                                $self->{log}->info("Report for message with tr-id=$transaction_id was received.");
                                                my $ignored_id = $self->{db}->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? limit 1',undef,$self->{smsc_id},$transaction_id);
                                                if ($ignored_id) {
                                                        my $id = $self->{db}->move_ignored_to_insms($ignored_id);
                                                        $self->{log}->info("Message SMS$id was passed to system.");
                                                }
                                                return undef;
                                        }
                                        return undef;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html');
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_pribaltika => {        pref => 'RCV_', # PRIBALTICA
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => ($params->{price}) ? "$params->{short}:$params->{price}" : $params->{short},
                                                abonent => $params->{phone},
                                                msg => encode('cp1251',decode('UTF-8',$params->{text})),
                                                data_coding => 0,
                                                esm_class => 0,
                                                link_id => "$params->{country}#$params->{operator}",
                                        };
                                        # �������
                                        if ($sms->{link_id} eq 'lv#lmt')        {
                                                $sms->{transaction_id} = substr($sms->{abonent},8);
                                                $sms->{abonent} = '371'.substr($sms->{abonent},0,8);
                                        }
                                        elsif ($sms->{link_id} eq 'ee#emt-ee')  {
                                                $sms->{abonent} = '372'.$sms->{abonent};
                                        }
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },
    http_kievstar => {  pref => 'RCV_', # KIEVSTAR
                        receive => sub {
                                        my($self) = @_;
                                        my $xml = $self->{query}->param('POSTDATA');
                                        $self->{log}->debug($xml);
                                        my $ref = XMLin($xml, ForceContent => 1);
                                        $self->{log}->debug(Dumper($ref));

                                        if ($ref->{service}->{content} eq 'content-request') {
											my $sms = {
													   smsc_id => $self->{smsc_id},
													   num => $ref->{sn}->{content},
													   abonent => $ref->{'sin'}->{content},
													   msg => encode('cp1251',$ref->{body}->{content}),
													   data_coding => 0,
													   esm_class => 0,
													   transaction_id => $ref->{rid},
													  };
											$self->{log}->debug(Dumper($sms));

											if ($sms->{num} eq '4565') { # Exception for MO shortcode 4565
												my $id = $self->{db}->insert_sms_into_insms($sms);
												$self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
												return $sms;
											}

											my $id = $self->{db}->insert_sms_into_insms_ignored($sms);
											$self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

#											unless ($self->{patterns}) {
												my @patterns;
												foreach my $id (1032,1137) {
													my $pattern = $self->{db}->get_value('select pattern from set_service_t where id=?',undef,$id);
													my $pattern_c;
													eval { $pattern_c = qr/$pattern/i; };
													if ($@) {
														$self->warning( "Wrong plugin pattern detected for SVC$id");
													} else {
														push(@patterns,$pattern_c);
													}
												}
												$self->{patterns} = \@patterns;
#											}
											my $match;
											foreach (@{$self->{patterns}}) {
												$match = 1 if ($sms->{msg} =~ m/$_/gi);
											}
											unless ($match) {
												$self->{log}->info("Message SMS$id was ignored.");
												return $sms;
											}

											my $msg = {
													   abonent => $sms->{abonent},
													   smsc_id => $self->{smsc_id},
													   operator_id => 0,
													   num => $sms->{num},
													   data_coding => 0,
													   esm_class => 0,
													   transaction_id => $sms->{transaction_id},
													   test_flag => 20,
													   parts => ['�������. ����� �� 2-�� SMS']};
											$self->{db}->move_outbox_to_outsms($msg);
											$self->{log}->info("Testing SMS created.");

											return $sms;
                                        }
                                        elsif ($ref->{service}->{content} eq 'delivery-report') {
											my $outsms_id = $ref->{mid};
											my $transaction_id = $ref->{rid};
											my $status = $ref->{status}->{content};
											my $error = $ref->{status}->{error};
											if ($status eq 'Delivered') {
												$self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
												$self->{db}->set_sms_report_status($outsms_id,12);
												my $ignored_id = $self->{db}->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? limit 1',undef,$self->{smsc_id},$transaction_id);
												if ($ignored_id) {
													my $id = $self->{db}->move_ignored_to_insms($ignored_id);
													$self->{log}->info("Message SMS$id was passed to system.");
												}

											}
											elsif ($status eq 'Delivering') {
												$self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
												$self->{db}->set_sms_report_status($outsms_id,11);
											}

											elsif ($status eq 'Undeliverable') {
												$self->{log}->info("Report for message SMS$outsms_id was received. Status: $status. Reason: $error");
												$self->{db}->set_sms_report_status($outsms_id,13);
											}
#                                               elsif ($status eq 'Undelivered') {
#                                                       $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
#                                                       $self->{db}->set_sms_report_status($outsms_id,13);
#                                               }
											else {
												$self->{log}->info("Report for message SMS$outsms_id was received. Status: $status. Reason: $error");
												$self->{db}->set_sms_report_status($outsms_id,13);
												yell("Unknown delivery report status for SMS$outsms_id\n$xml",code => 'WARNING',process => $self->{pr_name},
													 header => "$self->{pr_name}: Unknown delivery report status",ignore => { log4perl => 1});
											}
											return undef;
                                        }
                                        return undef;


                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/xml');
                                        print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<report>\n<status>Accepted</status>\n</report>\n";
                                        $self->{log}->debug("Response: Accepted");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                        $self->{log}->FATAL("$err");
                                },
        },


    http_aaa => {       pref => 'RCV_', # AAA
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{num},
                                                abonent => $params->{abonent},
                                                msg => $params->{text}, # encode('cp1251',decode('UTF-8',$params->{text})),
                                                data_coding => 0,
                                                esm_class => 0,
                                                transaction_id => $params->{transaction_id},
                                        };
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },


};

sub new {
   my ($class,$smsc_id,$debug_level,$smpp_mode) = @_;

        Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
        my $smpp_mode_name = 'rcv';

        my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);
        my %log_conf = (
           "log4perl.rootLogger"       => "$debug_level, Logfile",
           "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
                "log4perl.appender.Logfile.recreate" => 1,
                "log4perl.appender.Logfile.recreate_check_interval" => 300,
                "log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
                "log4perl.appender.Logfile.umask" => "0000",
           "log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
           "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
                "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','sms_client')
        );
        Log::Log4perl->init( \%log_conf);
        my $log=get_logger("");
        $log->info("Starting...");

        #  Connecting to DB
        my $db = new Disp::AltDB();
	Config->set_dbh($db->{db});
        my $cnf = $db->get_smsc_configuration($smsc_id);
#       $ENV{FCGI_SOCKET_PATH} = $cnf->{host}.":".$cnf->{port};
        $ENV{FCGI_SOCKET_PATH} = "127.0.0.1:".$cnf->{port};
        $ENV{FCGI_LISTEN_QUEUE} = 100;
        require CGI::Fast;
   my $self = { db              => $db,
                log             => $log,
                pr_name         => $pr_name,
                smpp_mode_name  => $smpp_mode_name,
                state           => 'stopped',
                status          => 'start',
                smsc_id         => $smsc_id,
                debug_level     => $debug_level,
                smpp_mode       => $smpp_mode,
              };
   @{$self}{keys(%$cnf)} = values(%$cnf);

   bless $self, $class;
   return $self;
}

#sub new {
#   my ($class,$query) = @_;
#   my $self = {query => $query, pr_name => 'HTTP_SMS'};
#   bless $self, $class;
#   return $self;
#}

sub run {
        my $self = shift;
        my $retval = $self->run_receiver();
        $self->enter_state('stopped');
        $self->{log}->info("Stopped.\n\n\n");
        return $retval;
}

sub run_receiver {
   my $self = shift;

   eval {
           $self->enter_state('run');
                $self->{log}->info("Started.");

                while (my $query = new CGI::Fast) {
                        last if (!$query);
                        $self->{query} = $query;
                        $self->{params} = $query->Vars;
                        $self->{log}->debug("Parameters: ".Dumper($self->{params}));
                        $self->{db}->reconnect();
                        my $sms = &{process_tab->{$self->{client_type}}{receive}}($self);
                        &{process_tab->{$self->{client_type}}{create_resp}}($self,$sms);
                       $self->heartbeat;
                        $self->routines;
                };
                $self->{log}->info('Stopping...');
        };
        if ($@) {
                $self->fatal_error($@);
                &{process_tab->{$self->{client_type}}{create_fatal_resp}}($self,$@) if ($self->{client_type} and exists(process_tab->{$self->{client_type}}));
                return 1;
        }
        return 0;
}


sub escape_value {
        my($self, $data) = @_;

        return '' unless(defined($data));

        $data =~ s/&/&amp;/sg;
        $data =~ s/</&lt;/sg;
        $data =~ s/>/&gt;/sg;
        $data =~ s/"/&quot;/sg;

        return $data;
}

sub xml_concat_message {
    my ( $self, $sms ) = @_;

    my $resp = "";

    # XXX - �� ���� ��� ������������ XML
    $resp .= "<sms>\n";
    $resp .= "<abonent>$sms->{abonent}</abonent>\n";
    $resp .= "<message type=\"concat\">".$self->escape_value($sms->{msg})."</message>\n";
    $resp .= "<dcs>$sms->{data_coding}</dcs>\n";
    $resp .= "</sms>\n";
    return $resp;
}

sub xml_hex_message {
    my ( $self, $sms ) = @_;

    my $resp = "";

    # XXX - �� ���� ��� ������������ XML
    $resp .= "<sms>\n";
    $resp .= "<abonent>$sms->{abonent}</abonent>\n";
    $resp .= "<message type=\"hex\">".unpack("H*", $sms->{msg})."</message>\n";
    $resp .= "<dcs>$sms->{data_coding}</dcs>\n";
    $resp .= "<esm>$sms->{esm_class}</esm>\n";
    $resp .= "</sms>\n";
    return $resp;
}


sub enter_state {
   my ($self,$state) = @_;
   if (! ($self->{state} eq $state)) {
                $self->{db}->set_process_state(uc($self->{smpp_mode_name}).'_'.$self->{smsc_id},uc($state),uc($self->{state}));
        $self->{state} = $state;
        $self->{log}->debug("Enter ".uc($state)." state.");
   }
   return 1;
}

sub heartbeat {
   my ($self) = @_;
        $self->{heartbeat} = 0 if (!$self->{heartbeat});
        if ((time() - $self->{heartbeat}) >= 60) {
                $self->{db}->heartbeat($self->{pr_name});
        $self->{log}->debug("HEARTBEAT.");
                $self->{heartbeat} = time();
        }
   return 1;
}

sub routines {
   my ($self) = @_;
        $self->{routines_time} = 0 if (!$self->{routines_time});
        if ((time() - $self->{routines_time}) >= 120) {
        $self->{log}->debug("Hour statistics: ".Dumper($self->{db}->{statistics}));
                $self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
        $self->{log}->debug("ROUTINES.");
                $self->{routines_time} = time();
        }
   return 1;
}

sub warning {
   my ($self,$err,$err2) = @_;
   $err2 = ($err2) ? "$err\n$err2" : $err;
   my $proc_name = uc($self->{smpp_mode_name}).'_'.$self->{smsc_id};
        $self->{log}->warn($err);
        yell($err2,code => 'WARNING',process => $proc_name,
                header => "$proc_name: $err",ignore => { log4perl => 1 });
   return 1;
}

sub fatal_error {
   my ($self,$err2) = @_;
   my ($err) = ($err2 =~ /^(.*?)$/m);
   my ($err_type) = ($err =~ /^(\w+_ERROR\d*):/s);
   $err_type = $err_type || 'FATAL_ERROR';
        $self->{log}->fatal($err2);
        my $ignore_email = ($err_type eq 'CONNECT_ERROR');
        $self->{db}->reconnect();
        yell($err2,code => $err_type, process => $self->{pr_name},
                header => "$self->{pr_name}: $err",ignore => { log4perl => 1, 'e-mail' => $ignore_email });
   return 1;
}

1;
