#!/usr/bin/perl -w
# $Id: $


=head1 NAME

Disp::DaemonManager

=head1 SYNOPSIS

=head1 DESCRIPTION

��������� �������� - ���������/�������������/������������� �� �
��������� ������� �������.

����� ��������� ������� ����� ���������, � ��� �������� � ���������
�����������:

=over

=item *

���������� ���������� � ����� ������ � �������� ������ - � ������� ������.

=item *

��������� ���������� - � ��������� � ����������� ������.

=item *

����������� ������ ������ - � �����������.

=back


=head1 METHODS

=cut
package Disp::DaemonManager;
use strict;
use warnings;
use base 'Disp::Object';
use POSIX ':sys_wait_h';
use IO::File;
use Disp::Config;
use Disp::DaemonManager;
use Disp::DaemonManager::Child;
use Disp::DaemonManager::PermanentSource;
use Disp::DaemonManager::TranscieverSource;
use Disp::Utils;
use Data::Dumper;
use Log::Log4perl qw/get_logger/;

my $log = get_logger();

our $VERSION = 0.1;		# ������ ������.

sub new {                         # () object

=head3 new (%params) object

�����������. ��������� ��� (������, ��� ������������� � ����
������). � Σ� ��������� ��������� ��������:

B<��������>	B<��������>	B<�������� �� ���������>
C<reload_period>	������������� ������������� ������������, � ��������.	15
C<sleep_cycle>	�� ������� ������ ������� ����� ���������� ��������� �������� �������� �����.	5

=cut

	my $self = shift->SUPER::new();
	my %params = @_;

	$self->{status_file} = $params{status_file} || 'monitor.status';
	$self->{pid_file} = $params{pid_file} || 'monitor.pid';
	$self->{reload_period} = $params{reload_period} || 15;
	$self->{sleep_cycle} = $params{sleep_cycle} || 1;

	$self->{sources} = [];
	$self->{next_query_time} = 0;

	$self->{grace_period} = 40;

	# ����������� id ������ -> pid
	$self->{running} = {};

	# ��� ��������� ������, ���� - id ������
	# �������� - ����� ����� { child => new Disp::DaemonManager::Child(...) }
	# Disp::DaemonManager::Child �������� � ���� ����� ����
	# ����������� ��������� ����� �������������� ��������� ����������.
	$self->{all_known} = {};

	# ���������� � ������������� ��������� � ����
	# pid => ��������_�����_����������
	# ��� ���������� ������ ��� ������������ TERM � ���� �����������
	# ����� � �������. ���� ����� �� ����������� �� ���������� �������
	# ������� - �� ��� ���������� ��� SIGKILL.
	$self->{termination} = {};

	# �� ������������� ������ �������� ������ ���������� �������.
	$self->{block_restart} = {};

	# ����������� pid -> id ������
	$self->{pids} = {};

	# ���� �������
	$self->{status_fh} = new IO::File($self->{status_file}, O_RDWR|O_CREAT);
	die "Can't open status_file $!" unless $self->{status_fh};

	return $self;
}

sub add_data_source {             # (%params) void

=head3 add_data_source (%params) void

��������� �������� ������ � DaemonManager.

 $dm->add_data_source( source => Disp::Some::DataSource->new(...), prefix => 'prefix');

=cut

	my ( $self, %params ) = @_;
	$log->info("Adding data source '".ref($params{source})."' with prefix '$params{prefix}'");
	push @{$self->{sources}}, \%params;
}

sub run {                         # () void
	my ( $self ) = @_;

	daemonize;

	$self->init_signals;

	open PID, "> $self->{pid_file}";
	print PID "                    $$\n";
	close PID;

	$log->info("Monitor main loop reached");
	eval {
		while (1) {
			if ($self->need_reload) {
				my $new_state = $self->{quit_flag} ? { } : $self->fetch_from_sources;
				$self->apply_differences($new_state);
			}

			# XXX Add some protection when child, created by
			# process_child() can be reaped by wait_pid() (this may
			# happen when child dies very early).
			while (my ($pid, $normal) = $self->wait_pid()) {
			    $self->process_child($pid, $normal);
			}

			$self->check_termination;
			$self->check_blocked;

			$log->debug(Dumper($self->{pids}));
			$log->debug(Dumper($self->{termination}));
			$self->dump_status;

			if ($self->{quit_flag} and not scalar keys %{$self->{termination}}) {
				last;
			}

			sleep(2);
		}
	};
	if ($@) {
		$log->error("Abnormal monitor termination: $@");
	}
	unlink $self->{pid_file};
	unlink $self->{status_file};
	$log->info("Cleanup finished, exiting monitor");
}


sub need_reload {                 # () boolean

=head3 need_reload () boolean

����������, �� ������ �� ��� ���� ������������ ����������������
������.

=cut

	my ( $self ) = @_;
	if ($self->{forced_reload} or $self->{next_query_time} < time()) {
		return 1;
	}
	return 0;
}

sub fetch_from_sources {          # () hashref
	my ( $self ) = @_;
	my %result;
	$log->debug("Querying sources START");
	foreach my $src (@{$self->{sources}}) {
		$log->debug("From source: $src->{prefix}");
		my $i = 0;
		foreach my $child (@{$src->{source}->fetch()}) {
			$result{$src->{prefix}.'___'.$child->id} = { child => $child };
			$i++;
		}
		$log->debug("Got $i items");
	}
	$self->{next_query_time} = time() + $self->{reload_period};
	if ($self->{forced_reload}) {
		$self->{forced_reload} = 0;
		$log->info("There was forced reload in progress, clearing flag");
	}
	$log->debug("Querying sources END");
	return \%result;
}

sub apply_differences {           # ($new_state) void
	my ( $self, $new_state ) = @_;
	my %current_names = map { $_ => 1 } keys %{$self->{all_known}};
	$log->debug("Checking differences in configurations");
	while (my($name, $data) = each %$new_state) {
		if ($current_names{$name}) {
			$log->debug("$name already exists");
			if ($self->{all_known}{$name}{child} != $data->{child}) {
				$log->info("Configuration of $name changed");

				# Throw away old data
				$self->{all_known}{$name} = $data;

				if (defined ($self->{running}{$name}) and $self->{running}{$name} > 0) {
					$log->info("Queueing $name termination");
					$self->queue_termination($name);
				} else {
					$log->info("Child $name configuration changed, but no running");
					$self->spawn_if_needed($name);
				}
			}
			delete $current_names{$name};
		} else {
			$log->debug("New child $name spawning");
			$self->{all_known}{$name} = $data;
			$self->spawn_if_needed($name);
		}
	}
	foreach my $left (keys %current_names) {
		$log->info("Terminating deleted child $left");
		$self->queue_termination($left);
		delete $self->{all_known}{$left};
	}
}

sub check_blocked {               # () void
	my ( $self ) = @_;
	return if $self->{quit_flag};
	foreach (keys %{$self->{block_restart}}) {
		$log->info("Checking whether blocked '$_' must be restarted");
		next if $self->is_blocked($_);
		$log->info("Blocked child '$_' spawning");
		$self->spawn_if_needed($_);
		delete $self->{block_restart}{$_};
	}
}

sub is_blocked {                  # ($child_id) boolean
	my ( $self, $child_id ) = @_;
	return 0 unless defined $self->{block_restart}{$child_id};
	return 1 if $self->{block_restart}{$child_id} > time();
	return 0;
}

sub spawn_if_needed {             # ($child_id) void
	my ( $self, $child_id ) = @_;
	my $child = $self->{all_known}{$child_id}{child};
	$log->debug("Deciding whether to spawn $child_id");

	if ($self->{running}{child_id}) {
		$log->error("Child '$child_id' already spawned");
		return;
	}

	if ($child->{active} and not $self->is_blocked($child_id)) {
		my $pid = $child->spawn;
		$self->{running}{$child_id} = $pid;
		$self->{pids}{$pid} = $child_id;
		$self->{process_start_time}{$child_id} = time;
		$log->debug(Dumper($child));
		$log->info("Spawned $child_id/$self->{running}{$child_id}");
	}
}

sub wait_pid {                    # () integer
	my ( $self ) = @_;
	my $pid = waitpid(-1, WNOHANG);
	return unless $pid > 0;

	my $normal_termination = POSIX::WIFEXITED($?) && (POSIX::WEXITSTATUS($?) == 0);
	return ($pid, $normal_termination);
}

sub check_termination {           # () void
	my ( $self ) = @_;
	while (my ($k, $v) = each %{$self->{termination}}) {
		$log->info("Killcheck $k, $v, ".time());
		next if $v + $self->{grace_period} > time();
		$log->info("KILLing $self->{pids}{$k}/$k with signal 9");
		$self->terminate_child($self->{pids}{$k}, 9);
	}
}

sub process_child {               # ($pid) void
	my ( $self, $pid, $normal ) = @_;
	my $child_id = $self->{pids}{$pid};
	my $child_entry = $self->{all_known}{$child_id};
	$log->info("Handling child $child_id/$pid termination");

	if (not $normal and ($child_entry->{child}{restart_timeout}||0) > 0) {
	    $self->{block_restart}{$child_id} = time() + $child_entry->{child}{restart_timeout};
	    $log->info("Delaying child '$child_id' restart");
	}

	delete $self->{running}{$child_id};
	delete $self->{pids}{$pid};
	delete $self->{termination}{$pid};
	delete $self->{process_start_time}{$child_id};

	if ($child_entry) {
		$log->debug("Child $child_id still here, maybe starting");
		$self->spawn_if_needed($child_id);
	}
}

sub queue_termination {           # ($child_id) void
	my ( $self, $child_id ) = @_;
	my $pid = $self->{running}{$child_id};
	unless ($pid) {
		$log->warn("No $child_id running");
		return;
	}
	$self->{termination}{$pid} = time();
	$self->terminate_child($child_id, 15);
}

sub terminate_child {             # ($child_id, $signal) void
	# ��������������� �������� ������, ��������� ���������� ���������
	# ��������.
	my ( $self, $child_id, $signal ) = @_;
	my $pid;
	unless (defined($pid=$self->{running}{$child_id})) {
		$log->warn("No pid for $child_id");
		return;
	}
	$log->info("$child_id/$pid signalled with signal $signal");
	kill $signal, $pid;
}

sub dump_status {                 # () void
	my ( $self ) = @_;
	my $fh = $self->{status_fh};

	seek $fh, 0, 0;
	print $fh time()."\n";
	while (my ($name,$v) = each %{$self->{all_known}}) {
		my $pid = "<none>";
		$pid = $self->{running}{$name} || $pid;
		my $status = ($pid =~ /^\d+$/ ) ? "RUNNING" : "NOT_RUNNING";
		my $time = defined($self->{process_start_time}{$name}) ?
		  (time - $self->{process_start_time}{$name}) : -1;
		print $fh "$name\t$status\t$pid\t$time\n";
		truncate $fh, tell $fh;
	}
}

sub init_signals {                # () void
	my ( $self ) = @_;
	$SIG{TERM} = sub { $self->_real_sig_term(@_); };
	$SIG{INT} = sub { $self->_real_sig_term(@_); };
	$SIG{HUP} = sub { $self->_real_sig_hup(@_); };
}

sub _real_sig_term {              # ($signal) void
	my ( $self, $signal ) = @_;
	$self->{quit_flag} = 1;
	$self->{forced_reload} = 1;
}

sub _real_sig_hup {               # ($signal) void
	my ( $self, $signal ) = @_;
	$self->{forced_reload} = 1;
}

sub run_monitor {
	my ( $init_sub, %attrs ) = @_;

	eval {
		Config::init(skip_db_connect => 1, config_file => $attrs{config_file});
		Log::Log4perl::init($attrs{log_config});
		yell("Starting $attrs{kind} monitor",
			 process => "MONITOR($attrs{kind})",
			 header => "MONITOR($attrs{kind}): Starting",
			 ignore => { database => 1 });
		my $dm = new Disp::DaemonManager( status_file => $attrs{status_file},
										  pid_file => $attrs{pid_file} );
		$init_sub->($dm);
		$dm->run;
		yell("Monitor $attrs{kind} finished",
			 process => "MONITOR($attrs{kind})",
			 header => "MONITOR($attrs{kind}): Monitor finished");
	};
	if ($@) {
		yell("ABNORMAL MONITOR TERMINATION: $@",
			 code => 'FATAL_ERROR',
			 process => "MONITOR($attrs{kind})",
			 header => "MONITOR: ABNORMAL MONITOR TERMINATION",
			 ignore => { database => 1 });
	}
}

sub perm_daemons {
	my $self = shift;
	my $prefix = shift;

	my @childs;

	while (@_ >= 2) {
		my $name = shift;
		my ($dir, $script) = @{(shift)};
		push @childs, Disp::DaemonManager::Child->new
		  ( id => $name,
		    active => 1,
		    restart_timeout => 5,
		    subdir => Config->param('system_root')."/".$dir,
		    execute => $script );
	}
	$self->add_data_source(source => Disp::DaemonManager::PermanentSource->new(\@childs),
						   prefix => $prefix);
	return;
}
1;
