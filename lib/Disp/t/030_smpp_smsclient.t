#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 6;
use Test::Exception;
use Data::Dumper;
use Disp::Test;
use Time::HiRes qw/usleep/;

sub sos;

Disp::Test::init();

BEGIN { use_ok('Disp::SMPP_SMSClient'); }

my $s = new Disp::SMPP_SMSClient(5, 'ERROR', 3);

$s->{last_throttle_count} = 125;
$s->{output_throttle} = 5;
sos;

$s->update_throttle;
is($s->{last_throttle_count}, 0, "Throttle cout resetted.");

is($s->throttle_send_limit, 5, "Full throttle limit when no messages was sent");

$s->update_throttle(3);
is($s->throttle_send_limit, 2, "Full throttle limit when 3 messages was sent");

$s->update_throttle(3);

ok(not(defined($s->throttle_send_limit)), "Throtle limit reached");

sos;
is($s->throttle_send_limit, 5, "Full throttle limit when no messages was sent - second try");
















sub sos { # start of second
	my $time = time;
	while ($time == time) {
		usleep(10000);
	}
}
