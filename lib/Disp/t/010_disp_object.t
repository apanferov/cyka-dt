#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 7;
use Test::Exception;

BEGIN { use_ok('Disp::Object') };

is_deeply(Disp::Object->new(), {}, "new() without params");

my $params = { a => 1, b => 2 };
my $tmp;
is_deeply($tmp=Disp::Object->new($params), $params, "new() with single hashref");

$tmp->{a} = 25;
is $params->{a}, 1, "new() with single hashref made fresh copy";

dies_ok { my $a = 'aaa'; Disp::Object->new(\$a)} "new() with single non-hashref argument dies";

dies_ok { Disp::Object->new ( a => b => 'c' ) } "new() dies with odd number of arguments > 1";

is_deeply(Disp::Object->new( a => 5, b => 7 ), { a => 5, b => 7 }, "initialized hash from even number of arguments")

