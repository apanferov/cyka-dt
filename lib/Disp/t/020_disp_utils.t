#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 25;
use Test::Exception;
use Data::Dumper;
use DBI;


BEGIN { use_ok('Disp::Utils') };

# misc utils
my $aref = [ { d7 => 'a', priority => 7 },
			 { d5 => 'b', priority => 5 },
			 { d3 => 'c', priority => 3 },
		   ];

my $pr_comparator = sub { $_[0]{priority} > $_[1]{priority}};
ordered_insert($aref, { d9 => 'd', priority => 9 }, $pr_comparator); # At start
ordered_insert($aref, { d4 => 'e', priority => 4 }, $pr_comparator); # In the middle
ordered_insert($aref, { d2 => 'f', priority => 2 }, $pr_comparator); # At end

is_deeply $aref, [
				  { d9 => 'd', priority => 9 },
				  { d7 => 'a', priority => 7 },
				  { d5 => 'b', priority => 5 },
				  { d4 => 'e', priority => 4 },
				  { d3 => 'c', priority => 3 },
				  { d2 => 'f', priority => 2 },
				 ], "ordered_insert works properly";

# SQL part testing
our $dbh = DBI->connect("DBI:mysql:test", "test", "test", { AutoCommit => 1,
														    RaiseError => 1 });

my $test_data = <<EOF;
drop table if exists test_020_utils
create table test_020_utils (col1 INTEGER PRIMARY KEY AUTO_INCREMENT, col2 VARCHAR(255))
insert into test_020_utils values (1, 'a1')
insert into test_020_utils values (2, 'b2')
insert into test_020_utils values (3, 'c3')
EOF

foreach (split /\n/, $test_data) {
	$dbh->do($_);
}

is_deeply selectall('test_020_utils'), [ { col1 => 1, col2 => 'a1' },
										 { col1 => 2, col2 => 'b2' },
										 { col1 => 3, col2 => 'c3' } ], "selectall() is working";

is_deeply selectall_hash('test_020_utils', 'col1'), { 1 => { col1 => 1, col2 => 'a1' },
													  2 => { col1 => 2, col2 => 'b2' },
													  3 => { col1 => 3, col2 => 'c3' },
													}, "selectall_hash() is working";

# Parser testing
my @data;
{
	local $/;
	@data = split /=-=-=-=\n/, scalar <DATA>;
}

my $a = parse_response($data[0]);
# Good response, one section
ok $a->{result};
is scalar(@{$a->{sections}}), 1;
is_deeply $a->{sections}[0]{destinations}, [ qw/111 222 333/ ];
is $a->{sections}[0]{status}, 'reply';
is $a->{sections}[0]{'bill-outgoing'}, 'bo';
is $a->{sections}[0]{data} , "text";

# Good response, two sections
$a = parse_response($data[1]);
ok $a->{result};
is scalar(@{$a->{sections}}), 2;
is_deeply $a->{sections}[0]{destinations}, [ qw/111 222 333/ ];
is_deeply $a->{sections}[1]{destinations}, [ qw/555 666/ ];

is $a->{sections}[0]{status}, 'reply';
is $a->{sections}[0]{'bill-incoming'}, 'bi';
is $a->{sections}[0]{data} , "text";

is $a->{sections}[1]{status}, 'notify';
is $a->{sections}[1]{'bill-outgoing'}, 'bo';
is $a->{sections}[1]{data} , "moretext";

# Bad response
$a = parse_response($data[2]);
ok not $a->{result};

# Response with mixed http/section header
$a = parse_response($data[3]);

is $a->{sections}[0]{status}, 'reply-mixed';
is $a->{sections}[0]{'bill-incoming'}, 'bi123';
is_deeply $a->{sections}[0]{destinations}, [qw/79265550017 79265550018 79265550019/];
is $a->{sections}[0]{data}, 'MIXED!';

END {
	if ($dbh) {
		$dbh->do("drop table if exists test_020_utils");
	}
}


package Config;
sub dbh {
	return $main::dbh;
}

package main;
__DATA__
HTTP/1.1 200 OK
Date: Wed, 14 Sep 2005 08:25:58 GMT
Server: Apache
Connection: close
Content-Type: text/plain

status: reply
bill-outgoing: bo
destination: 111, 222, 333
service-id: 1

text
=-=-=-=
HTTP/1.1 200 OK
Date: Wed, 14 Sep 2005 08:25:58 GMT
Server: Apache
Connection: close
Content-Type: text/plain

status: reply
bill-incoming: bi
destination: 111, 222, 333

text


status: notify
bill-outgoing: bo
destination: 555, 666

moretext
=-=-=-=
HTTP/1.1 403 Forbidden
Date: Wed, 14 Sep 2005 08:25:58 GMT
Server: Apache
Connection: close
Content-Type: text/plain

Some shit here
=-=-=-=
HTTP/1.1 200 OK
Date: Wed, 14 Sep 2005 08:25:58 GMT
Server: Apache
Connection: close
Content-Type: text/plain
X-Alt1-status: reply-mixed
X-Alt1-bill-incoming: bi123
X-Alt1-destination: 79265550017
X-Alt1-destination: 79265550018, 79265550019

MIXED!
