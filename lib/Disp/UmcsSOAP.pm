# -*- coding: utf-8 -*-
package Disp::UmcsSOAP;
use strict;
use warnings;
use Disp::Utils;
use XML::Simple;

=head1 Статусы

1 - Запрос на резервирование - неудача
2 - Запрос на резервирование - успех
3 - Подтверждение резервирования - неудача
4 - Подтверждение резервирования - успех
5 - Подтверждение резервирования - неудача с другой стороны
11 - Подтверждение резервирования - мы отказались, удача с другой стороны
12 - Подтверждение резервирования - мы отказалиь, неудача с другой стороны
6 - Покупка принята в обработку
7 - Покупка - не прошла
8 - Покупка - прошла
9 - Покупка - мы отказались
10 - Покупка - мы отказались, и оттуда - ошибка
13 - Явный отказ абонента от покупки
14 - Принято в обработку, не уложилось по времени - UMCS оповещено
15 - Принято в обработку, не уложилось по времени - UMCS не оповещено из ошибки с их стороны.
=cut

my %certificates = ( 1 => { ca_file => "/gate/asg5/etc/ca-bee.crt",
							pkcs12_file => "/gate/asg5/etc/umcs_a1.pfx",
						  },
					 2 => { ca_file => "/gate/asg5/etc/ca-bee.crt",
							pkcs12_file => "/gate/asg5/etc/umcs_a1_alt.pfx",
						  },
				   );

my %num_to_cert_map = ( 3121 => 1,
						8353 => 1,
						_default_ => 1,
						840501 => 2,
						840502 => 2,
						840503 => 2,
						840504 => 2,
						840511 => 2,
						840512 => 2,
						840513 => 2,
						840514 => 2,
						840521 => 2,
						840522 => 2,
						840531 => 2,
						840532 => 2,
						845021 => 2,
						845022 => 2,
						845023 => 2,
						845024 => 2,
						845031 => 2,
						845032 => 2,
						845033 => 2,
						845034 => 2,
					  );

use SOAP::Transport::HTTP;
use SOAP::Lite +trace => qw/all/;

use base qw/SOAP::Server::Parameters Disp::Object/;
use Data::Dumper;
use Encode qw(encode decode);

sub cert_conf {
	my ( $num, $log ) = @_;

	my $conf = $certificates{$num_to_cert_map{$num}} || $certificates{$num} || $certificates{$num_to_cert_map{_default_}};
	$ENV{HTTPS_CA_FILE} = $conf->{ca_file};
	$ENV{HTTPS_PKCS12_FILE} = $conf->{pkcs12_file};

	print "Certificates for $num: @{[Dumper($conf)]}\n";

 	$log->debug("Certificates for $num: @{[Dumper($conf)]}")
 	  if $log;
}

sub certificate_groups {
	keys %certificates;
}

sub certificate_numbers {
	my ( $certificate_group ) = @_;
	grep { $num_to_cert_map{$_} == $certificate_group } keys %num_to_cert_map;
}


sub ReportError {
	return 0;
}

my %sfdr_reason_names = ( 0 => 'Purchase is missing in FEO',
						  1 => 'Purchase is missing in UMCS',
						  2 => 'Purchase is in error state',
						  3 => 'Purchase price different in FEO and UMCS' );

my %sfdr_status_names = ( 0 => 'Reserved',
						  1 => 'Completed',
						  2 => 'Abonent rejection',
						  4 => 'UMCS error',
						  5 => 'Partner error',
						  7 => 'Undefined error',
						  8 => 'Transfered to registry',
						  9 => 'Unconfirmed' );

sub SendFileDailyReport {
    my ( $dailyReport ) =
      SOAP::Server::Parameters::byName([qw[dailyReport]], @_);

    my $SOAP_GLOBAL = $Disp::HTTP_SMSReceiver::SOAP_GLOBAL;

	my $parsed = XMLin($dailyReport, ForceArray => ['item'], KeyAttr => []);

	my $feoCode = $parsed->{code};

	for my $item (@{$parsed->{item}}) {
		my $order_id = $item->{order};
		my $umcs_price = $item->{price};
		my $our_price = $item->{priceFeo};
		my $reason = $item->{reason};
		my $errorCode = $item->{errorCode};
		my $status = $item->{status};

		yell("FEO: $feoCode\nSTATUS: $sfdr_status_names{$item->{status}}\nREASON: $sfdr_reason_names{$item->{reason}}\n".Dumper($item), code => 'ERROR', header => "RCV_$SOAP_GLOBAL->{smsc_id}: UMCS reported difference", process => "rcv_$SOAP_GLOBAL->{smsc_id}" );

		my $request_data = Config->dbh->selectrow_hashref("select * from tr_umcs_request where order_id = ?", undef, $order_id);

		unless ($request_data) {
			yell("Can't load order '$order_id' while processing UMCS FEO",
				 code => 'ERROR',
				 header => "RCV_$SOAP_GLOBAL->{smsc_id}: Can't update feo record",
				 process => 'rcv_$SOAP_GLOBAL->{smsc_id}' );
			next;
		}

		$umcs_price =~ s/,/./;

		Config->dbh->do('update tr_umcs_requests set umcs_status = ?, umcs_error = ?, umcs_price = ? where order_id = ?', undef,
						$status, $reason, $umcs_price, $order_id);

	}
}

sub UnReserveGood {
    my ( $orderId, $result ) =
      SOAP::Server::Parameters::byName([qw[orderID result]], @_);

    my $SOAP_GLOBAL = $Disp::HTTP_SMSReceiver::SOAP_GLOBAL;

    my $s = $SOAP_GLOBAL->{db}->get_sms_by_msg_id($SOAP_GLOBAL->{smsc_id}, $orderId);
    if ($s->{id}) {
	$SOAP_GLOBAL->{db}->set_sms_report_status($s->{id}, 13, undef, $s);
	$SOAP_GLOBAL->{log}->warn("Message OUTSMS$s->{id} was UNRESERVED with result($result).");
    }

    return Disp::UmcsSOAP::Helper::soap_return([0, 'UnReserveGoodResult', 'int']);
}

sub PurchaseAsync {
    my ( $orderId, $CTN, $goodPhone, $smsText ) =
      SOAP::Server::Parameters::byName([qw[orderID CTN goodPhone smsText]], @_);

    my $SOAP_GLOBAL = $Disp::HTTP_SMSReceiver::SOAP_GLOBAL;
    $SOAP_GLOBAL->{log}->info("PurchaseAsync(parameters): orderId=$orderId, CTN=$CTN, goodPhone=$goodPhone, smsText=$smsText");

    my $sms = { abonent => "7".$CTN,
                smsc_id => $SOAP_GLOBAL->{smsc_id},
                num => $goodPhone,
                data_coding => 0,
                esm_class => 0,
                transaction_id => $orderId,
				};

    if (my($osms_id) = $smsText =~ /^##(\d+)$/) {
	my $db = $SOAP_GLOBAL->{db}->clone;
	$SOAP_GLOBAL->{log}->debug('Duplicating AltDB');

        my $osms = $db->{db}->selectrow_hashref("select * from tr_outsmsa where id=? limit 1",
	    undef, $osms_id);
	$SOAP_GLOBAL->{log}->debug('SMS ' . Dumper($osms));

	Disp::UmcsSOAP::Helper::db_update($orderId, { purchase_date => today() });

	my($code, $pid) = Disp::UmcsSOAP::Helper::delayed_exec(sub {
	    my $umcs_status = $sms->{num} =~ /free/ ? 1 : 0;
	    $umcs_status = 1 unless($osms->{num} == $sms->{num} and $osms->{abonent} == $sms->{abonent});
	    my $our_status;
	    $SOAP_GLOBAL->{log}->info("Finishing the purchase. abonent=$osms->{abonent}, num=$osms->{num}, smsid=$osms->{id}");

$SOAP_GLOBAL->{log}->debug("PurchaseEnd umcs_status=$umcs_status");
	my $result;
	eval {
	    Disp::UmcsSOAP::cert_conf($osms->{num}, $SOAP_GLOBAL->{log});
            $result = Disp::UmcsSOAP::Helper::PurchaseEnd(
                $SOAP_GLOBAL->{host},
                'http://www.estylesoft.com/umcs/shopinterface/',
                $sms->{transaction_id},
                decode('cp1251',$osms->{msg}),
		$umcs_status
	        );
	};
	if ($@) {$SOAP_GLOBAL->{log}->error("IN_SOAP: $@"); die;}
$SOAP_GLOBAL->{log}->debug("PurchaseEnd result=$result");
	    if ($result == 0) {
		$our_status = $umcs_status == 0 ? 8 : 9;
            } else {
		$our_status = $umcs_status == 0 ? 7 : 10;
	    }
	    Disp::UmcsSOAP::Helper::db_update($sms->{transaction_id}, { our_status => $our_status });

	    if ($result == 0) {
		$db->set_sms_report_status($osms->{id},12,undef,$osms);
		$db->update_billing_($osms->{inbox_id}, $osms->{outbox_id});
		$SOAP_GLOBAL->{log}->info("Message SMS$osms->{id} was delivered to abonent.");
	    } else {
		$db->set_sms_report_status($osms->{id},255,undef,$osms);
		$SOAP_GLOBAL->{log}->warn("Message SMS$osms->{id} wasn't delivered.");
	    }
	});
	$SOAP_GLOBAL->{log}->debug("PurchaseEnd spawn to $pid");
    }
	else {
		$sms->{msg} = encode('cp1251', $smsText);
		Disp::UmcsSOAP::Helper::db_update($orderId, { our_status => 6,
												  purchase_date => today()},);
		$sms->{id} = $SOAP_GLOBAL->{db}->insert_sms_into_insms($sms);
	}

	$SOAP_GLOBAL->{sms} = $sms;
    $SOAP_GLOBAL->{log}->debug(Dumper($sms));

    return Disp::UmcsSOAP::Helper::soap_return([0, 'PurchaseAsyncResult', 'int']);
}

sub ReserveGoodAsync {
    my ( $orderId, $CTN, $goodPhone, $smsText ) =
      SOAP::Server::Parameters::byName([qw[orderID CTN goodPhone smsText]], @_);

    # Always succeed
    my ($code, $pid);
    my $timeout = 90;

    my $SOAP_GLOBAL = $Disp::HTTP_SMSReceiver::SOAP_GLOBAL;

    if (my($outsms_id) = $smsText =~ /^##(\d+)$/) {
	$SOAP_GLOBAL->{db}->do("update tr_outsmsa set msg_id=$orderId where id=$outsms_id");
    }

	Disp::UmcsSOAP::Helper::db_insert_reserve($orderId, $goodPhone, $CTN);

    $SOAP_GLOBAL->{log}->info("ReserveGoodAsync(parameters): orderId=$orderId, CTN=$CTN, goodPhone=$goodPhone, smsText=$smsText");

    ($code, $pid) = Disp::UmcsSOAP::Helper::delayed_exec
      (sub {
		   my %update;

		   cert_conf($goodPhone);

           my $operator_id = $SOAP_GLOBAL->{operator_id};
		   my $price = $SOAP_GLOBAL->{db}->num_price($operator_id, $goodPhone);

		   my $userInfo = decode('UTF-8', "доступ к услуге");

		   my $result = 0;
		   if (not defined $price) {
			   $price = 0;
			   $result = 105;
			   $update{our_status} = 3;
			   $userInfo = decode('UTF-8', "Неверный код товара, уточните его и попробуйте снова!");
		   }

		   $update{our_price} = $price;

           my $host = $SOAP_GLOBAL->{host};
           $SOAP_GLOBAL->{log}->info("Host: $host, Price: $price, Result: $result");

           my $ReserveAsyncEndResult;
           eval {
               $ReserveAsyncEndResult = Disp::UmcsSOAP::Helper::ReserveAsyncEnd
                 ($host,
                  'http://www.estylesoft.com/umcs/shopinterface/',
                  $orderId, # sessionId
                  $price,
                  $userInfo, # userInfo
                  $result # result
                 );
           };

           if ($@) {
               $SOAP_GLOBAL->{log}->error("IN_SOAP: $@");
               die;
           }

		   if ($ReserveAsyncEndResult == 0) {
			   $update{our_status} = $result == 105 ? 11 : 4;
		   } else {
			   $update{our_status} = $result == 105 ? 12 : 5;
		   }
		   $update{reserve_async_end_result} = $ReserveAsyncEndResult;
		   Disp::UmcsSOAP::Helper::db_update($orderId, \%update);
           $SOAP_GLOBAL->{log}->info("ReserveAsyncEnd(orderId=$orderId) returned $ReserveAsyncEndResult");
       });

	Disp::UmcsSOAP::Helper::db_update($orderId, { our_status => ($code == 0 ? 2 : 1)});
    $SOAP_GLOBAL->{log}->info("ReserveGoodAsync(orderId=$orderId) - spawned $pid for ReserveAsyncEnd");
    return Disp::UmcsSOAP::Helper::soap_return([$code, 'ReserveGoodAsyncResult'],
                                               [$timeout, 'timeOut']);
}

package Disp::UmcsSOAP::Helper;
use POSIX;

sub db_load {
	my ( $order_id ) = @_;
	Config->dbh->selectrow_hashref("select * from tr_umcs_requests where order_id = ?", undef, $order_id);
}

sub db_insert_reserve {
	my ( $order_id, $num, $abonent ) = @_;

	Config->dbh->do("insert into tr_umcs_requests(order_id, abonent, num, reserve_date) values (?, ?, ?, now())", undef,
					$order_id, $abonent, $num);

}

sub db_update {
	my ( $order_id, $update ) = @_;

	my @columns = keys %$update;

	my $set_part = join ", ", map { "$_ = ?" } @columns;
	my $sql = "update tr_umcs_requests set $set_part where order_id = ?";

	Config->dbh->do($sql, undef, values %$update, $order_id);
}


sub soap_return {
    my @ret;
    foreach my $a (@_) {
        my $data;

        if (@$a == 1) {
            $data = SOAP::Data->new(value => $a->[0]);
        }
        if (@$a == 2) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type('');
        }
        if (@$a == 3) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type($a->[2]);
        }
        push @ret, $data;
    }
    return @ret;
}

# Возвращает 0 в случае удачи, и 1 - в противоположном.
sub delayed_exec (&) {
    my ( $s ) = @_;
    my $pid = fork;
    if ($pid > 0) {
        return 0, $pid;
    } elsif ($pid < 0) {
        return 1;
    } else {
        close *STDOUT;
		sleep 3;

		eval {
			$s->();
		};

		if ($@) {
				warn $@;
			}
		exit;
    }
}

sub PurchaseEnd {
    my ( $host, $uri, $sessionId, $userInfo, $result ) = @_;

    SOAP::Trace->import('transport', \&log_message);

    my $soap_result = SOAP::Lite
      -> default_ns($uri)
      -> proxy($host)
      -> on_action(sub {return ;})
      -> PurchaseEnd(soap_return([$sessionId, 'sessionId', 'long'],
                                 [$userInfo, 'userInfo', 'string' ],
                                 [$result, 'result', 'int']));
    return $soap_result->result;
}

sub log_message {
    my ($in) = @_;
    open my $fh, ">>", "fault.log";
    if (UNIVERSAL::isa($in, "HTTP::Request")) {
        # do something...
        print $fh "REQ:".$in->as_string."\n"; # ...for example
    } elsif (UNIVERSAL::isa($in, "HTTP::Response")) {
        # do something
        print $fh "RESP:".$in->as_string."\n";
    } else {
        print $fh "UNKNOWN IN:".Dumper($in);
    }
}


sub ReserveAsyncEnd {
    my ( $host, $uri, $sessionId, $price, $userInfo, $result) = @_;

    SOAP::Trace->import('transport', \&log_message);

    my $soap_result = SOAP::Lite
      -> default_ns($uri)
      -> proxy($host)
      -> on_action(sub {return ;})
      -> ReserveAsyncEnd(soap_return([$sessionId, 'sessionId', 'long'],
                                     [sprintf('%2f', $price), 'price', 'string'],
                                     [$userInfo, 'userInfo', 'string'],
                                     [$result, 'result', 'int']));
    return $soap_result->result;
}

sub ProcessPurchaseShortPhoneAsync {
	my ( $host, $uri, $shortPhone, $smsText, $CTN ) = @_;

    SOAP::Trace->import('transport', \&log_message);

    my $soap_result = SOAP::Lite
      -> default_ns($uri)
      -> proxy($host)
      -> on_action(sub {return ;})
      -> ProcessPurchaseShortPhoneAsync(soap_return([$shortPhone, 'shortPhone', 'string'],
                                     [$smsText, 'smsText', 'string'],
                                     [$CTN, 'CTN', 'string']));
    return $soap_result->result;
}

sub TakeFileDailyReport {
	my ( $host, $uri, $batch ) = @_;

    SOAP::Trace->import('transport', \&log_message);

    my $soap_result = SOAP::Lite
      -> default_ns($uri)
      -> proxy($host)
      -> on_action(sub {return ;})
      -> TakeFileDailyReport(soap_return([$batch, 'dailyReport', 'string']));
    return $soap_result->result;
}

1;

__END__
# https://gw2.aqt.ru:85/UMCSShopInterface/UMCSShopInterfaceWS.asmx
# https://gw2.aqt.ru:85/UMCSShopInterface/UMCSShopInterfaceWS.asmx

alter table tr_umcs_feo add column callback_processed tinyint not null default 0;

