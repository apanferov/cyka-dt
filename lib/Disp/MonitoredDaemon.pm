#!/usr/bin/perl -w
package Disp::MonitoredDaemon;
use strict;
use warnings;
our $VERSION	= 0.1;		# Версия пакета.

use base 'Disp::Object';

# Standart modules
use Log::Log4perl;
use Data::Dumper;
use FindBin;

# Our modules
use Disp::Config;
use Disp::AltDB;
use Disp::Utils;

# log4perl stubs
BEGIN {
    foreach my $stub (qw/debug info warn error fatal/) {
	no strict 'refs';
	*$stub = sub {
	    my $self = shift;
	    die "log4perl not initialized" unless $self->{log};
	    $self->{log}->$stub(@_);
	};
    }
}

sub new {
    my $self = shift->SUPER::new(@_);

    my @unspecified;
    for (qw/smsc_id debug_level client_type stub kind/) {
	push @unspecified, $_ unless exists $self->{$_};
    }

    if (@unspecified) {
	die "Following mandatory parameters is not specified: ".join(", ", @unspecified);
    }

    Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf",
		 skip_db_connect => 1);

    $self->{log} = $self->_init_log4perl;
    $self->{db} = new Disp::AltDB;
    $self->{conf} = $self->{db}->get_smsc_configuration($self->{smsc_id});
    $self->{terminating} = 0;
    $self->{idle_sleep} ||= 1;
    $self->{heartbeat_interval} ||= 30;
    $self->{state} = "";
    $self->{stop_timeout} ||= 10;
    $self->{maintainance_period} = 300;
    $self->{last_maintainance_time} = time;
    $self->{last_received_sms_time} = undef;

    $self->info("MonitoredDaemon constructed, stub is $self->{stub}");

    return $self;
}

sub run {
    my ( $self ) = @_;

    eval {
	$self->_enter_state('init');

	$self->info("Initializing signals");
	$self->_setup_signals;

	$self->info("Initializing real worker");
	# XXX - die ? return value?
	$self->{stub}->init($self);

	$self->info("Entering main loop");
	$self->_enter_state('run');

	my $prev_rc;
	while (1) {

	    $self->_heartbeat_if_needed;
	    $self->_maintainance_if_needed;

	    # We are still here, and nothing has done on previous
	    # iteration, so sleep a litle
	    if (($prev_rc||"") eq 'idle') {
		sleep($self->{idle_sleep})
	    }

	    $prev_rc = $self->{stub}->do_work($self, $self->{terminating});

	    my $term = $self->{terminating};
	    my $has_something_to_do = $self->{stub}->has_something_to_do($self);

	    # Work as usually
	    if (not $term) {
		next;
	    }

	    # Grace time is out, last notify to stub
	    if ($self->{terminating} + $self->{stop_timeout} > time) {
		$self->{stub}->hard_kill($self);
		last;
	    }

	    # stub don't want anything more to do
	    unless ($has_something_to_do) {
		last;
	    }
	}
	$self->{stub}->deinit($self);
    };

    if ($@) {
	die $@;
	# yell;
    }

    $self->_enter_state('stopped');
}

sub _setup_signals {
    my ( $self ) = @_;

    my $term_sub = sub { $self->{terminating} = time;
			 $self->info("Recieved $_[0] signal, terminating\n");
		     };

    $SIG{TERM} = $term_sub;
    $SIG{QUIT} = $term_sub;
    $SIG{INT} = $term_sub;
}

sub _init_log4perl {
    my ( $self ) = @_;

    $self->{version} ||= 'unknown';

    my $pr_name = $self->proc_name;

    my %log_conf =
      ( "log4perl.rootLogger"				      => "$self->{debug_level}, Logfile",
	"log4perl.appender.Logfile"			      => "Log::Log4perl::Appender::File",
	"log4perl.appender.Logfile.recreate"		      => 1,
	"log4perl.appender.Logfile.recreate_check_interval"   => 300,
	"log4perl.appender.Logfile.recreate_check_signal"   => 'USR1',
	"log4perl.appender.Logfile.umask"		      => "0000",
	"log4perl.appender.Logfile.filename"		      => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
	"log4perl.appender.Logfile.layout"		      => "Log::Log4perl::Layout::PatternLayout",
	"log4perl.appender.Logfile.layout.ConversionPattern"  => $self->{version}.Config->param('log_pattern','sms_client'),
      );

    Log::Log4perl->init(\%log_conf);

    return Log::Log4perl::get_logger("");
}

sub _heartbeat_if_needed {
    my ( $self ) = @_;
    $self->{db}->heartbeat(uc $self->proc_name,$self->{last_received_sms_time});
    return;
}

sub proc_name {
    my ( $self ) = @_;
    return "$self->{kind}_$self->{smsc_id}";
}

sub _enter_state {
    my ( $self, $state ) = @_;

    if ($self->{state} ne 'state') {
	$self->{db}->set_process_state( uc $self->proc_name,
					uc $state,
					uc $self->{state} );
	$self->{state} = $state;
	$self->info("Entered '$state' state");
    }

    return;
}

sub get_sms_ready_to_send {
    my ( $self, $limit ) = @_;
    $self->{db}->get_sms_ready_to_send($self->{smsc_id}, $limit);
}

sub _maintainance_if_needed {
    my ( $self ) = @_;

    if ($self->{last_maintainance_time} + $self->{maintainance_period} < time) {
	if ($self->{stub}->can("maintainance")) {
	    $self->{stub}->maintainance($self);
	    $self->{log}->info("ROUTINES");
	}
    }
}

1;

