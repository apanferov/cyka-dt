#!/usr/bin/perl
package Disp::InboxManager;
use strict;
use warnings;
use Data::Dumper;
use base 'Disp::Object';
use Disp::Utils qw/selectall/;
use Log::Log4perl qw/get_logger/;

=head1 NAME

InboxManager - �������� �������� ���, �������� �� ���
��������������. ����� ��������� ��������� ��������� �������.

=cut

my $log_messages = get_logger('messages');

our $VERSION = '1.0';

sub new {
	my $self = shift->SUPER::new(@_);

	# XXXX ������� now_processed � tr_inbox_restart !!!!

	# ������������ ������������� ����� ���� ��������� �� ������� ���������.
	$self->{max_inbox_id} = 0;

	return $self;
}

sub max_inbox_id
{
	my($self, $id) = @_;
	my $max_id = $self->{max_inbox_id};

	defined($id) && ($self->{max_inbox_id} = $id);
	$max_id;
}

=head3 fetch

��������� ��������� ����������(��� ������� ����������, ���� �������
������) �������� SMS ���������. ���� ����������� ���������� ���������
�� �������, �������� ��������� ��������� �� ������� �� ���������
���������, ����� ��� ������� ��� ������.

=cut
sub fetch {
	my ( $self, $count, $skip_restart ) = @_;
	my $dbh = Config->dbh;

	my @out = $self->_fetch_inbox($count);

	push @out, $self->_fetch_restart($count - @out)
	  if not($skip_restart) and (@out < $count);

	return \@out;
}

=pod _fetch_inbox

������� ������� �������� ���������� ��������� �� ������� tr_inbox.
���� ���������� ������� ������ - �� ���������� ������� ����.

=cut
sub _fetch_inbox {
    my ( $self, $count ) = @_;
    my @out;
    if ($count > 0) {
#HACK: SQL hack for MTS. was added to protect from fraud numbers
	my $messages = Config->dbh->selectall_arrayref(
		"select *, if(smsc_id=5,trust_level(abonent),null) as level from tr_inbox where id > $self->{max_inbox_id} order by id limit $count",
		{ Slice => {} });

	if (@$messages) {
	    foreach my $m (@$messages) {
		$log_messages->info("INBOX_FETCH: MSG$m->{id}");
		$self->{max_inbox_id} = $m->{id} if $m->{id} > $self->{max_inbox_id};
	    }
	    $log_messages->info("INBOX_HIGH_MARK_UPDATE: $self->{max_inbox_id}");
	    $log_messages->info("INBOX_FETCH_COMMIT: need $count, got ".scalar(@$messages));
	    push @out, @$messages;
	}
    }
    return @out;
}

sub _fetch_restart {
    my ( $self, $count ) = @_;

    my @out;

    my $messages = Config->dbh->selectall_arrayref
      ("select r.try_count, i.*, if(i.smsc_id=5,trust_level(i.abonent),null) as level from tr_inbox_restart r join tr_inboxa i on r.inbox_id = i.id where next_date < now() and not now_processed order by next_date limit $count",
       { Slice => {} });

    if (@$messages) {
	my $upd = Config->dbh->prepare("UPDATE tr_inbox_restart SET now_processed = 1 WHERE inbox_id = ?");
	$log_messages->info("INBOX_RESTART: ".(scalar @$messages)." fetched");

	foreach my $m (@$messages) {
	    $log_messages->info("INBOX_RESTART: FETCHED MSG$m->{id}");
	    $log_messages->info("INBOX_RESTART: marked RESTART$m->{id}")
	      if $upd->execute($m->{id});
	}

	push @out, @$messages;
    }
    return @out;
}

sub mark_processed {
    my ( $self, $msg ) = @_;

    if ( Config->dbh->do("DELETE FROM tr_inbox_restart WHERE inbox_id = ?", undef, $msg->{id}) > 0 ) {
	$log_messages->info("INBOX_RESTART_DELETE: $msg->{id}");
    }
    if ( Config->dbh->do("DELETE FROM tr_inbox WHERE id = ?", undef, $msg->{id}) > 0) {
	$log_messages->info("INBOX_DELETE: $msg->{id}");
    }
}

sub schedule_retry {
    my ( $self, $inbox_id, $try_count, $date_time ) = @_;

    # mark_processed() MUST be called early in
    # handle_service_response, and before each handle error. This is
    # current state of deals.

    Config->dbh->do("INSERT INTO tr_inbox_restart (inbox_id, try_count, next_date, now_processed) values (?,?,?,0)",
		    undef, $inbox_id, $try_count, $date_time);
}

1;

__END__

CREATE TABLE tr_inbox_restart
( inbox_id INTEGER UNSIGNED PRIMARY KEY,
  try_count INTEGER NOT NULL DEFAULT 0,
  next_date DATETIME NOT NULL,
  now_processed TINYINT(1) NOT NULL DEFAULT 0,
  INDEX tr_inbox_restart__next_date__idx (next_date)
);
