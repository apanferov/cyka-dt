# -*- encoding: utf-8; tab-width: 8 -*-
package Disp::CreditFSMWorld;
use strict;
use warnings;
use utf8;
use Carp;
use English '-no_match_vars';

our $VERSION = '1.0.0';

use POSIX qw/strftime/;
use LWP::UserAgent;
use HTTP::Request::Common;
use Log::Log4perl qw/get_logger/;

sub new {
    my ( $proto, $dbh ) = @_;
    my $self = bless {
        dbh => $dbh,
    }, $proto;
    return $self;
}

sub enqueue_partner_notify {
    my $self = shift;
    my %params = @_;

    my ( $value ) = $self->{dbh}->selectrow_array(
        "select value from tr_credit_sequences where name = ? for update", undef, 'notify'
    );

    if (defined($value)) {
        $value++
    } else {
        $value = 1;
    }

    $self->{dbh}->do(
        "update tr_credit_sequences set value = ? where name = ?", undef,
        $value, 'notify',
    );

    $params{notify_id} = $value;

    $self->{dbh}->do(
        "insert into tr_credit_notifies set " . join (q{,}, map { "$_ = ?" } keys %params ),
        undef, values %params,
    );

    return;
}

sub push_mt {
    my $self = shift;
    my %params = @_;

    my $ua = LWP::UserAgent->new;
    $ua->timeout(Config->param('lwp_timeout' => 'credit'));

    my $uri = Config->param('push_uri' => 'credit');

    no utf8;
    my $req = POST $uri,
        [
            service_id => $params{service_id},
            msg => $params{msg},
        ];

    my $resp = $ua->request($req);
    if ($resp->is_success and $resp->content =~ /\A(\d+)$/m ) {
        get_logger(__PACKAGE__)->debug("Delivered push $1");
        return 1;
    } else {
        get_logger(__PACKAGE__)->error("Push to $uri failed\nRequest was @{[$req->as_string]}\nResponse was @{[$resp->as_string]}");
        return;
    }
}

sub now {
    return strftime('%Y-%m-%d %H:%M:%S', localtime);
}

sub now_ts {
    return time;
}

sub param {
    shift;
    Config->param(@_);
}

sub gmt_offset {
    my ( $self, $abonent ) = @_;

    confess "Invalid abonent '$abonent'" unless $abonent =~ /^7(\d{3})(\d{7})/;

    my $data =  $self->{dbh}->selectrow_hashref(
        "select time_offset from set_mtt_time_offset where def = ? and ? between start and end", undef,
        $1, $2,
    );

    return $data ? ( $data->{time_offset} + 3 ) * 60 : 3 * 60; # Defaulting to moscow
}

sub fetch_unprocessed_message {
    my ( $self ) = @_;

    return $self->{dbh}->selectrow_hashref(
        "select * from tr_credit_messages where cyka_processed = 0 order by updated_at limit 1",
    );
}

sub mark_message_processed {
    my ( $self, $message ) = @_;

    $self->{dbh}->do(
        "update tr_credit_messages set cyka_processed = 1 where id = ?", undef, $message->{id},
    );

    return;
}

sub load_line {
    my ( $self, $credit_line_id ) = @_;
    return $self->{dbh}->selectrow_hashref(
        "select * from tr_credit_lines where credit_line_id = ?", undef, $credit_line_id,
    );
}

sub fetch_unprocessed_line {
    my ( $self ) = @_;
    return $self->{dbh}->selectrow_hashref(
        "select * from tr_credit_lines where next_action_date < now() order by next_action_date limit 1",
    );
}

sub line_messages {
    my ( $self, $line ) = @_;
    return $self->{dbh}->selectall_arrayref(
        "select * from tr_credit_messages where credit_line_id = ? order by created_at limit 1",
        {Slice =>{}},
        $line->{credit_line_id},
    );
}

sub save_line {
    my ( $self, $line ) = @_;
    $self->{dbh}->do(
        "replace into tr_credit_lines set ".join(q{,}, map { "$_ = ?" } keys %$line), undef,
        values %$line,
    );
    return;
}

1;
