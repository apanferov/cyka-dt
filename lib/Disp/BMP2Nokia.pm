#!/usr/bin/perl -w
package Disp::BMP2Nokia;

use strict;
use warnings;
use base 'Exporter';
use Disp::RecodeUtils qw (decodeBMP);
our @EXPORT_OK = qw( OTAOperatorlogo_PORT OTAPictureMessage_PORT OTAPictureMessage OTAOperatorlogo encodeOTABitmap);
our $VERSION = '0.1';

use constant BITMAP_WIDTH => 72;
use constant BITMAP_HEIGHT => 14;
use constant BITMAP_HEIGHT_DOUBLE => 28;
use constant OTAOperatorlogo_PORT => 5506;
use constant OTAPictureMessage_PORT => 5514;

sub OTAPictureMessage {
	my ($text, $data) = @_;

	my ($bytearray, $width, $height) = decodeBMP($data);
	return -1 unless ($bytearray);
	# check image size(s)
	# 1. width
	return -1 unless ( $width == BITMAP_WIDTH );
	# 2. height
	return -1 unless ( $height == BITMAP_HEIGHT || $height == BITMAP_HEIGHT_DOUBLE );
	
	my $stream;
	# text
	$stream .= sprintf( "%02X", ord('0') );
#	$stream .= sprintf( "%02X", 0 );
#	$stream .= sprintf( "%04X", length($text) );
#	map { $stream .= sprintf( "%02X", ord($_)) } split(//,$text);

	# image
	$stream .= sprintf( "%02X", 2 );
	my $bitmap_stream = encodeOTABitmap( $width, $height, 1, $bytearray );
	$stream .= sprintf( "%04X", length($bitmap_stream) / 2 );
#	$stream .= sprintf( "%02X", 0 );
	$stream .= $bitmap_stream;

	return pack('H*',$stream);
}


sub OTAOperatorlogo {
	my ($countrycode, $operatorcode, $data) = @_;

	my ($bytearray, $width, $height) = decodeBMP( $data );
	return -1 if ($bytearray == -1) or ($width != BITMAP_WIDTH) or ($height != BITMAP_HEIGHT);

	my $stream;

#	$stream.= encodeOperatorID($countrycode, $operatorcode);
#	$stream.='00';
#	$stream.=encodeOTABitmap($width, $height, 1, $bytearray);
	$stream.='30';
	$stream.= encodeOperatorID($countrycode, $operatorcode);
	$stream.='0A';
	$stream.=encodeOTABitmap($width, $height, 1, $bytearray);

	return $stream;
}

# encode the operator ID (country, operator) into a litlle endian BCD string
sub encodeOperatorID {
	my ($country, $operator) = @_;

	my $c = sprintf("%03d", $country);
	my $o = sprintf("%02d", $operator);

	my @arr = split /|/, sprintf("%03d%02d", $country, $operator);
	my @enc;
 
	$enc[0] = ($arr[1] & 0x0F) << 4 | ($arr[0] & 0x0F);
	$enc[1] = (0x0F << 4)           | ($arr[2] & 0x0F);
	$enc[2] = ($arr[4] & 0x0F) << 4 | ($arr[3] & 0x0F);
 
	return join("", map { sprintf("%02X", $_) } @enc );
} 

# encode binary array into OTABitmap format
sub encodeOTABitmap {
	my ($width, $height, $depth, $ref_bytearray) = @_;
	my $stream='00';
	$stream.= sprintf("%02X", $width);
	$stream.= sprintf("%02X", $height);
	$stream.= sprintf("%02X", $depth);
	$stream.= uc unpack ('H*' , $ref_bytearray);
	return $stream;
}



1;

