#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Disp::HTTP_SMSClient;
use strict;
use warnings;
use base 'Exporter';
our $VERSION    = '1.001';
use DBI;
use Data::Dumper;
use CGI qw/:standard/;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Disp::AltDB;
use XML::Simple;

use SOAP::Transport::HTTP;
use SOAP::Transport::IO;
use IO::Scalar;


our $SOAP_GLOBAL;


use Encode qw(encode decode);

use constant process_tab => {
    http_umcs_old => { pref => 'RCV_',
                       receive => sub {

                           my ( $self ) = @_;
                           my $data = $self->{query}->param("POSTDATA");
                           my $out = "";
                           my $ofh = new IO::Scalar \$out;
                           my $fh = new IO::Scalar \$data;

                           local $SOAP_GLOBAL = { db => $self->{db},
                                                  log => $self->{log},
                                                  host => $self->{host},
                                                  smsc_id => $self->{smsc_id},
                                                };

                           $self->{log}->debug("IN: $data");

                           SOAP::Transport::IO::Server
                              -> new( in => $fh, out => $ofh )
                              -> dispatch_with({ 'http://www.estylesoft.com/umcs/shop/' => 'Disp::UmcsSOAP_OLD' })
                              -> handle;

                           $self->{log}->debug("OUT: $out");

                           print $self->{query}->header(-type=>'text/xml', -charset=>'utf-8');
                           print $out;
                       },
                       create_resp => sub {
                       },
                       create_fatal_resp => sub {
                           print header(-type=>'text/xml', -charset=>'utf-8');
                           print <<EOF;
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Body>
       <SOAP-ENV:Fault>
           <faultcode>SOAP-ENV:Server</faultcode>
           <faultstring>Uncaught exception while handling RPC request</faultstring>
           <detail>
               <e:myfaultdetails xmlns:e="http://alt1.ru/soap/fault">
                 <message>Nothing here, all details in log-file</message>
               </e:myfaultdetails>
           </detail>
       </SOAP-ENV:Fault>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF
                       },
                     },
    http_ivanovo => { pref => 'TRN_', # IVANOVO
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{dest},
                                                abonent => $params->{abonent},
                                                msg => $params->{sms},
                                                data_coding => 0,
                                                esm_class => 0,
                                        transaction_id => $params->{id},
                                        };
                                        return $sms unless ($sms->{abonent} =~ /\d+/);
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
           create_resp => sub {
               my ($self,$sms) = @_;
               my $smss;
               if ($sms->{abonent} =~ /^\d+$/) {
                   my $smc = 0; my $fc=0;
                   while ((!$smc) and ($fc < 24)) {
                       sleep 5; $fc++;
                       $self->{log}->debug('Fetching...');
                       $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$sms->{transaction_id});
                       $smc = scalar keys(%$smss);
                   }
               } else {
                   $self->{log}->debug('Fetching...');
                   $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},undef,$sms->{num});
               }
               my $sms_count = scalar keys(%$smss);
               my $resp = '';
               if ($sms_count) {
                   $self->{log}->debug("$sms_count message(s) fetched.");
                   foreach my $sms (sort {$a->{id} <=> $b->{id}} values(%$smss)) {

                       if ($sms->{esm_class} & 0x40) {
                           $resp .= $self->xml_hex_message($sms);
                       } else {
                           $resp .= $self->xml_concat_message($sms);
                       }

                       # XXX �� ������� �� ����, ��� ������ �� ���������� �������?
                       $self->{db}->set_sms_new_status($sms->{id},2,0,0,0);
                       $self->{db}->set_sms_delivered_status($sms->{id});
                       $self->{log}->info("Message SMS$sms->{id} was delivered to abonent.");
                   }
               }
               $resp = '<?xml version="1.0" encoding="windows-1251" ?>'."\n<content>\n$resp</content>";
               $self->{log}->debug("Response:\n $resp");
               print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
               print $resp;
           },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print '<?xml version="1.0" encoding="windows-1251" ?>';
                                        print "\n<content>\n</content>";
                                },
        },
    http_teleclick => {         pref => 'RCV_', # TELECLICK
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{dstaddr},
                                                abonent => $params->{srcaddr},
                                                msg => $params->{text},
                                                data_coding => 0,
                                                esm_class => 0,
                                        transaction_id => $params->{trans_id},
                                        link_id => $params->{linkid},
                                        };
                                        #($sms->{num},$sms->{transaction_id}) = split(/#/,$sms->{num});
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_ukraine2 => {  pref => 'RCV_', # UKRAINE OLD
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};
                                        my $msg= ( $params->{keyword}." ".$params->{text});
                                        $msg = encode('cp1251',decode('utf8',$msg));
                                        $msg =~ s/\+quot;/"'"/ieg;

                                        return undef if ($params->{action} eq 'delivery-report');
                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{'short-number'},
                                                abonent => $params->{phone},
                                                msg =>$msg,
                                                data_coding => 0,
                                                esm_class => 0,
                                                link_id => $params->{provider},
                                        transaction_id => $params->{'transaction-id'},
                                        };

                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html');
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_ukraine => {   pref => 'RCV_', # UKRAINE NIKITA NEW
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};
                                        my $msg= ( $params->{keyword}." ".$params->{text});
                                        $msg = encode('cp1251',decode('utf8',$msg));
                                        $msg =~ s/\+quot;/"'"/ieg;

                                        if (lc($params->{action}) eq 'deliver') {
                                        my $sms = {
                                                        smsc_id => $self->{smsc_id},
                                                        num => $params->{'short-number'},
                                                        abonent => $params->{phone},
                                                        msg =>$msg,
                                                        data_coding => 0,
                                                        esm_class => 0,
                                                        link_id => $params->{provider},
                                                transaction_id => $params->{'transaction-id'},
                                                };
                                                if ($params->{provider} =~ /^kstar/) {
                                                        $self->{log}->debug(Dumper($sms));
                                                        my $id = $self->{db}->insert_sms_into_insms_ignored($sms);
                                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                                        my $msg = {
                                                                abonent => $sms->{abonent},
                                                                smsc_id => $self->{smsc_id},
                                                                operator_id => 0,
                                                                num => $sms->{num},
                                                                data_coding => 0,
                                                                esm_class => 0,
                                                                transaction_id => $sms->{transaction_id},
                                                                test_flag => 20,
                                                                parts => ['�������. ����� �� 2-�� SMS']};
                                                        $self->{db}->move_outbox_to_outsms($msg);
                                                        $self->{log}->info("Testing SMS created.");
                                                        return $sms;
                                                } else {
                                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                                        $self->{log}->debug(Dumper($sms));
                                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                                        return $sms;
                                                }
                                        }
                                        elsif (lc($params->{action}) eq 'delivery-report') {
                                                my $transaction_id = $params->{'transaction-id'};
                                                $self->{log}->info("Report for message with tr-id=$transaction_id was received.");
                                                my $ignored_id = $self->{db}->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? limit 1',undef,$self->{smsc_id},$transaction_id);
                                                if ($ignored_id) {
                                                        my $id = $self->{db}->move_ignored_to_insms($ignored_id);
                                                        $self->{log}->info("Message SMS$id was passed to system.");
                                                }
                                                return undef;
                                        }
                                        return undef;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html');
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },

    http_pribaltika => {        pref => 'RCV_', # PRIBALTICA
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => ($params->{price}) ? "$params->{short}:$params->{price}" : $params->{short},
                                                abonent => $params->{phone},
                                                msg => encode('cp1251',decode('UTF-8',$params->{text})),
                                                data_coding => 0,
                                                esm_class => 0,
                                                link_id => "$params->{country}#$params->{operator}",
                                        };
                                        # �������
                                        if ($sms->{link_id} eq 'lv#lmt')        {
                                                $sms->{transaction_id} = substr($sms->{abonent},8);
                                                $sms->{abonent} = '371'.substr($sms->{abonent},0,8);
                                        }
                                        elsif ($sms->{link_id} eq 'ee#emt-ee')  {
                                                $sms->{abonent} = '372'.$sms->{abonent};
                                        }
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },
    http_kievstar => {  pref => 'RCV_', # KIEVSTAR
                        receive => sub {
                                        my($self) = @_;
                                        my $xml = $self->{query}->param('POSTDATA');
                                        $self->{log}->debug($xml);
                                        my $ref = XMLin($xml, ForceContent => 1);
                                        $self->{log}->debug(Dumper($ref));

                                        if ($ref->{service}->{content} eq 'content-request') {
                                        my $sms = {
                                                        smsc_id => $self->{smsc_id},
                                                        num => $ref->{sn}->{content},
                                                        abonent => $ref->{'sin'}->{content},
                                                        msg => encode('cp1251',decode('UTF-8',$ref->{body}->{content})),
                                                        data_coding => 0,
                                                        esm_class => 0,
                                                        transaction_id => $ref->{rid},
                                                };
                                                $self->{log}->debug(Dumper($sms));
                                                my $id = $self->{db}->insert_sms_into_insms_ignored($sms);
                                                $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                                my $msg = {
                                                        abonent => $sms->{abonent},
                                                        smsc_id => $self->{smsc_id},
                                                        operator_id => 0,
                                                        num => $sms->{num},
                                                        data_coding => 0,
                                                        esm_class => 0,
                                                        transaction_id => $sms->{transaction_id},
                                                        test_flag => 20,
                                                        parts => ['�������. ����� �� 2-�� SMS']};
                                                $self->{db}->move_outbox_to_outsms($msg);
                                                $self->{log}->info("Testing SMS created.");

                                                return $sms;
                                        }
                                        elsif ($ref->{service}->{content} eq 'delivery-report') {
                                                my $outsms_id = $ref->{mid};
                                                my $transaction_id = $ref->{rid};
                                                my $status = $ref->{status}->{content};
                                                my $error = $ref->{status}->{error};
                                                if ($status eq 'Delivered') {
                                                        $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
                                                        $self->{db}->set_sms_report_status($outsms_id,12);
                                                        my $ignored_id = $self->{db}->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? limit 1',undef,$self->{smsc_id},$transaction_id);
                                                        if ($ignored_id) {
                                                                my $id = $self->{db}->move_ignored_to_insms($ignored_id);
                                                                $self->{log}->info("Message SMS$id was passed to system.");
                                                        }

                                                }
                                                elsif ($status eq 'Delivering') {
                                                        $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
                                                        $self->{db}->set_sms_report_status($outsms_id,11);
                                                }

                                                elsif ($status eq 'Undeliverable') {
                                                                $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status. Reason: $error");
                                                                $self->{db}->set_sms_report_status($outsms_id,13);
                                                }
#                                               elsif ($status eq 'Undelivered') {
#                                                       $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status.");
#                                                       $self->{db}->set_sms_report_status($outsms_id,13);
#                                               }
                                                else {
                                                        $self->{log}->info("Report for message SMS$outsms_id was received. Status: $status. Reason: $error");
                                                        $self->{db}->set_sms_report_status($outsms_id,13);
                                                        yell("Unknown delivery report status for SMS$outsms_id\n$xml",code => 'WARNING',process => $self->{pr_name},
                                                                header => "$self->{pr_name}: Unknown delivery report status",ignore => { log4perl => 1});
                                                }
                                                return undef;
                                        }
                                        return undef;


                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/xml');
                                        print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<report>\n<status>Accepted</status>\n</report>\n";
                                        $self->{log}->debug("Response: Accepted");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                        $self->{log}->FATAL("$err");
                                },
        },


    http_aaa => {       pref => 'RCV_', # AAA
                        receive => sub {
                                        my($self) = @_;
                                        my $params = $self->{params};

                                my $sms = {
                                                smsc_id => $self->{smsc_id},
                                                num => $params->{num},
                                                abonent => $params->{abonent},
                                                msg => encode('cp1251',decode('UTF-8',$params->{text})),
                                                data_coding => 0,
                                                esm_class => 0,
                                                transaction_id => $params->{transaction_id},
                                        };
                                        my $id = $self->{db}->insert_sms_into_insms($sms);
                                        $self->{log}->debug(Dumper($sms));
                                        $self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
                                        return $sms;
                        },
                        create_resp => sub {
                                my ($self,$sms) = @_;
                                        print $self->{query}->header(-type => 'text/html', -charset => 'cp1251');
                                        print 'OK';
                                        $self->{log}->debug("Response: OK");
                        },
                        create_fatal_resp => sub {
                                my ($self,$err) = @_;
                                },
        },


};

sub new {
   my ($class,$query) = @_;
   my $self = {query => $query, pr_name => 'HTTP_SMS'};
   bless $self, $class;
   return $self;
}

sub run {
        my ($self) = @_;

        eval {
                #  Connecting to DB
                $self->{db} = new Disp::AltDB();
                my $remote_host = $ENV{REMOTE_ADDR};
                my $rows = $self->{db}->fetchall_arrayref("select id,debug_level,client_type from set_smsc where host like '%$remote_host%'");
                die "SMSC not found in DB. $remote_host" if (@$rows!=1);
                ($self->{smsc_id},$self->{debug_level},$self->{client_type}) = @{$rows->[0]};
                die "No supported SMSC type. $self->{client_type}" unless (exists(process_tab->{$self->{client_type}}));

                # Logging preparations
                $self->{pr_name} = uc(process_tab->{$self->{client_type}}{pref}.$self->{smsc_id});
                my %log_conf = (
                "log4perl.rootLogger"       => "$self->{debug_level}, Logfile",
                "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
                "log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/'.lc($self->{pr_name}).'.log',
	  			"log4perl.appender.Logfile.recreate"					 => 1,
	  			"log4perl.appender.Logfile.recreate_check_interval"		 => 300,
	  			"log4perl.appender.Logfile.recreate_check_signal"		 => 'USR1',
                "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
                "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','sms_client')
                        );
                Log::Log4perl->init( \%log_conf);
                my $log=get_logger("");
                $self->{log} = $log;

                $log->info("Starting...");
                $log->debug("SMSC: $self->{smsc_id}");
                $self->{params} = $self->{query}->Vars;
                $log->debug(Dumper(\%ENV));
                $log->debug("Parameters: ".Dumper($self->{params}));
                $log->debug("CT: $self->{client_type}");

                my $sms = &{process_tab->{$self->{client_type}}{receive}}($self);
                &{process_tab->{$self->{client_type}}{create_resp}}($self,$sms);
                $log->info("Stopped.\n\n\n");
        };

        if ($@) {
                yell("$@\n".Dumper(\%ENV),code => 'FATAL_ERROR',process => $self->{pr_name},
                        header => "$self->{pr_name}: $@",ignore => { log4perl => 1});
                &{process_tab->{$self->{client_type}}{create_fatal_resp}}($self) if ($self->{client_type} and exists(process_tab->{$self->{client_type}}));
        }

}

sub escape_value {
        my($self, $data) = @_;

        return '' unless(defined($data));

        $data =~ s/&/&amp;/sg;
        $data =~ s/</&lt;/sg;
        $data =~ s/>/&gt;/sg;
        $data =~ s/"/&quot;/sg;

        return $data;
}

sub xml_concat_message {
    my ( $self, $sms ) = @_;

    my $resp = "";

    # XXX - �� ���� ��� ������������ XML
    $resp .= "<sms>\n";
    $resp .= "<abonent>$sms->{abonent}</abonent>\n";
    $resp .= "<message type=\"concat\">".$self->escape_value($sms->{msg})."</message>\n";
    $resp .= "<dcs>$sms->{data_coding}</dcs>\n";
    $resp .= "</sms>\n";
    return $resp;
}

sub xml_hex_message {
    my ( $self, $sms ) = @_;

    my $resp = "";

    # XXX - �� ���� ��� ������������ XML
    $resp .= "<sms>\n";
    $resp .= "<abonent>$sms->{abonent}</abonent>\n";
    $resp .= "<message type=\"hex\">".unpack("H*", $sms->{msg})."</message>\n";
    $resp .= "<dcs>$sms->{data_coding}</dcs>\n";
    $resp .= "<esm>$sms->{esm_class}</esm>\n";
    $resp .= "</sms>\n";
    return $resp;
}

1;
