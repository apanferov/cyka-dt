#!/usr/bin/perl -w
package Disp::HTTP_SMSSender;
use strict;
use warnings;
use base 'Exporter';
use DBI;
use Data::Dumper;
use IO::File;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
use Config::Tiny;
use LWP::UserAgent;
use HTTP::Response;
use URI::URL;
use Encode qw(encode decode);
use XML::Simple;
use SOAP::Transport::HTTP;
use SOAP::Transport::IO;

use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Disp::UmcsSOAP;

use HTTP::Request::Common qw(POST);
use HTTP::Request::Common;

our $VERSION	= '1.001';

use constant process_tab => {
    http_umcs => { routines => sub {
					   my ( $self ) = @_;

					   my $stalled = Config->dbh->selectall_arrayref("select * from tr_umcs_requests where our_status = 6 and reserve_date between date_sub(now(), interval 91 minute) and date_sub(now(), interval 79 minute)", {Slice => {}});

					   $self->{log}->debug("UMCS routines, searching for stalled transactions");

					   for my $order (@$stalled) {
						   Disp::UmcsSOAP::cert_conf($order->{num});

						   my $result = Disp::UmcsSOAP::Helper::PurchaseEnd
							 ($self->{url},
							  'http://www.estylesoft.com/umcs/shopinterface/',
							  $order->{order_id},
							  decode('utf-8', "Сервис не ответил в установленное время"),
							  1
							 );

						   my $our_status;
						   my $reason;
						   if ($result == 0) {
							   $our_status = 14;
							   $reason = "successfull";
						   } else {
							   $our_status = 15;
							   $reason = "unsuccessfull";
						   }

						   Disp::UmcsSOAP::Helper::db_update($order->{order_id}, { our_status => $our_status });

						   my $message = "Stalled transaction detected. \nAttempt to rollback this transaction on UMCS side was $reason.  Order info is\n@{[Dumper($order)]}";
						   $self->{log}->error($message);
						   yell($message,
								code => 'ERROR',
								process => $self->{pr_name},
								header => "$self->{pr_name}($self->{name}): Stalled transaction detected",
								ignore => { log4perl => 1 },
								cc => 's.shchukin@alt1.ru, i.fedorov@alt1.ru, v.stolovitskiy@alt1.ru' );

					   }
					  # openssl pkcs12 -nodes -in umcs_a1.pfx -clcerts -out u.crt
					#	 openssl x509 -in u.crt -noout -enddate

				   },
				   send => sub {
                       my ( $self, $sms ) = @_;
                       my ( $db, $log ) = ($self->{db}, $self->{log});

					   Disp::UmcsSOAP::cert_conf($sms->{num}, $log);
					   my $order;
					   my $result;

					   if (!$sms->{transaction_id}) {
					  	  my $CTN = $sms->{abonent};
						  $CTN =~ s/.*(\d{10})$/$1/;
			sleep(1);
			eval {
                      	  $result = Disp::UmcsSOAP::Helper::ProcessPurchaseShortPhoneAsync
                      		   ($self->{url},
                      		    'http://www.estylesoft.com/umcs/shopinterface/',
							    $sms->{num},
                        		"##$sms->{id}",
                        		$CTN
                        		);
			};
			if ($@) {$log->error("IN_SOAP: $@"); die;}
			
						  $log->info("Purchase initiated by alt1. CTN=$CTN, num=$sms->{num}, smsid=$sms->{id}");
					   }
					   elsif ($order = Disp::UmcsSOAP::Helper::db_load($sms->{transaction_id}) and $order->{our_status} != 6) {
                           $db->set_sms_new_status($sms->{id},255,0,0,0);
                           $db->set_sms_delivered_status($sms->{id});
						   $self->fatal_error("Message in wrong status\nSMS: @{Dumper($sms)}\nORDER: @{[Dumper($order)]}");
						   return;
					   }
					   else {

					  		my $umcs_status = $sms->{num} =~ /free/ ? 1 : 0;
					  		my $our_status;

                      		$result = Disp::UmcsSOAP::Helper::PurchaseEnd
                      		   ($self->{url},
                      		    'http://www.estylesoft.com/umcs/shopinterface/',
                      		    $sms->{transaction_id},
                        		decode('cp1251',$sms->{msg}),
							    $umcs_status
                        		);

                      		if ($result == 0) {
							   $our_status = $umcs_status == 0 ? 8 : 9;
                      		} else {
							   $our_status = $umcs_status == 0 ? 7 : 10;
                      		}
					  		Disp::UmcsSOAP::Helper::db_update($sms->{transaction_id}, { our_status => $our_status });
					   }
					   
                       if ($result == 0) {
                           $db->set_sms_new_status($sms->{id},2,0,0,0);
                           $db->set_sms_delivered_status($sms->{id},1);
                           $log->info("Message SMS$sms->{id} was delivered to abonent.");
                       } else {
                           $log->warn("Message SMS$sms->{id} wasn't delivered.");
                           $db->set_sms_new_status($sms->{id},255,0,0,0);
                           $db->set_sms_delivered_status($sms->{id},1);
                       }

                       return;
                   }
                 },
    http_teleclick => { send => sub { # TELECLICK
    		my ($self,$sms) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
    		my $url = url($self->{url});
			$sms->{num} .= "#$sms->{transaction_id}" if ($sms->{transaction_id});

#			my $link_id = $self->{db}->get_value('select link_id from set_operator_t where id=?',undef,$sms->{'operator_id'});
			my $link_id = $self->{db}->get_link_id($sms->{abonent},$self->{smsc_id});
			my %req = (	partner => $self->{system_id},
    						password => $self->{password},
    						srcaddr => $sms->{num},
							dstaddr => $sms->{abonent},
							linkid => $link_id,
							text => $sms->{msg});
    		$url->query_form(%req);
    		$self->{log}->debug($url);
    		my $resp = $self->{ua}->get($url);
    		my $content = $resp->content();
    		if ($content eq 'OK') {
           	$db->set_sms_new_status($sms->{id},2,0,0,0,1);
           	$db->set_sms_delivered_status($sms->{id});
           	$log->info("Message SMS$sms->{id} was delivered to abonent.");
      	}
      	else  
      	{
         	$log->warn("Message SMS$sms->{id} wasn't delivered. ".$content);
         	$db->reconnect();
         	$db->set_sms_new_status($sms->{id},0,60,255,0,1);
         	$log->warn("Will try to resend in 1 minute.");
      	}
    		
			return undef;
		}, },


		http_ukraine => { send => sub { # UKRAIN
    		my ($self,$sms) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
    		my $status_code=$self->{status_code};
    		my $url = url($self->{url});
    		
			my $link_id = $self->{db}->get_link_id($sms->{abonent},$self->{smsc_id});
#			if ($link_id=='kstar') {
#    			$sms->{msg} = encode('UTF-8',decode('cp1251',$sms->{msg}));
#           	$log->debug('UTF-8 used');
#    		}
    		
			my $req = POST $url,
         		[login=> $self->{system_id},
					password=> $self->{password},
					'transaction-id'=>$sms->{transaction_id},
					text => $sms->{'msg'} ];
    		$self->{log}->debug($req->as_string); 	
    		my $resp=$self->{ua}->request($req);
  			my $reply = $resp->content();
    		$_=$reply;
    		my @result = /\d+/g;
    		$reply=int(join('',@result));
			$reply=(($reply>=0&&$reply<4)?$reply:4);    	  		
    		if ($reply==0) {
    		     	$db->set_sms_new_status($sms->{id},2,0,0,0,1);
           	$db->set_sms_delivered_status($sms->{id});
           	$log->info("Message SMS$sms->{id} was delivered to SMSC.");
      	}
      	else 	{
      		$db->set_sms_new_status($sms->{id},255,0,$reply,0,1);
           	$db->set_sms_delivered_status($sms->{id},1);
           	$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.$status_code->{$reply}.");
      	}  		
			return undef;
		}, },

		http_pribaltika => { send => sub { # PRIBALTICA
    		my ($self,$sms) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
    		my $url = url($self->{url});
#			$sms->{num} .= "#$sms->{transaction_id}" if ($sms->{transaction_id});
			
#			my $link_id = $self->{db}->get_value('select link_id from set_operator_t where id=?',undef,$sms->{'operator_id'});
			my $link_id = $self->{db}->get_link_id($sms->{abonent},$self->{smsc_id});

			my $msg;			
			if ($sms->{data_coding}==0x00) {
				$msg = '1'.encode('UTF-8',decode('cp1251',$sms->{msg}));
			}
			elsif ($sms->{data_coding}==0x08) {
				$msg = '3'.encode('UTF-8',decode('cp1251',$sms->{msg}));
			}
			elsif ($sms->{data_coding}==0xF5) {
				$msg = '2'.$sms->{msg};
			}
			else {
				die "wrong sms $sms->{id}";
			}

			#$log->debug($link_id);
			if ($link_id eq 'lv#lmt')	{
				$sms->{abonent} = substr($sms->{abonent},3).$sms->{transaction_id};
			}
			elsif ($link_id eq 'ee#emt-ee')	{
				$sms->{abonent} = substr($sms->{abonent},3);
			}
			my ($num) = split(/\s*:\s*/,$sms->{num});
			my ($country,$operator) = split(/\s*#\s*/,$link_id);
			my %req = (	short => $num,
							phone => $sms->{abonent},
							operator => $operator,
							country => $country,
							text => $msg);
    		$url->query_form(%req);
    		$self->{log}->debug($url);
    		my $resp = $self->{ua}->get($url);
    		my $content = $resp->content();
         $log->debug($content);

    		if ($resp->code() eq '200') {
				my $sms_count=int($content);
				if ($sms_count > 0) {
					$db->set_sms_new_status($sms->{id},2,0,0,0,1);
					$db->set_sms_delivered_status($sms->{id},0,$sms_count);
					$log->info("Message SMS$sms->{id} was delivered to abonent.");
				}
				else {
					$log->warn("Message SMS$sms->{id} wasn't delivered. \n".$content);
					$db->set_sms_new_status($sms->{id},0,60,255,0,1);
					$log->warn("Will try to resend in 1 minute.");
				}
			}
			else {
				$log->warn("Message SMS$sms->{id} wasn't delivered. \n".$content);
				$db->set_sms_new_status($sms->{id},0,600,255,0,1);
				$log->warn("Will try to resend in 10 minutes.");
			}
			return undef;
		}, },
		http_kievstar => { send => sub { # KIEVSTAR
    		my ($self,$sms) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
    		
    		my $paid_attr = ($sms->{test_flag}==20) ? 'true' : 'false';
			my $struct = {
								mid => $sms->{id},
								rid => $sms->{transaction_id},
								paid => $paid_attr,
								bearer => 'SMS',
								body => {
									'content-type' => 'text/plain',
									content => decode('cp1251',$sms->{msg})
								}
							};
			$struct->{sin}{content} = $sms->{num} unless ($sms->{transaction_id});
    		my $xml = encode('UTF-8',XMLout($struct, RootName => 'message', XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>'));
    		my $header = HTTP::Headers->new;
  			$header->header('content-type' => 'text/xml');
  			$header->authorization_basic($self->{system_id},$self->{password});
  			my $req = HTTP::Request->new('POST', $self->{url}, $header, $xml);
  			$log->debug($req->as_string());

    		my $resp=$self->{ua}->request($req);
			my $reply = $resp->content();
			$log->debug($reply); 
    		if (($resp->code() eq '200') and ($reply =~ m/^s*</)) {
				my $parsed_resp = XMLin($reply, ForceContent => 1);
				if ($parsed_resp->{status}->{content} eq 'Accepted') {
					$db->set_sms_new_status($sms->{id},2,0,0,0,1);
   		        	$db->set_sms_delivered_status($sms->{id});
         		  	$log->info("Message SMS$sms->{id} was delivered to SMSC.");
         		  	return;
         		}
         		elsif ($parsed_resp->{status}->{content} eq 'Rejected') {
					$db->set_sms_new_status($sms->{id},255,0,255,0,1);
					$db->set_sms_delivered_status($sms->{id},1);
					if ($sms->{inbox_id}) {
						$self->warning("Message SMS$sms->{id} wasn't accepted by SMSC.",
									   $resp->as_string().
									   "https://asg5.alt1.ru/?module=findmsg&action=inboxinfo&inbox_id=".$sms->{inbox_id})
					}
					$log->warn($resp->as_string());
					return;
         		}
			}
      	else {
         	$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n".$resp->as_string());
         	$db->set_sms_new_status($sms->{id},0,600,255,0,1);
         	$log->warn("Will try to resend in 10 minutes.");
         	return;
         	
      	}
			$db->set_sms_new_status($sms->{id},255,0,255,0,1);
			$db->set_sms_delivered_status($sms->{id},1);
			$self->warning("Message SMS$sms->{id} wasn't accepted by SMSC.",$resp->as_string());
			$log->warn($resp->as_string());
			return;
		}, },

		http_aaa => { send => sub { # AAA
    		my ($self,$sms) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
    		my $url = url($self->{url});
			my $msg = $sms->{msg}; # encode('UTF-8',decode('cp1251',$sms->{msg}));
			my %req = (	num => $sms->{num},
						abonent => $sms->{abonent},
						text => $msg,
						transaction_id => $sms->{transaction_id});
    		$url->query_form(%req);
    		$self->{log}->debug($url);
    		my $resp = $self->{ua}->get($url);
    		my $content = $resp->content();
         $log->debug($content);
        	$db->set_sms_new_status($sms->{id},2,0,0,0,1);
        	$db->set_sms_delivered_status($sms->{id},0,1);
        	$log->info("Message SMS$sms->{id} was delivered to abonent.");
    		
			return undef;
		}, },
};

sub new {
   my ($class,$smsc_id,$debug_level,$smpp_mode) = @_;
   
	Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
	my $smpp_mode_name = 'snd';
	
	#  Logging preparations
	
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);
	my %log_conf = (
	   "log4perl.rootLogger"       => "$debug_level, Logfile",
	   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
		"log4perl.appender.Logfile.recreate" => 1,
		"log4perl.appender.Logfile.recreate_check_interval" => 300,
		"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
		"log4perl.appender.Logfile.umask" => "0000",
	   "log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
	   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
	   	"log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','sms_client')
    	);
	Log::Log4perl->init( \%log_conf);
	my $log=get_logger("");
	$log->info("Starting...");

	#  Connecting to DB
	my $db = new Disp::AltDB();
	Config->set_dbh($db->{db});
	my $cnf = $db->get_smsc_configuration($smsc_id);
   my $self = {
		db => $db,log => $log, pr_name => $pr_name,
		smpp_mode_name => $smpp_mode_name,state=>'stopped',
		last_elink_time=>0xFFFF0000,
		wait_resp_count=>0,sleep=>0,status=>'start',
   	smsc_id=>$smsc_id,debug_level=>$debug_level,smpp_mode=>$smpp_mode
   };
   @{$self}{keys(%$cnf)} = values(%$cnf);
	
   bless $self, $class;
   return $self;
}

sub run {
	my $self = shift;
	my $retval = $self->run_sender();
	$self->enter_state('stopped');
	$self->{log}->info("Stopped.\n\n\n");
	return $retval;
}

sub run_sender {
   my $self = shift;
   
	$self->{ua} = LWP::UserAgent->new(timeout => 30);
  	$self->{ua}->agent('A1');
   eval {
	   $self->enter_state('run');
		$self->{log}->info("Started.");
   
#		$self->{db}->set_outsms_all_delivered($self->{smsc_id});
		do {
		 	if ( $self->can_send_msgs() ) {
		 		sleep(1) unless ($self->send_messages());
		 	}
		 	else {
		 		$self->{db}->ping();
		 	}
			
			$self->heartbeat;
			$self->routines;
		} until ($self->{terminated});
		$self->{log}->info('Stopping...');
	};
	if ($@) {
		$self->fatal_error($@);
		return 1;
	}
	return 0;
}   

sub can_send_msgs {
	my $self = shift;
	return 1;  
}

sub send_messages {
	my $self = shift;
	my ($db,$log) = ($self->{db},$self->{log});
	my $msgs = $db->get_sms_ready_to_send($self->{smsc_id});
	my $msg_count = scalar keys(%$msgs);
	return 0 if (!$msg_count);
	$log->debug("$msg_count message(s) fetched.");
	my $sended_count = 0;
	for (sort {$a <=> $b} keys(%$msgs)) {
		my $sms = $msgs->{$_};
		if ($sms->{test_flag}==2) {
        	$db->set_sms_new_status($sms->{id},2,0,0,0);
        	$db->set_sms_delivered_status($sms->{id},1);
			$log->info("Message SMS$sms->{id} was omitted. From:$sms->{num} To:$sms->{abonent} Text:$sms->{msg}");
		}
		else {
			&{process_tab->{$self->{client_type}}{send}}($self,$sms);
		}
		$sended_count++;
#		exit;
		last if ($self->{terminated});
	}
	return $sended_count;
}

sub enter_state {
   my ($self,$state) = @_;
   if (! ($self->{state} eq $state)) {
		$self->{db}->set_process_state(uc($self->{smpp_mode_name}).'_'.$self->{smsc_id},uc($state),uc($self->{state}));
   	$self->{state} = $state;
   	$self->{log}->debug("Enter ".uc($state)." state.");
   }
   return 1;
}

sub heartbeat {
   my ($self) = @_;
	$self->{heartbeat} = 0 if (!$self->{heartbeat});
 	if ((time() - $self->{heartbeat}) >= 60) {
		$self->{db}->heartbeat($self->{pr_name});
   	$self->{log}->debug("HEARTBEAT.");
		$self->{heartbeat} = time();
 	}
   return 1;
}

sub routines {
	my ($self) = @_;
	$self->{routines_time} = 0 if (!$self->{routines_time});
 	if ((time() - $self->{routines_time}) >= 120) {
		# $self->{db}->set_outsms_all_not_delivered($self->{smsc_id}) if (!$self->{ussd_type});
		$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
		&{process_tab->{$self->{client_type}}{routines}}($self)
		  if process_tab->{$self->{client_type}}{routines};
		$self->{log}->debug("ROUTINES.");
		$self->{routines_time} = time();
 	}
	return 1;
}

sub warning {
   my ($self,$err,$err2) = @_;
   $err2 = ($err2) ? "$err\n$err2" : $err;
   my $proc_name = uc($self->{smpp_mode_name}).'_'.$self->{smsc_id};
	$self->{log}->warn($err);
	yell($err2,code => 'WARNING',process => $proc_name,
		header => "$proc_name: $err",ignore => { log4perl => 1 });
   return 1;
}

sub fatal_error {
   my ($self,$err2) = @_;
   my ($err) = ($err2 =~ /^(.*?)$/m);
   my ($err_type) = ($err =~ /^(\w+_ERROR\d*):/s);
   $err_type = $err_type || 'FATAL_ERROR';
	$self->{log}->fatal($err2);
	my $ignore_email = ($err_type eq 'CONNECT_ERROR');
	yell($err2,code => $err_type, process => $self->{pr_name},
		header => "$self->{pr_name}: $err",ignore => { log4perl => 1, 'e-mail' => $ignore_email });
   return 1;
}

sub error {
	my ( $self, $message, $header ) = @_;

	$self->{log}->error($message);

	my %params = (code => 'ERROR',
				  process => $self->{pr_name},
				  header => "$self->{pr_name}($self->{name}): $header",
				  ignore => { log4perl => 1 } );

	# XXX add manager to yell CC?????

	yell($message, %params );
}



1;
