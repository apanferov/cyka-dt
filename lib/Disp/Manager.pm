#!/usr/bin/perl -w
# $Id: $

=head1 NAME

 Disp::Manager

=cut
package Disp::Manager;
use strict;
use base 'Disp::Storage::SQLCacheLoader';

our $VERSION	= 0.1;		# Версия пакета.

sub _table { "set_cyka_manager" }
sub _primary_key { "id" }


1;
