#!/usr/bin/perl -w
# $Id: $

=head1 NAME

 Disp::Storage::SQLCacheLoader

=head1 DESCRIPTION

=cut
package Disp::Storage::SQLCacheLoader;
use strict;
use base 'Disp::Object';

our $VERSION	= 0.1;		# Версия пакета.

my %per_class_cache;

sub load {
    my ( $proto, $id, $force ) = @_;
    $proto = ref($proto) || $proto;

    $per_class_cache{$proto} ||= {};

    if ($force or not exists $per_class_cache{$proto}{$id}) {
	my $sql = sprintf('select * from %s where %s  = ? limit 1',
			  $proto->_table, $proto->_primary_key);

	$per_class_cache{$proto}{$id} = Config->dbh->selectrow_hashref($sql, undef, $id);
    }

    return $proto->new($per_class_cache{$proto}{$id});
}

1;
