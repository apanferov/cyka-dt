#!/usr/bin/perl -w
package Disp::ConcatList;
use strict;
use warnings;
use base 'Exporter';
use Data::Dumper;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
use Disp::AltDB;
use Disp::Utils qw(yell);
our $VERSION	= '1.000';

sub new {
   my ($class) = @_;

   my %list;
   my $self = {
		list => \%list,
		log => get_logger("")
   };
	
   bless $self, $class;
   return $self;
}

sub msg {
	my ($self,$msg_id) = @_;
	return $self->{list}->{$msg_id};
}

sub add {
	my ($self,$sms) = @_;
	my $list = $self->{list};
	return undef unless ($sms);
	return undef unless (exists($sms->{sar_total_segments}));
	my $msg_id = "$sms->{abonent}-$sms->{sar_msg_ref_num}";
	my $seqnum = $sms->{sar_segment_seqnum};

	if (!exists($list->{$msg_id}) or !exists($list->{$msg_id}{$seqnum})) {
		$list->{$msg_id}{time} = time();
		$list->{$msg_id}{count} = $sms->{sar_total_segments};
		$list->{$msg_id}{$seqnum} = $sms;
		if ( $list->{$msg_id}{msg_type} !~ /^usim/ ) { # Don't override usim types
			$list->{$msg_id}{msg_type} = $sms->{msg_type};
		}
		$list->{$msg_id}{last_num} = $seqnum;
		$self->{log}->info("LIST: Added $sms->{abonent} -> $sms->{msg_type} -> $sms->{sar_msg_ref_num} ->$seqnum/$sms->{sar_total_segments} -> id: $sms->{id}");
	}
	elsif ($list->{$msg_id}{$seqnum}{id} != $sms->{id}) {
		$list->{$msg_id}{$seqnum}{duplicate_id} = $sms->{id};
		$self->{log}->info("LIST: Added duplicate $sms->{abonent} -> $sms->{sar_msg_ref_num} ->$seqnum/$sms->{sar_total_segments} -> id: $sms->{id}");
	}
	return $msg_id;		
}

sub is_ready { # ������ �� ��� � ������� (��� �� ����� ������)
	my ($self,$msg_id) = @_;
	my $msg = $self->{list}->{$msg_id};
	my $msg1 = (exists($msg->{1})) ? $msg->{1}->{msg} : '';
   if ($msg->{msg_type} =~ /^(sms|ussd)/) {
		return 1 if (length($msg1) > 153);
		return 1 if (length($msg1) >  67 and ($msg1 =~ m/[\x80-\xa6\xa8-\xff]/));
	}
	my $ready = 1;
   for (my $i=1; $i<=$msg->{count}; $i++) {
   	$ready &= exists($msg->{$i});
   }
	return $ready;
}

sub is_very_bad {
	my ($self,$msg_id) = @_;
	my $msg = $self->{list}->{$msg_id};
	my $bad = 0;
   for (my $i=1; $i<=$msg->{count}-1; $i++) {
   	$bad |= (!exists($msg->{$i}) and exists($msg->{$i+1}));
   }
	return $bad;
}

sub concat {
	my ($self,$msg_id) = @_;
	my $msg = $self->{list}->{$msg_id};
	my $very_bad = $self->is_very_bad($msg_id);
	for (my $i=1; $i<=$msg->{count}; $i++) {
		if (exists($msg->{$i})) {
			$msg->{msg} .= $msg->{$i}{msg};
		}
		elsif ($very_bad) {
			$msg->{msg} .= '...';
		}
	}
   if ($msg->{msg_type} =~ /^(sms|ussd)/) {
		$msg->{msg} =~ s/^\s+//; # �������� ������� ��������
		$msg->{msg} =~ s/\n+/ /; # �������� ��������� �����
	}
	return 1;
}

sub del {
	my ($self,$msg_id) = @_;
	delete($self->{list}->{$msg_id});
	return 1;
}

sub get_ready {
	my ($self,$timeout) = @_;
	my $list = $self->{list};
	my @ready_list;
	foreach my $msg_id (keys(%$list)) {
	   push(@ready_list,$msg_id) 
	   		if ($self->is_ready($msg_id) or ($self->msg($msg_id)->{time}+$timeout < time()));
	}
	return @ready_list;
}



1;
