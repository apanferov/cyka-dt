#!/usr/bin/perl -w
package Disp::HTTP_MMSClient;
use strict;
use warnings;
use base 'Exporter';
our $VERSION	= '1.001';
use DBI;
use Data::Dumper;
use CGI qw/:standard/;
use MIME::Parser;
use XML::Parser;
use XML::XPath;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
use File::Path qw (mkpath);
use Encode qw(encode decode);
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Disp::AltDB;

sub new {
   my ($class) = @_;
   my $self = {};

	# Getting request
	my $query = new CGI; $self->{query} = $query;
	$self->{raw_input} = "Content-Type: ".$query->content_type()."\n\n".$query->param('POSTDATA');
	$main::raw_input = $self->{raw_input};

	#  Connecting to DB
	my $db = new Disp::AltDB(); $self->{db} = $db;
	my $remote_host = $query->remote_host;
	my $rows = $db->fetchall_arrayref("select id,debug_level from set_smsc where smpp_mode=3 and host='$remote_host'");
	die "MMSC not found in DB. $remote_host" if (@$rows!=1);
	my ($smsc_id,$debug_level) = @{$rows->[0]};
	$self->{smsc_id} = $smsc_id; 	
	$self->{debug_level} = $debug_level;

	# Logging preparations
	$main::proc_name = 'mrcv_'.$smsc_id;
	my %log_conf = (
	   "log4perl.rootLogger"       => "$debug_level, Logfile",
	   "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
	   "log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/mrcv_'.$smsc_id.'.log',
	   "log4perl.appender.Logfile.recreate"						 => 1,
	   "log4perl.appender.Logfile.recreate_check_interval"		 => 300,
	   "log4perl.appender.Logfile.recreate_check_signal"		 => 'USR1',
	   "log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
	   "log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','mms_client')
	  	);
	Log::Log4perl->init( \%log_conf);
	my $log=get_logger(""); $self->{log} = $log;
	$log->info("Starting...");

	$log->debug("MMSC: $smsc_id");

   bless $self, $class;
   return $self;
}

sub parse {
	my ($self) = @_;
	
	# Parsing
	my $parser = MIME::Parser->new; $self->{parser} = $parser;
	$parser->output_to_core('ALL');
	$parser->tmp_to_core(1);
	$parser->ignore_errors(1);
	
	my $entity = eval { $parser->parse_data($self->{raw_input}) }
		or die "Something wrong with MIME message: @{[$@ || $parser->last_error]}\n";
		
	my $soap = $self->find_soap_entity($entity) or die('SOAP not found.');
		
	my $xpath = XML::XPath->new(xml => $soap->bodyhandle->as_string);
	my ($soapheader) = $xpath->findnodes('/soap-env:Envelope/soap-env:Header');
	my ($soapbody) = $xpath->findnodes('/soap-env:Envelope/soap-env:Body');
	my $msg = {}; $self->{msg} = $msg;
	$msg->{smsc_id} = $self->{smsc_id};
	$msg->{transaction_id} = $xpath->findvalue('TransactionID',$soapheader)->value();
	$msg->{abonent} = $xpath->findvalue('DeliverReq/Sender',$soapbody)->value();
	$msg->{abonent} =~ s/\+?(\d*)\/TYPE=PLMN/$1/;
	
	$msg->{num} = $xpath->findvalue('DeliverReq/Recipients/To/RFC2822Address',$soapbody)->value();
	my @num_nodes = $xpath->findnodes('DeliverReq/Recipients/To',$soapbody);
	push @num_nodes , $xpath->findnodes('DeliverReq/Recipients/Cc',$soapbody);
	foreach (@num_nodes) {
		my $num = $xpath->findvalue('RFC2822Address',$_);
		$num =~ s/\+?(\d*)\/TYPE=PLMN/$1/;
		if ($num =~ /^\d\d\d\d$/) { #very thin place
			$msg->{num} = "mms:$num";
		}
	}
		
	my $content_data;
	if ($xpath->exists('DeliverReq/Content',$soapbody)) {
		my $cid = $xpath->findvalue('DeliverReq/Content/@href',$soapbody)->value();
		$cid =~ s/cid:(\d*)/$1/; $msg->{content_id} = $cid;
		my $content_entity = $self->find_content_entity($entity,$cid) or die('Content entity not found.');
		$self->parse_content($msg,$content_entity);
		$content_data = $content_entity->stringify();
	}
	else {
		$self->{log}->warn('<Content> tag not found - empty message.');
	}
	$self->{log}->debug(Dumper($msg));
				
	$msg->{id} = $self->{db}->insert_mms_into_inmmsa($msg,$self->{raw_input},$content_data);
	$msg->{text} = "mms:$msg->{id}";
	my $msg_id = $self->{db}->insert_mms_into_inbox($msg);
	$self->{log}->info("Message MMS$msg->{id}(MSG$msg_id)received. From:$msg->{abonent} To:$msg->{num}");
	$self->create_success_resp();
	$self->{log}->info("Stopped.\n\n\n");
}

sub find_soap_entity {
	my ($self,$entity) = @_;
	my $soap;
	if (lc($entity->head->mime_type) eq 'multipart/related') {
		foreach my $part ($entity->parts()) {
			if (lc($part->head->mime_type) eq 'text/xml') {
				my $xpath = XML::XPath->new(xml => $part->bodyhandle->as_string);
				if ($xpath->exists('/soap-env:Envelope')) {$soap = $part; last;}
			}
		}
	}
	elsif (lc($entity->head->mime_type) eq 'text/xml') {
		my $xpath = XML::XPath->new(xml => $entity->bodyhandle->as_string);
		$soap = $entity if ($xpath->exists('/soap-env:Envelope'))
	}
	return $soap;
}

sub find_content_entity {
	my ($self,$entity,$cid) = @_;
	foreach my $p ($entity->parts()) {
		return $p if ($p->head->mime_attr("content-id") and ($p->head->mime_attr("content-id") eq "<$cid>"));
	}
	return undef;
}

sub parse_content {
	my ($self,$msg,$content_entity) = @_;
	my $i = 0;
	foreach my $p ($content_entity->parts()) {
		$i++;
		$msg->{content}->{$i}->{content_id} = $p->head->mime_attr("content-id");
		$msg->{content}->{$i}->{filename} = $p->head->recommended_filename;
		my $content_type = $p->head->mime_attr("content-type");
		$msg->{content}->{$i}->{content_type} = $content_type;
		if ($content_type =~ /^text\//) {
			my $charset = $p->head->mime_attr("content-type.charset");
			$msg->{content}->{$i}->{data} = encode('cp1251',decode($charset,$p->bodyhandle->as_string));
		}
		else {
			$msg->{content}->{$i}->{data} = $p->bodyhandle->as_string;
		}
	}
}

sub save_content {
	my ($self,$id,$msg) = @_;
	my $msgpath = "/tmp/mms_client/$id/";
	mkpath($msgpath);
	
	foreach my $p ($msg->{content}->parts()) {
		my ($fh, $filename);
		$filename = $msgpath.$p->head->recommended_filename;
#		print $filename."\n";
		open($fh,">",$filename);
		my $contenttype = $p->head->mime_attr("content-type");
		if ($contenttype =~ /^text\//) {
			my $charset = $p->head->mime_attr("content-type.charset");
			print $fh encode('cp1251',decode($charset,$p->bodyhandle->as_string));
		}
		else {
			$p->bodyhandle->print($fh);
		}
		$fh->close();
	}
}

sub create_success_resp {
	my $self = shift;
	my $tr_id = $self->{msg}->{transaction_id};
	print $self->{query}->header(-type => 'text/xml');
	print "<?xml version='1.0' ?>";
	print "<env:Envelope xmlns:env='http://schemas.xmlsoap.org/soap/envelope/'>";
		print "<env:Header>";
			print "<mm7:TransactionID xmlns:mm7='http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0' env:mustUnderstand='1'>$tr_id</mm7:TransactionID>";
		print "</env:Header>";
		print "<env:Body>";
			print "<DeliverRsp xmlns='http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0'>";
				print "<MM7Version>5.3.0</MM7Version>";
				print "<Status><StatusCode>1000</StatusCode><StatusText>Success</StatusText></Status>";
			print "</DeliverRsp>";
		print "</env:Body>";
	print "</env:Envelope>";
	$self->{log}->info("Succsess resp created.");
}

sub create_fault_resp {
	my $self = shift;
	my $tr_id = $self->{msg}->{transaction_id};
	print $self->{query}->header(-type => 'text/xml');
	print "<?xml version='1.0' ?>";
	print "<env:Envelope xmlns:env='http://schemas.xmlsoap.org/soap/envelope/'>";
		print "<env:Header>";
			print "<mm7:TransactionID xmlns:mm7='http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0' env:mustUnderstand='1'>$tr_id</mm7:TransactionID>";
		print "</env:Header>";
		print "<env:Body>";
			print "<env:Fault>";
				print "<faultcode>env:Client</faultcode>";
				print "<faultstring>Client error</faultstring>";
				print "<detail>";
					print "<VASPErrorRsp xmlns='http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0'>";
						print "<MM7Version>5.3.0</MM7Version>";
						print "<Status>";
							print "<StatusCode>3000</StatusCode>";
							print "<StatusText>Server Error</StatusText>";
#							print "<Details>";
#								print "<app:Reason xmlns:app='http://vendor.example.com/MM7Extension'>Location not covered in service</app:Reason>";
#							print "</Details>";
						print "</Status>";
					print "</ VASPErrorRsp>";
				print "</detail>";
			print "</env:Fault>";
		print "</env:Body>";
	print "</env:Envelope>";
	$self->{log}->info("Fault resp created.");
}