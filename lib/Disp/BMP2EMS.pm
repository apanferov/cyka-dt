#!/usr/bin/perl -w
package Disp::BMP2EMS;

use strict;
use warnings;
use base 'Exporter';
use Disp::RecodeUtils qw (decodeBMP);
our @EXPORT_OK = qw( EMSPictureMessage );
our $VERSION = '0.1';

sub EMSPictureMessage {
	my ($data) = @_;

	my ($bytearray, $width, $height) = decodeBMP($data);
	return -1 unless ($bytearray);
	# check image size(s)
	# 1. width
	return -1 unless ( ($width % 8) == 0 );
	# 2. height
	if (($height*$width) <= 1056 ) {
		my @parts = ($bytearray,$bytearray,$bytearray);
	   my $id = int(rand(255));
		for (my $i=0; $i < @parts; $i++) {
			$parts[$i] = chr(length($parts[$i])+10).
				"\x00\x03".chr($id).chr(@parts).chr($i+1).
				"\x12".chr(length($parts[$i])+3)."\x00".chr(length($parts[$i])/$height).chr($height).
				$parts[$i]."\x0A";
		}
		return \@parts;

#		my @parts = (chr(length($bytearray)+5)."\x12".chr(length($bytearray)+3)."\x00".sprintf('%c',($width/8)).chr($height).$bytearray);
#		return \@parts;
	}
	else {
		my $part_width=POSIX::floor(128/$height);
		$width = int($width/8);
		my @parts = (undef) x POSIX::ceil($width/$part_width);
		while (length($bytearray) > 0) {
      	my $head = substr($bytearray,0,$width);
      	$bytearray = substr($bytearray,$width,length($bytearray));
			for (my $i=0; $i < @parts; $i++) {
				$parts[$i] .= substr($head,$i*$part_width,$part_width);
	      }
      }
	   my $id = int(rand(255));
		for (my $i=0; $i < @parts; $i++) {
			$parts[$i] = chr(length($parts[$i])+10).
				"\x00\x03".chr($id).chr(@parts).chr($i+1).
				"\x12".chr(length($parts[$i])+3)."\x00".chr(length($parts[$i])/$height).chr($height).
				$parts[$i];
		}
		return \@parts;
	}
}

sub EMSPictureMessage2 {
	my ($data) = @_;

	my ($bytearray, $width, $height) = decodeBMP($data);
	return -1 unless ($bytearray);
	# check image size(s)
	# 1. width
	return -1 unless ( ($width % 8) == 0 );
	# 2. height
	return -1 unless ( ($height*$width) <= 1056 );
	
	my $stream;
	# text
	$stream .= "\x12".chr(length($bytearray)+3)."\x00".sprintf('%c',($width/8)).chr($height).$bytearray;
	return $stream;
}

1;

