#!/usr/bin/perl
use strict;
use warnings;

package Disp::Object;

sub new {
	my $proto = shift;
	$proto = ref($proto) || $proto;
	if ( @_ == 0 ) {
		return bless { }, $proto;
	} elsif ( @_ == 1 ) {
		my ( $maybe_ref ) = shift;
		if ( UNIVERSAL::isa($maybe_ref, 'HASH') ) {
			return bless { %$maybe_ref }, $proto;
		} else {
			die "Single parameter to new is not hashref";
		}
	} elsif ( not @_ % 2 ) {
		return bless { @_ }, $proto;
	} else {
		die "Invalid number of arguments";
	}
}


1;
