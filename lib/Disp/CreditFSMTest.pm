# -*- encoding: utf-8; tab-width: 8 -*-
package Disp::CreditFSMTest;
use strict;
use warnings;
use utf8;
use Carp;
use English '-no_match_vars';

our $VERSION = '1.0.0';

use base qw(Test::Class);
use Test::More;
use Test::Exception;
use YAML qw/LoadFile/;
use Storable qw/dclone/;
use Data::Dumper;
use Encode qw/is_utf8 decode encode/;
use Log::Log4perl qw/get_logger/;
use Carp;
use POSIX qw/strftime/;

sub test_001_use : Test {
    require_ok('Disp::CreditFSM');
}

sub test__start__initial_delivered : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};

    Disp::CreditFSM::run({
        world => $self->{world},
        event => {
            event => 'initial_delivered',
            message => $data->{message}{initial_delivered},
        },
        line => $data->{line}{1},
        messages => [ $data->{message}{initial_delivered} ],
    });

    is_deeply $data->{line}{1}, $data->{line}{'1_after_initial_delivered'};
    is_deeply $world->{enqueue_partner_notify}, [ $data->{notifies}{'1_after_initial_delivered'} ];
}

sub test__open__push : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};

    my $line = dclone($data->{line}{'1_after_initial_delivered'});

    $world->{now} = '2010-10-15 23:17:08';
    $data->{line}{open_after_push}{last_push_at} = '2010-10-15 23:17:08';

    Disp::CreditFSM::run({
        world => $self->{world},
        event => {
            event => 'push',
        },
        line => $line,
        messages => [ $data->{message}{initial_delivered} ],
    });

    is_deeply( $line, $data->{line}{open_after_push}, "last_push_at updated" );

    $data->{push_texts}{1}{msg} = encode('cp1251', decode('utf-8', $data->{push_texts}{1}{msg}));
    is_deeply $world->{push_mt}, [ $data->{push_texts}{1} ], "World push_mt was called once and with valid arguments";
}

sub test__open__full_write_off : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};

    Disp::CreditFSM::run({
        world => $self->{world},
        event => {
            event => 'write_off',
            message => $data->{message}{full_write_off},
        },
        line => $data->{line}{'1_after_initial_delivered'},
        messages => [ $data->{message}{initial_delivered}, $data->{message}{full_write_off} ],
    });

    is_deeply $data->{line}{'1_after_initial_delivered'}, $data->{line}{complete}, "line completed";
    is_deeply $world->{enqueue_partner_notify}, [ $data->{notifies}{complete} ], "partner is notified about line completion";
}

sub test__open__partial_write_off : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};

    Disp::CreditFSM::run({
        world => $self->{world},
        event => {
            event => 'write_off',
            message => $data->{message}{partial_write_off},
        },
        line => $data->{line}{'1_after_initial_delivered'},
        messages => [ $data->{message}{initial_delivered}, $data->{message}{partial_write_off} ],
    });

    # $world->{now} = '2010-10-10 17:00:35';
    # $data->{line}{open_after_partial}{next_action} = 'push';
    # $data->{line}{open_after_partial}{next_action_date} = '2010-10-11 10:00:00';

    is_deeply $data->{line}{'1_after_initial_delivered'}, $data->{line}{open_after_partial}, "line is back to open after partial write_off";
    is_deeply $world->{enqueue_partner_notify}, [ $data->{notifies}{partial_write_off} ], "partner is notified about partial write_off";
}

sub test__open__partial_rejected : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};

    Disp::CreditFSM::run({
        world => $self->{world},
        event => {
            event => 'rejected',
            message => $data->{message}{partial_rejected},
        },
        line => $data->{line}{'1_after_initial_delivered'},
        messages => [ $data->{message}{initial_delivered}, $data->{message}{partial_rejected} ],
    });

    # $world->{now} = '2010-10-10 17:00:35';
    # $data->{line}{open_after_partial}{next_action} = 'push';
    # $data->{line}{open_after_partial}{next_action_date} = '2010-10-11 10:00:00';

    is_deeply $data->{line}{'1_after_initial_delivered'}, $data->{line}{open_after_rejected}, "line is back to open after partial reject";
    is_deeply $world->{enqueue_partner_notify}, undef, "partner is not notified about partial reject";
}

sub test_003_datetime_older_than : Test(4) {
    my $self = shift;
    my $data = $self->{data};
    my $s = \&Disp::CreditFSM::datetime_older_than;

    ok $s->('2010-10-29 20:33:06', $data->{line}{1}{created_at}, 86400);
    ok not $s->('2010-10-28 20:33:06', $data->{line}{1}{created_at}, 86400);

    ok $s->('2010-11-15 20:33:06', $data->{line}{1}{created_at}, 864000);
    ok not $s->('2010-11-03 20:33:06', $data->{line}{1}{created_at}, 864000);
}

sub test_002_datetime_to_unix_time : Test(14) {
    my $self = shift;
    my $data = $self->{data};
    my $s = \&Disp::CreditFSM::datetime_to_unixtime;

    dies_ok { $s->('garbage') } "garbage in dates";
    dies_ok { $s->('2010-01-01 23:83:43') } "malformed time part";
    dies_ok { $s->('2010-27-13 23:55:43') } "malformed date part";
    dies_ok { $s->('2010-02-30 23:55:43') } "malformed date part";

    for (1..10) {
        my $ts = 1000000000 + int(rand(200000000));
        my $dt = strftime('%Y-%m-%d %H:%M:%S', localtime($ts));
        is $s->($dt), $ts, "datetime $dt must be converted back to timestamp $ts";
    }
}

sub test_credit_line_to_event : Test(3) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};
    my $s = \&Disp::CreditFSM::credit_line_to_event;

    my $line = $data->{line}{'1_after_initial_delivered'};

    $world->{now} = '2010-01-29 12:33:43';
    $world->{params}{credit}{stale_initial_check_interval} = 30 * 60;
    $world->{params}{credit}{bill_interval} = 60 * 60;
    $world->{params}{credit}{resend_delay} = 10 * 60;

    $line->{created_at} = '2010-01-29 11:15:43';
    is_deeply [$s->($world, $line)], [{
        event => 'stalled'
    }];

    $world->{now} = '2010-01-29 12:15:43';
    $line->{created_at} = '2010-01-29 12:00:00';
    $line->{last_push_at} = '2010-01-29 12:00:05';
    $world->{params}{credit}{resend_delay} = 10 * 60;

    is_deeply [$s->($world, $line)], [{
        event => 'push'
    }];

    $world->{now} = '2010-01-29 12:01:43';
    is_deeply [$s->($world, $line)], [];
}

sub test_credit_message_to_event : Test(2) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};
    my $s = \&Disp::CreditFSM::credit_message_to_event;

    is_deeply [ $s->($world, $data->{message}{initial}) ], [ ];
    is_deeply(
        [ $s->($world, $data->{message}{initial_delivered}) ],
        [ { event => 'initial_delivered', message => $data->{message}{initial_delivered} } ],
    );
}

sub test_004_expand_time_interval : Test(2) {
    my ( $self ) = @_;

    my $data = $self->{data};
    my $world = $self->{world};
    my $s = \&Disp::CreditFSM::expand_time_interval;

    $world->{params}{credit}{local_time_offset} = 3 * 60;

    is_deeply(
        [ $s->($world, 7*60, "10:00-15:00") ],
        [ (6+1)*3600, (11-1)*3600 ]
    );

    is_deeply(
        [ $s->($world, 7*60, "01:00-05:00") ],
        [ (21+1)*3600, (1-1)*3600 ],
    );

}

sub test_in_send_range : Test(6) {
    my ( $self ) = @_;
    my $data = $self->{data};
    my $world = $self->{world};
    my $s = \&Disp::CreditFSM::in_send_range;


    my @tests = (
        [ 1296739321, # 2011-02-03 16:22:01 MSK
          36000, 61200, # 10:00-17:00
          'can'
        ],
        [ 1296703368,  # 2011-02-03 06:22:48 MSK
          36000, 61200, # 10:00-17:00
          'not'
        ],
        [ 1296764600,  # 2011-02-03 23:23:20 MSK
          36000, 61200, # 10:00-17:00
          'not'
        ],
        [ 1296739321, # 2011-02-03 16:22:01 MSK
          61200, 36000, # 17:00-10:00
          'not'
        ],
        [ 1296703368,  # 2011-02-03 06:22:48 MSK
          61200, 36000, # 17:00-10:00
          'can'
        ],
        [ 1296764600,  # 2011-02-03 23:23:20 MSK
          61200, 36000, # 17:00-10:00
          'can'
        ],
    );

    for my $t (@tests) {
        my ($now_ts, $start, $end, $expected) = @$t;
        $world->{now_ts} = $now_ts;
        $data->{line}{1}{send_interval_start} = $start;
        $data->{line}{1}{send_interval_end} = $end;
        my $result = $s->($world, $data->{line}{1});
        $result = not $result if $expected eq 'not';
        my $test_name = "At $now_ts from $start to $end - '$expected'";
        ok($result, $test_name);
    }
}

sub test_initialize_line : Test {
    my ( $self ) = @_;

    my $data = $self->{data};
    my $world = $self->{world};
    my $s = \&Disp::CreditFSM::initialize_line;

    $world->{params}{credit}{send_interval} = "9:00-17:00";
    delete $data->{line}{1}{next_action_date};
    delete $data->{line}{1}{next_action};
    delete $data->{line}{1}{last_try_date};
    $data->{line}{1}{created_at} = $world->now;

    $world->{params}{credit}{local_time_offset} = 3 * 60;

    is_deeply (
        [$s->($world, $data->{message}{initial})],
        [$data->{line}{1}],
    );
}

sub load_data : Test(setup) {
    my ( $self ) = @_;
    $self->{data} = LoadFile($FindBin::RealBin . "/../lib/Disp/t/040_disp_creditfsm.yml");
    $self->{world} = WorldMock->new;
}

sub log_separator : Test(setup) {
    get_logger('')->debug('--------------------------------------------------------------------------------');
}

package WorldMock;
use Carp;

sub new {
    my $params = {
        credit => {
            bill_interval => 600,
        },
    };
    return bless {
        now => '2010-10-27 14:06:42',
        params => $params,
    }, shift;
}

sub enqueue_partner_notify {
    my $self = shift;
    my %params = @_;

    push @{$self->{enqueue_partner_notify}}, \%params;
    return;
}

sub push_mt {
    my $self = shift;
    my %params = @_;

    push @{$self->{push_mt}}, \%params;
    return;
}

sub now {
    return $_[0]->{now};
}

sub now_ts {
    return $_[0]->{now_ts};
}

sub param {
    my ( $self, $param, $section ) = @_;
    $section ||= '_';
    unless (exists $self->{params}{$section}{$param}) {
        confess "No mock param '$param' in section '$section'";
    }
    return $self->{params}{$section}{$param};
}

sub gmt_offset {
    return 3 * 60; # GMT+03:00
}

1;
