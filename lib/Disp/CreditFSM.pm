# -*- encoding: utf-8; tab-width: 8 -*-
package Disp::CreditFSM;
use strict;
use warnings;
use utf8;
use Carp;
use English '-no_match_vars';

our $VERSION = '1.0.0';

use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use Encode qw/encode decode/;
use Time::Local;
use POSIX qw/strftime/;

sub notify_partner($);
sub update_remaining();
sub send_push();
sub decide();
sub notify_partner_about_writeoff();

my %fsm = (
    failed => {
    },
    complete => {
    },
    incomplete => {
    },
    start => {
        initial_rejected => {
            actions => [ notify_partner('initial_rejected') ],
            target => 'failed',
        },
        stalled => {
            actions => [ notify_partner('initial_rejected') ],
            target => 'failed',
        },
        initial_delivered => {
            actions => [ update_remaining, notify_partner('initial_delivered')],
            target => 'open',
        },
    },
    open => {
        push => {
            actions => [ send_push ],
            target => 'open',
        },
        stalled => {
            actions => [ decide ],
            target => 'decide',
        },
        write_off => {
            actions => [ update_remaining, decide ],
            target => 'decide',
        },
        rejected => {
            actions => [ decide ],
            target => 'decide',
        },
        cancelled => {
            actions => [ notify_partner('incomplete') ],
            target => 'incomplete',
        },
    },
    decide => {
        schedule => {
            actions => [ notify_partner_about_writeoff ],
            target => 'open',
        },
        debt_paid => {
            actions => [ notify_partner('complete') ],
            target => 'complete',
        },
        timed_out => {
            actions => [ notify_partner('complete') ],
            target => 'incomplete',
        },
    },
);

sub run {
    my ( $initial_event ) = @_;

    my $log = get_logger('credit-etl.fsm');

    my $event_queue = [ $initial_event->{event} ];
    my $line = $initial_event->{line};
    my $messages = $initial_event->{messages};
    my $world = $initial_event->{world};

    $log->debug("Got event $initial_event->{event}{event} for credit_line $line->{credit_line_id} in state $line->{status}");

    my $add_event_sub = sub {
        my ( $event ) = @_;
        $log->debug("Add synthesized event $event->{event} for line $line->{credit_line_id}");
        push @$event_queue, $event;
    };

    while (@$event_queue) {
        my $event = shift @$event_queue;
        my $response = $fsm{$line->{status}}{$event->{event}};
        unless ($response) {
            $log->error("Event $event->{event} not supported in state $line->{status}");
	    next;
        }
        $log->debug("Line $line->{credit_line_id} will go from state $line->{status} to $response->{target}");
        if ($response->{actions}) {
            for my $action (@{$response->{actions}}) {
                my ( $sub, $sub_title ) = @$action;
                $log->debug("Executing '$sub_title' while doing state transition for line $line->{credit_line_id}");
                $sub->($event, $line, $messages, $add_event_sub, $world);
            }
        }
        $line->{status} = $response->{target};
    }
    return 1;
}

sub notify_partner($) {
    my ( $credit_type ) = @_;
    return [sub {
        my ( $event, $line, $messages, $add_event_sub, $world ) = @_;

        local $event->{message} = ($event->{message} || $messages->[0]);

        unless ($event->{message}) {
            confess "notify_partner: event has no attached message";
        }
        $world->enqueue_partner_notify(
            transaction_id         => $event->{message}{inbox_id},
            credit_installment_id  => $event->{message}{id},
            credit_type            => $credit_type,
            credit_total           => $event->{message}{credit_total},
            credit_remaining       => $event->{message}{credit_remaining},
            credit_installment     => $event->{message}{credit_installment},
        );
    }, 'notify_partner'];
}

sub update_remaining() {
    my $log = get_logger('credit-fsm.update_remaining');
    return [
        sub {
            my ( $event, $line, $messages, $add_event_sub, $world ) = @_;
            unless ($event->{message}) {
                confess "update_remaining: event has no attached message";
            }
            $log->debug("Updating remaining of line $line->{credit_line_id} from $line->{remaining} to $event->{message}{credit_remaining}");
            $line->{remaining} = $event->{message}{credit_remaining};
        }, 'update_remaining'
    ];
}

sub send_push() {
    return [
        sub {
            my ( $event, $line, $messages, $add_event_sub, $world ) = @_;

            my $push_msg = encode('cp1251', <<EOF);
status: notice
destination: $line->{abonent}
credit-message-id: $line->{initial_credit_message_id}
credit-installment: $line->{remaining}

Произведено списание в рамках услуги "Контент по карману"
EOF
            my $push_service_id = $messages->[0]{service_id};
            $world->push_mt(service_id => $push_service_id, msg => $push_msg);
            $line->{last_push_at} = $world->now;
        },
        'send_push',
    ];
}

sub decide() {
    return [
        sub {
            my ( $event, $line, $messages, $add_event_sub, $world ) = @_;

            my $new_event;

            if ( $event->{event} eq 'write_off' and defined $line->{remaining} and $line->{remaining} == 0 ) {
                $new_event = 'debt_paid';
            } elsif ( datetime_older_than($world->now, $line->{created_at}, $world->param(bill_interval => 'credit')) ) {
                $new_event = 'timed_out';
            } else {
                $new_event = 'schedule';
            }

            $add_event_sub->({
                event => $new_event,
                ( $event->{message} ? ( message => $event->{message}) : ())
            });
        },
        'decide',
    ]
}

sub notify_partner_about_writeoff() {
    return [
        sub {
            my ( $event, $line, $messages, $add_event_sub, $world ) = @_;
            if ($event->{message} and $event->{message}{credit_type} eq 'write_off') {
                notify_partner('write_off')->[0]->( $event, $line, $messages, $add_event_sub, $world );
            }
        },
        'notify_partner_when_message_present',
    ]
}

################################################################################
# Helpers
################################################################################
sub datetime_older_than {
    my ( $now, $datetime, $bill_interval ) = @_;

    my $now_ts = datetime_to_unixtime($now);
    my $line_ts = datetime_to_unixtime($datetime);

    return ($line_ts + $bill_interval < $now_ts);
}

sub datetime_to_unixtime {
    my ( $datetime ) = @_;

    confess "Bad datetime format '$datetime'"
        unless $datetime =~ /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/;
    my ( $year, $mon, $mday, $hour, $min, $sec ) = ( $1, $2, $3, $4, $5, $6 );
    return timelocal($sec, $min, $hour, $mday, $mon - 1, $year);
}

sub credit_line_to_event {
    my ( $world, $line ) = @_;

    if ( $line->{status} eq 'start') {
        if (datetime_older_than(
            $world->now, $line->{created_at}, $world->param(stale_initial_check_interval => 'credit')
        ) ) {
            return { event => 'stalled' };
        }
    } elsif ($line->{status} eq 'open') {
        if (datetime_older_than(
            $world->now, $line->{created_at}, $world->param(bill_interval => 'credit')
        ) ) {
            return { event => 'stalled' };
        }
        unless ($line->{last_push_at}) {
            confess "No last_push_at for line $line->{credit_line_id}";
        }

        unless (in_send_range($world, $line)) {
            return;
        }

        if (datetime_older_than(
            $world->now, $line->{last_push_at}, $world->param(resend_delay => 'credit')
        ) ) {
            return { event => 'push' };
        }
    }

    return;
}

=head2 in_send_range($world, $credit_line) -> boolean

Возвращает истину если сейчас можно отсылать сообщение абоненту
(местное время абонента находится в заданном интервале).

=cut
sub in_send_range {
    my ( $world, $line ) = @_;

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($world->now_ts());
    my $lt = $hour * 3600 + $min * 60 + $sec; # local time of day

    my $ss = $line->{send_interval_start};
    my $se = $line->{send_interval_end};

    if ($ss > $se) { # Проходит по границе суток
        return (($ss < $lt) or ($lt < $se));
    } else {
        return (($ss < $lt) and ($lt < $se));
    }

    return;
}



my %actionable_messages = map { $_ => 1 } qw/initial_delivered initial_rejected cancelled rejected write_off/;

sub credit_message_to_event {
    my ( $world, $message ) = @_;

    if ($actionable_messages{$message->{credit_type}}) {
        return {
            event => $message->{credit_type},
            message => $message,
        };
    }

    return;
}

sub expand_time_interval {
    my ( $world, $gmt_offset, $interval ) = @_;

    confess "Unparseable time interval '$interval'" unless $interval =~ /^(\d{1,2}):(\d{1,2})-(\d{1,2}):(\d{1,2})$/;

    my $start = ( $1 + 1 ) * 3600 + $2 * 60 + $world->param(local_time_offset => 'credit') * 60 - $gmt_offset * 60;
    my $end   = ( $3 - 1 ) * 3600 + $4 * 60 + $world->param(local_time_offset => 'credit') * 60 - $gmt_offset * 60;

    for ($start, $end) {
        if ($_ >= 86400) {
            $_ -= 86400;
        } elsif ($_ < 0 ) {
            $_ += 86400;
        }
    }

    return ( $start, $end );
}

sub initialize_line {
    my ( $world, $message ) = @_;

    my $gmt_offset = $world->gmt_offset($message->{abonent});
    my $send_interval = $world->param( send_interval => 'credit' );
    my ( $send_interval_start, $send_interval_end ) = expand_time_interval($world, $gmt_offset, $send_interval);

    my %result = (
        status => 'start',
        credit_line_id => $message->{credit_line_id},
        total => $message->{credit_total},
        remaining => $message->{credit_remaining},
        initial_credit_message_id => $message->{id},
        abonent => $message->{abonent},
        send_interval_start => $send_interval_start,
        send_interval_end => $send_interval_end,
        last_status_change => $world->now,
        last_push_at => $message->{updated_at},
        created_at => $world->now,
    );

    return \%result;
}

# TODO Proper calculation of next action date
my %active_states = map { $_ => 1 } qw/start open/;
sub next_action_date {
    my ( $world, $line ) = @_;

    if ($active_states{$line->{status}}) {
        return strftime('%Y-%m-%d %H:%M:%S', localtime(datetime_to_unixtime($world->now) + $world->param('scheduling_tick' => 'credit')));
    } else {
        return undef;
    }
}

1;
