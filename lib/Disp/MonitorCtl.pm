#!/usr/bin/perl -w
package Disp::MonitorCtl;
use strict;
use base 'Disp::Object';
use Iterator_Utils qw/:all/;
use IO::File;

# Accompaniing module to Disp::DaemonManager
our $VERSION	= 0.1;		# Версия пакета.

sub new_aggregated {
	my $proto = shift;
	return new Disp::MonitorCtl::Aggregator(map { new Disp::MonitorCtl(%$_) } @_);
}

sub new {
	my $proto = shift;
	my $self = $proto->SUPER::new(@_);

	unless ($self->{status} =~ m,^[-a-z0-9\./_]+$,i and $self->{pid} =~ m,^[-a-z0-9\./_]+$,i) {
		die "status and pid files must be speciefied in monitorctl";
	}

	return $self;
}

sub _status_iterator {
	my ( $self ) = @_;

	my $fh = new IO::File "< $self->{status}";
	my $stamp = <$fh>;
	chomp $stamp;
	return imap {
		chomp;
		my $a = { stamp => $stamp };
		@{$a}{qw/name state pid seconds/} = split /\s+/, $_, 4;
		$a;
	} filehandle_iterator($fh);
}

sub list {
	my ( $self ) = @_;
	my $it = $self->_status_iterator;
	my @res;
	my $cur;
	push @res, $cur while $cur = NEXTVAL($it);
	return \@res;
}

sub kill_child {
	my ( $self, $child, $sig ) = @_;
	$sig ||= 'TERM';

	my $it = igrep { $_->{name} eq $child } $self->_status_iterator;

	my $child = NEXTVAL($it);

	if ($child) {

		# Untaint pid
		unless ( $child->{pid} =~ /^(\d+)$/ ) {
			return 0;
		}

		my $pid = $1;

		my $cnt = kill $sig, $pid;
		return $cnt;
	}

	return 0;
}

sub reload {
	my ( $self ) = @_;

	my $ret = 0;

	open FILE, "< $self->{pid}";
	my $pid = <FILE>;
	chomp $pid;

	if ($pid > 0) {
		$ret = kill "HUP", $pid;
	}

	close FILE;
	return $ret;
}

sub print_long_list {
	my ( $self, $fh ) = @_;
	print $fh join("\t", @{$_}{qw/name state pid seconds stamp/})."\n"
 	  for @{$self->list};
}


package Disp::MonitorCtl::Aggregator;
use base 'Disp::Object';

sub new {
	my $self = shift->SUPER::new();
	$self->{monitors} = \@_;
	return $self;
}

sub list {
	my $self = shift;
	[ map { @{$_->list} } @{$self->{monitors}}  ];
}

sub kill_child {
	my ($self, $name, $sig) = @_;
	$sig ||= 'TERM';
	foreach (@{$self->{monitors}}) {
		if ($_->kill_child($name, $sig)) {
			return 1;
		}
	}
	return 0;
}

sub reload {
	my $self = shift;
	foreach (@{$self->{monitors}}) {
		$_->reload;
	}
	return 1;
}

sub print_long_list {
	my ( $self, $fh ) = @_;
	foreach (@{$self->{monitors}}) {
		$_->print_long_list($fh);
	}
}


1;
