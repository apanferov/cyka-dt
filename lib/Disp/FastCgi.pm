#!/usr/bin/perl
package Disp::FastCgi;
use strict;
use warnings;
use Disp::Utils;
use POSIX;

sub import {

	$ENV{FCGI_SOCKET_PATH} = $_[1];
	$ENV{FCGI_LISTEN_QUEUE} = 20;

	require CGI::Fast;
	require FCGI::ProcManager;
}

sub run {
	my ( $main, $init, $name, $options ) = @_;
	$name ||= $0;
	$options = {} unless (ref $options eq 'HASH');
	my $processes = $options->{processes} || 2;

	my $proc_manager = FCGI::ProcManager->new({ n_processes => $processes });
	eval {
		$SIG{USR1} = sub {};
		$proc_manager->pm_manage();
		$init->() if UNIVERSAL::isa($init, 'CODE');

                my $HANDLER = $SIG{USR1};
                POSIX::sigaction(SIGUSR1,
                        POSIX::SigAction->new(
                                sub { &{$HANDLER}() }, POSIX::SigSet->new(SIGUSR1), &POSIX::SA_RESTART));

		while (my $q = new CGI::Fast) {
			$proc_manager->pm_pre_dispatch();
			$main->($q);
			$proc_manager->pm_post_dispatch();
		}
	};
	if ($@) {
		yell("fastcgi($name): $@", code => 'FATAL',
			 process => "fastcgi($name)",
			 header => "fastcgi($name): $@",ignore => { log4perl => 1 }) if ($@);
	}
}
1;
