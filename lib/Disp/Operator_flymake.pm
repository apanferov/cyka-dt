#!/usr/bin/perl
# -*- encoding: utf-8 -*-
package Disp::Operator;
use warnings;
use strict;
use base 'Disp::Object';

our $VERSION	= 0.1;		# Версия пакета.

sub find_operator_by_name {
	my ( $self, $name ) = @_;
	return Config->dbh->selectrow_hashref("select * from set_operator where short_name = ?",undef,
										  $name);
}

my $default_output_operator;
sub default_output_operator {
	my ( $self ) = @_;
	$default_output_operator ||
	  ($default_output_operator = $self->find_operator_by_name('sms_traffic'));
}

=head2 determine_operator($abonent, %attrs) operator_smsc_pair

=cut
sub determine_operator {
	my ( $self, $abonent, %attrs ) = @_;
	my ( $smsc_id, $operator_id );

	($smsc_id, $operator_id) = $self->consult_num_map($abonent, $attrs{transport_type} || 'sms');

	# Allow caller to specify defaults for operator-id and smsc-id
	my ( $s_hack, $o_hack );
	$s_hack = 1 if ($attrs{hack_smsc}||"") =~ /^\d+$/;
	$o_hack = 1 if ($attrs{hack_operator}||"") =~ /^\d+$/;

	unless ($smsc_id and $operator_id) {
		if ($s_hack and $o_hack) {
			( $smsc_id, $operator_id ) = ( $attrs{hack_smsc}, $attrs{hack_operator} );
		} elsif ($s_hack) {
			$attrs{log}->info("Internal numhack triggered - $attrs{hack_smsc}")
			  if $attrs{log};

			my $tmp_op_id = Config->dbh->selectrow_array("select determine_operator(?,?,null)", undef, $abonent, $attrs{hack_smsc});
			if ($tmp_op_id) {
				$smsc_id  = $attrs{hack_smsc};
				$operator_id = $tmp_op_id;
				$attrs{log}->info("Internal numhack triggered give valid results - op:$operator_id, smsc:$smsc_id")
				  if $attrs{log};
			}
		} elsif ($o_hack) {
			$attrs{log}->info("Internal numhack triggered - operator is $attrs{hack_operator}")
			  if $attrs{log};
			$operator_id = $attrs{hack_operator};
		}
	}

	if ($attrs{force_operator}) {
	    $operator_id = $attrs{force_operator};
	    $smsc_id = undef;
	}

	# Пробуем номерные ёмкости в случае неудачи
	unless ($operator_id) {
		( $operator_id ) = Config->dbh->selectrow_array("select determine_operator(?,null,null)", undef, $abonent);
	}

	# По умолчанию - СМСТраффик
	unless ($operator_id) {
		$operator_id = $self->default_output_operator()->{id};
		if ($operator_id) {
			$attrs{log}->info("In 'notice' operator for abonent '$abonent' undetermined - using sms_traffic fallback - $operator_id")
			  if $attrs{log};
		}
	}

	# Если не задан СМСЦ - используем таковой по умолчанию для оператора
	my $tmp_oper = Disp::Operator->load($operator_id);

	# И заодно выполняем проверку, что нам не подсунули левак.
	return unless $tmp_oper;

	$smsc_id ||= $tmp_oper->{smsc_id};

	return ( $operator_id, $smsc_id ) if $operator_id and $smsc_id;
	return;
}

sub determine_operator_new {
	my ( $self, $abonent, %attrs ) = @_;

	my $smsc_id;
	my ( $operator_id ) = Config->dbh->selectrow_array("select operator_id from tr_num_map where abonent = ? and transport_type = 'sms'",
													   undef, $abonent);
	### Определение оператора
    $operator_id = $attrs{force_operator}
	  if ($attrs{force_operator});

	# Пробуем номерные ёмкости
	unless ($operator_id) {
		( $operator_id ) = Config->dbh->selectrow_array("select determine_operator(?,null,null)", undef, $abonent);
	}

	unless ($operator_id) {
		$operator_id = $attrs{hack_operator}
		  if (($attrs{hack_operator}||"") =~ /^\d+$/);
	}

	# По умолчанию
	unless ($operator_id) {
		$operator_id = $self->default_output_operator()->{id};
	}

	# Если не задан СМСЦ - используем таковой по умолчанию для оператора
	my $tmp_oper = Disp::Operator->load($operator_id);

	# И заодно выполняем проверку, что нам не подсунули левак.
	return unless $tmp_oper;

	$smsc_id ||= $tmp_oper->{smsc_id};

	return ( $operator_id, $smsc_id );
}

sub consult_num_map {
	my ($self, $dest, $transport) = @_;

	my @result = Config->dbh->selectrow_array("select smsc_id, operator_id from tr_num_map where abonent = ? and transport_type = ?",
											  undef, $dest, $transport);
	return @result;
}

my %loaded;
sub load {
	my ( $proto, $id ) = @_;

	$loaded{$id} = Config->dbh->selectrow_hashref("select * from set_operator where id = ?",undef, $id)
	  unless $loaded{$id};

	return $loaded{$id};
}

1;
