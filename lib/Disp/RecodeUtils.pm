#!/usr/bin/perl -w
# -*- coding: windows-1251-unix -*-
package Disp::RecodeUtils;

use strict;
use warnings;
use base 'Exporter';
use Encode;
use Encode::GSM0338;
use MIME::Base64;
use Image::Magick;
use Net::SMPP;
use File::Path qw (mkpath);

our @EXPORT_OK = qw(decodeBMP pack_long cut_parts pack_7bit pack_7bit_old ringtonetools hex2byte gsm_to_cp1251 cp1251_to_gsm gsm_compatible ucs2_to_cp1251 cp1251_to_ucs2 test decode_cp1251);
our $VERSION	= '0.1';

sub ucs2_to_cp1251 {
	my $data = shift;
	encode('cp1251',decode('UCS-2',$data));
}

sub decode_cp1251 {
	my $data = shift;
	$data = Encode::decode('cp1251',$data);
	$data =~ s/\|/"\x0A"/eg;
	$data =~ s/&#x([\da-f]{1,4});/chr(hex($1))/ieg;
	return $data;
}

sub cp1251_to_ucs2 {
	my $data = shift;
	return encode('UCS-2',decode_cp1251($data));
}

sub gsm_to_cp1251 {
	my $data = shift;
	return Encode::encode('cp1251',Encode::GSM0338::decode('gsm0338',$data));
}

sub cp1251_to_gsm {
	my $data = shift;
	return Encode::encode('gsm0338',decode_cp1251($data));
}
		
sub gsm_compatible {
	my $data = shift;
	my $ddata = decode_cp1251($data);
	eval {
		Encode::encode('gsm0338',$ddata,Encode::FB_CROAK);
	};
	return 0 if ($@);
	return 1;
}

sub get_length {
	my $data = shift;
	return length(decode_cp1251($data));
}

# decode BMP into binary array 
sub decodeBMP {
	my ($data) = @_;

	my $image=Image::Magick->new(magick=>'bmp');
	return -1 unless $image;

	$image->BlobToImage($data =~/^BM/ ? $data  : pack("H*",$data ) ); 

	# convert to monochrome
	$image->Set(magick => 'mono');
	$image->Negate;
	my $monochrome = $image->ImageToBlob( );

	# reverse bitorder
	my $ba = pack('b*',unpack('B*', $monochrome));

	my $width = $image->Get('columns');
	my $height = $image->Get('height');

	undef $image;
	return ($ba, $width, $height);
}

sub ringtonetools {
	my ($intype,$outtype,$msg,$id) = @_;
	mkpath('/tmp/ringtonetools');
	my $filetmp = "/tmp/ringtonetools/$id.$intype";
	my $fh;
	my @parts;
	my $opts = '';
	$opts .= ' -ems' if ($outtype eq 'imy');
	$opts .= ' -x' if ($outtype eq 'nokia');
	open ($fh ,">", $filetmp);
	print $fh $msg;
  	$fh->close();
  	my $rtt_path = Config->param('ringtonetools_path','decoder');
	my $a = `$rtt_path -intype $intype -outtype $outtype $opts $filetmp $filetmp.$outtype 2>&1`;
	if ( my $ret = $? >> 8 ) {
		die("Ringtonetools died with code $ret and message $a.");
	}
	system("");
#	unlink($filetmp);
	my $pattern = $outtype eq 'seo' ? $filetmp.'?*' : "$filetmp*";
	print $pattern;
	foreach my $filename ( <$pattern.$outtype>) {
		print "$filename \n";
		open($fh,"<",$filename);
		while (<$fh>) {
			chomp;
			push(@parts,$_) if (length($_) > 0);
		}
  		$fh->close();
#		unlink($filename);
	}
	return @parts;
}

# ����������� ����� PARTS �������� ��������� �������� UDH
sub pack_long {
	my ($parts,$additional_udh,$id) = @_;
	$additional_udh ||= '';
	if (@$parts <= 1) {
		$parts->[0] = chr(length($additional_udh)).$additional_udh.$parts->[0] if (length($additional_udh) > 0);
		return 1;
	}
   $id = int(rand(255)) unless ($id);
   for (my $i=0; $i < @$parts; $i++) {
   	my $udh = $additional_udh."\x00\x03".chr($id).chr(@$parts).chr($i+1);
   	$udh = chr(length($udh)).$udh;
      $parts->[$i] = $udh.$parts->[$i];
   }
   return 1;
}

sub pack_long2 {
	my ($parts,$additional_udh) = @_;
	if (@$parts <= 1) {
		$parts->[0] = chr(length($additional_udh)).$additional_udh.$parts->[0] if (length($additional_udh) > 0);
		return 1;
	}
	my $id = 1;
	for (my $i=0; $i < @$parts; $i++) {
		my $udh = $additional_udh."\x00\x03".chr($id).chr(@$parts).chr($i+1);
		$udh = chr(length($udh)).$udh;
		$parts->[$i] = $udh.$parts->[$i];
	}
   return 1;
}

# ��������� ������ PARTS �� ����� ������ �� ����� MAX_COUNT
sub cut_parts {
	my ($parts, $max_count, $cut_count) = @_;
	if ((@$parts==1) and (length($parts->[0])<=$max_count)) {
	   return @$parts;
	}
	for (my $i=0; $i < @$parts; $i++) {
	   if (length($parts->[$i]) > $cut_count) {
	      splice(@$parts,$i+1,0,substr($parts->[$i],$cut_count,length($parts->[$i])));
         $parts->[$i] = substr($parts->[$i],0,$cut_count);
      }
   }
   return @$parts;
}

#
sub pack_7bit
{
	my ($data) = @_;
	my $len = length($data);
	if (my $padding = (length($data) % 8)) {
		$data .= " " x (8 - $padding);
	}
	return Net::SMPP::pack_7bit($data);
}

sub pack_7bit_old
{
	my ($d) = @_;
	my $data = encode('GSM0338',$d); # ������ � 7��� � ������������ ������� �������� � GSM
	my $len = length($data);
	if (my $padding = (length($data) % 8)) {
		$data .= " " x (8 - $padding);
	}
	return Net::SMPP::pack_7bit($data);
}

sub hex2byte
{
	my $parts = shift;
	for (my $i=0; $i < @$parts; $i++) {
	   $parts->[$i] = pack('H*',$parts->[$i]);
   }
}

sub encode_textplain
{
	my ($data,$use8bit) = @_;
	my ($data_coding,@parts) = cut_text_sms($data);
	if (@parts==1) {
      if ($data_coding==8) {
         $parts[0] = encode('UCS-2',decode('cp1251',$parts[0]));
      }
      elsif (!$use8bit) {
         $parts[0] = pack_7bit($parts[0]);
      }
	   return ( $data_coding, 0x00, \@parts );
	}
	else {
	   my $id = int(rand(255));
	   for (my $i=0; $i < @parts; $i++) {
         if ($data_coding==8) {
            $parts[$i] = "\x05\x00\x03".chr($id).chr(@parts).chr($i+1).encode('UCS-2',decode('cp1251',$parts[$i]));
         }
         else {
            if (!$use8bit) {
               $parts[$i]=("\x00"x7).$parts[$i];
	            $parts[$i] = pack_7bit($parts[$i]);
               $parts[$i] =~ /^\x00{6}(.*)/;
            }
            $parts[$i] = "\x05\x00\x03".chr($id).chr(@parts).chr($i+1).$parts[$i];
	      }
	   }
	}

   return ( $data_coding, 0x40, \@parts );
}

sub gsm_to_cp1251_old {
	my $data = shift;
	$data =~ s/\x1b\x0a/\xf0/;
	$data =~ s/\x1b\x14/\xf1/;
	$data =~ s/\x1b\x28/\xf2/;
	$data =~ s/\x1b\x29/\xf3/;
	$data =~ s/\x1b\x2f/\xf4/;
	$data =~ s/\x1b\x3c/\xf5/;
	$data =~ s/\x1b\x3d/\xf6/;
	$data =~ s/\x1b\x3e/\xf7/;
	$data =~ s/\x1b\x40/\xf8/;
	$data =~ s/\x1b\x65/\xf9/;
	$data =~ tr/\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9/\x40\x3f\x24\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x0a\x3f\x3f\x0d\x3f\x3f\x3f\x5f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x1b\x3f\x3f\x3f\x3f\x20\x21\x22\x23\xa4\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x3f\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x3f\x3f\x3f\x3f\xa7\x3f\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x3f\x3f\x3f\x3f\x3f\x0a\x5e\x7b\x7d\x5c\x5b\x7e\x5d\x7c\x88/;
	return $data;
}

sub cp1251_to_gsm_old {
	my $data = shift;
	$data =~ tr/\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f/\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x0a\x3f\x3f\x0d\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x20\x21\x22\x23\x02\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x00\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x3f\x3f\x3f\x3f\x11\x3f\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x3f\x3f\x3f\x3f\x3f/;
	return $data;
}

sub gsm_compatible_old {
	my $data = shift;
	return $data =~ m/^[\x0a\x0d\x20-\x5a\x5f\x61-\x7a]*$/;
}

sub test {
	my $a = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f".
	"\x1B\x0A\x1B\x14\x1B\x28\x1B\x29\x1B\x2F\x1B\x3C\x1B\x3D\x1B\x3E\x1B\x40\x1B\x65"; #Extension
	my $b = "\x40\x3f\x24\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x0a\x3f\x3f\x0d\x3f\x3f\x3f\x5f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\x3f\xa0\x3f\x3f\x3f\x3f\x20\x21\x22\x23\xa4\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x3f\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x3f\x3f\x3f\x3f\xa7\x3f\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x3f\x3f\x3f\x3f\x3f".
	"\x0C\x5E\x7B\x7D\x5C\x5B\x7E\x5D\x7C\x88";
	$a = gsm_to_cp1251($a);
	_compare("1. gsm_to_cp1251",unpack("H*",$a),unpack("H*",$b));

	$a = "\x40&#x00A3;\x24&#x00A5;&#x00E8;&#x00E9;&#x00F9;&#x00EC;&#x00F2;&#x00E7;\x0A&#x00D8;&#x00F8;\x0D&#x00C5;&#x00E5;&#x0394;\x5F&#x03A6;&#x0393;&#x039B;&#x03A9;&#x03A0;&#x03A8;&#x03A3;&#x0398;&#x039E;\x0C\x5E\x7B\x7D\x5C\x5B\x7E\x5D&#x007C;\x88&#x00C6;&#x00E6;&#x00DF;&#x00C9;\x20\x21\x22\x23\xA4\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F&#x00A1;\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5A&#x00C4;&#x00D6;&#x00D1;&#x00DC;\xA7&#x00BF;\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A&#x00E4;&#x00F6;&#x00F1;&#x00FC;&#x00E0;";
	$b = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x0A\x1B\x14\x1B\x28\x1B\x29\x1B\x2F\x1B\x3C\x1B\x3D\x1B\x3E\x1B\x40\x1B\x65\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5A\x5B\x5C\x5D\x5E\x5F\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A\x7B\x7C\x7D\x7E\x7F";
	_compare("2. gsm_compatible",gsm_compatible($a),1);
	$a = cp1251_to_gsm($a);
	_compare("3. cp1251_to_gsm",unpack("H*",$a),unpack("H*",$b));
}

sub _compare {
	my ($num,$a,$b) = @_;
	if ($a eq $b) {
		print "$num - OK.\n";
		return 1;
	} else {
		print "$num - FAIL.\n$a\n$b\n";
		return 0;
	}
}

1;
