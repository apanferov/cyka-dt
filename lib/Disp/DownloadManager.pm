#!/usr/bin/perl
package Disp::DownloadManager;
use strict;
use warnings;
use Data::Dumper;
use HTTP::Request;
use URI;
use Time::HiRes qw/gettimeofday tv_interval/;
use Scalar::Util qw/weaken/;
use base 'Disp::Object';
use Log::Log4perl qw/get_logger/;
use curl_multi;

=head1 NAME

Disp::DownloadManager - �������� ������������ �������.

=head1 SYNOPSIS

 my $d = new Disp::DownloadManager;
 $d->{success_cb} = \&handle_success;
 $d->{error_cb} = \&handle_error;

 $d->add( {	uri => 'http://test.com/test.cgi',
 			params => { param1 => 123 },
 			some_app_specific_data => 'value1' } );
 # More add()-s here, or combined in single add()

 while ( $d->has_downloads ) {
	 $d->poll($timeout);
 }

=cut

my $log_messages = get_logger('messages');
my $log = get_logger;

sub new {
	my $self = shift->SUPER::new(@_);

	$self->{max_slots} ||= 150;
	$self->{http_timeout} ||= 110;
	$self->{tcp_timeout} ||= 20;
	$self->{curl} = new curl_multi::CurlMulti($self->{max_slots}, $self->{http_timeout}, $self->{tcp_timeout});

	return $self;
}

sub add {
	my ( $self, @links ) = @_;
	foreach my $link ( @links ) {
	  $self->_add_link($link);
	}
}

sub _add_link {
	my ( $self, $link ) = @_;

	my $seq = $self->_sequence_id;

	my $pool_entry = { original_data => $link,
			   pool_id => $seq,
			   url => $self->_make_url($link),
			   start_time => [gettimeofday] };

	$self->{pool}{$seq} = $pool_entry;
	$self->{curl}->add_url($pool_entry->{url}, $seq);

	$log_messages->info("DOWN_QUEUE: MSG$link->{original_message}{id} DOWN_QUEUE$seq queued link ".$pool_entry->{url});
}

sub poll {
	my ( $self, $timeout ) = @_;

	if ($self->{curl}->perform($timeout)) {
		my $r;
		while (exists (($r=$self->{curl}->fetch_result())->{status})) {
			my $pi = $self->{pool}{$r->{data}};
			delete $self->{pool}{$r->{data}};

			my $down_time = tv_interval($pi->{start_time});

			my $error;
			if ($r->{status} eq 'a timeout was reached') {
			    $error = 'TIMEOUT';
			}

			if ($r->{status} eq q{couldn't connect to server}) {
			    $error = 'CONNECT';
			}

			unless ($r->{contents} or $error) {
			    $error = 'EMPTY_RESPONSE';
			}

			if (not $error) {
			    $log_messages->info("GOT RESPONSE FOR DOWN$pi->{pool_id}/MSG$pi->{original_data}{original_message}{id}/SVC$pi->{original_data}{original_service}{id}/DOWNLOAD_TIME($down_time)");
			    $self->{success_cb}->( { link => $pi->{original_data},
						     data => $r->{contents},
						     url => $pi->{url},
						     time => $down_time } );
			} else {
			    $log_messages->error("TRANSPORT ERROR FOR DOWN$pi->{pool_id}/MSG$pi->{original_data}{original_message}{id}/ERROR($error)/DOWN_TIME($down_time)/SVC$pi->{original_data}{original_service}{id}");
			    $self->{error_cb}->( { link => $pi->{original_data},
						   network_error => $error,
						   http_error => 0,
						   error_reason => $r->{status},
						   message => "Empty service response",
						   url => $pi->{url} } );
			}
		}
	}
}

sub has_downloads {
  my ( $self ) = @_;
  return $self->{curl}->used_slots() > 0;
}

sub _sequence_id {
	my ( $self ) = @_;
	return ++$self->{sequence_id};
}

sub _make_url {
	my ( $self, $link ) = @_;
	my $u = URI->new($link->{url});
	$u->query_form( $link->{params} );
	return $u->as_string;
}

sub _on_error {
  my ( $self, $pool_item, $http_or_not, $reason ) = @_;
  delete $self->{pool}{$pool_item->{pool_id}};
  $self->{slots}--;
  $self->{error_cb}->( { link => $pool_item->{original_data},
						 network_error => not($http_or_not),
						 http_error => $http_or_not,
						 message => $reason,
						 url => $pool_item->{url},
					   }
					 );
}

sub report_active_downloads {
	my ( $self, $add_info_sub ) = @_;
	$log->info("IN_DOWN_QUEUE: ".$self->{curl}->used_slots);
	while (my($k,$v) = each %{$self->{pool}}) {
		$log->info("DOWN_QUEUE$k downloading ".$v->{url});
	}
	$log->info("IN_DOWN_QUEUE: END OF LIST");
}

sub free_slots {
  my ( $self ) = @_;
  return $self->{max_slots} - $self->{curl}->used_slots;
}

1;
