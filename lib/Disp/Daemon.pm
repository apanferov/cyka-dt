#!/usr/bin/perl -w
# $Id: $
package Disp::Daemon;
use strict;
use warnings;

=head1 NAME

Disp::Daemon - ������� ���� ����������.

=head1 DESCRIPTION

����� ��������� �������� ���� ����������: ������� �������� ���������,
����������� �� �� ��������� �������������� ��������, �������� ��������
�������.

��� �������� ������ ������������ �������� �ң� �������: InboxManager,
DownloadManager � ServiceManager.

�������� ����� ���������� �������� ���:

=over

=item *

� ������� InboxManager ���������� ��������� ���������� ��������� ���-���������.

=item *

��� ������� �� ���� ��������� ����������� ServiceManager ���������
������, � ������� ��� ������ ���� ����������. ������ �� �����,
ServiceManager ���������� URI, ������� ��������� � ������� �� �������
� DownloadManager.

=item *

DownloadManager ����� �������� ��� ���������� ���������� �������
�������� ��������� ��� callback'�. � ��� ��������������� �� ����
callback'�� ���������� ����������� ������� � ������ �� ServiceManager.

=back

�ӣ ��������� - ��� ��������� �������, ���-�� ��������� ��������,
������ ���������, ������������ ���������������� ������ � �.�.

=cut

our $VERSION	= 0.1;		# ������ ������.

use base 'Disp::Object';

use Log::Log4perl qw/get_logger/;
use Disp::Utils;
use Disp::DownloadManager;
use Disp::InboxManager;
use Disp::ServiceManager;
use Disp::Daemon::IPC;

my $log = get_logger();
my $log_messages = get_logger('messages');

sub new {
	my $self = shift->SUPER::new(@_);

	$self->{ipc}   = Disp::Daemon::IPC->worker(@_);
	$self->{down}  = Disp::DownloadManager->new;
	$self->{inbox} = Disp::InboxManager->new;

	hires_eval {
	    $self->{svc} = Disp::ServiceManager->new(inbox => $self->{inbox});
	} 'load_config', $log;

	# ����� ��� ���������� ����������, ���������� ����� ��������� �
	# ������� �� ������������. ����� ����, ��� ������� ����������,
	# ���������� ��������, ��������������� �������� ������
	# ����������. ���� ��� ������� 2 �������� - quit � reload.
	$self->{full_stop_operation} = 0;

	# ��� ����� full_stop_operation ��������������� �������� ��
	# ������������ ��������. ��� ����� �� �� � ��������������.
	$self->_init_signals;

	# ������� ��������� ������ ��� ��������� �������.
	$self->{down}{success_cb} = sub { $self->_real_success_cb(@_) };
	$self->{down}{error_cb} = sub { $self->_real_error_cb(@_) };

	$self->{ipc}->message('initialized', 'ok');
	return $self;
}

# ������������� ����������� �������� - ����� ��� ������ ���������, ���
# ������������ ��������.
sub _init_signals
{
	my($self) = @_;

	$SIG{$_} = sub { $self->{full_stop_operation} = 'quit'; }
		for qw(INT QUIT TERM);
}

# ����������� ��������� ����� DownloadManager'�, ������� ��������� �
# ������� InboxManager. ���������� ��������� �� ����������, ����
# ������������� ��������� ��� ������������ ��������. ��� �����������
# ����� ������� ������ ������ full_stop_operation.
sub _fill_free_slots {
	my ( $self, $count ) = @_;

	$count = 10;

	if ($self->{full_stop_operation}) {
		$log->info("Doing full_stop_operation '$self->{full_stop_operation}', new messages would not be added");
		return;
	}


	my $free_slots = $self->{down}->free_slots;
	return 0 unless $free_slots > $count;

	my $msgnum = 0;
	if ($count) {
	    my @messages;

	    my $skip_restart;
	    if ($free_slots < 70) {
		$skip_restart = 1;
	    }

	    $msgnum = @messages = @{$self->{inbox}->fetch($count, $skip_restart)};

	    $self->{down}->add(map { $self->{svc}->make_link($_) } @messages);

	    $log_messages->info("DOWNLOAD_QUEUE_LEN:".($self->{down}{max_slots}-$self->{down}->free_slots))
	      if @messages;
	}
	$msgnum;
}

# ���������� �� DownloadManager ��� �������� ���������� �������
sub _real_success_cb {
	my ( $self, $p ) = @_;
	$self->{svc}->handle_response($p);
# 	hires_eval {
# 	    $self->_fill_free_slots;
# 	} "fill_free_in_cb", $log_messages;
}

# ���������� �� DownloadManager ��� ��������� ������� �������
sub _real_error_cb {
	my ( $self, $p ) = @_;
	my $err;

	if ($p->{network_error} eq 'TIMEOUT') {
	    $err = Disp::ServiceManager::TIMEOUT_ERROR;
	} elsif ($p->{network_error} eq 'CONNECT') {
	    $err = Disp::ServiceManager::NETWORK_ERROR;
	} elsif ($p->{network_error} eq 'EMPTY_RESPONSE') {
	    $err = Disp::ServiceManager::EMPTY_RESPONSE_ERROR;;
	} elsif ($p->{http_error}) {
	    $err = Disp::ServiceManager::HTTP_ERROR;
	} else {
	    $err = Disp::ServiceManager::UNKNOWN_ERROR;
	    $log->error("Unknown error id DM callback")
	}

	$self->{svc}->mark_processed($p->{link}{original_message});
	$self->{svc}->handle_error( error => $err,
				    err_message => $p->{error_reason},
				    service => $p->{link}{original_service},
				    shandler => $p->{link}{original_shandler},
				    insms_count => $p->{link}{original_message}{insms_count},
				    abonent => $p->{link}{original_message}{abonent},
				    plug_id => $p->{link}{original_message}{plug_id},
				    response => $p->{data},
				    url => $p->{url},
				    identifiers => { inbox_id => $p->{link}{original_message}{id} },
				    orig_inbox_msg => $p->{link}{original_message} );
	# $self->_fill_free_slots;
};

# �������� ����
sub run
{
	my($self) = @_;

	$log->info("Dispatcher worker main loop reached");
	eval
	{
		my(undef, $pos) = $self->{ipc}->wait_message('go', 60);
		$pos && $self->{inbox}->max_inbox_id($pos);

		my $stopped_sent = 0;
		while (1)
		{
			if (getppid == 1)
			{
				$log->warn("Worker [$$] is orphan, exiting");
				$self->{full_stop_operation} = 'quit';
			}

			if (!$stopped_sent && $self->{full_stop_operation} eq 'quit')
			{
				getppid > 0 && $self->{ipc}->message('stopped', $self->{inbox}->max_inbox_id);
				$stopped_sent = 1;
			}

		    my $inboxmsg_num = $self->_fill_free_slots;

			if ($self->{down}->has_downloads)
			{
				$self->{down}->poll(1);
		    }
			elsif ($self->{full_stop_operation} eq 'quit')
			{
			    $log->info("Queue is empty, quiting");
			    last;
			}
			sleep(1) unless ($inboxmsg_num);
		}
	};
	if ($@)
	{
		my $err = $@;
		$log->error("ABNORMAL PROGRAM TERMINATION: $err");
		$self->{down}->report_active_downloads;
		die $err;
	}
	$log->info("Out of main loop, normal termination");
}

1;
__END__
