#!/usr/bin/perl
package Disp::EmailSender;
use warnings;
use strict;
use base 'Disp::Object';
use Data::Dumper;
use MIME::QuotedPrint;
use Encode qw/encode decode/;


sub init {
    my ( $self, $daemon ) = @_;

    $self->{smsc_id} = $daemon->{smsc_id};
    $self->{pr_name} = "snd_$self->{smsc_id}";

    $daemon->{db}->load_statistics($self->{pr_name});

    # $self->{relay} = Config->param( relay => 'smtp' );
    # $self->{from} = Config->param( from => 'smtp' );
    # $self->{envelope_from} = Config->param( envelope_from => 'smtp' );
    # $self->{domain} = $daemon->{conf}{host};

    return;
}

sub deinit {
    my ( $self, $daemon ) = @_;

    $daemon->{db}->save_statistics($self->{pr_name},$self->{smsc_id});

    return;
}

sub has_something_to_do {
    my ( $self, $daemon ) = @_;
    return 0;
}

sub hard_kill {
    my ( $self, $daemon ) = @_;
    return;
}

sub do_work {
    my ( $self, $daemon, $terminating ) = @_;

    return if $terminating;

    my @messages = values %{$daemon->get_sms_ready_to_send(1)};
    return 'idle' unless @messages == 1;

    my $msg = $messages[0];

    $daemon->{log}->info("Fetched OUTSMS$msg->{id}");

    my %params = $self->_mail_message_params($daemon, $msg);

    $daemon->{log}->debug("E-Mail parameters is ".Dumper(\%params));

    my $send_error = $self->_sendmail(%params);

    # XXX envelope from - to developers
    if ( not $send_error ) {
	$daemon->{db}->set_sms_new_status($msg->{id},2,0,0,0);
	$daemon->{db}->set_sms_delivered_status($msg->{id});
	$daemon->{log}->info("Message OUTSMS$msg->{id} to $msg->{abonent} via num $msg->{num} submitted to SMTP relay");
    } else {
	$daemon->{log}->info("Message OUTSMS$msg->{id} to $msg->{abonent} via num $msg->{num} failed with reason '$send_error'");
	$self->_error( "Failed to send e-mal",
		       "Some hren' happens" );
	# Reschedule to future if small amount of tries was given
    }
    return;
}

sub _sendmail {
    my ( $self, %params ) = @_;

    my $rc = open my $sm, "|-", "/usr/lib/sendmail", "-f", $params{envelope_from}, "-t"
      or die "SM failed: $!";

    print $sm <<EOF;
From: $params{from}
To: $params{to}
Subject: $params{subject}
Content-Type: text/plain; charset=koi8-r
Content-Transfer-Encoding: quoted-printable
Mime-Version: 1.0

$params{body}
EOF

    close $sm;

    return 0;
}

sub _mail_message_params {
    my ( $self, $daemon, $msg ) = @_;
    my %params;

    # From generation rules : '%s'
    # Subject
    # content-type, content-transfer-encoding

    my $from = $msg->{transaction_id};
    $params{from} = $from;
    $params{envelope_from} = $from;

    $params{body} = encode_qp(encode('koi8-r', decode('cp1251',$msg->{msg})));

    my $link = $daemon->{db}->get_link_id($msg->{abonent}, $msg->{smsc_id});

    $params{to} = "$msg->{abonent}\@$link";
    $params{subject} = "0:0";

    return %params;
}

sub maintainance {
    my ( $self, $daemon ) = @_;
    $daemon->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
}

1;
