#!/usr/bin/perl -w
# $Id: $

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=cut
package Disp::DaemonManager::Child;
use strict;
use warnings;
use base 'Disp::Object';

use overload
  '==' => \&_comparator,
  '!=' => sub { not _comparator(@_) };

our $VERSION	= 0.1;		# ������ ������.

sub new {
	my $self = shift->SUPER::new(@_);

	$self->{id} ||= 0;
	$self->{active} ||= 0;
	$self->{subdir} ||= "";
	$self->{execute} ||= "";
	$self->{environment} ||= {};
	$self->{restart_timeout} ||= 0;

	return $self;
}

sub id {
	$_[0]{id};
}

sub compare_hash_toplevel {       # ($hr1, $hr2) boolean
	my ( $hr1, $hr2 ) = @_;
	my %k2 = map { $_ => 1 } keys %$hr2;
	while (my ($k,$v)=each %$hr1) {
		return 0 if not exists $hr2->{$k};
		if (not defined $v) {
			return 0 if defined $hr2->{$k};
		} else {
			return 0 if not defined $hr2->{$k};
			return 0 if $v ne $hr2->{$k};
		}
		delete $k2{$k};
	}
	return 0 if keys %k2;
	return 1;
}

my %comparsion_methods = ( number => sub { $_[0] == $_[1] },
			   string => sub { $_[0] eq $_[1] },
			   map_ss => \&compare_hash_toplevel );

my %comparsions = ( active => 'number',
		    subdir => 'string',
		    execute => 'string' );

sub _comparator {
	my ( $i1, $i2 ) = @_;

	# Reset keys
	keys %comparsions;

	while (my ($k,$v) = each %comparsions) {
		my $v1 = $i1->{$k};
		my $v2 = $i2->{$k};
		my $method = ($comparsion_methods{$v} or $comparsion_methods{string});
		my $result = $method->($v1, $v2);
		return 0 unless $result;
	}
	return 1;
}

sub spawn {
	my ( $self ) = @_;
	my $child_pid = fork;
	die "Fork: $!" if $child_pid < 0;
	if ($child_pid) {
		return $child_pid;
	} else {
		$self->open_log;
		if ($self->prepare_exec) {
			$self->exec;
		}
		$self->error("Exec failed");
		exit 1;
	}
}

sub open_log {
	my ( $self ) = @_;
}

sub prepare_exec {
	my ( $self ) = @_;
	die "Couldn't chdir: $!" unless chdir $self->{subdir};
	close STDOUT;
	close STDERR;
	open STDERR,' > /dev/null';
	open STDOUT,' > /dev/null';
	while (my($k, $v) = each %{$self->{environment}}) {
		$ENV{$k} = $v if defined $v;
	}
	return 1;
}

sub exec {
	my ( $self ) = @_;
	exec $self->{execute};
}

sub error {
	my ( $self ) = @_;
}

1;
