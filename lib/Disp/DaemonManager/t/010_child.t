#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 13;
use Test::Exception;
use Data::Dumper;

BEGIN { use_ok('Disp::DaemonManager::Child'); }


# Helper tests



{
	no strict 'refs';
	my $nm = 'compare_hash_toplevel';
	*$nm = \&Disp::DaemonManager::Child::compare_hash_toplevel;
}

ok compare_hash_toplevel
  ( { a => 1, b => 2 },
	{ a => 1, b => 2 } );

ok compare_hash_toplevel( { }, { } );

ok not compare_hash_toplevel( { a => 1, b => 2 }, { a => 1 });
ok not compare_hash_toplevel( { a => 1, b => 2 }, { a => 1, b => 2, c => 3});

# Main tests

my $i1 = new Disp::DaemonManager::Child( );
isa_ok($i1, 'Disp::DaemonManager::Child');

my $i2 = new Disp::DaemonManager::Child( active => 1,
										 subdir => 'test'
									   );

my $i3 = new Disp::DaemonManager::Child( active => 1,
										 subdir => 'test',
										 unimportant_param => 1,
									   );

my $i4 = new Disp::DaemonManager::Child( active => 0,
										 subdir => 'test' );

ok $i2 == $i3;
ok $i2 != $i4;
ok $i3 != $i4;
ok $i3 == $i2;

my $i5 = new Disp::DaemonManager::Child( active => 1,
										 subdir => '../lib/Disp/DaemonManager/t/',
										 execute => 'sh ./010_exec.sh',
										 environment => { DISP_ENV_TEST => 'disp_env_really_test', }, );

my $pid = $i5->spawn;
my $got = waitpid $pid,0;

open A, '< ../lib/Disp/DaemonManager/t/test_out';
my $magic = <A>;
my $filepid = <A>;
my $env = <A>;
chomp $magic;
chomp $filepid;
chomp $env;

is $magic, '12345';
is $filepid, $pid;
is $env, 'disp_env_really_test';
