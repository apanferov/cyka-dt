#!/usr/bin/perl -w
# $Id: $

=head1 NAME

Disp::DaemonManager::TranscieverSource

=head1 METHODS

=cut
package Disp::DaemonManager::TranscieverSource;
use strict;
use Disp::DaemonManager::Child;
use base 'Disp::Object';
use Disp::Utils qw/pt_dumper_1/;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use Net::Domain;

my $log = get_logger();

use DBI;

our $VERSION    = 0.1;          # ������ ������.

sub new {
        my $self = shift->SUPER::new(@_);

		$self->{fqdn} = Net::Domain::hostfqdn();

        return $self;
}

sub _db_connect {                 # () boolean
        my ( $self ) = @_;
        eval {
                $self->{dbh} = DBI->connect("DBI:mysql:$self->{dbname};host=$self->{dbhost}",
                                                                        $self->{dbuser}, $self->{dbpassword},
                                                                        { AutoCommit => 1,
                                                                          RaiseError => 1 });
        };
        $log->debug("Connecting to 'DBI:mysql:$self->{dbname};host=$self->{dbhost}', $self->{dbuser}, $self->{dbpassword}");
        if ($@) {
                $log->error("DB connect failed: $@");
                delete $self->{dbh};
                return 0;
        }
        return 0 unless $self->{dbh};
        return 1;
}

sub child_maker {
        my ( $smpp_mode, $kind, $opt_execute ) = @_;
        return sub {
                my ( $self, $db ) = @_;
                return new Disp::DaemonManager::Child
                  ( environment => { SMPP_MODE => $smpp_mode,
                                                         SMPP_DEBUG_LEVEL => $db->{debug_level},
                                                         SMPP_SMSC_ID => $db->{id} },
                        id => "$db->{id}_$kind",
                        subdir => $self->{subdir},
                        execute => $opt_execute || $self->{execute},
                        active => $db->{is_active},
                        restart_timeout => $db->{rebind_timeout} );
        }
}

my %types = ( transciever			 => child_maker(3, 'transceiver'),
              transceiver			 => child_maker(3, 'transceiver'),
              reciever_transmitter	 => [ child_maker(1, 'transmitter'),
										  child_maker(2, 'receiver') ],
              receiver_transmitter	 => [ child_maker(1, 'transmitter'),
										  child_maker(2, 'receiver') ],
              smsc					 => child_maker(4, 'smpp_server'),
              mms					 => undef,
              mms_v2				 => child_maker(10, 'mms_sender1', 'perl ./mms_sender.pl'),
              http_ivanovo			 => [ child_maker(22, 'receiver') ],
              http_teleclick		 => [ child_maker(22, 'receiver'),
										  child_maker(21, 'transmitter') ],
              http_ukraine			 => [ child_maker(22, 'receiver'),
										  child_maker(21, 'transmitter') ],
              http_pribaltika		 => [ child_maker(22, 'receiver'),
										  child_maker(21, 'transmitter') ],
              smtp					 => [ child_maker(11, 'transmitter', 'perl ./smtp_client.pl'),
										  child_maker(12, 'receiver', 'perl ./smtp_client.pl') ],
              http_kievstar			 => [ child_maker(22, 'receiver'),
										  child_maker(21, 'transmitter') ],
              http_umcs				 => [ child_maker(22, 'receiver'),
										  child_maker(21, 'transmitter') ],
			  http_umcs_old			 => child_maker(21, 'transmitter'),
			  http_aaa				 => [ child_maker(22, 'reciever'),
										  child_maker(21, 'transmitter') ],
			  http_osmp              => child_maker(23, 'receiver'),
			  http_eport             => child_maker(23, 'receiver'),
			  http_cyberplat         => child_maker(23, 'receiver'),
              transmitter			 => child_maker(1, 'transmitter'),
			  http_transmitter       => child_maker(31, 'transmitter'),
			  http_receiver          => child_maker(32, 'receiver'),
			  http_receiver_transmitter => [  child_maker(31, 'transmitter'),
											  child_maker(32, 'receiver') ],
			  http_transceiver          => child_maker(33, 'transceiver'),
			);

sub _make_child {                 # ($db_data) object
        my ( $self, $db_data ) = @_;

        my $maker = $types{$db_data->{client_type}};

        return unless $maker;
        return map { $_->($self, $db_data) } @{_ensure_list($maker)};
}

sub _selectall {                  # () arrayref
        my ( $self ) = @_;

        my $res;
        eval {
                if (not defined($self->{dbh}) and not $self->_db_connect) {
                        die "No valid database handle";
                }
                $res = $self->{dbh}->selectall_arrayref("select * from set_smsc where run_on_host = ? or run_on_host = 'ANYWHERE'",
														{ Slice => { } },
														$self->{fqdn});
        };

        if ($@) {
                $log->error("Select failed: $@");
                # Invalidate dbh after error
                delete $self->{dbh};
                return $self->{cached} || {};
        } else {
                $self->{cached} = $res;
                return $res;
        }
}

sub fetch {                       # () void
        my ( $self ) = @_;
        return pt_dumper_1([ map { $self->_make_child($_) } @{$self->_selectall} ]);
}

sub _ensure_list {
        my ($elt) = @_;
        return $elt if UNIVERSAL::isa($elt, 'ARRAY');
        return [ $elt ];
}

=pod



ALTER TABLE set_smsc_t ADD COLUMN client_type VARCHAR(20) NOT NULL AFTER smpp_mode;
UPDATE set_smsc_t SET client_type = 'transciever' where smpp_mode = 0;
UPDATE set_smsc_t SET client_type = 'reciever_transmitter' where smpp_mode = 1;
UPDATE set_smsc_t SET client_type = 'smsc' where smpp_mode = 2;
UPDATE set_smsc_t SET client_type = 'mms' where smpp_mode = 3;
UPDATE set_smsc_t SET client_type = 'mms_v2' where smpp_mode = 4;
UPDATE set_smsc_t SET client_type = 'http_ivanovo' where smpp_mode = 5;
UPDATE set_smsc_t SET client_type = 'http_teleclick' where smpp_mode = 6;
UPDATE set_smsc_t SET client_type = 'http_ukraine' where smpp_mode = 7;
UPDATE set_smsc_t SET client_type = 'http_pribaltika' where smpp_mode = 8;
UPDATE set_smsc_t SET client_type = 'smtp' where smpp_mode = 9;
UPDATE set_smsc_t SET client_type = 'transmitter' where smpp_mode = 10;
ALTER TABLE set_smsc_t DROP COLUMN smpp_mode;
DROP VIEW IF EXISTS `set_smsc`;
CREATE VIEW `set_smsc` AS select * from `set_smsc_t` `s` where (`s`.`_deleted` <> 1);

ALTER TABLE set_smsc_t ADD COLUMN run_on_host varchar(255) not null after client_type;
DROP VIEW IF EXISTS `set_smsc`;
CREATE VIEW `set_smsc` AS select * from `set_smsc_t` `s` where (`s`.`_deleted` <> 1);


=cut


1;
