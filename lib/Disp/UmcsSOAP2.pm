# -*- coding: cp1251 -*-
package Disp::UmcsSOAP2;
use strict;
use warnings;
use Carp qw/confess/;
use Disp::Utils;
use XML::Simple;
use POE;

use SOAP::Transport::HTTP;
use SOAP::Lite +trace => qw/all/;

use base qw/SOAP::Server::Parameters Disp::Object/;
use Data::Dumper;
use Encode qw(encode decode);


###{
	POE::Session->create(
		inline_states => {

		_start => sub { $_[KERNEL]->alias_set('ender'); $_[KERNEL]->refcount_increment($_[SESSION], 'pending') },
		
		Delay => sub {
			my ($delay,$method) = @_[ARG0, ARG1];
			my @args = @_[ARG2..$#_];
			my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
#			$SOAP_GLOBAL->{log}->info("M:$method D:$delay ".Dumper(\@args));
			POE::Kernel->alarm_set($method, time()+$delay, @args)
				                or confess($!);
		},
		ReserveAsyncEnd =>
sub {
	my($orderId, $goodPhone) = @_[ARG0, ARG1];
	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	my %update;

	my $operator_id = $SOAP_GLOBAL->{operator_id};
	my $price       = $SOAP_GLOBAL->{db}->num_price($SOAP_GLOBAL->{operator_id}, $goodPhone);
	my $userInfo    = decode('UTF-8', "доступ к услуге");

	my $result = 0;
	if (not defined $price) {
		$price = 0;
		$result = 105;
		$update{our_status} = 3;
		$userInfo = decode('UTF-8', "Неверный код товара, уточните его и попробуйте снова!");
	}

	$update{our_price} = $price;

	$SOAP_GLOBAL->{log}->info("ReserveAsyncEnd:$orderId (price=$price, result=$result)");

	cert_conf($goodPhone);
	my $ReserveAsyncEndResult = Disp::UmcsSOAP2::Helper::ReserveAsyncEnd(
		$orderId, $price, $userInfo, $result);

	if ($ReserveAsyncEndResult == 0) {
		$update{our_status} = $result == 105 ? 11 : 4;
	} else {
		$update{our_status} = $result == 105 ? 12 : 5;
	}
	$update{reserve_async_end_result} = $ReserveAsyncEndResult;
	Disp::UmcsSOAP2::Helper::db_update($orderId, \%update);

	$SOAP_GLOBAL->{log}->info("ReserveAsyncEnd:$orderId returned $ReserveAsyncEndResult");
},#sub
			
		PurchaseEnd =>
sub {
	my($orderId, $goodPhone)  = @_[ARG0, ARG1, ARG2, ARG3];
	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	my $db = $SOAP_GLOBAL->{db};

	my $s = $SOAP_GLOBAL->{db}->get_sms_by_msg_id($SOAP_GLOBAL->{smsc_id}, $orderId);
	Disp::UmcsSOAP2::Helper::db_update($orderId, { purchase_date => today() });

#	my $umcs_status = $goodPhone =~ /free/ ? 1 : 0;
#	$umcs_status = 1
#		unless($outsms->{num} == $goodPhone and $outsms->{abonent} == $abonent);
	my $umcs_status = ($s->{id}) ? 0 : 1;

	$SOAP_GLOBAL->{log}->info("PurchaseEnd:$orderId (result=$umcs_status)");

	cert_conf($goodPhone);
	my $result = Disp::UmcsSOAP2::Helper::PurchaseEnd(
		$orderId,
		decode('cp1251', $s->{msg}),
		$umcs_status);
	$SOAP_GLOBAL->{log}->info("PurchaseEnd:$orderId returned $result");

	my $our_status;
	if (defined($result) and ($result == 0)) {
		$our_status = $umcs_status == 0 ? 8 : 9;
	} else {
		$our_status = $umcs_status == 0 ? 7 : 10;
	}
	Disp::UmcsSOAP2::Helper::db_update($orderId, { our_status => $our_status });

	if (defined($result) and ($result == 0)) {
		$db->set_sms_report_status($s->{id},12,undef,$s);
		$db->update_billing_($s->{inbox_id}, $s->{outbox_id});
		$SOAP_GLOBAL->{log}->info("Message SMS$s->{id} was delivered to abonent.");
	} else {
		$db->set_sms_report_status($s->{id},255,undef,$s);
		$SOAP_GLOBAL->{log}->warn("Message SMS$s->{id} wasn't delivered.");
	}

}#sub
	});
###}

=head1 �������

1  - ������ �� �������������� - �������
2  - ������ �� �������������� - �����
3  - ������������� �������������� - �������
4  - ������������� �������������� - �����
5  - ������������� �������������� - ������� � ������ �������
11 - ������������� �������������� - �� ����������, ����� � ������ �������
12 - ������������� �������������� - �� ���������, ������� � ������ �������
6  - ������� ������� � ���������
7  - ������� - �� ������
8  - ������� - ������
9  - ������� - �� ����������
10 - ������� - �� ����������, � ������ - ������
13 - ����� ����� �������� �� �������
14 - ������� � ���������, �� ��������� �� ������� - UMCS ���������
15 - ������� � ���������, �� ��������� �� ������� - UMCS �� ��������� �� ������ � �� �������.
=cut

my $asgpath = '/gate/asg5';

my %certificates = (
	1 => {
		ca_file     => "$asgpath/etc/ca-bee.crt",
		pkcs12_file => "$asgpath/etc/umcs_a1.pfx",
		},
	2 => {
		ca_file     => "$asgpath/etc/ca-bee.crt",
		pkcs12_file => "$asgpath/etc/umcs_a1_alt.pfx",
		},
	);

my %num_to_cert_map = (
	#_default_ => 1,
	#3121 => 1,
	#8353 => 1,
	840501 => 2,
	840502 => 2,
	840503 => 2,
	840504 => 2,
	840511 => 2,
	840512 => 2,
	840513 => 2,
	840514 => 2,
	840521 => 2,
	840522 => 2,
	840531 => 2,
	840532 => 2,
	845021 => 2,
	845022 => 2,
	845023 => 2,
	845024 => 2,
	845031 => 2,
	845032 => 2,
	845033 => 2,
	845034 => 2,
	);


sub cert_conf {
	my($num, $log) = @_;

	my $conf = $certificates{$num_to_cert_map{$num}} || $certificates{$num} || $certificates{$num_to_cert_map{_default_}};

	$ENV{HTTPS_CA_FILE}     = $conf->{ca_file};
	$ENV{HTTPS_PKCS12_FILE} = $conf->{pkcs12_file};

 	$log->debug("Certificates for $num: @{[Dumper($conf)]}")
		if $log;
}

sub certificate_groups {
	keys %certificates;
}

sub certificate_numbers {
	my($certificate_group) = @_;
	grep { $num_to_cert_map{$_} == $certificate_group } keys %num_to_cert_map;
}


sub ReportError {
	return 0;
}

my %sfdr_reason_names = (
	0 => 'Purchase is missing in FEO',
	1 => 'Purchase is missing in UMCS',
	2 => 'Purchase is in error state',
	3 => 'Purchase price different in FEO and UMCS'
	);

my %sfdr_status_names = (
	0 => 'Reserved',
	1 => 'Completed',
	2 => 'Abonent rejection',
	4 => 'UMCS error',
	5 => 'Partner error',
	7 => 'Undefined error',
	8 => 'Transfered to registry',
	9 => 'Unconfirmed'
	);

sub SendFileDailyReport {
	my($dailyReport) =
		SOAP::Server::Parameters::byName([qw[dailyReport]], @_);

	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	my $parsed      = XMLin($dailyReport, ForceArray => ['item'], KeyAttr => []);
	my $feoCode     = $parsed->{code};

	for my $item (@{$parsed->{item}})
	{
		my $order_id   = $item->{order};
		my $umcs_price = $item->{price};
		my $our_price  = $item->{priceFeo};
		my $reason     = $item->{reason};
		my $errorCode  = $item->{errorCode};
		my $status     = $item->{status};

		yell("FEO: $feoCode\nSTATUS: $sfdr_status_names{$item->{status}}\nREASON: $sfdr_reason_names{$item->{reason}}\n" . Dumper($item),
			code    => 'ERROR',
			header  => "RCV_$SOAP_GLOBAL->{smsc_id}: UMCS reported difference",
			process => "rcv_$SOAP_GLOBAL->{smsc_id}");

		my $request_data = $SOAP_GLOBAL->{db}->{db}->selectrow_hashref("select * from tr_umcs_request where order_id = ?",
			undef, $order_id);
		unless ($request_data) {
			yell("Can't load order '$order_id' while processing UMCS FEO",
				 code    => 'ERROR',
				 header  => "RCV_$SOAP_GLOBAL->{smsc_id}: Can't update feo record",
				 process => 'rcv_$SOAP_GLOBAL->{smsc_id}');
			next;
		}
		$umcs_price =~ s/,/./;
		$SOAP_GLOBAL->{db}->do('update tr_umcs_requests set umcs_status = ?, umcs_error = ?, umcs_price = ? where order_id = ?',
			undef, $status, $reason, $umcs_price, $order_id);
	}
}

sub UnReserveGood
{
	my($orderId, $result) =
	SOAP::Server::Parameters::byName([qw[orderID result]], @_);

	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{log}->info("UnReserveGood:$orderId (result=$result)");
	my $s = $SOAP_GLOBAL->{db}->get_sms_by_msg_id($SOAP_GLOBAL->{smsc_id}, $orderId);
	if ($s->{id}) {
		$SOAP_GLOBAL->{db}->set_sms_report_status($s->{id}, 13, undef, $s);
		$SOAP_GLOBAL->{db}->{db}->do("update tr_outsmsa set err_num=? where id=?",undef,$result,$s->{id});
		$SOAP_GLOBAL->{log}->warn("Message OUTSMS$s->{id} was UNRESERVED with result($result).");
	}

	return Disp::UmcsSOAP2::Helper::soap_return([0, 'UnReserveGoodResult', 'int']);
}

sub PurchaseAsync {
	my($orderId, $CTN, $goodPhone, $smsText) =
		SOAP::Server::Parameters::byName([qw[orderID CTN goodPhone smsText]], @_);

	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{log}->info("PurchaseAsync:$orderId (CTN=$CTN, goodPhone=$goodPhone, smsText=$smsText)");

	if (my($outsms_id) = $smsText =~ /^##(\d+)$/) {
		POE::Kernel->post('ender', 'Delay', 5, 'PurchaseEnd', $orderId, $goodPhone)
			or confess($!);
	}
	else {
		 my $sms = {
	                 abonent        => "7$CTN",
	                 smsc_id        => $SOAP_GLOBAL->{smsc_id},
	                 num            => $goodPhone,
	                 data_coding    => 0,
	                 esm_class      => 0,
	                 transaction_id => $orderId,
	        };

		$sms->{msg} = encode('cp1251', $smsText);
		$sms->{id}  = $SOAP_GLOBAL->{db}->insert_sms_into_insms($sms);
		Disp::UmcsSOAP2::Helper::db_update($orderId, { our_status => 6, purchase_date => today() });
		$SOAP_GLOBAL->{log}->debug(Dumper($sms));
	}

	return Disp::UmcsSOAP2::Helper::soap_return([0, 'PurchaseAsyncResult', 'int']);
}

sub ReserveGoodAsync
{
	my($orderId, $CTN, $goodPhone, $smsText) =
		SOAP::Server::Parameters::byName([qw[orderID CTN goodPhone smsText]], @_);

	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{log}->info("ReserveGoodAsync:$orderId (CTN=$CTN, goodPhone=$goodPhone, smsText=$smsText)");

	Disp::UmcsSOAP2::Helper::db_insert_reserve($orderId, $goodPhone, $CTN);
	if (my($outsms_id) = $smsText =~ /^##(\d+)$/) {
		$SOAP_GLOBAL->{db}->do("update tr_outsmsa set msg_id=? where id=?",
			undef, $orderId, $outsms_id);
	}

	POE::Kernel->post('ender', 'Delay' , 10, 'ReserveAsyncEnd', $orderId, $goodPhone)
		or confess($!);

	return Disp::UmcsSOAP2::Helper::soap_return(
		[0,  'ReserveGoodAsyncResult'],
		[90, 'timeOut']);
}

package Disp::UmcsSOAP2::Helper;
use strict;
use Carp qw/confess/;

sub db_load
{
	my($order_id) = @_;
	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{db}->{db}->selectrow_hashref("select * from tr_umcs_requests where order_id = ?",
		undef, $order_id);
}

sub db_insert_reserve
{
	my($order_id, $num, $abonent) = @_;
	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{db}->do("insert into tr_umcs_requests(our_status, order_id, abonent, num, reserve_date) values (2, ?, ?, ?, now())",
		undef, $order_id, $abonent, $num);

}

sub db_update
{
	my($order_id, $update) = @_;
	my $SOAP_GLOBAL = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL;
	$SOAP_GLOBAL->{db}->do('update tr_umcs_requests set '
		. join(', ', map { "$_ = ?" } keys %$update)
		. ' where order_id = ?',
		undef, values %$update, $order_id);
}


sub soap_return
{
	my @ret;
	foreach my $a (@_) {
		my $data;

		if    (@$a == 1) {
			$data = SOAP::Data->new(value => $a->[0]);
		}
		elsif (@$a == 2) {
			$data = SOAP::Data->name($a->[1] => $a->[0]);
			$data->type('');
		}
		elsif (@$a == 3) {
			$data = SOAP::Data->name($a->[1] => $a->[0]);
			$data->type($a->[2]);
		}
		push @ret, $data;
	}
	return @ret;
}

sub log_message
{
	my $ob  = shift;
	my $log = $Channel::HTTP::Func_UMCS::SOAP_GLOBAL->{log};
	return unless ($log);

	if (UNIVERSAL::isa($ob, 'HTTP::Request')) {
		$log->debug('REQ:' . $ob->as_string . "\n");
	}
	elsif (UNIVERSAL::isa($ob, 'HTTP::Response')) {
		$log->debug('RESP:' . $ob->as_string . "\n");
	}
	else {
		$log->debug('UNKNOWN IN:'.Dumper($ob));
	}
}

sub do_call
{
	## ($routine, @args) = @_;
	SOAP::Trace->import('transport', \&log_message);
	my $result;
	eval {
	$result = SOAP::Lite
		->default_ns('http://www.estylesoft.com/umcs/shopinterface/')
		->proxy($Channel::HTTP::Func_UMCS::SOAP_GLOBAL->{url})
		->on_action(sub {return ;})
		->call(shift, soap_return(@_))
		->result;
	};
	if ($@) {
		$Channel::HTTP::Func_UMCS::SOAP_GLOBAL->{log}->error("IN_SOAP: $@");
		confess();
	}
	return $result;
}

sub PurchaseEnd
{
	do_call('PurchaseEnd',
		[shift, 'sessionId', 'long'],
		[shift, 'userInfo',  'string'],
		[shift, 'result',    'int']);
}

sub ReserveAsyncEnd
{
	do_call('ReserveAsyncEnd',
		[shift,                 'sessionId', 'long'],
		[sprintf('%2f', shift), 'price',     'string'],
		[shift,                 'userInfo',  'string'],
		[shift,                 'result',    'int']);
}

sub ProcessPurchaseShortPhoneAsync
{
	do_call('ProcessPurchaseShortPhoneAsync',
		[shift, 'shortPhone', 'string'],
		[shift, 'smsText',    'string'],
		[shift, 'CTN',        'string']);
}

sub TakeFileDailyReport
{
	do_call('TakeFileDailyReport',
		[shift, 'dailyReport', 'string']);
}

1;
__END__
