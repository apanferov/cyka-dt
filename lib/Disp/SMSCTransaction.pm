#!/usr/bin/perl
package Disp::SMSCTransaction;
use strict;
use warnings;
use base 'Disp::Object';

=pod

Статусы:

1 - отменена явно
2 - отменена по сверке
3 - деньги по идее должны быть
4 - не погашена
5 - погашена



=cut

sub load {
	my ( $proto, $smsc_id, $transaction_id ) = @_;

	return $proto->construct(Config->dbh->selectrow_hashref("select * from tr_commerce_transactions where smsc_id = ? and transaction_id = ?", undef,
															$smsc_id, $transaction_id));
}

sub load_by_insms_id {
	my ( $proto, $insms_id ) = @_;
	return $proto->construct(Config->dbh->selectrow_hashref("select * from tr_commerce_transactions where insms_id = ?", undef,
															$insms_id));
}

sub construct {
	my ($proto, $maybe_hash) = @_;

	unless ($maybe_hash) {
		return undef;
	}

	my $self = $proto->SUPER::new($maybe_hash);

	$self->{_orig_params} =  $self->{parameters};
	$self->{parameters} = _decode_parameters($self->{parameters});

	return $self;
}

sub save {
	my ( $self ) = @_;

	my @fields = qw/smsc_id transaction_id exact_price status insms_id date/;

	local $self->{parameters} = _encode_parameters($self->{parameters});
	push @fields, 'parameters' if $self->{parameters} ne $self->{_orig_params};

	my $sql = "replace into tr_commerce_transactions set ".join(",", map { "$_ = ?" } @fields);
	print $sql ."\n";

	Config->dbh->do($sql, undef, @{$self}{@fields});
}

sub _encode_parameters {
	my ( $param_hash ) = @_;

	return join ",", map { _escape_string($_) . "=" . _escape_string($param_hash->{$_}) }
	  sort keys %$param_hash;
}

sub _decode_parameters {
	my ( $string ) = @_;

	my @pairs = split /,/, $string;
	my %result;

	for my $pair (@pairs) {
		my ( $k, $v ) = map { _unescape_string($_) } split /=/, $pair;
		$result{$k} = $v;
	}
	return \%result;
}


my %escape_tab;

BEGIN {
	for (0..255) {
		my $chr = chr;
		if ($chr =~ /[-_\.A-Za-z0-9]/) {
			$escape_tab{$chr} = $chr;
		} else {
			$escape_tab{$chr} = '%'.unpack("H*", $chr);
		}
	}
}

sub _escape_string {
	my ( $string ) = @_;

	join '', map {$escape_tab{$_}} split //, $string;
}

sub _unescape_string {
	my ( $string ) = @_;

	$string =~ s/%([0-9A-F]{2})/chr(hex($1))/gei;
	return $string;
}

1;
__END__

DROP TABLE IF EXISTS tr_commerce_transactions;
CREATE TABLE tr_commerce_transactions
(
 smsc_id INTEGER NOT NULL,
 transaction_id VARCHAR(20) NOT NULL,
 exact_price DOUBLE,
 status tinyint not null default 0,
 insms_id integer unsigned not null,
 parameters MEDIUMTEXT NOT NULL DEFAULT '',
 PRIMARY KEY (smsc_id, transaction_id),
 UNIQUE KEY(insms_id)
);

ALTER TABLE tr_commerce_transactions ADD COLUMN date datetime not null;

