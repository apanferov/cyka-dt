#!/usr/bin/perl
package Disp::Utils;
use strict;
use warnings;
use Data::Dumper;
use POSIX qw/LC_ALL :errno_h/;
use DBI;
use IPC::Open3;
use IO::Poll qw/POLLIN POLLOUT POLLHUP/;
use Fcntl qw/O_NONBLOCK/;
use Iterator_Utils ':all';
use Time::HiRes qw/gettimeofday tv_interval/;
use Net::Domain qw/hostname/;

use base 'Exporter';

our @EXPORT = qw/selectall selectall_hash parse_response pt_dumper_1
		 ordered_insert daemonize join_defined sql_insert_with_id
		 today sendmail yell iterated_db_select
		 ordered_insert_position max open3_simple read_file
		 db_double_do hires_eval raw_sendmail delayed_exec xml_quote
		 dumper_save dumper_load/;
our @EXPORT_OK = qw/with_transaction/;

=head3 ordered_insert ($arrayref, $element, $predicate) void

��������� ������� � ������������� ������, �������� ���� �������.

C<Parameters>:

	B<arrayref>		arrayref		������, � ������� ���������� �������.
	B<element>		anything		����������� �������
	B<predicate>	coderef			������� ���������. ����� ������� ����� �������� � �� �����, ��� ������� ������ ��� ���Σ� ��������� ��������.

=cut                   #
sub ordered_insert     # ($arrayref, $element, $predicate) void
{
	my ( $aref, $elt, $pred ) = @_;
	my $handled = 0;

	for (my $i = 0; $i <= $#$aref; $i++) {
		if ($pred->($elt, $aref->[$i])) {
			splice @$aref, $i, 0, $elt;
			$handled = 1;
			last;
		}
	}
	push @$aref, $elt unless $handled;
}


=head3 pt_dumper_1

Pass-Through dumper for single value, for dumping of temporary values
without introducing of temparary variables. For example

  return pt_dumper_1({ a => 3, b => 3 });

will be same as 

  return { a => 3, b => 3 };

but with printing dump of this structure as a side effect.

=cut 
sub pt_dumper_1 {
  warn Dumper($_[0]);
  return $_[0];
}

sub selectall {
	my ( $table, $where, $limit, $order ) = @_;
	$where = "where $where" if $where;
	$limit = "limit $limit" if $limit;
	$order = "order by $order" if $order;

	$where ||= "";
	$limit ||= "";
	$order ||= "";

	return Config->dbh->selectall_arrayref("select * from $table $where $order $limit",
										  { Slice => { } });
}

sub selectall_hash {
  my ( $table, $key ) = @_;
  return Config->dbh->selectall_hashref("select * from $table order by $key", $key);
}

sub parse_response {
  my ( $data ) = @_;
  my $out = _real_parse($data);

  unless ( $out->{result} ) {
	# There was parsing error
	return $out;
  }

  $out = _validate_http($out);
  return $out unless $out->{result};

  $out = _validate_sections($out);
  return $out;
}

sub _validate_http {
  my ( $out ) = @_;
  if ( exists($out->{http_state}) and $out->{http_state} != 200) {
	$out->{result} = 0;
	$out->{error} .= "HTTP code not 200, but $out->{http_state}\n";
  }
  return $out;
}

sub _validate_sections {
  my ( $out ) = @_;
  return $out; 
}


my %parse_table = (
				   http_state => sub {
					 my ( $line, $out ) = @_;
					 if ($line =~ m/^HTTP\/(1\.[01])\s+(\d+)\s+(\w+)/) {
					   $out->{http_state} = $2;
					   return 'http_headers'
					 }
					 return { result => 0,
							  error => 'Missing HTTP state header' };
				   },
				   section_data_whole => sub {
					   my ( $line, $out ) = @_;
					   $out->{section}{data} = "" unless defined $out->{section}{data};
					   $out->{section}{data} .= "$line\n";
					   return;
				   },
				   http_headers => sub {
					 my ( $line, $out ) = @_;
					 if ($line eq '') {
						 if (exists $out->{http_headers}{'x-alt1-status'}) {
							 foreach my $h (keys %{$out->{http_headers}}) {
								 next unless $h =~ /^X-Alt1-(.*)$/i;
								 $out->{section}{$1} = $out->{http_headers}{$h};
							 }
							 return 'section_data_whole';
						 }
						 return 'section_header';
					 } elsif ($line =~ m/^\s+(.*)$/) {
					   # Header continuation line
					   $out->{http_headers}{$out->{last_header}} .= $1;
					 } elsif ($line =~ m/^([^:\s]+)\s*:\s*(.*)$/) {
						 my $name = lc($1);
						 $out->{last_header} = $name;
						 my $val = $out->{http_headers}{$name} = $2;
						 # XXX: �����, ����� ����� ������ ���������
						 # ��������� ��� HTTP continuation lines
						 if ($1 =~ /X-Alt1-destination/i) {
							 $out->{http_headers}{'X-Alt1-destinations'} ||= [];
							 push @{$out->{http_headers}{'X-Alt1-destinations'}},
							   split /\s*,\s*/, $val;
						 }
					 } else {
					   return { result => 0,
								error => "Parse error with line $line in state http_headers",
							  };
					 }
					 return;
				   },
				   after_header_newlines => sub {
					 my ( $line, $out ) = @_;
					 if ($line ne '') {
					   $out->{section} = {};
					   return ('section_header', 1);
					 }
					 return;
				   },
				   section_header => sub {
					 my ( $line, $out ) = @_;
					 if ( $line =~ m/^([^:\s]+)\s*:\s*(.*)$/) {
					   my ( $name, $value ) = ( lc($1), $2 );
					   if ( $name eq 'destination' ) {
						 $out->{section}{destinations} ||= [];
						 push @{$out->{section}{destinations}},
						   split /\s*,\s*/, $value;
					   } else {
						 $out->{section}{$name} = $value
					   }
					 } elsif ($line eq '') {
					   return 'after_section_empty_line';
					 } else {
					   return { result => 0,
								error => "Parse error with line $line: invalid line in section_header",
							  };
					 }
					 return;
				   },
				   after_section_empty_line => sub {
					 my ( $line, $out ) = @_;
					 unless ($line eq '') {
					   return ('section_data', 1);
					 }
					 return;
				   },
				   section_data => sub {
					 my ( $line, $out ) = @_;
					 if ($line eq '') {
					   return 'posible_section_delimiter';
					 }
					 $out->{section}{data} = "" unless defined $out->{section}{data};
					 $out->{section}{data} .= "$line\n";
					 return;
				   },
				   posible_section_delimiter => sub {
					 my ( $line, $out ) = @_;
					 if ($line eq '') {
					   $out->{sections} ||= [];
					   push @{$out->{sections}}, $out->{section};
					   $out->{section} = {};
					   return 'after_header_newlines';
					 }
					 $out->{section}{data} .= "\n";
					 return ('section_data',1);
				   },
				  );

sub _real_parse {
  my ( $data ) = @_;
  my @lines = split /\r\n|\n|\r/, $data;

  my $out = {};
  my $state = ($lines[0] =~ /^HTTP\/1\./) ? 'http_state' : 'section_header';

  # print Data::Dumper::Dumper(\@lines);

  foreach my $line (@lines) {
	$line =~ s/\s*$//;

	#	warn "'$line' in '$state'";
	my ( $new_state, $redo ) = $parse_table{$state}->($line, $out);
	if (ref($new_state)) {
	  # Error, return as is
	  return $new_state;
	}
	if ($new_state) {
	  # warn "State change: $new_state";
	  $state = $new_state;
	}
	if ($redo) {
	  # warn "Redo";
	  redo;
	}
  }

  if ($out->{section}{data}) {
	$out->{sections} ||= [];
	push @{$out->{sections}}, $out->{section};
  }


  # Cleanup
  delete $out->{last_header};
  delete $out->{section};

  chomp $_->{data} for @{$out->{sections}};

  if (@{$out->{sections}}) {
	  $out->{result} = 1;
  } else {
	  $out->{result} = 0;
	  $out->{error} = 'Parser not found any sections in service response';
  }
  return $out;
}

sub _fork {                       # () void
	my ( $pid );
  FORK: {
		if (defined ($pid=fork)) {
			return $pid;
		} elsif ($! =~ /No more process/) {
            sleep 5;
            redo FORK;
        } else {
            die "Can't fork: $!";
        }
	}
}

sub daemonize {                   # () void
	my ($pid, $sess_id);
	if ($pid = _fork) {
		exit 0;
	}
	die "Cannot detach from controlling terminal" 
	  unless $sess_id = POSIX::setsid();
	$SIG{HUP} = 'IGNORE';

	if ($pid = _fork) {
		exit 0;
	}

	close(STDIN);
	close(STDERR);
	close(STDOUT);
    open(STDIN,  "+>/dev/null");
    open(STDOUT, "+>&STDIN");
    open(STDERR, "+>&STDIN");
}

sub join_defined {                # ($delimiter, @list) string
	my $delim = shift;
	return join($delim, grep { defined($_) } @_);
}


=head3 sql_insert_with_id ($table, $fields, $values) integer

��������� ������� ������ � ��������� �������, ����� ���� ����������
�������� last_insert_id().

C<Parameters>:
	B<table>		string			SQL table name
	B<fields>		arrayref		field names
	B<values>		arrayref		values for fields

=cut                              #
sub sql_insert_with_id {          # ($table, $fields, $values) integer
	my ( $table, $fields, $values ) = @_;
	Config->dbh->do("insert into $table (".join(', ', @$fields).") values(".join(', ', ('?')x@$fields).")",
					undef, @$values);
	my ($id) = Config->dbh->selectrow_array("select last_insert_id()");
	return $id;
}

=head3 today () string

���������� ������� ���� � ����� ��� ������ � ������� 'YYYY-mm-dd
HH:MM:SS'

=cut                              #
sub today {                       # () string
	return POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
}

=head3 _yell_contact_group($group_name) string

���������� ������ �-������, �������������� ��������� ����������
������. �������� ������ � ������� asg, ��������� ����

 yell_email_E<lt>��� ������E<gt>

���� ������� ������� �� �������, �� ������������ ��������
C<yell_email>.

=cut
sub _yell_contact_group {         # ($group_name) string
	my ( $group_name ) = @_;

        $group_name ||= "";

	my $email = Config->safe_param("yell_email_$group_name", undef);
#-vlad	my $mandatory_email = Config->safe_param("yell_email", "asg");
	$email ||= Config->safe_param("yell_email", "asg"); #+vlad

#-vlad	return join ', ', grep { $_ } $email, $mandatory_email;
	return $email; #+vlad
}


=head3 yell ($message) void

��������� ��������� �� ������ ���� ��������� ����������. � ������ ������ ���

=over

=item *

�������� ��������� �� �������� �����

=item *

������ � ��� ����� Log4perl

=item *

������ � ������� ���������� ����

=item *

������ � ���� ������

=back

C<Parameters>:
	B<message>		string			���������

=cut                              #
sub yell {                        # ($message, %params) void
	my ( $message, %params ) = @_;

	$params{code} ||= 'DEFAULT';
	$params{process} ||= '<none>';
	$params{header} ||= '';
	$params{ignore} ||= {};

	my $try_method = sub {
		my ( $sub, $name ) = @_;
		return if $params{ignore}{$name};
		eval {
			local $SIG{ALRM} = sub { die "alarm: method '$name' timeout" };
			alarm 3;
			$sub->();
			alarm 0;
		};
		if ($@) {
			$message .= "\nError while yelling by method '$name':\n$@\n\n";
		}
	};

	my $safe_get = sub {
		my ($param, $default) = @_;
		my $ret;
		eval {
			$ret = Config->param($param);
		};
		if ($@) {
			$ret = $default;
		}
		return $ret;
	};

	my $dbh = sub {
		my ( $database, $host, $user, $passwd ) = map { Config->param($_ => 'database') } qw/name host username password/;
		my $hndl = DBI->connect("DBI:mysql:$database;host=$host",
								$user, $passwd,
								{ AutoCommit => 1,
								  RaiseError => 1 });
		$hndl->do("SET NAMES 'cp1251'");
		$hndl->do("SET character_set_client=cp1251");
		return $hndl;
	};

	# Email
#	$try_method->(sub { sendmail(to => _yell_contact_group($params{contact_group} || ""),
#								 ($params{cc} ? (cc => $params{cc}) : () ),
#								 from => $safe_get->(yell_from => 'root'),
#								 subject => join(':', $safe_get->(yell_subject => 'ASG ERROR'),
#												 ($params{header} ? ($params{header}) : ())),
#								 body => "PROCESS: $params{process}\nCODE: $params{code}\nMESSAGE:".$message);
#					}, 'e-mail');

	# Database
	$try_method->(sub {
					  my $h = $dbh->();
					  eval {
						  $h->do(Config->param('yell_sql_query'), undef, $message, $params{process}, $params{code});
					  };
					  my $err = $@;
					  $h->disconnect;
					  die $err if $err;
				  }, 'database');

	# Log
	$try_method->(sub {
					  my $log = Log::Log4perl::get_logger($safe_get->(yell_category => 'critical'));
					  $log->error("PROCESS: $params{process}, CODE: $params{code}, MESSAGE:".$message);
				  }, 'log4perl');

	# Email-To-File
	POSIX::setlocale(LC_ALL, 'C');
	my $date = POSIX::strftime('%a, %d %b %Y %H:%M:%S', gmtime);
	POSIX::setlocale(LC_ALL, $ENV{LANG} || "");
	my $home = $ENV{HOME} || ".";

	$try_method->(sub {
			  open FILE, ">> ".$safe_get->(yell_file => "$home/yell.log");
			  print FILE sprintf("Date: %s\nTo: %s%s\nFrom: %s\nContent-Type: %s\nSubject: %s: %s\n\nPROCESS: %s\nCODE: %s\nMESSAGE: %s\n\n========================================\n",
			  	$date,
			  	_yell_contact_group($params{contact_group}) || '',
				$params{cc} ? ", $params{cc}" : '',
				$safe_get->(yell_from => 'root'),
				$params{content_type} || 'text/plain; charset=windows-1251',
				hostname(),
				join(':', $safe_get->(yell_subject => 'ASG ERROR'), $params{header} ? $params{header} : ''),
				$params{process},
				$params{code},
				$message
				);
			  close FILE;
		      }, 'e-mail');

	# File
	$try_method->(sub {
			  open my $fh, ">> ", $safe_get->(err_file => "$home/error.log");
			  print $fh "========================================\n".today()."\nPROCESS: $params{process}, CODE: $params{code}, HEADER: $params{header}, MESSAGE: $message\n";
		      }, 'file');
}

sub sendmail {
	my (%p) = @_;

	my $ct = $p{content_type} || 'text/plain; charset=windows-1251';

	my $to = $p{to};
	$to .= ", $p{cc}" if $p{cc};

	open(SENDMAIL, "|/usr/lib/sendmail -oi -t") or die "Cant fork for sendmail: $!\n";
	print SENDMAIL <<"EOF";
From: $p{from}
To: $to
Subject: $p{subject}
Content-Type: $ct

$p{body}
EOF

	close(SENDMAIL) or warn "sendmail didnt close nicely";
}

# ������ ������ ��������� sendmail'�
sub raw_sendmail {
	my ( $content ) = @_;

	open(SENDMAIL, "|/usr/lib/sendmail -oi -t") or die "Cant fork for sendmail: $!\n";
	print SENDMAIL $content;
	close(SENDMAIL) or warn "sendmail didnt close nicely";
}

=head3 iterated_db_select ($query, $fetch_method, @bind_values) iterator

������� �������� ��� �������� ����������� SQL-�������.

=cut                              #
sub iterated_db_select {          # ($dbh, $query, $fetch_method, @bind_values) iterator
	my $query = shift;
	my $dbh = Config->dbh;

	my $method = 'fetchrow_hashref';

	if (@_ and ref($_[0])) {
		$method = @{shift @_}[0];
		# warn "Method selector: $method";
	}

	my $st = $dbh->prepare($query);
	$st->execute(@_);

	return Iterator { $st->$method(); };
}

sub ordered_insert_position {
	my ( $array, $predicate ) = @_;

	return 0 unless @$array;
	for (my $i = 0; $i < $#$array; $i++) {
		local $_ = $array->[$i];
		if ($predicate->()) {
			return $i;
		}
	}
	return $#$array;
}

sub max {
	return unless @_;
	my $res = shift;
	while (@_) {
		my $tmp = shift;
		$res = $tmp if $res < $tmp;
	}
	return $res;
}

sub open3_simple {
    my ( $cmd_and_args, $stdin ) = @_;
    my ( $stderr, $stdout ) = ( "", "" );
    $stdin ||= "";

    my @cmd = (ref $cmd_and_args eq 'ARRAY') ? @$cmd_and_args : $cmd_and_args;

    my $pid = open3(\*A, \*B, \*C, @cmd);
    unless ($pid > 0) {
	return (0, '', 'COULDN\'T RUN: '.join(" ", @cmd));
    }

    my ( $wtr, $rdr, $err ) = ( \*A, \*B, \*C );

    my $poll = new IO::Poll;

    if (length $stdin) {
	$poll->mask($wtr => POLLOUT | POLLHUP);
    } else {
	$wtr->close;
    }

    $poll->mask($rdr => POLLIN | POLLHUP);
    $poll->mask($err => POLLIN | POLLHUP);

    while ($poll->handles) {
	$poll->poll;

	foreach my $out_h ($poll->handles(POLLOUT)) {
	    if ($out_h == $wtr) {
		my $rv = syswrite $wtr, $stdin, 4096;
		if ($rv > 0) {
		    $stdin = substr $stdin, $rv;
		}
		unless (length $stdin) {
		    $poll->mask($out_h, 0);
		    $out_h->close;
		}
	    }
	}

	foreach my $in_h ($poll->handles(POLLIN)) {
	    my $tmp = "";
	    my $rv = sysread $in_h, $tmp, 4096;

	    die "$! in poll" unless defined $rv;

	    if ($in_h == $rdr) {
		$stdout .= $tmp;
	    } elsif ($in_h == $err) {
		$stderr .= $tmp;
	    }

	    if ($rv == 0) {
		$poll->mask($in_h, 0);
		close($in_h);
	    }
	}

	foreach my $hup_h ($poll->handles(POLLHUP)) {
	    $poll->mask($hup_h, 0);
	    close($hup_h);
	}
    }

    my $ret = waitpid($pid, 0);
    return ($? >> 8, $stdout, $stderr);
}

sub read_file {
	my ( $name ) = @_;
	local $/;
	open my $file, "<", $name;
	my $ret = <$file>;
	return $ret;
}

sub hires_eval (&$$) {
    my ( $code, $name, $logger ) = @_;

    my $tv_start = [gettimeofday];

    $code->();

    $logger->info("Ellapsed ".tv_interval($tv_start)." in $name");
}

=head3 db_double_do ($code_ref) void

�������� ��� ������ ������ MySQL. � ������ ������������� �������
��������� �������� ��� �� ��������� ���.

=cut
sub db_double_do (&$) {
    my ( $code, $log ) = @_;

    eval {
	$code->();
    };

    if ($@) {
	if ($@ =~ /Explicit or implicit commit is not allowed in stored function or trigger/) {
	    $log->error("Error in db_double_do: $@") if $log;
	    $code->();
	} else {
	    die $@;
	}
    }
}

sub delayed_exec (&$) {
    my ( $s, $timeout ) = @_;
    my $pid = fork;
    if ($pid > 0) {
        return 0, $pid;
    } elsif ($pid < 0) {
        return 1;
    } else {
        close *STDOUT;
		sleep $timeout;

		eval {
			$s->();
		};

		if ($@) {
				warn $@;
			}
		exit;
    }
}

# string must be in internal unicode
sub xml_quote {
	my ( $string ) = @_;

	$string =~ s/"/&quot;/g;
	$string =~ s/&/&amp;/g;
	$string =~ s/</&lt;/g;
	$string =~ s/>/&gt;/g;

	return $string;
}

sub dumper_save {
	local $Data::Dumper::Indent = 0;
	local $Data::Dumper::Purity = 1;
	return Dumper($_[0]);
}

sub dumper_load {
	return unless defined $_[0];
	my $VAR1;
	eval $_[0];
	return if $@;
	return $VAR1;
}

sub with_transaction(&$) {
    my ( $sub, $dbh ) = @_;
    $dbh->begin_work;
    my $result = eval {
        my $result = $sub->($dbh);
        $dbh->commit;
        return $result;
    };
    if ($@) {
        my $err = $@;
        eval { $dbh->rollback; };
        die $err;
    }
    return $result;
}

1;

