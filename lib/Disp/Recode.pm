#!/usr/bin/perl -w
# -*- coding: windows-1251-unix -*-
package Disp::Recode;

use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Data::Dumper;
use GSM0338;
use Disp::BMP2Nokia qw (OTAPictureMessage OTAOperatorlogo);
use Disp::BMP2EMS qw (EMSPictureMessage);
use Disp::RTTTL2Nokia qw (OTARingingTone);
use Disp::RecodeUtils qw (decodeBMP pack_long cut_parts pack_7bit pack_7bit_old ringtonetools hex2byte gsm_to_cp1251 cp1251_to_gsm decode_cp1251 gsm_compatible);
use Encode qw(encode decode find_encoding);
use Disp::Utils;
our @EXPORT_OK = qw[encode_sms decode_sms];
our $VERSION	= '0.1';

my $gsm0338_obj = find_encoding('GSM0338');
my $cp1251_obj = find_encoding('cp1251');
my $ucs2_obj = find_encoding('UCS-2');

sub decode_sms {
	my ($sms) = @_;

	# Very ugly hack
	$sms->{gsm_alphabet} = 1
		if (
			(($sms->{operator_id} == 300) # Vivacell
			or ($sms->{operator_id} == 177) # MTS Ural
			or ($sms->{operator_id} == 179) # MTS Siberia
			) and ($sms->{smsc_id}!=238) #A1 Systems Magic
		);
	#

	# ������������� SAR-����������
	if (!defined($sms->{sar_msg_ref_num})) {
		$sms->{sar_msg_ref_num} = 's'.$sms->{id};
		$sms->{sar_total_segments} = 1;
		$sms->{sar_segment_seqnum} = 1;
	}

	# ���������� �������, ��� ��� SMS � ��������� SMS
	$sms->{msg_type} = 'sms';
	$sms->{transport_type} = 'sms';
	# � ����� ��� USSD?
	if (($sms->{ussd_service_op}||0)==1) {
		$sms->{msg_type} = 'ussd_pssr_ind';
		$sms->{transport_type} = 'ussd';
	}
	elsif (($sms->{ussd_service_op}||0)==18) {
		$sms->{msg_type} = 'ussd_ussr_conf';
		$sms->{transport_type} = 'ussd';
	}
	$sms->{transport_type} = 'ussd' if ($sms->{ussd_type} and ($sms->{ussd_type} > 0));

	if ( $sms->{client_type} and
	    ($sms->{client_type} =~ /^http_(umcs|umcs_old|eport|osmp|cyberplat)$/)) {
		$sms->{transport_type} = 'ecomm';
	}

	my $msg = $sms->{msg};

	# ��������� UDH � MSG ���� ���������

	my $simperia_udh_hack = $msg =~ /^(?:\x07|\x05)(?:\x71|\x51)\x03/; #+binarin

	my @udh;
	my $udh_len = 0; # Including UDH Length byte

#	warn unpack("H*", $msg);
#	warn "HACK: $simperia_udh_hack";

	if ($simperia_udh_hack or $sms->{esm_class} & 0x40) { #+binarin: simperia_udh_hack
		$udh_len = ord(substr($msg,0,1)) + 1;
		@udh = split(//,substr($msg,0,$udh_len));
		$msg = substr($msg,$udh_len,length($msg));
	}

	if ((($sms->{data_coding} == 0) and ($sms->{use7bit} == 1)) or
		(($sms->{data_coding} == 0) and ($sms->{use7bit} == 2) and $udh_len) or
		(($sms->{data_coding} == 0) and ($sms->{use7bit} == 4) and $udh_len) or
		(($sms->{data_coding} == 0) and ($sms->{use7bit} == 5)) 
#		or (($sms->{data_coding} == 0xF2) and ($sms->{smsc_id} !=7) and ($sms->{smsc_id} !=6) and ($sms->{smsc_id} !=128) and ($sms->{smsc_id} !=25) and ($sms->{smsc_id} !=176)) # 0xF2 - SIM Specific message with GSM Default Alphabet
	   ) {
		$msg = "\x00"x($udh_len).$msg;
		$msg = Net::SMPP::unpack_7bit($msg);
		$msg = substr($msg,$udh_len,length($msg));
		$msg =~ s/^\x00+//; # �������� ������� 00
		my $msg_orig = $msg;
		$msg = gsm_to_cp1251($msg); # 7bit ���������� ������ ���� ���������� �� GSM � ASCII
		$sms->{msg_type} .= '+badcoding'
		  unless ($msg_orig eq cp1251_to_gsm($msg));
	}
	elsif (($sms->{data_coding} & 12) == 8) {
		my $msg_orig = $msg;
		$msg = encode('cp1251',decode('UCS-2',$msg));
		$sms->{msg_type} .= '+badcoding'
		  unless ($msg_orig eq encode('UCS-2',decode('cp1251',$msg)));
	}
	elsif (($sms->{data_coding} & 12) == 4) {
		$sms->{msg_type} = '8bit_data'
		  if $sms->{msg_type} eq 'sms'; #+binarin: fix for 'ulyanovsk ussd';
	}
	elsif (($sms->{data_coding} == 0) and $sms->{gsm_alphabet}){
		my $msg_orig = $msg;
		$msg = gsm_to_cp1251($msg);
		$sms->{msg_type} .= '+badcoding'
		  unless ($msg_orig eq cp1251_to_gsm($msg));
	}
        elsif ((($sms->{data_coding} & 0xCC) == 0) and !$sms->{gsm_alphabet}){
		$sms->{msg} =~ s/\x1b\x0a/\x0a/;
		$sms->{msg} =~ s/\x1b\x14/\x5e/;
		$sms->{msg} =~ s/\x1b\x28/\x7b/;
		$sms->{msg} =~ s/\x1b\x29/\x7d/;
		$sms->{msg} =~ s/\x1b\x2f/\x5c/;
		$sms->{msg} =~ s/\x1b\x3c/\x5b/;
		$sms->{msg} =~ s/\x1b\x3d/\x7e/;
		$sms->{msg} =~ s/\x1b\x3e/\x5d/;
		$sms->{msg} =~ s/\x1b\x40/\x7c/;
		$sms->{msg} =~ s/\x1b\x65/\x88/;
        }

	if ($sms->{data_coding}  == 247) {
		$sms->{msg_type} = 'te_specific';
        }
	if ($sms->{data_coding}  == 246) {
		$sms->{msg_type} = 'usim_specific';
	}


#	warn Dumper(\@udh);

	if (@udh == $udh_len) {
  		shift @udh if (@udh); # UDH length byte
		while (@udh) {
#			if ((ord($udh[0]) == 0x00)and(ord($udh[1]) == 0x03)) { #-binarin
			if (($udh[0] =~ /^(?:\x00|\x71|\x51)$/)and(ord($udh[1]) == 0x03)) { #+binarin
				if ((ord($udh[3]) > 0)and(ord($udh[4]) <= ord($udh[3]))) {
					($sms->{sar_msg_ref_num},$sms->{sar_total_segments},$sms->{sar_segment_seqnum}) =
					  (ord($udh[2]),ord($udh[3]),ord($udh[4]));
				}
			}
			elsif ((ord($udh[0]) == 0x08)and(ord($udh[1]) == 0x04)) {
				if ((ord($udh[4]) > 0)and(ord($udh[5]) <= ord($udh[4]))) {
					($sms->{sar_msg_ref_num},$sms->{sar_total_segments},$sms->{sar_segment_seqnum}) =
					  (unpack('S',$udh[2].$udh[3]),ord($udh[4]),ord($udh[5]));
				}
			}

			#+binarin: elsif -> if
			if (ord($udh[0]) == 0x70) {
				$sms->{msg_type} = 'usim_command_packet';
			}

			#+binarin: elsif -> if
			if ($udh[0] =~ /^(?:\x71|\x51)$/) { #+binarin: 51
				$sms->{msg_type} = 'usim_response_packet';
			}
			@udh = @udh[ord($udh[1])+2..$#udh];
		}
	}
	else {
		$sms->{msg_type} = 'sms+bad';
	}

	my $smsc_list={map { $_ => 1 } (5,25,18,260,49,281,55,238)};
	if ($smsc_list->{$sms->{smsc_id}} and
	    ($sms->{sar_total_segments}==1) and ($sms->{sar_segment_seqnum}==1) and (length($msg)>70)) {
		my ($coding,$cut_count,$max_count) = (gsm_compatible($msg)) ? ('GSM0338',153,160) : ('UCS-2',134,140);
		my $msg_bytes = encode($coding,decode_cp1251($msg));
		my $len = length($msg_bytes);
		if ($len > $max_count) {
			use POSIX;
			$sms->{supposed_sms_count} = ceil($len / $cut_count);
			use Log::Log4perl qw(get_logger);
	                my $log=get_logger("");
	                $log->warn("SUPPOSED ID:$sms->{id} count $len/$cut_count=$sms->{supposed_sms_count} ($coding,$len > $max_count)");
		}
	}
	
	$sms->{msg_type} = 'fraud' if ($sms->{transaction_id} =~ /^#fraud#/i);
	$sms->{msg_original} = $sms->{msg};
	$sms->{msg} = $msg;
	return 1;
}

## HACK MTS
my %cur_mts_smsc_id;
##
sub encode_sms {
	my ($msg) = @_;

	## HACK MTS support for MTS link pool 501-505;
	my $op = $msg->{operator_id};
	if ((501 <= $msg->{smsc_id}) and ($msg->{smsc_id} <= 505)) {
		$cur_mts_smsc_id{$op} = ($cur_mts_smsc_id{$op} and ($cur_mts_smsc_id{$op} < 505)) ? 
					$cur_mts_smsc_id{$op}+1 : 
					501;
		$msg->{smsc_id} = $cur_mts_smsc_id{$op};
	}
	my $content_type = $msg->{content_type};
	my $retval = 0; 
	if ($content_type eq 'wap-push') {
		$retval = encode_wappush($msg);
	}
  	elsif ($content_type eq 'flash-msg') {
		$retval = encode_flash($msg);
	}
  	elsif ($content_type eq 'app-port-long') {
		$retval = encode_app_port($msg);
	}
  	elsif ($content_type eq 'ussd-ussr_request') {
		$retval = encode_ussd($msg);
	}
  	elsif ($content_type eq 'ussd-pssr_response') {
		$retval = encode_ussd($msg);
	}
  	elsif ($content_type eq 'ussd-upper_menu') {
		$retval = encode_ussd($msg);
	}
  	elsif ($content_type eq 'ussd-redirect') {
		$retval = encode_ussd($msg);
	}
  	elsif ($content_type eq 'ota') {
		$retval = encode_ota($msg);
	}
  	elsif ($content_type eq 'oma') {
		$retval = encode_oma($msg);
	}
  	elsif ($content_type eq 'oma2') {
		$retval = encode_oma2($msg);
	}
  	elsif ($content_type eq 'image/bmp') {
		$retval = encode_bmp($msg);
	}
  	elsif ($content_type eq 'audio/x-rtttl') {
		$retval = encode_rtttl($msg);
	}
  	elsif ($content_type eq 'audio/x-midi') {
		$retval = encode_midi($msg);
	}
  	elsif ($content_type eq 'ems') {
		$retval = encode_ems($msg);
	}
  	elsif ($content_type eq 'forward_as_is') {
		$retval = encode_forward($msg);
	}
	elsif ($content_type eq 'hidden') {
		$retval = encode_hidden_text($msg);
	}
	else {
		$retval = encode_text($msg); 
		$content_type = 'text/plain';
	}
	my $retmsg = ($msg->{target_type})?"$content_type -> $msg->{target_type}":$content_type;
	return $retval,$retmsg;
}

###### FORWARD ######
sub encode_forward {
	my $msg = shift;
	($msg->{data_coding},$msg->{esm_class}) = (0x00,0x00);
	my $db = Disp::AltDB::get_db();
	my $insms_ids = $db->get_value('select transport_ids from tr_inboxa where id=?',undef,$msg->{inbox_id});
	my $rows = $db->{db}->selectall_arrayref("select id,pdu from tr_insmsa where id in ($insms_ids) order by id") if ($insms_ids);
	my @pdus;
	foreach(@$rows) {
		my %p = (id => $_->[0], pdu => $_->[1]);
		push(@pdus,\%p) ;
	}
	$msg->{pdus} = \@pdus;
	return 1;
}

sub encode_forward_old {
	my $msg = shift;
	($msg->{data_coding},$msg->{esm_class}) = (0x00,0x00);
	my $db = Disp::AltDB::get_db();
	my $insms_ids = $db->get_value('select transport_ids from tr_inboxa where id=?',undef,$msg->{inbox_id});
	my $rows = $db->fetchall_arrayref("select pdu from tr_insmsa where id in ($insms_ids) order by id") if ($insms_ids);
	my @pdus;
	push(@pdus,@$_) for(@$rows);
	$msg->{pdus} = \@pdus;
	return 1;
}

###### USSD ######
sub encode_ussd {
	my $msg = shift;
	my ($data_coding,$max_count) = ($msg->{msg} =~ m/[\x80-\xff]/)?(8,70):(0,160);
	truncate_msg($msg,$max_count);
	my $text = $msg->{msg};
	$text =~ s/\|/"\x0A"/eg;

	if ($data_coding==8) {
		$text = encode('UCS-2',decode('cp1251',$text)) unless ($msg->{use1251});
	}
	else {
		$text = pack_7bit_old($text) if ($msg->{use7bit}==1);
	}
  	my @parts = ($text);
	($msg->{data_coding},$msg->{esm_class},$msg->{ussd_service_op},$msg->{parts}) =
      ($data_coding,0,2,\@parts);
  	if ($msg->{target_type} =~ /^(cat\d\d?\_\w*)$/) {
		$msg->{tariff} = uc($1);
	}
  	if ($msg->{content_type} eq 'ussd-pssr_response') {
  		$msg->{ussd_service_op} = 17;
	}
	elsif ($msg->{content_type} eq 'ussd-ussr_request') {
		$msg->{ussd_service_op} = 2;
	}
	elsif ($msg->{content_type} eq 'ussd-upper_menu') {
		$msg->{ussd_service_op} = 32;
	}
	elsif ($msg->{content_type} eq 'ussd-redirect') {
		$msg->{ussd_service_op} = 33;
	}
	else {
		return 0;
	}
	return 1;
}

sub encode_ussd2 {
	my $msg = shift;
	truncate_msg($msg,160);
	my $text = $msg->{msg};
	$text =~ s/\|/"\x0A"/eg;
  	$text = pack_7bit_old($text) if ($msg->{use7bit});
  	my @parts = ($text);
	($msg->{data_coding},$msg->{esm_class},$msg->{ussd_service_op},$msg->{parts}) = 
      (0,0,2,\@parts);
  	if ($msg->{content_type} eq 'ussd-pssr_response') {
  		$msg->{ussd_service_op} = 17;
	}
	elsif ($msg->{content_type} eq 'ussd-ussr_request') {
		$msg->{ussd_service_op} = 2;
	}
	else {
		return 0;
	}
	return 1;
}

sub encode_oma2 {
	my $msg = shift;
	my @parts;
	($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
	truncate_msg($msg,2000);
	@parts = pack('H*',$msg->{msg});
	cut_parts(\@parts,133,128);
	my $additional_udh = "\x05\x04\x0b\x84\x00\x00";
	Disp::RecodeUtils::pack_long2(\@parts,$additional_udh);
	$msg->{parts} =  \@parts;
	return 1;
}


###### OMA ###### ��� ����������
sub encode_oma {
	my $msg = shift;
	my @parts;
	($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
	truncate_msg($msg,2000);
	@parts = pack('H*',$msg->{msg});
	cut_parts(\@parts,133,128);
	my $additional_udh = "\x05\x04\x0b\x84\x04\x0f";
	pack_long(\@parts,$additional_udh);
	$msg->{parts} =  \@parts;
	return 1;
}

###### OTA ###### ��� ����������
sub encode_ota {
	my $msg = shift;
	my @parts;
	($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
	truncate_msg($msg,2000);
	@parts = pack('H*',$msg->{msg});
	cut_parts(\@parts,133,128);
	my $additional_udh = "\x05\x04\xc3\x4f\xc0\x02";
	pack_long(\@parts,$additional_udh);
	$msg->{parts} =  \@parts;
	return 1;
}

###### BMP ######
sub encode_bmp {
	my $msg = shift;
	truncate_msg($msg,4000);
	my @parts;
  	if ($msg->{target_type} eq 'image/vnd.nokia.logo') {
		return 0;
	}
	elsif ($msg->{target_type} eq 'image/vnd.nok-oplogo') {
		($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
		@parts = OTAOperatorlogo(250,1,$msg->{msg});
		return 0 if $parts[0] eq -1;
		cut_parts(\@parts,133,128);
		my $additional_udh = "\x05\x04\x15\x8A\x00\x00";
		pack_long(\@parts,$additional_udh);
		$msg->{parts} =  \@parts;
	}
#done 
	elsif ($msg->{target_type} eq 'image/vnd.nokia.picture') {
		($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
		@parts = OTAPictureMessage('',$msg->{msg});
		return 0 if $parts[0] eq -1;
		cut_parts(\@parts,133,128);
		my $additional_udh = "\x05\x04\x15\x8A\x00\x00";
		pack_long(\@parts,$additional_udh);
		$msg->{parts} =  \@parts;
	}
#done 
	elsif ($msg->{target_type} eq 'application/vnd.ems.v3') {
		($msg->{data_coding},$msg->{esm_class}) = (0xF5,0x40);
#		my $additional_udh = EMSPictureMessage($msg->{msg});
#		pack_long(\@parts,$additional_udh);
#   	$msg->{parts} =  \@parts;
		$msg->{parts} = EMSPictureMessage($msg->{msg});
	}
#done 
	elsif ($msg->{target_type} eq 'image/vnd.siemens.picture') {
		($msg->{data_coding},$msg->{esm_class}) = (0xF5,0x00);
		@parts = ringtonetools('bmp','seo',pack('H*',$msg->{msg}),$msg->{id});
		$msg->{parts} =  \@parts;
	}
	else {
		return 0;
	}
	return 1;
}

###### RTTTL ######
sub encode_rtttl {
	my $msg = shift;
	truncate_msg($msg,2000);
	my @parts;
#done
  	if ($msg->{target_type} eq 'application/vnd.nokia.ringing-tone') {
		($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
		@parts = ringtonetools('rtttl','nokia',$msg->{msg},$msg->{id});
		hex2byte(\@parts);
		my $additional_udh = "\x05\x04\x15\x81\x00\x00";
		pack_long(\@parts,$additional_udh);
		$msg->{parts} =  \@parts;
	}
#done
	elsif ($msg->{target_type} eq 'application/vnd.ems.v3') {
		($msg->{data_coding},$msg->{esm_class}) = (0xF5,0x40);
		@parts = ringtonetools('rtttl','imy',$msg->{msg},$msg->{id});
		hex2byte(\@parts);
		$msg->{parts} =  \@parts;
	}
	else {
		return 0;
	}
	return 1;
}

###### MIDI ######
sub encode_midi {
	my $msg = shift;
	truncate_msg($msg,2000);
	my @parts;
  	if ($msg->{target_type} eq 'application/vnd.nokia.ringing-tone') {
		($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
		my $rtttl = ringtonetools('-intype midi -outtype rtttl',pack('H*',$msg->{msg}));
		@parts = OTARingingTone($rtttl);
		return 0xfff0 if $parts[0] == -1;
		cut_parts(\@parts,133,128);
		my $additional_udh = "\x05\x04\x15\x81\x00\x00";
		pack_long(\@parts,$additional_udh);
		$msg->{parts} =  \@parts;
	}
	elsif ($msg->{target_type} eq 'application/vnd.ems.v3') {
		$msg->{parts} =  \@parts;
	}
#done
	elsif ($msg->{target_type} eq 'siemens') {
		($msg->{data_coding},$msg->{esm_class}) = (0xF5,0x00);
		@parts = ringtonetools('midi','seo',pack('H*',$msg->{msg}),$msg->{id});
		$msg->{parts} = \@parts;
	}
	else {
		return 0;
	}
	return 1;
}

###### TEXT/PLAIN ######
sub encode_hidden_text {
	my $msg = shift;
#	$msg->{msg} = 'hidden';
	$msg->{cut_type}=4;
	my $params = dumper_load($msg->{params});
	$params->{hidden} = 1;
	$msg->{params} = dumper_save($params);
	my $retval = encode_text($msg);
	return $retval;
}


sub encode_text {
	my $msg = shift;
	# Very ugly hack for Beeline MT 7052
	$msg->{cut_type}=2 if (($msg->{smsc_id} =~ /^(520|20)$/) and ($msg->{num} =~ /^(7052|5504)/));
	#
	$msg->{udh_id} = int(rand(254))+1;
	my ($data_coding,$coding_obj,$cut_count,$max_count,$trunc_count,$pref_count) =
	  (gsm_compatible($msg->{msg})) ? (0,$gsm0338_obj,152,160,760,4) : (8,$ucs2_obj,134,140,938,8);
	($cut_count,$trunc_count) = (132,924) if (($msg->{cut_type}==1)and(!gsm_compatible($msg->{msg})));
	$trunc_count = 36 *  $trunc_count if ($msg->{client_type} eq 'smsc');
	my $text = substr($coding_obj->encode(decode_cp1251($msg->{msg})),0,$trunc_count);
	$max_count = (gsm_compatible($msg->{msg})) ? 137 : 120 if ($msg->{cut_type}==5);
	$cut_count = $max_count-$pref_count if (($msg->{cut_type}==3)or($msg->{cut_type}==5));
	$text = substr($text,0,$max_count) if ($msg->{cut_type}==4);
	my @parts = ($text);
	cut_parts(\@parts,$max_count,$cut_count) if ($msg->{cut_type}!=2);
	if ((($msg->{cut_type}==3)or($msg->{cut_type}==5)) and (@parts>1)) {
		for (my $i=0; $i < @parts; $i++) {
			my $pref = ($i+1).'/'.scalar(@parts).' ';
			$parts[$i] = $coding_obj->encode($pref).$parts[$i];
		}
	}

	if ($msg->{use1251}) {
		for (my $i=0; $i < @parts; $i++) {
			$parts[$i] = $cp1251_obj->encode($coding_obj->decode($parts[$i]));
		}
	}
	elsif ($data_coding==0) {
		if (($msg->{use7bit}==1)and(@parts==1)) {
			$parts[0] = pack_7bit($parts[0]);
		}
		elsif ((($msg->{use7bit}==1)or($msg->{use7bit}==3)or($msg->{use7bit}==4))and(@parts>1)) {
			for (my $i=0; $i < @parts; $i++) {
				$parts[$i]=("\x00"x7).$parts[$i];
				$parts[$i] = pack_7bit($parts[$i]);
				$parts[$i] =~ s/^.{6}//;
			}
		}
		elsif (!$msg->{gsm_alphabet}) {
			for (my $i=0; $i < @parts; $i++) {
				$parts[$i] = $cp1251_obj->encode($gsm0338_obj->decode($parts[$i]));
			}
		}
	}
	my $esm_class = ((@parts>1) and (!$msg->{cut_type})) ? 0x40 : 0x00;
	$msg->{sar_msg_ref_num} = ((@parts>1) and ($msg->{cut_type}==1)) ? int(rand(254)+1) : 0x00;
	pack_long(\@parts,undef,$msg->{udh_id}) if (!$msg->{cut_type});
	($msg->{data_coding},$msg->{esm_class},$msg->{parts}) = ($data_coding,$esm_class,\@parts);
	return 'text/plain ('.{0 => 'udh',1 => 'sar',2 => 'payload',3 => 'sms',4 => 'single sms'}->{$msg->{cut_type}}.')';
}

sub encode_text_old {
	my $msg = shift;
	truncate_msg($msg,800);
	my $text = $msg->{msg};
	my ($data_coding,$cut_count,$max_count) =
	  ($text =~ m/[\x80-\xff]/)?(8,66,70):(0,152,160); # ��������� � 153 �� 152 (67->66 also) �� ������ ������
	$cut_count = $max_count-4 if ($msg->{cut_type}==3);
	$text =~ s/\|/"\x0A"/eg;
	$text = substr($text,0,$max_count) if ($msg->{cut_type}==4);
	my @parts = ($text);  #	my @parts = split(/\s*#\s*/,$text);
	cut_parts(\@parts,$max_count,$cut_count) if ($msg->{cut_type}!=2);
	if (($msg->{cut_type}==3) and (@parts>1)) {
		for (my $i=0; $i < @parts; $i++) {
			$parts[$i]=($i+1).'/'.scalar(@parts).' '.$parts[$i];
		}
	}
	if ($data_coding==8) {
		if (! $msg->{use1251}) {
			for (my $i=0; $i < @parts; $i++) {
				$parts[$i] = encode('UCS-2',decode('cp1251',$parts[$i]));
			}
		}
	}
	elsif (($msg->{use7bit}==1)or($msg->{use7bit}==3)or($msg->{use7bit}==4)) {
		if (@parts==1) {
			$parts[0] = pack_7bit_old($parts[0]) if ($msg->{use7bit}==1);
		}
		else {
			for (my $i=0; $i < @parts; $i++) {
				$parts[$i]=("\x00"x7).$parts[$i];
				$parts[$i] = pack_7bit_old($parts[$i]);
				$parts[$i] =~ s/^.{6}//; # ���� ���������� �� '?' ��-�� GSM ���������
			}
		}
	}
	elsif (($data_coding==0)and $msg->{gsm_alphabet}) {
		for (my $i=0; $i < @parts; $i++) {
			$parts[$i] = cp1251_to_gsm($parts[$i]);
		}
	}
	my $esm_class = ((@parts>1) and (!$msg->{cut_type})) ? 0x40 : 0x00;
	$msg->{sar_msg_ref_num} = ((@parts>1) and ($msg->{cut_type}==1)) ? int(rand(254)+1) : 0x00;
	pack_long(\@parts,undef,$msg->{udh_id}) if (!$msg->{cut_type});
	if (($msg->{cut_type}==2) and ($msg->{target_type} =~ /^(cat\d\d?\_\w*)$/)) {
		$msg->{tariff} = uc($1);
	}
	($msg->{data_coding},$msg->{esm_class},$msg->{parts}) = ($data_coding,$esm_class,\@parts);
	return 'text/plain ('.{0 => 'udh',1 => 'sar',2 => 'payload',3 => 'sms',4 => 'single sms'}->{$msg->{cut_type}}.')';
}

##### WAP #####
sub encode_wappush {
	# ����� �� ������� $sms_data = "%01%06%04%03%AE%81%EA%02%05%6A%00%45%C6%0b%03" . $url . "%00%08%01%03" . $name . "%00%01%01"
	my $msg = shift;
	truncate_msg($msg,200);
	my ($href,$text) = split(/\s*#\s*/,$msg->{msg});
	$text ||= '';

	if ($href =~ m/^http:\/\/www\./) {
		$href =~ s/^http:\/\/www\./\x0D\x03/;
	}
	elsif ($href =~ m/^http:\/\//) {
		$href =~ s/^http:\/\//\x0C\x03/;
	}
	else {
		$href = "\x0C\x03".$href;
	}
	$text = encode('UTF-8',decode('cp1251',$text));

	my $data = "\x01\x06\x01\xAE\x02\x05\x6A\x00\x45\xC6\x08".$href."\x00\x01\x03".$text."\x00\x01\x01";

	my @parts = ($data);
	cut_parts(\@parts,133,128);
	pack_long(\@parts,"\x05\x04\x0B\x84\x23\xF0");
	@parts = ($msg->{msg}) if ($msg->{smsc_id}==53); # ��������� ������� ��� ����������
	($msg->{data_coding},$msg->{esm_class},$msg->{parts}) = (0xF5, 0x40,\@parts);
	return 1;
}

sub encode_flash {
	# ������� ��������� ������ ������, �.�. �� �����������
	my $msg = shift;
	my $ret = encode_text($msg);
	$msg->{data_coding} = $msg->{data_coding} | 0x10;
	return $ret;
}

sub encode_app_port {
	my $msg = shift;
	truncate_msg($msg,260);
	return encode_text($msg)
	  unless ($msg->{msg} =~ m/^\d+:?\d*#/);
	my ($destination_port,$source_port,$text) = $msg->{msg} =~ /^(\d+):?(\d*)#(.*)$/;
	$destination_port = pack('n',int($destination_port));
	$source_port = pack('n',int($source_port) || 16000);

	my @parts = ($text);
	cut_parts(\@parts,133,128);
	pack_long(\@parts,"\x05\x04".$destination_port.$source_port);
	($msg->{data_coding},$msg->{esm_class},$msg->{parts}) = (0x04, 0x40,\@parts);
	return 1;
}

sub encode_ems {
	my $msg = shift;
	truncate_msg($msg,280);
	($msg->{data_coding},$msg->{esm_class}) = (0x04,0x40);
	my @parts = pack('H*',$msg->{msg});
	$msg->{parts} =  \@parts;
	return 1;
}

sub truncate_msg {
	my ($msg,$len) = @_;
	if (length($msg->{msg}) > $len) {
		$msg->{msg_original} = $msg->{msg};
		$msg->{msg} = substr($msg->{msg},0,$len);
		my $db = Disp::AltDB::get_db();
		my ($s_name,$s_email,$m_email) = 
		  $db->{db}->selectrow_array('select s.fullname,s.email,m.email from set_service s left join set_cyka_manager m on s.manager_id=m.id where s.id=?',undef,$msg->{service_id});
		$s_email .= ($s_email and $m_email) ? ",$m_email" : $m_email;
		yell("Message text exceeds limits ($len characters), will be truncated\nService: $s_name\n".Dumper($msg),
			 code => 'WARNING',
			 process => 'ENCODER',
			 contact_group => 'encoder_trunc',
			 cc => $s_email,
			 header => "ENCODER: Message text exceeds limits");
	}
}


1;
