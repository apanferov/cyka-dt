#!/usr/bin/perl -w
package Disp::Mysql;
use strict;
use warnings;

our $VERSION	= 0.1;		# Версия пакета.

sub common_columns {
    my ( $dbh_1, $db_1, $table_1, $dbh_2, $db_2, $table_2 ) = @_;

    my @c1 = table_columns($dbh_1, $db_1, $table_1);
    my @c2 = table_columns($dbh_2, $db_2, $table_2);

    my %diff;

    $diff{$_}++ for @c1;
    $diff{$_}++ for @c2;

    return grep { $diff{$_} == 2 } keys %diff;
}

sub table_columns {
    my ( $dbh, $database, $table ) = @_;

    return select_array($dbh, "select column_name from information_schema.columns where table_schema like ? and table_name like ?",
			$database, $table);

}

sub select_array {
    my $dbh = shift;
    my $query = shift;

    my $st = $dbh->prepare($query);
    $st->execute(@_);

    my @ret;
    while (my @row = $st->fetchrow_array) {
	push @ret, $row[0];
    }
    return @ret;
}


1;
