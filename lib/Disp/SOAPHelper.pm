package Disp::SOAPHelper;

use strict;
use warnings;

sub soap_return {
    my @ret;
    foreach my $a (@_) {
        my $data = SOAP::Data->new(value => $a->[0]);
        if (@$a > 1) {
            $data->name($a->[1]);
        }
        if (@$a > 2) {
            $data->type($a->[2]);
        }
        push @ret, $data;
    }
    return @ret;
}

1;
