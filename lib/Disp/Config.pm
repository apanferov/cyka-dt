#!/usr/bin/perl
package Config;
use strict;
use warnings;
use DBI;
use ConfigTinyWrapper;
use Data::Dumper;
use File::Basename;
use Params::Validate qw/:all/;
use Net::Domain qw(hostfqdn);

my $p;

sub init {
        my ( %params ) = validate(@_, { skip_db_connect => { optional => 1 },
                                                                        config_file => { type => SCALAR } });

        $p = ConfigTinyWrapper->read( $params{config_file} );

        die $ConfigTinyWrapper::errstr unless $p;

        if ($p->{_}{postinclude}) {
		my $pp = ConfigTinyWrapper->read(dirname($params{config_file}) . '/' . $p->{_}{postinclude});
		while (my($section, $data) = each %$pp) {
			map { $p->{$section}{$_} = $data->{$_} } keys %$data;
		}
	}
	my @dbconf = split(/\s*,\s*/,($p->{_}{db_billing} || ''));
		foreach (@dbconf) {
			my $conf_name = (dirname($params{config_file}) . "/$_") unless -f $_;
			my $tmp = ConfigTinyWrapper->read( $conf_name );
			die $ConfigTinyWrapper::errstr unless $tmp;
			$p->{db_billing}{$tmp->{_}{host}} = $tmp->{_};
		}
        @dbconf = split(/\s*,\s*/,($p->{_}{db_transport} || ''));
		foreach (@dbconf) {
			my $conf_name = (dirname($params{config_file}) . "/$_") unless -f $_;
			my $tmp = ConfigTinyWrapper->read( $conf_name );
			die $ConfigTinyWrapper::errstr unless $tmp;
			$p->{db_transport}{$tmp->{_}{host}} = $tmp->{_};
		}
		$p->{_}{local_db_host} ||= hostfqdn();
        $p->{database} = $p->{db_transport}{$p->{_}{local_db_host}} || $p->{db_billing}{$p->{_}{local_db_host}};

        $p->{_}{original_config} = $params{config_file};

        db_connect() unless $params{skip_db_connect};
}

sub big_dbh {
        return $p->{big_dbh} if $p->{big_dbh};

        my $dbconf = Config->param('replication_config');
        $dbconf = (dirname(Config->param('original_config')) . "/$dbconf") unless -f $dbconf;
        my $tmp = ConfigTinyWrapper->read( $dbconf );
        die $ConfigTinyWrapper::errstr unless $tmp;

        $tmp = $tmp->{_};

        $p->{big_dbh} = DBI->connect("DBI:mysql:$tmp->{name};host=$tmp->{host}",
                                                                 $tmp->{username}, $tmp->{password},
                                                                 { AutoCommit => 1,
                                                                   RaiseError => 1 });
        $p->{big_dbh}->do("SET NAMES 'cp1251'");
        $p->{big_dbh}->do("SET character_set_client=cp1251");

        return $p->{big_dbh};
}

sub db_connect {                  # () void
	unless (exists $p->{dbh} and $p->{dbh})
	{
		$p->{dbh} = DBI->connect("DBI:mysql:$p->{database}{name};host=$p->{database}{host}",
                                                         $p->{database}{username}, $p->{database}{password},
                                                         { AutoCommit => 1,
                                                           RaiseError => 1 });
        $p->{dbh}->{mysql_auto_reconnect} = 0;
        $p->{dbh}->do("SET NAMES 'cp1251'");
        $p->{dbh}->do("SET character_set_client=cp1251");
	}
}

sub param {                       # ($name, $opt_section) anything
        my ( $proto, $name, $section ) = @_;
        $section ||= '_';
#        die "Unknown Config parameter '$name'($section)" unless exists $p->{$section}{$name};
        return $p->{$section}{$name};
}

sub safe_param {                  # ($name, $default, $section) anything
        my ( $proto, $name, $default, $section ) = @_;
        my $ret;
        eval {
                $ret = Config->param($name => $section);
        };
        if ($@) {
                $ret = $default;
        }
        return $ret;
}

sub dbh {
        if ( $p->{dbh} and not $p->{dbh}->ping) {
                delete $p->{dbh};
                db_connect;
        }
        return $p->{dbh};
}

sub clone_dbh {
	$p->{dbh}->{InactiveDestroy} = 1;
	$p->{dbh} = $p->{dbh}->clone;
	$p->{dbh}->{mysql_auto_reconnect} = 0;
	$p->{dbh}->do("SET NAMES 'cp1251'");
	$p->{dbh}->do("SET character_set_client=cp1251");
}

=head3 section ($section) hashref

���������� ���� ������ ����������������� ����� � ���� ����.

C<Parameters>:
        B<section>              string                  ��� ������.

=cut                              #
sub section {                     # ($section) hashref
        my ( $proto, $section ) = @_;
        unless (exists $p->{$section}) {
                die "No such section '$section' in Config";
        }
        return $p->{$section};
}

sub dbh_config_maker {
    my ( $proto, $param ) = @_;

    my $or_conf = Config->param('original_config');
    my $db_conf = Config->param($param);

    $db_conf = (dirname($or_conf) . "/$db_conf") unless -f $db_conf;
    my $tmp = ConfigTinyWrapper->read( $db_conf );
    die $ConfigTinyWrapper::errstr unless $tmp;

    my $dbh = DBI->connect("DBI:mysql:$tmp->{_}{name};host=$tmp->{_}{host}",
                           $tmp->{_}{username}, $tmp->{_}{password},
                           { AutoCommit => 1,
                             PrintError => 0,
                             RaiseError => 1 });
    $dbh->{mysql_auto_reconnect} = 0;
    $dbh->do("SET NAMES 'cp1251'");
    $dbh->do("SET character_set_client=cp1251");
    return ($dbh, $tmp->{_});
}

sub dbh_config_loader {
    my ( $proto, $db_conf ) = @_;

    my $or_conf = Config->param('original_config');

    $db_conf = (dirname($or_conf) . "/$db_conf") unless -f $db_conf;
    my $tmp = ConfigTinyWrapper->read( $db_conf );
    die $ConfigTinyWrapper::errstr unless $tmp;

    my $dbh = DBI->connect("DBI:mysql:$tmp->{_}{name};host=$tmp->{_}{host}",
                           $tmp->{_}{username}, $tmp->{_}{password},
                           { AutoCommit => 1,
                             RaiseError => 1 });
    $dbh->{mysql_auto_reconnect} = 0;
    $dbh->do("SET NAMES 'cp1251'");
    $dbh->do("SET character_set_client=cp1251");
    return $dbh;
}

sub set_dbh {
	my ( $proto, $dbh ) = @_;
	$p->{dbh} = $dbh;
}

1;
