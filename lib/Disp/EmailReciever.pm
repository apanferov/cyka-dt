#!/usr/bin/perl -w
# $Id: $
package Disp::EmailReciever;
use strict;
use warnings;
use base 'Disp::Object';
use Data::Dumper;
use MIME::Parser;
use MIME::QuotedPrint;
use Encode qw/decode encode/;
use File::Path 'mkpath';
use Disp::Utils;

our $VERSION	= 0.1;		# Версия пакета.

sub init {
    my ( $self, $daemon ) = @_;

    $self->{smsc_id} = $daemon->{smsc_id};
    $self->{proc_name} = $daemon->proc_name;
    $self->{time_to_get_mail} = time();
    $self->{mail_get_interval} = 60; #300;
    $self->{pr_name} = "rcv_$self->{smsc_id}";

    $daemon->{db}->load_statistics($self->{pr_name});

    return;
}

sub deinit {
    my ( $self, $daemon ) = @_;

    $daemon->{db}->save_statistics($self->{pr_name},$self->{smsc_id});

    return;
}

sub has_something_to_do {
    my ( $self, $daemon ) = @_;
    return 0;
}

sub hard_kill {
    my ( $self, $daemon ) = @_;
    return;
}

sub do_work {
    my ( $self, $daemon, $terminating ) = @_;

    if ( $self->_get_mail($daemon) ) {
	$self->_process_mail($daemon);
    } else {
	$daemon->{db}->ping;
    }

    $daemon->{db}->save_statistics($self->{pr_name},$self->{smsc_id});

    return 'idle';
}

sub _get_mail {
    my ( $self, $daemon ) = @_;

    return 0 if $self->{time_to_get_mail} > time;

    my ( $ret, $out, $err ) = open3_simple(['/usr/bin/fetchmail',
											'-f', '-',
											'-m', "/usr/bin/procmail ".$self->_path_to_procmail_config],
										   read_file($self->_path_to_fetchmail_config));

    $self->{time_to_get_mail} = time + $self->{mail_get_interval};

    if ($ret == 1) {
	$daemon->{log}->info("Mailbox is empty");
	return 0;
    } elsif ($ret == 0) {
	$daemon->{log}->info("Some mail fetched");
	return 1;
    } else {
	$daemon->{log}->error("Fetchmail failed with errorcode $ret");

	$self->_yell($daemon, "RC: $ret\nSTDOUT:\n$out\n\nSTDERR:\n$err",
		     header => "Fetchmail failed");
	return 0;
    }
}

sub _process_mail {
    my ( $self, $daemon ) = @_;

    my $m = new MIME::Parser;
    $m->decode_headers(1);
    $m->output_to_core(1);

    my $in_dir = Config->param('system_root').sprintf('/var/mail/smtp_%i', $self->{smsc_id});
	-d $in_dir or mkpath $in_dir;

    opendir DIR, $in_dir or die "Mail readdir failed: $!";
    $daemon->{log}->debug("Reading mail from $in_dir");

    my $done_sub = sub {
	my ( $name, $folder ) = @_;
	$daemon->{log}->info("Moving message text to folder '$folder'");
	my $outdir = "$in_dir/$folder";
	-d $outdir or mkpath $outdir;
	my $outname = sprintf('%s/%s_%d_%d', $outdir, $name, $$, time);
 	rename "$in_dir/$name", $outname
 	  or die "Renaming of $name failed";
    };

    foreach my $name (grep { /^\d+$/ && -f "$in_dir/$_" } readdir DIR) {
	$daemon->{log}->debug("Will read file named $in_dir/$name");
	my $contents = read_file("$in_dir/$name");
 	my $e = $m->parse_data($contents);

	chomp(my $from = $e->get('from'));
	chomp(my $to = $e->get('to'));

	my ( $abonent, $link ) = $self->_extract_abonent($daemon, $from);
	my ( $num ) = $self->_extract_number($daemon, $to);

	unless ($abonent) {
	    $daemon->{log}->warn("Can't get abonent from: $from");
	    $self->_yell($daemon, "Can't get abonent from: $from",
		 header => 'Cannot determine abonent from e-mail');
	    $done_sub->($name, 'error');
	    next;
	}

	unless ($num) {
	    $daemon->{log}->warn("Can't get short number from: $to");
	    $self->_yell($daemon, "Can't get short number from: $to",
		 header => "Cannot extract short number");
	    $done_sub->($name, 'error');
	    next;
	}

	chomp(my $ct = $e->get('content-type'));
	chomp(my $te = $e->get('content-transfer-encoding'));;

	chomp(my $msg = join "\n", map { chomp $_; $_ } @{$e->body});

	if ($te eq 'quoted-printable') {
	    $msg = decode_qp($msg);
	} elsif ($te eq '8bit') {
	    # Do nothing
	} else {
	    $self->_yell($daemon, "Unknown transfer encoding: $te",
		 header => "Unknown transfer-encoding");
	    $daemon->{log}->error("Unknown transfer encoding: $te");
	    $done_sub->($name, 'error');
	    next;
	}

	if ($ct eq 'text/plain; charset=us-ascii') {
	    # Do nothing
	} elsif ($ct eq 'text/plain; charset=koi8-r') {
	    $msg = encode('cp1251', decode('koi8-r', $msg));
	} elsif ($ct eq 'text/plain; charset=koi8-u') {
	    $msg = encode('cp1251', decode('koi8-u', $msg));
	} else {
	    $self->_yell($daemon, "Unknown content-type: $ct",
		 header => 'Cannot handle unknown content-type in e-mail');
	    $daemon->{log}->error("Unknown content-type: $ct");
	    $done_sub->($name, 'error');
	    next;
	}

	my %sms;

	$sms{abonent} = $abonent;
	$sms{smsc_id} = $self->{smsc_id};
	$sms{link_id} = $link;
	$sms{num} = $num;
	$sms{msg} = $msg;
	$sms{data_coding} = 0;
	$sms{esm_class} = 0;

	$sms{pdu} = $contents;
	$sms{transaction_id} = $to;

	my $id = $daemon->{db}->insert_sms_into_insms(\%sms);
	$daemon->{last_received_sms_time} = time();

	$daemon->{log}->info("Stored INSMS$id from abonent $abonent to num $num");;
	$done_sub->($name, 'processed');
    }
    closedir DIR;
}

sub _path_to_fetchmail_config {
    my ( $self ) = @_;
    Config->param('system_root').sprintf('/etc/smtp_%i_fetchmail.conf', $self->{smsc_id});
}

sub _path_to_procmail_config {
    my ( $self ) = @_;
    Config->param('system_root').sprintf('/etc/smtp_%i_procmail.conf', $self->{smsc_id});
}

sub _maildir {
    my ( $self ) = @_;
    Config->param('system_root')."/var/mail";
}

sub _extract_number {
    my ( $self, $daemon, $to ) = @_;

    my ( $num_part, $domain ) = split /\@/, $to, 2;

    return unless $domain eq 'gateasg.alt1.ru';
    return unless $num_part =~ /^(?:umc|jump)(\d+)$/;

    return $1;
}

sub _extract_abonent {
    my ( $self, $daemon, $from ) = @_;

    my ( $abonent, $link ) = split /\@/, $from, 2;

    # XXX from config
    return unless $abonent =~ /^\d+$/;
    return unless $link =~ /^(partner.(?:umc|mts).com.ua|partner.jeans.net.ua)$/;

    return ( $abonent, $link );
}

sub _yell {
    my ( $self, $daemon, $msg, %params ) = @_;
    yell( $msg ,
	  code => 'ERROR',
	  process => $self->{proc_name},
	  header => "$self->{proc_name}: $params{header}",
	  ignore => { log4perl => 1 });

}

1;




