package Disp::Daemon::IPC;
use strict qw(vars);
use warnings;
#use utf8;

use Socket;
use IO::Handle;
use IO::Select;
use Fcntl qw/F_SETFD/;
use Data::Dumper;
use Log::Log4perl qw(get_logger);

my $log = get_logger('disp_daemon_ipc');

sub set_fh {
	my ( $self, $fh ) = @_;
	$self->{fh} = $fh;
	$self->{select} = IO::Select->new;
	$self->{select}->add($fh);
}

sub can_read {
	my ( $self ) = @_;

	my @can_read = $self->{select}->can_read(0);

	if ( @can_read == 1 and $can_read[0] == $self->{fh} ) {
		return 1;
	}

	return 0;
}

sub worker {
	my $proto = shift;
	$proto = ref($proto) || $proto;

	my $self = bless { }, $proto;

	my $socket_fd = $_[1];
	unless ( $socket_fd =~ /^\d+$/) {
		die "No socket passed";
	}

	open my $manager_fh, "+<&=", $socket_fd or die "socket reopen: $!";
	$manager_fh->autoflush(1);
	$self->set_fh($manager_fh);
	return $self;
}

sub spawn {
	my $proto = shift;
	$proto = ref($proto) || $proto;

	my $self = bless { }, $proto;

	my ( $manager_fh, $worker_fh );

	socketpair($worker_fh, $manager_fh, AF_UNIX, SOCK_STREAM, PF_UNSPEC)
	  or die "socketpair: $!";
	my $child_pid = fork;

	if ( not defined $child_pid ) {
		die "fork failed: $!";
	} elsif ( $child_pid > 0) {
	    $self->{pid} = $child_pid;
	    close $manager_fh;
	    $worker_fh->autoflush(1);
	    $self->set_fh($worker_fh);
	    return $self;
	} else {

		close $worker_fh;

		fcntl($manager_fh, F_SETFD, 0)
		  or die "Can't clear close-on-exec flag on manager_fh: $!";

		my $socket_fd = fileno($manager_fh);

		exec $0, 'worker', $socket_fd;
	}
}

sub message {
	my $self = shift;
	my @data = @_;
	# XXX Normal serialization here
	my $data = join ",", @data;
	my $len = sprintf '%08X', length $data;

	my $real_data = $len . $data;
	my $writen = syswrite $self->{fh}, $real_data;

	if ( length($real_data) != $writen) {
		die "Shit happens while sending IPC message";
	}
	$log->debug("[$$] Message sent: '$real_data'");
	return;
}

sub check_message {
	my ( $self ) = @_;
	if ( $self->can_read ) {
		my $read;
		my $len;

		$read = sysread $self->{fh}, $len, 8;

		return unless $read; # looks like other side is dead
		die "IPC protocol violation: read only $read bytes instead of 8" if $read != 8;

		$len = hex($len);

		my $data;

		$read = sysread $self->{fh}, $data, $len;
		die "Bad IPC packet: got $read bytes instead of $len" unless $read == $len;
		$log->debug("[$$] Recieved: $data\n");
		return split /,/, $data;
	}
	return (0);
}

sub pid {
    $_[0]{pid};
}

sub kill {
    my ( $self, $signal ) = @_;
    kill 'TERM', $self->{pid};
}

sub wait_message {
    my ( $self, $message, $timeout ) = @_;

    $timeout ||= 60;
    my $end_time = time + $timeout;

    $log->debug("[$$] Waiting for '$message'...\n");
    while (time < $end_time) {
	my @message = $self->check_message;

	# Process must be dead
	unless (defined $message[0]) {
	    return;
	}

	if ($message[0] and $message[0] eq $message) {
	    $log->debug("[$$] Wait for '$message' succeded");
	    return @message;
	}

	sleep(1);
    }
    return;
}

1;
__END__
