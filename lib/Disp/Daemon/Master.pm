package Disp::Daemon::Master;
use strict qw(vars);
use Log::Log4perl qw(get_logger);
use Disp::Daemon::IPC;
use Disp::Utils qw(yell);
use POSIX ":sys_wait_h";
use Fcntl ':flock';
use FindBin;

my $log = get_logger('disp_daemon_master');
my @ipc = ();
my($RESTART, $TERMINATE) = (0, 0);
my($pid_fd, $pid_file)   = (undef, "$FindBin::Bin/../bin/dispatcher_master.lock");

sub _pid_init
{
	open $pid_fd, "> $pid_file" or die "Cannot open $pid_file for writing";
	flock($pid_fd, LOCK_EX|LOCK_NB) or die "$pid_file is locked, master is already running";
	print $pid_fd $$;
}

sub _pid_exit
{
	flock($pid_fd, LOCK_UN) && close $pid_fd && unlink $pid_file;
}

sub _sig_init
{
	$SIG{HUP} = sub
		{
			$log->debug(sprintf('Restarting worker [%d] on SIGHUP', $ipc[$#ipc]->pid));
			$RESTART = 1;
		};

	$SIG{$_} = sub
		{
			$log->info("Stopping master [$$]");
			for my $ipc (@ipc)
			{
				$log->debug(sprintf('Terminating worker [%d] on INT*QUIT*TERM', $ipc->pid));
				$ipc->kill;
			}
			$TERMINATE = 1;
		}
		for qw(INT QUIT TERM);

	$SIG{CHLD} = sub
		{
			while((my $kid = waitpid(-1, WNOHANG)) > 0)
			{
				$log->debug("Worker [$kid] was reaped");
				@ipc = grep { $kid != $_->pid } @ipc;
			}
			$log->debug(sprintf('Running workers (%s)', join(',', map { $_->pid } @ipc) ));
			$RESTART = 1 unless (@ipc);
		};
}

sub run
{
	&_pid_init;
	&_sig_init;

	$log->info("Dispatcher master [$$] is running");
	until ($TERMINATE)
	{
		$log->info('Starting worker...');

		my $ipc = Disp::Daemon::IPC->spawn;
		my(undef, $ok) = $ipc->wait_message('initialized', 45);
		if (!$ok)
		{
			$log->error(sprintf('Worker [%d] is not initialized in proper time', $ipc->pid));
			yell(sprintf('Worker [%d] is not initialized in proper time, running workers (%d)', $ipc->pid, scalar @ipc),
				code    => 'PROC_INIT',
				process => 'DISPATCHER MASTER',
				header  => 'Dispatcher worker initialization');
			$ipc->kill;
			next;
		}

		push @ipc, $ipc;

		if (@ipc < 2)
		{
			$ipc->message('go');
			$log->info(sprintf('Worker [%d] is started', $ipc->pid));
		}
		else
		{
			my $prev = $ipc[$#ipc - 1];
			$prev->kill;

			my(undef, $pos) = $prev->wait_message('stopped', 45);
			$log->error(sprintf('Worker [%d] didnt send position in proper time', $prev->pid))
				if(!defined $pos);

			$ipc->message('go', $pos);

			$log->info(sprintf('Another worker [%d] is started at pos %d', $ipc->pid, $pos));
		}

		sleep(1) while (!$RESTART);
		$RESTART = 0;
	}
	&_pid_exit;
	$log->info("Dispatcher master [$$] is finished");
}

1;
__END__
