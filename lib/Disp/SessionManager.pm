#!/usr/bin/perl -w
package Disp::SessionManager;
use strict;
use warnings;
use base 'Exporter';
use Data::Dumper;
use Date::Parse qw(str2time);
our $VERSION	= '1.000';

# sub new {
#    my ($class) = @_;

#    my $sessions = {};
#    my $ar = Config->dbh->selectall_hashref('select inbox_id,date,abonent,num,unix_timestamp(end_date) as end_date,service_id,keyword from tr_session where end_date > now() order by inbox_id','inbox_id');
#    $sessions->{$ar->{$_}{abonent}.'-'.$ar->{$_}{num}} = $ar->{$_} foreach(keys %$ar);
#    my $self = {
# 			   sessions => $sessions,
# 			   cleanup_counter => 0
#    };


#    bless $self, $class;
#    return $self;
# }

# sub create_session {
# 	my ($self,$msg,$service,$keyword) = @_;
# 	my $session = {inbox_id => $msg->{id},
# 				   date => $msg->{date},
# 				   abonent => $msg->{abonent},
# 				   num => $msg->{num},
# 				   end_date => str2time($msg->{date}) + $service->{session_timeout},
# 				   service_id => $service->{id},
# 				   keyword => $keyword
# 				  };
# 	$self->update_session($session);
# 	$self->cleanup;
# 	return $session;
# }

# sub get_session {
# 	my ($self,$msg) = @_;
# 	my $key = $msg->{abonent}.'-'.$msg->{num};
# 	return $self->{sessions}{$key}
# 	  if ($self->{sessions}{$key} and ($self->{sessions}{$key}{end_date} > str2time($msg->{date})));
# 	return undef;
# }

# sub update_session {
# 	my ($self,$session) = @_;
# 	Config->dbh->do('do update_session(?,?,?,?,from_unixtime(?),?,?)', undef,
# 					$session->{inbox_id}, $session->{date}, $session->{abonent},
# 					$session->{num}, $session->{end_date}, $session->{service_id},
# 					$session->{keyword});
# 	my $key = $session->{abonent}.'-'.$session->{num};
# 	$self->{sessions}{$key} = $session;
# }

# sub cleanup {
# 	my ($self) = @_;
# 	if ($self->{cleanup_counter}++ > 1000) {
# 		$self->{cleanup_counter} = 0;
# 		my $date = time();
# 		my $sessions = $self->{sessions};
# 		foreach (keys(%$sessions)) {
# 			delete $sessions->{$_} if ($sessions->{$_}{end_date} < $date);
# 		}
# 	}
# }



sub new {
   my ($class) = @_;
   my $self = {
   };
   bless $self, $class;
   return $self;
}

sub create_session {
	my ($self,$msg,$service,$keyword) = @_;
	my $session = {inbox_id => $msg->{id},
				   date => $msg->{date},
				   abonent => $msg->{abonent},
				   num => $msg->{num},
				   end_date => str2time($msg->{date}) + $service->{session_timeout},
				   service_id => $service->{id},
				   keyword => $keyword
				  };
	$self->update_session($session);
	return $session;
}

sub get_session {
	my ($self,$msg) = @_;
	my $num = $msg->{num};
	$num =~ s/#$//; # Beeline Postpone Payment Content 
	my $ar = Config->dbh->selectrow_hashref('select id,inbox_id,date,abonent,num,unix_timestamp(end_date) as end_date,service_id,keyword from tr_session where abonent=? and num=? and end_date>=? order by end_date desc limit 1',undef,$msg->{abonent},$num,$msg->{date});
	return $ar;
}

sub update_session {
	my ($self,$session) = @_;
	Config->dbh->do('do update_session(?,?,?,?,?,from_unixtime(?),?,?)', undef,
					$session->{id},
					$session->{inbox_id}, $session->{date}, $session->{abonent},
					$session->{num}, $session->{end_date}, $session->{service_id},
					$session->{keyword});
}

1;
