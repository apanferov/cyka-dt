#!/usr/bin/perl -w
# -*- coding: windows-1251-unix -*-
package Disp::SMPP_SMSClient;
use strict;
use warnings;
use base 'Exporter';
use DBI;
use Net::SMPP;
use Data::Dumper;
use IO::File;
use IO::Select;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
use Config::Tiny;
use Time::HiRes qw(gettimeofday tv_interval usleep);
our $VERSION	= '1.001';

#+binarin
my %error_tab = ( # [$wait, $sleep, $block]
				 85 => { # Ivanovo
						0xffff => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x8001 => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x8004 => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x8005 => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x800b => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x800c => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						0x8013 => { retry => 4,
									first => [900,0,900],
									other => [900,0,900] },
						_default_ => { retry => 0,
									   first => 3,
									   other => 3 },
						_no_resp_ => { retry => 3,
									   first => 600,
									   other => 600 }
					   },
				 _default_ => {
							   _default_ => { retry => 0,
											  first => 666,
											  other => 666 },
							   0x14 => { retry => 10,
										 first => [10,1,30],
										 other => [10,1,30] },
							   0x58 => { retry => 100,
										 first => [1,1],
										 other => [1,1] },
							   0x8 => { retry => 10,
										first => 10,
										other => 60 },
							   _no_resp_ => { retry => 5,
											  first => 60,
											  other => 60 }
							  }
				);

my %dr_tab = (
	2 => {status => 12, name => 'DELIVRD', message_state => 2},
	DELIVRD => {status => 12, name => 'DELIVRD', message_state => 2},
	6 => {status => 11, name => 'ACCEPTD', message_state => 6},
	ACCEPTD => {status => 11, name => 'ACCEPTD', message_state => 6},
	8 => {status => 13, name => 'REJECTD', message_state => 8},
	REJECTD => {status => 13, name => 'REJECTD', message_state => 8},
	3 => {status => 14, name => 'EXPIRED', message_state => 3},
	EXPIRED => {status => 14, name => 'EXPIRED', message_state => 3},
	5 => {status => 15, name => 'UNDELIV', message_state => 5},
	UNDELIV => {status => 15, name => 'UNDELIV', message_state => 5},
);

my %process_tab = (
    0x00000001 => { cmd => 'bind_receiver',
    	accept => {'connect'=>[1,0,0,0,1],}, reply => sub {
    		my ($self,$pdu) = @_;
    		return unless ($self->{smpp_mode} == 4);
			my	$seq = $self->{smpp}->bind_receiver_resp(system_id => '',
				seq => $pdu->{seq}, status => 0x0d);
			$self->{log}->debug("BIND_RECEIVER_RESP $seq sent.");
			$self->{log}->info("BIND_RECEIVER not supplied.");
			$self->{select}->remove($self->{smpp});
    		$self->{smpp}->close();
    		undef $self->{smpp};

		}, },
    0x80000001 => { cmd => 'bind_receiver_resp',
    	accept => {'bind'=>[1,0,1,0,0],}, reply => sub {
    		my ($self,$pdu) = @_;
    		return unless ($self->{state} eq 'bind');
			die("CONNECT_ERROR:Can't bind to $self->{host}:$self->{port}.".$self->get_status_text($pdu->{status})."\n".Dumper($pdu)) if ($pdu->{status});
			$self->{log}->info("Bound as receiver.");
  			$self->enter_state('run');
		}, },
    0x00000002 => { cmd => 'bind_transmitter',
    	accept => {'connect'=>[1,0,0,0,1],}, reply => sub {
    		my ($self, $pdu) = @_;
    		return unless ($self->{smpp_mode} == 4);
			my $seq = $self->{smpp}->bind_transmitter_resp(system_id => '',
			   seq => $pdu->{seq}, status => 0x0d);
			$self->{log}->debug("BIND_TRANSMITTER_RESP $seq sent.");
			$self->{log}->info("BIND_TRANSMITTER not supplied.");
			$self->{select}->remove($self->{smpp});
    		$self->{smpp}->close();
    		undef $self->{smpp};
		}, },
    0x80000002 => { cmd => 'bind_transmitter_resp',
    	accept => {'bind'=>[1,1,0,0,0],}, reply => sub {
    		my ($self,$pdu) = @_;
    		return unless ($self->{state} eq 'bind');
			die("CONNECT_ERROR:Can't bind to $self->{host}:$self->{port}.".$self->get_status_text($pdu->{status})."\n".Dumper($pdu)) if ($pdu->{status});
			$self->{log}->info("Bound as transmitter.");
  			$self->enter_state('run');
		}, },
    0x00000009 => { cmd => 'bind_transceiver',
    	accept => {'connect'=>[1,0,0,0,1],}, reply => sub {
    		my ($self, $pdu) = @_;
    		my $seq;
    		if (($self->{system_id} eq $pdu->{system_id})and($self->{password} eq $pdu->{password})) {
				$seq = $self->{smpp}->bind_transceiver_resp(system_id => $pdu->{system_id},
															seq => $pdu->{seq},
															sc_interface_version => "\x34");
				$self->{log}->debug("BIND_TRANSCEIVER_RESP $seq sent.");
				$self->{log}->info("ESME bound as transceiver.");
				$self->enter_state('run');
			}
			else {
				$seq = $self->{smpp}->bind_transceiver_resp(system_id => $pdu->{system_id},
															seq => $pdu->{seq},
															status => 0x0d);
				$self->{log}->debug("BIND_TRANSCEIVER_RESP $seq sent.");
				$self->{log}->warn('ESME binding failed. Incorrect system_id/password.');
				$self->{select}->remove($self->{smpp});
				$self->{smpp}->close();
    			undef $self->{smpp};
			}
		}, },
    0x80000009 => { cmd => 'bind_transceiver_resp',
    	accept => {'bind'=>[1,0,0,1,0],}, reply => sub {
    		my ($self,$pdu) = @_;
    		return unless ($self->{state} eq 'bind');
			die("CONNECT_ERROR:Can't bind to $self->{host}:$self->{port}.".$self->get_status_text($pdu->{status})."\n".Dumper($pdu))
			  if ($pdu->{status});
			$self->{log}->info("Bound as transceiver.");
  			$self->enter_state('run');
		}, }, 
    0x00000004 => { cmd => 'submit_sm',
    	accept => {'run'=>[1,0,0,0,1],}, reply => sub {
    		my ($self, $pdu) = @_;
    		my ($status,$id) = (($self->{smsc_id} == 102)or($self->{smsc_id} == 183)or($self->{smsc_id} == 185)) ? (0,1) : $self->process_data_pdu($pdu);
    		my $seq;
    		$status = $status || 0;
    		if ($status) {
				$seq = $self->{smpp}->submit_sm_resp(message_id=>'',seq => $pdu->{seq},status => $status);
			}
			else {
				$seq = $self->{smpp}->submit_sm_resp(message_id=>$id,seq => $pdu->{seq});
			}
		   $self->{log}->debug("SUBMIT_SM_RESP $seq sent.($status)");
		}, },
    0x80000004 => { cmd => 'submit_sm_resp', 
    	accept => {'run'=>[1,1,0,1,0], 'unbind'=>[1,1,0,1,0],}, reply => sub {
    		my ($self,$pdu) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
			#temp
			my $t0 = [gettimeofday];
			#temp

			$self->dec_wait_resp_count();
			my $id = $pdu->{seq} + $self->{seq_shift};
			my $sms = $self->{sent_buffer}{$id};
			## read sms from db if required
			delete $self->{sent_buffer}{$id};

			my $fm_error_rule = $self->exec_event('submit_resp',$self,$pdu,$sms);
			$fm_error_rule = $self->exec_event('on_submit_resp',$self,$pdu,$sms) || $fm_error_rule;
			my ($err,$msg_id) = ($pdu->{status},$pdu->{message_id});
			# ���������� ������� �������
			my $error_rule = $fm_error_rule || $self->get_error_rule($err);

			###########################################################
			#  ���������� ������� �������
			###########################################################
			if ($err==0) {
				$msg_id =~ s/^0+//;
				$msg_id = hex($msg_id) if ($self->{msg_id_to_dec});
				$log->info("OUTSMS:$id ACCEPTED (message_id=$msg_id)");
				$sms->{status} = 2;
				$sms->{err_num} = $err;
				$sms->{msg_id} = $msg_id;
				$db->set_sms_new_status($id,2,0,$err,$msg_id);
				$sms->{outbox_status} = $db->set_sms_delivered_status($id,$error_rule->{ignore_billing},$error_rule->{fixed_count});
				#$sms->{outbox_status} = $db->set_sms_delivered_status2($sms,$error_rule->{ignore_billing},$error_rule->{fixed_count});
				$self->outbox_status_changed($sms);
#				$log->info("OUTSMS:$id ACCEPTED (message_id=$msg_id)");
#				$log->debug("Error rule ".Dumper($error_rule));
				delete $self->{msg_try_count}{$id};
				#temp
				my $elapsed = tv_interval($t0);
				$log->debug("resp_proc_time=$elapsed");
				#temp
				return;
			}

			###########################################################
			#  ��������� ������
			###########################################################
			$log->warn("OUTSMS:$id REJECTED (status=$err) ".$self->get_status_text($err));

			if ($error_rule) {
				# msg_try_count - ���������� ��������� ������� �����������
				$self->{msg_try_count}{$id} = $db->get_value('select retry_count from tr_outsms where id=?',undef,$id)
					unless defined $self->{msg_try_count}{$id};
				my $next_retry = $self->{msg_try_count}{$id};
				$self->{msg_try_count}{$id}++;
				my $timeouts;
				my $limit = $error_rule->{retry};
				# ������� ��������
				if (defined($limit) and $next_retry > $limit) {

                                        $self->exec_event('retry_limit_reached', $self, $sms, $pdu);
					$sms->{status} = 0xff;
					$sms->{err_num} = $err;
					$sms->{msg_id} = $msg_id;
					$db->set_sms_new_status($id,0xff,0,$err,$msg_id);
					$sms->{outbox_status} = $db->set_sms_delivered_status($id,1);
					$self->outbox_status_changed($sms);

					# ����������� ����� ������ ����
					if ($sms->{params}{resend_smsc_id} and ($sms->{params}{resend_smsc_id} != $self->{id})) {
						my $new_id = $db->resend_through_smstraffic($sms->{outbox_id}, $sms->{params}{resend_smsc_id});
						$log->warn("OUTSMS:$id failed (outbox_id=$sms->{outbox_id}) was resent through $sms->{params}{resend_smsc_id} - new id is $new_id");
					} else {
                                            $log->debug("OUTSMS:$id Retry limit reached.");
					}

					delete $self->{msg_try_count}{$id};
					#temp
					my $elapsed = tv_interval($t0);
					$log->debug("resp_proc_time=$elapsed");
					#temp
					return;
				# ������ ������
				} elsif ($next_retry == 1) {
					$timeouts = $error_rule->{first};
				# ����������� �������
				} else {
					$timeouts = $error_rule->{other};
				}

				# ���������� ��������� �����������
				my ( $wait, $sleep, $block );
				if (ref $timeouts) {
					( $wait, $sleep, $block ) = @$timeouts;
				} else {
					( $wait, $sleep, $block ) = ( $timeouts, 0, 0);
				}

				# ��������/��������� ��������
				if (defined($wait)) {
					$db->set_sms_new_status($id,0,$wait,$err,$msg_id);
					$log->info("OUTSMS:$id Will try to resend in $wait seconds. Retry $next_retry\/$limit.");
				}
				# ������������ �������� ���� ���������
				if (defined($sleep) and $sleep > 0) {
					$self->{sleep} = time() + $sleep;
					# return to buffer if not wait
					$self->{buffer}{$id} = $sms unless (defined($wait));
					$log->info("Sleeping $sleep seconds.");
				}
				# ���������� ��������
				if (defined($block) and $block > 0) {
					my $ab = $db->get_value('select abonent from tr_outsms where id=?',undef,$id);
					$self->{blocked_abonents}->{$ab} = time()+$block;
					$log->info("Abonent $ab blocked for $block seconds.");
				}

				#temp
				my $elapsed = tv_interval($t0);
				$log->debug("resp_proc_time=$elapsed");
				#temp
				return;
			} else {
				# �� ������� ������� �������
				$log->error("OUTSMS:$id REJECTED. ".$self->get_status_text($err)."\n".Dumper($pdu));
				$self->warning("Error handling incorrect setup while sending SMS$id");
				$sms->{status} = 0xff;
				$sms->{err_num} = $err;
				$sms->{msg_id} = $msg_id;
				$db->set_sms_new_status($id,0xff,0,$err,$msg_id);
				$sms->{outbox_status} = $db->set_sms_delivered_status($id,1);
				$self->outbox_status_changed($sms);

				#temp
				my $elapsed = tv_interval($t0);
				$log->debug("resp_proc_time=$elapsed");
				#temp
				return;
			}

		}, },
    0x00000005 => { cmd => 'deliver_sm',
    	accept => {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],}, reply => sub {
    		my ($self,$pdu) = @_;
    		$self->{last_received_sms_time} = time();
			#temp
			my $t0 = [gettimeofday];
			#temp
    		$self->process_data_pdu($pdu);
    		die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
    		my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq});
			#temp
			my $elapsed = tv_interval($t0);
			#temp
			$self->{log}->debug("DELIVER_SM_RESP $seq sent in $elapsed");
		}, },
    0x80000005 => { cmd => 'deliver_sm_resp',
    	accept => {'run'=>[1,0,0,0,1], 'unbind'=>1,}, reply => sub {
    		my ($self,$pdu) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
			#temp
			my $t0 = [gettimeofday];
			#temp
			my $id = $pdu->{seq} + $self->{seq_shift};
			my ($err,$msg_id) = ($pdu->{status},hex($pdu->{message_id}));
			my $sms = $self->{sent_buffer}{$id};
			## read sms from db if required
			delete $self->{sent_buffer}{$id};
			
			if ($err==0) {
				$db->set_sms_new_status($id,2,0,$err,$msg_id);
				$db->set_sms_delivered_status($id);
				$log->info("OUTSMS:$id ACCEPTED.");
			}
			elsif ($err==0x08) {
				$log->warn("OUTSMS:$id REJECTED ".$self->get_status_text($err)." Will retry in 10 seconds.");
				$db->set_sms_new_status($id,0,10,$err,$msg_id);
			}
			else {
				$log->error("OUTSMS:$id REJECTED ".$self->get_status_text($err)."\n".Dumper($pdu));
				$db->set_sms_new_status($id,0xff,0,$err,$msg_id);
				$db->set_sms_delivered_status($id,1);
			}
			$self->dec_wait_resp_count();
			#temp
			my $elapsed = tv_interval($t0);
			$log->debug("resp_proc_time=$elapsed");
			#temp
		}, },
    0x00000006 => { cmd => 'unbind', 
    	accept => {'run'=>[1,1,1,1,1],}, reply => sub {
    		my ($self, $pdu) = @_;
			my $seq = $self->{smpp}->unbind_resp(seq => $pdu->{seq});
			$self->{log}->debug("UNBIND_RESP $seq sent.");
			$self->{select}->remove($self->{smpp});
    		$self->{smpp}->close();
    		undef $self->{smpp};
    		$self->{terminated} = time();
			$self->{log}->info('Unbound successfully.');
		}, },
    0x80000006 => { cmd => 'unbind_resp', 
    	accept => {'unbind'=>[1,1,1,1,0],}, reply => sub {
    		my ($self, $pdu) = @_;
			$self->{select}->remove($self->{smpp});
    		$self->{smpp}->close();
    		undef $self->{smpp};
    		$self->{log}->info("Unbound from $self->{host}:$self->{port} successfully.");
    		$self->enter_state('stopped');
		}, },
    0x00000015 => { cmd => 'enquire_link',
    	accept => {'run'=>[1,1,1,1,1],'unbind'=>[1,1,1,1,1]}, reply => sub {
    		my ($self, $pdu) = @_;
			my $seq = $self->{smpp}->enquire_link_resp(seq => $pdu->{seq});
			$self->{log}->debug("ENQUIRE_LINK_RESP $seq sent.");
	    }, },
    0x80000015 => { cmd => 'enquire_link_resp',
    	accept => {'run'=>[1,1,1,1,1], 'unbind'=>[1,1,1,1,1],}, reply => sub {
			my ($self, $pdu) = @_;
			$self->{last_elink_time} = 0xFFFF0000;
			$self->dec_wait_resp_count();
		}, },
    0x00000103 => { cmd => 'data_sm',
    	accept => {'run'=>[1,0,1,1,1],}, reply => sub {
    		my ($self,$pdu) = @_;
    		$self->process_data_pdu($pdu);
    		my $seq = $self->{smpp}->data_sm_resp(message_id => '',seq => $pdu->{seq});
			$self->{log}->debug("DATA_SM_RESP $seq sent.");
		}, },
    0x80000103 => { cmd => 'data_sm_resp',
    	accept => {'run'=>[1,1,0,1,0], 'unbind'=>[1,1,0,1,0],}, reply => sub {
    	    my ($self,$pdu) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
			#temp
			my $t0 = [gettimeofday];
			#temp

			my ($id,$err,$msg_id) = ($pdu->{seq},$pdu->{status},$pdu->{message_id});
			$self->dec_wait_resp_count();
			if ($err==0) {
				$db->set_sms_new_status($id,2,0,$err,$msg_id);
				$db->set_sms_delivered_status($id);
				$log->info("OUTSMS:$id ACCEPTED (message_id=$msg_id)");
			} else {
				$log->error("OUTSMS:$id REJECTED ".$self->get_status_text($err)."\n".Dumper($pdu));
				$db->set_sms_new_status($id,0xff,0,$err,$msg_id);
				$db->set_sms_delivered_status($id,1);
			}

			#temp
			my $elapsed = tv_interval($t0);
			$log->debug("resp_proc_time=$elapsed");
			#temp
			return;
		}, },
    0x80000000 => { cmd => 'generic_nack',
    	accept => {'run'=>[1,1,1,1,0],}, reply => sub {
    		return;
		}, },
    0x00000003 => { cmd => 'query_sm', accept => undef, reply => undef, },
    0x80000003 => { cmd => 'query_sm_resp', accept => undef, reply => undef, },
    0x00000007 => { cmd => 'replace_sm', accept => undef, reply => undef, },
    0x80000007 => { cmd => 'replace_sm_resp', accept => undef, reply => undef, },
    0x00000008 => { cmd => 'cancel_sm', accept => undef, reply => undef, },
    0x80000008 => { cmd => 'cancel_sm_resp', accept => undef, reply => undef, },
    0x0000000b => { cmd => 'outbind', accept => undef, reply => undef, },
    0x00000021 => { cmd => 'submit_multi', accept => undef, reply => undef, },
    0x80000021 => { cmd => 'submit_multi_resp', accept => undef, reply => undef, },
    0x00000102 => { cmd => 'alert_notification', accept => undef, reply => undef, },
);

# �����������
sub new {
	my ($class,$smsc_id,$debug_level,$smpp_mode) = @_;

	Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
	my $smpp_mode_name = {Net::SMPP::Transmitter => 'snd',
						  Net::SMPP::Receiver => 'rcv',
						  Net::SMPP::Transceiver => 'trn',
						  4 => 'smsc'
						 }->{$smpp_mode};
	die("FATAL_ERROR: unknown mode $smpp_mode") unless (defined $smpp_mode_name);

	#  Logging preparations
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);
	my %log_conf = (
					"log4perl.rootLogger" => "$debug_level, Logfile",
					"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
					"log4perl.appender.Logfile.recreate" => 1,
					"log4perl.appender.Logfile.recreate_check_interval" => 300,
					"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
					"log4perl.appender.Logfile.umask" => "0000",
					"log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
					"log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
					"log4perl.appender.Logfile.layout.ConversionPattern" =>
#					POSIX::strftime('v%m.%d%H ',localtime((stat($INC{'Disp/SMPP_SMSClient.pm'}))[9])).
					Config->param('log_pattern','sms_client'));
	Log::Log4perl->init( \%log_conf);
	my $log=get_logger("");
	$log->info("Starting...");

	#  Connecting to DB
	my $db = new Disp::AltDB();
	$db->load_statistics($pr_name);
	my $cnf = $db->get_smsc_configuration($smsc_id);
	die('FATAL_ERROR:Not supported SMPP version: $cnf->{smpp_version}.')
	  unless ($cnf->{smpp_version} == 0x34);
	my $self = {db => $db,log => $log, pr_name => $pr_name,
				smpp_mode_name => $smpp_mode_name,state=>'stopped',
				last_elink_time=>0xFFFF0000,
				wait_resp_count=>0,sleep=>0,status=>'start',
				smsc_id=>$smsc_id,debug_level=>$debug_level,smpp_mode=>$smpp_mode,
				msg_try_count => { }, abonent_msg_speed => { },
				last_throttle_count => 0, last_throttle_time => 0,
				blocked_abonents => {},
				routines_event_delay => 0,
				buffer => {},
				sent_buffer => {},
				fetch_limit => 100,
				window_limit => 100,
				seq_shift => Config->param('seq_shift','sms_client') || 0,
			};
	@{$self}{keys(%$cnf)} = values(%$cnf);
	$self->{window_limit} = 1 if ($self->{sync});

	if ($self->{func_module}) {
		eval "require $self->{func_module}";
		if ($@) {
			yell($@,code => 'WARNING',
				 process => $self->{pr_name},
				 header => "$self->{pr_name}: Can't load $self->{func_module}",
				 ignore => { log4perl => 1 });
			$self->{func_module} = undef;
		}
		else {
			$log->info("Module $self->{func_module} loaded.");
		}
	}

	$self->{error_tab} = $error_tab{$self->{smsc_id}} || $error_tab{_default_};

	bless $self, $class;
	$self->exec_event('on_init',$self);
	return $self;
}

sub run {
	my $self = shift;
	my $skip_start = shift;
	my $retval = 1;
	unless ($skip_start) {
		if ($self->{smpp_mode} == 4) {
			$retval = $self->run_smsc();
		}
		else {
			$retval = $self->run_client();
		}
		$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
		$self->enter_state('stopped');
	} else {
		$retval = $self->run_fake();
	}
	$self->{log}->info("Stopped.\n\n\n");
	return $retval;
}

sub run_client {
	my $self = shift;

	eval {
		$self->enter_state('connect');
		my @hosts = split (/\//,$self->{host});
		my $smpp;
		foreach my $host (@hosts) {
			my %connect_params = (
								  smpp_version => $self->{smpp_version},
								  interface_version => $self->{interface_version},
								  system_id => $self->{system_id},
								  password => $self->{password},
								  addr_ton => $self->{addr_ton},
								  addr_npi => $self->{addr_npi},
								  system_type => $self->{system_type},
								  address_range => $self->{addr_range},
								  port => $self->{port},
								  async => 1,
								  timeout => 0);
			my $via;
			if ($self->{local_addr}) {
				$connect_params{LocalAddr} = $self->{local_addr};
				$via = "via $self->{local_addr}";
			}
			$self->{log}->debug("Connecting to $host:$self->{port} $via...");
			$smpp = Net::SMPP->new_connect($host,%connect_params);
			if ($smpp) { $self->{host} = $host; last; };
		}
		$self->{db}->reconnect();
		die("CONNECT_ERROR:Can't connect to $self->{host}:$self->{port}.")
		  unless ($smpp);
		$self->{log}->info("Connected to $self->{host}:$self->{port}.");
		$self->{smpp} = $smpp;
		$self->{select} = IO::Select->new($smpp);

		$self->send_bind();
  		die ("CONNECT_ERROR:Can't bind to $self->{host}:$self->{port}.")
		  unless (($self->receive(60)) and ($self->{state} eq 'run'));

		$self->{db}->set_outsms_all_delivered($self->{smsc_id});
		$self->{last_rcv_time} = time();
		do {
			if ($self->receive(1)) {
				$self->{last_rcv_time} = time();
			}

			# Removes MTS bug
			if ($self->{sync} and ($self->{wait_resp_count} > 0) and (time() > ($self->{last_snd_time}+30))) {
				$self->set_wait_resp_count(0);
			}

		 	if ( $self->can_send_msgs() ) {
		 		$self->send_messages();
		 	}
		 	else {
		 		$self->{db}->ping();
		 	}

			$self->send_elink() if ($self->can_send_elink());
			die "CONNECT_ERROR:ENQUIRE_LINK_RESP hasn't been received. Disconnecting..."
			  if ($self->{last_elink_time}+60 < time());
			die "CONNECT_ERROR:RESP hasn't been received. Disconnecting..."
			  if ($self->{sync} and ($self->{last_rcv_time}+$self->{elink_timeout}+10 < time()));

			$self->heartbeat;
			$self->routines;
		} until ($self->{terminated} and (!$self->{wait_resp_count} or ($self->{terminated}+10 < time())));
		if (defined($self->{smpp})) {
			$self->{log}->info('Stopping...');
			$self->send_unbind();
	  		die ("CONNECT_ERROR_:Unbound from $self->{host}:$self->{port} unsuccessfully.") 
			  unless ($self->receive(10));
		}
	};
	if ($@) {
		$self->fatal_error($@);
		if ($self->{smpp}) {
			$self->{select}->remove($self->{smpp});
  			$self->{smpp}->close();
  			undef $self->{smpp};
  		}
		$self->exec_event('on_stop', $self);
  		return 1;
	}
	$self->exec_event('on_stop', $self);
	return 0;
}

sub run_smsc {
	my $self = shift;
  	$self->{log}->info('Started.');

	eval {
		while (!$self->{terminated}){
			my $c = Net::SMPP->new_listen($self->{host} || undef,
										  smpp_version => $self->{smpp_version},
										  interface_version => $self->{interface_version},
										  addr_ton => $self->{addr_ton},
										  addr_npi => $self->{addr_npi},
										  source_addr_ton => $self->{source_addr_ton},
										  source_addr_npi => $self->{source_addr_npi},
										  dest_addr_ton => $self->{dest_addr_ton},
										  dest_addr_npi => $self->{dest_addr_npi},
										  system_type => $self->{system_type},
										  port => $self->{port},
										  facilities_mask => 0x00010003,
										  async => 1 )
			  or die("CONNECT_ERROR_:Can't create server: $!");

			$self->enter_state('listen');
			my $smpp;
			while (!$self->{terminated}){
				$self->heartbeat;
				$self->{db}->ping() or die('FATAL_ERROR:Database ping failed.');
				$smpp = $c->accept;
				last if (defined $smpp);
			}
			$c->close();
			$self->enter_state('connect');
			$self->{smpp} = $smpp;
			$self->{select} = IO::Select->new($smpp);
			$self->{log}->info('Connected.');
			$self->{last_rcv_time} = time();
			$self->set_wait_resp_count(0);
			do {
				$self->send_messages() if ( $self->can_send_msgs() );

				if ($self->receive(1)) {
					$self->{last_rcv_time} = time();
				}

				$self->send_elink() 
				  if ($self->can_send_elink());
				die "CONNECT_ERROR:ENQUIRE_LINK_RESP hasn't been received. Disconnecting..."
				  if ($self->{last_elink_time}+60 < time());
				die "CONNECT_ERROR:BIND_TRANCIEVER hasn't been received during last 30 seconds. Disconnecting..."
				  if (($self->{state} eq 'connect') and ($self->{last_rcv_time}+30 < time()));
				$self->heartbeat;
				$self->routines;
			} until (($self->{terminated} and !$self->{wait_resp_count}) or (!defined($self->{smpp})));
		}
	};
	$self->exec_event('on_stop', $self);
	if ($@) {
		$self->fatal_error($@);
		return 1;
	}
	return 0;
}

sub run_fake {
	my $self = shift;
	$self->exec_event('run_fake', $self);
	# [2012/01/24 11:59:46,408] [10805] WARN OUTSMS: DR message not found message_id=33BFE801 (status=2)
	#while (<STDIN>) {
	#	if (/^\[(\d+)\/(\d+)\/(\d+)\s(\d+)\:(\d+)\:(\d+)\,.*DR message not found message_id=([\d\w]+)\s\(status=([\d\w]+)\)$/) {
	#		my $rep_date = "$1-$2-$3 $4:$5:$6";
	#		$self->process_dr({},$7,$8,$rep_date);
	#	}
	#}
	return 0;
}

sub send_bind {
	my $self = shift;
	$self->enter_state('bind');
	if ($self->{smpp_mode}==Net::SMPP::Transmitter) {
		$self->{bind_seq} = $self->{smpp}->bind_transmitter(); 
		$self->{log}->debug("BIND_TRANSMITTER $self->{bind_seq} sent.");
	}
	elsif ($self->{smpp_mode}==Net::SMPP::Receiver) {
		$self->{bind_seq} = $self->{smpp}->bind_receiver(); 
		$self->{log}->debug("BIND_RECEIVER $self->{bind_seq} sent.");
	}
	elsif ($self->{smpp_mode}==Net::SMPP::Transceiver) {
		$self->{bind_seq} = $self->{smpp}->bind_transceiver(); 
		$self->{log}->debug("BIND_TRANSCEIVER $self->{bind_seq} sent.");
	}
}

sub can_send_elink {
	my $self = shift;
	return ( ((time() - $self->{last_rcv_time}) > $self->{elink_timeout}) and # �� ���� ����������
			 (($self->{wait_resp_count}==0) or (!$self->{sync})) and # �� ���� ������ � ����. ������
			 ($self->{last_elink_time}==0xFFFF0000) and # �� ��� ������ �����
			 ($self->{state} eq 'run') ); #
}

sub send_elink {
	my $self = shift;
	my $seq = $self->{smpp}->enquire_link();
	$self->{last_snd_time} = time();
  	$self->{last_elink_time} = time();
	$self->{log}->debug("ENQUIRE_LINK $seq sent.");
	$self->inc_wait_resp_count();
}

sub send_unbind {
	my $self = shift;
	$self->enter_state('unbind');
	my $seq = $self->{smpp}->unbind()
	  if (defined($self->{smpp}));
	$self->{log}->debug("UNBIND $seq sent.");
}

sub can_send_msgs {
	my $self = shift;
	return ( (($self->{smpp_mode} & Net::SMPP::Transmitter) or 
			  ($self->{smpp_mode} == 4)) and # �������� � ������ MODE
			 (!$self->{terminated}) and # �� ����������
			 ($self->window_check) and
			 (!$self->{sync} or ($self->{wait_resp_count}==0)) and # �� ���� ����� � ���������� ������
			 (time() > $self->{sleep}) and # �� ���� ��-�� ������������ �������� ����
			 ($self->{state} eq 'run') );
}

sub throttle_send_limit {
	my ($self) = @_;
	my ($tr, $time);
	return 1000 unless $tr = $self->{output_throttle};
	$self->update_throttle;
	my $count = $tr - $self->{last_throttle_count};
	return 0
	  unless $count > 0;
	return $count;
}

sub update_throttle {
	my ($self, $msg_count) = @_;
	my $time = time;
	if ($time != $self->{last_throttle_time}) {
		$self->{last_throttle_count} = 0;
		$self->{last_throttle_time} = $time;
	}
	$self->{last_throttle_count} += $msg_count
	  if $msg_count;
}

sub get_dest_addr {
	my ($self, $sms ) = @_;
	if ($self->{smpp_mode} == 4) {
		return $sms->{num}.'#'.$sms->{id}
		  if ($self->{pass_transaction_id}==2);
		return $sms->{num};
	}
	else {
		return $sms->{abonent};
	}
}

sub get_src_addr {
	my ($self, $sms ) = @_;
	if ($self->{smpp_mode} == 4) {
		return $sms->{abonent};
	}
	else {
		return $sms->{num}.'#'.$sms->{transaction_id} if (($sms->{transaction_id}));
		return $sms->{num};
	}
}


sub prepare_pdu_to_send {
	my ($self, $sms ) = @_;
	my $db = $self->{db};

	die "ESME can't send Delivery Receipt."
	  if ((($sms->{esm_class}&0x3C)==0x04) and ($self->{smpp_mode}!=4));

	my $cmd = ($self->{smpp_mode} == 4) ? Net::SMPP::CMD_deliver_sm : Net::SMPP::CMD_submit_sm;
	my %pdu = (priority_flag => 1,
			   protocol_id => 0x00,
			   source_addr => $self->get_src_addr($sms),
			   destination_addr => $self->get_dest_addr($sms),
			   data_coding => $sms->{data_coding},
			   source_addr_ton => $self->{src_addr_ton},
			   source_addr_npi => $self->{src_addr_npi},
			   dest_addr_ton => $self->{dest_addr_ton},
			   dest_addr_npi => $self->{dest_addr_npi},
			   seq => $sms->{id} - $self->{seq_shift},
			   short_message => $sms->{msg},
			   esm_class => $sms->{esm_class},
			   registered_delivery => $self->{registered_delivery});
	$pdu{validity_period} = '000001000000000R' if ($self->{smsc_id} == 6);
	$pdu{validity_period} = '000001000000000R' if ($self->{smsc_id} == 54);
	if ($sms->{num} !~ /^[\d\#]+$/) {
		$pdu{source_addr_ton} = 5;
		$pdu{source_addr_npi} = 0;
	}
	if (length($sms->{msg}) > 254) {
		$pdu{short_message} = '';
		$pdu{message_payload} = $sms->{msg};
	}
	if ($sms->{sar_msg_ref_num}) {
		$pdu{sar_msg_ref_num} = pack('S',$sms->{sar_msg_ref_num} || '');
		$pdu{sar_total_segments} = pack('C',$sms->{sar_total_segments} || '');
		$pdu{sar_segment_seqnum} = pack('C',$sms->{sar_segment_seqnum} || '');
	}

	if ($self->{smpp_mode} == 4) {
		if ($self->{pass_transaction_id}==1) {
			$pdu{20481} = pack('L',$sms->{id});
		}
		if ($self->{pass_operator} or $self->{pass_parts_count}) {
			my ($operator_id,$transport_count) = 
   			$self->{db}->{db}->selectrow_array('select operator_id,transport_count from tr_inboxa where id=?',undef,$sms->{inbox_id});
			$pdu{20482} = pack('S',$operator_id) if ($self->{pass_operator});
			$pdu{20483} = pack('C',$transport_count) if ($self->{pass_parts_count});
		}
		if ($sms->{params}{dr}) {
			$pdu{esm_class} = 0x04;
			$pdu{data_coding} = 0;
			$pdu{registered_delivery} = 0;
			$pdu{receipted_message_id} = $sms->{params}{dr}{message_id};
			$pdu{message_state} = pack('C',$sms->{params}{dr}{message_state});
		}
	}

	return ($cmd, \%pdu);
}

sub send_messages {
	my $self = shift;
	my ($db,$log) = ($self->{db},$self->{log});
	my $limit = $self->throttle_send_limit;

	my $msgs = $self->{buffer};
	my $msg_count = scalar keys(%$msgs);
	if ($msg_count < $limit) {
		$msgs = $db->get_sms_ready_to_send($self->{smsc_id}, $self->{fetch_limit});
		$msg_count = scalar keys(%$msgs);
		$db->move_low_priority_to_outsms($self->{smsc_id}, $self->{fetch_limit})
			if ($msg_count < $self->{fetch_limit});
	}
	return 0 if (!$msg_count);
	$log->debug("$msg_count message(s) fetched.");
	my $sended_count = 0;
	my $window_limit = $self->window_check;
#	for (sort {$a <=> $b} keys(%$msgs)) {
#	for (sort { return -1 if (!$msgs->{$a}{push_id} and $msgs->{$b}{push_id});
#				return  1 if ($msgs->{$a}{push_id} and !$msgs->{$b}{push_id});
#				return $a <=> $b; } keys(%$msgs)) {
#	for (sort { return 1 if (!$msgs->{$a}{inbox_id} and $msgs->{$b}{inbox_id});
#			return  -1 if ($msgs->{$a}{inbox_id} and !$msgs->{$b}{inbox_id});
#			return $a <=> $b; } keys(%$msgs)) {
	for (
		sort {
			return 1 if ( $msgs->{$a}{err_num} == 0x14 and $msgs->{$b}{err_num} != 0x14 ); # MQF error is last
			return -1 if ( $msgs->{$a}{err_num} != 0x14 and $msgs->{$b}{err_num} == 0x14 ); # MQF error is last
			return 1 if ( !$msgs->{$a}{inbox_id} and $msgs->{$b}{inbox_id} );
			return -1 if ( $msgs->{$a}{inbox_id} and !$msgs->{$b}{inbox_id} );
			return $a <=> $b; 
		} keys(%$msgs)
	) {
#		last if ($sended_count==$limit);
		last unless $self->throttle_send_limit;
		last if ($window_limit <= $sended_count);
		my $sms = $msgs->{$_};
		delete $msgs->{$_};

		my $block_time = $self->{blocked_abonents}->{$sms->{abonent}} || 0;
		if ($block_time > time()) {
			$db->set_sms_new_status($sms->{id},0,$block_time - time(),0xffffff,0);
			next;
		}

		my $error_rule = $self->get_error_rule('_no_resp_');
		my $resp_delay = $error_rule->{first};
		if ($sms->{status} == 1) {
			$log->warn("OUTSMS:$sms->{id} REJECTED (no resp).");
			$self->dec_wait_resp_count();
			$resp_delay = $error_rule->{other};
			if ($sms->{retry_count} > $error_rule->{retry}) {
				$self->exec_event('retry_limit_reached', $self, $sms, undef);
				$sms->{status} = 0xff;
				$sms->{err_num} = 0xffffff;
				$sms->{msg_id} = 0;
				$db->set_sms_new_status($sms->{id},0xff,0,0xffffff,0);
				$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},1);
				$self->outbox_status_changed($sms);
				delete $self->{sent_buffer}{$sms->{id}};
				$log->warn("OUTSMS:$sms->{id} Retry limit reached.");
				next;
			}
		}

		my $last_time = $self->{sended_abonents}->{$sms->{abonent}} || 0;
		### For IVANOVO
		next if (($self->{smsc_id}==85) and (time() - $last_time) < 30);
		my ($cmd, $pdu) = $self->prepare_pdu_to_send($sms);
		my $new_cmd = $self->exec_event('before_send',$self,$cmd,$sms,$pdu);
		$cmd = $new_cmd
		  if (defined($new_cmd));
		$new_cmd = $self->exec_event('on_send_sm',$self,$cmd,$sms,$pdu);
		$cmd = $new_cmd
		  if (defined($new_cmd));

		# For testing only
		if ( $self->{smsc_id} == 56 ) {
			$pdu->{0xDEAD} = pack "c", int(rand(4));
		}

		if ($self->{pass_inbox_id}) {
			$self->{log}->debug("Passing inbox_id by SMPP: $sms->{inbox_id}");
			$pdu->{0x2a3b} = $sms->{inbox_id};
		}

		if (!$cmd) {
			$log->debug("OUTSMS:$sms->{id} SKIPPED (src=$sms->{num} dest=$sms->{abonent} link=$sms->{link_id} tr_id=$sms->{transaction_id})");
			next;
		}
		if ($sms->{test_flag}==2) {
			$log->debug("TEST PDU omitted:\n".Dumper($pdu));
			$sms->{status} = 2;
			$sms->{err_num} = 0;
			$sms->{msg_id} = 0;
        		$db->set_sms_new_status($sms->{id},2,0,0,0);
	        	$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},1);
			$self->outbox_status_changed($sms);
			$log->info("OUTSMS:$sms->{id} OMITTED (src=$sms->{num} dest= $sms->{abonent} link=$sms->{link_id} tr_id=$sms->{transaction_id})");
		} else {
			if ($sms->{pdu}) {
				$self->{smpp}->req_backend($cmd, $sms->{pdu}, undef, seq => ($sms->{id} - $self->{seq_shift}));
				$log->info("SMS$sms->{id} was sent. Data(HEX):".unpack('H*',$sms->{pdu}));
			} else {
				$log->debug("PDU($cmd) Sent:\n".Dumper($pdu));
				if ($cmd == Net::SMPP::CMD_submit_sm) {$self->{smpp}->submit_sm(%$pdu);}
				elsif ($cmd == Net::SMPP::CMD_deliver_sm) {$self->{smpp}->deliver_sm(%$pdu);}
				elsif ($cmd == Net::SMPP::CMD_data_sm) {$self->{smpp}->data_sm(%$pdu);}
				else {die 'Wrong CMD';}
				$log->info("OUTSMS:$sms->{id} SENT (src=$sms->{num} dest=$sms->{abonent} link=$sms->{link_id} tr_id=$sms->{transaction_id})");
			}
			$db->set_sms_new_status($sms->{id},1,$resp_delay,0,0,1);
			$self->inc_wait_resp_count();
			$self->update_throttle(1);
			$self->{last_snd_time} = time();
		}
		$self->{sent_buffer}{$sms->{id}} = $sms;
		$sended_count++;
		$self->{sended_abonents}->{$sms->{abonent}} = time();
		last if ($self->{sync} or $self->{terminated});
	}
	$log->debug("$sended_count message(s) sent. Limit=$limit");
	return $sended_count;
}

sub receive {
	my ($self,$wait) = @_;
	my ($db,$log) = ($self->{db},$self->{log});
	my $i = 0;
	if ($self->{select}->can_read($wait)) {
		$log->debug('There is something in socket to read.');
		while ($self->{select}->can_read(0) and ($i < 100)) {
			my $pdu = $self->{smpp}->read_pdu();
			unless (defined($pdu)) {
				my $smpperror = ${*{$self->{smpp}}}{smpperror};
				my $smpperrorcode = ${*{$self->{smpp}}}{smpperrorcode};
				$self->{select}->remove($self->{smpp});
				$self->{smpp}->close();
				if (($smpperrorcode==1)or($smpperrorcode==2)) {
					die("CONNECT_ERROR:Can't read PDU. $smpperror");
				}
				else {
					die("CONNECT_ERROR_:Can't read PDU. $smpperror");
				}
			}
			$i++;
			$self->debug_pdu($pdu);
			$self->exec_event('on_receive_pdu',$self,$pdu);
			if (defined $process_tab{$pdu->{cmd}}{reply}) {
				if ($process_tab{$pdu->{cmd}}{accept}{$self->{state}}[$self->{smpp_mode}]) {
					&{$process_tab{$pdu->{cmd}}{reply}}($self, $pdu);
				}
				else {
					$self->warning(sprintf("Command not allowed in ".uc($self->{smpp_mode_name})." mode ".uc($self->{state})." state, command_id=0x%x",$pdu->{cmd}),Dumper($pdu));
				}
			}
			else {
				$self->warning(sprintf("Don't know what to do, command_id=0x%x",$pdu->{cmd}),Dumper($pdu));
			}
		}
	}
	return $i;
}

sub process_data_pdu {
	my ($self,$pdu) = @_;
	my ($db,$log) = ($self->{db},$self->{log});
	$log->debug("Starting PDU processing."); ####

	unless (($pdu->{cmd}==5)and(($pdu->{esm_class}&0x3C)==0x04)) {
		if (!$pdu->{source_addr}) {
			$self->warning("Source address is absent. PDU ignored.",Dumper($pdu));
			return 0, 0xA;
		}
		if (!$pdu->{destination_addr}) {
			$self->warning("Destination address is absent. PDU ignored.",Dumper($pdu));
			return 0, 0xB;
		}
	}

	#+binarin
	# XXX Does we need it in other client types?
	if ($self->{abonent_speed_limit} and $self->{abonent_speed_limit} > 0) {
		if (defined $self->{abonent_msg_speed}{$pdu->{source_addr}}) {
			my $entry = $self->{abonent_msg_speed}{$pdu->{source_addr}};
			my $time = time;
			my $time_diff = $time - $entry->[1];
			my $hour_complement = 3600.0 - $time_diff;
			$hour_complement = 0.0 if $hour_complement < 0;

			$entry->[0] *= $hour_complement / 3600.0;
			$entry->[0] += 1;
			$entry->[1] = $time;

			$log->debug("Abonent $pdu->{source_addr} has speed $entry->[0]");

#		   if ( $entry->[0] > $self->{abonent_speed_limit} ) {
#			   my $id = 0; # XXXX $db->insert_sms_into_insms_ignored($sms);
#			   $self->warning("Speed limit reached for abonent $pdu->{source_addr}: $entry->[0] of $self->{abonent_speed_limit}\nStored ignored SMS as $id");
#			   return 0;
#		   }

		} else {
			$self->{abonent_msg_speed}{$pdu->{source_addr}} = [ 0, time ];
		}
	}
	#+binarin

	my $sms = {};
	$sms->{smsc_id} = $self->{smsc_id};
	$sms->{link_id} = $self->{default_link_id} if ($self->{default_link_id});  # delete?
	$sms->{num} = $pdu->{destination_addr};
	$sms->{abonent} = $pdu->{source_addr};
	$sms->{msg} = $pdu->{short_message};
	$sms->{data_coding} = $pdu->{data_coding};
	$sms->{esm_class} = $pdu->{esm_class} & 0xFC;
	$sms->{pdu} = $pdu->{data};
	($sms->{num},$sms->{abonent}) = ($sms->{abonent},$sms->{num}) if ($self->{smpp_mode}==4);
	($sms->{num},$sms->{transaction_id}) = split(/#/,$sms->{num}) if ($self->{smpp_mode}!=4);

	$sms->{msg} = $pdu->{message_payload} if (length($sms->{msg})==0);
	$sms->{ussd_service_op} = unpack('C',$pdu->{ussd_service_op} || '');
#	$sms->{ussd_service_op} = (($sms->{msg}=~/RENEWSESSION/)?1:18) if ($self->{ussd_type}==4);
	$sms->{sar_msg_ref_num} = unpack('S',$pdu->{sar_msg_ref_num} || '');
	$sms->{sar_total_segments} = unpack('C',$pdu->{sar_total_segments} || '');
	$sms->{sar_segment_seqnum} = unpack('C',$pdu->{sar_segment_seqnum} || '');
	$sms->{user_message_reference} = unpack('S',$pdu->{user_message_reference} || '');
#	$sms->{transaction_id} = $sms->{user_message_reference} || undef if ($self->{ussd_type}==4);
	$sms->{link_id} = $pdu->{5122} if ($pdu->{5122});

	my $ignore = $self->exec_event('after_receive',$self,$pdu,$sms);
#	my $ignore = 0;
#	$ignore = $self->{func_module}->after_receive($self,$pdu,$sms) if ($self->{func_module});

	if ($self->{smpp_mode} == 4) {
		if ($self->{pass_transaction_id}) {
			my $outsms_id = 0;
			if ($self->{pass_transaction_id} == 1) {
				$outsms_id = unpack('L',$pdu->{20481});
			}
			elsif ($self->{pass_transaction_id} == 2) {
				($sms->{num},$outsms_id) = split(/#/,$sms->{num});
			}
			my ($delivered_date,$abonent,$num,$transaction_id) = 
			  $self->{db}->{db}->selectrow_array('select unix_timestamp(delivered_date),abonent,num,transaction_id from tr_outsmsa where id=?',undef,$outsms_id);
  			return (1332,undef) if ($sms->{abonent} != $abonent);
  			return (1332,undef) if ($sms->{num} != $num);
  			return (1332,undef) if ($self->{tr_output_period} and ((time() - $delivered_date) > $self->{tr_output_period}));
			$sms->{transaction_id} = $transaction_id || undef;
		}
		else {
			($sms->{num},$sms->{transaction_id}) = split(/#/,$sms->{num});
		}
	}

	if ((($pdu->{cmd}==5)or($pdu->{cmd}==259))and(($sms->{esm_class}&0x3C)==0x04)) {
		# ��������� Delivery Report
		my ($rep_stat,$rep_date,$msg_id);
		if ($sms->{msg} =~ m/^id:(\S+)\s.*done date:(\d+) stat:(\w+)/) {
			$rep_stat = $3;
			$msg_id = $1;
			$rep_date = "20$1-$2-$3 $4:$5" if ($2 =~ /^(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/);
		}
		my $receipted_message_id = $pdu->{receipted_message_id};
		$receipted_message_id = hex($pdu->{receipted_message_id})
		  if ($self->{msg_id_to_dec});
		$msg_id = $receipted_message_id || $msg_id;
		$msg_id =~ s/\x00//g;
		$msg_id =~ s/^0+//;
		my $msg_state = unpack('C',$pdu->{message_state});
		my $stat_text = $msg_state || $rep_stat;
		$log->debug("Delivery report received. message_id='$msg_id'");
		if ($msg_id) {
			$self->process_dr($pdu,$msg_id,$stat_text,$rep_date);
		}
	}
	elsif (($pdu->{cmd}==5)and($sms->{esm_class}>0x04)and($sms->{esm_class}<0x40)) {
		$self->warning('Wrong esm_class in PDU',Dumper($pdu));
		my $id = $db->insert_sms_into_insms($sms);
		$self->{last_received_sms_time} = time();
		$log->info("INSMS:$id RECEIVED (src=$sms->{abonent} dest$sms->{num} link=$sms->{link_id} tr_id=$sms->{transaction_id})");
	}
	else {
		$ignore = $self->exec_event('on_receive_sm',$self,$pdu,$sms) || $ignore;
		$log->debug("Starting insertion of PDU into DB."); ####
		if ($ignore) {
			my $id = $db->insert_sms_into_insms_ignored($sms);
			my $reason = ($sms->{ignore_reason}) ? "reason=$sms->{ignore_reason}" : '';
			$log->info("INSMS:$id IGNORED (src=$sms->{abonent} dest=$sms->{num} link=$sms->{link_id} tr_id=$sms->{transaction_id}) $reason");
			return (0,undef);
		}
		else {
			my $id = $db->insert_sms_into_insms($sms);
			$self->{last_received_sms_time} = time();
			$log->info("INSMS:$id RECEIVED (src=$sms->{abonent} dest=$sms->{num} link=$sms->{link_id} tr_id=$sms->{transaction_id})");
			return (0,$id);
		}
	}
	return 0;
}

sub process_dr {
	my ($self,$pdu,$msg_id,$stat_text,$rep_date) = @_;
	my ($db,$log) = ($self->{db},$self->{log});
	my $s = $db->get_sms_by_msg_id($self->{smsc_id},$msg_id,$pdu->{source_addr});
	if ($s and $s->{id}) {
		$self->exec_event('on_receive_dr',$self,$pdu,$s);
		my $id = $s->{id};
		$log->debug("Delivery report ($stat_text) for SMS$id.");
		my $st = $dr_tab{$stat_text};
		if ($st) {
			$s->{outbox_status} = $db->set_sms_report_status($id,$st->{status},$rep_date,$s);
			$log->info("OUTSMS:$id DR $st->{name} (status=$stat_text)");
			$self->outbox_status_changed($s);
			if ($s->{params}{smsc_insms_ids}) {
				my $insms_id = $s->{params}{smsc_insms_ids}[$s->{sar_segment_seqnum}-1];
				$db->set_dr_to_outsms($insms_id,$st->{message_state},$rep_date) if ($insms_id);
			}
		}
		else {
			$log->info("OUTSMS:$id DR ? (status=$stat_text)");
		}
	}
	else {
		$log->warn("OUTSMS: DR message not found message_id=$msg_id (status=$stat_text)");
	}
}


sub outbox_status_changed {
	my ($self,$sms) = @_;
	if ($sms->{outbox_status}) {
		$self->{log}->info("OUTSMS:$sms->{id} MSG$sms->{outbox_id} (status=$sms->{outbox_status})");
		my $params = $self->exec_event('on_outbox_status',$self,$sms);
		$self->{db}->redis_report($sms, $params) if ($sms->{registered_delivery});
	}
}

sub exec_event {
	my $self = shift @_;
	my $event_name = shift @_;
	my @params = @_;
	my $retval = undef;
	eval {
		if ($self->{func_module} and $self->{func_module}->can($event_name)) {
			$self->{log}->debug("Executing event handler $self->{func_module}::$event_name");
			$retval = $self->{func_module}->$event_name(@params);
		};
	};
	if ($@) {
		$self->warning($@);
		$retval = undef;
	}
	return $retval;
}

sub get_status_text {
	my ($self,$err) = @_;
	my $errmsg = sprintf('0x%x.',$err).' ';
	if ($self->{status_code}->{$err}) {
		$errmsg .= $self->{status_code}->{$err};
	}
	else {
		$errmsg .= 'Unknown error.';
	}
	return $errmsg;
}

sub window_check {
	my $self = shift @_;
	my $window_size = scalar keys %{$self->{sent_buffer}};
#	$self->{log}->debug("Window size: $window_size/$self->{window_limit}");
	if ($window_size < $self->{window_limit}) {
		$self->{full_window_timestamp} = undef;
		return ($self->{window_limit} - $window_size);
	}
	if ($self->{full_window_timestamp}) {
		if ((time - $self->{full_window_timestamp}) > 60) {
			$self->{log}->error("window is full during last 60 seconds. Terminating...");
			$self->{terminated} = time;
		}
	} else {
		$self->{full_window_timestamp} = time;
	}
	return 0;
}

sub dec_wait_resp_count {
	my ($self) = @_;
	$self->{wait_resp_count}--;
	$self->{wait_resp_count} = 0 if ($self->{wait_resp_count} < 0);
	$self->{log}->debug("Waiting for $self->{wait_resp_count} resps --");
}

sub inc_wait_resp_count {
	my ($self) = @_;
	$self->{wait_resp_count}++;
	$self->{log}->debug("Waiting for $self->{wait_resp_count} resps ++");
}

sub set_wait_resp_count {
	my ($self,$val) = @_;
	$self->{wait_resp_count} = $val;
	$self->{log}->debug("Waiting for $self->{wait_resp_count} resps set");
}

sub debug_pdu {
	my ($self,$pdu) = @_;
	my $cmd = uc ($process_tab{$pdu->{cmd}}{cmd} || 'unknown');
	$self->{log}->debug("$cmd $pdu->{seq} received:\n".Dumper($pdu))
	  if ($self->{log}->is_debug());
	return 1;
}

sub get_error_rule {
	my ($self,$err) = @_;
	my $error_rule;
	if ($err eq '_no_resp_') {
		$error_rule = $self->{error_tab}{$err} || $error_tab{_default_}{$err};
	}
	else {
		$error_rule = $self->{error_tab}{$err} || $self->{error_tab}{_default_};
	}
	return $error_rule;
}

sub enter_state {
	my ($self,$state) = @_;
	if (! ($self->{state} eq $state)) {
		$self->{db}->set_process_state(uc($self->{smpp_mode_name}).'_'.$self->{smsc_id},uc($state),uc($self->{state}));
		$self->{state} = $state;
		$self->{log}->debug("Enter ".uc($state)." state.");
	}
	return 1;
}

sub heartbeat {
	my ($self) = @_;
	$self->{heartbeat} = 0
	  if (!$self->{heartbeat});
 	if ((time() - $self->{heartbeat}) >= 60) {
		$self->{db}->heartbeat($self->{pr_name},$self->{last_received_sms_time});
		$self->{log}->debug("HEARTBEAT.");
		$self->{heartbeat} = time();
 	}
	return 1;
}

sub routines {
	my ($self) = @_;

	foreach (keys(%{$self->{sended_abonents}})) {
		delete($self->{sended_abonents}->{$_})
		  if ((time() - $self->{sended_abonents}->{$_}) >= 60);
	}

	my $current_time = time();

	$self->{routines_time} = 0 if (!$self->{routines_time});
 	if (($current_time - $self->{routines_time}) >= 60) {
		$self->routines_procedure();
		$self->{log}->debug("ROUTINES.");
		$self->{routines_time} = $current_time;
	}

	if (defined($self->{routines_event_delay})
		and ($current_time - $self->{routines_event_time}) >= $self->{routines_event_delay}) {
			$self->{routines_event_delay} = $self->exec_event('routines',$self);
			$self->{routines_event_time} = $current_time;
	}

	$self->{save_statistics_time} = 0 if (!$self->{save_statistics_time});
 	if (($current_time - $self->{save_statistics_time}) >= 300) {
		$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
		$self->{log}->debug("STATISTICS SAVED.");
		$self->{save_statistics_time} = $current_time;
	}

	$self->{resend_pdu_time} = 0 if (!$self->{resend_pdu_time});
 	if (($current_time - $self->{resend_pdu_time}) >= 600) {
#		$self->{db}->set_outsms_all_not_delivered($self->{smsc_id});
#		  if (!$self->{ussd_type});
		$self->{resend_pdu_time} = $current_time;
	}
	return 1;
}

sub routines_procedure {
	my ($self) = @_;

	# ��������������� � ��
#	if ($self->{db}->{do_count}>1000) {
#		$self->{db}->{do_count} = 0;
#		$self->{db}->{db}->disconnect;
#		$self->{db}->connect;
#		$self->{log}->debug("Reconnected to DB.");
#	}

	# ��������� ������ ������������ ���������
	for (keys(%{$self->{blocked_abonents}})) {
		my $blab = $_;
		if ($self->{blocked_abonents}->{$blab} < time()) {
			delete($self->{blocked_abonents}->{$blab});
		}
	}
	return 1;
}

sub log_sms_info {
	my $sms = shift;
	my $text = '';
	$text .= "n=$sms->{num} " if $sms->{num};
	$text .= "a=$sms->{abonent} " if $sms->{abonent};
	$text .= "l=$sms->{link_id} " if $sms->{link_id};
	$text .= "tr=$sms->{transaction_id} " if $sms->{transaction_id};
	return $text;
}

sub warning {
	my ($self,$err,$err2) = @_;
	$err2 = ($err2) ? "$err\n$err2" : $err;
	$self->{log}->warn($err);
	yell($err2,code => 'WARNING',process => $self->{pr_name},
		 header => "$self->{pr_name}($self->{name}): $err",ignore => { log4perl => 1 });
	return 1;
}

sub fatal_error {
	my ($self,$err2) = @_;
	my ($err) = ($err2 =~ /^(.*?)$/m);
	my ($err_type) = ($err =~ /^(\w+_ERROR\d*):/s);
	$err_type = $err_type || 'FATAL_ERROR';
	$self->{log}->fatal($err2);
	my $ignore_email = ($err_type eq 'CONNECT_ERROR');
	$self->{db}->reconnect();
	yell($err2,code => $err_type, process => $self->{pr_name},
		 header => "$self->{pr_name}($self->{name}): $err",ignore => { log4perl => 1, 'e-mail' => $ignore_email });
	return 1;
}

sub add_pdu_handler {
	my ($self,$command,$name,$accept,$handler) = @_;
    $process_tab{$command} = {cmd => $name, accept => $accept, reply => $handler};
	return 1;
}

1;
