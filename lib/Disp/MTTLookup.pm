#!/usr/bin/perl -w
# $Id: $

=head1 NAME

 Disp::MTTLookup

=head1 METHODS

=cut
package Disp::MTTLookup;
use Disp::Utils;
use base 'Disp::Object';
use strict;
use warnings;
use Data::Dumper;

use Iterator_Utils ':all';


our $VERSION	= 0.1;		# Версия пакета.

=head2 init_from_db ($db_iterator) void

Подгатавливает к работе объект. Данные берутся из итератора.

=cut                              #
sub init_from_db {                # () void
	my ( $self, $iterator, $warn_method ) = @_;

	$self->{db} = {};
	while (my $i = NEXTVAL($iterator)) {
		my $len = length $i->{num_start};
		unless ($len == length $i->{num_end}) {
			$warn_method->("Length mismatch") if $warn_method;
			next;
		}

		my $arr = ($self->{db}{$i->{def}}{$len} ||= []);

		my $pos = ordered_insert_position($arr, sub { $i->{num_start} < $_->{num_start}});

		my $check_lower = ($pos != 0);
		my $check_upper = ($pos < $#$arr);

		# overlap check
		my ($lower_violation, $upper_violation);

		if ($check_lower) {
			$lower_violation = (($i->{num_start} <= $arr->[$pos-1]{num_end})
								or ($i->{num_end} <= $arr->[$pos-1]{num_end}));
		}

		if ($check_upper) {
			$upper_violation = (($i->{num_start} >= $arr->[$pos]{num_start})
								or ($i->{num_end} >= $arr->[$pos]{num_start}));
		}

		if ( $upper_violation or $lower_violation ) {
			$warn_method->("Position overlap for DEF $i->{def}: $i->{num_start}-$i->{num_end}",
						   $check_lower ? "Prev end: ".$arr->[$pos-1]{num_end} : (),
						   $check_upper ? "Next start: ".$arr->[$pos]{num_start} : ())
			  if $warn_method;
			next;
		}
		splice @$arr, $pos, 0, $i;
	}
}

=head3 lookup ($abonent) hashref

Ищет наиболее подходящую для указанного абонента запись в базе МТТ.
Работает только для России. Если не Россия, или не найдено -
возвращает 'undef'.

=cut                              #
sub lookup {                      # ($abonent) hashref
	my ( $self, $abonent ) = @_;

	return unless $abonent =~ /^7(\d{3})(\d+)/;
	my ( $def, $rest ) = ($1, $2);
	my $def_entry = $self->{db}{$def};
	return unless $def_entry;

	foreach (reverse sort keys %$def_entry) {
		# warn "Lookup length: $_";
		my $match = mtt_binary_search($def_entry->{$_}, substr($rest, 0, $_));
		return $match if $match;
	}
}

sub mtt_binary_search {
	my ( $arr, $part ) = @_;

	my $a = 0;
	my $b = $#$arr;

	while ($a < $b) {
		my $center = $a + (($b-$a) >> 1);
		if ( $arr->[$center]{num_end} < $part ) {
			$a = $center + 1;
		} elsif ($arr->[$center]{num_start} > $part) {
			$b = $center - 1;
		} else {
			# We got it
			return $arr->[$center];
		}
	}

	if ($a==$b and $arr->[$a]{num_start} <= $part and $part <= $arr->[$a]{num_end}) {
		return $arr->[$a];
	}

	return;
}

1;
