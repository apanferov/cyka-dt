#!/usr/bin/perl -w
# $Id: $
package Disp::Billing::CalcMegafon;
use strict;
use warnings;
use base 'Disp::Billing::CalcDefault';
use Data::Dumper;
our $VERSION	= 0.1;		# ������ ������.

sub new {
	my $proto = shift;
	my $self = $proto->SUPER::new(@_);

	return $self;
}

sub execute {
	my ( $self, $month ) = @_;

	my $label = "($self->{smsc}{billing_rule}:$self->{smsc}{id})";

	$self->_parse_ranges($self->{ranges}, 'p_ranges');

	my $sql = "select pl.id as pid, pl.num as pnum, sum(b.insms_count)
	as inct, sum(b.insms_count*pr.price_in) as isum,
	sum(b.outsms_count) as ocnt, sum(b.outsms_count*pr.price_out) as
	osum from plug pl left outer join billing b on pl.id = b.plug_id left
	outer join plug_price pr on b.plug_id = pr.plug_id and pr.date_start <=
	date(b.date) and pr.date_end >= date(b.date) where smsc_id = ? and
	(extract(YEAR_MONTH from b.date) = ? or b.date is null) group by pl.id";

	my $plugs = Config->dbh->selectall_arrayref($sql, {Slice=>{}}, $self->{smsc}{id}, $month);


	print "$label\n";

	foreach my $plug (@$plugs) {
		my $prev_g = 0;
		$self->process_month($plug->{pid}, $month, sub {
								 my ( $date, $plug_price ) = @_;
								 my $a_sms = $plug_price->{price_in};
								 my $b_sms = $plug->{icnt} || 0 ;
								 my $c_sms = $plug->{ocnt} || 0;
								 #  $e = $a_sms * $b_sms;
								 my $e = $plug->{isum} || 0;

								 my $f = $plug_price->{price_out} * ( $b_sms + $c_sms );

								 my $p = ($b_sms) ? (( $e - $f ) / $b_sms) : 666;

								 my $g = $self->_range_check($self->{p_ranges}, $p);

								 unless ($g) {
									 print "$label No koef G for P '$p' found\n";
									 next;
								 }
								 $g = $g->{koef};

								 my $data = { price_in => ($a_sms - $plug_price->{price_out}) * $g,
											  price_out => $plug_price->{price_out} * $g };

								 if ($prev_g != $g) {
									 print "$label For $plug->{pnum} at '$date' A = $a_sms, B = $b_sms, C = $c_sms, E = $e, F = $f, P = $p, G = $g, \$ = $data->{price_in}/$data->{price_out}\n";
								 }
								 $prev_g = $g;
								 return $data; });
	}

}





1;
