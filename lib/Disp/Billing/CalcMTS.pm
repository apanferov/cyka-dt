#!/usr/bin/perl -w
# $Id: $
package Disp::Billing::CalcMTS;
use strict;
use warnings;
use base 'Disp::Billing::CalcDefault';
use Data::Dumper;
our $VERSION	= 0.1;		# ������ ������.

sub new {
	my $proto = shift;
	my $self = $proto->SUPER::new(@_);

	return $self;
}

sub execute {
	my ( $self, $month ) = @_;

	$self->_parse_ranges($self->{ranges}, 'grand_ranges');

	my $label = "($self->{smsc}{billing_rule}:$self->{smsc}{id})";

	my $sql = "select pl.id as pid, pl.num as pnum, sum(b.insms_count)
	as inct, sum(b.insms_count*pr.price_in) as isum,
	sum(b.outsms_count) as ocnt, sum(b.outsms_count*pr.price_out) as
	osum from plug pl left join billing b on pl.id = b.plug_id left
	join plug_price pr on b.plug_id = pr.plug_id and pr.date_start <=
	date(b.date) and pr.date_end >= date(b.date) where smsc_id = ? and
	extract(YEAR_MONTH from b.date) = ? group by pl.id";

	my $plugs = Config->dbh->selectall_arrayref($sql, {Slice=>{}}, $self->{smsc}{id}, $month);

	my $d_obsh = 0;
	my $k_kp;

	# Two pass process. First, calculate K_kp. Then actually store prices in database.
	foreach my $pass (1..2) {
		foreach my $plug (@$plugs) {
			if ($pass == 1) {
				$d_obsh += ($plug->{isum} || 0);
			} else {
				$self->process_month($plug->{pid}, $month, sub {
										 my ( $date, $plug_price ) = @_;
										 return { price_in => $plug_price->{price_in} * $k_kp,
												  price_out => $plug_price->{price_out} }; });
			}
		}
		if ($pass == 1) {
			$k_kp = $self->_range_check($self->{grand_ranges}, $d_obsh / 1000);
			die "No matching range for d_obsh $d_obsh" unless $k_kp;
			$k_kp = $k_kp->{koef};
			print "$label selected k_kp=$k_kp because d_obsh=$d_obsh\n";
		}
	}

}





1;
