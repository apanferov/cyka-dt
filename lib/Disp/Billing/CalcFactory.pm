#!/usr/bin/perl -w
# $Id: $

package Disp::Billing::CalcFactory;
use strict;


our $VERSION	= 0.1;		# ������ ������.


sub make_calc {
	my ( $proto, $smsc, $config ) = @_;

	my $class;
	if ($config->{class}) {
		$class = "Disp::Billing::Calc$config->{class}";
	} else {
		die __PACKAGE__.": class was not given in make_calc parameters";
	}
	my $instance;
	{
		no strict 'refs';
		my $tmp = "${class}::VERSION";
		unless (defined $$tmp) {
			eval "use $class";
			if ($@) {
				die "Load of '$class' failed: $@";

			}
		}
	}
	eval {
		$instance = $class->new($smsc, $config);
	};
	if ($@) {
		die __PACKAGE__.": can't create '$class' instance : $@";
	}
	unless ($instance) {
		die __PACKAGE__.": '$class' instance creation failed";
	}
	return $instance;
}

1;
