#!/usr/bin/perl -w
# $Id: $
package Disp::Billing::CalcDefault;
use strict;
use warnings;
use base 'Disp::Object';
use Data::Dumper;
use Date::Manip;
use Time::Local;
use POSIX qw/strftime/;

our $count = 0;

our $VERSION	= 0.1;		# ������ ������.

sub new {
	my $proto = shift;
	my $smsc = shift;
	my $self = $proto->SUPER::new(@_);

	$self->{smsc} = $smsc;

	return $self;
}

sub execute {
	my ($self, $month) = @_;

	my $plugs = Config->dbh->selectall_arrayref("select * from plug where smsc_id = ? and (_deleted is null or _deleted = 0)", {Slice=>{}}, $self->{smsc}{id});
	foreach my $plug (@$plugs) {
		$self->process_month( $plug->{id}, $month, sub {
								  my ( $date, $plug_price ) = @_;
								  return { price_in => ($plug_price->{price_in} || 0) * $self->{koef},
										   price_out => $plug_price->{price_out} || 0 };
							  } );
	}
}

sub process_month {
	my ( $self, $plug_id, $month, $func ) = @_;
	die "Invalid month format: $month" unless $month =~ /^(\d{4})(\d{2})$/;
	my $first = timelocal(1,1,12,1,$2-1,$1-1900);

	my $prev;
	my $date = ParseDate("epoch $first");

	my $prev_data;

	do {
		my $sql_date = UnixDate($date, '%Y-%m-%d');


		my $plug_price = $self->find_plug_price($sql_date, $plug_id);
		my $data = $func->($sql_date, $plug_price);

		$data->{date} = $sql_date;
		$data->{plug_id} = $plug_id;
		$data->{currency} = $plug_price->{currency};

		$self->_store_price($data);

		if ($data->{price_in} != (defined($prev_data->{price_in}) ? $prev_data->{price_in} : -1 ) or
			$data->{price_out} != ($prev_data->{price_out} || 0) or
			$data->{currency} ne ($prev_data->{currency} || "") ) {
			print "For PLUG$plug_id\tfrom date $sql_date prices set to $data->{price_in}/$data->{price_out} ($data->{currency})\n";
		}

		$prev_data = $data;
		$date = DateCalc($date, "+ 1 day");
	} while ( UnixDate($date, '%d') != 1);
}

sub find_plug_price {
	my ( $self, $sql_date, $plug_id ) = @_;
	my $price = Config->dbh->selectrow_hashref('select * from plug_price where date_start <= ? and date_end >= ? and plug_id = ?',
											   {Slice=>{}}, $sql_date, $sql_date, $plug_id);
	die "No prices for PLUG:$plug_id on '$sql_date'" unless $price;
	return $price;
}

sub _parse_ranges {
	my ($self, $ranges, $to) = @_;

	$self->{$to} = [];

	my $start = 0;
	my @ranges = split /\s*;\s*/, $ranges;
	foreach (@ranges) {
		my ( $end, $koef ) = split /-/;
		unless (defined $koef) {
			$koef = $end;
			$end = -1;
		}
		push @{$self->{$to}}, { start => $start, end => $end, koef => $koef };
		$start = $end;
	}
}

sub _parse_prices {
	my ($self, $prices, $to) = @_;

	$self->{$to} = {};

	my $start = 0;
	my @prices = split /\s*;\s*/, $prices;
	foreach (@prices) {
		my ( $num, $price ) = split /-/;
		$self->{$to}{$num} = $price;
	}
}

sub _store_price {
	my ($self, $price_data) = @_;
	my $sql = "insert into billing_price (date, plug_id, price_in, price_out, currency) values (?,?,?,?,?) ON DUPLICATE KEY UPDATE date = ?, plug_id = ?, price_in = ?, price_out = ?, currency = ?";
	Config->dbh->do($sql, undef, (@{$price_data}{qw/date plug_id price_in price_out currency/}) x 2);
}

sub _range_check {
	my ( $self, $ranges, $value ) = @_;
	foreach (@$ranges) {
		my $upper = ($_->{end} > 0) ? ( $value <= $_->{end} ) : 1;
		my $lower = ($_->{start} > 0 ) ? ( $value >= $_->{start} ) : 1;
		if ($upper and $lower) {
			return $_;
		}
	}
	return undef;
}

1;
