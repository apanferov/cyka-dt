#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Disp::AltDB;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use DBI;
use Disp::Utils;
use Carp qw(confess);
use Net::Domain qw(hostfqdn);
use Data::Dumper;
use POSIX;
my $altdb_handler;

our @EXPORT = qw/db_transports/;

# �����������
sub new {
   my ($class,$conf) = @_;
   my $self = { tr_count => 0,
				conf => $conf || Config->section('database') };
   Disp::AltDB::connect($self);
   bless $self, $class;
   $altdb_handler = $self;
   return $self;
}

sub connect {
	my ($self) = @_;
	if ($self->{conf}) {
 		$self->{db} = DBI->connect(
			"DBI:".( $self->{conf}{type} ? $self->{conf}{type} : "mysql" ).":$self->{conf}{name};host=$self->{conf}{host}".( $self->{conf}{port} ? ";port=$self->{conf}{port}" : "" ),
			$self->{conf}{username}, $self->{conf}{password},
			{ AutoCommit => 1, RaiseError => 1 });

		$self->{db}->{mysql_auto_reconnect} = 0;
		$self->{db}->do("SET NAMES 'cp1251'");
		$self->{db}->do("SET character_set_client=cp1251");
	}
	else {
		warn "Database config not found";
		Config::db_connect();
		$self->{db} = Config::dbh();
#		$self->{db}->do('SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED');
	}
}

sub reconnect {
   my ($self) = shift;
        if (!$self->{db}->ping()) {
                eval {
                        $self->connect();
                };
                return 0 if ($@);
        };
   return 1;
}

sub disconnect {
	my ($self) = @_;
	$self->{db}->disconnect();
}

sub ping {
	my ($self) = shift;
	return $self->{db}->ping();
}

sub begin_work {
	my $self = shift;
	eval {
		if ($self->{tr_count} > 1000) {
			$self->disconnect();
			$self->connect();
			$self->{tr_count} = 0;
			use Log::Log4perl qw(get_logger);
			my $log=get_logger("");
			$log->debug('Reconnected to DB.');
		}
		$self->{tr_count}++;
		return $self->{db}->begin_work();
	};
	confess $@ if ($@);
}

sub commit {
	my $self = shift;
	eval {
		return $self->{db}->commit();
	};
	confess $@ if ($@);
}

sub rollback {
	my $self = shift;
	eval {
		return $self->{db}->rollback();
	};
	confess $@ if ($@);
}

sub do {
	my $self = shift;
	eval {
		return $self->{db}->do(@_);
	};
	confess $@ if ($@);
}

sub fetchall_arrayref {
	my $self = shift;
	my $q_text = shift;
	return $self->{db}->selectall_arrayref($q_text);
}

sub get_value {
	my ($self) = shift;
	my ($val) = $self->{db}->selectrow_array(@_);
	return $val;
}

# ����������
sub finish {
	my ($self) = @_;
	$self->{db}->disconnect();
}

sub repeat_twice {
   my $self = shift;
   my $func = shift;
   my @params = @_;
  	eval {
		$func->($self,@params);
	};
   if ($@) {
    	$self->rollback();
     	$func->($self,@params);
   }
}

my $num_price_cache = {};
sub num_price {
    my ( $self, $operator_id, $num ) = @_;
    unless (exists($num_price_cache->{$operator_id}) and exists($num_price_cache->{$operator_id}{$num})) {

        my ( $price ) = $self->{db}->selectrow_array("select price_in from set_num_price where operator_id = ? and num = ? and date_start <= now() order by date_start desc limit 1", undef,
                                                     $operator_id, $num);
        # XXX Or die here?
        $num_price_cache->{$operator_id}{$num} = $price;
    }
    return $num_price_cache->{$operator_id}{$num};
}



# �������� ������������ ����
# ������������: SMS_CLIENT
sub get_smsc_configuration {
   my ($self, $smsc_id) = @_;
   my $db = $self->{db};
   $self->{smsc_id} = $smsc_id;

    my $cnf = $db->selectrow_hashref('select * from set_smsc where id=?', undef, $smsc_id);
    my($local_addr) = $db->selectrow_array('select local_addr from set_smsc_addr where smsc_id=? and host=?', undef,
		$smsc_id,
		'anywhere' eq lc($cnf->{run_on_host})
			? hostfqdn()
			: $cnf->{run_on_host});

	$cnf->{local_addr} = $local_addr if $local_addr;

   $cnf->{_smpp_mode} = $cnf->{smpp_mode};
   my @err_types = split(/\s*,\s*/,($cnf->{err_types} || 'smpp'));
   delete($cnf->{smpp_mode});

   my $tmp = $db->selectall_arrayref("select err_num,err_text from set_errors where err_type in (".join(",", ('?') x @err_types).")",undef,@err_types);
   my %status_code;
   $status_code{$_->[0]} = $_->[1] foreach (@$tmp);
   $cnf->{status_code} = \%status_code;

   delete $cnf->{id};
   return $cnf;
}

sub set_process_state {
	my ($self,$pr_name,$pr_state,$pr_state_old) = @_;
	$self->begin_work();
	$self->do('insert into tr_process_state_log (date,pr_name,pr_state,pr_state_old) values (now(),?,?,?)',undef,$pr_name,$pr_state,$pr_state_old);
	$self->do('replace into tr_process_state (pr_name,pr_state,pr_state_old) values (?,?,?)',undef,$pr_name,$pr_state,$pr_state_old);
	$self->commit();
}

sub heartbeat {
        my ($self,$pr_name,$last_received_sms_time) = @_;
#       $self->do('update tr_process_heartbeat set pr_heartbeat=now() where pr_name=?',undef,$pr_name);
        if (!$last_received_sms_time) {
                $last_received_sms_time = $self->get_value('select unix_timestamp(last_received_sms_time) from tr_process_heartbeat where pr_name=?',undef,$pr_name);
        }
		$self->begin_work();
        $self->do('replace INTO tr_process_heartbeat set pr_heartbeat=now(),pr_name=?,last_received_sms_time=from_unixtime(?)',undef,$pr_name,$last_received_sms_time);
		$self->commit();
}

# �������� ���� SMSC_ID -> CLIENT_TYPE
# ������������ � DECODER
sub get_smsc_client_type {
    my ( $self ) = @_;
    my $db = $self->{db};
    my $hr = $db->selectall_hashref('select id,client_type from set_smsc','id');
    $hr->{$_}=$hr->{$_}{client_type} foreach (keys(%$hr));
    return $hr;
}

# �������� ���� SMSC_ID->USE7BIT
# ������������: ENCODER, DECODER
sub get_smsc_use7bit {
   my ($self) = @_;
   my $db = $self->{db};
   my $hr = $db->selectall_hashref('select id,use7bit from set_smsc','id');
   $hr->{$_}=$hr->{$_}{use7bit} foreach (keys(%$hr));
   return $hr;
}

# �������� ���� SMSC_ID->USE1251
# ������������: ENCODER
sub get_smsc_use1251 {
   my ($self) = @_;
   my $db = $self->{db};
   my $hr = $db->selectall_hashref('select id,use1251 from set_smsc','id');
   $hr->{$_}=$hr->{$_}{use1251} foreach (keys(%$hr));
   return $hr;
}

# �������� ���� SMSC_ID->CUT_TYPE
# ������������: ENCODER
sub get_smsc_cut_type {
   my ($self) = @_;
   my $db = $self->{db};
   my $hr = $db->selectall_hashref('select id,cut_type from set_smsc','id');
   $hr->{$_}=$hr->{$_}{cut_type} foreach (keys(%$hr));
   return $hr;
}

# �������� ���� SMSC_ID->USSD_TYPE
# ������������: DECODER
sub get_smsc_ussd_type {
   my ($self) = @_;
   my $db = $self->{db};
   my $hr = $db->selectall_hashref('select id,ussd_type from set_smsc','id');
   $hr->{$_}=$hr->{$_}{ussd_type} foreach (keys(%$hr));
   return $hr;
}

#
# ������������: SMS_CLIENT
sub set_outsms_all_not_delivered {
	my @params = @_;
	eval { set_outsms_all_not_delivered_(@params); };
	if ($@) {
		my ($self) = @params;
		$self->rollback();
		set_sms_delivered_status_(@params);
	}
}

sub set_outsms_all_not_delivered_ {
	my ($self,$smsc_id) = @_;
	$self->begin_work();
	$self->do('update tr_outsms set status=0 where (status=1) and (smsc_id=?) and (now() > adddate(delivery_time,INTERVAL 3 MINUTE))',undef,$smsc_id);
	$self->commit();
	return 1;
}

#
# ������������: SMS_CLIENT
sub set_outsms_all_delivered {
	my ($self,$smsc_id) = @_;
	my $tmp = $self->{db}->selectall_arrayref('select id from tr_outsms where (status>1) and (smsc_id=?)',undef,$smsc_id);
	if (@$tmp) {
		$self->set_sms_delivered_status($_->[0]) foreach (@$tmp);
	}
	return 1;
}

sub set_outsms_all_fail_deliver {
	my ($self,$smsc_id) = @_;
	my $tmp = $self->{db}->selectall_arrayref('select id from tr_outsms where (status=1) and (smsc_id=?) and (now() > adddate(delivery_time,INTERVAL 3 MINUTE))',undef,$smsc_id);
	if (@$tmp) {
		foreach (@$tmp) {
			my $id=$_->[0];
			$self->set_sms_new_status($id,255,0,0,0);
			$self->set_sms_delivered_status($id);
		}
	}
	return 1;
}

# ��������� ��������������
#
sub set_outsms_autoincrement {
	my ($self) = @_;
	my $db = $self->{db};
	my ($m) = $db->selectrow_array('select max(c.m) from (select max(a.id) m from tr_outsms a union select max(b.id) m from tr_outsmsa b) c');
	$m++;
	$self->do("alter table tr_outsms auto_increment=$m");
	return 1;
}

# ������� �� OUTSMS ��������� ������� � ��������
# ������������: SMS_CLIENT
sub get_sms_ready_to_send {
	my ($self, $smsc_id, $limit) = @_;
	die 'AltDB: smsc_id not defined' unless (defined $smsc_id);
	$limit ||= 10;
	return {} unless ($limit > 0);
	my $msgs = $self->{db}->selectall_hashref("select * from tr_outsms where (smsc_id=?) and (delivery_time<now()) and (status in (0,1)) order by inbox_id desc,id limit $limit",'id',undef,$smsc_id);
	for (keys(%$msgs)) {
		$msgs->{$_}{params} = dumper_load($msgs->{$_}{params}) if ($msgs->{$_}{params});
	}
	return $msgs;
}


# ������� �� OUTSMS ��������� ������� � ��������
# ������������: HTTP_SMS_CLIENT
sub get_sms_ready_to_send_tr {
	my ($self, $smsc_id, $transaction_id, $num) = @_;
	unless (defined $smsc_id) {die 'AltDB: smsc_id not defined';}
	my $msgs;
	if (defined $transaction_id) {
		$msgs = $self->{db}->selectall_hashref('select * from tr_outsms where (smsc_id=?) and (transaction_id=?) and (status=0) order by id limit 10','id',undef,$smsc_id,$transaction_id);
	} else {
		$msgs = $self->{db}->selectall_hashref('select * from tr_outsms where (smsc_id=?) and (num=?) and (now() > adddate(date,INTERVAL 2 MINUTE)) and (status=0) order by id limit 10','id',undef,$smsc_id,$num);
	}
	for (keys(%$msgs)) {
		$msgs->{$_}{params} = dumper_load($msgs->{$_}{params}) if ($msgs->{$_}{params});
	}
	return $msgs;
}

sub move_low_priority_to_outsms {
	my ($self,$smsc_id,$limit) = @_;
#	return;
	return unless ($limit and {20=>1,520=>1,501=>1,502=>1,503=>1,504=>1,505=>1,511=>1,512=>1,513=>1,514=>1,515=>1}->{$smsc_id});
	$self->begin_work();
	my $tmp = $self->{db}->selectall_arrayref("select outbox_id,sar_segment_seqnum from tr_outsms_low_priority where smsc_id=? order by outbox_id,sar_segment_seqnum limit $limit",undef,$smsc_id);
	foreach (@$tmp) {
		$self->do("insert into tr_outsms select * from tr_outsms_low_priority where outbox_id=? and sar_segment_seqnum=?",undef,@$_);
		$self->do("delete from tr_outsms_low_priority where outbox_id=? and sar_segment_seqnum=?",undef,@$_);
	}
	$self->commit();
}


# ������� �� INSMS ��������� ��� ������������� � �������
# ������������: DECODER
#sub get_sms_ready_to_decode_all {
#   my ($self) = @_;
#   return $self->{db}->selectall_hashref('select * from tr_insms order by id','id');
#}
sub get_sms_ready_to_decode {
	my ($self,$maxid) = @_;
	$maxid ||= 0;
	return $self->{db}->selectall_hashref('select * from tr_insms where id>? order by id limit 20','id',undef,$maxid);
}


# ������� �� OUTBOX ��������� ��� ������������� � ��������
# ������������: ENCODER
sub get_sms_ready_to_encode {
	my ($self) = @_;
	return $self->{db}->selectall_hashref("select * from tr_outbox where transport_type='sms' order by id limit 50",'id');
}


# ��������� ID ��������� � OUTSMSA �� ��� MSG_ID
# ������������: SMS_CLIENT
sub get_sms_id_by_msg_id {
	my ($self, $smsc_id, $msg_id) = @_;
	my ($id) = $self->{db}->selectrow_array('select id from tr_outsmsa where smsc_id=? and msg_id=? order by id desc limit 1',undef,$smsc_id,$msg_id);
	return $id;
}

sub get_sms_settings_by_msg_id {
	my ($self, $smsc_id, $msg_id) = @_;
	my ($id,$registered_delivery,$push_id) =
	  $self->{db}->selectrow_array('select id,registered_delivery,push_id from tr_outsmsa where smsc_id=? and msg_id=? order by id desc limit 1',undef,$smsc_id,$msg_id);
	return ($id,$registered_delivery,$push_id);
}

my $mts_tab = {5 => 1, 501 => 1, 502 => 1, 503 => 1, 504 => 1, 505 => 1};
sub get_sms_by_msg_id {
	my ($self, $smsc_id, $msg_id, $abonent) = @_;
	my $sms = ($mts_tab->{$smsc_id}) ?
		$self->{db}->selectrow_hashref('select *,if(abonent=?,0,1) as eq from tr_outsmsa where smsc_id in ('.join(',',keys(%$mts_tab)).') and msg_id=? order by eq desc limit 1',undef,$abonent,$msg_id) :
		$self->{db}->selectrow_hashref('select *,if(abonent=?,0,1) as eq from tr_outsmsa where smsc_id=? and msg_id=? order by eq desc limit 1',undef,$abonent,$smsc_id,$msg_id);
	$sms->{params} = dumper_load($sms->{params}) if ($sms->{params});
	return undef unless ($sms->{id});
	return $sms;
}

sub get_insms_id_by_transaction_id {
	my ( $self, $smsc_id, $transaction_id ) = @_;
	my ($id) = $self->{db}->selectrow_array('select id from tr_insmsa where smsc_id=? and transaction_id=? order by id desc limit 1',undef,$smsc_id,$transaction_id);
	return $id;
}

# ��������� ������ ������� ��������� � OUTSMS
# ������������: SMS_CLIENT
sub set_sms_new_status {
	my ($self, $id, $status, $seconds, $err, $msg_id, $update_retry_count) = @_;
	unless (defined $id) {die 'AltDB: id not defined';}
	unless (defined $status) {die 'AltDB: status not defined';}
	my $ret_val;
	my $retry_count_increment = ($update_retry_count) ? 1 : 0;
	$self->begin_work();
	if (defined($seconds)) {
        $ret_val = $self->do('update tr_outsms set retry_count=retry_count+?, status=?, delivery_time=adddate(now(),INTERVAL ? SECOND), err_num=?, msg_id=? where id=?',
							 undef,$retry_count_increment,$status,$seconds,$err,$msg_id,$id);
	}
	else {
        $ret_val = $self->do('update tr_outsms set retry_count=retry_count+?, status=?, err_num=?, msg_id=? where id=?',
							 undef,$retry_count_increment,$status,$err,$msg_id,$id);
	}
	$self->commit();
	return $ret_val;
}


# ��������� ������� ���������� ��������� � OUTSMS
# ������������: SMS_CLIENT
sub set_sms_delivered_status {
	my @params = @_;
	my $outbox_status;
	eval {
		$outbox_status = set_sms_delivered_status_(@params);
	};
	if ($@) {
		use Log::Log4perl qw(get_logger);
        my $log=get_logger("");
        $log->warn($@);
		my ($self) = @params;
		$self->rollback();
		$outbox_status = set_sms_delivered_status_(@params);
	}
	return $outbox_status;
}

sub set_sms_delivered_status_ {
	my ($self, $id, $ignore_billing, $fixed_count) = @_;
	unless (defined $id) {die 'AltDB: id not defined';}
	my $outbox_status;
	
	my $printErrorStatus = $self->{PrintError};
	my $raiseErrorStatus = $self->{RaiseError};
	my $sms;
	eval {
		$self->{PrintError} = 0;	 
		$self->{RaiseError} = 1; 
		$self->begin_work();
		$self->do('insert into tr_outsmsa (id,abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,params,outbox_id,inbox_id,push_id,insms_id,registered_delivery,status,retry_count,err_num,msg_id,delivery_time,delivered_date,pdu,test_flag,date) select id,abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,params,outbox_id,inbox_id,push_id,insms_id,registered_delivery,status,retry_count,err_num,msg_id,delivery_time,now(),pdu,test_flag,date from tr_outsms where id=?',
			  undef,$id);
		$sms = $self->{db}->selectrow_hashref('select * from tr_outsms where id=?',undef,$id);
		$sms->{params} = dumper_load($sms->{params}) if ($sms->{params});
		$self->do('delete from tr_outsms where id=?',undef,$id);
		$outbox_status = $self->update_delivery_status_($sms); # always calculate outbox_status because it required for billing
		if (!$ignore_billing) {
			$self->update_billing_($sms->{inbox_id}, $sms->{outbox_id}, $fixed_count);
		}
		$self->commit();
	};
	if ($@) {
		# ��������� ��� ����� ID
		$self->rollback; 
		
		$self->begin_work();
		# �������� ��������� ID
		my ($new_id) = $self->{db}->selectrow_array('select (t1.id + @@auto_increment_increment) as empty_id
from tr_outsmsa as t1
where
(id - @@auto_increment_offset) % @@auto_increment_increment = 0 and
(select 1 from tr_outsmsa as sta where sta.id = (t1.id + @@auto_increment_increment)) is null and
(select 1 from tr_outsms as st where st.id = (t1.id + @@auto_increment_increment)) is null
order by t1.id
limit 1');

		$sms = $self->{db}->selectrow_hashref('select * from tr_outsms where id=?',undef,$id);
		$sms->{params} = dumper_load($sms->{params}) if ($sms->{params});
		if ($new_id) {
			$self->do(
				'insert into tr_outsmsa (id,abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,params,outbox_id,inbox_id,push_id,insms_id,registered_delivery,status,retry_count,err_num,msg_id,delivery_time,delivered_date,pdu,test_flag,date) select ?,abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,params,outbox_id,inbox_id,push_id,insms_id,registered_delivery,status,retry_count,err_num,msg_id,delivery_time,now(),pdu,test_flag,date from tr_outsms where id=?',
				undef,
				$new_id,
				$id
			);
			$self->do('delete from tr_outsms where id=?',undef,$id);
		}
		$outbox_status = $self->update_delivery_status_($sms); # always calculate outbox_status because it required for billing
		if (!$ignore_billing) {
			$self->update_billing_($sms->{inbox_id}, $sms->{outbox_id}, $fixed_count);
		}
		$self->commit();		
	}	
	$self->{PrintError} = $printErrorStatus;	 
	$self->{RaiseError} = $raiseErrorStatus; 	
	
	$self->add_sms_to_statistics($sms->{operator_id},$sms->{num},'outcome_succ') if ($sms->{status}==2);
	$self->add_sms_to_statistics($sms->{operator_id},$sms->{num},'outcome_fail') if ($sms->{status}==255);
	return $outbox_status;
}

sub update_billing_ {
	my ($self, $inbox_id, $outbox_id, $fixed_count) = @_;
	if (defined($inbox_id) and $outbox_id) {
		if (defined($fixed_count)) {
			$self->do('update tr_billing set outsms_count=?, cyka_processed=0 where inbox_id=? and outbox_id=?',undef,$fixed_count,$inbox_id,$outbox_id);
		}
		else {
			$self->do('update tr_billing set outsms_count=outsms_count+1, cyka_processed=0 where inbox_id=? and outbox_id=?',undef,$inbox_id,$outbox_id);
		}
	}
}

sub update_billing {
	my ($self, $inbox_id, $outbox_id, $fixed_count) = @_;
	$self->begin_work();
  	$self->update_billing_($inbox_id, $outbox_id, $fixed_count);
	$self->commit();
}

sub update_delivery_status_ {
	my ($self,$sms) = @_;
	my ($outbox_id,$push_id,$insms_id,$status,$err_num) = map { $sms->{$_} } qw(outbox_id push_id insms_id status err_num);
	my $outbox_status;
#	if ($outbox_id and (501<=$sms->{smsc_id}) and ($sms->{smsc_id}<=505) and ($outbox_status=$self->new_outbox_delivery_status($sms))) {
	if ($outbox_id and ($outbox_status=$self->new_outbox_delivery_status($sms))) {
		# make change
		$self->do('update tr_outboxa set delivery_status=? where id=?',undef,$outbox_status,$outbox_id);
		use Log::Log4perl qw(get_logger);
	        my $log = get_logger('');
	        $log->debug("STATUS MSG$outbox_id SAVED $outbox_status") if ($outbox_status);
	}
#	elsif ($outbox_id) {
#		$self->do('update tr_outboxa set delivery_status=? where id=?',undef,$status,$outbox_id);
#	}
	if ($insms_id) {
		$err_num = 0
		  unless ($err_num and ($status == 255));
		my ($abonent,$num,$operator_id,$smsc_id) =
		  $self->{db}->selectrow_array('select abonent,num,operator_id,smsc_id from tr_insmsa where id=?',undef,$insms_id);
        my %status_map = (12 => 2, 11 => 6, 13 => 8, 14 => 3, 15 => 5, 2 => 6, 255 => 8);
		my $pdu = Net::SMPP::encode_submit_v34(undef,
											   service_type => '',
											   source_addr_ton => 1,
											   source_addr_npi => 1,
											   source_addr => $abonent,
											   dest_addr_ton => 0,
											   dest_addr_npi => 1,
											   destination_addr => $num,
											   esm_class => 0x04,
											   protocol_id => 0x00,
											   priority_flag => 0,
											   schedule_delivery_time => '',
											   validity_period => '',
											   registered_delivery => 0x00,
											   replace_if_present_flag => 0,
											   data_coding => 0,
											   sm_default_msg_id => 0,
											   short_message => '',
											   network_error_code => pack('CS',0,$err_num)
											  );
		$pdu .= Net::SMPP::encode_optional_params(
												  receipted_message_id => $insms_id."\x00",
												  message_state => pack('C',$status_map{$status}));
		$self->do('insert into tr_outsms (abonent,num,operator_id,smsc_id,pdu,esm_class,delivery_time,date) values (?,?,?,?,?,4,now(),now())',undef,$abonent,$num,$operator_id,$smsc_id,$pdu);
	}
	return $outbox_status;
}


# ��������� ������� ��������� � OUTSMS �� ������ � ��������
# ������������: SMS_CLIENT
sub set_sms_report_status {
	my @params = @_;
	my $outbox_status;
	eval {
		$outbox_status = set_sms_report_status_(@params);
	};
	if ($@) {
		use Log::Log4perl qw(get_logger);
        my $log=get_logger("");
        $log->warn($@);
		my ($self) = @params;
		$self->rollback();
		$outbox_status = set_sms_report_status_(@params);
	}
	return $outbox_status;
}
sub set_sms_report_status_ {
	my ($self,$id,$new_status,$rep_date,$sms) = @_;
	#unless (defined $sms->{id}) {die 'AltDB: id not defined';}
	my $outbox_status;
	$self->begin_work();
	if ($rep_date) {
        	$self->do('update tr_outsmsa set status=?, delivered_date=? where id=?',undef,$new_status,$rep_date,$id);
	} else {
	        $self->do('update tr_outsmsa set status=?, delivered_date=now() where id=?',undef,$new_status,$id);
	}
#	if ($sms and $sms->{registered_delivery}) {
	if ($sms) { # always calculate outbox_status because it required for billing
		$sms->{status} = $new_status;
		$sms->{err_num} = 0;
	        $outbox_status = $self->update_delivery_status_($sms);
	}
	$self->commit();
	return $outbox_status;
};

# ������� ����� ��������� � INSMS
# ������������: SMS_CLIENT
sub insert_sms_into_insms {
	my ($self,$sms) = @_;
	die 'AltDB: Abonent is not defined.'
	  unless ($sms->{abonent});
	die 'AltDB: Num is not defined.'
	  unless ($sms->{num});
	$self->begin_work();
	$self->update_link_map_($sms->{abonent},$sms->{smsc_id},$sms->{link_id}) if ($sms->{link_id});
	my $operator_id = $sms->{operator_id} || $self->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});
	$self->do('insert into tr_insmsa (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())',
			  undef,$sms->{abonent},$sms->{smsc_id},
			  $operator_id,
			  $sms->{num},$sms->{msg},$sms->{data_coding},
			  $sms->{esm_class},$sms->{transaction_id},$sms->{ussd_service_op},
			  $sms->{sar_msg_ref_num},$sms->{sar_total_segments},
			  $sms->{sar_segment_seqnum},$sms->{user_message_reference},
			  $sms->{link_id},$sms->{pdu});
	$self->do('insert into tr_insms (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date,id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),last_insert_id())',
			  undef,$sms->{abonent},$sms->{smsc_id},
			  $operator_id,
			  $sms->{num},$sms->{msg},$sms->{data_coding},
			  $sms->{esm_class},$sms->{transaction_id},$sms->{ussd_service_op},
			  $sms->{sar_msg_ref_num},$sms->{sar_total_segments},
			  $sms->{sar_segment_seqnum},$sms->{user_message_reference},
			  $sms->{link_id},$sms->{pdu});
	my $id = $self->get_last_insert_id();
	$self->commit();
	$self->add_sms_to_statistics($operator_id,$sms->{num},'income');
	return $id;
}

# ���������� ����������������� ��������� � ��������� �������.
# ������������ - SMPP_SMSClient::process_data_pdu()
sub insert_sms_into_insms_ignored {
	my ($self,$sms) = @_;
	$self->begin_work();
	$self->update_link_map_($sms->{abonent},$sms->{smsc_id},$sms->{link_id}) if ($sms->{link_id});
	$self->do('insert into tr_insms_ignored (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date) values (?,?,determine_operator(?,?,?),?,?,?,?,?,?,?,?,?,?,?,?,now())',
			  undef,$sms->{abonent},$sms->{smsc_id},
			  $sms->{abonent},$sms->{smsc_id},$sms->{link_id},
			  $sms->{num},$sms->{msg},$sms->{data_coding},
			  $sms->{esm_class},$sms->{transaction_id},$sms->{ussd_service_op},
			  $sms->{sar_msg_ref_num},$sms->{sar_total_segments},
			  $sms->{sar_segment_seqnum},$sms->{user_message_reference},
			  $sms->{link_id},$sms->{pdu});
	my $id = $self->get_last_insert_id();
	$self->commit();
	return $id;
}

# .
# ������������ -
sub move_ignored_to_insms {
	my ($self,$id) = @_;
	unless (defined $id) {die 'AltDB: id not defined';}
	$self->begin_work();
	my ($operator_id,$num) = $self->{db}->selectrow_array('select operator_id,num from tr_insms_ignored where id=?',undef,$id);
	$self->do('insert into tr_insmsa (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date) select abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date from tr_insms_ignored where id=?',undef,$id);
	$self->do('insert into tr_insms (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date,id) select abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,user_message_reference,link_id,pdu,date,last_insert_id() from tr_insms_ignored where id=?',undef,$id);
	my $new_id = $self->get_last_insert_id();
	$self->do('delete from tr_insms_ignored where id=?',undef,$id);
	$self->commit();
	$self->add_sms_to_statistics($operator_id,$num,'income');
	return $new_id;
}

# ������� ����� ��������� � INMMSA
# ������������: MMS_CLIENT
sub insert_mms_into_inmmsa {
	my ($self,$msg,$data,$content) = @_;
	$self->begin_work();
	$self->do('insert into tr_inmmsa (abonent,smsc_id,operator_id,num,transaction_id,content,data,date) values (?,?,determine_operator(?,?,?),?,?,?,?,now())',
			  undef,$msg->{abonent},$msg->{smsc_id},$msg->{abonent},$msg->{smsc_id},$msg->{link_id},$msg->{num},$msg->{transaction_id},$content,$data);
	my $id = $self->get_last_insert_id();
	if ($msg->{content}) {
		foreach my $i (keys(%{$msg->{content}})) {
			$self->do('insert into tr_inmmsa_data (inmmsa_id,content_id,content_type,filename,data) values (?,?,?,?,?)',
					  undef,$id,$msg->{content}->{$i}->{content_id},
					  $msg->{content}->{$i}->{content_type},$msg->{content}->{$i}->{filename},
					  $msg->{content}->{$i}->{data});
        }
	}
	$self->commit();
	return $id;
}


sub insert_mms_into_inbox {
	my ($self,$msg) = @_;
	$self->begin_work();
	$self->do("insert into tr_inboxa (abonent,smsc_id,operator_id,num,msg,msg_type,transaction_id,transport_type,transport_ids,transport_count,date) values (?,?,determine_operator(?,?,?),?,?,'mms',null,'mms',?,1,now())",
			  undef,$msg->{abonent},$msg->{smsc_id},$msg->{abonent},$msg->{smsc_id},$msg->{link_id},$msg->{num},$msg->{text},$msg->{id});
	$self->do("insert into tr_inbox  (id,abonent,smsc_id,operator_id,num,msg,msg_type,transaction_id,transport_type,transport_ids,transport_count,date) values (last_insert_id(),?,?,determine_operator(?,?,?),?,?,'mms',null,'mms',?,1,now())",
			  undef,$msg->{abonent},$msg->{smsc_id},$msg->{abonent},$msg->{smsc_id},$msg->{link_id},$msg->{num},$msg->{text},$msg->{id});
	my $id = $self->get_last_insert_id();
	$self->commit();
	return $id;
}


# ������� ��������� �� INSMS � INBOX
# ������������: DECODER
sub move_insms_to_inbox {
	my ($self,$msg) = @_;
	my $ids;
	my $supposed_count;
	for (my $i=1; $i<=$msg->{count}; $i++) {
        if (exists($msg->{$i})) {
			push(@$ids,$msg->{$i}{id});
			push(@$ids,$msg->{$i}{duplicate_id}) if ($msg->{$i}{duplicate_id});
			$supposed_count = $msg->{$i}{supposed_sms_count} if ($msg->{$i}{supposed_sms_count});
		}
	}
	my $sms_list = join(',',@$ids);
	my $sms_count = $supposed_count || scalar(@$ids);
	my $last_num = $msg->{last_num};
	my $test_flag = ($self->get_value('select abonent from set_optest_number_pool where abonent=?',undef,$msg->{$last_num}{abonent})) ? 10 : 0;
	my $date = POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
	$self->begin_work();
	$self->do("insert into tr_inboxa (abonent,smsc_id,operator_id,num,msg,msg_type,transaction_id,transport_type,transport_ids,transport_count,test_flag,date) values (?,?,?,?,?,?,?,?,?,?,?,?)",
			  undef,$msg->{$last_num}{abonent},$msg->{$last_num}{smsc_id},
			  $msg->{$last_num}{operator_id},$msg->{$last_num}{num},
			  $msg->{msg},$msg->{msg_type},$msg->{$last_num}{transaction_id},
			  $msg->{$last_num}{transport_type},
			  $sms_list,$sms_count,$test_flag,$date);
	$self->do("insert into tr_inbox  (id,abonent,smsc_id,operator_id,num,msg,msg_type,transaction_id,transport_type,transport_ids,transport_count,test_flag,date) values (last_insert_id(),?,?,?,?,?,?,?,?,?,?,?,?)",
			  undef,$msg->{$last_num}{abonent},$msg->{$last_num}{smsc_id},
			  $msg->{$last_num}{operator_id},$msg->{$last_num}{num},
			  $msg->{msg},$msg->{msg_type},$msg->{$last_num}{transaction_id},
			  $msg->{$last_num}{transport_type},
			  $sms_list,$sms_count,$test_flag,$date);
	my $id = $self->get_last_insert_id();
	$self->do('delete from tr_insms where id=?',undef,$_) foreach (@$ids);
	$self->commit();
	return $id;
}

# ������� ��������� �� INSMS � INBOX_BAD
# ������������: DECODER
sub move_insms_to_inbox_bad {
	my ($self,$sms) = @_;
	$self->begin_work();
	$self->do("insert into tr_insms_bad select * from tr_insms where id=?",undef,$sms->{id});
	$self->do('delete from tr_insms where id=?',undef,$sms->{id});
	$self->commit();
	return 1;
}


# ������� ��������� �� OUTBOX � OUTSMS
# ������������: ENCODER
sub move_outbox_to_outsms {
	my ($self,$msg) = @_;
	my $parts = $msg->{parts};
	my $pdus = $msg->{pdus};
	my @ids;
	my $table_name = 'tr_outsms';
#	$table_name = 'tr_outsms_low_priority'
#		if (	!$msg->{inbox_id} 
#			and ({20=>1,520=>1,501=>1,502=>1,503=>1,504=>1,505=>1,511=>1,512=>1,513=>1,514=>1,515=>1}->{$msg->{smsc_id}}) 
#			and ({1596=>1,1644=>1,1646=>1,1648=>1,1650=>1,1689=>0,1707=>1,1717=>1}->{$msg->{service_id}}));
	$self->begin_work();
	if ($pdus) {
		for (my $i=0; $i<@$pdus; $i++) {
			$self->do("insert into $table_name (abonent,smsc_id,operator_id,num,pdu,data_coding,esm_class,transaction_id,ussd_service_op,tariff,inbox_id,outbox_id,push_id,insms_id,registered_delivery,test_flag,params,delivery_time,date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,adddate(now(),INTERVAL ? SECOND),now())",
					  undef,$msg->{abonent},$msg->{smsc_id},$msg->{operator_id},$msg->{num},$pdus->[$i]->{pdu},
					  $msg->{data_coding},$msg->{esm_class},$msg->{transaction_id},
					  $msg->{ussd_service_op},$msg->{tariff},$msg->{inbox_id},$msg->{id},
					  $msg->{push_id},$pdus->[$i]->{id},$msg->{registered_delivery},
					  $msg->{test_flag},$msg->{params},
					  int($i*2) );
			push(@ids,$self->get_last_insert_id());
		}
	}
	elsif ($parts) {
		my $delay = 1;
		my $parts_count = @$parts;
		for (my $i=0; $i<$parts_count; $i++) {
			$self->do("insert into $table_name (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,inbox_id,outbox_id,push_id,insms_id,registered_delivery,test_flag,params,delivery_time,date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,adddate(now(),INTERVAL ? SECOND),now())",
					  undef,$msg->{abonent},$msg->{smsc_id},$msg->{operator_id},$msg->{num},$parts->[$i],
					  $msg->{data_coding},$msg->{esm_class},$msg->{transaction_id},
					  $msg->{ussd_service_op},$msg->{sar_msg_ref_num},$parts_count,$i+1,
					  $msg->{tariff},$msg->{inbox_id},$msg->{id},
					  $msg->{push_id},$msg->{insms_id},$msg->{registered_delivery},
					  $msg->{test_flag},$msg->{params},
					  int($i*$delay) );
			push(@ids,$self->get_last_insert_id());
		}
		$self->redis_init_outbox_status($msg->{id},$parts_count);
	}
	$self->do('delete from tr_outbox where id=?',undef,$msg->{id}) if (defined($msg->{id}));
	$self->commit();
	return @ids;
}

sub set_dr_to_outsms {
	my ($self,$message_id,$message_state,$done_date) = @_;
	dbh(undef, sub {
		my $db = $_[0];

		$db->begin_work;
		my $msgs = $db->{db}->selectall_hashref("select * from tr_insmsa where id=?",'id',undef,$message_id);
		my $id;
		my $sms = $msgs->{$message_id};
		if ($sms) {
			my $params = dumper_save({dr => {message_state => $message_state, message_id => $sms->{id}}});
			my $msg = "";
			$db->do("insert into tr_outsms (abonent,smsc_id,operator_id,num,msg,data_coding,esm_class,transaction_id,ussd_service_op,sar_msg_ref_num,sar_total_segments,sar_segment_seqnum,tariff,inbox_id,outbox_id,push_id,insms_id,registered_delivery,test_flag,params,delivery_time,date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now())",
				undef,$sms->{abonent},$sms->{smsc_id},$sms->{operator_id},$sms->{num},
				"",0,4,undef,undef,0,1,1,undef,0,0,undef,undef,0,0,$params);
			$id = $db->get_last_insert_id();
		}
		$db->commit;
		return $id;
	});
}
		
sub update_blacklist {
	my ($self,$abonent,$time) = @_;
	$self->begin_work();
	$self->do('replace into tr_blacklist (abonent,blacklist_timestamp) values (?,?)',undef,$abonent,$time);
	$self->commit();
	return 1;
}

=head2 clone_outbox($outbox_id, $overrides) -> integer

������ � ������� �� �������� ���������, �������� ������ �� ����������
���������� ������������� ��������� � ������ ����������, �������
���������� ���������. ����� ����������� ����������� ������.

��������, ��������� ������� ��������� � ��� ������������� ���������
��� ����, �� � ���������� ������� � ��������� ��������������� ����������:

 $db->clone_outbox(182375, msg => '����� ���������', transaction_id => undef);

���������� outbox_id ������ ��������� ��� undef � ������ �������.

=cut
sub clone_outbox {
    my $self = shift;
    my $outbox_id = shift;
    my %overrides = @_;

    my $existing = $self->{db}->selectrow_hashref(
        "select * from tr_outboxa where id = ?", undef, $outbox_id
    );
    return if not $existing;

    my $existing_billing = $self->{db}->selectrow_hashref(
        "select * from tr_billing where outbox_id = ? limit 1", undef,
        $outbox_id,
    );
    return if not $existing_billing;

    while (my($k, $v) = each %overrides) {
        if (exists $existing->{$k}) {
            $existing->{$k} = $v;
        }
        if (exists $existing_billing->{$k}) {
            $existing_billing->{$k} = $v;
        }
    }

    $self->begin_work();

    # �����
    delete $existing->{id}; # Remove primary key
    $self->{db}->do(
        "insert into tr_outboxa set ".
            join(q{,}, map { qq{$_ = ?} } keys %$existing),
        undef, values %{$existing}
    );
    my $new_outbox_id = $self->get_last_insert_id();
    $existing->{id} = $new_outbox_id;

    # �������
    $existing_billing->{outbox_id} = $new_outbox_id;
    $self->{db}->do(
        "insert into tr_billing set ".
            join(q{,}, map { qq{$_ = ?} } keys %$existing_billing),
        undef, values %{$existing_billing}
    );

    # �������
    $self->{db}->do(
        "insert into tr_outbox set ".
            join(q{,}, map { qq{$_ = ?} } keys %$existing),
        undef, values %{$existing}
    );
    $self->commit;

    return $new_outbox_id;
}


sub resend_through_smstraffic {
   my ($self,$outbox_id, $smsc_id) = @_;
   my $status = $self->get_value('select resend_through_smstraffic(?,?)',undef,$outbox_id, $smsc_id);
   return $status;
}

sub resend_through_smstraffic_with_num {
   my ($self,$outbox_id, $smsc_id, $num) = @_;
   my $status = $self->get_value('select resend_through_smstraffic_with_num(?,?,?)',undef,$outbox_id, $smsc_id, $num);
   return $status;
}


sub update_link_map_ {
	my ($self,$abonent,$smsc_id,$link_id) = @_;
	$self->do('select update_link_map(?,?,?)',undef,$abonent,$smsc_id,$link_id);
	return 1;
}

sub get_link_id {
	my ($self,$abonent,$smsc_id) = @_;
	my $link_id = $self->get_value('select link_id from tr_link_map where abonent=? and smsc_id=?',undef,$abonent,$smsc_id);
	return $link_id;
}

sub get_link_id_by_op {
	my ($self,$operator_id,$smsc_id) = @_;
	my $link_id = $self->get_value('select link_id from set_link_pool where operator_id=? and smsc_id=? limit 1',undef,$operator_id,$smsc_id);
	return $link_id;
}

sub is_blocked {
	my ($self,$abonent,$shortcode,$type) = @_;
	return $self->get_value('select err_num from set_blocked_abonent where abonent=? and (shortcode=? or shortcode is null) and (type=? or type is null)',undef,$abonent,$shortcode,$type);
}

# ���������, ������������ �� ������� ��� ���������
sub is_blocked_by_pref {
	my ($self, $operator_id, $msg) = @_;
	my $db = $self->{db};

	return 1 unless ($operator_id);

	my $k = "operator{$operator_id}_block_by_pref";
	unless ($self->{$k} and ($self->{$k}{last_update_time} + 5*60 > time)) {
		my $hr = $db->selectall_hashref("select id, pref, is_reg_exp from set_blocked_pref where operator_id = $operator_id", 'id');
		my $pattern_c;
		my $pattern = '';
		foreach (keys(%$hr)) {
			if ($hr->{$_}{is_reg_exp} eq 'Y') {
				eval { $pattern_c = qr/$hr->{$_}{pref}/i; };
				if ($@) {
					yell($@, code => 'ALERT', header => $@, process => "$0($$)");
				} else {
					$pattern .= '|' if ($pattern);
					$pattern .= $hr->{$_}{pref};
				}
			} else {
				$pattern .= '|' if ($pattern);
				$pattern .= quotemeta($hr->{$_}{pref});
			}
		}
		if ($pattern) {
			$pattern = '^\s*('.$pattern.')';
			eval { $pattern_c = qr/$pattern/i; };
			if ($@) {
				yell($@, code => 'ALERT', header => $@, process => "$0($$)");
			}
		}
		$self->{$k}{pattern} = $pattern_c;
		$self->{$k}{last_update_time} = time;
	}

	if ($self->{$k}{pattern} && $msg =~ m/$self->{$k}{pattern}/i) {
		return 1;
	}
	return 0;
}

# ����������� ���������
# ���������� �������� operator_id
sub determine_operator {
	my ($self, $abonent, $smsc_id, $link_id) = @_;
	my $operator_id = $self->get_value('select determine_operator(?,?,?)', undef, $abonent, $smsc_id, $link_id);
	return $operator_id;
}

# ���������� �������� last_insert_id
# ������������: AltDB
sub get_last_insert_id {
	my ($self) = @_;
	my ($id) = $self->{db}->selectrow_array('select last_insert_id()');
	return $id;
}

sub load_outbox_by_outsms_id {
	my ( $self, $outsms_id ) = @_;
	my $a = $self->{db}->selectrow_hashref("select b.* from tr_outboxa b, tr_outsmsa s where s.outbox_id = b.id and s.id = ? limit 1", undef, $outsms_id);
	return $a;
}

sub add_sms_to_statistics {
	my ($self, $operator_id, $num, $type) = @_;
	$self->{statistics}->{int(time()/3600)}->{$operator_id}->{$num}->{$type}++;
}

sub save_statistics {
	my ($self,$pr_name,$smsc_id) = @_;
	my $current_hour = int(time()/3600);
	foreach (keys(%{$self->{statistics}})) {
        my $hour = $_;
		foreach (keys(%{$self->{statistics}->{$hour}})) {
			my $operator_id=$_;
			foreach (keys(%{$self->{statistics}->{$hour}->{$operator_id}})) {
				my $num=$_;
				foreach (keys(%{$self->{statistics}->{$hour}->{$operator_id}->{$num}})) {
					my $status=$_;
					$self->begin_work();
					eval {
						$self->do('replace into tr_sms_statistics (hour,operator_id,num,status,count,smsc_id,pr_name) values (?,?,?,?,?,?,?)',undef,
								  $hour,$operator_id,$num,$status,
								  $self->{statistics}->{$hour}->{$operator_id}->{$num}->{$status},
								  $smsc_id,$pr_name);
					};
					if ($@) {
						$self->rollback();
						$self->begin_work();
						$self->do('replace into tr_sms_statistics (hour,operator_id,num,status,count,smsc_id,pr_name) values (?,?,?,?,?,?,?)',undef,
								  $hour,$operator_id,$num,$status,
								  $self->{statistics}->{$hour}->{$operator_id}->{$num}->{$status},
								  $smsc_id,$pr_name);
					}
					$self->commit();
				}
			}
		}
		delete $self->{statistics}->{$hour} if ($hour != $current_hour);
	}
}

sub load_statistics {
	my ($self,$pr_name) = @_;
	my $db = $self->{db};
	my $current_hour = int(time()/3600);
	my $tmp = $db->selectall_arrayref("select hour,operator_id,num,status,count from tr_sms_statistics where pr_name=? and hour=?",
									  undef,$pr_name,$current_hour);
	$self->{statistics}->{$_->[0]}->{$_->[1]}->{$_->[2]}->{$_->[3]} = $_->[4] foreach (@$tmp);
}

sub clear_resend_flag {
	my ($self, $outbox_id) = @_;
	$self->do("update tr_outboxa set resend_flag = null where id = ?", undef, $outbox_id);
}


sub get_db {
	return $altdb_handler;
}

sub clone
{
	my($self) = @_;
	$self->{db}->{InactiveDestroy} = 1; #fork-safe handle
	return Disp::AltDB->new($self->{conf});
}

my @dbases;
sub db_transports
{
	unless (@dbases)
	{
		while (my($host, $attr) = each %{Config->section('db_transport')})
		{
			eval {
				if ($host eq Config->param('local_db_host')) {
					$dbases[0]->{$host} = Disp::AltDB->new();
					unshift @{$dbases[1]}, $host;
				}
				else {
					$dbases[0]->{$host} = Disp::AltDB->new($attr);
					push @{$dbases[1]}, $host;
				}
			};
			if ($@) {
				yell($@, code => 'ALERT', header => $@, process => "$0($$)");
			}
		}
	}
	return @dbases;
}

sub dbh ($&)
{
	my($host, $fn) = @_;
	my($dbases, $hosts) = db_transports();

	if ($host && $host ne 'ANYWHERE') {
		my $db;
		if (exists $dbases->{$host}) {
			$db = $dbases->{$host};
			$db->reconnect;
		}
		else {
			my $msg = "database on $host not found, using local database";
			yell($msg,
				code => 'ALERT', header => $msg, process => "$0($$)");

			$db   = Disp::AltDB->new;
			$host = Config->param('local_db_host');
		}
		&$fn($db, $host);
	}
	else {
		for $host (@{$hosts})
		{
			my $db = $dbases->{$host};
			$db->reconnect;
			&$fn($db, $host) and last;
		}
	}
}

use Redis;
sub redis_connect {
	my ($self) = @_;
	eval { 
		$self->{redis} = Redis->new(server => Config->param('server','redis'), encoding => undef);
	};
	delete $self->{redis} if ($@);
}

sub redis_report {
	my ($self,$sms,$params)=@_;
#	return unless ((501 <= $sms->{smsc_id}) and ($sms->{smsc_id} <= 505));
	$self->redis_connect unless ($self->{redis});
	my $msg = dumper_save({
				outbox_id => $sms->{outbox_id},
				inbox_id => $sms->{inbox_id},
				push_id => $sms->{push_id},
				status => $sms->{outbox_status},
				paid => $sms->{paid},
				date => $sms->{delivered_date},
				service_id => $sms->{params}{service_id}
			});
	eval {
		$self->{redis}->lpush('dr_queue',$msg);
	};
	if ($@) {
		$self->redis_connect;
		$self->{redis}->lpush('dr_queue',$msg);
	}
}

sub redis_disconnect {
	my ($self) = @_;
	eval {
		$self->{redis}->quit if ($self->{redis});
		delete $self->{redis};
	};
}

sub new_outbox_delivery_status	{
	my ($self,$sms) = @_;
	return $sms->{status} if ($sms->{sar_total_segments} == 1);
	return undef unless ($sms->{outbox_id});

	use Log::Log4perl qw(get_logger);
	my $log = get_logger('');
	$log->debug("STATUS MSG$sms->{outbox_id}");
	my $status_data = $self->redis_get_outbox_status($sms->{outbox_id},$sms->{sar_total_segments});
	my $list = $status_data->{list};
	my $old_status = $status_data->{status};
	my $old_list_str = join(',',@$list);

	$list->[$sms->{sar_segment_seqnum}-1] = $sms->{status};
#		if ($sms->{status} > $list->[$sms->{sar_segment_seqnum}-1]);
	$self->redis_set_outbox_status($sms->{outbox_id},$status_data);
	my $new_status = $status_data->{status};
	$log->info("STATUS MSG$sms->{outbox_id} $old_status($old_list_str) -> $new_status(".join(',',@$list).")");
	return 0 if ($old_status == $new_status);
	return $new_status;
}


sub redis_init_outbox_status {
	my ($self,$outbox_id,$len) = @_;
	return unless ($outbox_id and $len);
	my @st = map {0} (1 .. $len);
	$self->redis_set_outbox_status($outbox_id,{list => \@st, status => 0});
}

sub redis_get_outbox_status {
	my ($self,$outbox_id,$len) = @_;
	$self->redis_connect unless ($self->{redis});
	my $data = $self->{redis}->hget(_outbox_status_hash_name($outbox_id),$outbox_id);
	if ($data) {
		$data = dumper_load($data);
		$data->{status} = _outbox_status_from_list($data->{list});
		return $data;
	}

	use Log::Log4perl qw(get_logger);
	my $log = get_logger('');
	$log->debug("STATUS MSG$outbox_id loading from db");
	my @st = map {0} (1 .. $len);
	my $tmp = $self->{db}->selectall_arrayref('select sar_segment_seqnum,status from tr_outsmsa where outbox_id=?', undef, $outbox_id);
	if (@$tmp) {
		$st[$_->[0]-1] = $_->[1] foreach (@$tmp);
	}
	return {list => \@st, status => _outbox_status_from_list(\@st) };
}

sub redis_set_outbox_status {
	my ($self,$outbox_id,$data) = @_;
	$self->redis_connect unless ($self->{redis});
	$data->{status} = _outbox_status_from_list($data->{list});
	$self->{redis}->hset(_outbox_status_hash_name($outbox_id),$outbox_id,dumper_save($data));
	$self->{redis}->expire(_outbox_status_hash_name($outbox_id),3600);
}

sub redis_del_outbox_status {
	my ($self,$outbox_id) = @_;
	$self->redis_connect unless ($self->{redis});
	$self->{redis}->hdel(_outbox_status_hash_name($outbox_id),$outbox_id);
}

sub _outbox_status_hash_name {
	my ($outbox_id) = @_;
	my $n = int ($outbox_id / 100000);
	return "outbox_status_$n";
}

sub _outbox_status_from_list {
	my ($list) = @_;
	my $status = 12;
	foreach (@$list) {
		if (!$_) { return 0; }
	}
	foreach (@$list) {
		if ($_ == 12) {
			next;
		} elsif ($_ == 13) {
			return 13;
		} elsif ($_ == 14) {
			return 14;
		} elsif ($_ == 15) {
			return 15;
		} elsif ($_ == 2) {
			$status = 2;
		} elsif ($_ == 11) {
			$status = 2;
		}  elsif (($_ == 10) and ($status ==12)) {
			$status = 10;
		} else {
			return 255; # or $_
		}
	}
	return $status;
}

1;
