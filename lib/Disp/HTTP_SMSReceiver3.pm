#!/usr/bin/perl
package Disp::HTTP_SMSReceiver3;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

# sub POE::Kernel::TRACE_DEFAULT  () { 1 }

use POE qw/Component::FastCGI_m/;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use XML::Simple;
use Time::HiRes qw/gettimeofday tv_interval/;
use Crypt::OpenSSL::RSA;

use base qw 'Disp::Object';

use Disp::Utils;
use Disp::Config;
use Disp::AltDB;
use Disp::Operator;
use Disp::SMSCTransaction;

use Encode;

use constant process_tab =>
  { http_eport => { init => \&eport_init,
					handle_request => \&eport_handle_request,
					handle_alarm => \&eport_handle_timer },
	http_cyberplat => { init => \&cyberplat_init,
						handle_request => \&cyberplat_handle_request,
						handle_alarm => \&cyberplat_handle_alarm },
	http_osmp => { init => \&osmp_init,
				   handle_request => \&osmp_handle_request,
				   handle_alarm => \&osmp_handle_alarm },
  };

sub new {
	my ($proto,$smsc_id,$debug_level,$smpp_mode, %options) = @_;
	my $self = $proto->SUPER::new();

	$self->{routines_interval} = $options{routines_interval} || 60;

	my $smpp_mode_name = 'rcv';
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);

	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf",
				 skip_db_connect => 1);

	my $log = init_log($pr_name, $debug_level);
	$log->info("Starting...");

	my $db = new Disp::AltDB;
	my $cnf = $db->get_smsc_configuration($smsc_id);

	$self->{db}				  = $db;
	$self->{log}			  = $log;
	$self->{pr_name}		  = $pr_name;
	$self->{smpp_mode_name}	  = $smpp_mode_name;
	$self->{state}			  = 'stopped';
	$self->{status}			  = 'start';
	$self->{smsc_id}		  = $smsc_id;
	$self->{debug_level}	  = $debug_level;
	$self->{smpp_mode}		  = $smpp_mode;

	@{$self}{keys(%$cnf)} = values(%$cnf);

	my $hndl = process_tab->{$self->{client_type}}->{init};
	if ( ref $hndl eq 'CODE' ) {
		$hndl->($self);
	}

	$self->enter_state("initialized");

	return $self;
}

sub run {
	my ($self) = @_;
	eval {
		$self->{log}->info("Listening...");

		POE::Component::FastCGI_m->new( Port => $self->{port},
										Address => '127.0.0.1',
										RoutinesInterval => $self->{routines_interval},
										HandleRequest => sub { $self->handle_request(@_); },
										HandleRoutines => sub { $self->handle_routines(@_) },
										HandleYell => sub { $self->{log}->error(@_); },
										HandleAlarm => sub { $self->handle_alarm(@_); });

		$self->{log}->info("Entering POE loop...");
		$self->enter_state("run");
		POE::Kernel->run();
		$self->enter_state("stopped");
	};

	if ($@) {
		$self->error("$@", "Uncaught exception");
	}
}

sub handle_routines {
	my ( $self ) = @_;
	my $t0 = [gettimeofday];
	$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
	$self->{db}->heartbeat($self->{pr_name},$self->{last_received_sms_time});
	my $elapsed = tv_interval($t0, [gettimeofday]);
	$self->{log}->info("ROUTINES($elapsed)");
}

sub handle_request {
	my ( $self, $session_id, $heap, $request, $input ) = @_;

	$self->{log}->debug("FastCGI request is: ".Dumper($input));

	$self->{db}->reconnect(); # XXX more clever
	$heap->{session_id} = $session_id;
	my $hndl = process_tab->{$self->{client_type}}->{handle_request};
	return unless ref $hndl eq 'CODE';

	$self->{log}->debug("[$session_id] REQUEST: ".Dumper(scalar $request->Vars));

	return $hndl->($self, $heap, $request);
}

sub handle_alarm {
	my ( $self, $session_id, $heap ) = @_;

	my $hndl = process_tab->{$self->{client_type}}->{handle_alarm};

	return unless ref $hndl eq 'CODE';

	return $hndl->($self, $heap);
}

sub init_log {
	my ( $pr_name, $debug_level ) = @_;

	my %log_conf =
	  (
	   "log4perl.rootLogger"									 => "$debug_level, Logfile",
	   "log4perl.appender.Logfile"								 => "Log::Log4perl::Appender::File",
	   "log4perl.appender.Logfile.recreate"						 => 1,
	   "log4perl.appender.Logfile.recreate_check_interval"		 => 300,
	   "log4perl.appender.Logfile.recreate_check_signal"		 => 'USR1',
	   "log4perl.appender.Logfile.umask"						 => "0000",
	   "log4perl.appender.Logfile.filename"						 => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
	   "log4perl.appender.Logfile.layout"						 => "Log::Log4perl::Layout::PatternLayout",
	   "log4perl.appender.Logfile.layout.ConversionPattern"		 => Config->param('log_pattern','sms_client')
	  );
	Log::Log4perl->init( \%log_conf);
	return get_logger("");
}

sub enter_state {
	my ( $self, $state ) = @_;
	$self->{db}->set_process_state($self->{pr_name},uc($state),uc($self->{state}));
	$self->{state} = $state;
	$self->{log}->info("Entering state '$state'");
}

sub error {
	my ( $self, $message, $header ) = @_;

	$self->{log}->error($message);

	yell($message,
		 code => 'ERROR',
		 process => $self->{pr_name},
		 header => "$self->{pr_name}: $header",
		 ignore => { log4perl => 1 });
}

sub warning {
	my ( $self, $message, $header ) = @_;

	$self->{log}->warn($message);

	yell($message,
		 code => 'WARNING',
		 process => $self->{pr_name},
		 header => "$self->{pr_name}: $header",
		 ignore => { log4perl => 1 });
}

sub http_response {
	my ( $status, $body, %args ) = @_;

	my $status_line = "Status: $status";
	my $content_type = $args{content_type} || 'text/plain';
	$content_type .= "; charset=$args{charset}" if $args{charset};

	return [ "$status_line\r\nContent-type: $content_type\r\n\r\n$body" ];
}

################################################################################
# E-port
################################################################################
sub eport_init {
	my ( $self ) = @_;

	$self->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};

	$self->{our_rsa_private_key} = Crypt::OpenSSL::RSA->new_private_key
	  ( read_file(Config->param('system_root')."/etc/rsa_private_$self->{smsc_id}.key") );

	$self->{remote_rsa_public_key} = Crypt::OpenSSL::RSA->new_public_key
	  ( read_file(Config->param('system_root')."/etc/rsa_public_$self->{smsc_id}.key") );

	# $self->{remote_rsa_public_key}->use_md5_hash;

	$self->load_num_prices;
}

my $eport_date_regexp = qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[-+]\d{2}$/;
my $eport_id_regexp = qr/^\d+$/;
my $eport_sum_regexp = qr/^\d+\.\d{2}$/;
my $eport_account_regexp = qr/^7\d{10}$/;

my %eport_requests_validators = ( payment => [ id => $eport_id_regexp,
											   pay_time => $eport_date_regexp,
											   account => $eport_account_regexp,
											   sum => $eport_sum_regexp,
											   service => qr/^./,
											   timestamp => $eport_date_regexp ],
								  check => [ account => qr/^\d+$/,
											 service => qr/^./,
											 sum => $eport_sum_regexp,
											 timestamp => $eport_date_regexp ],
								  status => [ id => $eport_id_regexp,
											  timestamp => $eport_date_regexp ],
								  revoke => [ id => $eport_id_regexp,
											  timestamp => $eport_date_regexp ]
								);

sub eport_request_validate {
	my ( $self, $params ) = @_;

	my $validator = $eport_requests_validators{$params->{type}};
	return unless $validator;

	my $i = 0;
	while ($i < $#$validator) {
		my ( $key, $reg ) = ( $validator->[$i], $validator->[$i+1]);

		unless ( $params->{$key} =~ $reg ) {
			$self->{log}->error("$key param invalid");
			return;
		}
		$i += 2;
	}
	return 1;
}

sub eport_handle_request {
	my ( $self, $heap, $q ) = @_;

	my %params = $q->Vars;

	unless ($self->eport_request_validate(\%params)) {
		return $self->eport_response(\%params,
									 { result => 'E1',
									   reason => 'Not all mandatory fields present or have apropriate value' });
	}

	my $sign_format_valid = 0;

	unless ($params{sign} and ($params{sign} =~ /^[0-9A-Z]+$/i) and (length($params{sign}) % 2 == 0)) {
		return $self->eport_response(\%params,
									 { result => 'E1',
									   reason => 'No signature param or invalid signature format' });
	}

	# Проверка подписи
	my ($sign_valid, $sign_comment) = $self->eport_sign_validate(\%params);
	unless ($sign_valid) {
		$self->error("Can't validate signature: \n$sign_comment\nRequest is: ".Dumper(\%params), "Problem while validating signature");
		return $self->eport_response(\%params,
									 { result => 'E3',
									   reason => 'Invalid signature' });
	}

	if ($params{type} =~ /^(check|payment)/) {

		my $abonent = $params{account};
		my $msg = $params{service};

# 		if ($abonent !~ /^7(9265550017|9261193892)$/ or $msg !~ /^\s*\d{3}\s*$/) {
# 			$abonent = undef;
# 		}

		unless ($abonent) {
			$self->warning("Invalid account '$params{account}'/'$params{service}'",
						   "Invalid account string format");
			return $self->eport_response(\%params,
										 { result => 'F3',
										   reason => 'Invalid account' });
		}

		# Сумма в заданных пределах
		if ($params{sum} < $self->{min_price}) {
			$self->warning("Price '$params{sum}' lower than minimum '$self->{min_price}'",
						   "Price problem: sum lower than minimum");
			return $self->eport_response(\%params,
										 { result => 'F1',
										   reason => 'Price is less then allowed' });
		}

		if ($params{sum} > $self->{max_price}) {
			$self->warning("Price '$params{sum}' greater than maximum '$self->{max_price}'",
						   "Price problem: sum greater than maximum");
			return $self->eport_response(\%params,
										 { result => 'F2',
										   reason => 'Price is greater then allowed' });
		}

		# Сумма преобразуется в короткий номер
		my $num = $self->sum_to_num($params{sum});
		unless ($num) {
			$self->warning("Can't convert sum '$params{sum}' to short number",
						   "Price problem: cant convert sum to num");
			return $self->eport_response(\%params,
										 { result => 'F4',
										   reason => 'Incorrect price' });
		}

		# Для запросов check на этом всё заканчивается
		if ($params{type} eq 'check') {
			$self->{log}->debug("Check passed: num-$num, abonent-$abonent, text-$msg for params:\n".Dumper(\%params));
			return $self->eport_response(\%params,
										 { result => 'S1',
										   reason => 'All fields looks correct' });
		}

		unless ($params{id} =~ /^\d+$/) {
			$self->warning("Incorrect 'id' format: $params{id}",
						   "Incorrect 'id' format");
			return $self->eport_response(\%params,
										 { result => 'E1',
										   reason => 'Invalid id format' });
		}

		my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $params{id});
		my $insms_id;

		unless ($trans) {
			my $sms = { smsc_id => $self->{smsc_id},
						num => $num,
						abonent => $abonent,
						msg => encode('cp1251',decode('koi8-r',$msg)),
						data_coding => 0,
						esm_class => 0,
						transaction_id => $params{id}
					  };

			eval {
				$insms_id = $self->{db}->insert_sms_into_insms($sms);
				$trans = Disp::SMSCTransaction->new({ smsc_id => $self->{smsc_id},
													  transaction_id => $params{id},
													  insms_id => $insms_id,
													  status => 0,
													  date => today(),
													  exact_price => $params{sum},
													  parameters => { pay_time => $params{pay_time},
																	  account => $params{account},
																	  sum => $params{sum},
																	  timestamp => $params{timestamp}
																	}
													});
				$trans->save;
			};

			if ($@) {
				$self->error("Can't store message to DB: $@", "SMS insertion failed");
				eval { Config->dbh->rollback; };
				return $self->eport_response(\%params,
											 { result => 'E2',
											   reason => 'Couldnt store to DB' });
			}

			$self->{log}->info("[$heap->{session_id}] SMS$insms_id recieved: from '$abonent' to '$num' with text '$msg'");
		}

		if ($trans->{status} == 3) {
			return $self->eport_response(\%params,
										 { result => 'F5',
										   reason => 'Already handled' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $self->eport_response(\%params,
										 { result => 'F7',
										   reason => 'Already handled and revoked' });
		}

		$heap->{insms_id} = $insms_id;
		$heap->{trans} = $trans;
		$heap->{params} = \%params;

		return 5;

	} elsif ($params{type} eq 'status') {
		my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $params{id});

		unless ($trans) {
			return $self->eport_response(\%params,
										{ result => 'F6',
										  reason => 'No such transaction' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $self->eport_response(\%params,
										{ result => 'S2',
										  pay_time => $trans->{parameters}{pay_time},
										  accepted => eport_date($trans->{date}),
										  reason => 'Transaction cancelled',
										  account => $trans->{parameters}{account},
										  sum => $trans->{parameters}{sum},
										  revoked => $trans->{parameters}{revoke_date},
										});
		}

		if ($trans->{status} == 3) {
			return $self->eport_response(\%params,
										{ result => 'S1',
										  pay_time => $trans->{parameters}{pay_time},
										  reason => 'Transaction handled successfully',
										  account => $trans->{parameters}{account},
										  sum => $trans->{parameters}{sum},
										  accepted => eport_date($trans->{date}),
										});
		}
		return $self->eport_response(\%params,
									 { result => 'E4',
									   reason => 'Status not known yet' });
	} elsif ($params{type} eq 'revoke') {
		my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $params{id});

		unless ($trans) {
			return $self->eport_response(\%params,
										{ result => 'F6',
										  reason => 'No such transaction' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $self->eport_response(\%params,
										{ result => 'F7',
										  reason => 'Already revoked' });
		}

		if ($trans->{status} == 3) {
			$trans->{status} = 1;
			$trans->{parameters}{revoke_date} = eport_date(today());

			$trans->save;

			return $self->eport_response(\%params,
										{ result => 'S2',
										  reason => 'Transaction revoked',
										  revoked => $trans->{parameters}{revoke_date},
										});
		}
		return $self->eport_response(\%params,
									 { result => 'E4',
									   reason => 'Processing not completed yet' });
	}

	my $resp =  $self->eport_response(\%params,
									  { result => 'E1',
										reason => 'Unsupported' });
	return $resp;
}

sub eport_handle_timer {
	my ( $self, $heap ) = @_;

	$self->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$heap->{trans}{transaction_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			# $self->{db}->resend_through_smstraffic($v->{outbox_id}, $self->{sms_traffic_smsc_id});
			$self->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$self->{db}->set_sms_delivered_status($v->{id});
			$self->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$self->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$self->{sms_traffic_smsc_id}");

			$self->{log}->debug("NUM is $v->{num}");

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return $self->eport_response($heap->{params},
										 { result => ($v->{num} eq 'free' ? 'F4' : 'S1'),
										   reason => 'Successfully handled',
										   accepted => eport_date($heap->{trans}{date}) });
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 7) {
		$self->error("[$heap->{session_id}] No response for SMS$heap->{insms_id}, giving up",
					 "No response created in given timeframe");
		return $self->eport_response($heap->{params},
									 { result => 'E2',
									   reason => 'No response created in given timeframe' });
	}

	return 5;
}

sub eport_response {
	my ( $self, $orig, $params ) = @_;

	my %new_req = %$params;

	for (keys %$orig) {
		$new_req{$_} = $orig->{$_};
	}

	my $normalized = eport_normalize_request_string(\%new_req);
	my $sign = unpack("H*", $self->{our_rsa_private_key}->sign($normalized));

	return http_response('200 OK', "$normalized&sign=$sign",
						 content_type => 'application/x-www-form-urlencoded',
						 charset => 'windows-1251');
}

sub eport_sign_validate {
	my ( $self, $params ) = @_;


	$self->{log}->debug("OUR NORM: ".eport_normalize_request_string($params));

	my $valid;
	eval {
		$valid = $self->{remote_rsa_public_key}->verify(eport_normalize_request_string($params),
														pack("H*", $params->{sign}));
	};
	if ($@) {
		return ( undef, $@ );
	}
	return ($valid, "");
}

sub eport_sign_make {
	my ( $self, $params ) = @_;
}

sub eport_normalize_request_string {
	my ( $params ) = @_;
	my @out;
	foreach my $par (sort grep { $_ ne 'sign' } keys %$params  ) {
		my $tmp = $params->{$par};
		my $char_code;
		push @out, $par . "=" . eport_escape_string($params->{$par});
	}
	return join '&', @out;
}

my %eport_escape_tab;

BEGIN {
	for (0..255) {
		my $chr = chr;
		if ($chr =~ /[-_\.A-Za-z0-9]/) {
			$eport_escape_tab{$chr} = $chr;
		} else {
			$eport_escape_tab{$chr} = '%'.uc(unpack("H*", $chr));
		}
	}
# 	print Dumper(\%eport_escape_tab);
}

sub eport_escape_string {
	my ( $string ) = @_;

	join '', map {$eport_escape_tab{$_}} split //, $string;
}

sub eport_unescape_string {
	my ( $string ) = @_;

	$string =~ s/%([0-9A-F]{2})/chr(hex($1))/gei;
	return $string;
}

sub eport_split_account {
	my ( $string ) = @_;

	return unless $string =~ /^(7\d{10})\/(\d{3})/;

	return ( $1, $2 );
}

sub eport_date {
	my ( $date ) = @_;
	$date =~ s/\s/T/;
	$date .= "+03";
	return $date;
}

################################################################################
# Cyberplat
################################################################################
sub cyberplat_init {
	my ( $self ) = @_;

	$self->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
	$self->load_num_prices;
}

my %cyberplat_action_handlers = ( check => \&cyberplat_action_check,
								  payment => \&cyberplat_action_payment,
								  cancel => \&cyberplat_action_cancel,
								  status => \&cyberplat_action_status
								);

sub cyberplat_handle_request {
	my ( $self, $heap, $q ) = @_;

	my $action  = $q->param('action');
	my $handler = $cyberplat_action_handlers{$action};
	if ($handler) {
		return $handler->($self, $heap, $q);
	} else {
		return cyberplat_response(1, "Неизвестный тип запроса");
	}
}

sub cyberplat_handle_alarm {
	my ( $self, $heap ) = @_;
	$self->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$heap->{txn_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			$self->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$self->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$self->{sms_traffic_smsc_id}");
			$self->{db}->resend_through_smstraffic($v->{outbox_id}, $self->{sms_traffic_smsc_id});
			$self->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$self->{db}->set_sms_delivered_status($v->{id});

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return cyberplat_response(0,
									  "Платёж обработан",
									  authcode => $heap->{trans}{insms_id},
									  date => cyber_date($heap->{trans}{date}));
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 7) {
		$self->error("[$heap->{session_id}] No response for SMS$heap->{trans}{insms_id}, giving up",
					 "No service response in given time");
		return cyberplat_response(10,
								  "Временная неудача, запрос должен быть повторен позже");
	}

	return 5;
}

sub cyberplat_action_check {
	my ( $self, $heap, $q ) = @_;

	my $number = $q->param('number');
	my ( $abonent, $text ) = cyberplat_split_number($number);
	my $sum = $q->param('amount');

	unless ($abonent) {
		$self->warning("Can't extract abonent and text: $number", "Incorrect abonent format");
		return cyberplat_response(2, "Неправильный абонент");
	}

	my $num = $self->sum_to_num($sum);
	unless ( $num ) {
		$self->warning("Unknown sum: $sum", "Incorrect sum");
		return cyberplat_response(3, "Неправильная сумма");
	}

	$self->{log}->info("[$heap->{session_id}] Check passed: number - '$number', amount - '$sum'; converted to num - '$num'");

	return cyberplat_response(0, "Похоже на правду");
}

sub cyberplat_action_payment {
	my ( $self, $heap, $q ) = @_;

	my $number = $q->param('number');
	my ( $abonent, $text ) = cyberplat_split_number($number);
	my $sum = $q->param('amount');

	unless ($abonent) {
		$self->warning("Can't extract abonent and text: $number", "Incorrect abonent format");
		return cyberplat_response(2, "Неправильный абонент");
	}

	my $num = $self->sum_to_num($sum);
	unless ( $num ) {
		$self->warning("Unknown sum: $sum", "Incorrect sum");
		return cyberplat_response(3, "Для данного тарифа разрешено принимать только фиксированные суммы");
	}

	my $txn_id = $q->param("receipt");
	unless ($txn_id =~ /^\d+$/) {
		return cyberplat_response(4, "Поле receipt имеет неверный формат");
	}

	my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $txn_id);
	my $insms_id;

	unless ($trans) {
		my $sms = { smsc_id => $self->{smsc_id},
					num => $num,
					abonent => $abonent,
					msg => $text,
					data_coding => 0,
					esm_class => 0,
					transaction_id => $txn_id };
		$insms_id = $self->{db}->insert_sms_into_insms($sms);
		$trans = Disp::SMSCTransaction->new({ smsc_id => $self->{smsc_id},
											  transaction_id => $txn_id,
											  insms_id => $insms_id,
											  date => today(),
											  status => 0,
											  exact_price => $sum,
											  parameters => {} });
		$trans->save;
		$self->{log}->info("[$heap->{session_id}] SMS$insms_id recieved from '$abonent' to '$num' with text 'text'");
	}

	if ($trans->{status} == 3) {
		return cyberplat_response(0,
								  "Этот платёж уже был успешно проведён",
								  authcode => $trans->{insms_id},
								  date => cyber_date($trans->{date}));
	}

	if ($trans->{status} != 0) {
		$self->error("Payment $txn_id has incorrect status '$trans->{status}'",
					 "Incorrect transaction status");
		return cyberplat_response(11,
								  "Платёж в непонятном состоянии");
	}

	$heap->{trans} = $trans;
	$heap->{txn_id} = $txn_id;

	return 5;
}

sub cyberplat_action_cancel {
	my ( $self, $heap, $q ) = @_;

	my $receipt = $q->param('receipt');
	my $mes = $q->param('mes');

	my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $receipt);

	unless ($trans) {
		$self->warning("Can't load transaction '$receipt'", "cancel for unknown receipt");
		return cyberplat_response(4, "Такой транзакции нет в базе");
	}

	if ($trans->{status} == 1 or $trans->{status} == 2) {
		return cyberplat_response(0, "Платёж уже был отменён",
								  authcode => $trans->{insms_id},
								  date => cyber_date($trans->{date}));
	}

	$trans->{status} = 1;
	$trans->{parameters}{cancel_reason} = $mes;
	$trans->save;

	$self->{log}->info("[$heap->{session_id}] Canceled transaction $receipt");

	return cyberplat_response(0, "Платёж отменён",
							  authcode => $trans->{insms_id},
							  date => cyber_date($trans->{date}));
}

sub cyberplat_action_status {
	my ( $self, $heap, $q ) = @_;

	my $receipt = $q->param('receipt');
	my $mes = $q->param('mes');

	my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $receipt);

	unless ($trans) {
		$self->warning("Can't load transaction '$receipt'", "cancel for unknown receipt");
		return cyberplat_response(4, "Такой транзакции нет в базе");
	}

	if ($trans->{status} == 0) {
		return cyberplat_response(8, "Состояние платежа неопределено");
	}

	if ($trans->{status} == 3) {
		return cyberplat_response(0,
								  "Платёж успешно проведён",
								  date => cyber_date($trans->{date}),
								  authcode => $trans->{insms_id} );
	}

	return cyberplat_response(7, "Платёж отменён",
							  date => cyber_date($trans->{date}),
							  authcode => $trans->{insms_id} );

}

# helpers
sub cyber_date {
	my ( $string ) = @_;

	$string =~ s/\s/T/;
	return $string;
}

sub cyberplat_split_number {
	my ( $string ) = @_;
	return unless $string =~ /^(7\d{10})\/(.+)/;

	return ( $1, $2 );
}

sub cyberplat_response {
	my ( $code, $message, %args ) = @_;

	my %data = ( code  => $code,
				 message => $message );

	@data{keys %args} = values %args;

	my $data = \%data;

	my $xml = XMLout({response => $data},
					  KeepRoot => 1,
					  NoAttr => 1,
					  XMLDecl => 1 );

	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'utf-8');
}

################################################################################
# OSMP
################################################################################
sub osmp_init {
	my ( $self ) = @_;
	$self->{log}->info("I've made changes in abonent/text decoding.");
	$self->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
	$self->load_num_prices;
}

sub osmp_handle_request {
	my ( $self, $heap, $q ) = @_;

	my $command = $q->param('command') || "";
	my $txn_id = $q->param('txn_id') || "";
	my $account = $q->param('account') || "";
	my $sum = $q->param('sum') || "";

	if (not $command or not $txn_id or not $account or not $sum) {
		$self->error("[$heap->{session_id}] Invalid request: @{[Dumper(scalar $q->Vars)]}", "Incorrect OSMP command");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  "Not all required fields present" );
	}

	# command check
	unless ($command =~ /^(pay|check)$/) {
		$self->error("[$heap->{session_id}] Incorrect command '$command' ignored", "Incorrect OSMP command");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  "Incorect command '$command'" );
	}

	# txn_id check
	unless ($txn_id =~ /^\d{1,20}$/) {
		$self->error("[$heap->{session_id}] Incorrect txn_id '$txn_id' ignored", "Incorrect OSMP transaction id");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  "Incorect txn_id format '$txn_id'" );
	}

	# lower than min sum check
	if ($sum < $self->{min_price}) {
		$self->error("$sum is lower than $self->{min_price}", "OSMP sum not in range");
		return osmp_response( $txn_id,
							  241 );
	}

	# greater than max sum check
	if ($sum > $self->{max_price}) {
		$self->error("$sum is greater than $self->{max_price}", "OSMP sum not in range");
		return osmp_response( $txn_id,
							  242 );
	}

# А чё это было?
#	my ( $abonent, $msg ) = ($account, $q->param('pay_type') || ""); # +- vlad osmp_parse_account($account);

	my ( $abonent, $msg );

	my $pay_type = $q->param('pay_type');

	my $num;

	if (not $pay_type or $pay_type == 5) {
		($abonent, $msg) = osmp_parse_account($account);
		$num = $self->sum_to_num($sum);
	} else {
		$abonent = $account;
		$msg = "$pay_type";
		$num = "pt:$pay_type";
	}

	unless ($num) {
		$self->error("num not exists for sum '$sum'", "OSMP - num not found");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  'Cant convert sum to shortnum');
	}

	# can we extract abonent number and message text
	unless ($abonent) {
		$self->warning("Can't parse account '$account' to abonent and message text", "OSMP - incorrect account format");
		return osmp_response( $txn_id,
							  4 );
	}

	# Проверка на этом этапе заканчивается.
	if ($command eq 'check') {
		$self->{log}->info("[$heap->{session_id}] Check passed for txn_id: '$txn_id'");
		return osmp_response( $txn_id,
							  0 );
	}

	my $trans = Disp::SMSCTransaction->load($self->{smsc_id}, $txn_id);
	my $insms_id;

	unless ($trans) {
		my $sms = { smsc_id => $self->{smsc_id},
					num => $num,
					abonent => $abonent,
					msg => $msg,
					data_coding => 0,
					esm_class => 0,
					transaction_id => $txn_id };

		$insms_id = $self->{db}->insert_sms_into_insms($sms);
		$trans = Disp::SMSCTransaction->new({ smsc_id => $self->{smsc_id},
											  transaction_id => $txn_id,
											  insms_id => $insms_id,
											  date => today(),
											  status => 0,
											  exact_price => $sum,
											  parameters => {} });
		$trans->save;
		$self->{log}->info("[$heap->{session_id}] SMS$insms_id recieved from '$abonent' to '$num' with text 'text'");
	}

	if ($trans->{status} == 3) {
		return osmp_response($txn_id,
							 0,
							 $trans->{insms_id},
							 'This payment alreadry handled',
							 $trans->{exact_price});
	}

	if ($trans->{status} == 1 or $trans->{status} == 2) {
		return osmp_response($txn_id,
							 300,
							 undef,
							 'This payment has canceled');
	}

	$heap->{trans} = $trans;
 	return 5;
}

sub osmp_response {
	my ( $txn_id, $result, $prv_txn, $comment, $sum ) = @_;

	my $data = { 'osmp_txn_id' => $txn_id,
				 'result' => $result };

	$data->{prv_txn} = $prv_txn if defined $prv_txn;
	$data->{comment} = $comment if defined $comment;
	$data->{sum} = $sum if defined $sum;

	my $xml = XMLout({response => $data},
					 KeepRoot => 1,
					 NoAttr => 1,
					 XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>' );

	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'utf-8');
}

sub osmp_parse_account {
	my ( $text ) = @_;
	if ($text =~ /^[78](\d{10})\/(.+)/) {
		return ( "7$1", $2 );
	}
	return;
}

sub osmp_handle_alarm {
	my ( $self, $heap ) = @_;

	$self->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$heap->{trans}{transaction_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			$self->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$self->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$self->{sms_traffic_smsc_id}");
			$self->{db}->resend_through_smstraffic_with_num($v->{outbox_id}, $self->{sms_traffic_smsc_id}, '1121');
			$self->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$self->{db}->set_sms_delivered_status($v->{id});

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return osmp_response($heap->{trans}{transaction_id},
								 0,
								 $heap->{trans}{insms_id},
								 "Payment accepted",
								 $heap->{trans}{exact_price});
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 5) {
		$self->{log}->error("[$heap->{session_id}] No response for SMS$heap->{trans}{insms_id}, giving up");
		return osmp_response($heap->{trans}{transaction_id},
							 0,
							 undef,
							 'Payment accepted, message queued');
	}

	return 5;
}


################################################################################
# Price to num conversions
################################################################################
sub load_num_prices {
	my ( $self ) = @_;

	my $prices = $self->{db}{db}->selectall_arrayref("select * from set_real_num_price where date_start <= date(now()) and price_in > 0 and operator_id = ? and mt",
													 {Slice => {}},
													 $self->{operator_id});

	$self->{log}->info("Loading prices for $self->{operator_id}");

	$self->{min_price} = 1000000;
	$self->{max_price} = 0;

	foreach my $pr (@$prices) {
		$self->{log}->debug("Price for $pr->{num} is $pr->{price_in}");
		if ($pr->{price_in} > $self->{max_price}) {
			$self->{max_price} = $pr->{price_in};
		}
		if ($pr->{price_in} < $self->{min_price}) {
			$self->{min_price} = $pr->{price_in};
		}
	}

	$self->{prices} = $prices;
	print Dumper($self->{prices});
}

sub sum_to_num {
	my ( $self, $sum ) = @_;

	my $delta = 1000000;
	my $found;

	foreach my $pr (@{$self->{prices}}) {
		my $test = $sum - $pr->{price_in};

		next if $test < 0;

		if ($test < $delta) {
			$delta = $test;
			$found = $pr;
		}
	}

	if ($found) {
		return $found->{num};
	}

	return;
}

1;

__END__
account=79265550017%2fsthsthsnt&sum=23.58&timestamp=2007-11-13T13%3a14%3a18%2b03&type=check&sign=4b3606448e36b0eda68b9151facf2fe3368ad1cdfdfb739d3989e14752d1372907f1a6bf12313639cce08f117ca7b98517b405664c2c8fb6c075a294c857396001f09f68fb68919e309183d4e57e07ed170be9b39d6082700ba416b17f38d3d959e6f9824a3f7435d6c5ac0fd24045520fc9152410723ad78236e4ec9f26042b73b75851560082bd522cf79af82b7311cafced82a80c84c0b58076e1476422ba1114b037299352afedf437dda8d0fce15177790de8b78e2095d4e3630243a6419b109ad3944bd83dc967efda009e386d8135ac8c3471454f1901736c45206578d264bd74678808cd1383080d1ace0ab01d110250078f6e3b4e1d0ee87bef613e

account=79265550017%2f123&service=1&sum=1.37&timestamp=2007-12-21T15%3a16%3a28%2b03&type=check
account=79265550017%2F123&service=1&sum=1.37&timestamp=2007-12-21T15%3A16%3A28%2B03&type=check
