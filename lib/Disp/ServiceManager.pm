#!/usr/bin/perl
# -*- encoding: utf-8; tab-width: 4 -*-

=head1 NAME

Disp::ServiceManager

=head1 DESCRIPTION

Disp::ServiceManager - генерирут ссылки для DownloadManager'а на
основе конфигурационных данных и входящих СМС. Обрабатывает полученные
от сервиса данные, отыслая ответы пользователю, реагируя на ошибки и т.п.

=cut

package Disp::ServiceManager;
use strict;
use warnings;
use locale;
use Data::Dumper;
use Log::Log4perl qw/get_logger/;
use Params::Validate qw/:all/;
use POSIX;
use Date::Parse qw(str2time);
use Carp;

use base 'Disp::Object';

use List::MoreUtils qw/first_index/;

use Disp::Utils;
use Disp::MTTLookup;
use Disp::Operator;
use Translit qw/translit_text/;
use Disp::Translit;
use Disp::RecodeUtils;
use Disp::AltDB;
use Disp::SessionManager;

my $log_messages = get_logger('messages');
my $log_sdetect = get_logger('service_detection');
my $log = get_logger;

our $VERSION = '1.0';

my %auto_restart_policy = ( ussd => [ ],
                            _default_ => [ 1, 4, 28, 58 ] );

# Каким образом на какие ошибки реагировать
our %ERRORS = ( NO_SUCH_SERVICE => { message => 'No matching service',
                                     errsms_insert => 1,
                                     notify => 'services',
                                   },
                PUSH_NO_SERVICE => { message => 'Service not found while processing PUSH',
                                     errsms_insert => 1,
                                     error_callback => 1,
                                     auto_restart => 1,
                                     notify => 'services',
                                   },
                TIMEOUT_ERROR => { message => 'Connection to service timed out',
                                   errsms_insert => 1,
                                   error_callback => 1,
                                   auto_restart => 1,
                                   notify => 'services',
                                 },
                EMPTY_RESPONSE_ERROR => { message => 'Service returned nothing',
                                          errsms_insert => 1,
                                          error_callback => 1,
                                          auto_restart => 1,
                                          notify => 'services',
                                        },
                NETWORK_ERROR => { message => 'Could not connect to service',
                                   errsms_insert => 1,
                                   error_callback => 1,
                                   auto_restart => 1,
                                   notify => 'services',
                                 },
                HTTP_ERROR => { message => 'Service returned not OK http state',
                                errsms_insert => 1,
                                error_callback => 1,
                                auto_restart => 1,
                                notify => 'services',
                              },
                PARSE_ERROR => { message => 'Malformed service response',
                                 errsms_insert => 1,
                                 error_callback => 1,
                                 auto_restart => 1,
                                 notify => 'services',
                               },
                NOTICE_ERROR => { message => 'Could not resolve given "outcode" to plug_id',
                                  errsms_insert => 1,
                                  error_callback => 1,
                                  notify => 'developers',
                                },
                LOGIC_ERROR => { message => 'Incorrect data combination in service response',
                                 errsms_insert => 1,
                                 error_callback => 1,
                                 notify => 'developers',
                               },
                FORWARD_ERROR => { message => 'Forwarding of SMS failed',
                                   errsms_insert => 1 ,
                                   error_callback => 1,
                                   notify => 'services',
                                 },
                ROUTING_ERROR => { message => 'Plug not found',
                                   errsms_insert => 1,
                                   error_callback => 1,
                                   notify => 'services',
                                 },
		SUBPARTNER_ERROR => { message => 'Subpartner not provided',
				   errsms_insert => 1,
				   error_callback => 1,
				   notify => 'services',
			   },
                UNKNOWN_ERROR => { message => 'Unknown or unexpected here error',
                                   notify => 'developers',
                                   error_callback => 1,
                                 } );

# Create constants
foreach (keys %ERRORS) {
        my $n = $_;
        no strict 'refs';
        *$n = sub { $n };
}

# Register error handlers, order DOES matter
our %ERROR_PARAM_HANDLERS;
our @ERROR_PARAM_ORDER;
register_param_handler( auto_restart => \&error_restart_inbox );
register_param_handler( errsms_insert => \&error_errsms_insert);
register_param_handler( error_callback => \&error_do_callback);
register_param_handler( billing_insert => \&error_billing_insert_incoming);
register_param_handler( notify => \&error_do_yell );

# register_param_handler( notify => \&error_email_corresponding_contacts );

# Смысл всего этого таков - посредством
# Disp::ServiceManager::handle_error() другие модули сообщают сюда,
# какой именно тип ошибки произошел. handle_error() выбирает на
# основании этого типа одну запись из %ERRORS.  Для каждого ключа в
# $ERRORS{$type} вызываются зарегестрированные на него с помощью
# register_param_handler() обработчики. Причём, что забавно, можно
# вынести %ERRORS куда-нибудь для редактирования через вёб-интерфейс.

sub new {
        my $self = shift->SUPER::new(@_);

        POSIX::setlocale(POSIX::LC_ALL, 'ru_RU.CP1251');

		if ($self->{'push'}) {
			$self->{$_}=1 foreach qw/skip_load_mtt skip_load_prefix_rewrite skip_regexp skip_load_allocations/;
		}
        $self->load_config;

        return $self;
}

sub _add_plugin {
        my ( $self, $plugin ) = @_;

        my $aref = ($self->{plugin_map}{$plugin->{operator_id}}{$plugin->{num}} ||= []);

        ordered_insert($aref, $plugin, sub { $_[0]{priority} > $_[1]{priority}  });
}

sub _set_default_for_num {
        my ( $self, $plugin ) = @_;
        $self->{default_num}{$plugin->{operator_id}}{$plugin->{num}} = $plugin;
}

sub _set_default_for_operator {
    my ( $self, $plugin ) = @_;
    $self->{default_operator}{$plugin->{operator_id}} = $plugin;
}

sub _load_prices {
        my ( $self ) = @_;

        Config->dbh->do("drop table if exists disp_price_loader");
        Config->dbh->do("create temporary table disp_price_loader as select operator_id, num, min(date_start) as date_start from set_num_price where date_start <= date(now()) group by  operator_id, num");

        my $tmp = Config->dbh->selectall_arrayref("select n.operator_id as operator_id, n.num as num, n.price_in as price_in, n.currency as currency, n.mt as mt from disp_price_loader d join set_num_price n on n.num = d.num and n.operator_id = d.operator_id and n.date_start = d.date_start", {Slice => {}});
        foreach (@$tmp) {
                $self->{prices}{$_->{operator_id}}{$_->{num}} = { price => $_->{price_in},
                                                                                                                  currency => $_->{currency},
                                                                                                                  mt => $_->{mt} };
        }
}

sub login_to_money_consumer_id {
        my ( $self, $login ) = @_;

        return unless $login;

        my $from_login = $self->{money_consumer_by_login}{$login}{id};

        if ($from_login and ref $self->{set_cyka_money_consumer}{$from_login} eq 'HASH') {
                return $from_login;
        }

        if (ref $self->{set_cyka_money_consumer}{$login} eq 'HASH') {
                return $login;
        }

        return undef;
}


=head3 load_config

(Пере)загружает конфигурацию сервисов из БД. Вызывается при создании
объекта, а также при принудительном перечитывании конфигов.

=cut
sub load_config {
        my ( $self ) = @_;

		hires_eval {
            ($self->{sms_traffic_operator}) = Config->dbh->selectrow_array("select id from set_operator_t where short_name = 'sms_traffic'");
            $self->{set_operator} = selectall_hash('set_operator', 'id');
        } "load operator", $log_messages;

        $self->{operator} = Disp::Operator->new();

        hires_eval {
            my $it = iterated_db_select("select * from set_mtt_base");
            $self->{mtt_lookup} = Disp::MTTLookup->new();
            $self->{mtt_lookup}->init_from_db($it);
        } "load mtt", $log_messages
		  unless ($self->{skip_load_mtt});

        hires_eval {
            foreach (qw/set_service set_plugin set_smsc set_cyka_money_consumer set_sgroup set_cyka_manager/) {
				@{$self}{$_} = selectall_hash($_, 'id') unless ($self->{"skip_load_$_"});
			}
            $self->{money_consumer_by_login} = selectall_hash('set_cyka_money_consumer', 'login');
        } "load set tables", $log_messages;

		hires_eval {
			my $tmp = selectall("set_operator_route");
			$self->{set_operator_route}{$_->{operator_id}}{$_->{num}} = $_ for @$tmp;
			warn Dumper($self->{set_operator_route});
		} "load routing table", $log_messages;

        hires_eval {
            $self->_load_prices;
        } "load prices", $log_messages
		  unless ($self->{skip_load_prices});

        my ( $id, $plugin, $plug, $smsc, $rwr, $srv );

        while (($id, $smsc) = each %{$self->{set_smsc}}) {
                $smsc->{real_smsc} = 1 if $smsc->{client_type} ne 'smsc';
        }

	while (($id, $srv) = each %{$self->{set_service}}) {
#		$srv->{shade_partners}{$_} = $srv->{shade_partner1} foreach (split(/,/,$srv->{subpartners1}));
#		$srv->{shade_partners}{$_} = $srv->{shade_partner2} foreach (split(/,/,$srv->{subpartners2}));
		eval {
			$srv->{shade_partners_regexp1} = qr/$srv->{subpartners1}/i
				if ($srv->{shade_partner1} and $srv->{subpartners1});
		}
	}

        hires_eval {
            my $rewrite_data = selectall("set_prefix_rewrite");
            foreach (@$rewrite_data) {
                $_->{prefix} = lc($_->{prefix});
                $_->{prefix} =~ s/^\s+//;
                $_->{prefix} =~ s/\s+$//;
                $self->{prefix_rewrite}{$_->{operator_id}}{$_->{num}}{$_->{prefix}} = $_;
            }
        } "load prefix rewrite", $log_messages
		  unless ($self->{skip_load_prefix_rewrite});

        # готовимся к определению сервиса
        hires_eval {
            while (( $id, $plugin ) = each %{$self->{set_plugin}}) {
                # Compile regexp
                eval {
                    $plugin->{pattern_c} = qr/$plugin->{pattern}/i;
                    $plugin->{msg_type_c} = qr/$plugin->{msg_type}/i if $plugin->{msg_type};
                };
                if ($@) {
                    yell( "Wrong plugin pattern detected for SVC$plugin->{service_id}\n https://asg.alt1.ru/?module=plugins&action=pluginedit&id=$plugin->{id}",
                          code => 'WRONG_PATTERN',
                          process => 'DISPATCHER',
                          header => "Wrong plugin pattern detected for SVC$plugin->{service_id}");
                } else {
                    $self->_add_plugin($plugin);
                    $self->_set_default_for_num($plugin) if $plugin->{is_default_num};
                    $self->_set_default_for_operator($plugin) if $plugin->{is_default_operator};
                }
            }
        } "compile regexp", $log_messages
		  unless ($self->{skip_regexp});

        hires_eval {
            $self->_load_outcode;
        } "load outcode", $log_messages;

		hires_eval {
			$self->_load_allocations;
		} "load allocations", $log_messages
		  unless ($self->{skip_load_allocations});

        $self->{session_manager} = Disp::SessionManager->new()
		  unless ($self->{skip_session_manager});
}

sub _load_allocations {
	my ( $self ) = @_;

	my $st = Config->dbh->prepare("select c.service_id,c.operator_id,c.num,c.b_price_fixed_p,c.currency from set_cyka_plugin_t c, set_service s where c.service_id=s.id and s.request_type=1");
	$st->execute;
	while (my($sid, $op, $num, $price, $cur) = $st->fetchrow_array) {
		$self->{allocations}{$sid}{$op}{$num} = "$price$cur";
	}
}

sub _load_outcode {               # () void
        my ( $self ) = @_;

        # XXX - maybe populate database with default outcodes?
        my $st = Config->dbh->prepare("select name, service_id, operator_id, num, smsc_id, default_partner_id from set_outcode");
        $st->execute;
        while (my($nm, $sid, $op, $num, $smsc, $default_partner_id) = $st->fetchrow_array) {
                $self->{outcode}{$op}{$sid}{lc $nm} = $num;
                $self->{outcode_smsc}{$op}{$sid}{lc $nm} = $smsc;
                $self->{outcode_default_partner_id}{$op}{$sid}{lc $nm} = $default_partner_id;
        }
}

sub _find_plugin {
        my ( $self, $msg ) = @_;

        my $pluglist = $self->{plugin_map}{$msg->{operator_id}}{$msg->{num}};

        $log_sdetect->debug("=> Plugin detection start, ".(ref($pluglist) eq 'ARRAY' ? @$pluglist : 0)." plugin in the list");

        my $found_plugin;
		my $keyword;

        foreach my $plugin (@$pluglist) {
                $log_sdetect->debug("MSG$msg->{id}: trying PLUGIN$plugin->{id}");
                pos $msg->{msg_trans} = 0;
                if (scalar $msg->{msg_trans} =~ m/$plugin->{pattern_c}/gi) {
					$keyword = $1;
                    my $re_pos = pos $msg->{msg_trans};

                        if ($plugin->{msg_type_c}) {
                                $log_sdetect->debug("Checking msgtype");
                                unless ($msg->{msg_type} =~ $plugin->{msg_type_c}) {
                                        $log_sdetect->debug("msgtype check failed, skipping");
                                        next;
                                }
                        }

                        $log_sdetect->debug("MSG$msg->{id}: PLUGIN$plugin->{id} matched");
                        $found_plugin = $plugin;

                    $msg->{_prefix_end} = $re_pos;

                        last;


                }
        }

        unless ($found_plugin) {
                $found_plugin = $self->{default_num}{$msg->{operator_id}}{$msg->{num}};
                $log_sdetect->debug("Defaulting to plugin $found_plugin->{id}")
                  if ref($found_plugin) eq 'HASH';
        }

        unless ($found_plugin) {
            $found_plugin = $self->{default_operator}{$msg->{operator_id}};
            $log_sdetect->debug("Defaulting to plugin $found_plugin->{id} (operator default)")
              if ref($found_plugin) eq 'HASH';
        }

        unless ($found_plugin) {
                $log_sdetect->error("Plugin not found for MSG$msg->{id}");
                return;
        }

        $log_sdetect->debug("=> Plugin detection resulted in plugin '$found_plugin->{id}'");

        return $found_plugin, $keyword;
}

=pod

create view prefix_rewrite as select operator_id, num, keyword as prefix, text as replacement
  from keyword join keyword_operator on keyword.id = keyword_id;

=cut

sub prefix_rewrite {
        my ( $self, $msg_text, $operator_id, $num ) = @_;

        my @words = grep { length($_) > 0 } split /\s+/, $msg_text;

        return $msg_text unless @words;

        my @trans_words = map { lc(translit_text($_)) } @words;


        my $wc = $#words;

        $wc = 4 if $wc > 4;

        for ( my $i = $wc; $i >= 0; $i-- ) {
                my $prefix_text = join ' ', @trans_words[0..$i];
                # warn "try rewrite $i length - prefix text $prefix_text";

                my $rest = "";

                if ( $i < $#words ) {
                        $rest = join ' ', @words[$i+1..$#words];
                }

                if (my $tmp = $self->{prefix_rewrite}{$operator_id}{$num}{$prefix_text})
                {
                        $log_sdetect->info("Replacing '$prefix_text' with '$tmp->{replacement}'");
                        my $a = $tmp->{replacement} . " " . $rest;
                        $a =~ s/\s+$//;

                        if($tmp->{keyword_type}=='1')
                        {
                            $a = "sputnik " . $a;
                        }

                        # warn "Replaced '$msg_text' with '$a'";
                        return $a;
                }

        }

        return $msg_text;

}

sub check_antispam {
    my ( $self, $msg, $service ) = @_;

    my $code_len = $service->{antispam_code_length};

    if ($code_len > 0 and $service->{antispam_update_period} > 0 and length($service->{antispam_allowed_methods}) > 0) {

        my ( $cc, $pc, $vt ) = Config->dbh->selectrow_array("select current_code, previous_code, unix_timestamp(valid_through) from tr_service_antispam where service_id = ?", undef,
                                                            $service->{id});

        if (time > $vt) {
            yell( join("\n\n",
                       "Service: ".$self->describe_service($service->{id}),
                       "Valid through: $vt",
                       "Now: ".time),
                  cc => join(", ", $self->service_contacts($service->{id})),
                  header => "Antispam data outdated (SVC:".$self->describe_service($service->{id}).")",
                  code => 'ERROR',
                  process => 'DISPATCHER' );
            return 1;
        }

        my $prefix = substr $msg->{msg_trans}, 0, $msg->{_prefix_end};
        my $postfix = substr $msg->{msg_trans}, $msg->{_prefix_end};

        my $code_to_remove;
        my $pos_to_remove;
        my $space_preserve;
        my $success = 0;

        if ($postfix =~ /(\s*)$cc/g) {
            $space_preserve = $1;
            $success = 1;
            $code_to_remove = $cc;
            $pos_to_remove = pos $postfix;
        }

        if ($postfix =~ /(\s*)$pc/g) {
            $space_preserve = $1;
            $success = 1;
            $code_to_remove = $pc;
            $pos_to_remove = pos $postfix;
        }

        if ($success) {
            $msg->{msg_trans} = $prefix . $space_preserve . substr $postfix, $pos_to_remove;
            $msg->{msg} =~ s/$code_to_remove//;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 0;
    }
}

sub check_blacklist {
    my ( $self, $msg, $plugin, $service ) = @_;

    my $interval = $plugin->{blacklist_interval} || 60;

    my ( $abonent, $timestamp ) = Config->dbh->selectrow_array("select abonent, blacklist_timestamp from tr_blacklist where abonent = ?", undef,
                                                               $msg->{abonent});

    return 0 unless $abonent;

    if ($timestamp + $interval * 60 < time()) {
        return 0;
    }

    $log_messages->info("IGNORE BLACKLIST MSG$msg->{id}: FROM ABONENT$msg->{abonent} VIA OPERATOR$msg->{operator_id} NUM$msg->{num} TO SERVICE$service->{id}");
#     yell( "Blacklist succeded:\nAbonent: $msg->{abonent}\nBlacklisted at: ".localtime($timestamp)."\nBlacklist interval: $interval minutes\nService: ".$self->describe_service($service->{id})."\nShort num: $msg->{num}\nOperator: ".$self->describe_operator($msg->{operator_id}),
#           code => 'BLACLISTED_DETECTED',
#           process => 'DISPATCHER',
#           contact_group => 'fraud',
#           header => "Blacklisted abonent $msg->{abonent} not passed to service" );
    return 1;
}

sub check_unique_message {
    my ( $self, $msg, $service ) = @_;
    my ( $inbox_id ) = Config->dbh->selectrow_array("select inbox_id from tr_unique_message where num=? and msg=?", undef,$msg->{num},$msg->{msg_trans});
    return 1 if ($inbox_id and ($inbox_id != $msg->{id}));
    Config->dbh->do("do update_unique_message(?,?,?,?)",undef,$msg->{num},$msg->{msg_trans},$service->{id},$msg->{id});
    return 0;
}

=begin mysql

DROP TABLE IF EXISTS tr_fraud_number;
CREATE TABLE `tr_fraud_number` (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  `abonent` varchar(20) NOT NULL,
  `num` varchar(20) NOT NULL,
  `hour` datetime NOT NULL,
  `count` int(11) NOT NULL default '0',
  UNIQUE KEY  (`abonent`,`num`,`hour`),
  KEY `fraud_service__abonent__service_id__idx` (`abonent`,`service_id`),
  KEY `fraud_service__hour__idx` (`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251


=cut
sub check_fraud {
        my ($self, $msg, $plugin, $service) = @_;

        return 0 if exists $msg->{try_count};

		my $hour = POSIX::strftime('%Y-%m-%d %H:00:00', localtime);

		Config->dbh->do("insert into tr_fraud_number set abonent = ?, num = ?, hour = ?, count = ? on duplicate key update count = count + ?", undef,
						$msg->{abonent}, $msg->{num}, $hour, 1, 1);

        if ($plugin->{is_fraud} and $plugin->{fraud_interval} > 0 and $plugin->{fraud_limit} > 0) {
                my ( $whitelist_check ) = Config->dbh->selectrow_array("select in_whitelist from tr_fraud_whitelist where abonent = ?",undef,$msg->{abonent});

                my ( $count_in_interval ) = Config->dbh->selectrow_array
                  ("select sum(count) from tr_fraud_number where abonent = ? and num = ? and hour >= date_sub(?, interval ".($plugin->{fraud_interval} - 1)." hour)", undef,
                   $msg->{abonent}, $msg->{num}, $hour);

                $log->info("MSG$msg->{id} - Fraud check is enabled, $count_in_interval messages in period $plugin->{fraud_interval} when limit is $plugin->{fraud_limit}");

                if ($count_in_interval > $plugin->{fraud_limit}) {
                        $log->info("MSG$msg->{id} - FRAUD DETECTED: ABONENT$msg->{abonent}, SERVICE$service->{id}, OPERATOR$msg->{operator_id}, NUMf$msg->{num}");

                        if (not $whitelist_check) {
							$log_messages->info("IGNORE FRAUD MSG$msg->{id}: FROM ABONENT$msg->{abonent} VIA OPERATOR$msg->{operator_id} NUM$msg->{num} TO SERVICE$service->{id}");
# 							yell( "Fraud check succeded:\nAbonent: $msg->{abonent}\nMessage count: $count_in_interval\nCheck period: $plugin->{fraud_interval} hours\nCheck limit: $plugin->{fraud_limit}\nService: ".$self->describe_service($service->{id})."\nShort num: $msg->{num}\nOperator: ".$self->describe_operator($msg->{operator_id}),
# 								  code => 'FRAUD_DETECTED',
# 								  process => 'DISPATCHER',
# 								  contact_group => 'fraud',
# 								  header => "Fraud detected for abonent $msg->{abonent}" );
							return 1;
                        } else {
							$log_messages->info("MSG$msg->{id} IS LIKE A FRAUD, BUT ABONENT IS IN WHITELIST");
							return 0;
                        }
                }
        }
        return 0;
}

=head3 make_link

Создаёт ссылку для DownloadManager, которая должны быть использована
для обработки входящей СМС.

=cut
sub make_link {
    my ( $self, $msg ) = @_;

    $msg->{msg_orig} = $msg->{msg};
    $msg->{msg_trans} = lc(translit_text($msg->{msg}));
    my $rewritten = $self->prefix_rewrite($msg->{msg}, $msg->{operator_id}, $msg->{num});;

    if ($rewritten ne $msg->{msg}) {
        $msg->{msg} = $rewritten;
        $msg->{msg_trans} = translit_text($rewritten);
    }

    my $service;
    my ($plugin, $keyword);
    my $session;
    if ($msg->{transaction_id} =~ m/{PTN(\d+)}/) {
      my $service_id = $1;
      $log_messages->debug("SERV_PTN: MSG$msg->{id} PTN service found:$service_id");

      my $pluglist = $self->{plugin_map}{$msg->{operator_id}}{$msg->{num}};
      foreach my $p (@$pluglist) {
          if ($p->{service_id} == $service_id) {
              $plugin = $p;
              $msg->{_prefix_end} = 0;
              last;
          }
      }

      $service = $self->{set_service}{$service_id};
    } else {
      # Try to determine service and handlers
#      ($plugin, $keyword) = $self->_find_plugin($msg); # some data added to $msg !

      $session = $self->{session_manager}->get_session($msg)
          unless ($plugin and $plugin->{msg_type_c});

      if ($session) {
          $log_messages->debug("SERV_SESS: MSG$msg->{id} Session found:$session->{id}");
#         if ( !$plugin or ($session->{service_id} != $plugin->{service_id}) ) {
              my $pluglist = $self->{plugin_map}{$msg->{operator_id}}{$msg->{num}};
              foreach my $p (@$pluglist) {
                  if ($p->{service_id} == $session->{service_id}) {
                      $plugin = $p;
                      $msg->{_prefix_end} = 0;
                      $msg->{msg} = $session->{keyword}.$msg->{msg};
                      $msg->{msg_trans} = translit_text($msg->{msg});
                      last;
                  }
              }
#          }
      }

      ($plugin, $keyword) = $self->_find_plugin($msg) unless ($plugin); # some data added to $msg !

      $service = $self->{set_service}{$plugin->{service_id}};
    }

    # Detection failed
    unless ($service->{uri}) {
        $log_messages->error("SERV_IGNORE: MSG$msg->{id} due to lack of service");

        $self->mark_processed($msg);

        my $op_inf = $self->describe_operator($msg->{operator_id});

        my $errmsg = <<EOF;
Service not found for msg $msg->{id}
Operator: $op_inf
Num: $msg->{num}
EOF
        my $error = 'NO_SUCH_SERVICE';
        my $svc = { id => 0 };

        $self->handle_error( error => $error,
                             err_message => $errmsg,
                             service => $svc,
                             insms_count => $msg->{transport_count},
                             abonent => $msg->{abonent},
                             operator_id => $msg->{operator_id},
                             num => $msg->{num},
                             identifiers => { inbox_id => $msg->{id} });
        return;
    }

    # Remember smsc_id for given abonent and transport, for later usage
    $self->update_num_map(abonent => $msg->{abonent},
                          transport_type => $msg->{transport_type},
                          smsc_id => $msg->{smsc_id},
                          operator_id => $self->{set_operator}{$msg->{operator_id}}{num_map_operator_id})
        if ($self->{set_operator}{$msg->{operator_id}}{num_map_operator_id});

#    my $region = $self->{mtt_lookup}->lookup($msg->{abonent});
#    if ($region) {
#        $region = $region->{region_id};
#    }
#    else {
#        $region = 0;
#    }

    my $params = { url => $service->{uri},
                   params => { msg => $msg->{msg},
                               transport_count => $msg->{transport_count},
                               user_id => $msg->{abonent},
                               transaction_id => $msg->{id},
                               date => $msg->{date},
#                               op_transaction_id => ($msg->{transaction_id} || ""),
                               service_id => $service->{id},
#                               gmt_offset => $self->{set_smsc}{$msg->{smsc_id}}{gmt_offset},
                               num => $msg->{num},
#                               op => $self->{set_operator}{$msg->{operator_id}}{parent_id},
                               operator => $self->{set_operator}{$msg->{operator_id}}{short_name},
                               msg_type => $msg->{msg_type},
                               operator_id => $msg->{operator_id},
                               msg_trans => $msg->{msg_trans},
#                              region => $region 
                               },
                   original_service => $service,
                   original_message => $msg,
                   original_plugin => $plugin };

    my $kontent_po_karmanu;
    if ( $msg->{transaction_id} =~ m{kpk=\{(\d+)\}} ) {
        $kontent_po_karmanu = $1;
    }
    if ( $kontent_po_karmanu ) {
        my $credit_message_id = $1;
        my $ignore = $self->make_link_credit_processing($msg, $credit_message_id, $params, $service);
        return if $ignore;
    }

    my $fuck_off_msg = "Vami prevysheno ogranichenie obraschenij k servisu. Povtorite cherez 1 chas";
	my $uniq_message_stop;
    if ($self->check_fraud($msg, $plugin, $service)) {
        $msg->{is_fraud} = 1;
        $fuck_off_msg = $service->{fraud_msg} || $fuck_off_msg;
    }
    elsif ($self->check_blacklist($msg, $plugin, $service)) {
        $msg->{is_fraud} = 2;
        $fuck_off_msg = $service->{blacklist_msg} || $fuck_off_msg;
    }
    elsif ($self->check_antispam($msg, $service)) { # Modifies $msg
        $msg->{is_fraud} = 3;
        $fuck_off_msg = "Takoy tekst bolshe neveren, poluchite noviy tekst";
        #        } elsif ($service->{unique_message_pattern}
        #		and $service->{unique_message_dub_response}
        #		and ($msg->{msg_trans} =~ /$service->{unique_message_pattern}/)
        #		and $self->check_unique_message($msg, $service)) {
        #            $uniq_message_stop = 1;
        #            $fuck_off_msg = $service->{unique_message_dub_response};
        #	    yell( "Service: ".$self->describe_service($service->{id})."\nAbonent: $msg->{abonent}\nNum: $msg->{num}\nMessage: $msg->{msg}",
        #                   cc => 'S.Timonin@alt1.ru,P.Shabanova@alt1.ru',
        #		   header => "Unique message repeated (SVC:".$self->describe_service($service->{id}).")",
        #                   code => 'WARNING',
        #                   process => 'DISPATCHER' );
    }

    if ($msg->{is_fraud} or $uniq_message_stop) {
        $self->handle_service_response( orig => $msg,
                                        orig_type => 'inbox',
                                        service => $service,
                                        plugin => $plugin,
                                        url => 'no-proto://nowhere.nonexistent',
                                        response => $self->direct_response_section($msg,
                                                                                   $service,
                                                                                   $fuck_off_msg))
            unless $kontent_po_karmanu;
        return;
    }

    $log_messages->info("SERV_LINK: MSG$msg->{id} link generated");

    if ($session) {
        $session->{end_date} = str2time($msg->{date}) + $service->{session_timeout};
        $self->{session_manager}->update_session($session) if ($session->{inbox_id});
        $log_messages->info("SERV_SESS: MSG$msg->{id} session $session->{inbox_id} processed");
    }
    elsif ($keyword and $service->{session_timeout}) {
        $session = $self->{session_manager}->create_session($msg,$service,$keyword);
        $log_messages->info("SERV_SESS: MSG$msg->{id} session $session->{inbox_id} created");
    }

    $params->{params}{test_flag} = 1 if $msg->{test_flag};
    $params->{params}{partner_allocation} = $self->{allocations}{$service->{id}}{$msg->{operator_id}}{$msg->{num}}
	if ($service->{request_type} == 1);
    $params->{params}{this_is_retry} = $msg->{try_count} if $msg->{try_count} and $msg->{try_count} > 0;
    #HACK: SQL hack for MTS. was added to protect from fraud numbers
    $params->{params}{level} = $msg->{level} if $msg->{level};

    return $params;
}

sub make_link_credit_processing {
    my ( $self, $msg, $credit_message_id, $params, $service ) = @_;

    my $credit_message = Config->dbh->selectrow_hashref(
        "select * from tr_credit_messages where id = ?", undef, $credit_message_id,
    );
    unless ($credit_message) {
        $self->mark_processed($msg);
        $log_messages->error("SERV_IGNORE: MSG$msg->{id} due to absent 'Kontent po karmanu' record in table");
        # XXX yell here
        return 1;
    }

    if (not $credit_message->{inbox_id} or $msg->{service_id} != $credit_message->{service_id}) {
       $log_messages->debug("Updating 'content po karmanu details");
        Config->dbh->do(
            'call update_credit_message_from_dispatcher(?,?,?,?)', undef,
            $credit_message_id, $msg->{id}, $service->{id}, $msg->{operator_id},
        );
    }
    if ($service->{credit_policy} eq 'allow') {
        $log_messages->debug("MSG$msg->{id}: added 'content po karmanu' parameters to generated link");
        $params->{params}{credit_installment_id} = $credit_message->{id};
        $params->{params}{credit_type} = $credit_message->{credit_type};
        $params->{params}{credit_total} = $credit_message->{credit_total};
        $params->{params}{credit_remaining} = $credit_message->{credit_remaining};
        $params->{params}{credit_installment} = $credit_message->{credit_installment};
    }
    else {
        $self->mark_processed($msg);
        $log_messages->error("SERV_IGNORE: MSG$msg->{id} due to 'Kontent po karmanu' prohibition");
        return 1;
    }
    return 0;
}

sub update_num_map {              # (%params) void
        my ( $self, %p ) = @_;
#        Config->dbh->do("insert into tr_num_map (abonent, transport_type, smsc_id, operator_id) values (?,?,?,?) ON DUPLICATE KEY UPDATE smsc_id = ?, operator_id = ?",
#                        undef, $p{abonent}, $p{transport_type}, $p{smsc_id}, $p{operator_id}, $p{smsc_id}, $p{operator_id});
#        Config->dbh->do("replace into tr_num_map (abonent, transport_type, smsc_id, operator_id) values (?,?,?,?)",
#                        undef, $p{abonent}, 'sms', $p{smsc_id}, $p{operator_id});
        Config->dbh->do("select update_num_map(?,?)",
                        undef, $p{abonent}, $p{operator_id});
}


=head3 _start_transaction () void

Начинает транзакцию, или если таковая уже есть - то просто увеличивает
счётчик начатых.

=cut                   #
sub _start_transaction # () void
{
        my ( $self ) = @_;
        unless ($self->{already_in_transaction}) {
                $self->{already_in_transaction} = 0;
                Config->dbh->begin_work;
        }
        $self->{already_in_transaction}++;
        $log->debug("TransactionS: $self->{already_in_transaction}");
}

=head3 _end_transaction () void

Уменьшает счётчик начатых транзакций, и если их число достигло нуля - вызывает commit.

=cut                   #
sub _end_transaction   # () void
{
        my ( $self ) = @_;
        unless (--$self->{already_in_transaction}) {
                Config->dbh->commit;
        }
        $log->debug("TransactionE: $self->{already_in_transaction}");
}

=head3 handle_response

Вызывается когда менеджеру закачек удалось успешно получить ответ от
сервиса. Здесь будет произведён парсинг ответа сервиса и на основе
этого будут выполнены необходимые действия.

=cut
sub handle_response {
        my ( $self, $data ) = @_;

        $self->handle_service_response( orig => $data->{link}{original_message},
                                        orig_type => 'inbox',
                                        service => $data->{link}{original_service},
                                        plugin => $data->{link}{original_plugin},
                                        url => $data->{url},
                                        response => $data->{data} );
}

sub mark_processed {
        my ( $self, $msg ) = @_;

        $self->{inbox}->mark_processed($msg);
}

my @outbox_fields = qw/abonent smsc_id operator_id num msg content_type target_type transport_type transaction_id service_id inbox_id push_id test_flag resend_flag resend_smsc_id registered_delivery params tariff date/;
my $outbox_transaction_id_pos = first_index { $_ eq 'transaction_id' } @outbox_fields;
my $outboxa_insert = "insert into tr_outboxa (".join(', ', @outbox_fields).") values (".join(', ', ('?') x @outbox_fields).")";
my $outbox_insert = "insert into tr_outbox (".join(', ', 'id', @outbox_fields).") values (".join(', ', ('?') x (1+@outbox_fields)).")";

sub send_message {
        my ( $self, $data, $section ) = @_;

        $data->{inbox_id} ||= 0;
	$data->{date} ||= POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);

        return undef unless $data->{smsc_id} and $data->{num};
                my $is_mts = exists {501=>1,502=>1,503=>1,504=>1,505=>1,511=>1,512=>1,513=>1,514=>1,515=>1}->{$data->{smsc_id}};
		if (($data->{content_type} eq 'flash-msg' or $data->{content_type} eq 'app-port-long') 
				and ($data->{transaction_id} !~ m{kpk=\{(\d+)\}})) {
			unless ($is_mts) {
			$data->{smsc_id} = 551; #85;
			$data->{operator_id} = 619; #204;
		        }
		}

		# HACK MTS: Replace HIDDEN content type with TEXT/PLAIN for MTS subscribers
		if ({501=>1,502=>1,503=>1,504=>1,505=>1,511=>1,512=>1,513=>1,514=>1,515=>1}->{$data->{smsc_id}}) {
                       $data->{content_type} = 'text/plain' if ($data->{content_type} eq 'hidden');
		}

		my $translit_triggered = 0;
		my $approx_parts_count = 1;

		if ( $self->{set_operator}{$data->{operator_id}}{force_detranslit} == 1
			 and not $self->{set_service}{$data->{service_id}}{trusted_translit}) {
			my $old_msg = $data->{msg};
			$data->{msg} = Disp::Translit::transform($data->{msg})
				if (Disp::RecodeUtils::gsm_compatible($data->{msg}));

			$translit_triggered = ($data->{msg} ne $old_msg);
		}

		if ( $self->{set_operator}{$data->{operator_id}}{force_detranslit} == 2) {
			$data->{msg} = translit_text($data->{msg});
		}

		my $msg_len = length $data->{msg};
		if ( $data->{msg} =~ /[\x80-\xFF]/) {
			# Кириллица
			if ( $msg_len > 70 ) {
				while ( $msg_len > 66) {
					$approx_parts_count++;
					$msg_len -= 66;
				}
			}
		} else {
			# Латиница
			if ( $msg_len > 160 ) {
				while ( $msg_len > 152) {
					$approx_parts_count++;
					$msg_len -= 152;
				}
			}
		}

		local $data->{params} = $data->{params};
#		$data->{params} = dumper_save($data->{params}) if ref $data->{params};
#		my %params = ();
		while (my($a,$b) = each %$section) {
			$data->{params}{$a} = $b if ($a =~ /^p\./);
		}
		$data->{params}{service_id} = $data->{service_id};# if ($data->{registered_delivery});
		$data->{params}{resend_smsc_id} = $data->{resend_smsc_id}
			if ($data->{resend_flag} and $data->{resend_smsc_id});

		my $svc = $self->{set_service}{$data->{service_id}};
		my $shade_partner;
		my $subptr = $section->{subpartner} || '';
		if ($svc->{shade_partners_regexp1} and ($subptr =~ $svc->{shade_partners_regexp1})) {
#		if (defined($section->{subpartner})) {
#			my $subptr = $section->{subpartner};
			$data->{params}{subpartner} = $subptr;
#			$shade_partner = $svc->{shade_partners}{$subptr};

			$shade_partner = $svc->{shade_partner1};
		}
#		$shade_partner ||= $svc->{shade_partners}{all};

		if ($svc->{shade_percent} and $shade_partner) {
			$data->{params}{shade_partner} = $shade_partner;
			$data->{params}{shade_percent} = $svc->{shade_percent};
		}
		$data->{params} = dumper_save($data->{params}) if ref $data->{params};

        my @values = map { $data->{$_} } @outbox_fields;

	my($id, $rv) = (0, undef);
	my $run_on_host = $self->{set_smsc}->{$data->{smsc_id}}->{disp_run_on_host};

	eval { Disp::AltDB::dbh($run_on_host, sub {
		my $dbh      = $_[0]->{db};
		$run_on_host = $_[1];

		my $inside_transaction = !$dbh->{AutoCommit} or $dbh->begin_work;

        my $credit_message_id;

        if ($data->{add_credit_details}) {
            ($credit_message_id) = $dbh->selectrow_array(
                "select add_credit_installment(?, ?, ?)", undef,
                $data->{add_credit_details}{original_id},
                $data->{push_id},
                $data->{add_credit_details}{installment},
            );
            unless ($credit_message_id) {
                confess "add_credit_installment() failed";
            }
            $values[$outbox_transaction_id_pos] .= "kpk={$credit_message_id}";
        }

		eval { $dbh->do($outboxa_insert, undef, @values); };
		$dbh->do($outboxa_insert, undef, @values) if ($@);

		($id) = $dbh->selectrow_array("select last_insert_id()");
		$dbh->do($outbox_insert, undef, $id, @values);

        $log_messages->info("ATTACHED CREDIT$credit_message_id to OUTBOX$id")
            if $credit_message_id;

		$rv = { outbox_id => $id, date => $data->{date} };

		$inside_transaction or $dbh->commit;

		$log_messages->info("ROUTED OUTBOX$id VIA SMSC$data->{smsc_id}($data->{num}) ON $run_on_host");
		$log_messages->info("TRANSLIT TRIGGER FOR OUTBOX$id")
			if $translit_triggered;
		1;
	}) };
	if ($@) {
		my $msg = "Insert of OUTBOX$id has failed on $run_on_host";
		$log->error("$msg: $@");
		yell("$msg\n\n$@",
			code => 'ERROR', process => 'DISPATCHER', header => $msg);
	};

	return $rv;
}

sub send_error_message {
        my ( $self, $dest, $data, $orig ) = @_;
        $log_messages->info("SERV_OUTGOING_ERROR: IN_REPLY_TO:MSG$orig->{id} DEST:$dest");
}

sub handle_error {     # (%params) void
        my ( $self, %p ) = @_;

        $log->debug(Dumper(\%p));

        my $e = $ERRORS{$p{error}};
        $e ||= $ERRORS{UNKNOWN_ERROR};

        $self->_start_transaction;

        foreach my $param (@ERROR_PARAM_ORDER) {
            if (exists $e->{$param}) {
                foreach my $handler (@{$ERROR_PARAM_HANDLERS{$param}}) {
                    $handler->($self,
                               error_data => $e,
                               params => \%p );
                }
            }
        }
        $self->_end_transaction;
}

my @brecords_fields = qw/date inbox_id outbox_id service_id abonent smsc_id num insms_count outsms_count push_id dir partner_id operator_id category test_flag legal_owner_id is_fraud subservice_id cyka_processed/;
my $brecords_insert = "replace into tr_billing (".join(", ", @brecords_fields).") values (".join(", ", ('?') x (@brecords_fields)).")";
#my $billingf_insert = "insert into billingf (".join(", ", 'date', @brecords_fields).") values (now(), ".join(", ", ('?') x (@brecords_fields)).")";

sub brecords_save {               # ($data) void
        my ( $self, $data ) = @_;
        return unless $data;
        return unless defined($data->{inbox_id}) or defined($data->{outbox_id}) or defined($data->{push_id});

        $data->{inbox_id} ||= 0;
        $data->{outbox_id} ||= 0;
        $data->{push_id} ||= 0;
        $data->{insms_count} ||= 0;
        $data->{outsms_count} ||= 0;
        $data->{is_fraud} ||= 0;
        $data->{partner_id} ||= 0;
	yell(POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime)."\n".Dumper($data),
		                         header => "Date field is missed in brecords_save call",
					                          process => 'DISPATCHER')
							  if (!$data->{date});

	$data->{date} ||= POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);

        # $log->debug("BRECORD: ".Dumper($data));

        $data->{cyka_processed} = 0;

        my @values = map { $data->{$_} } @brecords_fields;
	my $db_host = ($data->{outbox_id}) ?
		$self->{set_smsc}->{$data->{smsc_id}}->{disp_run_on_host} :
		undef;
	Disp::AltDB::dbh($db_host,
		sub {
			my $dbh  = $_[0]->{db};
			my $host = $_[1];
			my $inside_transaction = !$dbh->{AutoCommit} or $dbh->begin_work;
			$dbh->do($brecords_insert, undef, @values);
			$inside_transaction or $dbh->commit;
			$log->debug("Bill record was written on $host");
			1;
		}
	);
#        Config->dbh->do($brecords_insert, undef, @values);
}

# sub error_email_corresponding_contacts {
#       my ( $self, $err_key, $err_info, $err_data ) = @_;
#       $log->error("HANDLING error_email_corresponding_contacts");
# }

sub error_errsms_insert {
        my ( $self, %p ) = @_;

        $log->error("Error $p{params}{error} - ".($p{params}{err_message}||""));

        my $reason = $p{error_data}{message};
        $reason .= " : " . $p{params}{err_message} if $p{params}{err_message};

        my $data = { %{$p{params}{identifiers}} };
        $data->{errcode} = $p{params}{error};
        $data->{errmessage} = $reason;
        $data->{service_id} = $p{params}{service}{id};
        $data->{response_id} = $p{params}{response_id};

        $log->debug(Dumper($data));

        $self->errsms_save($data);
}

my @errsms_fields = qw/inbox_id push_id response_id errcode errmessage service_id/;
my $errsms_insert = "insert into tr_errsms (".join(', ', @errsms_fields).",date) values (".join(', ', ('?') x @errsms_fields).",now())";

sub errsms_save {                 # ($data) void
        my ( $self, $data ) = @_;

        Config->dbh->do($errsms_insert, undef, map { $data->{$_} } @errsms_fields);
}

=head2 Non-Class functions

=cut
sub register_param_handler {
        my ( $param, $handler ) = @_;

        unless (exists $ERROR_PARAM_HANDLERS{$param}) {
            push @ERROR_PARAM_ORDER, $param;
        }

        $ERROR_PARAM_HANDLERS{$param} ||= [];
        push @{$ERROR_PARAM_HANDLERS{$param}}, $handler;
}

#v-
#sub out_plug {                    # ($abonent, $service_id, $outcode) integer
#        my ( $self, $abonent, $service_id, $outcode ) = @_;
#
#        my $smsc_id = $self->find_abonent_smsc($abonent);
#        return undef unless $smsc_id;
#        return $self->{outcode}{$smsc_id}{$service_id}{lc $outcode};
#}



=head3 find_abonent_smsc ($self, $abonent) integer

Находит и возвращает ид SMS-центра, через который к нам обращался
абонент с указанным номером.

=cut                              #
#sub find_abonent_smsc {           # ($self, $abonent) integer
#        my ( $self, $abonent ) = @_;
#        my ( $smsc_id ) = Config->dbh->selectrow_array("select smsc_id from tr_num_map where abonent = ?", undef,
#                                                                                                   $abonent);
#        return $smsc_id;
#}


my $hsr_params = { orig_type => { regex => qr/^(push|inbox)$/ },
                                   error_cb => { type => CODEREF, optional => 1 },
                                   orig => { type => HASHREF },
                                   service => { type => HASHREF },
                                   response => { type => SCALAR },
                                   plugin => { type => HASHREF, optional => 1 },
                                   url => { type => SCALAR, optional => 1 } };

sub _prepare_errsms_template {    # ($p) hashref
        my ( $self, $p ) = @_;

        my %args = ( service => $p->{service},
                     url => $p->{url},
                     response => $p->{response} );

        # Attention - need to move somewhere
        # error => 'PARSE_ERROR' - this also

        $args{error_cb} = $p->{error_cb} if ref($p->{error_cb}) eq 'CODE';

        if ($p->{orig_type} eq 'inbox') {
                # Пришедшие нормальным путём СМС-ки
                $args{identifiers} = { inbox_id => $p->{orig}{id} };
                $args{insms_count} = $p->{orig}{transport_count};
                $args{abonent} = $p->{orig}{abonent};
                $args{smsc_id} = $p->{orig}{smsc_id};
                $args{num} = $p->{orig}{num};
		$args{date} = $p->{orig}{date};
                $args{url} = $p->{url};
                $args{err_message} = "ERROR in SVC$p->{service}{id} resonse to MSG$p->{orig}{id}: ";
                $args{orig_inbox_msg} = $p->{orig};
        } elsif ($p->{orig_type} eq 'push') {
                # Или ответы сервиса через PUSH
                $args{identifiers} = { push_id => $p->{orig}{id} };
                $args{err_message} = "ERROR in SVC$p->{service}{id} PUSH$p->{orig}{id}: ";
        }
        return \%args;
}

sub _prepare_outbox_template {    # ($p) hashref
        my ( $self, $p ) = @_;

        my $res =  { service_id => $p->{service}{id},
                     test_flag => $p->{orig}{test_flag} || 0,
                     ($p->{orig_type} =~ /^inbox$/i ? "inbox_id" : "push_id" ) => $p->{orig}{id} };

        my $svc = $self->{set_service}{$p->{service}{id}};
        if ($svc->{resend_smsc_id}) {
                $res->{resend_smsc_id} = $svc->{resend_smsc_id};
                $res->{resend_flag} = 1;
        }

		if ($p->{orig}{is_fraud}) {
			$res->{params}{is_fraud} = $p->{orig}{is_fraud};
		}

        return $res;
}

sub _prepare_billing_template {   # ($p) hashref
        my ( $self, $p ) = @_;
        my $tmpl = { service_id => $p->{service}{id},
                                 ($p->{orig_type} eq 'inbox' ? "inbox_id" : "push_id") => $p->{orig}{id},
                                 test_flag => $p->{orig}{test_flag} || 0,
                                 is_fraud => $p->{orig}{is_fraud},
                                 insms_count => 0,
                                 outsms_count => 0 };
        return $tmpl;
}

my %section_handlers = ( ignore => \&_handle_section_ignore,
                         yell => \&_handle_section_yell,
                         reply => \&_handle_section_reply,
                         'delayed-reply' => \&_handle_section_delayed_reply,
                         notice => \&_handle_section_notice,
                         'notice-pseudo' => \&_handle_section_notice_pseudo,
                         forward => \&_handle_section_forward_text,
                         'force-forward' => \&_handle_section_force_forward,
                         retry => \&_handle_section_retry,
                         return => \&_handle_section_return_text );

sub _handle_section_delayed_reply {
    my ( $self, %p ) = @_;

    # 'delayed-reply' only in PUSH
    unless ($p{params}{orig_type} eq 'push') {
        $self->handle_error(%{$p{templates}{error}},
                            error => 'LOGIC_ERROR',
                            err_message => $p{templates}{error}{err_message} . " 'delayed-reply' only supported in PUSH" );
        return;
    }

    # Validate service
    local $p{params}{service} = $self->{set_service}{$p{params}{service}{id}};
    unless ($p{params}{service}) {
        $self->handle_error(%{$p{templates}{error}},
                            error => 'PUSH_NO_SERVICE',
                            err_message => $p{templates}{error}{err_message} . " service not found for 'delayed-reply' PUSH" );
        return;
    }

    # Load transaction

    my $trn = $self->load_transaction($p{section}{'transaction-id'});
    unless ($trn) {
        $self->handle_error(%{$p{templates}{error}},
                            error => 'LOGIC_ERROR',
                            err_message => $p{templates}{error}{err_message} . " No such transaction in 'delayed-reply'" );
        return;
    }

    # Check service validity
    if (defined $trn->{service_id}) {
      unless ($trn->{service_id} eq $p{params}{service}{id}) {
        $self->handle_error(%{$p{templates}{error}},
                            error => 'LOGIC_ERROR',
                            err_message => $p{templates}{error}{err_message} . " Service-id mismatch in delayed transaction" );
        return;
      }
    }
    # Check transaction time
    my $now = time();
    my $request_time = str2time($trn->{date});

    # 'delayed-reply' only in 10 minutes window
    #if ($now - $request_time > 10 * 60) {
    #    $self->handle_error(%{$p{templates}{error}},
    #                        error => 'LOGIC_ERROR',
    #                        err_message => $p{templates}{error}{err_message} . " 'delayed-reply' out of 10 minutes period");
    #    return;
    #}

    {
        # Override params
        local $p{params}{orig} = $trn;
        local $p{params}{orig_type} = 'inbox';

	local $p{templates}{billing}{inbox_id} = $trn->{id};
	local $p{templates}{sms}{inbox_id} = $trn->{id};

        $self->_handle_section_reply(%p);
    }
}

sub _handle_section_retry {
    my ( $self, %p ) = @_;

    if ($p{params}{orig_type} ne 'inbox') {
        $self->handle_error(%{$p{templates}{error}},
                            error => 'LOGIC_ERROR',
                            err_message => $p{templates}{error}{err_message} . "'retry' not in INBOX response" );
        return;
    }

    my $tm = $p{section}{timeout} || 60;

    # Lower and upper limit for message in queue
    if ($tm < 60) {
        $tm = 60;
    }

    if ($tm > 3600 * 24) {
        $tm = 3600 * 24;
    }

    my $new_time = POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime(time + $tm));

    my $try = 0;#HACK <to skip the letters about message handling> ++$p{params}{orig}{try_count};

    $self->{inbox}->schedule_retry( $p{params}{orig}{id}, $try, $new_time);
    $log_messages->info("Handled 'retry' section for MSG$p{params}{orig}{id} - scheduled retry at '$new_time'");
}

sub _handle_section_ignore {      # (%params) void
        my ( $self, %p ) = @_;
        $log_messages->info("Handled 'ignore' section for MSG$p{params}{orig}{id}");
        return;
}

sub _handle_section_yell {        # (%params) void
    my ( $self, %p ) = @_;

    my $cc;
    my @emails;
    my $svc = $self->{set_service}{$p{params}{service}{id}};
    my $mgr = $self->{set_cyka_manager}{$svc->{manager_id}};

    if ($mgr and $mgr->{email}) {
        push @emails, $mgr->{email};
    }

    if ($svc->{email}) {
        push @emails, $svc->{email};
    }

    $cc = join ", ", @emails;

    my $sdesc = "SVC$p{params}{service}{id} (".$self->describe_service($p{params}{service}{id}).")";

    yell( "Service $sdesc called manual yell\nSection content is:\n\n$p{section}{data}",
          cc => $cc,
          code => 'NOTICE',
          process => 'DISPATCHER',
          header => "Manual yell called by service $sdesc",
          contact_group => 'services' );

    $log_messages->info("Handled 'yell' section for $p{params}{orig_type}$p{params}{orig}{id}");

    return;
}

sub _handle_section_reply {
        my ( $self, %p ) = @_;

        local $p{templates}{sms}{registered_delivery} = 0;
        my $rd = $p{section}{'registered-delivery'};
        if ( defined($rd) and $rd > 0 ) {
          $p{templates}{sms}{registered_delivery} = 1;
        }

        if ($p{params}{orig_type} ne 'inbox') {
                $self->handle_error(%{$p{templates}{error}},
                                                        error => 'LOGIC_ERROR',
                                                        err_message => $p{templates}{error}{err_message} . "'reply' section in PUSH request" );
                return;
        }

        my $msg = { %{$p{templates}{sms}} };

        $msg->{smsc_id} = $p{params}{orig}{smsc_id};
        $msg->{num} = $p{params}{orig}{num};
        $msg->{operator_id} = $p{params}{orig}{operator_id};
        $msg->{transaction_id} = $p{params}{orig}{transaction_id};

        if ($msg->{transaction_id} and $msg->{transaction_id} =~ /kpk=\{\d+\}/ and $p{section}{spamtail}) {
            $msg->{transaction_id} =~ s/kpk=\{\d+\}//;
            $msg->{transaction_id} ||= undef;
        }


        $msg->{content_type} = $p{section}{'content-type'} || 'text/plain';
        $msg->{target_type} = $p{section}{'target-type'};
	$msg->{tariff} = $p{section}{tariff} if exists $p{section}{tariff};
        $msg->{abonent} = $p{params}{orig}{abonent};
        $msg->{transport_type} = 'sms';
        $msg->{msg} = $p{section}{data};

        # М.б., необходимо изменить короткий номер.
        my $plug;
        if (defined($plug = $p{section}{plug}) and length($plug) > 0) {
            my $tmp = $self->{outcode}{$p{params}{orig}{operator_id}}{$p{params}{service}{id}}{lc $plug};
            if ($tmp) {
                $log_messages->info("While processing 'reply' section for MSG$p{params}{orig}{id} - short number '$msg->{num}' replaced with '$tmp'");
                $msg->{smsc_id} = $self->{outcode_smsc}{$p{params}{orig}{operator_id}}{$p{params}{service}{id}}{lc $plug} || $msg->{smsc_id};
                $msg->{num} = $tmp;
            } else {
                yell( join("\n\n",
                           "Service: ".$self->describe_service($p{params}{service}{id}),
                           "Plug: $plug",
                           "Original short number: $msg->{num}"),
                      cc => join(", ", $self->service_contacts($p{params}{service}{id})),
                      header => "No outcode for 'reply' section (SVC:".$self->describe_service($p{params}{service}{id}).")",
                      code => 'WARNING',
                      process => 'DISPATCHER' );
            }
        }

        my $sent = $self->send_message($msg,$p{section});

        # Store billing info
        my $bill = { %{$p{templates}{billing}} };
        $bill->{dir} = 2;
        $bill->{outbox_id} = $sent->{outbox_id};
        $bill->{operator_id} = $msg->{operator_id};
        $bill->{smsc_id} = $msg->{smsc_id};
        $bill->{num} = $msg->{num};
        $bill->{abonent} = $msg->{abonent};
        $bill->{subservice_id} = $p{section}{'subservice-id'};
	$bill->{date} = $sent->{date};

        $self->brecords_save($bill);

        $log_messages->info("Handled 'reply' section for MSG$p{params}{orig}{id} - OUTBOX$sent->{outbox_id}");
        return $sent->{outbox_id};
}

sub _handle_section_notice__credit_helper {
    my ( $self, $p ) = @_;

    for my $field (qw/credit-message-id credit-installment/) {
        if ($p->{section}{$field} !~ /^\d+$/ ) {
            $self->handle_error(%{$p->{templates}{error}}, error => 'LOGIC_ERROR',
                                err_message => $p->{templates}{error}{err_message} . " $field parameter with non-numeric value or not specified" );
            return 'ignore';
        }
    }

    my $credit_message = Config->dbh->selectrow_hashref(
        "select * from tr_credit_messages where id = ?", undef, $p->{section}{'credit-message-id'},
    );

    unless ($credit_message) {
            $self->handle_error(%{$p->{templates}{error}}, error => 'LOGIC_ERROR',
                                err_message => $p->{templates}{error}{err_message} . " no credit message with id $p->{section}{'credit-message-id'}" );
            return 'ignore';
    }

    if (@{$p->{section}{destinations}} != 1 or $p->{section}{destinations}[0] ne $credit_message->{abonent}) {
        $self->handle_error(%{$p->{templates}{error}}, error => 'LOGIC_ERROR',
                            err_message => $p->{templates}{error}{err_message} . " 'notify' section with 'kontent po karmanu' must refer only to abonent that initiated transaction" );
        return 'ignore';
    }

    if ($p->{params}{service}{id} != $credit_message->{service_id}) {
        $self->handle_error(%{$p->{templates}{error}}, error => 'LOGIC_ERROR',
                            err_message => $p->{templates}{error}{err_message} . " service_id mismatch while trying to create 'kontent-po-karmanu' mt message" );
        return 'ignore';
    }

    $p->{templates}{sms}{add_credit_details} = {
        original_id => $p->{section}{'credit-message-id'},
        installment => $p->{section}{'credit-installment'},
    };

    $p->{routing_override} = [
        $credit_message->{smsc_id}, $credit_message->{operator_id}, $credit_message->{num},
    ];

    $log->info($p->{routing_override});

    return;
}

my %restricted_ops = (
		107=>1, # Tele2
		105=>1,138=>1,175=>1,176=>1,177=>1,178=>1,179=>1,180=>1,181=>1, # MTS
		109=>1,110=>1,112=>1,113=>1,116=>1,119=>1,121=>1,127=>1, #Megafon
		122=>1, #BWC
		120=>1, #Beeline

		534=>1, #MTS
#		532=>1, 649=>1, 647=>1, 645=>1, 643=>1, 651=>1, 655=>1, 653=>1, 657=>1, #Megafon
		577=>1, #Tele2
		536=>1, #Beeline
		);
sub _handle_section_notice_pseudo {
  my ( $self, %p ) = @_;

  my $was_sent = "";

  local $p{templates}{sms}{registered_delivery} = 0;
  my $rd = $p{section}{'registered-delivery'};
  if ( defined($rd) and $rd > 0 ) {
    $p{templates}{sms}{registered_delivery} = 1;
  }

  my $first_outbox_id;
  foreach my $dest (@{$p{section}{destinations}}) {
    my $num;
    my $default_partner_id;

    # Определение оператора
    my ($operator_id, $smsc_id) = $self->{operator}->determine_operator_new(
      $dest, log => $log,
      force_operator => $p{section}{'force-operator-id'},
      hack_operator => $p{section}{'operator-id'},
      provider => $p{section}{'provider'}
    );
    ( $smsc_id, $operator_id, $num ) = @{$p{routing_override}} if $p{routing_override};
    my @operator_list = ($operator_id);
    unless ($operator_id && $restricted_ops{$operator_id}) {
      @operator_list = ($p{section}{'provider'} and ($p{section}{'provider'} eq 'mks')) ? (627,637) : (619,635);
    }

    # Find plug through which message must be sent
    foreach ( @operator_list ) {
	    $operator_id = $_;
	    $smsc_id = $self->{set_operator}{$operator_id}{smsc_id}
	    	if ({627=>1,619=>1,635=>1,637=>1}->{$operator_id});
	    foreach (grep { $_ } $p{section}{plug}, 'default') {
	      last if $num; # For routing_override
	      $num = $self->{outcode}{$operator_id}{$p{params}{service}{id}}{lc $_};
	      if ($num) {
        	if (my $tmp = $self->{outcode_smsc}{$operator_id}{$p{params}{service}{id}}{lc $_}) {
				    ## plug smsc_overried
	          $smsc_id = $tmp;
        	}
	        elsif ( my $override = $self->route_by_num($operator_id, $num) ) {
        	  $smsc_id = $override;
	        }
        	$default_partner_id = $self->{outcode_default_partner_id}{$operator_id}{$p{params}{service}{id}}{lc $_};
	        last;
      	      }
	    }
	    last if $num;
    }

   # Игнорируем попытки сервиса рассылать сообщения абонентам конкретного оператора
   if ($num =~ /reject/) {
     $log->info("Reject push to abonent '$dest' of operator $operator_id");
     next;
   }

    my $error_details = "\n";

    $error_details .= "Operator: " . ($operator_id ? $self->describe_operator($operator_id) : "<UNKNOWN>") . "\n";
    $error_details .= "SMSC: ". ($self->{set_smsc}{$smsc_id}{name} || "<UNKNOWN>") . "\n";

    # Нет номера - ошибка
    unless ($num) {
      $self->handle_error(
        %{$p{templates}{error}},
        error => 'ROUTING_ERROR',
        err_message => $p{templates}{error}{err_message} . "No outcode for plug '$p{section}{plug}' ,abonent '$dest', smsc: '$smsc_id', operator: '$operator_id'$error_details"
      );
      next;
    }

    # Нет СМСЦ - ошибка
    unless ($smsc_id) {
      $self->handle_error(
        %{$p{templates}{error}},
        error => 'ROUTING_ERROR',
        err_message => $p{templates}{error}{err_message} . "SMSC not found for plug '$p{section}{plug}' and abonent '$dest' (operator - '$operator_id')$error_details"
      );
      next;
    }

    # Send message
    my $msg = { %{$p{templates}{sms}} };

    $msg->{smsc_id} = $smsc_id;
    $msg->{operator_id} = $operator_id;
    $msg->{num} = $num;
    $msg->{transport_type} = 'sms';
    $msg->{content_type} = $p{section}{'content-type'} || 'text/plain';
    $msg->{target_type} = $p{section}{'target-type'};
    $msg->{tariff} = $p{section}{tariff} if exists $p{section}{tariff};
    $msg->{abonent} = $dest;
    $msg->{msg} = $p{section}{data};

    my $sent = $self->send_message($msg,$p{section});

    $was_sent .= ", " if $was_sent;
    $was_sent .= "OUTBOX$sent->{outbox_id}";

    # Store billing info
    my $bill = { %{$p{templates}{billing}} };
    $bill->{dir} = 2;
    $bill->{outbox_id} = $sent->{outbox_id};
    $bill->{smsc_id} = $msg->{smsc_id};
    $bill->{num} = $num;
    $bill->{operator_id} = $msg->{operator_id};
    $bill->{abonent} = $dest;
    $bill->{subservice_id} = $p{section}{'subservice-id'};
    $bill->{partner_id} = $p{section}{partner} || $p{section}{'partner-id'} || $default_partner_id;
    $bill->{date} = $sent->{date};
    $self->brecords_save($bill);

    # Открываем/Обновляем сесcию
    my $svc = $self->{set_service}{$p{params}{service}{id}};
    my $session = $self->{session_manager}->get_session($msg);
    if ($session) {
      $session->{end_date} = str2time($msg->{date});
      $self->{session_manager}->update_session($session);
      $log_messages->info("PSEUDO_SESS: Pseudo session $session->{id} ended");
    }
    my $fake_svc = {id => $svc->{id},
	    	    session_timeout => int($p{section}{timeout}) || $svc->{session_timeout} || 3600
		};
    $session = $self->{session_manager}->create_session($msg, $fake_svc, ($p{section}{keyword}) ? $p{section}{keyword} : '');
    $log_messages->info("PSEUDO_SESS: Pseudo session $session->{id} created");

    $first_outbox_id = $sent->{outbox_id} unless ($first_outbox_id);
  }

    $log->warn("No valid destinations at all while handling 'notice-pseudo' section, while destinations were: ".join(',',@{$p{section}{destinations}}))
      unless $was_sent;
    $log_messages->info("Handled 'notice-pseudo' section for ".uc($p{params}{orig_type})."$p{params}{orig}{id} - following messages was sent '$was_sent'");

    return $first_outbox_id;
}

sub _handle_section_notice {
    my ( $self, %p ) = @_;

    my $was_sent = "";

    local $p{templates}{sms}{registered_delivery} = 0;
    my $rd = $p{section}{'registered-delivery'};
    if ( defined($rd) and $rd > 0 ) {
        $p{templates}{sms}{registered_delivery} = 1;
    }

    if ($p{section}{'credit-message-id'} or $p{section}{'credit-installment'}) {
        my $ignore = $self->_handle_section_notice__credit_helper(\%p);
        return if $ignore;
    }

	my $first_outbox_id;
    foreach my $dest (@{$p{section}{destinations}}) {
        my $num;
        my $default_partner_id;

        # Определение оператора
	# HACK
	$p{section}{'force-operator-id'} = 536 if ($p{section}{'operator-id'} and ($p{section}{'operator-id'}==120) and ($p{section}{'plug'} =~ /^2535\-/));
        my ($operator_id, $smsc_id) = $self->{operator}->determine_operator_new
            ($dest, log => $log,
             force_operator => $p{section}{'force-operator-id'},
             hack_operator => $p{section}{'operator-id'},
     	     provider => $p{section}{'provider'});

        ( $smsc_id, $operator_id, $num ) = @{$p{routing_override}}
            if $p{routing_override};

        # Определение оператора полностью провалилось
        unless ($operator_id) {
            $self->handle_error(%{$p{templates}{error}},
                                error => 'ROUTING_ERROR',
                                err_message => $p{templates}{error}{err_message} . "Operator unknown for abonent '$dest'" );
            next;
        }

        # Find plug through which message must be sent
        foreach (grep { $_ } $p{section}{plug}, 'default') {
            last if $num; # For routing_override
            $num = $self->{outcode}{$operator_id}{$p{params}{service}{id}}{lc $_};
            if ($num) {
                if (my $tmp = $self->{outcode_smsc}{$operator_id}{$p{params}{service}{id}}{lc $_}) {
								# plug smsc_overried
                    $smsc_id = $tmp;
                }
                elsif ( my $override = $self->route_by_num($operator_id, $num) ) {
                    $smsc_id = $override;
                }
                $default_partner_id = $self->{outcode_default_partner_id}{$operator_id}{$p{params}{service}{id}}{lc $_};
                last;
            }
        }

        # Игнорируем попытки сервиса рассылать сообщения абонентам конкретного оператора
        if ($num =~ /reject/) {
            $log->info("Reject push to abonent '$dest' of operator $operator_id");
            next;
        }

        my $error_details = "\n";

        $error_details .= "Operator: " . ($operator_id ? $self->describe_operator($operator_id) : "<UNKNOWN>") . "\n";
        $error_details .= "SMSC: ". ($self->{set_smsc}{$smsc_id}{name} || "<UNKNOWN>") . "\n";

        # Нет номера - ошибка
        unless ($num) {
            $self->handle_error(%{$p{templates}{error}},
                                error => 'ROUTING_ERROR',
                                err_message => $p{templates}{error}{err_message} . "No outcode for plug '$p{section}{plug}' ,abonent '$dest', smsc: '$smsc_id', operator: '$operator_id'$error_details" );
            next;
        }

        # Нет СМСЦ - ошибка
        unless ($smsc_id) {
            $self->handle_error(%{$p{templates}{error}},
                                error => 'ROUTING_ERROR',
                                err_message => $p{templates}{error}{err_message} . "SMSC not found for plug '$p{section}{plug}' and abonent '$dest' (operator - '$operator_id')$error_details" );
            next;
        }
	
	# HACK MTS: Reject HIDDEN MT for MTS subscribers
	if ({501=>0,502=>0,503=>0,504=>0,505=>0,511=>0,512=>0,513=>0,514=>0,515=>0}->{$smsc_id} and ($p{section}{'content-type'} eq 'hidden')) {
		$self->handle_error(%{$p{templates}{error}},
			error => 'ROUTING_ERROR',
			err_message => $p{templates}{error}{err_message} . "Hidden MT is forbidden in MTS" );
		next;
	}
	if ($p{section}{data} =~ /RABOTA\.SX/i) {
		$self->handle_error(%{$p{templates}{error}},
			error => 'ROUTING_ERROR',
			err_message => $p{templates}{error}{err_message} . "Blocked." );
		next;
	}
	if ($p{section}{data} =~ m/^(52536709|56991197|717719|714564|988909)/) {
		$self->handle_error(%{$p{templates}{error}},
			error => 'ROUTING_ERROR',
			err_message => $p{templates}{error}{err_message} . "Blocked." );
		next;
	}

#	if (($p{params}{service}{id} == 5) and $self->{prices}{$operator_id}{$num}{mt} and (not exists $p{section}{subpartner})) {
#		$self->handle_error(%{$p{templates}{error}},
#			error => 'SUBPARTNER_ERROR',
#			err_message => $p{templates}{error}{err_message} . "Subpartner parameter not found in section header" );
#		next;
#	}

        # Send message
        my $msg = { %{$p{templates}{sms}} };

        $msg->{smsc_id} = $smsc_id;
        $msg->{operator_id} = $operator_id;
        $msg->{num} = $num;

        $msg->{transport_type} = 'sms';
        $msg->{content_type} = $p{section}{'content-type'} || 'text/plain';
        $msg->{target_type} = $p{section}{'target-type'};
	$msg->{tariff} = $p{section}{tariff} if exists $p{section}{tariff};
        $msg->{abonent} = $dest;
        $msg->{msg} = $p{section}{data};

        my $sent = $self->send_message($msg,$p{section});

        $was_sent .= ", " if $was_sent;
        $was_sent .= "OUTBOX$sent->{outbox_id}";

        # Store billing info
        my $bill = { %{$p{templates}{billing}} };
        $bill->{dir} = 2;
        $bill->{outbox_id} = $sent->{outbox_id};
        $bill->{smsc_id} = $msg->{smsc_id};
        $bill->{num} = $num;
        $bill->{operator_id} = $msg->{operator_id};
        $bill->{abonent} = $dest;
        $bill->{subservice_id} = $p{section}{'subservice-id'};
        $bill->{partner_id} = $p{section}{partner} || $p{section}{'partner-id'} || $default_partner_id;
	$bill->{date} = $sent->{date};

        $self->brecords_save($bill);

		$first_outbox_id = $sent->{outbox_id} unless ($first_outbox_id);
    }

    $log->warn("No valid destinations at all while handling 'notice' section, while destinations were: ".join(',',@{$p{section}{destinations}}))
        unless $was_sent;
    $log_messages->info("Handled 'notice' section for ".uc($p{params}{orig_type})."$p{params}{orig}{id} - following messages was sent '$was_sent'");
    return $first_outbox_id;
}

sub _handle_section_force_forward {
    my ( $self, %p ) = @_;
    $p{force_forward} = 1;
    $self->_handle_section_forward_text(%p);
}

sub _handle_section_forward_text {
        my ( $self, %p ) = @_;

        my ($op, $short_num) = split /:/, $p{section}{plug}, 2;
	my $abonent = $p{section}{destinations}[0];

	my ($operator_id, $smsc_id) = $self->{operator}->determine_operator_new
		($abonent, log => $log,
		force_operator => $op,
		provider => $p{section}{provider} );
	if ( my $override = $self->route_by_num($operator_id, $short_num) ) {
		$smsc_id = $override;
	}

        unless ($smsc_id =~ /^\d+$/ and $short_num =~ /^(?:\d+|infobip)(:\d+)?$/) {
                $self->handle_error(%{$p{templates}{error}},
                                                        error => 'FORWARD_ERROR',
                                                        err_message => $p{templates}{error}{err_message} . "While forwarding plug must be in '<num>:<num>' format");
                return;
        }

        $log->debug("PF: $smsc_id : $short_num");

#        if ($self->{set_smsc}{$smsc_id}{real_smsc} and not $p{force_forward}) {
#                $self->handle_error(%{$p{templates}{error}},
#                                                        error => 'FORWARD_ERROR',
#                                                        err_message => $p{templates}{error}{err_message} . "Forwarding allowed only to virual SMSCs" );
#                return;
#        }

        my $msg = { %{$p{templates}{sms}} };
        $msg->{smsc_id} = $smsc_id;
        $msg->{operator_id} = $operator_id;
        $msg->{num} = $short_num;
        $msg->{content_type} = $p{section}{'content-type'} || 'text/plain';
        $msg->{target_type} = $p{section}{'target-type'};
	$msg->{tariff} = $p{section}{tariff} if exists $p{section}{tariff};
        $msg->{transport_type} = 'sms';
        $msg->{transaction_id} = $p{params}{orig}{transaction_id};

        $msg->{abonent} = $p{section}{destinations}[0];
        $msg->{msg} = $p{section}{data};

        my $sent = $self->send_message($msg,$p{section});

        # Store billing info
        my $bill = { %{$p{templates}{billing}} };
        $bill->{dir} = 2;
        $bill->{outbox_id} = $sent->{outbox_id};
        $bill->{smsc_id} = $smsc_id;
        $bill->{operator_id} = $operator_id;
        $bill->{num} = $short_num;
        $bill->{abonent} = $msg->{abonent};
        $bill->{subservice_id} = $p{section}{'subservice-id'};
	$bill->{date} = $sent->{date};

        $self->brecords_save($bill);

        $log_messages->info("Handled 'forward' section for MSG$p{params}{orig}{id} - routed OUTBOX$sent->{outbox_id} via SMSC$smsc_id:$short_num");
        return;
}

sub _handle_section_return_text {
        my ( $self, %p ) = @_;

        # XXXX add protection for missing abonent!
        my $abonent = $p{section}{destinations}[0];
        my $short_num = $p{section}{plug};

		my ($operator_id, $smsc_id) = $self->{operator}->determine_operator_new
		  ($abonent, log => $log,
		   force_operator => $p{section}{'force-operator-id'},
		   hack_operator => $p{section}{'operator-id'},
	  	   provider => $p{section}{provider} );

	if ( my $override = $self->route_by_num($operator_id, $short_num) ) {
		$smsc_id = $override;
	}

#        my ($smsc_id, $operator_id) = $self->consult_num_map($abonent, 'sms');

#        # feature
#        if ($p{section}{'operator-id'}) {
#            my $tmp_operator_id = $p{section}{'operator-id'};
#            my $tmp = Disp::Operator->load($tmp_operator_id);
#            if ($tmp) {
#                $operator_id = $tmp_operator_id;
#                $smsc_id = $tmp->{smsc_id};
#            }
#        }

        unless ($short_num) {
                $self->handle_error(%{$p{templates}{error}},
                                    error => 'FORWARD_ERROR',
                                    err_message => $p{templates}{error}{err_message} . "short_num must be given in 'plug' field while returning message" );
                return;
        }

		if (my $override = $self->route_by_num($operator_id, $short_num)) {
			$smsc_id = $override;
		}

        unless ($operator_id and $smsc_id) {
                $self->handle_error(%{$p{templates}{error}},
                                    error => 'FORWARD_ERROR',
                                    err_message => $p{templates}{error}{err_message} . "While 'returning' - smsc_id and operator_id cannot be determined for abonent '$abonent'");
                return;
        }


        unless ($self->{set_smsc}{$smsc_id}{real_smsc}) {
                $self->handle_error(%{$p{templates}{error}},
                                                        error => 'FORWARD_ERROR',
                                                        err_message => $p{templates}{error}{err_message} . "Returning allowed only to real SMSCs ($smsc_id)\n".Dumper($self->{set_smsc}{$smsc_id}) );
                return;
        }

        my $msg = { %{$p{templates}{sms}} };
        $msg->{smsc_id} = $smsc_id;
        $msg->{operator_id} = $operator_id;
        $msg->{num} = $short_num;
        $msg->{content_type} = $p{section}{'content-type'} || 'text/plain';
        $msg->{target_type} = $p{section}{'target-type'};
	$msg->{tariff} = $p{section}{tariff} if exists $p{section}{tariff};
        $msg->{transport_type} = 'sms';
        $msg->{transaction_id} = $p{params}{orig}{transaction_id};
        $msg->{abonent} = $abonent;
        $msg->{msg} = $p{section}{data};
	$msg->{params}{smsc_insms_ids} = [ split(/\,/,$p{params}{orig}{transport_ids}) ];

        if ($p{section}{'registered-delivery'}) {
            $msg->{registered_delivery} = 1;
        }

        my $sent = $self->send_message($msg,$p{section});

        # Store billing info
        my $bill = { %{$p{templates}{billing}} };
        $bill->{dir} = 2;
        $bill->{outbox_id} = $sent->{outbox_id};
        $bill->{smsc_id} = $smsc_id;
        $bill->{operator_id} = $operator_id;
        $bill->{num} = $short_num;
        $bill->{abonent} = $msg->{abonent};
        $bill->{subservice_id} = $p{section}{'subservice-id'};
	$bill->{partner_id} = $p{section}{'partner-id'};
	$bill->{date} = $sent->{date};

        $self->brecords_save($bill);

        $log_messages->info("Handled 'return' section for MSG$p{params}{orig}{id} - routed OUTBOX$sent->{outbox_id} via PLUG$smsc_id($short_num) for abonent '$abonent'");
        return;
}

sub handle_service_response {

    eval {
        handle_service_response_real(@_);
    };

    if ($@) {
        if ($@ =~ /Explicit or implicit commit is not allowed in stored function or trigger/) {
            $log_messages->error("Error in db_double_do: $@, rolling back");
            eval {
                Config->dbh->rollback();
            };
            handle_service_response_real(@_);
        } else {
            die $@;
        }
    }
}

sub handle_service_response_real {     # (%params) void
        my $self = shift;
        my %p = validate(@_, $hsr_params );

        if ($p{orig_type} !~ /^(inbox|push)$/) {
                $log->error('');
                yell(Dumper(\%p),
                         header => "Unknown orig type '$p{orig_type}'",
                         process => 'DISPATCHER');
                return;
        }

        # Шаблонные хэши для вызова handle_error(), send_message() и brecords_save()
        # Разница между push'ем и обычным ходом SMS именно в них отрабатывается.
        my $templates = { error => $self->_prepare_errsms_template(\%p),
                          sms => $self->_prepare_outbox_template(\%p),
                          billing => $self->_prepare_billing_template(\%p) };
        $self->_start_transaction;

        if ($p{orig_type} eq 'inbox') {
                $self->mark_processed($p{orig});
        }

        my $response = $self->response_save(\%p);
        my $parsed = parse_response($p{response});

        $templates->{error}{response_id} = $response->{response_id};

        my $error_detected = 0;
        my ($partner, $legal_owner, $price_category, $content_id);
        my $subservice_id;

        if ($parsed->{result}) {

                $subservice_id = $parsed->{sections}[0]{'subservice-id'};

                # Some special handling for inbox messages
                if ( $p{orig_type} eq 'inbox' ) {

                    # Final report for automatic retries, if needed
                    if (exists $p{orig}{try_count}) {
                        my $svc_desc = $self->describe_service($p{service}{id});
                        yell( join("\n\n",
                                   "Message processing succeed on try $p{orig}{try_count} for service $svc_desc",
                                   "Details at: ".$self->inbox_uri($p{orig}{id}) ),
                              cc => join(", ", $self->service_contacts($p{service}{id})),
                              header => "Message handled successfully (SVC:$svc_desc)",
                              code => 'SUCCESS',
                              process => 'DISPATCHER',
							  contact_group => 'services');
                    }

		    ### Unique message logic
		    my $service = $self->{set_service}{$p{service}{id}};
		    if ($service->{unique_message_pattern}
                	and $service->{unique_message_dub_response}
		        and ($p{orig}{msg_trans} =~ /$service->{unique_message_pattern}/)
#			and ($service->{id} == 5) # for testing
			and $self->check_unique_message($p{orig}, $service)) {
			$parsed->{sections} = [ { destinations => [ $p{orig}{abonent} ],
                                                  status => 'reply',
			                          data => $service->{unique_message_dub_response} } ];
			yell( "Service: ".$self->describe_service($service->{id})."\nAbonent: $p{orig}->{abonent}\nNum: $p{orig}->{num}\nMessage: $p{orig}->{msg}",
				cc => 'P.Shabanova@alt1.ru',
				header => "Unique message repeated (SVC:".$self->describe_service($service->{id}).")",
				code => 'WARNING',
				process => 'DISPATCHER' );
		    }

                    # SMS-tails for normal sms-messages
                    my $spam_period = $self->{set_operator}{$p{orig}{operator_id}}{addon_spam_period};
                    my $spam_message = $self->{set_operator}{$p{orig}{operator_id}}{addon_spam_message};
                    my $spam_expire = $self->{set_operator}{$p{orig}{operator_id}}{addon_spam_expire};

		    my $service_id = $p{service}{id};
		    my $skip_spamtail = $self->{set_service}{$service_id}{skip_spamtail};

		    my $has_replies;
		    foreach my $section (@{$parsed->{sections}}) {
			if ($section->{status} eq 'reply') {
			     $has_replies = 1;
			}
		    }


                    if ( not $skip_spamtail and
		         $has_replies and
			 $spam_period and
                         $spam_period > 0 and
                         $spam_message and
                         length($spam_message) > 0 and
                         time() < $spam_expire and
			 $p{orig}{transport_type} eq 'sms' and
                         $self->can_send_spam($p{orig}{abonent}, $p{orig}{operator_id}, $spam_period)
                       ) {
                        push @{$parsed->{sections}}, { destinations => [ $p{orig}{abonent} ],
                                                       status => 'reply',
                                                       spamtail => 'true',
                                                       data => $spam_message };
                        $log_messages->info("Added SPAMTAIL to MSG$p{orig}{id} of abonent $p{orig}{abonent} (OPERATOR$p{orig}{operator_id}, SVC$service_id) with skip_spamtail $skip_spamtail");
                    }
                }

                foreach my $section (@{$parsed->{sections}}) {

                        # Партнёры и правообладатели - только для секций типа reply!
                        if ($section->{status} eq 'reply') {
                            $partner ||= $section->{partner};
                            $legal_owner ||= $section->{'content-owner'};
                            $price_category ||= $section->{'content-price-category'};
                            $content_id ||= $section->{'content-id'};
                        }
			if ($section->{status} eq 'ignore') {
				$partner ||= $section->{partner} || $section->{'partner-id'};
			}


                        my $section_handler = $section_handlers{$section->{status}};
                        if (ref($section_handler) eq 'CODE') {
                                $section_handler->( $self,
                                                                        section => $section,
                                                                        response => $response,
                                                                        templates => $templates,
                                                                        params => \%p );
                        } else {
                            # Unknown section status
                            $log->error("Unknown section status: $section->{status}");
                            my $svc_desc = $self->describe_service($p{service}{id});
                            yell( join("\n\n",
                                       "Incorrect section while handlind response of service $svc_desc",
                                       "Section was: ". Dumper($section)."\n"),
                                  cc => join(", ", $self->service_contacts($p{service}{id})),
                                  header => "Invalid section in service '$svc_desc' response",
                                  code => 'ERROR',
                                  process => 'DISPATCHER' );
                        }
                }
        } else {
                $error_detected = 1;
                $self->handle_error(%{$templates->{error}},
                                                        error => 'PARSE_ERROR',
                                                        err_message => $templates->{error}{err_message} . "service response parse error - $parsed->{error}" );
        }

        # Для нормальных СМС биллим входящее сообщение - в независимости от того, была ли ошибка.
        if ($p{orig_type} eq 'inbox') {
                my $bill = { %{$templates->{billing}} };

                $bill->{insms_count} = $p{orig}{transport_count};
                $bill->{smsc_id} = $p{orig}{smsc_id};
                $bill->{num} = $p{orig}{num};
                $bill->{dir} = 1;
                $bill->{abonent} = $p{orig}{abonent};
                $bill->{operator_id} = $p{orig}{operator_id};
                $bill->{subservice_id} = $subservice_id;
		$bill->{date} = $p{orig}{date};

                my $cyka_it = 0;
                unless ($error_detected) {
                        $bill->{partner_id} = $self->determine_partner($p{plugin}, $partner);

                        if ($content_id and $content_id =~ /^\d+$/) {
##                              Config->dbh->do("insert into payment_info set payment_id = 0, content_id = ?, date = now(), transaction_id = ?", undef,
#                                                          $content_id, $p{orig}{id});
                            Config->dbh->do("insert into cyka_downloads_wap  set
                                             id = NULL,  date = now(), transport_type = '10',cyka_processed = 1,
                                             content_id = ?, transaction_id = ?, comment = 'wap-partnerka'",
                                            undef, $content_id, $p{orig}{id});
                        }
                        $log->debug("The price category has value '".($price_category||'undef')."'");
                        if (defined($price_category)) {
                                $bill->{category} = $price_category;
                                $bill->{legal_owner_id} = $self->login_to_money_consumer_id('generic_content_owner');
                        }
                        $cyka_it = 1;
                }

                $self->brecords_save($bill);
#               Config->dbh->do("call cyka_bill_record(?)", undef, $p{orig}{id})
#                 if $cyka_it;
        }
        $self->_end_transaction;
}

sub handle_service_response_simple {
        my $self = shift;
#        my %p = validate(@_, $hsr_params );
		my %p = @_;

        my $templates = { error => $self->_prepare_errsms_template(\%p),
                          sms => $self->_prepare_outbox_template(\%p),
                          billing => $self->_prepare_billing_template(\%p) };
        $templates->{error}{response_id} = 0;

        $self->_start_transaction;

		my $section_handler = $section_handlers{$p{section}{status}};
		my $outbox_id;
		if (ref($section_handler) eq 'CODE') {
			$p{section}{destinations} = [ $p{section}{destination} ];
#			my $log = get_logger('push');
#			$log->debug(Dumper(\%p));
			$outbox_id = $section_handler->( $self,
											 section => $p{section},
											 templates => $templates,
											 params => \%p );
		} else {
			# Unknown section status
			$log->error("Unknown section status: $p{section}{status}");
			my $svc_desc = $self->describe_service($p{service}{id});
			yell( join("\n\n",
					   "Incorrect section while handlind response of service $svc_desc",
					   "Section was: ". Dumper($p{section})."\n"),
				  cc => join(", ", $self->service_contacts($p{service}{id})),
				  header => "Invalid section in service '$svc_desc' response",
				  code => 'ERROR',
				  process => 'DISPATCHER' );
		}

        $self->_end_transaction;
		return $outbox_id;
}

sub determine_partner {
        my ( $self, $plugin, $partner_from_service ) = @_;

        my $pid_s = $self->login_to_money_consumer_id($partner_from_service);
        my $pid_p = $plugin->{partner_id};

        $log->debug("Partner detection: plugin - '".($pid_p || "<none>")."', service - '".($pid_s||"<none>")."(".($partner_from_service || "<none>").")");

        if ($plugin->{ignore_partner_from_service} || (!$partner_from_service)) {
                $log->debug("Partner detection: ignoring from service");
                return $pid_p;
        }

        my $ret = $pid_s || $plugin->{partner_id};
        $log->debug("Partner detection: got '".($ret || "<none>")."'");
        return $ret;
}

my @response_fields = qw/response service_id request_url type orig_id msg_sent/;
my $response_insert = "insert into tr_sresponse(".join(', ', 'date', @response_fields).") values (".join(', ', 'now()', ('?') x @response_fields).")";

sub response_save {               # ($data) integer
        my ( $self, $p ) = @_;

# # 2015-05-19	
#	return { response_id => 0 }
#		if ({1596=>1,1644=>1,1646=>1,1648=>1,1650=>1,1654=>1}->{$p->{service}{id}}); # black list

        my $data = { response => $p->{response},
                                 service_id => $p->{service}{id},
                                 request_url => $p->{url},
                                 orig_id => $p->{orig}{id},
                                 type => $p->{orig_type},
                                 msg_sent => $p->{orig}{msg_orig}
                           };

        Config->dbh->do($response_insert, undef,
                                        map { $data->{$_} } @response_fields );

        my ( $resp_id ) = Config->dbh->selectrow_array("select last_insert_id()");

        return { response_id => $resp_id };
}

sub error_do_callback {           # () void
        my ( $self, %p ) = @_;

        my $tmp;
        if (ref($tmp=$p{params}{error_cb}) eq 'CODE') {
                my $reason = $p{error_data}{message};
                $reason .= " : " . $p{params}{err_message} if $p{params}{err_message};
                $tmp->($reason);
        }
}

sub error_billing_insert_incoming { # () void
        my ( $self, %p ) = @_;

        my %data = %{$p{params}{identifiers}};
        $data{date} = $p{params}{date};
        $data{service_id} = $p{params}{service}{id};
        $data{abonent} = $p{params}{abonent};
        $data{plug_id} = $p{params}{plug_id};
        $data{insms_count} = $p{params}{insms_count} || 0;
        $data{dir} = 1;

        $self->brecords_save(\%data);
}

sub error_restart_inbox {
    my ( $self, %p ) = @_;

    return unless exists $p{params}{orig_inbox_msg};

    my $orig = $p{params}{orig_inbox_msg};
    my $svc_id = $p{params}{service}{id};

    unless ($svc_id) {
        # XXX yell about absence of service
        return;
    }

    my $try_count = $orig->{try_count} || 0;
    $try_count++;

    $p{params}{err_message} ||= "";

    my $policy = $auto_restart_policy{$orig->{transport_type}}
      || $auto_restart_policy{_default_};

    $log->debug("Selected restart policy ".Dumper($policy));

    if ($try_count > @$policy) {
        $p{params}{err_message} .= "\nMSG$orig->{id} ${try_count}TH(OF ".(scalar @$policy).") TRY FAILED, MESSAGE PROCESSING STOPPED\n";
        my $svc_desc = $self->describe_service($svc_id);
        yell( join("\n\n",
                   "Whole auto-retry process failed for service $svc_desc",
                   "Details at: ".$self->inbox_uri($orig->{id}),
                   "Last error: \n". $p{params}{err_message} ),
              cc => join(", ", $self->service_contacts($svc_id)),
              header => "Message handling totally failed on try $try_count for service $svc_desc",
              code => 'ERROR',
              process => 'DISPATCHER',
			  contact_group => 'services');
        return;
    }

    # at least 1 minute of grace time for service
    my $minimum_restart_time = time + 60;
    my $normal_restart_time = str2time($orig->{date}) + 60 * $policy->[$try_count - 1];

    my $max_time = max($minimum_restart_time, $normal_restart_time);

    $log->info("RESTART MIN: $minimum_restart_time, NORM: $normal_restart_time, selected $max_time");

    my $time = POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime($max_time));

    $log_messages->info("MSG$orig->{id} ${try_count}TH TRY SCHEDULED AT $time DUE TO ERROR");

    $p{params}{err_message} .= "\nMSG$orig->{id} ${try_count}TH(OF ".(scalar @$policy).") TRY SCHEDULED AT $time DUE TO ERROR\n";

    $self->{inbox}->schedule_retry( $p{params}{orig_inbox_msg}{id},
                                    $try_count,
                                    $time );
}

sub error_do_yell {               # () void
        my ( $self, %p ) = @_;

        my $add_info = "";
        my $cc;

        my $header_add = "";

        if ($p{params}{service}{id}) {
            my @emails;
            my $svc = $self->{set_service}{$p{params}{service}{id}};
            my $mgr = $self->{set_cyka_manager}{$svc->{manager_id}};

            if ($mgr and $mgr->{email}) {
                push @emails, $mgr->{email};
            }

            if ($svc->{email}) {
                push @emails, $svc->{email};
            }
            my $svc_desc = $self->describe_service($p{params}{service}{id});
            $add_info .= "\n\nService name: $svc_desc";
            $header_add = " (SERVICE: $svc_desc)";
            $cc = join ", ", @emails;
        }

		if ($p{params}{error} eq 'NO_SUCH_SERVICE') {
			$cc = join ", ", $self->operator_contacts($p{params}{operator_id});
		}

        if ($p{params}{identifiers}{inbox_id}) {
                $add_info .= "\n\n".Config->param('message_link').$p{params}{identifiers}{inbox_id};
        }

        if (my $tmp_url = $p{params}{url}) {
            $tmp_url =~ s/^http:/хттп:/;
            $add_info .= "\n\nURL was: $tmp_url";
        }

        $add_info .= "\n\nService response:\n----------\n".($p{params}{response}||"")."\n----------";

	my $ignore_email = ($p{error_data}{notify} eq 'none') ? 1 : 0;
        yell( "$p{params}{err_message}$add_info",
                  cc => $cc,
                  code => $p{params}{error},
                  process => 'DISPATCHER',
                  header => $p{error_data}{message} . $header_add,
                  contact_group => $p{error_data}{notify},
		  ignore => {'e-mail' => $ignore_email}
                );
}

sub consult_num_map {
        my ($self, $dest, $transport) = @_;

        my @result = Config->dbh->selectrow_array("select smsc_id, operator_id from tr_num_map where abonent = ? and transport_type = ?",
                                                                                          undef, $dest, $transport);
        return @result;
}

sub describe_service {
        my ( $self, $service_id ) = @_;
        my $svc = $self->{set_service}{$service_id};
        return "$self->{set_sgroup}{$svc->{sgroup_id}}{name} - $svc->{name}";
}

sub describe_operator {
    my ( $self, $operator_id ) = @_;

    my $op = $self->{set_operator}{$operator_id};

    if ($op) {
        my $parent_op = $self->{set_operator}{$op->{parent_id}};


        return "$parent_op->{name} - $op->{name} ($operator_id)";
    }

    return "INCORRECT_OPERATOR_ID";
}

sub can_send_spam {
    my ( $self, $abonent, $operator_id, $period ) = @_;

    return unless $abonent;

    my ( $ab, $time ) = Config->dbh->selectrow_array("select abonent, last_spam_time from tr_abonent_spam where abonent = ? and operator_id = ? ",
                                                     undef, $abonent, $operator_id);

    # No entry in database
    unless ( $ab ) {
        Config->dbh->do("replace into tr_abonent_spam (abonent, operator_id, last_spam_time) values (?, ?, ?)",
                        undef, $abonent, $operator_id, time());
        return 1;
    }

    if ( $time + $period < time ) {
        Config->dbh->do("update tr_abonent_spam SET last_spam_time = ? WHERE abonent = ? and operator_id = ?",
                        undef, time, $abonent, $operator_id);
        return 1;
    }

    return 0;
}

=head3 service_contacts ($service_id) list

Возвращает связанные с указанным сервисом e-mail'ы в виде
списка. Связанные e-mail'ы - это например, e-mail'ы менеджера и
разработчика.

=cut
sub service_contacts {
    my ( $self, $service_id ) = @_;

    my @emails;
    my $svc = $self->{set_service}{$service_id};
    my $mgr = $self->{set_cyka_manager}{$svc->{manager_id}};

    unless ($svc) {
        # XXX yell
        return;
    }

    if ($mgr and $mgr->{email}) {
        push @emails, $mgr->{email};
    }

    if ($svc->{email}) {
        push @emails, $svc->{email};
    }

    return @emails;

}

sub operator_contacts {
	my ( $self, $operator_id ) = @_;
    my $svc = $self->{set_operator}{$operator_id};
    my $mgr = $self->{set_cyka_manager}{$svc->{manager_id}};
	if ($mgr and $mgr->{email}) {
		return ( $mgr->{email} );
	}
	return;
}

sub inbox_uri {
    my ( $self, $inbox_id ) = @_;
    return Config->param('message_link').$inbox_id;
}

sub load_transaction {
	my ( $self, $transaction_id ) = @_;
	my $tr;
	Disp::AltDB::dbh(undef, sub {
		$tr = $_[0]->{db}->selectrow_hashref("select * from tr_inboxa where id = ?",
			undef, $transaction_id);
		$tr->{brecord} = $_[0]->{db}->selectrow_hashref("select * from tr_billing where inbox_id = ? and outbox_id = 0",
			undef, $tr->{id}) if $tr;
		$tr;
	});
	$tr; 
}

sub brecord_for_transaction {
    my ( $self, $transaction ) = @_;
   return Config->dbh->selectrow_hashref("select * from tr_billing where inbox_id = ? and outbox_id = 0", undef, $transaction->{id});
}

=head3 direct_response_section($msg, $service, $text) string

Генерирует текст секции для посылки текстового сообщения в ответ на
какое либо-сообщение. Используется для отправки сообщений о фроде и
нахождении в чёрном списке. Для генерации сообщения используется
различная информация, в том числе тип сервиса.

=cut
sub direct_response_section {
    my ( $self, $msg, $service, $text ) = @_;

    my $content_type = ($msg->{transport_type} eq 'ussd' ) ? 'ussd-pssr_response' : 'text/plain';

    return <<EOF;
status: reply
destination: $msg->{abonent}
bill-incoming: transport-direct-in
bill-outgoing: transport-direct-out
content-type: $content_type

$text
EOF
}

sub route_by_num {
	my ( $self, $operator_id, $num ) = @_;
	return $self->{set_operator_route}{$operator_id}{$num}{out_smsc_id};
}

sub services {
	my ( $self ) = @_;
	return $self->{set_service};
}

1;
__END__

DROP TABLE IF EXISTS set_operator_route;
CREATE TABLE set_operator_route (
id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
operator_id INTEGER UNSIGNED NOT NULL REFERENCES set_operator_t(id),
num VARCHAR(20) NOT NULL,
out_smsc_id INTEGER UNSIGNED NOT NULL REFERENCES set_smsc_t(id),
UNIQUE KEY (operator_id, num));


CREATE TABLE tr_service_out_control(
 service_id integer primary key,
 translit_triggered tinyint not null default 0,
 max_parts_count integer not null default 0 );

update set_service_t set skip_spamtail = 1 where id in (692,956,962,942,918,940,955,961);



