#!/usr/bin/perl
package Disp::HTTP_SMSReceiver2;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

# sub POE::Kernel::TRACE_DEFAULT  () { 1 }

use POE qw/Component::FastCGI_m/;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use XML::Simple;
use base qw 'Disp::Object';

use Disp::Utils;
use Disp::Config;
use Disp::AltDB;
use Disp::Operator;

use constant process_tab =>
  { http_osmp =>
	{
	 init => sub {
		 my ( $self ) = @_;
		 $self->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
	 },

	 handle_request => sub {
		 my ( $self, $heap, $q ) = @_;

		 my $command = $q->param('command') || "";
		 my $txn_id = $q->param('txn_id') || "";
		 my $account = $q->param('account') || "";
		 my $sum = $q->param('sum') || "";

		 unless ($command =~ /^(pay|check)$/) {
			 # XXX yell here
			 $self->{log}->error("[$heap->{session_id}] Incorrect command '$command' ignored");
			 return osmp_response( $txn_id,
								   300,
								   undef,
								   "Incorect command '$command'" );
		 }

		 unless ($txn_id =~ /^\d+$/) {
			 # XXX yell here
			 $self->{log}->error("[$heap->{session_id}] Incorrect txn_id '$txn_id' ignored");
			 return osmp_response( $txn_id,
								   300,
								   undef,
								   "Incorect txn_id format '$txn_id'" );
		 }

		 ####################################
		 if ($sum < 10) {
			 $self->{log}->info("Sum not in range");
			 return osmp_response( $txn_id,
								   241 );
		 }
		 if ($sum > 300) {
			 $self->{log}->info("Sum not in range");
			 return osmp_response( $txn_id,
								   242 );
		 }

		 unless ($account =~ /^(7\d{10})\/(.+)$/) {
			 $self->{log}->info("Incorrect account");
			 return osmp_response( $txn_id,
								   4 );
		 }

		 my $abonent = $1;
		 my $msg = $2;

		 unless ( $abonent =~ /^(79265550017|79261193892|79262626254)$/ ) {
			 $self->{log}->info("Incorrect account");
			 return osmp_response( $txn_id,
								   5 );
		 }

		 # новая транзакция
		 my $sms = { smsc_id => $self->{smsc_id},
					 num => '666',
					 abonent => $abonent,
					 msg => $msg,
					 data_coding => 0,
					 esm_class => 0,
					 transaction_id => $txn_id
				   };

		 if ($command eq 'check') {
			 $self->{log}->info("[$heap->{session_id}] Check passed for txn_id: '$txn_id'");
			 return osmp_response( $txn_id,
								   0 );
		 }

		 my $maybe_prev = $self->{db}{db}->selectrow_hashref("select * from tr_smsc_transaction_archive where smsc_id = ? and transaction_id = ?", undef,
															 $self->{smsc_id}, $txn_id);

		 my $id;
		 if ($maybe_prev) {
			 $id = $maybe_prev->{inbox_id};
		 } else {
			 $id = $self->{db}->insert_sms_into_insms($sms);
			 $self->{db}{db}->do("insert into tr_smsc_transaction_archive set smsc_id = ?, transaction_id = ?, completed = 0, inbox_id = ?", undef,
								 $self->{smsc_id}, $txn_id, $id);
			 $self->{log}->info("[$heap->{session_id}] SMS$id added - abonent: '$sms->{abonent}', num: '$sms->{num}', text: '$sms->{msg}', txn_id: '$txn_id'");
		 }

		 return osmp_response($txn_id,
							  0,
							  $id,
							  undef,
							  $sum); # decode here
		 ####################################

		 my ( $valid, $abonent, $text ) = osmp_parse_account($account);

		 unless ($valid) {
			 # XXX yell here
			 $self->{log}->error("[$heap->{session_id}] Incorrect account '$account' ignored");
			 return osmp_response( $txn_id,
								   4,
								   undef,
								   'Cant extract abonent and text from account');
		 }
		 unless ( $self->{db}->num_price($self->{operator_id}, $sum) > 0) {
			 $self->{log}->error("[$heap->{session_id}] Unknown summ '$sum' ignored");
			 # XXX yell here
			 return osmp_response( $txn_id,
								   300,
								   undef,
								   'Unknown price, cant determine action'
								 );
		 }

		 if ($command eq 'check') {
			 $self->{log}->info("[$heap->{session_id}] Check passed for txn_id: '$txn_id'");
			 return osmp_response( $txn_id,
								   0 );
		 }

		 my $tr = osmp_record_transaction($q);

		 # OSMP - законченная транзакция???
		 # OSMP - неоконченная транзакция???

		 # новая транзакция
		 my $sms = { smsc_id => $self->{smsc_id},
					 num => $sum,
					 abonent => $abonent,
					 msg => $text,
					 data_coding => 0,
					 esm_class => 0,
					 transaction_id => $txn_id
				   };
		 my $id = $self->{db}->insert_sms_into_insms($sms);
		 $self->{log}->info("[$heap->{session_id}] SMS$id added - abonent: '$sms->{abonent}', num: '$sms->{num}', text: '$sms->{msg}', txn_id: '$txn_id'");

		 $heap->{insms_id} = $id;
		 $heap->{check_count} = 1;
		 $heap->{txn_id} = $txn_id;

		 return 5;
	 },
	 handle_alarm => sub {
		 my ( $self, $heap ) = @_;

		 $self->{log}->debug("[$heap->{session_id}] Fetching...");

		 my $smss = $self->{db}->get_sms_ready_to_send_tr($self->{smsc_id},$heap->{txn_id});
		 my $smc = scalar keys(%$smss);

		 if ($smc > 0) {
			 # billing insms_count, cyka_processed update
			 # mark transaction as completed

			 # Undefined behaviour when more than one SMS
			 # But it's not our problem =)
			 while (my ($k,$v) = each %$smss) {

				 $self->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
				 $self->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$self->{sms_traffic_smsc_id}");
				 $self->{db}->resend_through_smstraffic($v->{outbox_id}, $self->{sms_traffic_smsc_id});
				 $self->{db}->set_sms_new_status($v->{id},2,0,0,0);
				 $self->{db}->set_sms_delivered_status($v->{id});

				 return osmp_response($heap->{txn_id},
									  0,
									  $heap->{insms_id},
									  $v->{msg} ); # decode here
			 }
		 }

		 $heap->{check_count}++;
		 if ($heap->{check_count} > 9) {
			 $self->{log}->error("[$heap->{session_id}] No response for SMS$heap->{insms_id}, giving up");
			 return osmp_response($heap->{txn_id},
								  1,
								  undef,
								  'Service doesnt responded in time')
		 }

		 return 5;
	 },
	},
  };

sub new {
	my ($proto, $smsc_id,$debug_level,$smpp_mode) = @_;
	my $self = $proto->SUPER::new();

	my $smpp_mode_name = 'rcv';
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);

	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf",
				 skip_db_connect => 1);

	my $log = init_log($pr_name, $debug_level);
	$log->info("Starting...");

	my $db = new Disp::AltDB;
	my $cnf = $db->get_smsc_configuration($smsc_id);

	$self->{db}				  = $db;
	$self->{log}			  = $log;
	$self->{pr_name}		  = $pr_name;
	$self->{smpp_mode_name}	  = $smpp_mode_name;
	$self->{state}			  = 'stopped';
	$self->{status}			  = 'start';
	$self->{smsc_id}		  = $smsc_id;
	$self->{debug_level}	  = $debug_level;
	$self->{smpp_mode}		  = $smpp_mode;

	@{$self}{keys(%$cnf)} = values(%$cnf);

	my $hndl = process_tab->{$self->{client_type}}->{init};
	if ( ref $hndl eq 'CODE' ) {
		$hndl->($self);
	}

	return $self;
}

sub run {
	my ($self) = @_;
	$self->{log}->info("Listening...");
	POE::Component::FastCGI_m->new( Port => $self->{port},
									Address => '127.0.0.1',
									HandleRequest => sub { $self->handle_request(@_) },
									HandleAlarm => sub { $self->handle_alarm(@_) });
	$self->{log}->info("Entering POE loop...");
	POE::Kernel->run();
}

sub handle_request {
	my ( $self, $session_id, $heap, $request ) = @_;

	$self->{db}->reconnect();

	$heap->{session_id} = $session_id;

	my $hndl = process_tab->{$self->{client_type}}->{handle_request};

	return unless ref $hndl eq 'CODE';

	return $hndl->($self, $heap, $request);
}

sub handle_alarm {
	my ( $self, $session_id, $heap ) = @_;

	my $hndl = process_tab->{$self->{client_type}}->{handle_alarm};

	return unless ref $hndl eq 'CODE';

	return $hndl->($self, $heap);
}

sub init_log {
	my ( $pr_name, $debug_level ) = @_;

	my %log_conf =
	  (
	   "log4perl.rootLogger"									 => "$debug_level, Logfile",
	   "log4perl.appender.Logfile"								 => "Log::Log4perl::Appender::File",
	   "log4perl.appender.Logfile.recreate"						 => 1,
	   "log4perl.appender.Logfile.recreate_check_interval"		 => 300,
	   "log4perl.appender.Logfile.recreate_check_signal"		 => 'USR1',
	   "log4perl.appender.Logfile.umask"						 => "0000",
	   "log4perl.appender.Logfile.filename"						 => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
	   "log4perl.appender.Logfile.layout"						 => "Log::Log4perl::Layout::PatternLayout",
	   "log4perl.appender.Logfile.layout.ConversionPattern"		 => Config->param('log_pattern','sms_client')
	  );
	Log::Log4perl->init( \%log_conf);
	return get_logger("");
}

sub http_response {
	my ( $status, $body, %args ) = @_;

	my $status_line = "Status: $status";
	my $content_type = $args{content_type} || 'text/plain';
	$content_type .= "; charset=$args{charset}" if $args{charset};

	return [ "$status_line\r\nContent-type: $content_type\r\n\r\n$body" ];
}

sub osmp_response {
	my ( $txn_id, $result, $prv_txn, $comment, $sum ) = @_;

	my $data = { 'osmp_txn_id' => $txn_id,
				 'result' => $result };

	$data->{prv_txn} = $prv_txn if defined $prv_txn;
	$data->{comment} = $comment if defined $comment;
	$data->{sum} = $sum if defined $sum;

	my $xml = XMLout({response => $data},
					 KeepRoot => 1,
					 NoAttr => 1,
					 XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>' );

	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'utf-8');
}

sub osmp_parse_account {
	my ( $text ) = @_;
	if ($text =~ /^(7\d{10})\/(.+)/) {
		return ( 1, $1, $2 );
	}
	return;
}

sub osmp_record_transaction {
	my ( $db, $smsc_id, $q ) = @_;
#	my $txn_id = $q->param($txn_id);
}

1;

__END__
DROP TABLE IF EXISTS tr_smsc_transaction_archive;
CREATE TABLE tr_smsc_transaction_archive
(
 smsc_id INTEGER NOT NULL,
 transaction_id INTEGER NOT NULL,
 remote_date DATETIME,
 completed TINYINT NOT NULL DEFAULT '0',
 insms_id INTEGER,
 inbox_id INTEGER,
 exact_price DOUBLE,
 PRIMARY KEY (smsc_id, transaction_id)
);
ALTER TABLE tr_smsc_transaction_archive CHANGE column transaction_id transaction_id BIGINT NOT NULL;

