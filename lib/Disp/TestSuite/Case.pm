#!/usr/bin/perl -w
# $Id: $

=head1 NAME

Disp::TestSuite::Case
=head1 METHODS

=cut
package Disp::TestSuite::Case;
use strict;
use warnings;
use base 'Disp::Object';
use Disp::Utils;

our $VERSION	= 0.1;		# ������ ������.


=head3 with_table ($table) void

������������� ������� �� ���������. ������ �� ����� C<insert()>.

=cut
sub with_table {                  # ($table) void
	my ( $self, $table ) = @_;
	$self->{table} = $table;
	return;
}


=head3 head (@names) void

������������� �������� ��������, ������� � ����������� �����
������������� ��������, ������� ��������� ������ ��������, ������ ���
C<insert()>,

=cut
sub head {                        # (@names) void
	my $self = shift;
	$self->{head} = [ @_ ];
	return;
}


=head3 store($key [,$value]) anything

��������� � �������� ��������� ������.  ��� ������ � ����� ����������
���������� ��������, �����Σ���� ��� ���� ������. � ����� �����������
- ��������� 2-� ��������, ��������� 1-� ��� ����.

=cut
sub store {                       # ($key [,$value]) anything
	my $self = shift;
	my $key = shift;
	if (@_) {
		$self->{storage}{$key} = $_[0];
		return;
	} else {
		return $self->{storage}{$key};
	}
}


=head3 insert (@list) void

��������� SQL-������ "INSERT" ��� ���������� ������. ��� ������������
������ � ����������� ���������� ����� ������������ ��� �������
(������������� C<with_table()>) � ������ �ͣ� �����(C<head()>).

���� ������ ������� ������ - ������, �� ��� ���������������� ��� ��� �
�����������. � ������ ������ �������������� ������ ����:
C<last_insert_id>. ��� ������� C<last_insert_id> � ���� ����������
����� ���������� ��������� INSERT-���������, �� ���� ����������
C<last_insert_id()> � ����������� � ������� C<store()> ��� �����������
�������������.

=cut
sub insert {                      # (@list) void
	my ( $self, @list ) = @_;
	my $params;
	if (ref $list[0]) {
		$params = shift @list;
	}
	$self->dbh->do("insert into $self->{table} (".join(', ', @{$self->{head}}).") values (".
				   join(', ', ("?") x @{$self->{head}}).")", @list);
	if ($params->{last_insert_id}) {
		my ( $id ) = $self->dbh->selectrow_array("select last_insert_id()");
		$self->store($params->{last_insert_id} => $id);
	}
	return;
}

=head3 select ($sql, @placeholder_values) arrayref

��������� SELECT, ���������� �������� placeholder'��. ������� ��
�������� DBI::selectall_arrayref - ��, ��� �������� ������������ �
����, ��������� ��� ����������� ������� ������� ���������
C<Disp::TestSuite::Case>, ������ ��� C<check_count> � C<has_row>.

=cut
sub select {                      # ($sql, @placeholder_values) arrayref
	my ( $self, $sql, @ph ) = @_;
	my $result = {};
	$result->{data} = $self->dbh->selectall_arrayref($sql, { Slice => {} }, @ph);
	return $result;
}

=head3 check_count ($result, $count) void

=cut
sub check_count {                 # ($result, $count) void
	my ( $self, $result, $count ) = @_;
	$self->ok($count > 0, "Expected count > 0");
	$self->ok(@{$result->{data}} == $count, "Checking count of rows");
}


=head3 has_row ($result, @row_data) void

=cut
sub has_row {                     # ($result, @row_data) void
	my ( $self, $result, @row_data ) = @_;
	my $row_data = maplist { $_[0] => $_=>[1] } $self->{head}, \@row_data;
}

=head3 ok ($result, $message) void

=cut                              #
sub ok {                          # ($result, $message) void
	my ( $result, $message ) = @_;
}

1;
