#!/usr/bin/perl -w
# $Id: $

=head1 NAME

Disp::TestSuite::CaseCompiler

=head1 METHODS

=cut
package Disp::TestSuite::CaseCompiler;
use strict;
use warnings;
use Data::Dumper;

our $VERSION	= 0.1;		# ������ ������.

my @lexer_rules = ( qr/^\s*$/ => undef,
					qr/^\[([\w_]+)\]/ => sub { return { type => 'section_start',
														section => $1 } },
					qr/^\@insert\s+([_\w]+)/ => sub { return { type => 'insert',
															table => $1 } },
					qr/^\@select\s+(.*)/ => sub { return { type => 'select',
														   query => "select $1",
														 } },
					qr/^\@rows\s+(\d+)\s*$/ => sub { return { type => 'rows',
															  rows => $1,
															} },
					qr/^\@head\s+(.*)$/ => sub { return { type => 'head',
														  columns => [ split /\s+/, $1 ] } },
					qr/^\@id:(\w+)\s+(.*)$/ => sub { return { type => 'data_row',
															  data => [ split /\t/, $2 ],
															  store_id => $1 } },
					qr/^(.*)$/ => sub { return { type => 'data_row',
												 data => [ split /\t/, $1 ] } },
				  );

=head3 parse_token ($line) arrayref

=cut
sub parse_token {                 # ($line) arrayref
	my ( $line ) = @_;

	for (my $i = 0; $i < @lexer_rules; $i+=2) {
		my ($test, $result) = @lexer_rules[$i, $i+1];
		if (my @val = ($line =~ $test)) {
			return unless ref($result) eq 'CODE';
			return $result->(@val);
		}
	}
	die "No matching rule";
}

=head3 tokenize ($stream) void

=cut                              #
sub tokenize {                    # ($stream) void
	my ( $stream ) = @_;

	my @result;

	while (<$stream>) {
		chomp;
		my $token;
		eval {
			$token = parse_token($_);
		};
		if ($@) {
			warn "Skipping, error in tokenizer: $@";
			next;
		}
		unless ($token) {
			warn "Skipping line";
			next;
		}
		push @result, $token;
	}
	return \@result;
}

my %compile_tab = ( start => { section_start => sub {
								   my ( $token ) = @_;
								   
							   }, },
					section => { insert => { },
								 select => { },
							   },
					insert => {
							   head => { },
							  },
					select => {
							   rows => { },
							  },
					insert_data => { },
					select_data => {
									DEFAULT => { },
								   },
								   );


=head3 compile ($source, $target) boolean

����������� ���� � �������. ���������� true, ���� ���� �������� �����
������ ����������������� �����.

=cut                              #
sub compile {                     # ($source, $target) boolean
	my ( $source, $target ) = @_;

}

1;

__END__
