#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Disp::IpCheck;
use strict;
use warnings;
use lib '../lib';
use Disp::Utils;
#use Log::Log4perl qw/get_logger/;
#use Data::Dumper;
our @EXPORT = qw(check_service_ip check_all_ip check_ip);

################################################################################
#  �������� ������������ Ip
#
#  �������� ������������ �� ������ $ips ����� ������ ����������� �������.
#
#  �������� ��������:
#   - ���� ������ $ips ������ ��� �� ������, ��������� ��� Ip �� ���������
#   - ���� ���� �� �������� '/', �� ������������ Ip-Ip
#   - ���� ���� �������� '/', �� ������������ �� ��������� � ������� Ip-(���� + �����)
#
#  ������� ����� ���� ������ ����� ���������:
#   - ������ �������� - �������/�����, ������: 192.168.0.0/255.255.255.0
#   - ����� CIDR - �������/CIDR, ������: 192.168.0.0/24
################################################################################

# whitelist "����� ����"
my @wl = (
  '192.168.252.104',  # ������� ����
  '192.168.252.199',  # �������� ������
  '178.63.89.198', '185.60.135.94',    # 7hlp
  '78.140.130.146',   # �����
  '172.16.160.118', '213.21.201.48',  # ����� (test)
  '172.16.100.0/24',  # A1Systems
  '172.16.200.0/24',  # A1Agregator
  '193.68.66.2',      # MKS
  '213.21.201.48',    # dt1.riga.ru
  '172.16.160.0/24',  # ���������� ������
  '178.63.66.211',    # dt1.local
  '5.9.113.172'       # bill2.local
);

# �����������
sub new {
  my ($class) = @_;

  my $self = {};
  refresh($self);

  bless $self, $class;
  return $self;
}

# ��������� ����������� ip-������ ��� �������� ������ 5 ���
sub refresh {
  my ($self) = @_;

  unless (defined($self->{last_update_time}) && ($self->{last_update_time} + 5*60 > time)) {
    $self->{services} = Config->dbh->selectall_hashref('select id, ips from set_service', 'id');
    $self->{last_update_time} = time;
  }
}

# �������� ������ ����� - whitelist
sub check_wl {
  my ($self, $ip) = @_;
  my $ips = join(',', @wl);
  return check_ip($self, $ip, $ips);
}

# �������� �� ����������� IP ��� C������
sub check_service_ip {
  my ($self, $ip, $sid) = @_;

  return 1 if (check_wl($self, $ip));
  return 0 unless (defined($sid));

  my $match = 0;
  refresh($self);
  my $ips = $self->{services}{$sid}{ips};

  $match = check_ip($self, $ip, $ips);
  return $match;
}

# �������� �� ����������� IP ��� ���� C�������
sub check_all_ip {
  my ($self, $ip) = @_;

  return 1 if (check_wl($self, $ip));

  my $match = 0;
  my $ips = '';
  foreach (keys %{$self->{services}}) {
    if ($self->{services}{$_}{ips}) {
      $ips .= ',' if ($ips);
      $ips .= $self->{services}{$_}{ips};
    }
  }

  $match = check_ip($self, $ip, $ips);
  return $match;
}

# ����� �������������� IP � ������� �� ������ $ips
sub check_ip {
  my ($self, $ip, $ips) = @_;

  return 0 unless ($ips);

  $ips =~ s/\s//g;
  foreach (split (',', $ips)) {
    my ($network, $cidr) = split ('\/');
    if (defined($cidr)) {
      if ($cidr =~ m/\./) {
        return 1 if ((aton($ip) & aton($cidr)) == aton($network));
      } else {
        return 1 if ((aton($ip) & cidr_to_nmask($cidr)) == aton($network));
      }
    } else {
      return 1 if ($ip eq $network);
    }
  }
  return 0;
}

# �������� ��������� IP � �������
sub ip_vs_net {
  my ($self, $ip, $network, $mask) = @_;

  if ((aton($ip) & aton($mask)) == aton($network)) {
    return 1;
  } else {
    return 0;
  }
}

################################################################################
#  ���������� �������
#  (�������� ����� ��������?)
################################################################################

# ������� IP � �����
sub aton {
  my $addr = shift;
  my ($a0, $a1, $a2, $a3) = split('\.', $addr);
  return $a3 + ($a2<<8) + ($a1<<16) + ($a0<<24);
}

# ������� ����� � IP
sub ntoa {
  my $ip = shift;
  return sprintf("%d.%d.%d.%d",
    (($ip & 0xFF000000)>>24), (($ip & 0xFF0000)>>16), (($ip & 0xFF00)>>8), ($ip & 0xFF));
}

# �������� ����� �� CIDR
sub cidr_to_nmask {
  my $cidr = shift;
  $cidr = 0 if ($cidr < 0);
  $cidr = 32 if ($cidr > 32);
  return ~((1<<(32-$cidr))-1);
}

1;
