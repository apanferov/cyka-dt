#!/usr/bin/perl -w
package Disp::Test;
use strict;
use Log::Log4perl;
use Disp::Config;

our $VERSION	= 0.1;		# Версия пакета.

sub init {
	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg_test.conf");
}

1;
