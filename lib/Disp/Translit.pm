package Disp::Translit;
# -*- coding: cp1251 -*- 
use strict;
use warnings;
use Data::Dumper;

sub transform {
	flatten
	  (antitranslit_filter
	   (password_filter
		(tld_filter
		 (http_filter
		  (email_filter([$_[0]]))))));

}

sub flatten {
	my ( $stream ) = @_;
	my @flat;
	foreach (@$stream) {
		if (ref $_) {
			push @flat, $_->[1];
		} else {
			push @flat, $_;
		}
	}
	return join "", @flat;
}

sub make_filter(&) {
	my ( $work_sub ) = @_;
	return sub {
		my ( $stream ) = @_;

		# print Dumper($stream);

		my @out;
		foreach my $tok (@$stream) {
			if (ref $tok) {
				push @out, $tok;
			} else {
				push @out, $work_sub->($tok);
			}
		}
		return \@out;
	}
}

*email_filter = make_filter {
	my ( $tok ) = @_;
	my @out;

	while ($tok =~ /^(.*?)([-\.a-zA-Z0-9]+)@([-\.a-zA-Z0-9]+)(.*)$/) {
		my $email = $2 . '@' . $3;
		push @out, $1 if $1;
		push @out, [ 'email', $email ];
		$tok = $4;
	}

	push @out, $tok if $tok;

	@out;
};

*http_filter = make_filter {
	my ( $tok ) = @_;
	my @out;

	while ($tok =~ /^(.*?)(http:\/\/[-0-9A-Za-z\.\/\?%+=&_]+)(.*)$/i) {
		push @out, $1 if $1;
		push @out, [ 'http', $2 ];
		$tok = $3;
	}

	push @out, $tok if $tok;

	@out;
};

*password_filter = make_filter {
	my ( $tok ) = @_;
	my @out;

	while ($tok =~ /^(.*?)(:\s*[^,.;\s]+)(.*)$/) {
		push @out, $1 if $1;
		push @out, [ 'password', $2 ];
		$tok = $3;
	}

	push @out, $tok if $tok;

	@out;
};

*tld_filter = make_filter {
	my ( $tok ) = @_;
	my @out;

	#while ( $tok =~ /(.*?)([-A-Za-z0-9\.]+)\.(AC|AD|AE|AERO|AF|AG|AI|AL|AM|AN|AO|AQ|AR|ARPA|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BIZ|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CAT|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|COM|COOP|CR|CU|CV|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EDU|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GOV|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|INFO|INT|IO|IQ|IR|IS|IT|JE|JM|JO|JOBS|JP|KE|KG|KH|KI|KM|KN|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|MG|MH|MIL|MK|ML|MM|MN|MO|MOBI|MP|MQ|MR|MS|MT|MU|MUSEUM|MV|MW|MX|MY|MZ|NA|NAME|NC|NE|NET|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|ORG|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PRO|PS|PT|PW|PY|QA|RE|RO|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TRAVEL|TT|TV|TW|TZ|UA|UG|UK|UM|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|YU|ZA|ZM|ZW|ASIA|EH|KP|ME|RS|TEL)\b(.*)$/ ) {
	while ( $tok =~ /(.*?)([-A-Za-z0-9\.]+)\.(ru|com|org|net|biz|ws|kg)\b(.*)$/i ) {
		push @out, $1 if $1;
		push @out, [ 'tld', $2 . "." . $3 ];
		$tok = $4;
	}
	push @out, $tok if $tok;

	@out;
};

*antitranslit_filter = make_filter {
	my ( $tok ) = @_;
	my @out;

	my @words = split /([\s(){},\.;:!-])/, $tok;
	my @new_words;

	foreach my $word (@words) {
		if ($word =~ /^[a-z']+$/i ) {
			push @new_words, antitranslit($word);
		} else {
			push @new_words, $word;
		}
	}

	join "", @new_words;
};

sub antitranslit {
	my ( $word ) = @_;

	$word =~ s/S[cC][hH]/�/g;
	$word =~ s/s[cC][hH]/�/g;

	$word =~ s/Z[hH]/�/g;
	$word =~ s/z[hH]/�/g;
	$word =~ s/S[hH]/�/g;
	$word =~ s/s[hH]/�/g;
	$word =~ s/C[hH]/�/g;
	$word =~ s/c[hH]/�/g;
	$word =~ s/Y[uU]/�/g;
	$word =~ s/y[uU]/�/g;
	$word =~ s/Y[aA]/�/g;
	$word =~ s/y[aA]/�/g;

	$word =~ tr/ABVGDEZIJKLMNOPRSTUFHC'Yabvgdezijklmnoprstufhc'yWw/��������������������������������������������������/;

	return $word;
}

1;
