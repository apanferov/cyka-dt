#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Disp::HTTP_SMSReceiver4;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use POE qw/Component::FastCGI_m/;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use XML::Simple;
use Time::HiRes qw/gettimeofday tv_interval/;
use Crypt::OpenSSL::RSA;

use base qw 'Disp::Object';

use Disp::Utils;
use Disp::Config;
use Disp::AltDB;
use Disp::Operator;
use Disp::SMSCTransaction;

sub new {
	my ($proto,$smsc_id,$debug_level,$smpp_mode, %options) = @_;
	my $self = $proto->SUPER::new();

	$self->{routines_interval} = $options{routines_interval} || 60;

	my $smpp_mode_name = 'rcv';
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);

	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf",
				 skip_db_connect => 1);

	my $log = init_log($pr_name, $debug_level);
	$log->info("Starting...");

	my $db = new Disp::AltDB;
	my $cnf = $db->get_smsc_configuration($smsc_id);

	$self->{db}				  = $db;
	$self->{log}			  = $log;
	$self->{pr_name}		  = $pr_name;
	$self->{smpp_mode_name}	  = $smpp_mode_name;
	$self->{state}			  = 'stopped';
	$self->{status}			  = 'start';
	$self->{smsc_id}		  = $smsc_id;
	$self->{debug_level}	  = $debug_level;
	$self->{smpp_mode}		  = $smpp_mode;

	@{$self}{keys(%$cnf)} = values(%$cnf);

	if ($self->{func_module}) {
		eval "require $self->{func_module}";
		if ($@) {
			yell($@,code => 'WARNING',process => $self->{pr_name},
				 header => "$self->{pr_name}: Can't load $self->{func_module}",ignore => { log4perl => 1 });
			$self->{func_module} = undef;
		}
		else {
			$log->info("Module $self->{func_module} loaded.");
		}
	}

	$self->exec_event('init',$self);
	$self->exec_event('rcv_on_init',$self);
	$self->enter_state("initialized");

	return $self;
}

sub run {
	my $self = shift;
	my $retval = $self->run_client();
	$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
	$self->enter_state('stopped');
	$self->{log}->info("Stopped.\n\n\n");
	return $retval;
}

sub run_client {
	my ($self) = @_;
	eval {
		POE::Component::FastCGI_m->new( Port => $self->{port},
										Address => '127.0.0.1',
										RoutinesInterval => $self->{routines_interval},
										HandleRequest => sub { $self->handle_request(@_); },
										HandleRoutines => sub { $self->handle_routines(@_) },
										HandleAlarm => sub { $self->handle_alarm(@_); },
										HandleResult => sub { $self->handle_result(@_); });

		$self->enter_state("run");
		$self->{log}->info("Started.");
		POE::Kernel->run();
	};
	if ($@) {
		$self->error("$@", "Uncaught exception");
		return 1;
	}
	return 0;
}

sub handle_result {
	my ( $self, $session_id, $heap, $ret ) = @_;
	$heap->{session_id} = $session_id;
	return $self->exec_event('handle_result', $self, $heap, $ret);
}

sub handle_routines {
	my ( $self ) = @_;
	my $t0 = [gettimeofday];
	$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
	$self->{db}->heartbeat($self->{pr_name},$self->{last_received_sms_time});
	my $elapsed = tv_interval($t0, [gettimeofday]);
	$self->exec_event('rcv_on_routines',$self);
	$self->{log}->info("ROUTINES($elapsed)");
}

sub handle_request {
	my ( $self, $session_id, $heap, $request, $input ) = @_;

	$self->{log}->debug("FastCGI request is: ".Dumper($input));

	$self->{db}->reconnect(); # XXX more clever
	$heap->{session_id} = $session_id;
	$self->{log}->debug("[$session_id] REQUEST: ".Dumper(scalar $request->Vars));

	return $self->exec_event('handle_request',$self, $heap, $request);
}

sub handle_alarm {
	my ( $self, $session_id, $heap ) = @_;
	return $self->exec_event('handle_timer',$self, $heap);
}

sub init_log {
	my ( $pr_name, $debug_level ) = @_;

	my %log_conf =
	  (
	   "log4perl.rootLogger"									 => "$debug_level, Logfile",
	   "log4perl.appender.Logfile"								 => "Log::Log4perl::Appender::File",
	   "log4perl.appender.Logfile.recreate"						 => 1,
	   "log4perl.appender.Logfile.recreate_check_interval"		 => 300,
	   "log4perl.appender.Logfile.recreate_check_signal"		 => 'USR1',
	   "log4perl.appender.Logfile.umask"						 => "0000",
	   "log4perl.appender.Logfile.filename"						 => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
	   "log4perl.appender.Logfile.layout"						 => "Log::Log4perl::Layout::PatternLayout",
	   "log4perl.appender.Logfile.layout.ConversionPattern"		 => Config->param('log_pattern','sms_client')
	  );
	Log::Log4perl->init( \%log_conf);
	return get_logger("");
}

sub enter_state {
	my ($self,$state) = @_;
	if (! ($self->{state} eq $state)) {
		$self->{db}->set_process_state(uc($self->{smpp_mode_name}).'_'.$self->{smsc_id},
									   uc($state),
									   uc($self->{state}));
		$self->{state} = $state;
		$self->{log}->debug("Enter ".uc($state)." state.");
	}
	return 1;
}

sub outbox_status_changed {
	my ($self,$sms) = @_;
	if ($sms->{outbox_status}) {
		$self->{log}->info("OUTSMS:$sms->{id} MSG$sms->{outbox_id} (status=$sms->{outbox_status})");
		$self->{db}->redis_report($sms) if ($sms->{registered_delivery});
	}
}

sub error {
	my ( $self, $message, $header ) = @_;

	$self->{log}->error($message);

	yell($message,
		 code => 'ERROR',
		 process => $self->{pr_name},
		 header => "$self->{pr_name}($self->{name}): $header",
		 ignore => { log4perl => 1 });
}

sub warning {
	my ( $self, $message, $header ) = @_;

	$self->{log}->warn($message);

	yell($message,
		 code => 'WARNING',
		 process => $self->{pr_name},
		 header => "$self->{pr_name}($self->{name}): $header",
		 ignore => { log4perl => 1 });
}

sub exec_event {
	my $self = shift @_;
	my $event_name = shift @_;
	my @params = @_;
	my $retval = undef;
	eval {
		if ($self->{func_module} and $self->{func_module}->can($event_name)) {
			$self->{log}->debug("Executing event handler $self->{func_module}::$event_name");
			$retval = $self->{func_module}->$event_name(@params);
		};
	};
	if ($@) {
		$self->warning($@);
		$retval = undef;
	}
	return $retval;
}

1;
