#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::Utils;
use strict;
use warnings;
use base 'Exporter';
use Encode qw(encode decode);
use Translit qw/translit_text/;
our @EXPORT = qw(match_num_patterns check_service_patterns simple_smpp_decode check_limit_pattern);

sub match_num_patterns {
	my ($self,$operator_id,$num,$msg) = @_;
	my $key = "__patterns_num--$operator_id--$num";
	my $key_time = "$key--last_upd_time";
	unless ($self->{$key} and ($self->{$key_time}+300 > time)) {
		my $pts = $self->{db}{db}->selectall_arrayref('select service_id,pattern from set_plugin where operator_id=? and num=? order by priority desc',undef,$operator_id,$num);
		my @patterns;
		foreach (@$pts) {
			my $pattern_c;
			my ($svc_id,$pattern) = @$_;
			eval { $pattern_c = qr/$pattern/i; };
			if ($@) {
				$self->warning( "Wrong plugin pattern detected for SVC$svc_id");
			} else {
				push(@patterns,$pattern_c);
			}
		}
		$self->{$key} = \@patterns;
		$self->{$key_time} = time;
	}
	my $match = 0;
	foreach my $pattern (@{$self->{$key}}) {
		if ($msg =~ m/$pattern/gi) {
			$match = 1;
			$self->{log}->debug('One of patterns matched.');
		}
	}
	return $match;
}

sub check_service_patterns {
	my $self = shift;
	my $msg = shift;
	my @services = @_;
	unless ($self->{__patterns} and ($self->{__patterns_last_update_time}+300 > time)) {
		my @patterns;
		foreach my $id (@services) {
			my $pattern = $self->{db}->get_value('select pattern from set_service_t where id=?',undef,$id);
			my $pattern_c;
			eval { $pattern_c = qr/$pattern/i; };
			if ($@) {
				$self->warning( "Wrong plugin pattern detected for SVC$id");
			} else {
				push(@patterns,$pattern_c);
			}
		}
		$self->{__patterns} = \@patterns;
		$self->{__patterns_last_update_time} = time;
	}
	my $match = 0;
	foreach my $pattern (@{$self->{__patterns}}) {
		if ($msg =~ m/$pattern/gi) {
			$match = 1;
			$self->{log}->debug('One of patterns matched.');
		}
	}
	return $match;
}

sub simple_smpp_decode {
	my ($sms) = @_;
	return (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
}

# �������� ���-�� ��������� �� �������� ������������ ���������� �������
# �� �������� n ������
sub check_limit_pattern {
	my ($self, $msg, $abonent, $limit, $interval, $pattern) = @_;

	# whitelist "����� ����"
	if ({79252266226=>1,79857666618=>1}->{$abonent}) {
		return 0;
	}

	my $pattern_c;
	eval { $pattern_c = qr/$pattern/i; };
	if ($@) {
		$self->warning("Wrong pattern detected PATTERN = '$pattern_c'");
	}

	my $match = 0;
	if (defined($pattern_c) && $msg =~ m/$pattern_c/gi) {
		my $msg_ref = $self->{db}->fetchall_arrayref("select msg from tr_inboxa isa where date > date_add(now(), interval -$interval second) and abonent = $abonent");
		if ((scalar @$msg_ref) >= $limit) {
			my $match_cnt = 0;
			foreach (@$msg_ref) {
        @$_[0] = lc(translit_text(@$_[0]));
				$match_cnt++ if @$_[0] =~ m/$pattern_c/gi;
				if ($match_cnt >= $limit) {
					$match = 1;
					$self->{log}->debug('Reached the limit on the pattern matched.');
					last;
				}
			}
		}
	}
	return $match;
}

1;
