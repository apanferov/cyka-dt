#!/usr/bin/perl -w
package Disp::HTTP_SMSSender4;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use base 'Exporter';
use DBI;
use Data::Dumper;
#use IO::File;
use Log::Log4perl qw(get_logger);
use Log::Log4perl::Level;
#use Config::Tiny;
use LWP::UserAgent;
use HTTP::Response;
#use URI::URL;
#use Encode qw(encode decode);

use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils qw(yell);
#use Disp::UmcsSOAP;

our $VERSION	= '1.001';

sub new {
	my ($class,$smsc_id,$debug_level,$smpp_mode) = @_;

	Config::init(config_file => "../etc/asg.conf");
	my $smpp_mode_name = 'snd';

	#  Logging preparations
	my $pr_name = uc($smpp_mode_name.'_'.$smsc_id);
	my %log_conf = (
					"log4perl.rootLogger"       => "$debug_level, Logfile",
					"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
					"log4perl.appender.Logfile.recreate" => 1,
					"log4perl.appender.Logfile.recreate_check_interval" => 300,
					"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
					"log4perl.appender.Logfile.umask" => "0000",
					"log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/'.lc($pr_name).'.log',
					"log4perl.appender.Logfile.layout" => "Log::Log4perl::Layout::PatternLayout",
					"log4perl.appender.Logfile.layout.ConversionPattern" => Config->param('log_pattern','sms_client')
				   );
	Log::Log4perl->init( \%log_conf);
	my $log=get_logger("");
	$log->info("Starting...");

	#  Connecting to DB
	my $db = new Disp::AltDB();
	my $cnf = $db->get_smsc_configuration($smsc_id);
	my $self = {
				db => $db,log => $log, pr_name => $pr_name,
				smpp_mode_name => $smpp_mode_name,state=>'stopped',
				last_elink_time=>0xFFFF0000,
				wait_resp_count=>0,sleep=>0,status=>'start',
				smsc_id=>$smsc_id,debug_level=>$debug_level,smpp_mode=>$smpp_mode
			   };
	@{$self}{keys(%$cnf)} = values(%$cnf);

  	eval "require $self->{func_module}";
  	if ($@) {
		fatal_error($self,"Can't load module $self->{func_module}\n$@");
		die($@);
	}
	else {
		$log->info("Module $self->{func_module} loaded.");
	}

	bless $self, $class;
	return $self;
}

sub run {
	my $self = shift;
	my $retval = $self->run_sender();
	$self->enter_state('stopped');
	$self->{log}->info("Stopped.\n\n\n");
	return $retval;
}

sub run_sender {
	my $self = shift;

	eval {
		$self->{ua} = LWP::UserAgent->new(timeout => 30);
  		$self->{ua}->agent('A1');
		my $async = $self->{func_module}->sender_init($self);
		$self->enter_state('run');
		$self->{log}->info("Started.");

#		$self->{db}->set_outsms_all_delivered($self->{smsc_id});
		if ($async eq 'asynchttp') {
			use POE qw(Component::Client::HTTP);

			$self->{$async} = sub {
				my($esme, $sms, $req, $resp_cb) = @_;
				POE::Session->create(inline_states => {
    					_start   => sub {
						$esme->{log}->debug("SESSION:".$_[SESSION]->ID." STARTED");
						POE::Kernel->post($async, 'request', 'response', $req);
						},
    					_stop    => sub {
						$esme->{log}->debug("SESSION:".$_[SESSION]->ID." STOPPED");
						},
    					response => sub {
						$esme->{log}->debug("RESPONSE:".Dumper($_[ARG1]));
						$resp_cb->($esme, $sms, $_[ARG1]->[0]);
						}
  					});
				};
			POE::Component::Client::HTTP->spawn(
				Alias     => $async,
				Timeout   => 30);
			POE::Session->create(inline_states => {
				_start => sub {
					$_[KERNEL]->yield('pending_messages');
				},
				pending_messages => sub {
					if($self->{terminated}){
						return;
					}
		 			if ($self->can_send_msgs) {
						$self->send_messages
						? $_[KERNEL]->yield('pending_messages')
						: $_[KERNEL]->delay('pending_messages', 1);
		 			}
		 			else {
		 				$self->{db}->ping();
						$_[KERNEL]->delay('pending_messages', 1);
		 			}
					$self->heartbeat;
					$self->routines;
				}
			});
			POE::Kernel->run();
		}
		else {
			do {
		 		if ( $self->can_send_msgs() ) {
		 			sleep(1) unless ($self->send_messages());
		 		}
		 		else {
		 			$self->{db}->ping();
		 		}

				$self->heartbeat;
				$self->routines;

			} until ($self->{terminated});
		}
		$self->{log}->info('Stopping...');
	};
	if ($@) {
		$self->fatal_error($@);
		return 1;
	}
	return 0;
}

sub can_send_msgs {
	my $self = shift;
	return 1;
}

sub send_messages {
	my $self = shift;
	my ($db,$log) = ($self->{db},$self->{log});
	my $limit = $self->throttle_send_limit;
	my $msgs = $db->get_sms_ready_to_send($self->{smsc_id});
	my $msg_count = scalar keys(%$msgs);
	return 0 if (!$msg_count);
	$log->debug("$msg_count message(s) fetched. Limit=$limit");
	my $sended_count = 0;
	for (sort {$a <=> $b} keys(%$msgs)) {
		last if ($sended_count==$limit);
		my $sms = $msgs->{$_};
		if ($sms->{test_flag}==2) {
        	$db->set_sms_new_status($sms->{id},2,0,0,0);
        	$db->set_sms_delivered_status($sms->{id},1);
			$log->info("Message SMS$sms->{id} was omitted. From:$sms->{num} To:$sms->{abonent} Text:$sms->{msg}");
		}
		else {
			my $link_id = $db->get_link_id($sms->{abonent},$self->{smsc_id});
			$sms->{link_id} = $link_id if ($link_id);
			$self->{func_module}->sender_handle_sms($self, $sms);
		}
		$sended_count++;
		last if ($self->{terminated});
	}
	return $sended_count;
}

sub update_throttle {
	my ( $self, $msg_count ) = @_;

	my $time = time;
	if ($time != $self->{last_throttle_time}) {
		$self->{last_throttle_count} = 0;
		$self->{last_throttle_time} = $time;
	}

	$self->{last_throttle_count} += $msg_count
	  if $msg_count;
}

sub throttle_send_limit {
	my ( $self ) = @_;
	my ($tr, $time);
	return 1000 unless $tr = $self->{output_throttle};
	$self->update_throttle;
	my $count = $tr - $self->{last_throttle_count};
	return 0 unless $count > 0;
	return $count;
}

sub enter_state {
   my ($self,$state) = @_;
   if (! ($self->{state} eq $state)) {
		$self->{db}->set_process_state(uc($self->{smpp_mode_name}).'_'.$self->{smsc_id},uc($state),uc($self->{state}));
   	$self->{state} = $state;
   	$self->{log}->debug("Enter ".uc($state)." state.");
   }
   return 1;
}

sub heartbeat {
   my ($self) = @_;
	$self->{heartbeat} = 0 if (!$self->{heartbeat});
 	if ((time() - $self->{heartbeat}) >= 60) {
		$self->{db}->heartbeat($self->{pr_name});
   	$self->{log}->debug("HEARTBEAT.");
		$self->{heartbeat} = time();
 	}
   return 1;
}

sub routines {
   my ($self) = @_;
	$self->{routines_time} = 0 if (!$self->{routines_time});
 	if ((time() > $self->{routines_time})) {
		$self->{db}->save_statistics($self->{pr_name},$self->{smsc_id});
   	$self->{log}->debug("ROUTINES.");
		$self->{routines_time} = time()+120;
 	}
	$self->{user_routines_time} = 0 if (!$self->{user_routines_time});
 	if (time() > $self->{user_routines_time}) {
 		my $user_delta = $self->{func_module}->sender_handle_routines($self);
		$self->{user_routines_time} = time() + $user_delta;
 	}
   return 1;
}

sub outbox_status_changed {
	my ($self,$sms) = @_;
	if ($sms->{outbox_status}) {
		$self->{log}->info("OUTSMS:$sms->{id} MSG$sms->{outbox_id} (status=$sms->{outbox_status})");
		$self->{db}->redis_report($sms) if ($sms->{registered_delivery});
	}
}

sub warning {
	my ($self,$err,$err2) = @_;
	$err2 = ($err2) ? "$err\n$err2" : $err;
	$self->{log}->warn($err);
	yell($err2,code => 'WARNING',process => $self->{pr_name},
		header => "$self->{pr_name}($self->{name}): $err",ignore => { log4perl => 1 });
	return 1;
}

sub fatal_error {
	my ($self,$err2) = @_;
	my ($err) = ($err2 =~ /^(.*?)$/m);
	my ($err_type) = ($err =~ /^(\w+_ERROR\d*):/s);
	$err_type = $err_type || 'FATAL_ERROR';
	$self->{log}->fatal($err2);
	my $ignore_email = ($err_type eq 'CONNECT_ERROR');
	yell($err2,code => $err_type, process => $self->{pr_name},
		header => "$self->{pr_name}($self->{name}): $err",ignore => { log4perl => 1, 'e-mail' => $ignore_email });
	return 1;
}

sub error {
	my ( $self, $message, $header ) = @_;

	$self->{log}->error($message);

	yell($message,
		 code => 'ERROR',
		 process => $self->{pr_name},
		 header => "$self->{pr_name}($self->{name}): $header",
		 ignore => { log4perl => 1 });
}



1;
