#!/usr/bin/perl
# -*- encoding: cp1251 -*-
package Channel::Mixin_TrafficSender;
use strict;
use warnings;

use Disp::Operator;

=pod

��� �������� �������� ������ ����� ������������� ���� ���������
������ ��� �� ������, �� �������� ������ �������� ��������� -
����������� ��������� ����� SMSTraffic.

������ � �������� �������� ������ ��� ����������� ������� ���� �����,
�� ����� ����������� ������ ������� ��������� ������, �������
��������� HTTP_SMSReciever4 ��� ����� ���������.

������� �� ��� ��������, ��������� HTTP_SMSSender4, ���� ����� ������.

=cut
sub sender_init {
	my ( $proto, $esme ) = @_;
	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	$esme->{log}->info("OUTBOX$sms->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");
	$esme->{db}->resend_through_smstraffic_with_num($sms->{outbox_id}, $esme->{sms_traffic_smsc_id}, '1121');
	$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
	$esme->{db}->set_sms_delivered_status($sms->{id});
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}





1;

