package Channel::HTTP::Func_MTS_Commerce;
use strict;
use Data::Dumper;
use SOAP::Lite;
#use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);

use constant PROVIDER_ID => '60524DDB56D8FE2BA5BB92C4D5A5B72A';

sub rcv_init
{
    my($pkg, $esme) = @_;
}

sub handle_request {
    my($pkg, $esme, $heap, $query) = @_;
    my $out = SOAP::Server
        ->new
        ->dispatch_to(TwoPhaseInteraction->new($esme))
        ->handle($query->param('POSTDATA'));
            
    $esme->{log}->debug('OUT:'.$out);

    return http_response(200, $out, content_type => 'application/soap', charset => 'utf-8');
}

sub sender_init {
    my($pkg, $esme) = @_;
#    MPCEntry->proxy($esme->{url}."test");
}

sub sender_handle_routines { 0 }
        
sub sender_handle_sms {
    my($pkg, $esme, $sms) = @_;

    eval {
SOAP::Trace->import('transport', sub{$esme->{log}->debug(Dumper(shift))} );

        MPCEntry->startPayment($sms->{abonent}, PROVIDER_ID, 100, 'a1test');
    };
    if ($@) {
        $esme->{log}->error($@);
    }
}
        
1;
=pod
BEGIN {
    no strict;
    map {
        my $type = $_;
        my $method="SOAP::Serializer::as_$type";
        *$method = sub {
            my($self, $value, $name, $type, $attr) = @_;
            return [$name, {'xsi:type' => $type, %$attr}, [map { $self->encode_object($_) } @$value]];
        }
    }
    qw(RegisterRequest SendSmsRequest requestBody messages message contentList content dstMsisdnList requestHeader);
}
=cut                                    
package TwoPhaseInteraction;
use SOAP::Lite;
use base 'SOAP::Server::Parameters';

sub new {
    my($class, $esme) = @_;
    bless { esme => $esme }, $class;
}

sub checkPaymentAvail {
    my $self = shift;
    my($a) = SOAP::Server::Parameters::byName([qw(a)], pop);
    $self->{esme}{log}->debug('checkPaymentAvail' . Dumper(\@_));
    return SOAP::Data->value(1)->type('int');
}

sub registerPayment {
    my $self = shift;
    my($b) = SOAP::Server::Parameters::byName([qw(a)], pop);
    $self->{esme}{log}->debug('registerPayment' . Dumper(\@_));
    return SOAP::Data->value(1)->type('int');
}


package MCPEntry;
use strict;
use base 'SOAP::Lite';

use constant RUB  => 643;  # ISO 4217
use constant EXPO => 2;    # 1632 => 16,32 RUB


sub startPayment {
    my($self, $client, $provider, $amount, $descr) = @_;

    my $self = SOAP::Lite
        ->ns('http://mcpentry.intervale.com/xsd')
        ->on_action(sub {'urn:startPayment'})
        ->on_fault(sub{ die Dumper(\@_) });

    $self->serializer->register_ns("http://schemas.xmlsoap.org/wsdl/soap12/","soap12");
    $self->serializer->register_ns("http://www.w3.org/2006/05/addressing/wsdl","wsaw");
    $self->serializer->register_ns("http://schemas.xmlsoap.org/wsdl/mime/","mime");
    $self->serializer->register_ns("http://mcpentry.intervale.com/xsd","ns");
    $self->serializer->register_ns("http://schemas.xmlsoap.org/wsdl/soap/","soap");
    $self->serializer->register_ns("http://schemas.xmlsoap.org/wsdl/","wsdl");
    $self->serializer->register_ns("http://schemas.xmlsoap.org/wsdl/http/","http");
    $self->serializer->register_ns("http://mcpentry.intervale.com/xsd","xsd");
    
    my $payment = [
        SOAP::Data->value('amount' => $amount)->type('long'),
        SOAP::Data->value('currency' => RUB),
        SOAP::Data->value('exponent' => EXPO),
        SOAP::Data->value('description' => $descr)
    ];
    my $request = [
        SOAP::Data
            ->value( SOAP::Data->value('phone' => $client) )
            ->type('SPReq.Client'),
        SOAP::Data
            ->value( SOAP::Data->value('id' => $provider)->type('PCID') )
            ->type('SPReq.Provider'),
        SOAP::Data
            ->value( $payment )
            ->type('SPReq.Payment')
    ];
    my $result = $self->call('startPayment', 
            SOAP::Data->value(SPReq => $request)->type('SPReqMessage')->attr( version => '1.4' ));

    return $result->result;
}
