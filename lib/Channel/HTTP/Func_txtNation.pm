package Channel::HTTP::Func_txtNation;
use strict;
use Data::Dumper;
use Channel::HTTP::HTTPUtils qw(http_response);
use LWP::UserAgent;
use HTTP::Request;
use URI::URL;

use constant BILLING => 0;
use constant CHECKER => 1;
use constant CURRENCY => 2;
use constant LOGIN => 3;
use constant PASSWORD => 4;

use constant VALUE  => 0;
use constant NUMBER => 1;

my %report_status_tab = (
	'DELIVERED' => 12,
	'FAILED' => 13,
	'NO_CREDIT' => 13,
	'UNKNOWN' => 13,
	'REJECTED' => 13,
	'ACKNOWLEDGED' => 10
);

my %source = (
	#DENMARK
	'DK-1231' => ['MT','MT','DKK','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'DK-1231-sk5' => [50],
	'DK-1231-111' => [50],
	'DK-1231-222' => [50],
	'DK-1231-333' => [50],
	'DK-1231-444' => [50],
	'DK-1231-555' => [50],

	'DK-1259' => ['MT','MT','DKK','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'DK-1259-sk5' => [50],
	'DK-1259-111' => [50],
	'DK-1259-222' => [50],
	'DK-1259-333' => [50],
	'DK-1259-444' => [50],
	'DK-1259-555' => [50],


	#IRELAND
	'IE-57977' => ['MT','MT','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'IE-57977-sk5' => [2.50],
	'IE-57977-111' => [2.50],
	'IE-57977-222' => [2.50],
	'IE-57977-333' => [2.50],
	'IE-57977-444' => [2.50],
	'IE-57977-555' => [2.50],

	#NORWAY
	'NO-2226' => ['MT','MT','NOK','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'NO-2226-sk5' => [50],
	'NO-2226-111' => [50],
	'NO-2226-222' => [50],
	'NO-2226-333' => [50],
	'NO-2226-444' => [50],
	'NO-2226-555' => [50],


	#FINLAND
	'FI-17292' => ['MT','MT','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'FI-17292-sk5' => [5],
	'FI-17292-111' => [5],
	'FI-17292-222' => [5],
	'FI-17292-333' => [5],
	'FI-17292-444' => [5],
	'FI-17292-555' => [5],

	#NETHERLAND
	'NL-5040' => ['MT','MT','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'NL-5040-sk5' => [1.50],
	'NL-5040-111' => [1.50],
	'NL-5040-222' => [1.50],
	'NL-5040-333' => [1.50],
	'NL-5040-twig' => [1.50],

	#SWEDEN
	'SE-72221' => ['MT','MT','SEK','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'SE-72221-sk5' => [50],
	'SE-72221-111' => [50],
	'SE-72221-222' => [50],
	'SE-72221-333' => [50],
	'SE-72221-444' => [50],
	'SE-72221-555' => [50],

	#TURKEY
	'TR-6662' => ['MT','MT','TRY','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'TR-6662-sk5' => [10],
	'TR-6662-111' => [10],
	'TR-6662-222' => [10],
	'TR-6662-333' => [10],

	#GREECE
	'GR-54344' => ['MO','MO','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'GR-54344-sk5' => [3.62],
	'GR-54344-111' => [3.62],
	'GR-54344-222' => [3.62],
	'GR-54344-333' => [3.62],

	'GR-54345' => ['MO','MO','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'GR-54345-sk5' => [3.62],
	'GR-54345-111' => [3.62],
	'GR-54345-222' => [3.62],
	'GR-54345-333' => [3.62],

	#UNITED KINGDOM
	'UK-68899' => ['MT','MT','GBP','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'UK-68899-sk5' => [2],
	'UK-68899-111' => [2],
	'UK-68899-222' => [2],
	'UK-68899-333' => [2],

	'UK-60999' => ['MT','MT','GBP','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'UK-60999-sk5' => [5],
	'UK-60999-111' => [5],
	'UK-60999-222' => [5],
	'UK-60999-333' => [5],

	#GERMANY
	'DE-63000' => ['MT','MT','EUR','sia','2b8d3c62218d7d97bea1dca3a066fffe'],
	'DE-63000-sk52' => [1.99,'63000:199'],
	'DE-63000-sk5 2' => [1.99,'63000:199'],
	'DE-63000-sk55' => [4.99,'63000:499'],
	'DE-63000-sk5 5' => [4.99,'63000:499'],
	'DE-63000-111 rk'  => [4.99,'63000:499'],
	'DE-63000-111 7891'  => [4.99,'63000:199'],
	'DE-63000-111 789'  => [4.99,'63000:499'],
); # use http://xml.txtnation.com/xml

sub init
{}

sub handle_request
{
	my($pkg, $esme, $heap, $query) = @_;
	my($log,$db) = ($esme->{log},$esme->{db});

	$log->debug('POSTDATA:'.$query->param("POSTDATA"));
	my $action = $query->param('action');

	if ($action eq 'mpush_ir_message')
	{
		my($aid, $network, $country, $billing_type, $shortcode, $msg) = map { $query->param($_) || '' }
			qw(id network country billing shortcode message);

		if ($msg =~ /^ask\s(a1baltic|sia)\sstop/i) {
			return http_response(200, 'OK');
		}

		my $prefix = lc($msg);
		my $b = $source{"$country-$shortcode"};
		my $a;
		if ($b) {
			while (!$a and $prefix) {
				$prefix = substr($prefix,0,-1);
				$a = $source{"$country-$shortcode-$prefix"};
			}
		}

		if (!$a) {
			$esme->error("Unknown combination of $country-$shortcode-$prefix");
			return http_response(404, 'FAILED');
		}
		my $transaction_id = sprintf('%s:%s:%s:%s', $aid, $network, $country, $prefix);
		my $id;
		my $sms = {
			smsc_id        => $esme->{smsc_id},
			num            => $a->[NUMBER] || $shortcode,
			abonent        => $query->param('number'),
			msg            => $msg,
			transaction_id => $transaction_id,
			data_coding    => 0,
			esm_class      => 0,
			link_id        => $network,
			};


		$log->debug('SMS:'.Dumper($sms));

		if ($id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id})) {
			$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
			return http_response(200, 'OK');
		}

		if($billing_type ne $b->[CHECKER]) {
			$esme->error("Incorrect billing type $billing_type for $country-$shortcode");
			return http_response(500, 'FAILED');
		}

		if ($b->[BILLING] ne 'DO') {
			$id = $db->insert_sms_into_insms($sms);
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
		}
		else {
			$id = $db->insert_sms_into_insms_ignored($sms);
			$log->info("Message IGNSMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

			my $ok = submit_resp($esme, {
				reply    => 1,
				id       => $aid,
				message  => 'Thanks. The order is in the second SMS.',
				value    => $a->[VALUE],
				currency => $b->[CURRENCY],
				number   => $sms->{abonent},
				network  => $sms->{link_id}, 
			        cc       => $b->[LOGIN],
			        ekey     => $b->[PASSWORD],
			});

			if ($ok) {
				$log->info("Thanks message SMS$id was delivered to SMSC.")
			}
			else {
				$log->warn("Thanks message SMS$id wasn't delivered to SMSC.");
				return http_response(500, 'FAILED');
			}
		}
	}
	elsif ($action eq 'mp_report')
	{
		my($id, $report, $reason_id) = map { $query->param($_) } qw(id report reason_id);

		$log->info("DR($id): $report $reason_id");
		my $sms = $db->get_sms_by_msg_id($esme->{smsc_id}, $id);

		if ($sms) {
			my $status = $report_status_tab{uc($report)};
			if ($status and ($sms->{status} > $status)) {
				#$esme->warning("Incorrect new status $status ignored (was $sms->{status})");
			} elsif ($status) {
				my($aid, $network, $country, $prefix) = split(':', $sms->{transaction_id});
				my ($shortcode) = split(':', $sms->{num});
				my $b = $source{"$country-$shortcode"};

				$sms->{outbox_status} = $db->set_sms_report_status($sms->{id}, $status, undef, $sms);
				$esme->outbox_status_changed($sms);
				if (($b->[BILLING] eq 'MT') and ($status == 12) and ($sms->{tariff} !~ /^free$/)) {
					$db->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
				}
			} else {
				$esme->warning("Unknown report status $report");
			}
		} elsif ($sms = get_ignored_sms_id_by_transaction_id($esme, $id)) {
			if (lc($report) eq 'delivered') {
				# handle DO (double optin) second phase
				my $sms_id = $db->move_ignored_to_insms($sms);
				$log->info("IGNSMS$sms moved to SMS$sms_id");
			}
		}
		else {
			$log->warn("Neither OUTSMS<msgid:$id> nor IGNSMS<tid:$id> found");
			return http_response(500, 'FAILED');
		}
	}
	else {
		$esme->error("Unknown action: $action");
		return http_response(400, 'FAILED');
	}

	return http_response(200, 'OK');
}

sub sender_init
{}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	$log->debug('OUTSMS:'.Dumper($sms));

	my($aid, $network, $country, $prefix) = split(':', $sms->{transaction_id});
	my ($shortcode) = split(':', $sms->{num});
	my $b = $source{"$country-$shortcode"};
	my $a = $source{"$country-$shortcode-$prefix"};

	if (!$a) {
		$esme->error("Unknown combination of $country-$shortcode-$prefix");
		return http_response(404, 'FAILED'); #???????????????????????????????????
	}

	my %params = (
		number  => $sms->{abonent},
		network => $network,
		value   => ($sms->{tariff} !~ /^free$/) ? $a->[VALUE] : 0,
		currency => $b->[CURRENCY],
		message => $sms->{msg}
		);

	# second phase of two-phase dopt-in billing
	if    ($b->[BILLING] eq 'DO') {
		$sms->{inbox_id} = undef;
		$params{value}   = 0;
	}
	# charge only the first one
	elsif ($b->[BILLING] eq 'LC' && $sms->{sar_segment_seqnum} > 1) {
		$params{value}   = 0;
	}

	if ($sms->{inbox_id} and ($sms->{sar_segment_seqnum} == 1)) {
		$params{reply}   = 1;
		$params{id}      = $aid;
	}
	else {
		return if $b->[BILLING] eq 'IO';

		$params{reply}   = 0;
		$params{id}      = $sms->{id};
	}

	$params{cc}   = $b->[LOGIN];
	$params{ekey} = $b->[PASSWORD];

	if (submit_resp($esme, \%params)) {
		$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		$db->set_sms_new_status($sms->{id},2,0,0,$params{id},1);
		my $ignore_billing = ($b->[BILLING] eq 'MT') ? 1 : 0;
		$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},$ignore_billing,1);
	}
	else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		$db->set_sms_new_status($sms->{id},255,60,255,$params{id},1);
		$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},1);
	}
	$esme->outbox_status_changed($sms);
}

sub sender_handle_routines { 0 }


sub submit_resp
{
	my($esme, $query) = @_;

	$esme->{log}->debug('QUERY:'.Dumper($query));

	my $ua    = LWP::UserAgent->new;
	$ua->timeout(30);
	my $reply = $ua->post($esme->{url}, $query)->content();

	$esme->{log}->info("RESP:$reply");

	return ($reply eq 'SUCCESS');
}

sub get_ignored_sms_id_by_transaction_id
{
	my($esme, $transaction_id) = @_;
	my($id) = $esme->{db}->{db}->selectrow_array("select id from tr_insms_ignored where smsc_id=? and transaction_id like '$transaction_id:%' order by id desc limit 1",undef,$esme->{smsc_id});
	return $id;
}

1;
__END__
