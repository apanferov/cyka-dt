package Channel::HTTP::Func_Magti;
use strict qw(vars);
use Data::Dumper;
use Encode qw(encode decode);
use URI::URL;
use Channel::HTTP::HTTPUtils qw(http_response);
use POSIX qw(strftime);

my %num_service = (
	'8012' => '1',
	'8013' => '2',
	'8014' => '3'
	);

my %service_num = (
	'1' => '8012',
	'2' => '8013',
	'3' => '8014'
	);

my %errors = (
	0 => 'Operation successful',
	1 => 'Internal error',
	2 => 'No charge given',
	3 => 'Invalid request',
	4 => 'Invalid query',
	5 => 'Empty message',
	6 => 'Prefix error',
	7 => 'MSISDN error',
	9 => 'Negative balance'
	);
	
sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $sms = {
		smsc_id        => $esme->{smsc_id},
		num            => $service_num{$q->param('service_id')},
		abonent        => substr($q->param('from'), 1), # cut off (+)
		msg            => encode('cp1251', $q->param('text')),
		transaction_id => $q->param('message_id'),
		data_coding    => 0,
		esm_class      => 0,
		};

	if (my $id=$db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id})) {
		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	}
	elsif (0) {#($sms->{msg} =~ /^(95|86111|exe|5GAME)/i) {
		my $id = $esme->{db}->insert_sms_into_insms_ignored($sms);
		$esme->{log}->info("Message SMS$id was ignored. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	else {
		$esme->{log}->debug(Dumper($sms));
		my $id = $esme->{db}->insert_sms_into_insms($sms);
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	return http_response('200 OK', 'OK');
}


sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});

	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if $esme->{local_addr};

	my $ua = new LWP::UserAgent;
	$ua->timeout(30);

	my $url = url($esme->{url});
	my %req = (
		username   => $esme->{system_id},
		password   => $esme->{password},
		service_id => $num_service{$sms->{num}},
		message_id => $sms->{transaction_id},
		to         => "+$sms->{abonent}",
		text       => encode('utf8', $sms->{msg}),
		type       => 'text'
		);

	$url->query_form($url->query_form, %req);
	$log->debug($url);

	my $resp = $ua->get($url);

	my $reply = $resp->content();
	$log->debug("Response: $reply");

	if ($resp->code() ne '200') {
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		$log->warn("Will try to resend in 10 minutes.");
	}
	else {
		my($code) = $reply =~ /^(\d{4})/;
		
		if (defined $code && $code == 0) {
			$db->set_sms_new_status($sms->{id},2,0,0,0,1);
			$db->set_sms_delivered_status($sms->{id},0,1);
			$db->set_sms_report_status($sms->{id},2,undef,$sms);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		}
		else {
			$db->set_sms_new_status($sms->{id},255,0,$code,0,1);
			$db->set_sms_delivered_status($sms->{id},1,0);
			$db->set_sms_report_status($sms->{id},13,undef,$sms);
			$log->warn("Message SMS$sms->{id} was not accepted by SMSC. $errors{$code}");
		}
	}
	return;
}

sub sender_handle_routines { 0 }

1;
__END__
