#!/usr/bin/perl
# -*- coding: utf-8 -*-
package Channel::HTTP::Func_Webmoney;
use utf8;
use strict;
use warnings;

use Data::Dumper;
use URI;
use XML::Simple;
use POE;
use POE::Component::Client::HTTP;
use XML::Simple;
use POSIX qw/strftime/;

use WebMoney::Signer;
use WebMoney::X3;


use Channel::HTTP::HTTPUtils qw/http_response/;
use Channel::HTTP::PriceToNum qw/sum_to_num load_num_prices/;

use Disp::Utils;

use base qw/Channel::Mixin_TrafficSender/;

sub init {
	my ( $proto, $esme ) = @_;
	$esme->{log}->debug("Init");
	load_num_prices($esme);
	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};

# 	$ENV{HTTPS_KEY_FILE} = Config->param('system_root') . "/etc/wm_" . $esme->{smsc_id} . ".key";
# 	$ENV{HTTPS_CERT_FILE} = Config->param('system_root') . "/etc/wm_" . $esme->{smsc_id} . ".crt";

	POE::Component::Client::HTTP->spawn( Agent => 'A1 Transport',
										 Alias => 'ua',
										 Timeout => 30, );

	POE::Session->create( inline_states => { _start => \&fetcher_start,
											 timer => \&fetcher_timer,
											 init => \&fetcher_init,
											 http_response => \&fetcher_response,
											 shutdown => \&fetcher_shutdown },
						  heap => { esme => $esme, });

}

sub fetcher_shutdown {
	my ( $heap, $kernel, $session ) = @_[HEAP, KERNEL, SESSION];

	$heap->{esme}{log}->debug("[@{[$session->ID]}] WM fetcher session shutdown");

	$kernel->alarm_remove_all();
}

sub fetcher_init {
	my ( $kernel, $heap, $session ) = @_[KERNEL, HEAP, SESSION];

	$kernel->post('fastcgi', 'register_destructor', 'ua', 'shutdown');
	$kernel->post('fastcgi', 'register_destructor', $session, 'shutdown');

	$heap->{signer} = WebMoney::Signer->new($heap->{esme}{system_id}, $heap->{esme}{password}, Config->param('system_root')."/etc/wm_$heap->{esme}{smsc_id}.kwm");
}

sub fetcher_start {
	my ( $heap, $kernel, $session ) = @_[HEAP, KERNEL, SESSION];

	# к этому моменту компонент fastcgi еще не создан, поэтому
	# инициализацию продолжим позже, после входа в главный цикл POE
	$kernel->yield('init');

	$heap->{esme}{log}->debug("@{[$session->ID]} WM fetcher session spawned");
	$kernel->delay_set('timer', 3);

}

sub wm_request_dates {
	my ( $heap ) = @_;

	# В боевом режиме прощее идти с маленьким окном (например в 20
	# минут?) Походу вёбманям не нравятся запросы на болшие временные интервалы

	my ( $tmp ) = Config->dbh->selectrow_array("select max(datecrt) from tr_webmoney_payments where smsc_id = ?", undef,
											   $heap->{esme}{smsc_id});

#	unless ($tmp) {
		$tmp = strftime('%Y-%m-%d %H:%M:%S',localtime(time - 86400 * 2));
#	}

	return ( from_sql_date($tmp), strftime('%Y%m%d %H:%M:%S',localtime(time + 60)) );
}

sub fetcher_timer {
	my ( $heap, $kernel, $session ) = @_[HEAP, KERNEL, SESSION];

	return if $heap->{fetcher_in_progress};

	$heap->{fetcher_in_progress} = 1;

	my ( $datestart, $datefinish ) = wm_request_dates($heap);

	my $xml =  WebMoney::X3::make( wmid => $heap->{esme}{system_id},
								   purse => $heap->{esme}{url},
								   datestart => $datestart,
								   datefinish => $datefinish,
								   signer => $heap->{signer});

	$heap->{esme}{log}->debug($xml);

	my $req = HTTP::Request->new('POST', 'https://w3s.webmoney.ru/asp/XMLOperations.asp',
								 [ 'Content-Length' => length $xml ],
								 $xml);

 	$kernel->post('ua', 'request', 'http_response', $req);
	$heap->{esme}{log}->debug("@{[$session->ID]} WM request queued: @{[Dumper($req)]}");
}

sub fetcher_response {
	my ($heap, $request_packet, $response_packet, $kernel) = @_[HEAP, ARG0, ARG1, KERNEL];

	$heap->{fetcher_in_progress} = 0;

	# HTTP::Request
	my $request_object  = $request_packet->[0];

	# HTTP::Response
	my $response_object = $response_packet->[0];

	$heap->{esme}{log}->debug("Got WM response: ".$response_object->content);

	if ($response_object->is_success) {
		process_wm_operations($heap->{esme}, XMLin($response_object->content, KeepRoot => 1, KeyAttr => { operation => '+id' }, ForceArray => [ qw/operation/ ]));
	} else {
		$heap->{esme}->error("Response is @{[Dumper($response_object)]}", "Payments fetch failed");
	}

 	$kernel->delay_set('timer', 3600 );
}

sub validate_request {
	my ( $esme, $q ) = @_;

	my ( $sum, $date, $text, $abonent ) = map { scalar $q->param($_) } qw/sum date text abonent/;

	# validate request
	unless ($date =~ /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/) {
		return wm_response(error => 'Invalid date format', fatal => '1', code => 'INVALID_REQUEST');
	}

	unless ($sum > 0) {
		return wm_response(error => 'Invalid sum', fatal => '1', code => 'INVALID_REQUEST');
	}

	unless ($abonent =~ /^\d+$/) {
		return wm_response(error => 'Invalid abonent number', fatal => '1', code => 'INVALID_REQUEST');
	}

 	my $num = sum_to_num($esme, $sum);
 	unless ($num) {
 		return wm_response(error => 'Cant convert sum to short number', code => 'SUM_ERROR');
 	}

	return;
}

sub find_payments {
	my ( $smsc_id, $date, $sum ) = @_;

# 	my $payments = Config->dbh->selectall_arrayref("select * from tr_webmoney_payments where smsc_id = ? and ? between date_add(datecrt, interval datecrt_shift - datecrt_tolerance second) and date_add(datecrt, interval datecrt_shift + datecrt_tolerance second) and ? between 0.99 * (amount + comission) and 1.02 * (amount + comission)", {Slice => {}},
# 												   $smsc_id, $date, $sum);

	my $payments = Config->dbh->selectall_arrayref("select * from tr_webmoney_payments where smsc_id = ? and date_add(?, interval datecrt_shift second) between date_add(datecrt, interval  - datecrt_tolerance second) and date_add(datecrt, interval datecrt_tolerance second) and ? between 0.99 * (amount + comission) and 1.02 * (amount + comission)", {Slice => {}},
												   $smsc_id, $date, $sum);

	return unless $payments;
	return @$payments;
}

sub check_consumed_duplicates {
	my ( $list, $abonent ) = @_;
	for (@$list) {
		if ($_->{consumed} and $_->{abonent} eq $abonent) {
			return 1;
		}
	}
	return;
}

sub get_spare_payment {
	my ( $list ) = @_;

	for (@$list) {
		unless ($_->{consumed}) {
			return $_;
		}
	}

	return;
}

sub handle_routines {
}

sub make_sms {
	my ( $esme, $payment, $text, $abonent ) = @_;
	my $sms = { smsc_id => $esme->{smsc_id},
				num => sum_to_num($esme, $payment->{amount}),
				abonent => $abonent,
				msg => $text,
				data_coding => 0,
				esm_class => 0,
				transaction_id => $payment->{operation_id}
			  };

	my $insms_id = $esme->{db}->insert_sms_into_insms($sms);

	return $insms_id;
}

sub consume_payment {
	my ( $pmt, $abonent, $insms_id ) = @_;
	Config->dbh->do("update tr_webmoney_payments set abonent = ?, consumed = 1, insms_id = ? where smsc_id = ? and operation_id = ?", undef,
					$abonent, $insms_id, $pmt->{smsc_id}, $pmt->{operation_id});
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $invalid_resp = validate_request($esme, $q);
	return $invalid_resp if $invalid_resp;

	my ( $sum, $date, $text, $abonent ) = map { scalar $q->param($_) } qw/sum date text abonent/;

	my @sms = find_payments($esme->{smsc_id}, $date, $sum);

	unless (@sms) {
		# Some bruteforce check here?
		return wm_response(error => 'No payments at all, try some time later', code => 'NO_PAYMENT');
	}

	if (check_consumed_duplicates(\@sms, $abonent)) {
		return wm_response(error => 'This looks like payment dublicate, ignored', code => 'DUPLICATE_PAYMENT', fatal => '1');
	}

	my $pmt = get_spare_payment(\@sms);
	unless ($pmt) {
		return wm_response(error => 'No more spare payments', code => 'NO_PAYMENT' );
	}

	my $insms_id = make_sms($esme, $pmt, $text, $abonent);
	consume_payment($pmt, $abonent, $insms_id);

	$heap->{pmt} = $pmt;
	$heap->{insms_id} = $insms_id;

	return 5;
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;

	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id},$heap->{pmt}{operation_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		my $last_sms;
		while (my ($id,$sms) = each %$smss) {
			$esme->{log}->info("OUTBOX$sms->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");
			$esme->{db}->resend_through_smstraffic_with_num($sms->{outbox_id}, $esme->{sms_traffic_smsc_id}, '1121');
			$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
			$esme->{db}->set_sms_delivered_status($sms->{id});
			$last_sms = $sms;
		}

		return wm_response(id => $heap->{insms_id}, text => $last_sms->{msg});
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 7) {
		# return incomplete
		return wm_response(id => $heap->{insms_id});
	}
	return 5;
}

sub wm_response {
	my ( %params ) = @_;

	my $xml;

	if ($params{error}) {
		$xml = { response => { error => { content => $params{error} } } };
		$xml->{response}{error}{fatal} = $params{fatal} if $params{fatal};
		$xml->{response}{error}{code} = $params{code} if $params{code};
	} else {
		$xml = { response => { info => { id => $params{id} } } };

		if ($params{text}) {
			$xml->{response}{info}{content} = $params{text};
			$xml->{response}{info}{text} = 1;
		}
	}

	return http_response(200, XMLout($xml, KeepRoot => 1, XMLDecl => 1));
}


# Return tuples of form (comission, datecrt_shift, datecrt_tolerance)
my @payment_recognizers
  = ( sub {
		  my ( $oper ) = @_;
		  if ($oper->{desc} =~ /В счет оплаты покупки ценных бумаг через ЗАО ОСМП \(osmp\.ru\)\. Удержана комиссия ([0-9.]+) р\./) {
			  return ( $1, 86400 + 1800, 3600 );
		  } else {
			  return;
		  }
	  },
	  sub {
		  my ( $oper ) = @_;
		  if ($oper->{desc} =~ /В счет оплаты покупки ценных бумаг через ЗАО "е-порт" \(e-port\.ru\)\. Удержана комиссия ([0-9.]+) р\./) {
			  return ( $1, 61, 120);
		  } else {
			  return;
		  }
	  },
	  sub {
		  my ( $oper ) = @_;
		  if ($oper->{desc} =~ /В счет оплаты покупки ценных бумаг через  ООО "Свободная? касса" \(freecash\.ru\)\. Удержана комиссия ([0-9.]+) р\./) {
			  return ( $1, 62, 120 );
		  } else {
			  return;
		  }
	  },
	  sub {
		  my ( $oper ) = @_;
		  if ($oper->{desc} =~ /В счет оплаты покупки ценных бумаг через терминал Элекснет\. Удержана комиссия ([0-9.]+) р\./) {
			  return ( $1, 63, 120 );
		  } else {
			  return;
		  }
	  },
	);

sub process_wm_operations {
	my ( $esme, $resp ) = @_;

	my $parsed = $resp;

	$esme->{log}->debug("Parsed resp is: @{[Dumper($resp)]}");

	if ($parsed->{'w3s.response'}{retval} == 0
		and $parsed->{'w3s.response'}{operations}{cnt} > 0) {

		while (my($id, $oper) = each %{$parsed->{'w3s.response'}{operations}{operation}}) {

			my ($exists) = Config->dbh->selectrow_array("select amount from tr_webmoney_payments where smsc_id = ? and operation_id = ?", undef,
														$esme->{smsc_id}, $id);
			if ($exists) {
				unless ($oper->{amount} == $exists) {
					$esme->error("Payment is @{[Dumper($oper)]}, but saved price is $exists", "WM payment info mismatch");
				}
			} else {

				my ( $comission, $datecrt_shift, $datecrt_tolerance );

				for (@payment_recognizers) {
					( $comission, $datecrt_shift, $datecrt_tolerance ) = $_->($oper);
					last if defined $comission;
				}

				$comission ||= 0;
				$datecrt_shift ||= 60;
				$datecrt_tolerance ||= 120;

				Config->dbh->do("insert into tr_webmoney_payments set smsc_id = ?, operation_id = ?, amount = ?, datecrt = ?, comission = ?, datecrt_shift = ?, datecrt_tolerance = ?",undef,
								$esme->{smsc_id}, $id, $oper->{amount}, sql_date($oper->{datecrt}),
								$comission, $datecrt_shift, $datecrt_tolerance);
			}
		}
	} else {
		$esme->error("Parsed resp is: @{[Dumper($resp)]} ", "WebMoney payments fetch failed");
	}
}

sub sql_date {
	my ( $date ) = @_;
	if ($date =~ /^(\d{4})(\d{2})(\d{2}) (\d{2}:\d{2}:\d{2})/) {
		return "$1-$2-$3 $4";
	} else {
		return;
	}
}

sub from_sql_date {
	my ( $date ) = @_;
	$date =~ s/^(\d{4})-(\d{2})-(\d{2}) (\d{2}:\d{2}:\d{2})$/$1$2$3 $4/;
	$date;
}


1;

__END__

CREATE TABLE tr_webmoney_payments(
 smsc_id INTEGER NOT NULL,
 operation_id BIGINT NOT NULL,
 amount DECIMAL(10,4),
 comission DECIMAL(10, 4) not null default 0,
 datecrt DATETIME NOT NULL,
 datecrt_tolerance integer not null default 0,
 datecrt_shift integer not null default 0,
 consumed tinyint not null default 0,
 abonent varchar(20),
 insms_id INTEGER,
 PRIMARY KEY(smsc_id, operation_id),
 INDEX ( datecrt, amount ) );

CREATE TABLE tr_webmoney_payments(
 smsc_id INTEGER NOT NULL,
 operation_id INTEGER(20) NOT NULL,
 ts_id INTEGER(20),
 tranid INTEGER(20),
 pursesrc VARCHAR(20),
 pursedst VARCHAR(20),
 amount DECIMAL(10,4),
 comiss DECIMAL(10,6),
 opertype INTEGER,
 wminvid INTEGER,
 orderid INTEGER,
 desc VARCHAR(255),
 datecrt DATETIME NOT NULL,
 dateupd DATETIME,
 corrwm INTEGER(20),
 rest DECIMAL(12,4),

 consumed tinyint not null default 0,
 abonent varchar(20),
 text varchar(255)


 PRIMARY KEY(smsc_id, operation_id),

)



http://gateasg.alt1.ru/sms_wmr.exe?sum=10&date=2008-08-22+17%3A20%3A55&abonent=79264242051&text=test&user_agent=Mozilla%2F4.0+%28compatible%3B+MSIE+7.0%3B+Windows+NT+6.0%3B+WebMoney+Advisor%3B+SLCC1%3B+.NET+CLR+2.0.50727%3B+Media+Center+PC+5.0%3B+.NET+CLR+3.0.04506%29&remote_addr=217.21.46.167
