package Channel::HTTP::Func_MobileTrend;
use strict qw(vars);
use Data::Dumper;
use XML::Simple;
use Encode qw(encode decode);
use Channel::HTTP::HTTPUtils qw(http_response);
use POSIX qw(strftime);
use URI::URL;

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
	
	my $xml = $q->param('xmldata');
	if ($xml) {
		$log->debug($xml);

		my $ref = XMLin($xml, ForceContent => 0);
		$log->debug(Dumper($ref));
		my $sms = {
			smsc_id        => $esme->{smsc_id},
			num            => $ref->{mo}{shortcode},
			abonent        => $ref->{mo}{number},
			msg            => encode('cp1251',$ref->{mo}{content}),
			data_coding    => 0,
			esm_class      => 0,
			link_id        => "$ref->{mo}{mcc}$ref->{mo}{mnc}",
			transaction_id => "$ref->{mo}{msgid}-$ref->{mo}{number}",
		};
		$log->debug(Dumper($sms));

		my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
		if ($id) {
			$log->warn("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
		}
		elsif (0) {
			$log->warn("SMS $sms->{transaction_id} was fully ignored.");
		}
		else {
			$id = $db->insert_sms_into_insms($sms);
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
    		}
		# Synchronous
		if ( {730=>1,216=>1,255=>1}->{$ref->{mo}{mcc}}
			or (($ref->{mo}{mcc} eq '260') and ($ref->{mo}{shortcode} eq '92555')) ) {
				$heap->{transaction_id} = $sms->{transaction_id};
				$heap->{try} = 0;
				return 5; # First try in 5 seconds
		}
    	}
	elsif (my $msg_id = $q->param('msgid'))
	{
		my $msg_state = $q->param('status');
		my $s = $db->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
		if ($s and $s->{id}) {
			$log->debug("Delivery report ($msg_state) for SMS$s->{id}.");
			if ($msg_state =~ /(delivered|handset)/i) {
				$db->set_sms_report_status($s->{id},12,undef,$s);
				$db->update_billing($s->{inbox_id},$s->{outbox_id},1);
				$log->info("OUTSMS:$s->{id} DR DELIVRD (status=$msg_state)");
			} elsif ($msg_state =~ /(acked|smsc|buffered)/i) {
			} else {
				$db->set_sms_report_status($s->{id},13,undef,$s);
				$esme->warning($q->param('status'),'Unknown status');
				$log->info("OUTSMS:$s->{id} DR REJECTED (status=$msg_state)");
			}
		}
		else {
			$log->warn("OUTSMS: DR message not found message_id=$msg_id");
		}
	}
	my $resp = http_response('200 OK', qq(<?xml version="1.0" encoding="UTF-8"?>\n<return>\n<status>0</status>\n<error/>\n</return>\n));
	$log->debug("Response: 0");
	return $resp;
}

sub handle_timer
{
	my($pkg, $esme, $heap) = @_;

	$heap->{try}++;
	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $msgs      = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id}, $heap->{transaction_id});
	my $msg_count = scalar keys(%$msgs);
	$esme->{log}->debug("$msg_count message(s) fetched.");

	my $rv = 10; # Retry in 10 seconds
	if ($msg_count > 0) {
		my(undef, $sms)    = each %$msgs;
		$heap->{outsms_id} = $sms->{id};
		my $output = encode('utf-8', decode('cp1251',
			XMLout({status => '0', error => '', message => $sms->{msg} },
			XMLDecl => '<?xml version="1.0" encoding="utf-8"?>',
			RootName => 'return',
			NoAttr => 1)
		));

		$esme->{log}->debug("[$heap->{session_id}]\n$output");
		$rv = http_response(200, $output, content_type => 'text/xml', charset => 'utf-8');
		$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
		$esme->{db}->set_sms_delivered_status($sms->{id});
		$rv = http_response(200, $output, content_type => 'text/xml', charset => 'utf-8');
	} elsif ($heap->{try} >= 6) { # 10*5+5= aproximently 1 minute
		$esme->{log}->debug("[$heap->{session_id}] Timed out...");
		my $output = encode('utf-8', decode('cp1251',
			XMLout({status => '1', error => 'Service unavailable', message => '' },
			XMLDecl => '<?xml version="1.0" encoding="utf-8"?>',
			RootName => 'return',
			NoAttr => 1)
		));
		$esme->{log}->debug("[$heap->{session_id}]\n$output");
		$rv = http_response(200, $output, content_type => 'text/xml', charset => 'utf-8');
	}
	return $rv;
}

sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my ($mcc,$mnc) = $sms->{link_id} =~ /^(\d\d\d)(\d\d)$/;
	my ($tr_id,$msisdn) = split(/-/,$sms->{transaction_id});
	my $struct = {
		login  => $esme->{system_id},
		password => $esme->{password},
		mt     => {
			msgid => $tr_id,
			contenttype => 'text',
			content => decode('cp1251',$sms->{msg}),
			number => $msisdn,
			shortcode => $sms->{num},
			mcc => $mcc,
			mnc => $mnc
		}
	};
	my $xml = XMLout($struct, NoAttr => 1, RootName => 'sms', XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>');
	$log->debug("XML $xml");
	$xml = encode('UTF-8',$xml);

	my $ua = new LWP::UserAgent;
	$ua->timeout(20);
	my $resp = $ua->post($esme->{url}, {xmldata => $xml});

	my $reply = $resp->content();
	$log->debug($reply); 

	unless ($resp->code() eq '200') {
	        $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
			. "Will try to resend in 10 minutes.\n"
			. $resp->as_string());
	 	$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	} else {
		my $ref = XMLin($reply, ForceContent => 0);
  		my $status = $ref->{status};
		if ( $ref->{status} == 0) {
			$db->set_sms_new_status($sms->{id}, 2, 0, 0, $ref->{msgid}, 1);
			$db->set_sms_delivered_status($sms->{id}, 1);
         		$log->info("Message SMS$sms->{id} was delivered to SMSC.");
        	} else {
			$db->set_sms_new_status($sms->{id}, 255, 0, 255, 0, 1);
			$db->set_sms_delivered_status($sms->{id}, 1);
			$log->warn("Message SMS$sms->{id} wasn't accepted by SMSC.\n" . $resp->as_string());
         	}
	} #HTTP 200 OK
}

sub sender_handle_routines { 0 }

1;
__END__
