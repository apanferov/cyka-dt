#!/usr/bin/perl
package Channel::HTTP::Func_Comepay;
use strict;
use warnings;

use Channel::HTTP::PriceToNum qw/load_num_prices sum_to_num/;
use Channel::HTTP::HTTPUtils qw/http_response/;
use Disp::Utils;

use Digest::MD5;
use XML::Simple;
use Data::Dumper;

use base qw/Channel::Mixin_TrafficSender/;

sub init {
	my ( $proto, $esme ) = @_;

	$esme->{log}->debug("Init");
	load_num_prices($esme);
}

sub action_check {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $account = $q->param('account');
	my $sum = $q->param('sum');

	my $num = sum_to_num($esme, $sum);

	if ( $num and $account =~ /^7\d{10}\/.+$/) {
		return comepay_response( $esme, result => 0, operation => 'check', account => $account, sum => $sum);
	} else {
		return comepay_response( $esme, result => 500, fatal => 'true',
								 operation => 'check',
								 account => $account,
								 sum => $sum);
	}
}

sub action_check_noprice {
	my ( $proto, $esme, $heap, $q ) = @_;
	my $account = $q->param('account');
	if ( $account =~ /^7\d{10}\/.+$/) {
		return comepay_response( $esme, result => 0, operation => 'check', account => $account);
	} else {
		return comepay_response( $esme, result => 500, fatal => 'true', operation => 'check', account => $account);
	}
}

sub action_payment {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $id_payment = $q->param('id_payment');
	my $account = $q->param('account');
	my $sum = $q->param('sum');
	my $date = $q->param('date');

	my $num = sum_to_num($esme, $sum);

	if ( $num and $account =~ /^(7\d{10})\/(.+)$/) {
		my $abonent = $1;
		my $msg = $2;

		my $id = $esme->{db}->get_insms_id_by_transaction_id($esme->{smsc_id}, $id_payment);

		if ($id) {
			$esme->warning("Duplicate payment $id_payment ( SMS$id )\n@{[Dumper(scalar $q->Vars)]}", "Duplicate payment");
			return comepay_response( $esme,
									 result => 516, fatal => 'true',
									 operation => 'payment',
									 id_payment => $id_payment,
									 'ext-id_payment' => $id,
									 date => $date,
									 account => $account,
									 sum => $sum,
								   );
		}

		my $sms = { smsc_id => $esme->{smsc_id},
					num => $num,
					abonent => $abonent,
					msg => $msg,
					data_coding => 0,
					esm_class => 0,
					transaction_id => $id_payment };

		my $insms_id = $esme->{db}->insert_sms_into_insms($sms);
		return comepay_response( $esme,
								 result => 0,
								 operation => 'payment',
								 id_payment => $id_payment,
								 'ext-id_payment' => $insms_id,
								 date => $date, 
								 account => $account,
								 sum => $sum,
							   );
	} else {
		return comepay_response( $esme, result => 500, fatal => 'true',
								 operation => 'payment',
								 account => $account,
								 sum => $sum);
	}
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	if ($q->param('operation') eq 'check') {
		if ($q->param('sum')) {
			return action_check(@_);
		} else {
			return action_check_noprice(@_);
		}
	}

	if ($q->param('operation') eq 'payment') {
		return action_payment(@_);
	}

	$esme->error("Unrecognized request: @{[Dumper(scalar $q->Vars)]}", "Unrecognized request");
	return http_response(200, "Unrecognized");
}

sub comepay_response {
	my ( $esme, %params ) = @_;

	my $fatal = delete $params{fatal};
	while (my($k,$v) = each %params) {
		$params{$k} = { content => $v };
		$params{$k}{fatal} = 'true' if $k eq 'result' and $fatal;
	}

	my $xml = XMLout( {response => \%params}, KeepRoot => 1, XMLDecl => 1);

	$esme->{log}->debug("XML response is: $xml");

	return http_response(200, $xml);
}

1;
