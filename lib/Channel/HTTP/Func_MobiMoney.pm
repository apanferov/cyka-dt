package Channel::HTTP::Func_MobiMoney;
use strict;
use Data::Dumper;
use Channel::HTTP::HTTPUtils qw(http_response);

sub rcv_init
{}

sub handle_request
{
    my($pkg, $esme, $heap, $query) = @_;

    my $in  = $query->param('POSTDATA');
    my $out = PaycashShopService
        ->new($esme)
        ->handle($in);

    return http_response(200, $out,
        content_type => 'text/xml', charset => 'utf-8' );
}


sub sender_init
{
    my($pkg, $esme) = @_;
    ShopService::init($esme->{url}, $esme->{log});
}

sub sender_handle_routines { 0 }

sub sender_handle_sms
{
    my($pkg, $esme, $sms) = @_;
    my($db, $log) = ($esme->{db}, $esme->{log});

    $log->debug("OUTSMS:".Dumper($sms));
    eval {
        my($num, $price) = split '-', $sms->{num};
        die "Bad price: $price. Limit [1-500]RUR" if $price < 1*100 || $price > 500*100;

        my($pay_id, $pay_tm, $pay_res, $pay_str) =
            ShopService::InitPayment($sms->{abonent}, 'tboxer', $num);

	die { code => $pay_res, msg => $pay_str } if $pay_res > 1;

        $db->set_sms_new_status($sms->{id},2,0,0,$pay_id);
	$db->set_sms_delivered_status($sms->{id},1);
	$log->info("SMS$sms->{id} was delivered with PaymentID=$pay_id");
    };
    if ($@) {
	$esme->warning(ref $@ ? Dumper($@) : $@);

	my $err_num = ref $@ eq 'HASH' ? $@->{code} : 0;

        $db->set_sms_new_status($sms->{id},255,0, $err_num);
	$db->set_sms_delivered_status($sms->{id},1);
	$log->warn("SMS$sms->{id} wasn't delivered");
    }
}



BEGIN {

    package ShopService;
    use strict;
    use Data::Dumper;
    use SOAP::Lite;

    $ENV{HTTPS_PROXY} = '172.31.170.36:8080';
    $ENV{HTTPS_PKCS12_FILE} = '/gate/asg5/etc/twinsbox_mobi.p12';

    my $shop;

    sub init
    {
        my($endpoint, $log) = @_;
#       SOAP::Trace->import('transport', sub{$log->debug(Dumper(shift))} ) if $log;

        $shop = SOAP::Lite
            ->new
            ->proxy($endpoint)
            ->default_ns('https://merchant.mobi-money.ru/soap/ShopService/')
            ->on_action(sub {join '',@_})
            ->on_fault(sub { die ref $_[1] ? $_[1]
                : SOAP::Fault->faultcode('transport')->faultstring($_[0]->transport->message)});
    }

    sub InitPayment
    {
        my($phone, $sms, $service_num) = @_;

        $service_num ||= 0;

        my $result = $shop
            ->call(InitPayment =>
		SOAP::Data
                    ->name('Phone')
                    ->value($phone)->type('string'),
		SOAP::Data
                    ->name('SMS')
                    ->value($sms)->type('string'),
		SOAP::Data
                    ->name('ServiceNum')
                    ->value($service_num)->type('int')
		);

        SOAP::Server::Parameters::byName(
            [qw(PaymentID PaymentTime PaymentResult PaymentResultStr)], $result);
    }

} #BEGIN

BEGIN {

    package PaycashShopService;
    use strict;
    use Data::Dumper;
    use SOAP::Lite;
    use URI;
    use URI::QueryParam;
    use Encode qw(encode decode);
    use XML::Simple;
    use base qw(SOAP::Server::Parameters);

    sub new
    {
        my($class, $esme) = @_;
    SOAP::Trace->import('transport', sub{$esme->{log}->debug(Dumper(shift))} );
        bless { esme => $esme }, ref $class || $class;
    }

    sub handle
    {
        my($self, $in) = @_;

	my $out = SOAP::Server
            ->new
	    ->dispatch_to($self)
	    ->handle($in);

        $self->{esme}{log}->debug('OUT:'.$out);

        return $out;
    }

    sub PaymentContract
    {
	my $self = shift;
        my($pay_id, $pay_tm, $user_params) =
	  SOAP::Server::Parameters::byName([qw(PaymentID PaymentTime UserParams)], pop);

	my $sms = $self->{esme}{db}->get_sms_by_msg_id($self->{esme}{smsc_id}, $pay_id);
	if (!$sms) {
		die "PaymentID=$pay_id not found";
	}
        my($num, $price) = split '-', $sms->{num};
        die "Bad price: $price. Limit [1-500]RUR" if $price < 1*100 || $price > 500*100;

        my $contract =
            '<?xml version="1.0" encoding="utf-8"?>'.
            '<contract>'.
            '<param id="orderID" label="Номер заказа">'.$sms->{id}.'</param>'.
            '</contract>'; 

	return
            SOAP::Data
                ->name('Sum') # limit: min=1RUR, max=500RUR
                ->value($price/100)->type('float'),
	    SOAP::Data
                ->name('PayeeRegData')
                ->value($user_params)->type('string'),
	    SOAP::Data
                ->name('Contract')
                ->value($contract)->type('string');
    }

  sub PaymentAuthorization
  {
	my $self = shift;
	my($pay_id, $pay_tm, $sum, $auth_tm, $payee, $is_repeat) =
	  SOAP::Server::Parameters::byName(
            [qw(PaymentID PaymentTime Sum AuthorizationTime PayeeRegData IsRepeat)], pop);

	my $sms = $self->{esme}{db}->get_sms_by_msg_id($self->{esme}{smsc_id}, $pay_id);
	if (!$sms) {
		die "PaymentID=$pay_id not found";
	}

    my $reply = #encode('utf-8', decode('cp1251',
		XMLout({
                    param => [
                        {id=>"orderID", ref=>"orderID", label=>"", content=>$sms->{id}},
                        {id=>"comment", label=>"", content=> decode('utf8',  encode('utf8', decode('cp1251', $sms->{msg})) )}
                        ]
                    },
                    RootName => 'success', XMLDecl => '<?xml version="1.0" encoding="utf-8"?>');
		#)
                #);
$self->{esme}{log}->debug("REPLY".Dumper($reply));
#        my $reply =
#            '<?xml version="1.0" encoding="utf-8"?>'.
#            '<success>'.
#            '<param id="orderID" ref="orderID" label="Номер заказа">'.$user->{orderID}.'</param>'.
#            '<param id="comment" label="Описание заказа">'.encode('utf8', decode('cp1251', $sms->{msg})).'</param>'.
#            '</success>'; 

        #XXX
	$self->{esme}{db}->set_sms_report_status($sms->{id},12,0,$sms);
        $self->{esme}{db}->update_billing($sms->{inbox_id}, $sms->{outbox_id}) if !$is_repeat;

        return
	  SOAP::Data
		->name('ReplyResource')
		->value($reply)->type('string'),
	  SOAP::Data
		->name('ReplyResourceIsFailure')
		->value(0)->type('boolean'),
  }

    sub PaymentCancellation
    {
	my $self = shift;
	my($pay_id, $pay_tm, $pay_res, $pay_str, $payee) =
	  SOAP::Server::Parameters::byName(
            [qw(PaymentID PaymentTime PaymentResult PaymentResultStr PayeeRegData)], pop);

	my $sms = $self->{esme}{db}->get_sms_by_msg_id($self->{esme}{smsc_id}, $pay_id);
	if (!$sms) {
		die "PaymentID=$pay_id not found";
	}

        #XXX
	$self->{esme}{db}->set_sms_report_status($sms->{id},13,0,$sms);
        return;
    }

} #BEGIN
1;
__END__