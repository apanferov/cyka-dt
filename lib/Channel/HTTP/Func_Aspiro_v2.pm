#!/usr/bin/perl
package Channel::HTTP::Func_Aspiro_v2;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use URI::URL;
use LWP::UserAgent;
use HTTP::Request;

use Channel::HTTP::HTTPUtils qw/http_response/;

my $status_tab = {
				  '202' => 11,
				  '203' => 12,
				  '204' => 12,
				  '901' => 13,
				  '902' => 14,
				  '903' => 13,
				  '904' => 15,
				  '905' => 15,
				  '906' => 15,
				  '909' => 13,
				  '910' => 13,
				  '912' => 13,
				  '913' => 13,
				  '914' => 13,
				  '916' => 13,
				  '917' => 13,
				  '918' => 13,
				  '951' => 13,
				  '998' => 13,
				  '999' => 13
				 };
my $gate_tab = { EE => {login => '#y_CmS2Y-', password => 'Ej*P7HKH-'},
		 LV => {login => 'wygQnZHO', password => 'uzZO1XZm'},
		 LT => {login => 'K6CQwLsSLhT', password => 'hUt1R2SkGBD'},
		};

sub rcv_on_init {
	$XML::Simple::PREFERRED_PARSER = 'XML::LibXML::SAX';
}

sub handle_request {
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $xml = $q->param('POSTDATA');
	$log->debug($xml);

	my $ref = XMLin($xml, KeepRoot => 1);
	$log->debug(Dumper($ref));

	if ($ref->{gate}) {
		my $sms = {
			   smsc_id        => $esme->{smsc_id},
			   num            => $ref->{gate}{targetNumber},
			   abonent        => $ref->{gate}{senderNumber},
			   msg            => $ref->{gate}{sms}{content},
			   data_coding    => 0,
			   esm_class      => 0,
			   transaction_id => "$ref->{gate}{country}-$ref->{gate}{operator}-$ref->{gate}{id}-$ref->{gate}{referenceId}",
			   link_id        => "$ref->{gate}{country}-$ref->{gate}{operator}",
		};
		$log->debug(Dumper($sms));

		my $id = $db->insert_sms_into_insms($sms);
		$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	} elsif ($ref->{gatedr}) {
		$heap->{ref} = $ref;

		return {
			timer => 5,
			content => http_response('200 OK', ''),
		};
	}

	my $resp = http_response('200 OK');
	$log->debug("Response: 200 OK");
	return $resp;
}


sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_sms {
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my ($country,$operator,$tr_id,$ref_id) = split(/-/,$sms->{transaction_id});
	my $struct = {
				  country    => $country,
				  accessNumber => $sms->{num},
				  senderNumber => $sms->{num},
				  targetNumber => $sms->{abonent},
				  operator => $operator,
				  price => 0,
				  sms   => {
							content => decode('cp1251',$sms->{msg})
						   }
				 };
	$struct->{referenceId} = $ref_id
	  if $ref_id;

	if ($country eq 'LV') {
		if ($sms->{num} eq "1863") {
			if ($sms->{tariff} !~ /^free$/) {
				$struct->{price} = 300;
				$struct->{sms}{content} = decode('cp1251',"$sms->{msg} Samaksats LVL 3.00/ EUR 4.27");
			}
		} else {
			$esme->warning("Unknown tariff for MT $country $sms->{num}",Dumper($sms));
		}
		if ($operator eq 'LM') {
			$struct->{customProperties}{customProperty}{key} = "lmt_service_desc";
			$struct->{customProperties}{customProperty}{value} = $sms->{params}{'p.service_id'} || "";
		}
		if ($operator eq 'T2') {
			$struct->{invoicetext} = $sms->{params}{'p.invoicetext'} if ($sms->{params}{'p.invoicetext'});
		}
	} elsif ($country eq 'LT') {
		if ($operator eq 'T2') {
			$struct->{invoicetext} = $sms->{params}{'p.invoicetext'} if ($sms->{params}{'p.invoicetext'});
		}
	}

#    my $xml = encode('UTF-8',
#		XMLout($struct, RootName => 'gate', XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>')
#		);
    my $xml = encode('UTF-8',XMLout($struct, RootName => 'gate', NoAttr => 1, KeyAttr => [],XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>'));

    my $header = HTTP::Headers->new;
  	$header->header('content-type' => 'text/xml');
  	$header->authorization_basic($gate_tab->{$country}{login}, $gate_tab->{$country}{password});

  	my $req = HTTP::Request->new('POST', $esme->{url}, $header, $xml);
  	$log->debug($req->as_string());

    my $resp  = $esme->{ua}->request($req);
	my $reply = $resp->content();
	$log->debug($reply);

	if ($resp->code() eq '200') {
		my $ref = XMLin($reply, KeepRoot => 1);
		if ($ref->{gatedr}{status} eq '201') {
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
			$db->set_sms_new_status($sms->{id}, 2, 0, 0, $ref->{gatedr}{id}, 1);
		}
		else {
			$log->warn("Message SMS$sms->{id} rejected ($ref->{gatedr}{status}). \n");
			$db->set_sms_new_status($sms->{id}, 255, 0, $ref->{gatedr}{status}, 0, 1);

		}
		my $ignore_billing = ($country eq 'LV') ? 1 : 0;
		$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},$ignore_billing);
		$db->redis_report($sms, {}) if ($sms->{registered_delivery});
	}
	else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
				   . "Will try to resend in 10 minutes.\n"
				   . $resp->as_string());
		$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	}
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}



#sub rcv_on_init {
#	my ( $proto, $esme ) = @_;
#}

sub rcv_on_routines {
	my ( $proto, $esme ) = @_;
}


sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
#	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	$heap->{try}++;
	my $ret = { timer => 5 }; # Retry in 5 seconds
	my $ref = $heap->{ref};

	my $msg_id         = $ref->{gatedr}{id};
	my $status         = $status_tab->{ $ref->{gatedr}{status} };
	my $statusstr      = $ref->{statusMessage};

#	sleep(5); # DR might come before MT saved in outsmsa
	my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
	if ($outsms) {
		if ($status) {
			$log->info("Report for message SMS$outsms->{id} was received. Status: $ref->{gatedr}{status}."
				. ($statusstr and " Reason: $statusstr"));
			my ($country) = split(/-/,$outsms->{transaction_id});
			$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id},1)
				unless (($outsms->{tariff} =~ /^free$/) or ($country ne 'LV') or ($status != 12));
			$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id},$status,undef,$outsms);
			$db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
		} else {
			$esme->warning("Unknown report $status\n".Dumper($ref));
		}
		$ret = {};
	}	elsif ($heap->{try} >= 5) { # 5*5+5=30 seconds
		$log->debug("[$heap->{session_id}] Timed out...");
		$ret = {};
	}
 	return $ret;
}

1;
