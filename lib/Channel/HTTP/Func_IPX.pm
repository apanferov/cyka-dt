#!/usr/bin/perl
package Channel::HTTP::Func_IPX;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use LWP::UserAgent;
use HTTP::Request;
use Channel::HTTP::HTTPUtils qw/http_response/;

my %tariff_tab = (# operator => num => tariff
		301 => {free => 'EUR0', '81020' => 'EUR300', '82030' => 'EUR450', '73973' => 'EUR50' },
		302 => {free => 'EUR0', '81020' => 'EUR300', '82030' => 'EUR450', '73973' => 'EUR50' },
		303 => {free => 'EUR0', '81020' => 'EUR300', '82030' => 'EUR450', '73973' => 'EUR50' },
		613 => {free => 'EUR0', '81020' => 'EUR300', '82030' => 'EUR450', '73973' => 'EUR50' },
);

sub xml_escape {
	my ( $data ) = @_;
	$data =~ s/&/&amp;/sg;
	$data =~ s/</&lt;/sg;
	$data =~ s/>/&gt;/sg;
	$data =~ s/"/&quot;/sg;
	return $data;
}

sub init {
	my ( $proto, $esme ) = @_;
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;
	
	if (defined ($q->param('StatusCode'))) {
		handle_new_dr($esme, $heap, $q);
	} else {
		handle_new_sms($esme, $heap, $q);
	}
	return http_response('200 OK','',content_type => 'text/plain',charset => 'utf-8');
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	my $req = sender_prepare_request($proto, $esme, $sms);
	
	$esme->{log}->debug("Request is:\n".$req->as_string());

	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $resp = $ua->request($req);

	return sender_handle_response($proto, $esme, $sms, $resp);
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}

###########
# INTERNAL
###########
sub handle_new_sms {
	my ( $esme, $heap, $q, $parsed ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $transaction_id	= $q->param('MessageId')."/".$q->param('OriginatorAddress');
	my $short_num		= $q->param('DestinationAddress');
	my $abonent		= $q->param('OriginatorAddress');
	if ($abonent =~ /^#(.*)$/) {
		$abonent = hex(substr($1,-15));
	}
	my $text		= $q->param('Message');
	my $operator		= $q->param('Operator');


	my $sms = { smsc_id => $esme->{smsc_id},
				transaction_id => $transaction_id,
				num => $short_num,
				abonent => $abonent,
				msg => encode('cp1251',decode('UTF-8',$text)),
				data_coding => 0,
				esm_class => 0,
				link_id => $operator,
			  };

	my $id = $db->insert_sms_into_insms($sms);
	$log->debug(Dumper($sms));
	$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
}

sub handle_new_dr {
	my ( $esme, $heap, $q, $parsed ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $msg_id	   = $q->param('MessageId');
	my $statuscode     = $q->param('StatusCode');
	my $reasoncode     = $q->param('ReasonCode');
	
#	if ($ddate !~ /^(\d{4})(\d{2})(\d{2})T(\d{2}):(\d{2}):(\d{2})$/) {
#		$esme->error("Invalid ddate : '$ddate'", "Invalid ddate");
#		return comgate_response(500, "Invalid ddate");
#	}
#	$ddate = "$1-$2-$3 $4:$5:$6";

	my $sms = $esme->{db}->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
	if ($sms) {
		$sms->{outbox_status} = $db->set_sms_report_status($sms->{id},($statuscode == 0 ? 12 : 15),undef,$sms);
		$esme->outbox_status_changed($sms);
		if (($statuscode == 0) and ($sms->{tariff} !~ /^free$/)) {
			$db->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
		}
		$log->info("OUTSMS:$sms->{id} delivery report handled : status $statuscode");
	} else {
		$log->warn("OUTSMS: DR message not found message_id=$msg_id");
	}
}


sub sender_prepare_request {
	my ( $proto, $esme, $sms ) = @_;

	my $req = HTTP::Request->new(POST => $esme->{url});
	my $msg = ($sms->{data_coding} == 0)
		? $sms->{msg}
		: encode('utf8',decode('UCS-2',$sms->{msg}));
	$msg = xml_escape($msg);
	my $tariff = ($sms->{tariff} =~ /^free$/)
		? $tariff_tab{$sms->{operator_id}}{free}
		: $tariff_tab{$sms->{operator_id}}{$sms->{num}};
	my ($transaction_id,$abonent) = split(/\//,$sms->{transaction_id});
	$req->content(<<EOF);
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
xmlns:typ="http://www.ipx.com/api/services/smsapi52/types">
<soapenv:Header/>
<soapenv:Body>
<typ:SendRequest>
<typ:correlationId>$sms->{id}</typ:correlationId>
<typ:originatingAddress>$sms->{num}</typ:originatingAddress>
<typ:originatorTON>0</typ:originatorTON>
<typ:destinationAddress>$abonent</typ:destinationAddress>
<typ:userData>$msg</typ:userData>
<typ:userDataHeader>#NULL#</typ:userDataHeader>
<typ:DCS>-1</typ:DCS>
<typ:PID>-1</typ:PID>
<typ:relativeValidityTime>-1</typ:relativeValidityTime>
<typ:deliveryTime>#NULL#</typ:deliveryTime>
<typ:statusReportFlags>1</typ:statusReportFlags>
<typ:accountName>#NULL#</typ:accountName>
<typ:tariffClass>$tariff</typ:tariffClass>
<typ:VAT>-1</typ:VAT>
<typ:referenceId>$transaction_id</typ:referenceId>
<typ:serviceName>#NULL#</typ:serviceName>
<typ:serviceCategory>#NULL#</typ:serviceCategory>
<typ:serviceMetaData>#NULL#</typ:serviceMetaData>
<typ:campaignName>#NULL#</typ:campaignName>
<typ:username>$esme->{system_id}</typ:username>
<typ:password>$esme->{password}</typ:password>
</typ:SendRequest>
</soapenv:Body>
</soapenv:Envelope>
EOF
	return $req;
}

sub sender_handle_response {
	my ( $proto, $esme, $sms, $response ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
	
	$log->debug($response->as_string());
	if ($response->is_success) {
		my $parsed;
		eval {
			$parsed = XMLin($response->content, KeepRoot => 0);
			$log->debug(Dumper($parsed));
		};

		my $err = $@;
		if ($err) {
			$esme->error("Error while parsing SendSMS response: $@", "Invalid SendSMS response");
		} else {
			my $status = $parsed->{'soapenv:Body'}{'SendResponse'}{'responseCode'};
			if ($status == 0) {
				$log->info("Message SMS$sms->{id} was delivered to SMSC.");
				my $msg_id = $parsed->{'soapenv:Body'}{'SendResponse'}{'messageId'};
				$db->set_sms_new_status($sms->{id}, 2, 0, 0, $msg_id, 1);
				$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id}, 1);
				$esme->outbox_status_changed($sms);
				return;
			}
		}
		$log->warn("Message SMS$sms->{id} wasn't accepted by SMSC.\n" . $response->as_string());
		$db->set_sms_new_status($sms->{id}, 255, 0, 255, 0, 1);
		$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id}, 1);
		$esme->outbox_status_changed($sms);
	} else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
			. "Will try to resend in 10 minutes.\n"
			. $response->as_string());
		$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	}
}



1;
