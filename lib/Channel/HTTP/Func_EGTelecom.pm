#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_EGTelecom; # smsc_id = 559
use strict;
use Data::Dumper;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);
use URI::Escape;

my $status_tab = {
	1 => 12,
	2 => 13,
	4 => 11,
	8 => 11,
	16 => 13
};

sub sender_init {
#	my ($proto, $esme) = @_;
} 

sub sender_handle_routines {
#	my ($proto, $esme) = @_;
} 

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $smsid = $q->param('smsid');

	if ($smsid) {
		# �������� ���
		my $msg;
		if ($q->param('charset')) {
			$msg = encode('cp1251', decode($q->param('charset'), $q->param('text')));
		} else {
			$msg = encode('cp1251', $q->param('text'));
		}
		my $abonent = $q->param('subno');
		if ($abonent =~ m/%/) {
			$abonent = uri_unescape($abonent);
		}
		$abonent = substr($abonent, 1) if ($abonent =~ m/^#/);

		my $sms = {
			smsc_id        => $esme->{smsc_id},
			abonent        => $abonent,
			num            => $q->param('scode'),
			msg            => $msg,
			data_coding    => 0,
			esm_class      => 0,
			transaction_id => $q->param('smsid').':'.($q->param('charset') || 'utf8'),
			link_id        => $q->param('mccmnc')
		};

		my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
		if ($id) {
			$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
		} else {
			$id = $db->insert_sms_into_insms($sms);
			$log->debug(Dumper($sms));
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
		}

		return http_response('200 OK', '');
	} else {
		# �������� ����� � ��������
    $heap->{q} = $q;

		return {
			timer => 5,
			content => http_response('200 OK', ''),
		};
	}
}

sub handle_timer
{
	my($pkg, $esme, $heap) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
  my $q = $heap->{q};

	my $msg_id    = $q->param('id');
	my $type      = $q->param('type');
	my $status    = $status_tab->{ $q->param('type') };
	my $statusstr = $q->param('text');
	$log->debug("msg_id = $msg_id; type = $type; status = $status; statusstr = $statusstr");

	my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $msg_id);
	if ($outsms && $status) {
		$log->info("Report for message SMS$outsms->{id} was received. Status: ".$type."."
			. ($statusstr and " Reason: $statusstr"));
		$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status, undef, $outsms);
		$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1) if ($status == 12);
		$db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
	} else {
		$esme->warning("Bad request: ".Dumper($q), "Bad request");
	}

	return {};
}

sub sender_handle_sms {
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my($transaction_id, $charset) = split /:/, $sms->{transaction_id};
	my $msg = ($sms->{data_coding} == 0)
		? $sms->{msg}
		: encode($charset, decode('UCS-2', $sms->{msg}));

	my $req = {
		user => $esme->{system_id},
		pass => $esme->{password},
		smsid => $transaction_id,
		text => $msg,
		custmtid => $sms->{id},
		dlraddr => 'http://gateasg.alt1.ru/sms.exe?type=%d&text=%a&id='.$sms->{id}
	};
	$log->debug("Request: ".Dumper($req));

	my $ua = LWP::UserAgent->new;
	$ua->timeout(20);
	my $resp = $ua->post($esme->{url}, $req);
	my $reply = $resp->content();
	my $resp_code = $resp->code();
	$log->debug("Response: $reply");
	$log->debug("RespCode = $resp_code");

	if (($resp_code eq '200') && ($reply =~ m/^\s*OK/)) {
		$db->set_sms_new_status($sms->{id},2,0,0,$sms->{id},1);
		$db->set_sms_delivered_status($sms->{id},1,1);
		$log->info("Message SMS$sms->{id} was delivered to SMSC.");
	} else {
		$db->set_sms_new_status($sms->{id},255,0,255,0,1);
		$db->set_sms_delivered_status($sms->{id},1);
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
	}
}

1;
__END__

MO format
http://sms.cust.com/path/to/script
?text=Chat+Just+testing                     # Text message
&keyw=CHAT                                  # Keyword that triggered the routing
&subno=%-2B34667778899                      # Sender�s phone number (subscriber number in international format)
&scode=66000                                # Destination short code
&mccmnc=21407                               # Mobile country code + mobile network code
&smsid=1975493c-df9e-4cf7-a218-75263329f79  # SMS reference ID
&user=customer                              # Credentials to the notifcation script
&pass=custpass                              # Credentials to the notifcation script

200: Ok
400: Bad request, the message will be marked as erroneous and will not be retried
default: Message will be retried until expiration

MT format
http://kgw.egtelecom.es/proc/sendsms.php
?user=customer
&pass=custpass
&smsid=1975493c-df9e-4cf7-a218-75263329f79
&text=This+is+the+answer

Mandatory parameters
user, pass: the customer user name and password
smsid: MO message reference

Message parameters
text: the text message or the binary message in hexadecimal characters
coding: 0 means 7bit, 1 means 8bit and 2 means UCS
udh: User Data Header for binary messages
bulk: if set to 1, send the answer as a non-premium MT

Other parameters
charset (optional): indicates the encoding of the text, defaults to UTF-8
dlraddr: indicates the http address to call back whenever a DLR is received
custmtid: this is a unique ID generated by the customer to avoid duplicate outgoing SMS
sac: if set to DEL, it will clear any existing end user session for this number

DeliveryReport format
http://kgw.egtelecom.es/proc/sendsms.php
?user=customer
&pass=custpass
&smsid=1975493cdf9e-4cf7-a218-75263329f79
&text=This+is+the+answer
&dlraddr=http%3A%2F%2Fmy.server.com%2Fpath%2Fto%2Fdlr%2Fscript%3Ftype%3D%25d%26text%3D%25a%26id%3Dcust_internal_id


http://my.server.com/path/to/dlr/script?type=%d&text=%a&id=cust_internal_id
replace to
http://my.server.com/path/to/dlr/script?type=8&text=ACK%2F&id=cust_internal_id
