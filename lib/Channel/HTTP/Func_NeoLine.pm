#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_NeoLine;
use strict;
use Data::Dumper;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);

sub handle_request
{
  my($pkg, $esme, $heap, $q) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});

#+message - ����� sms ��� ��������
#+sender - ������� �������� (8����������)
# country - ��� ������ ��������� �������� (ru, ua, ...)
# price - ���������� ��������
# service_id - id �������
#+message_id - id ��������� (��� ������������� ������ sms � mt ������������)
#+keyword - �������
#+shortcode - �������� �����, �� ������� ���� ���������� ���������
# area - ������� �������� (��� ������; ��� ������ ����� - 0)
#+operator - ������� �������� ��������
#+billing_type - ��� �����������: mo ��� mt
#+status - ������ ������: pending (������������ � �������� �� �������� ��������� ���������), ok ��� failed (������������ � ������� �� ������ ��� mt)
# sig - �������� �������

  if ($q->param('status') == 'pending') {
    # �������� ���
  	my $sms = {
  		smsc_id        => $esme->{smsc_id},
  		abonent        => $q->param('sender'),
  		num            => $q->param('shortcode'),
  		msg            => encode('cp1251', $q->param('keyword').$q->param('message')),
  		data_coding    => 0,
  		esm_class      => 0,
  		transaction_id => $q->param('message_id'),
      link_id        => $q->param('operator') 
  	};
  
  	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
  	if ($id) {
  		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
  	} else {
  		$id = $db->insert_sms_into_insms($sms);
      $log->debug(Dumper($sms));
  		$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
  	}

    # ������� ��� � ���������� ������������
    $heap->{transaction_id} = $sms->{transaction_id};
    $heap->{billing_type} = $q->param('billing_type');
    $heap->{try} = 0;
    return 5; # First try in 5 seconds
  } elsif ($q->param('status') == 'ok' || $q->param('status') == 'failed') {
    # �������� ����� � ��������
		my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $q->param('message_id'));
		if ($outsms) {
      my $status_id = ($q->param('status') == 'ok') ? 12 : 13;
    
			$log->info("Report for message SMS$outsms->{id} was received. Status: $q->param('status').");
			$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status_id, undef, $outsms);
			$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1) if ($status_id == 12); 
      $db->redis_report($outsms, {}) if ($outsms->{registered_delivery});

      return http_response('200 OK', '');
		} else {
  		$esme->warning("message_id = $q->param('message_id') not found!", "Bad request");

      return http_response('400', '');
		}
  }
}

sub handle_timer
{
  my($pkg, $esme, $heap) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});

  $heap->{try}++;
  $log->debug("[$heap->{session_id}] Fetching...");

  my $msgs      = $db->get_sms_ready_to_send_tr($esme->{smsc_id}, $heap->{transaction_id});
  my $msg_count = scalar keys(%$msgs);
  $log->debug("$msg_count message(s) fetched.");

  my $rv = 5; # Retry in 5 seconds
  if ($msg_count > 0)
  {
    my(undef, $sms)    = each %$msgs;
    $heap->{outsms_id} = $sms->{id};

    my $output = $sms->{msg};

    $log->debug("[$heap->{session_id}]\n$output");
    $rv = http_response(200, $output);

    $db->set_sms_new_status($sms->{id},2,0,0,$heap->{transaction_id});    
    $db->set_sms_delivered_status($sms->{id},($heap->{billing_type} = 'mt') ? 1 : undef);
    $db->redis_report($sms, {});
    $log->info("[$heap->{session_id}] SMS$sms->{id} was delivered to SMSC");
	} elsif ($heap->{try} > 4) { # 4*5+5=25 seconds 
    $log->debug("[$heap->{session_id}] Timed out...");
    $rv = sub{'Timed out'};
  }

  return $rv;
}

sub rcv_on_routines {
	my ($pkg, $esme) = @_;
	my $a = $esme->{db}->{db}->selectall_arrayref('select id from tr_outsms where smsc_id=? and date < subdate(now(), interval 1 minute)',
		undef, $esme->{smsc_id});

	foreach (@$a) {
		my $id = $_->[0];
		$esme->{db}->set_sms_new_status($id,255,0,0,0);
		$esme->{db}->set_sms_delivered_status($id,1);
	}
}

1;
__END__

MO format
���������(NeoLine) �������� ������-���������� ��������(A1) �� ���������� �����������:
message - ����� sms ��� ��������
sender - ������� �������� (8����������)
country - ��� ������ ��������� �������� (ru, ua, ...)
price - ���������� ��������
service_id - id �������
message_id - id ��������� (��� ������������� ������ sms � mt ������������)
keyword - �������
shortcode - �������� �����, �� ������� ���� ���������� ���������
area - ������� �������� (��� ������; ��� ������ ����� - 0)
operator - ������� �������� ��������
billing_type - ��� �����������: mo ��� mt
status - ������ ������: pending (������������ � �������� �� �������� ��������� ���������),
ok ��� failed (������������ � ������� �� ������ ��� mt)
sig - �������� �������

MT format
no

DeliveryReport format
= MO
