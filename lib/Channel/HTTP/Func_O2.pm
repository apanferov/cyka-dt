#!/usr/bin/perl
package Channel::HTTP::Func_O2;
use strict;
use warnings;
use Channel::HTTP::HTTPUtils;
use HTTP::Request::Common qw(POST);
use HTTP::Request::Common;
use XML::Simple;
use Tree::RB;


sub sender_init {
	my ( $proto, $esme ) = @_;
	$esme->{tree} = Tree::RB->new(sub {$_[0] <=> $_[1]});
}

sub sender_handle_sms{
	my ( $proto, $esme, $sms ) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});

	my $struct = {
				  method => 'SendSMS',
				  login => { content => $esme->{system_id} },
				  pwd => { content => $esme->{password} },
				  originator => { content => 'aaa' },
				  phone_to => { content => ($sms->{abonent} =~ m/(.{10})$/)[0]},
				  message => { content => $sms->{msg} },
				  sync => { content => $sms->{id}},
				 };
	my ($code, $resp, $parsed_resp) = request($esme,$struct);

	if ($code eq '200') {
		if ($parsed_resp->{method} eq 'SendSMS') {
			my $msg_id = $parsed_resp->{sms}->{id} || 0;
			my $parts = $parsed_resp->{sms}->{parts} || 0;
			$db->set_sms_new_status($sms->{id},2,0,0,$msg_id,1);
			$db->set_sms_delivered_status($sms->{id},0,$parts);
			add($esme,time()+60,$msg_id);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
			return;
		}
		elsif ($parsed_resp->{method} eq 'ErrorSMS') {
			$db->set_sms_new_status($sms->{id},255,0,255,0,1);
			$db->set_sms_delivered_status($sms->{id},1);
			$esme->warning("Message SMS$sms->{id} wasn't accepted by SMSC.",$resp->as_string()."https://asg5.alt1.ru/?module=findmsg&action=inboxinfo&inbox_id=".$sms->{inbox_id});
			return;
		}
	}
	else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n".$resp->as_string());
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Will try to resend in 10 minutes.");
		return;
	}
	$db->set_sms_new_status($sms->{id},255,0,255,0,1);
	$db->set_sms_delivered_status($sms->{id},1);
	$esme->warning("Message SMS$sms->{id} wasn't accepted by SMSC.",$resp->as_string());
	return;
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});
	my $node = $esme->{tree}->min();
	while ($node and ($node->key() <= time())) {
		my @list = @{$node->val()};
		$esme->{tree}->delete($node->key());
		while (@list) {
			my $msg_id = shift(@list);
			my $struct = {method => 'CheckStatusSMS',sms_id => {content => $msg_id}};
			my ($code, $resp, $parsed_resp) = request($esme,$struct);
			if (($code eq '200')and($parsed_resp->{method} eq 'CheckSMS')) {
				my $state_id = $parsed_resp->{sms}->{state_id} || '';
				my $state_update = $parsed_resp->{sms}->{state_update} || '';
				if ($state_id eq 'st_deliver') {
					my $s = $db->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
					$db->set_sms_report_status($s->{id},12,$state_update,$s);
					$log->info("SMS$s->{id} was delivered to abonent ($state_id).");
					next;
				}
				elsif ($state_id eq 'st_fail') {
					my $s = $db->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
					$db->set_sms_report_status($s->{id},13,$state_update,$s);
					$log->info("SMS$s->{id} wasn't delivered to abonent ($state_id).");
					next;
				}
				elsif ($state_id eq 'st_notdeliver') {
					my $s = $db->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
					$db->set_sms_report_status($s->{id},15,$state_update,$s);
					$log->info("SMS$s->{id} wasn't delivered to abonent ($state_id).");
					next;
				}
			}
			add($esme, time()+600, $msg_id);
		}
	}
	continue {
		$node = $esme->{tree}->min();
	}
	my $delta = 0;
	$delta = ($esme->{tree}->min()->key() || 0) - time() if ($esme->{tree}->min());
	$delta = $delta > 60 ? 60 : $delta;
	$delta = $delta < 1 ? 1 : $delta;
	return $delta;
}

sub add{
	my ( $esme, $time, $element ) = @_;
	my $val = $esme->{tree}->get($time);
	if ($val) {
		push(@$val,$element);
	}
	else {
		$esme->{tree}->put($time,[$element]);
	}
}

sub request{
	my ($esme,$struct) = @_;

	my $xml = XMLout($struct, RootName => 'request', XMLDecl => '<?xml version="1.0" encoding="windows-1251"?>');
	my $header = HTTP::Headers->new;
	$header->header('content-type' => 'text/xml');
	my $req = HTTP::Request->new('POST', $esme->{url}, $header, $xml);
	$esme->{log}->debug("REQUEST\n".$req->as_string());

	my $resp=$esme->{ua}->request($req);

	$esme->{log}->debug("RESPONSE\n".$resp->content()); 
	my $code = $resp->code();
	return $code, $resp, XMLin($resp->content(), ForceContent => 1) 
		if ($code eq '200');
	return $code, $resp;
}



1;
