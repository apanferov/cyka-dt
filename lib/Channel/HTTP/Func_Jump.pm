package Channel::HTTP::Func_Jump;
use strict qw(vars);
use Data::Dumper;
use XML::Simple;
use Encode qw(encode decode);
use Channel::HTTP::HTTPUtils qw(http_response);
use POSIX qw(strftime);

my %Dr_status = (
	Delivered     => 12,
	Delivering    => 11,
	Undeliverable => 13
);

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

    my $xml = $q->param('POSTDATA');
    $log->debug($xml);

    my $ref = XMLin($xml, ForceContent => 1);
    $log->debug(Dumper($ref));

    if ($ref->{service}->{content} eq 'content-request')
	{
		my $sms = {
			smsc_id        => $esme->{smsc_id},
			num            => $ref->{sn}->{content},
			abonent        => $ref->{'sin'}->{content},
			msg            => encode('cp1251',$ref->{body}->{content}),
			data_coding    => 0,
			esm_class      => 0,
			link_id        => $ref->{operator}->{content},
			transaction_id => $ref->{rid},
		};
		$log->debug(Dumper($sms));

		my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
		if ($id) {
			$log->warn("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
		}
		elsif ($sms->{msg} =~ m/^\s*258691/i) {
#		elsif (0) {
			$log->warn("SMS $sms->{transaction_id} was fully ignored.");
		}
		else {
			$id = $db->insert_sms_into_insms($sms);
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
    	}
    }
	elsif ($ref->{service}->{content} eq 'delivery-report')
	{
		my $outsms_id      = $ref->{mid};
		my $transaction_id = $ref->{rid};
		my $status         = $ref->{status}->{content};
		my $error          = $ref->{status}->{error};

		$log->info("Report for message SMS$outsms_id was received. Status: $status."
			. ($error and " Reason: $error"));
		my $sms = $db->{db}->selectrow_hashref('select * from tr_outsmsa where id=? limit 1', undef, $outsms_id);
		$db->set_sms_report_status($outsms_id, $Dr_status{$status} || 13, undef, $sms);
		if ($status eq 'Delivered') {
			$db->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1)
				unless ($sms->{tariff} =~ /^free$/);
		}

		$Dr_status{$status} or yell("Unknown delivery report status for SMS$outsms_id\n$xml",
				code    => 'WARNING',
				process => $esme->{pr_name},
				header  => "$esme->{pr_name}: Unknown delivery report status",
				ignore  => { log4perl => 1 }
				);
	}

    my $resp = http_response('200 OK', qq(<?xml version="1.0" encoding="UTF-8"?>\n<report>\n<status>Accepted</status>\n</report>\n)
		);
    $log->debug("Response: Accepted");
	return $resp;
}


sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $paid = 'true';
	$paid = 'false' if ($sms->{tariff} =~ /^free$/);
	$paid = 'false' unless ($sms->{operator_id} == 159);
	my $struct = {
		mid    => $sms->{id},
		rid    => $sms->{transaction_id},
		paid   => $paid,
		bearer => 'SMS',
		body   => {
			'content-type' => 'text/plain',
			content        => decode('cp1251',$sms->{msg})
		}
	};

	$sms->{transaction_id} or $struct->{sin}{content} = $sms->{num};

    my $xml = encode('UTF-8',
		XMLout($struct, RootName => 'message', XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>')
		);

    my $header = HTTP::Headers->new;
  	$header->header('content-type' => 'text/xml');
  	$header->authorization_basic($esme->{system_id}, $esme->{password});

  	my $req = HTTP::Request->new('POST', $esme->{url}, $header, $xml);
  	$log->debug($req->as_string());

    my $resp  = $esme->{ua}->request($req);
	my $reply = $resp->content();
	$log->debug($reply); 

	unless (($resp->code() eq '200') and ($reply =~ m/^\s*<\?xml/))
	{
        $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
			. "Will try to resend in 10 minutes.\n"
			. $resp->as_string());
        $db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
    }
	else
	{
		my $ref = XMLin($reply, ForceContent => 1);
  		my $status = $ref->{status}->{content};

		if ($status eq 'Accepted')
		{
			$db->set_sms_new_status($sms->{id}, 2, 0, 0, 0, 1);
			$db->set_sms_delivered_status($sms->{id}, 1);
         	$log->info("Message SMS$sms->{id} was delivered to SMSC.");
        }
        else
		{
			$db->set_sms_new_status($sms->{id}, 255, 0, 255, 0, 1);
			$db->set_sms_delivered_status($sms->{id}, 1);

			$log->warn("Message SMS$sms->{id} wasn't accepted by SMSC.\n" . $resp->as_string());
         }
	} #HTTP 200 OK
}

sub sender_handle_routines { 0 }

1;
__END__
