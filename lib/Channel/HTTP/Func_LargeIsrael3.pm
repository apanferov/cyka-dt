#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_LargeIsrael3;
use strict;
use warnings;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use HTTP::Request;
use URI;
use URI::QueryParam;
use XML::Simple;

use Channel::HTTP::HTTPUtils qw/http_response/;

sub sender_init {
	return 'asynchttp';
}

sub sender_handle_sms {
	my ($proto, $esme, $sms) = @_;

	my $req = _sender_prepare_request($esme, $sms);

	$esme->{log}->info("Will request @{[$req->uri->as_string]}");

	$esme->{asynchttp}->($esme, $sms, $req, \&_sender_handle_response);
	$esme->{db}->set_sms_new_status($sms->{id},0,600);
}

sub sender_handle_routines {
}

sub init {
}

my %request_identification_sets = (
	mo  => [qw(from to text ts)],
	dlr => [qw(id cli shortcode msg_ts dlr_ts status reason)]
	);
my %request_handlers = (
	mo  => \&_handle_mo_request,
	dlr => \&_handle_dlr_request
	);

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $req_type;

  OUTER:
	while (my($k, $v) = each %request_identification_sets) {
		for (@$v) {
			unless ($q->param($_)) {
				next OUTER;
			}
		}
		$req_type = $k;
		last;
	}
	# reset hash iterator
	keys %request_identification_sets;


	unless ($req_type) {
		$esme->error("Request is: @{[Dumper(scalar $q->Vars)]} ", "Israel request not recognized");
		return http_response(500, "request not recognized");
	}
	$esme->{log}->debug("req_type=$req_type");
	$heap->{query} = $q;
	return $request_handlers{$req_type}->($esme, $heap, $q);
}


sub handle_timer {
#	my $proto = shift;
#	return _handle_dlr_request(@_, $_[2]->{query});
}

###

sub _sender_handle_response {
	my($esme, $sms, $response) = @_;

	$esme->{log}->debug("Response: ".$response->content);
	my $parsed;
	eval {
		$parsed = XMLin($response->content, KeepRoot => 0);
	};
	if ($@) {
		$esme->error("Error parsing Israel response XML: code is @{[$response->code]}, content is:\n@{[$response->content]}\n\nError is: $@", "Error parsing response XML");
		$parsed = {message => {reason => $@}};
	}
	$esme->{log}->debug("Parsed: ".Dumper($parsed));

	my $status = $parsed->{message}{status}{code};
	if ($status == 0) {
		$esme->{db}->set_sms_new_status($sms->{id},2,0,0,$parsed->{message}{queue_id},1);
		$esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->{log}->info("Message SMS$sms->{id} was delivered to SMSC.");
	}
	elsif ( $status == 500 or $status == 502 or $status == 503 or not defined $status) {
		if ($sms->{retry_count} > 6) {
			$esme->{log}->error("Message SMS$sms->{id} wasn't delivered to SMSC.\nRetry limit reached, giving up.\n\n".$parsed->{message}{reason});
			$esme->{db}->set_sms_new_status($sms->{id},255,0,255,0,1);
			$esme->{db}->set_sms_delivered_status($sms->{id},1);
		}
		else {
			$esme->{log}->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n".$parsed->{message}{reason});
			$esme->{db}->set_sms_new_status($sms->{id},0,600,255,0,1);
			$esme->{log}->warn("Will try to resend in 10 minutes.");
		}
	}
	else {
		# Total failure
		$esme->{db}->set_sms_new_status($sms->{id},255,0,255,0,1);
		$esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->{log}->warn("Message SMS$sms->{id} wasn't accepted by SMSC.",
					   "HTTP resp was: ".$response->as_string());
	}
}

sub _sender_prepare_request {
	my ($esme, $sms) = @_;
	my $port = "NIS10.0";

	# ������� ��� ���������� ����������� ������
	my $inbox_msg = $esme->{db}->get_value('select msg from tr_inboxa where id=?',undef,$sms->{inbox_id});
	$port = "NIS0.5" if ($inbox_msg =~ /^\?\?\?\?/);

	my $u = new URI($esme->{url});
	$u->query_form(
		account => $esme->{system_type},
		user    => $esme->{system_id},
		pass    => $esme->{password},
		from    => $sms->{num},
		to      => $sms->{abonent},
		text    => encode('UTF-8', decode('cp1251', $sms->{msg})),
		msgtype => 'text',
		port    => $port,
		$sms->{params}{'p.service_id'} ? (service_id => $sms->{params}{'p.service_id'}) : ()
	);

	return HTTP::Request->new(GET => $u->as_string);
}

sub _handle_mo_request {
	my ($esme, $heap, $q) = @_;

	my $abonent = $q->param('from');

	unless ($abonent =~ /^\+(\d+)$/) {
		$esme->error("Abonent is: '@{[$abonent]}'", "Israel request: invalid abonent");
		return http_response(500, "Invalid request - malformed 'from'");
	}
	$abonent = $1;

	my $num = $q->param('to');
	my $text = $q->param('text');

	my $sms = { smsc_id => $esme->{smsc_id},
				num => $num,
				abonent => $abonent,
				msg => encode('cp1251',decode('UTF-8',$text)),
				data_coding => 0,
				esm_class => 0,
				link_id => $q->param('mcc').'-'.$q->param('mnc') };
	my $id = $esme->{db}->insert_sms_into_insms($sms);
	$esme->{last_received_sms_time} = time();
	$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

	return http_response(200, "OK");
}

my %delivery_status_mapping = (
	QUEUED    => 11,
	DELIVERED => 12,
	REJECTED  => 13,
	FAILED    => 15,
	DISCARDED => 13
	);

sub _handle_dlr_request {
	my ($esme, $heap, $q) = @_;

	my $msg_id = $q->param('queue_id');
	my $status = $q->param('status');

	my $sms = $esme->{db}->get_sms_by_msg_id($esme->{smsc_id}, $msg_id);

	if ($sms) {
		my $asg_status = $delivery_status_mapping{$status} || 13;
		$esme->{db}->set_sms_report_status($sms->{id}, $asg_status, undef, $sms);
		$esme->{db}->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1)
			if ($asg_status == 12);
		$esme->{log}->info("Message SMS$sms->{id} was delivered with status: $status($asg_status)");

		return http_response(200, "OK");

	} else {
		$esme->error("MSG$msg_id unknown for SMSC$esme->{smsc_id}", "delivery report for unknown message");
		return http_response(500, "Delivery report for unknown message");
	}
}

1;
