#!/usr/bin/perl
package Channel::HTTP::Func_Roboxchange;
use strict;
use warnings;

use Data::Dumper;
use URI;

use Channel::HTTP::HTTPUtils qw/http_response/;
use Disp::Utils;
use Digest::MD5 qw(md5_hex);

use base qw/Channel::Mixin_TrafficSender/;

sub init {
	my ( $proto, $esme ) = @_;
	$esme->{log}->debug("Init");

	$esme->{rx}{num_prices} = Config->dbh->selectall_hashref
	  ("select num, price_in from set_real_num_price where date_start <= date(now()) and price_in > 0 and operator_id = ? order by date_start desc limit 1",
	   'num',
	   undef,
	   $esme->{operator_id});

	$esme->{rx}{MerchantLogin} = $esme->{system_id};
	($esme->{rx}{MerchantPass1}, $esme->{rx}{MerchantPass2}) = split /\|/, $esme->{password}, 2;
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	if ($q->param('inv_id')) {
		return handle_robox_request($proto, $esme, $heap, $q);
	}

	if ($q->param('abonent')) {
		return handle_shared_request($proto, $esme, $heap, $q);
	}

	$esme->error("Unrecognized request to ROBOX client:\n@{[Dumper(scalar $q->Vars)]}", "Urecognized request");
	return http_response(403, "<h1>You are not welcome here</h1>");
}

sub ip_match {
	my ( $address, $set ) = @_;
	return $address eq $set;
}

sub get_price {
	my ( $esme, $num ) = @_;
	return $esme->{rx}{num_prices}{$num}{price_in};
}

sub handle_shared_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my ( $abonent, $num, $msg ) = map { $q->param($_) || undef } qw/abonent num msg/;

	my $price = get_price($esme, $num);

	unless ($abonent =~ /^7\d{10}$/) {
		return http_response(200, "error: abonent_incorrect");
	}

	unless ($msg and length $msg) {
		return http_response(200, "error: message_incorrent");
	}

	unless ($price) {
		return http_response(200, "error: short_num_incorrect");
	}

	Config->dbh->do("insert into tr_roboxchange set abonent = ?, num = ?, msg = ?, price = ?, date = now(), paid = 0", undef,
					$abonent, $num, $msg, $price);

	my ( $id ) = Config->dbh->{mysql_insertid};

	my $link = make_robox_link(esme => $esme,
							   abonent => $abonent,
							   num => $num,
							   msg => $msg,
							   price => $price,
							   id => $id);

	return http_response(200,"link: $link");
}

sub make_robox_link {
	my %p = @_;
	my $u = new URI($p{esme}{url});


	my %request = ( lang => 'ru',
					mrh => $p{esme}{rx}{MerchantLogin},
					in_curr => 'rur',
					out_summ => $p{price},
					inv_id => $p{id},
					inv_desc => 'access to short number',
				  );

	$p{esme}{log}->debug("Robox link params: @{[Dumper(\%request)]}");

	$request{crc} = md5_hex("$request{mrh}:$request{out_summ}:$request{inv_id}:$p{esme}{rx}{MerchantPass1}");
	$u->query_form(%request);
	my $uri =  $u->as_string;

	$p{esme}{log}->debug("Generated link is $uri");
	return $uri;
}

sub handle_robox_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my ( $out_summ, $inv_id, $crc ) = map { $q->param($_) || "" } qw/out_summ inv_id crc/;

	my $crc_our = md5_hex("$out_summ:$inv_id:$esme->{rx}{MerchantPass2}");

	if ($crc ne $crc_our) {
		$esme->error("CRC mismatch: got $crc, need $crc_our", "Payment crc mismatch");
		return http_response(200, "CRC mismatch");
	}

	my $pmt = Config->dbh->selectrow_hashref("select * from tr_roboxchange where id = ?", undef, $inv_id);

	unless ($pmt) {
		$esme->eror("No invoice with id '$inv_id'", "Missing invoice");
		return http_response(200, "inv_id not found");
	}

	if ($pmt->{paid}) {
		$esme->error("Invoice $inv_id already paid", "Duplicate invoice");
		return http_response(200, "already paid");
	}

	my $sms = { smsc_id => $esme->{smsc_id},
				num => $pmt->{num},
				abonent => $pmt->{abonent},
				msg => $pmt->{msg},
				data_coding => 0,
				esm_class => 0,
				transaction_id => $inv_id };

	my $insms_id = $esme->{db}->insert_sms_into_insms($sms);
	Config->dbh->do("update tr_roboxchange set paid = 1 where id = ?", undef, $pmt->{id});

	return http_response(200, "OK$inv_id");
}

1;
