package Channel::HTTP::Func_Nvgr;
use strict qw(vars);
use Data::Dumper;
use Encode qw(encode decode);
use URI::URL;
use XML::Simple;
use Channel::HTTP::HTTPUtils qw(http_response);
use POSIX qw(strftime);


sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $sms = {smsc_id => $esme->{smsc_id},
			   num => $q->param('sms_number'),
			   abonent => $q->param('sender_number'),
			   msg => encode('cp1251',$q->param('sended_text')),
			   transaction_id => $q->param('sms_id'),
			   data_coding => 0,
			   esm_class => 0,
			  };
	$esme->{log}->debug(Dumper($sms));

	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
	if ($id) {
		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	}
	else {
		$id = $esme->{db}->insert_sms_into_insms($sms);
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	return http_response('200 OK', '');
}


sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});
	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if $esme->{local_addr};
	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $bill = ($sms->{tariff} =~ /^free$/i) ? 0 : 1;
	my %req = (login => $esme->{system_id}.$sms->{num},
			password => $esme->{password},
			msisdn => $sms->{abonent},
			sts_text => decode('cp1251',$sms->{msg}),
			bill => $bill
		 );
	$log->debug("Request: ".Dumper(\%req));
	my $resp = $ua->post($esme->{url},\%req);
	my $reply = $resp->content();
	$log->debug("Response: $reply");
	if (($resp->code() eq '200') and ($reply =~ m/^\s*<\?xml/)) {
		my $ref = eval { XMLin($reply, ForceContent => 0) };
		$esme->warning($@,"Parsing error") if ($@);
		my $status = $ref->{status};
		if (defined($status) and (($status == 0) or ($status == -700))) {
			# $id, $status, $seconds, $err, $msg_id, $update_retry_count
			$db->set_sms_new_status($sms->{id},12,0,0,$ref->{smsid},1);
			$db->set_sms_delivered_status($sms->{id},0,$bill);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		} else {
			$db->set_sms_new_status($sms->{id},255,0,0-$ref->{status},0,1);
			$db->set_sms_delivered_status($sms->{id},1);
			$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. ($ref->{status})");
		}
	} else {
        $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Will try to resend in 10 minutes.");
	}
	return;
}

sub sender_handle_routines { 0 }

1;
__END__
