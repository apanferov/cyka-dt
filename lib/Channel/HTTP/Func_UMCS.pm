package Channel::HTTP::Func_UMCS;
use strict;
use Data::Dumper;
use Encode qw(encode decode);

use SOAP::Transport::IO;

use Channel::HTTP::HTTPUtils qw/http_response/;
use Disp::Config;
use Disp::UmcsSOAP2;
our $SOAP_GLOBAL;


sub init
{
	my($pkg, $esme) = @_;
	$SOAP_GLOBAL = { log => $esme->{log}, db => $esme->{db}, url => $esme->{url}, operator_id => $esme->{operator_id}, smsc_id => $esme->{smsc_id} };
}

sub handle_request
{
	my($pkg, $esme, $heap, $query) = @_;

	my $input  = $query->param("POSTDATA");
	my $output = '';
	my $ifh = IO::Scalar->new(\$input);
	my $ofh = IO::Scalar->new(\$output);

	# HTTP_SMSReceiver4 debug it ### $esme->{log}->debug('INPUT ' . $input);

	SOAP::Transport::IO::Server
		->new(in => $ifh, out => $ofh)
		->dispatch_with({ 'http://www.estylesoft.com/umcs/shop/' => 'Disp::UmcsSOAP2' })
		->handle;

	$esme->{log}->debug('OUTPUT ' . $output);
	
	http_response(200, $output, content_type => 'text/xml', charset => 'utf-8');
}
	
sub sender_init
{
	my($pkg, $esme) = @_;
	$SOAP_GLOBAL = { log => $esme->{log}, db => $esme->{db}, url => $esme->{url}, operator_id => $esme->{operator_id} };
}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $result;
	my $order;
	my $status = 255;

	if (!$sms->{transaction_id}) {
		my $CTN = $sms->{abonent};
		$CTN =~ s/.*(\d{10})$/$1/;

		Disp::UmcsSOAP2::cert_conf($sms->{num});
		sleep(1);
		$result = Disp::UmcsSOAP2::Helper::ProcessPurchaseShortPhoneAsync(
			$sms->{num},
			"##$sms->{id}",
			$CTN);
		$status = 2;
	}
	elsif ($order = Disp::UmcsSOAP2::Helper::db_load($sms->{transaction_id}) and $order->{our_status} != 6) {
		$db->set_sms_new_status($sms->{id},255,0,0,0);
		$db->set_sms_delivered_status($sms->{id});
		$esme->fatal_error("Message in wrong status\nSMS: @{Dumper($sms)}\nORDER: @{[Dumper($order)]}");
		return;
	}
	else {
		my $umcs_status = $sms->{num} =~ /free/ ? 1 : 0;
		$result = Disp::UmcsSOAP2::Helper::PurchaseEnd(
			$sms->{transaction_id},
			decode('cp1251', $sms->{msg}),
			$umcs_status);
		$status = 12;

		my $our_status = ($result == 0 and $umcs_status == 0 ? 8 : 9)
			or ($umcs_status == 0 ? 7 : 10);

		Disp::UmcsSOAP2::Helper::db_update($sms->{transaction_id}, { our_status => $our_status });
	}

	if ($result == 0) {
		$db->set_sms_new_status($sms->{id},$status,0,0,int($sms->{transaction_id}));
		$db->set_sms_delivered_status($sms->{id},1);
		$log->info("Message SMS$sms->{id} was delivered to abonent.");
	} else {
		$db->set_sms_new_status($sms->{id},255,60,0,int($sms->{transaction_id}));
		$log->warn("Message SMS$sms->{id} wasn't delivered. Retry in 1 min");
	}
}

sub sender_handle_routines
{
	my($pkg, $esme) = @_;

	$esme->{log}->debug("UMCS routines, searching for stalled transactions");
	my $stalled = $esme->{db}->{db}->selectall_arrayref("select * from tr_umcs_requests where our_status = 6 and reserve_date between date_sub(now(), interval 91 minute) and date_sub(now(), interval 79 minute)",
		{ Slice => {} });

	for my $order (@$stalled)
	{
		my $result = Disp::UmcsSOAP2::Helper::PurchaseEnd(
			$order->{order_id},
			decode('utf-8', "Сервис не ответил в установленное время"),
			1);

		my($our_status, $reason) = ($result == 0 and (14, 'successfull'))
			or (15, 'unsuccessfull');

		Disp::UmcsSOAP2::Helper::db_update($order->{order_id}, { our_status => $our_status });

		my $message = "Stalled transaction detected. \n"
			. "Attempt to rollback this transaction on UMCS side was $reason.\n"
			. "Order info is\n"
			. Dumper($order);
		$esme->{log}->error($message);
		yell($message,
			code    => 'ERROR',
			process => $esme->{pr_name},
			header  => "$esme->{pr_name}($esme->{name}): Stalled transaction detected",
			ignore  => { log4perl => 1 });

	}
}

1;
__END__
