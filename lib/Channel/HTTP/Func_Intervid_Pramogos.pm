#!/usr/bin/perl
package Channel::HTTP::Func_Intervid_Pramogos;
use strict;
use warnings;
use Channel::HTTP::HTTPUtils;

sub init{
	my ( $proto, $esme ) = @_;
}

sub handle_request{
	my ( $proto, $esme, $heap, $q ) = @_;
	my $params = $q->Vars;
	
	my $sms = {
		smsc_id => $self->{smsc_id},
		num => ($params->{price}) ? "$params->{short}:$params->{price}" : $params->{short},
		abonent => $params->{phone},
		msg => encode('cp1251',decode('UTF-8',$params->{text})),
		data_coding => 0,
		esm_class => 0,
		link_id => "$params->{country}#$params->{operator}",
	};
	
	if ($sms->{link_id} eq 'lv#lmt') {
		$sms->{transaction_id} = substr($sms->{abonent},8);
		$sms->{abonent} = '371'.substr($sms->{abonent},0,8);
	}
	elsif ($sms->{link_id} eq 'ee#emt-ee') {
		$sms->{abonent} = '372'.$sms->{abonent};
	}
	
	my $id = $self->{db}->insert_sms_into_insms($sms);
	$self->{log}->debug(Dumper($sms));
	$self->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

	$self->{log}->debug("Response: OK");
	return http_response('200 OK', 'OK',
						 content_type => 'text/xml',
						 charset => 'cp1251');

 }

sub handle_alarm{
	my ( $proto, $esme, $heap ) = @_;
}

1;