package Channel::HTTP::Func_MTS_UA;
use strict qw(vars);
use Data::Dumper;
use Encode qw(encode decode);
use URI::URL;
use Channel::HTTP::HTTPUtils qw(http_response);

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
	
	my $abonent_addr = $q->param('from');
	my ( $abonent, $abonent_domain ) = split /\@/, $abonent_addr, 2;
	($abonent,$abonent_domain) = ('380000000000','alt1.ru') if ( $abonent_addr =~ /v\.islamov/);
	my $num_addr = $q->param('to');
	$num_addr =~ s/[\>\<]//g;
	my ( $num ) = $num_addr =~ /^[a-zA-Z]*(\d+)\x40/;
	my $msg = $q->param('msg');
	$msg = substr($msg,ord($1)+1) if ($msg =~ /^([\x01-\x09])/); # Detect UDH and remove it
	my $sms = {smsc_id => $esme->{smsc_id},
		num => $num,
		abonent => $abonent,
		msg => $msg,
		transaction_id => substr($q->param('message-id'),0,64),
		data_coding => 0,
		esm_class => 0,
		link_id => "$abonent_domain $num_addr"
	};
	$esme->{log}->debug(Dumper($sms));

	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
	if ($id) {
		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	}
	else {
		$id = $esme->{db}->insert_sms_into_insms($sms);
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	return http_response('200 OK', '');
}


sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});
	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if $esme->{local_addr};
	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $url = url($esme->{url});
	my ( $abonent_domain, $num_addr ) = split /\s/, $sms->{link_id}, 2;
	$sms->{abonent} = 'v.islamov' if ($sms->{abonent} == 380000000000);
	my %req = (from => $num_addr,
		to => "$sms->{abonent}\@$abonent_domain",
		msg => $sms->{msg}
	);

	$url->query_form(%req);
	$log->debug($url);
	my $resp = $ua->get($url);
	my $reply = $resp->content();
	$log->debug("Response: $reply");
	if ($resp->code() eq '200') {
		$db->set_sms_new_status($sms->{id},2,0,0,0,1);
		$db->set_sms_delivered_status($sms->{id},0,1);
		$log->info("Message SMS$sms->{id} was delivered to SMSC.");
	}
	else {
        $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Will try to resend in 10 minutes.");
	}
	return;
}

sub sender_handle_routines { 0 }

1;
__END__
