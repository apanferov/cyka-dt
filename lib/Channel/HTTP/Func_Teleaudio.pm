#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_Teleaudio;
use strict;
use warnings;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Request::Common;
use URI;
use URI::QueryParam;
use XML::Simple;

use Channel::HTTP::HTTPUtils qw/http_response/;

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_response {
	my ( $proto, $esme, $sms, $response ) = @_;

	my $code = $response->code;

	my $need_retry = 0;
	my $reason = "";

	if ($code == 200) {
		my $parsed;
		eval {
			$esme->{log}->debug("HTTP response: \n".$response->content);
			$parsed = XMLin($response->content,
							KeepRoot => 1,
							ForceArray => [qw/sms messageDelivered messageSent mms/],
							KeyAttr => {});
			$esme->{log}->debug("Parsed HTTP response: @{[Dumper($parsed)]}");
		};

		if ($@) {
			my $err = $@;
			$reason = "SMSC returned invalid XML:\n@{[$response->content]}, parsing failed with: $err";
			$need_retry = 1;
		} else {
			my $submit_resp = $parsed->{response}{sms}[0];
			if (ref($submit_resp) eq 'HASH' and $submit_resp->{accepted} and $submit_resp->{guid}) {
				if ($submit_resp->{accepted} eq 'true') {
					$esme->{db}->set_sms_new_status($sms->{id},2,0,0,$submit_resp->{guid},1);
					$esme->{db}->set_sms_delivered_status($sms->{id});
					$esme->{log}->info("Message SMS$sms->{id} was delivered to SMSC.");
					return;
				} else {
					$reason = "SMSC rejected our sms with reason: ".Dumper($submit_resp->{reason});
				}
			}
		}
	} else {
		$need_retry = 1;
		$reason = "SMSC responded with not-200 HTTP result: @{[$response->code]}\n@{[$response->content]}";
	}

	if ($need_retry and $sms->{retry_count} < 6) {
		$esme->{log}->warn("Message SMS$sms->{id} wasn't delivered to SMSC. $reason");
		$esme->{log}->warn("Will try to resend in 10 minutes.");
		$esme->{db}->set_sms_new_status($sms->{id},0,600,255,0,1);
	} else {
		$esme->error("Message SMS$sms->{id} wasn't delivered to SMSC.\n$reason", "Message wasn't delivered to SMSC");
		$esme->{db}->set_sms_new_status($sms->{id},255,0,255,0,1);
		$esme->{db}->set_sms_delivered_status($sms->{id},1);
	}
}

sub sender_prepare_request {
	my ( $proto, $esme, $sms ) = @_;

	my $link_id = $esme->{db}->get_link_id($sms->{abonent}, $esme->{smsc_id});

	my $xml = encode('UTF-8',
		XMLout({ data => { sms => { id => $sms->{transaction_id},
										  net => $link_id,
										  priorytet => 3,
										  msisdn => $sms->{abonent},
										  la => $sms->{num},
										  msg => decode('cp1251',$sms->{msg}) } } },
					 KeepRoot => 1,
					 NoAttr => 1,
					 XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>'));

	my $u = new URI($esme->{url});
	$u->query_form(ver => 'ext');

	my $req = POST($u->as_string,
				   Content_Type => 'form-data',
				   Content => { xmlmsg => $xml });
	return $req;
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	my $req = sender_prepare_request($proto, $esme, $sms);

	$esme->{log}->debug("Will request @{[$req->as_string]}");

 	my $resp = $esme->{ua}->request($req);

# 	my $resp = HTTP::Response->new(200, "OK", undef, <<EOF);
# ?xml version="1.0" encoding="utf-8" ?>
# <response>
# <sms>
# <accepted>true</accepted>
# <guid>ffffffff</guid>
# <reason>test</reason>
# </sms>
# </response>
# EOF

	return sender_handle_response($proto, $esme, $sms, $resp);
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}

sub init {
	my ( $proto, $esme ) = @_;
}

my %type_handlers = ( sms => \&handle_message,
					  messageDelivered => \&handle_delivery_report, );

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $parsed;
	eval {
		$parsed = XMLin($q->param('xmlmsg'), KeepRoot => 1, ForceArray => [qw/sms messageDelivered messageSent mms/], KeyAttr => {});
	};

	if ($@) {
		$esme->error("Erorr parsing XML: @{[$q->param('xmlmsg')]}\n\nError is: $@", "Error parsing XML");
		return http_response(500, "Error while parsing XML");
	}

	foreach my $type (keys %{$parsed->{data}}) {
		my $handler = $type_handlers{$type};
		if ($handler) {
			$handler->($proto, $esme, $heap, $q, $_) for @{$parsed->{data}{$type}};
		} else {
			$esme->error("Unsupported XML item $type: @{[Dumper($parsed->{data}{$type})]}", "Unsupported item in XML");
		}
	}
	return http_response(200, 'OK');
}

sub handle_message {
	my ( $proto, $esme, $heap, $q, $item ) = @_;

	my $id = $esme->{db}->get_insms_id_by_transaction_id($esme->{smsc_id}, $item->{id});

	if ($id) {
		$esme->warning("SMS $item->{id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	} else {
		$item->{msisdn} =~ s/^\+//;
		$item->{msg} = "" if ref $item->{msg};

		my $sms = { smsc_id => $esme->{smsc_id},
					num => $item->{la},
					abonent => $item->{msisdn},
					msg => encode('cp1251', $item->{msg}),
					data_coding => 0,
					esm_class => 0,
					transaction_id => $item->{id},
					link_id => $item->{net} };
		my $id = $esme->{db}->insert_sms_into_insms($sms);
		$esme->{last_received_sms_time} = time();
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
}

my %delivery_status_map = ( delivered => 12,
							failed => 13,
							timeout => 14 );

sub handle_delivery_report {
	my ( $proto, $esme, $heap, $q, $item ) = @_;
	my $sms = $esme->{db}->get_sms_by_msg_id($esme->{smsc_id}, $item->{id});
	my $asg_status = $delivery_status_map{$item->{status}};
	if ($asg_status) {
		my $del_date = $item->{time};
		$del_date =~ s/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/$1-$2-$3 $4:$5:$6/;
		$esme->{db}->set_sms_report_status($sms->{id},$asg_status,$del_date,$sms);
		$esme->{log}->info("Message SMS$sms->{id} was delivered with status: $item->{status}($asg_status)");
	} else {
		$esme->warning("Unknown delivery status: $item->{messageDelivered}{status}",
					   "Unknown delivery status, ignored report");
	}
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

1;

__END__

wget --post-file teleaudio_sms_postdata --header="Content-Type: multipart/form-data; boundary=xYzZY" http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl
wget --post-file teleaudio_dlr_postdata --header="Content-Type: multipart/form-data; boundary=xYzZY" http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl
