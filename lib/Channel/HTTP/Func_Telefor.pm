#!/usr/bin/perl
package Channel::HTTP::Func_Telefor;
use strict;
use warnings;
use Channel::HTTP::HTTPUtils qw(http_response);
use Encode qw(encode decode);
use XML::Simple;

################################################################################
# Main API
################################################################################
sub init {
	my ( $proto, $esme ) = @_;
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $num = $q->param('bnumber') || ""; #'0690645045';
	my $abonent = $q->param('anumber') || "";
	my $msg = encode('cp1251',$q->param('content'));
	my $tr_id =  $q->param('id');
	my $operator = $q->param('operator');

	my $sms = { smsc_id => $esme->{smsc_id},
				num => $num,
				abonent => $abonent,
				msg => $msg,
				data_coding => 0,
				esm_class => 0,
				link_id => $operator,
				transaction_id => $tr_id};
	my $id = $esme->{db}->insert_sms_into_insms($sms);
	$esme->{log}->info("[$heap->{session_id}] SMS$id recieved from '$abonent' to '$num' with text '$msg'");
	$heap->{transaction_id} = $tr_id;
	$heap->{try} = 0;
 	return 5;
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;

	$heap->{try}++;
	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id},$heap->{transaction_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		my ($k) = keys(%$smss);
		my $v = $smss->{$k};

		my $xml = XMLout({ response => {content => decode('cp1251',$v->{msg})} },
						 KeepRoot => 1,
						 NoAttr => 1,
						 XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>' );
		$esme->{log}->debug("XML: $xml");
		$esme->{db}->set_sms_new_status($v->{id},2,0,0,0);
		$esme->{db}->set_sms_delivered_status($v->{id});
		$esme->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
		return http_response('200 OK', $xml,
							 content_type => 'text/xml',
							 charset => 'utf-8');
	}
	elsif ($heap->{try} >= 5) {
		$esme->warning("No response messages","No response messages");
		return http_response('200 OK', '',
							 content_type => 'text/plain',
							 charset => 'utf-8');
	}

	return 5;
}

sub rcv_on_routines {
	my ( $proto, $esme ) = @_;
	my $arr = $esme->{db}->{db}->selectall_arrayref('select id from tr_outsms where smsc_id=? and date < subdate(now(), interval 5 minute)',undef,$esme->{smsc_id});
	foreach (@$arr) {
		my $id = $_->[0];
		$esme->{db}->set_sms_new_status($id,255,0,0,0);
		$esme->{db}->set_sms_delivered_status($id,1);
	}
}

1;
