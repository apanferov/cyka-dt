#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
################################################################################
# OSMP
################################################################################
package Channel::HTTP::Func_OSMP;
use strict;
use warnings;
use Channel::HTTP::PriceToNum qw(load_num_prices);
use Channel::HTTP::HTTPUtils;

################################################################################
# Main API
################################################################################
sub init {
	my ( $proto, $esme ) = @_;
	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
	load_num_prices($esme);
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $command = $q->param('command') || "";
	my $txn_id = $q->param('txn_id') || "";
	my $account = $q->param('account') || "";
	my $sum = $q->param('sum') || "";

	# command check
	unless ($command =~ /^(pay|check)$/) {
		$esme->error("[$heap->{session_id}] Incorrect command '$command' ignored", "Incorrect OSMP command");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  "Incorect command '$command'" );
	}

	# txn_id check
	unless ($txn_id =~ /^\d{1,20}$/) {
		$esme->error("[$heap->{session_id}] Incorrect txn_id '$txn_id' ignored", "Incorrect OSMP transaction id");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  "Incorect txn_id format '$txn_id'" );
	}

	# lower than min sum check
	if ($sum < $esme->{min_price}) {
		$esme->error("$sum is lower than $esme->{min_price}", "OSMP sum not in range");
		return osmp_response( $txn_id,
							  241 );
	}

	# greater than max sum check
	if ($sum > $esme->{max_price}) {
		$esme->error("$sum is greater than $esme->{max_price}", "OSMP sum not in range");
		return osmp_response( $txn_id,
							  242 );
	}

	my $num = sum_to_num($esme,$sum);

	unless ($num) {
		$esme->error("num not exists for sum '$sum'", "OSMP - num not found");
		return osmp_response( $txn_id,
							  300,
							  undef,
							  'Cant convert sum to shortnum');
	}

	my ( $abonent, $msg ) = ($account, $q->param('pay_type') || ""); # +- vlad osmp_parse_account($account);

	# can we extract abonent number and message text
#	unless ($abonent) {
#		$esme->warning("Can't parse account '$account' to abonent and message text", "OSMP - incorrect account format");
#		return osmp_response( $txn_id,
#							  4 );
#	}

	# а񫞠�󡰥 衪᮷飠泱�
	if ($command eq 'check') {
		$esme->{log}->info("[$heap->{session_id}] Check passed for txn_id: '$txn_id'");
		return osmp_response( $txn_id,
							  0 );
	}

	my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $txn_id);
	my $insms_id;

	unless ($trans) {
		my $sms = { smsc_id => $esme->{smsc_id},
					num => $num,
					abonent => $abonent,
					msg => $msg,
					data_coding => 0,
					esm_class => 0,
					transaction_id => $txn_id };

		$insms_id = $esme->{db}->insert_sms_into_insms($sms);
		$trans = Disp::SMSCTransaction->new({ smsc_id => $esme->{smsc_id},
											  transaction_id => $txn_id,
											  insms_id => $insms_id,
											  date => today(),
											  status => 0,
											  exact_price => $sum,
											  parameters => {} });
		$trans->save;
		$esme->{log}->info("[$heap->{session_id}] SMS$insms_id recieved from '$abonent' to '$num' with text 'text'");
	}

	if ($trans->{status} == 3) {
		return osmp_response($txn_id,
							 0,
							 $trans->{insms_id},
							 'This payment alreadry handled',
							 $trans->{exact_price});
	}

	if ($trans->{status} == 1 or $trans->{status} == 2) {
		return osmp_response($txn_id,
							 300,
							 undef,
							 'This payment has canceled');
	}

	$heap->{trans} = $trans;
 	return 5;
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;

	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id},$heap->{trans}{transaction_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			$esme->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$esme->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");
			$esme->{db}->resend_through_smstraffic_with_num($v->{outbox_id}, $esme->{sms_traffic_smsc_id}, '1121');
			$esme->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$esme->{db}->set_sms_delivered_status($v->{id});

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return osmp_response($heap->{trans}{transaction_id},
								 0,
								 $heap->{trans}{insms_id},
								 "Payment accepted",
								 $heap->{trans}{exact_price});
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 5) {
		$esme->{log}->error("[$heap->{session_id}] No response for SMS$heap->{trans}{insms_id}, giving up");
		return osmp_response($heap->{trans}{transaction_id},
							 1,
							 undef,
							 'Service doesnt responded in time')
	}

	return 5;
}

sub sender_init {
}

sub sender_handle_routines {
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;
	my $db = $esme->{db};
	$db->set_sms_new_status($sms->{id},2,0,0,0);
	$db->set_sms_delivered_status($sms->{id},1);
	my $new_id = $db->resend_through_smstraffic($sms->{outbox_id}, 85);
	$esme->{log}->info("OUTBOX$sms->{outbox_id} was resent through 85 - new id is $new_id");
}

################################################################################
# 
################################################################################
sub osmp_response {
	my ( $txn_id, $result, $prv_txn, $comment, $sum ) = @_;

	my $data = { 'osmp_txn_id' => $txn_id,
				 'result' => $result };

	$data->{prv_txn} = $prv_txn if defined $prv_txn;
	$data->{comment} = $comment if defined $comment;
	$data->{sum} = $sum if defined $sum;

	my $xml = XMLout({response => $data},
					 KeepRoot => 1,
					 NoAttr => 1,
					 XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>' );

	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'utf-8');
}

sub osmp_parse_account {
	my ( $text ) = @_;
	if ($text =~ /^[78](\d{10})\/(.+)/) {
		return ( "7".$1, $2 );
	}
	return;
}


1;
