package Channel::HTTP::Func_A1A;
use strict;
use Data::Dumper;
use Channel::HTTP::HTTPUtils qw(http_response);
use LWP::UserAgent;
use HTTP::Request;
use URI::URL;

sub rcv_init
{
}

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	if ($db->get_value('select 1 from tr_inboxa where id=? and abonent=?',undef,$q->param('op_transaction_id'),$q->param('user_id'))) {
		my $yell_msg = "Endless loop detected. Message ".$q->param('transaction_id')." was rejected.";
		$esme->warning("$yell_msg\nA1 MSG".$q->param('op_transaction_id'),$yell_msg);
		return http_response('200 OK', "status: yell\n\n$yell_msg");
	}
	my $transaction_id = sprintf('%s:%d', $q->param('transaction_id'), $q->param('service_id'));
	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $transaction_id);

	if ($id) {
		$esme->warning("SMS $transaction_id was already saved to insms as $id, ignored", "Duplicate sms ignored");
	} else {
		my $msg = $q->param('msg');
		$msg  =~ s/\x0d?\x0a/|/g;
		my $sms = {
				   smsc_id        => $esme->{smsc_id},
				   num            => $q->param('num'),
				   abonent        => $q->param('user_id'),
				   msg            => $msg,
				   transaction_id => $transaction_id,
				   data_coding    => 0,
				   esm_class      => 0,
				   link_id        => $q->param('operator_id')
				  };
		my $id = $db->insert_sms_into_insms($sms);
		$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	return http_response('200 OK', "status: ignore\n\n_");
}

sub sender_init
{
}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $ua = LWP::UserAgent->new;
	$ua->timeout(20);

	my($transaction_id, $service_id) = split /:/, $sms->{transaction_id};
	my $url = url($esme->{url});

	my $msg = $sms->{msg};
	$msg  =~ s/\x0d?\x0a/|/g;
	$url->query_form(
		msg => sprintf("status: delayed-reply\ndestination: %s\ntransaction-id: %s\n\n%s",
			$sms->{abonent}, $transaction_id, $msg),
		service_id => $service_id);
	$log->debug($url);

	my $resp = $ua->get($url);

	my $reply = $resp->content();
	$log->debug($reply);

	if ($resp->code() eq '200' and $reply !~ /Service-id mismatch in delayed transaction/) {
		if ($reply =~ /^\d+$/) {
			$db->set_sms_new_status($sms->{id},2,0,0,$reply,1);
			$db->set_sms_delivered_status($sms->{id},0,1);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		}
		else {
			$db->set_sms_new_status($sms->{id},255,0,255,0,1);
			$db->set_sms_delivered_status($sms->{id},1);
			$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		}
	}
	else {
		my $retry_interval = 10 << ($sms->{retry_count} << 1);
		$db->set_sms_new_status($sms->{id},0,$retry_interval,255,0,1);

       	$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. ($reply)");
		$log->warn("Will try to resend in $retry_interval seconds.");
	}
}

sub sender_handle_routines { 0 }

1;
__END__
