#!/usr/bin/perl
package Channel::HTTP::HTTPUtils;
use strict;
use warnings;
our @EXPORT_OK = qw[http_response];

use base qw/Exporter/;

sub http_response {
	my ( $status, $body, %args ) = @_;

	my $status_line = "Status: $status";
	my $content_type = $args{content_type} || 'text/plain';
	$content_type .= "; charset=$args{charset}" if $args{charset};

	return [ "$status_line\r\nContent-type: $content_type\r\n\r\n$body" ];
}

1;
