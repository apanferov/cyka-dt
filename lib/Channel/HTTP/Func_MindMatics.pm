#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_MindMatics; # smsc_id = ?
use strict;
use Data::Dumper;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);
use URI::Escape;

# �������:
# RECIVED => 2
# ACCEPTD => 11
# DELIVRD => 12
# REJECTD => 13
# EXPIRED => 14
# UNDELIV => 15

my $status_tab = {
	0 => 12, # DELIVERED
	1003 => 13, # UNDELIVERABLE
	1001 => 14, # EXPIRED
	1002 => 13, # DELETED
	1004 => 13, # REJECTED
	1005 => 13, # UNKNOWN
};

sub sender_init {
#  my ($proto, $esme) = @_;
} 

sub sender_handle_routines {
#  my ($proto, $esme) = @_;
} 

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	if ($q->param('transactionid')) {
		# MO
		my $msg;
		if ($q->param('coding')) {
			$msg = encode('cp1251', decode($q->param('charset'), $q->param('content')));
		} else {
			$msg = encode('cp1251', decode('ISO-8859-1', $q->param('content')));
		}

		my $sms = {
			smsc_id        => $esme->{smsc_id},
			abonent        => $q->param('from'),
			num            => $q->param('to'),
			msg            => $q->param('content'),
			data_coding    => 0,
			esm_class      => 0,
			transaction_id => $q->param('transactionid').':'.($q->param('coding') || 'ISO-8859-1'),
			link_id        => $q->param('from_operator') || $q->param('from_serviceprovider')
		};

		my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
		if ($id) {
			$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
		} else {
			$id = $db->insert_sms_into_insms($sms,($q->param('from_country') eq 'MO') ? 0 : 1);
			$log->debug(Dumper($sms));
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
		}

		return { content => http_response(200, 'OK') };
	} else {
		my $xml;
		eval {
			$xml = XMLin($q->param('POSTDATA'), KeepRoot => 0);
		};
		if ($@) { return { content => http_response(500, "Unknown request") } };
 		$log->debug($xml);

		if ($xml->{guid} && $xml->{myid} && $xml->{status} && $xml->{msisdn}) {
			# DR (Payment - MT Payment Notification - "��������" = "����������")

			my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $xml->{guid});
			if ($outsms && $xml->{status}) {
				my $status = (lc($xml->{status}) == 'success') ? 12 : 13;
				$log->info("Report for message SMS$outsms->{id} was received. Status: $xml->{status}.");
				$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status, undef, $outsms);
					$esme->outbox_status_changed($outsms);
				$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1) if ($status == 12);
				$db->redis_report($outsms, {}) if ($outsms->{registered_delivery});

				return { content => http_response(200, 'OK') };
			}
		}

# 		# DR (XML)
# 		if ($xml->{result} && $xml->{msgid}) {
# 			my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $xml->{msgid});
# 			if ($outsms && defined($xml->{errorcode})) {
# 		 		$log->info("Report for message SMS$outsms->{id} was received.")
#
# 				my $status = $status_tab{$xml->{errorcode}} || 13;
# 				my $errorcode = $xml->{errorcode};
# 				if {$errorcode} {
# 					my $detailederror = $xml->{detailederror} || '<no detailederror>';
# 					my $errormessage = $xml->{errormessage} || '<no errormessage>';
#
# 					$log->info("Error detected: errorcode = $errorcode, detailederror = $detailederror, errormessage = $errormessage.");
# 				}
# 				$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status, undef, $outsms);
# 					$esme->outbox_status_changed($outsms);
# 				$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1) if ($status == 12);
# 				$db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
#
# 				return { content => http_response(200, 'OK') };
# 			}
# 		}
#
#		$esme->warning("Unknown request\n".Dumper($q));
	}

	return { content => http_response(500, "Unknown request") };
}

sub sender_handle_sms {
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my($transaction_id, $charset) = split /:/, $sms->{transaction_id};
	my $msg = ($sms->{data_coding} == 0)
		? $sms->{msg}
		: encode($charset, decode('UCS-2', $sms->{msg}));
	$msg = xml_escape($sms->{msg});

	my $req = {
		version => '3.1',
		cid => $esme->{system_id},
		password => $esme->{password},
		transactionid => $transaction_id,
		from => $sms->{num},
		to => $sms->{abonent},
		content => $msg,
		coding => $charset,
		myid => $sms->{id},
		notification => 1,
	};
	$log->debug(Dumper($req));

	my $ua = LWP::UserAgent->new;
	$ua->timeout(20);
	my $resp = $ua->post($esme->{url}, $req);
	my $reply = $resp->content();
	$log->debug($reply);

	my $is_send = 0;
	if ($resp->code() eq '200') {
		my $xml = XMLin($reply, KeepRoot => 0);
		$log->debug($xml);
		if (lc($xml->{result}) eq 'success') {
			my $msgid = $xml->{msgid} || '<no msgid for SMS$sms->{id}>';
			$db->set_sms_new_status($sms->{id},2,0,0,$msgid,1);
			$db->set_sms_delivered_status($sms->{id},1,1);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");

			$is_send = 1;
		} else {
			my $errorcode = $xml->{errorcode} || '<no errorcode>';
			my $errormessage = $xml->{errormessage} || '<no errormessage>';
			$log->warn("Error detected: errorcode = $errorcode, errormessage = $errormessage");
		}
	}
	unless ($is_send) {
		$db->set_sms_new_status($sms->{id},255,0,255,0,1);
		$db->set_sms_delivered_status($sms->{id},1);
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
	}
}

sub xml_escape {
	my ( $data ) = @_;
	$data =~ s/&/&amp;/sg;
	$data =~ s/</&lt;/sg;
	$data =~ s/>/&gt;/sg;
	$data =~ s/"/&quot;/sg; #"
	return $data;
}

1;
__END__

Sample 1: Premium inbound message forwarding for MO billing.
============================================================
https://yourURL/yourScripts/
?transactionid=109889937
&expirytime=2010-05
30T15:59:40+02:00
&billingtype=MO
&paymentstatus=SUCCESS
&amount=2.99
&currency=EUR
&guid=34BFC5FE99CEEFEE35DD7FF2DADE9498
&content=YOUR%20PIN%20is%20234
&to=83222
&to_country=DE
&from=00491703131416
&from_country=DE
&from_operator=D1
&from_serviceprovider=D1
&coding=ISO-8859-1
&timestamp=2010-05-30T15:36:25+02:00

Sample 2: Premium inbound message forwarding for MT billing.
============================================================
https://yourURL/yourScripts/
?transactionid=109889937
&expirytime=2010-05-30T15:59:40+02:00
&billingtype=MT
&paymentstatus=OPEN
&amount=2.99
&currency=EUR
&guid=34BFC5FE99CEEFEE35DD7FF2DADE9498
&content=YOUR_PIN_is_234
&to=83222
&to_country=DE
&from=00491703131416
&from_country=DE
&from_operator=D1
&from_serviceprovider=D1
&coding=ISO-8859-1
&timestamp=2010-05-30T15:36:25+02:00

Sample 3: Reply message for MT billing.
=======================================
https://smsserver.mindmatics.com/messagegateway/outbound/sms
?cid=1234
&password=secret
&version=3.1
&from=48232
&to=0039111456686
&content=Thank%20you%20for%20using%20MOPAY
&transactionid=109970021
&myid=MyShop

Sample 4: Reply message for MO billing.
=======================================
https://smsserver.mindmatics.com/messagegateway/outbound/sms?cid=1234
&password=secret
&version=3.1
&from=48232
&to=0039111456686
&content=Thank%20you%20for%20using%20MOPAY
&transactionid=109970021
&myid=MyShop

Sample 5: Unbilled reply message for MO and MT billing
======================================================
https://smsserver.mindmatics.com/messagegateway/outbound/sms?cid=1234
&password=secret
&version=3.1
&from=48232
&to=0039111456686
&content=Thank%20you%20for%20using%20MOPAY
&transactionid=109970021
&command=free
&myid=52413524

Sample 6: Successful Delivery Response
======================================
<?xml version=�1.0� encoding=�ISO-8859-1�?>
<smsreport>
<date>2007-05-27 14:00:12 +0200</date>
<result>success</result>
<msgid>2D67C865FB6FA25A9261C75E8D2F0F2B </msgid>
<msgparts>1</msgparts>
</smsreport>

Sample 7: Unsuccessful Delivery Response
========================================
<?xml version=�1.0� encoding=�ISO-8859-1�?>
<smsreport>
<date>2007-05-27 14:05:12 +0200</date>
<result>error</result>
<errorcode>-70</errorcode>
<errormessage>maximum content length exceeded</errormessage>
</smsreport>

Sample 8: SMS Delivery Notification Successful
==============================================
<?xml version="1.0" encoding="ISO-8859-1"?>
<notificationreport >
<date>07.01.2003 17:50:55</date>
<result>success</result>
<msgid>0C9270F5B00ACD01DC3F4AB28DCD1442</ msgid >
<myid>ABC1234DE</myid>
<cid>1234</cid>
<recnbr>00491712345678</recnbr>
</notificationreport >

Sample 9: SMS Delivery Notification Failed
==========================================
<?xml version="1.0" encoding="ISO-8859-1"?>
<notificationreport>
<date>07.01.2003 17:51:23</date>
<result>error</result>
<msgid>0C9270F5B00ACD01DC3F4AB28DCD1442</msgid>
<myid>ABC1234DE</myid>
<cid>1224</cid>
<recnbr>00491712345678</recnbr>
<errorcode>1001</errorcode>
<detailederror>007</ detailederror >
<errormessage>sms timeout</errormessage>
</notificationreport>
