#!/usr/bin/perl
################################################################################
# Cyberplat
################################################################################
package Channel::HTTP::Func_CyberPlat;
use strict;
use warnings;
use Channel::HTTP::PriceToNum;
use Channel::HTTP::HTTPUtils;

################################################################################
# Main API
################################################################################
sub init {
	my ( $proto, $esme ) = @_;

	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
	load_num_prices($esme);
}

my %cyberplat_action_handlers = ( check => \&cyberplat_action_check,
								  payment => \&cyberplat_action_payment,
								  cancel => \&cyberplat_action_cancel,
								  status => \&cyberplat_action_status
								);

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $action  = $q->param('action');
	my $handler = $cyberplat_action_handlers{$action};
	if ($handler) {
		return $handler->($esme, $heap, $q);
	} else {
		return cyberplat_response(1, "����������� ��� �������");
	}
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id},$heap->{txn_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			$esme->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$esme->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");
			$esme->{db}->resend_through_smstraffic($v->{outbox_id}, $esme->{sms_traffic_smsc_id});
			$esme->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$esme->{db}->set_sms_delivered_status($v->{id});

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return cyberplat_response(0,
									  "����� ���������",
									  authcode => $heap->{trans}{insms_id},
									  date => cyber_date($heap->{trans}{date}));
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 7) {
		$esme->error("[$heap->{session_id}] No response for SMS$heap->{trans}{insms_id}, giving up",
					 "No service response in given time");
		return cyberplat_response(10,
								  "��������� �������, ������ ������ ���� �������� �����");
	}

	return 5;
}

################################################################################
# 
################################################################################
sub cyberplat_action_check {
	my ( $esme, $heap, $q ) = @_;

	my $number = $q->param('number');
	my ( $abonent, $text ) = cyberplat_split_number($number);
	my $sum = $q->param('amount');

	unless ($abonent) {
		$esme->warning("Can't extract abonent and text: $number", "Incorrect abonent format");
		return cyberplat_response(2, "������������ �������");
	}

	my $num = sum_to_num($esme,$sum);
	unless ( $num ) {
		$esme->warning("Unknown sum: $sum", "Incorrect sum");
		return cyberplat_response(3, "������������ �����");
	}

	$esme->{log}->info("[$heap->{session_id}] Check passed: number - '$number', amount - '$sum'; converted to num - '$num'");

	return cyberplat_response(0, "������ �� ������");
}

sub cyberplat_action_payment {
	my ( $esme, $heap, $q ) = @_;

	my $number = $q->param('number');
	my ( $abonent, $text ) = cyberplat_split_number($number);
	my $sum = $q->param('amount');

	unless ($abonent) {
		$esme->warning("Can't extract abonent and text: $number", "Incorrect abonent format");
		return cyberplat_response(2, "������������ �������");
	}

	my $num = sum_to_num($esme,$sum);
	unless ( $num ) {
		$esme->warning("Unknown sum: $sum", "Incorrect sum");
		return cyberplat_response(3, "��� ������� ������ ��������� ��������� ������ ������������� �����");
	}

	my $txn_id = $q->param("receipt");
	unless ($txn_id =~ /^\d+$/) {
		return cyberplat_response(4, "���� receipt ����� �������� ������");
	}

	my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $txn_id);
	my $insms_id;

	unless ($trans) {
		my $sms = { smsc_id => $esme->{smsc_id},
					num => $num,
					abonent => $abonent,
					msg => $text,
					data_coding => 0,
					esm_class => 0,
					transaction_id => $txn_id };
		$insms_id = $esme->{db}->insert_sms_into_insms($sms);
		$trans = Disp::SMSCTransaction->new({ smsc_id => $esme->{smsc_id},
											  transaction_id => $txn_id,
											  insms_id => $insms_id,
											  date => today(),
											  status => 0,
											  exact_price => $sum,
											  parameters => {} });
		$trans->save;
		$esme->{log}->info("[$heap->{session_id}] SMS$insms_id recieved from '$abonent' to '$num' with text 'text'");
	}

	if ($trans->{status} == 3) {
		return cyberplat_response(0,
								  "���� ����� ��� ��� ������� �������",
								  authcode => $trans->{insms_id},
								  date => cyber_date($trans->{date}));
	}

	if ($trans->{status} != 0) {
		$esme->error("Payment $txn_id has incorrect status '$trans->{status}'",
					 "Incorrect transaction status");
		return cyberplat_response(11,
								  "����� � ���������� ���������");
	}

	$heap->{trans} = $trans;
	$heap->{txn_id} = $txn_id;

	return 5;
}

sub cyberplat_action_cancel {
	my ( $esme, $heap, $q ) = @_;

	my $receipt = $q->param('receipt');
	my $mes = $q->param('mes');

	my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $receipt);

	unless ($trans) {
		$esme->warning("Can't load transaction '$receipt'", "cancel for unknown receipt");
		return cyberplat_response(4, "����� ���������� ��� � ����");
	}

	if ($trans->{status} == 1 or $trans->{status} == 2) {
		return cyberplat_response(0, "����� ��� ��� �������",
								  authcode => $trans->{insms_id},
								  date => cyber_date($trans->{date}));
	}

	$trans->{status} = 1;
	$trans->{parameters}{cancel_reason} = $mes;
	$trans->save;

	$esme->{log}->info("[$heap->{session_id}] Canceled transaction $receipt");

	return cyberplat_response(0, "����� �������",
							  authcode => $trans->{insms_id},
							  date => cyber_date($trans->{date}));
}

sub cyberplat_action_status {
	my ( $esme, $heap, $q ) = @_;

	my $receipt = $q->param('receipt');
	my $mes = $q->param('mes');

	my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $receipt);

	unless ($trans) {
		$esme->warning("Can't load transaction '$receipt'", "cancel for unknown receipt");
		return cyberplat_response(4, "����� ���������� ��� � ����");
	}

	if ($trans->{status} == 0) {
		return cyberplat_response(8, "��������� ������� ������������");
	}

	if ($trans->{status} == 3) {
		return cyberplat_response(0,
								  "����� ������� �������",
								  date => cyber_date($trans->{date}),
								  authcode => $trans->{insms_id} );
	}

	return cyberplat_response(7, "����� �������",
							  date => cyber_date($trans->{date}),
							  authcode => $trans->{insms_id} );

}

# helpers
sub cyber_date {
	my ( $string ) = @_;

	$string =~ s/\s/T/;
	return $string;
}

sub cyberplat_split_number {
	my ( $string ) = @_;
	return unless $string =~ /^(7\d{10})\/(.+)/;

	return ( $1, $2 );
}

sub cyberplat_response {
	my ( $code, $message, %args ) = @_;

	my %data = ( code  => $code,
				 message => $message );

	@data{keys %args} = values %args;

	my $data = \%data;

	my $xml = XMLout({response => $data},
					  KeepRoot => 1,
					  NoAttr => 1,
					  XMLDecl => 1 );

	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'utf-8');
}


1;