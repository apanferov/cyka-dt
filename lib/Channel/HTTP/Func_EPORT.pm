#!/usr/bin/perl
package Channel::HTTP::Func_EPORT;
use strict;
use warnings;
use Channel::HTTP::PriceToNum;
use Channel::HTTP::HTTPUtils;

################################################################################
# E-port
################################################################################
sub init {
	my ( $proto, $esme ) = @_;

	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};

	$esme->{our_rsa_private_key} = Crypt::OpenSSL::RSA->new_private_key
	  ( read_file(Config->param('system_root')."/etc/rsa_private_$esme->{smsc_id}.key") );

	$esme->{remote_rsa_public_key} = Crypt::OpenSSL::RSA->new_public_key
	  ( read_file(Config->param('system_root')."/etc/rsa_public_$esme->{smsc_id}.key") );

	# $esme->{remote_rsa_public_key}->use_md5_hash;

	load_num_prices(Esme);
}

my $eport_date_regexp = qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[-+]\d{2}$/;
my $eport_id_regexp = qr/^\d+$/;
my $eport_sum_regexp = qr/^\d+\.\d{2}$/;
my $eport_account_regexp = qr/^7\d{10}$/;

my %eport_requests_validators = ( payment => [ id => $eport_id_regexp,
											   pay_time => $eport_date_regexp,
											   account => $eport_account_regexp,
											   sum => $eport_sum_regexp,
											   service => qr/^./,
											   timestamp => $eport_date_regexp ],
								  check => [ account => qr/^\d+$/,
											 service => qr/^./,
											 sum => $eport_sum_regexp,
											 timestamp => $eport_date_regexp ],
								  status => [ id => $eport_id_regexp,
											  timestamp => $eport_date_regexp ],
								  revoke => [ id => $eport_id_regexp,
											  timestamp => $eport_date_regexp ]
								);

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my %params = $q->Vars;

	unless ($esme->eport_request_validate(\%params)) {
		return $esme->eport_response(\%params,
									 { result => 'E1',
									   reason => 'Not all mandatory fields present or have apropriate value' });
	}

	my $sign_format_valid = 0;

	unless ($params{sign} and ($params{sign} =~ /^[0-9A-Z]+$/i) and (length($params{sign}) % 2 == 0)) {
		return $esme->eport_response(\%params,
									 { result => 'E1',
									   reason => 'No signature param or invalid signature format' });
	}

	# �������� �������
	my ($sign_valid, $sign_comment) = $esme->eport_sign_validate(\%params);
	unless ($sign_valid) {
		$esme->error("Can't validate signature: \n$sign_comment\nRequest is: ".Dumper(\%params), "Problem while validating signature");
		return $esme->eport_response(\%params,
									 { result => 'E3',
									   reason => 'Invalid signature' });
	}

	if ($params{type} =~ /^(check|payment)/) {

		my $abonent = $params{account};
		my $msg = $params{service};

		if ($abonent !~ /^7(9265550017|9261193892)$/ or $msg !~ /^\s*\d{3}\s*$/) {
			$abonent = undef;
		}

		unless ($abonent) {
			$esme->warning("Invalid account '$params{account}'/'$params{service}'",
						   "Invalid account string format");
			return $esme->eport_response(\%params,
										 { result => 'F3',
										   reason => 'Invalid account' });
		}

		# ����� � �������� ��������
		if ($params{sum} < $esme->{min_price}) {
			$esme->warning("Price '$params{sum}' lower than minimum '$esme->{min_price}'",
						   "Price problem: sum lower than minimum");
			return $esme->eport_response(\%params,
										 { result => 'F1',
										   reason => 'Price is less then allowed' });
		}

		if ($params{sum} > $esme->{max_price}) {
			$esme->warning("Price '$params{sum}' greater than maximum '$esme->{max_price}'",
						   "Price problem: sum greater than maximum");
			return $esme->eport_response(\%params,
										 { result => 'F2',
										   reason => 'Price is greater then allowed' });
		}

		# ����� ������������� � �������� �����
		my $num = sum_to_num($esme,$params{sum});
		unless ($num) {
			$esme->warning("Can't convert sum '$params{sum}' to short number",
						   "Price problem: cant convert sum to num");
			return $esme->eport_response(\%params,
										 { result => 'F4',
										   reason => 'Incorrect price' });
		}

		# ��� �������� check �� ���� �� �������������
		if ($params{type} eq 'check') {
			$esme->{log}->debug("Check passed: num-$num, abonent-$abonent, text-$msg for params:\n".Dumper(\%params));
			return $esme->eport_response(\%params,
										 { result => 'S1',
										   reason => 'All fields looks correct' });
		}

		unless ($params{id} =~ /^\d+$/) {
			$esme->warning("Incorrect 'id' format: $params{id}",
						   "Incorrect 'id' format");
			return $esme->eport_response(\%params,
										 { result => 'E1',
										   reason => 'Invalid id format' });
		}

		my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $params{id});
		my $insms_id;

		unless ($trans) {
			my $sms = { smsc_id => $esme->{smsc_id},
						num => $num,
						abonent => $abonent,
						msg => $msg,
						data_coding => 0,
						esm_class => 0,
						transaction_id => $params{id}
					  };

			eval {
				$insms_id = $esme->{db}->insert_sms_into_insms($sms);
				$trans = Disp::SMSCTransaction->new({ smsc_id => $esme->{smsc_id},
													  transaction_id => $params{id},
													  insms_id => $insms_id,
													  status => 0,
													  date => today(),
													  exact_price => $params{sum},
													  parameters => { pay_time => $params{pay_time},
																	  account => $params{account},
																	  sum => $params{sum},
																	  timestamp => $params{timestamp}
																	}
													});
				$trans->save;
			};

			if ($@) {
				$esme->error("Can't store message to DB: $@", "SMS insertion failed");
				eval { Config->dbh->rollback; };
				return $esme->eport_response(\%params,
											 { result => 'E2',
											   reason => 'Couldnt store to DB' });
			}

			$esme->{log}->info("[$heap->{session_id}] SMS$insms_id recieved: from '$abonent' to '$num' with text '$msg'");
		}

		if ($trans->{status} == 3) {
			return $esme->eport_response(\%params,
										 { result => 'F5',
										   reason => 'Already handled' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $esme->eport_response(\%params,
										 { result => 'F7',
										   reason => 'Already handled and revoked' });
		}

		$heap->{insms_id} = $insms_id;
		$heap->{trans} = $trans;
		$heap->{params} = \%params;

		return 5;

	} elsif ($params{type} eq 'status') {
		my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $params{id});

		unless ($trans) {
			return $esme->eport_response(\%params,
										{ result => 'F6',
										  reason => 'No such transaction' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $esme->eport_response(\%params,
										{ result => 'S2',
										  pay_time => $trans->{parameters}{pay_time},
										  accepted => eport_date($trans->{date}),
										  reason => 'Transaction cancelled',
										  account => $trans->{parameters}{account},
										  sum => $trans->{parameters}{sum},
										  revoked => $trans->{parameters}{revoke_date},
										});
		}

		if ($trans->{status} == 3) {
			return $esme->eport_response(\%params,
										{ result => 'S1',
										  pay_time => $trans->{parameters}{pay_time},
										  reason => 'Transaction handled successfully',
										  account => $trans->{parameters}{account},
										  sum => $trans->{parameters}{sum},
										  accepted => eport_date($trans->{date}),
										});
		}
		return $esme->eport_response(\%params,
									 { result => 'E4',
									   reason => 'Status not known yet' });
	} elsif ($params{type} eq 'revoke') {
		my $trans = Disp::SMSCTransaction->load($esme->{smsc_id}, $params{id});

		unless ($trans) {
			return $esme->eport_response(\%params,
										{ result => 'F6',
										  reason => 'No such transaction' });
		}

		if ($trans->{status} == 1 or $trans->{status} == 2) {
			return $esme->eport_response(\%params,
										{ result => 'F7',
										  reason => 'Already revoked' });
		}

		if ($trans->{status} == 3) {
			$trans->{status} = 1;
			$trans->{parameters}{revoke_date} = eport_date(today());

			$trans->save;

			return $esme->eport_response(\%params,
										{ result => 'S2',
										  reason => 'Transaction revoked',
										  revoked => $trans->{parameters}{revoke_date},
										});
		}
		return $esme->eport_response(\%params,
									 { result => 'E4',
									   reason => 'Processing not completed yet' });
	}

	my $resp =  $esme->eport_response(\%params,
									  { result => 'E1',
										reason => 'Unsupported' });
	return $resp;
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;

	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $smss = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id},$heap->{trans}{transaction_id});
	my $smc = scalar keys(%$smss);

	if ($smc > 0) {
		# billing insms_count, cyka_processed update
		# mark transaction as completed

		# Undefined behaviour when more than one SMS
		# But it's not our problem =)
		while (my ($k,$v) = each %$smss) {

			# $esme->{db}->resend_through_smstraffic($v->{outbox_id}, $esme->{sms_traffic_smsc_id});
			$esme->{db}->set_sms_new_status($v->{id},2,0,0,0);
			$esme->{db}->set_sms_delivered_status($v->{id});
			$esme->{log}->info("[$heap->{session_id}] SMS$k delivered to SMSC");
			$esme->{log}->info("[$heap->{session_id}] OUTBOX$v->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");

			$esme->{log}->debug("NUM is $v->{num}");

			$heap->{trans}{status} = 3;
			$heap->{trans}->save;

			return $esme->eport_response($heap->{params},
										 { result => ($v->{num} eq 'free' ? 'F4' : 'S1'),
										   reason => 'Successfully handled',
										   accepted => eport_date($heap->{trans}{date}) });
		}
	}

	$heap->{check_count}++;
	if ($heap->{check_count} > 7) {
		$esme->error("[$heap->{session_id}] No response for SMS$heap->{insms_id}, giving up",
					 "No response created in given timeframe");
		return $esme->eport_response($heap->{params},
									 { result => 'E2',
									   reason => 'No response created in given timeframe' });
	}

	return 5;
}





sub eport_request_validate {
	my ( $esme, $params ) = @_;

	my $validator = $eport_requests_validators{$params->{type}};
	return unless $validator;

	my $i = 0;
	while ($i < $#$validator) {
		my ( $key, $reg ) = ( $validator->[$i], $validator->[$i+1]);

		unless ( $params->{$key} =~ $reg ) {
			$esme->{log}->error("$key param invalid");
			return;
		}
		$i += 2;
	}
	return 1;
}

sub eport_response {
	my ( $esme, $orig, $params ) = @_;

	my %new_req = %$params;

	for (keys %$orig) {
		$new_req{$_} = $orig->{$_};
	}

	my $normalized = eport_normalize_request_string(\%new_req);
	my $sign = unpack("H*", $esme->{our_rsa_private_key}->sign($normalized));

	return http_response('200 OK', "$normalized&sign=$sign",
						 content_type => 'application/x-www-form-urlencoded',
						 charset => 'windows-1251');
}

sub eport_sign_validate {
	my ( $esme, $params ) = @_;


	$esme->{log}->debug("OUR NORM: ".eport_normalize_request_string($params));

	my $valid;
	eval {
		$valid = $esme->{remote_rsa_public_key}->verify(eport_normalize_request_string($params),
														pack("H*", $params->{sign}));
	};
	if ($@) {
		return ( undef, $@ );
	}
	return ($valid, "");
}

sub eport_sign_make {
	my ( $esme, $params ) = @_;
}

sub eport_normalize_request_string {
	my ( $params ) = @_;
	my @out;
	foreach my $par (sort grep { $_ ne 'sign' } keys %$params  ) {
		my $tmp = $params->{$par};
		my $char_code;
		push @out, $par . "=" . eport_escape_string($params->{$par});
	}
	return join '&', @out;
}

my %eport_escape_tab;

BEGIN {
	for (0..255) {
		my $chr = chr;
		if ($chr =~ /[-_\.A-Za-z0-9]/) {
			$eport_escape_tab{$chr} = $chr;
		} else {
			$eport_escape_tab{$chr} = '%'.uc(unpack("H*", $chr));
		}
	}
# 	print Dumper(\%eport_escape_tab);
}

sub eport_escape_string {
	my ( $string ) = @_;

	join '', map {$eport_escape_tab{$_}} split //, $string;
}

sub eport_unescape_string {
	my ( $string ) = @_;

	$string =~ s/%([0-9A-F]{2})/chr(hex($1))/gei;
	return $string;
}

sub eport_split_account {
	my ( $string ) = @_;

	return unless $string =~ /^(7\d{10})\/(\d{3})/;

	return ( $1, $2 );
}

sub eport_date {
	my ( $date ) = @_;
	$date =~ s/\s/T/;
	$date .= "+03";
	return $date;
}


1;