package Channel::HTTP::Func_Life;
use strict;
use Data::Dumper;
use XML::Simple;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);


sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;

	my $xml = XMLin($q->param('POSTDATA'), ForceContent => 0);
	$esme->{log}->debug(Dumper($xml));
	my $sms = {
		smsc_id        => $esme->{smsc_id},
		abonent        => $xml->{params}->{param}->[0]->{value}->{string},
		num            => $xml->{params}->{param}->[1]->{value}->{string},
		msg            => encode('cp1251', $xml->{params}->{param}->[3]->{value}->{string}),
		data_coding    => 0,
		esm_class      => 0,
		transaction_id => $heap->{session_id}
	};
	$esme->{log}->debug(Dumper($sms));

	my $id = $esme->{db}->insert_sms_into_insms($sms);
	$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

	$heap->{transaction_id} = $sms->{transaction_id};
	$heap->{try} = 0;
	return 5; # First try in 5 seconds
}

sub handle_timer
{
	my($pkg, $esme, $heap) = @_;

	$heap->{try}++;
	$esme->{log}->debug("[$heap->{session_id}] Fetching...");

	my $msgs      = $esme->{db}->get_sms_ready_to_send_tr($esme->{smsc_id}, $heap->{transaction_id});
	my $msg_count = scalar keys(%$msgs);
	$esme->{log}->debug("$msg_count message(s) fetched.");

	my $rv = 10; # Retry in 10 seconds
	if ($msg_count > 0)
	{
		my(undef, $sms)    = each %$msgs;
		$heap->{outsms_id} = $sms->{id};

		my $output = encode('utf-8',
			decode('cp1251',
				XMLout({params => {'_bug_'=>[], param => {'_bug_'=>[], value => {string => $sms->{msg} }}}},
					XMLDecl => '<?xml version="1.0" encoding="utf-8"?>',
					RootName => 'methodResponse',
					NoAttr => 1)
				)
			);

		$esme->{log}->debug("[$heap->{session_id}]\n$output");
		$rv = http_response(200, $output, content_type => 'text/xml', charset => 'utf-8');

		$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
		$esme->{db}->set_sms_delivered_status($sms->{id});
		$esme->{log}->info("[$heap->{session_id}] SMS$sms->{id} was delivered to SMSC");
	}
	elsif ($heap->{try} >= 60) # 10*59+5= aproximently 10 minutes
	{
		$esme->{log}->debug("[$heap->{session_id}] Timed out...");
		$rv = sub{'Timed out'};
	}

	return $rv;
}

sub rcv_on_routines {
	my ($pkg, $esme) = @_;
	my $a = $esme->{db}->{db}->selectall_arrayref('select id from tr_outsms where smsc_id=? and date < subdate(now(), interval 1 minute)',
		undef, $esme->{smsc_id});

	foreach (@$a) {
		my $id = $_->[0];
		$esme->{db}->set_sms_new_status($id,255,0,0,0);
		$esme->{db}->set_sms_delivered_status($id,1);
	}
}

1;
