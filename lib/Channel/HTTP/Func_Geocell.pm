package Channel::HTTP::Func_Geocell;
use strict qw(vars);
use Data::Dumper;
use Encode qw(encode decode);
use URI::URL;
use Channel::HTTP::HTTPUtils qw(http_response);
use POSIX qw(strftime);

my $src_tab = {'8012' => '45321',
			   '8013' => '45322',
			   '8014' => '45323' };

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

    my $sms = {smsc_id => $esme->{smsc_id},
			   num => $q->param('BNMB'),
			   abonent => $q->param('ANMB'),
			   msg => $q->param('SMST'),
			   transaction_id => $q->param('SMSID'),
			   data_coding => 0,
			   esm_class => 0,
			  };
	$esme->{log}->debug(Dumper($sms));

	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
	if ($id) {
		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	}
	else {
		$id = $esme->{db}->insert_sms_into_insms($sms);
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	return http_response('200 OK', '');
}


sub sender_init {}

sub sender_handle_sms
{
	my($pkg, $esme, $sms) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});
	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if $esme->{local_addr};
	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $url = url($esme->{url});
	my %req = (src => $src_tab->{$sms->{num}},
			   dst => $sms->{abonent},
			   txt => $sms->{msg}
			  );

	$url->query_form(%req);
	$log->debug($url);
	my $resp = $ua->get($url);
	my $reply = $resp->content();
	$log->debug("Response: $reply");
	if ($resp->code() eq '200') {
		$db->set_sms_new_status($sms->{id},2,0,0,0,1);
		$db->set_sms_delivered_status($sms->{id},0,1);
		$log->info("Message SMS$sms->{id} was delivered to SMSC.");
	}
	else {
        $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Will try to resend in 10 minutes.");
	}
	return;
}

sub sender_handle_routines { 0 }

1;
__END__
