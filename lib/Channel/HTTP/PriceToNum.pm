#!/usr/bin/perl
package Channel::HTTP::PriceToNum;
use strict;
use warnings;

use Data::Dumper;

our @EXPORT_OK = qw[load_num_prices sum_to_num];
use base qw/Exporter/;


################################################################################
# Price to num conversions
################################################################################
sub load_num_prices {
	my ( $esme ) = @_;

	my $prices = $esme->{db}{db}->selectall_arrayref("select * from set_real_num_price where date_start <= date(now()) and price_in > 0 and operator_id = ? and mt",
													 {Slice => {}},
													 $esme->{operator_id});

	$esme->{log}->info("Loading prices for $esme->{operator_id}");

	$esme->{min_price} = 1000000;
	$esme->{max_price} = 0;

	foreach my $pr (@$prices) {
		$esme->{log}->debug("Price for $pr->{num} is $pr->{price_in}");
		if ($pr->{price_in} > $esme->{max_price}) {
			$esme->{max_price} = $pr->{price_in};
		}
		if ($pr->{price_in} < $esme->{min_price}) {
			$esme->{min_price} = $pr->{price_in};
		}
	}

	$esme->{prices} = $prices;
	$esme->{log}->debug(Dumper($esme->{prices}));
}

sub sum_to_num {
	my ( $esme, $sum ) = @_;

	my $delta = 1000000;
	my $found;

	foreach my $pr (@{$esme->{prices}}) {
		my $test = $sum - $pr->{price_in};

		next if $test < 0;

		if ($test < $delta) {
			$delta = $test;
			$found = $pr;
		}
	}

	if ($found) {
		return $found->{num};
	}

	return;
}


1;
