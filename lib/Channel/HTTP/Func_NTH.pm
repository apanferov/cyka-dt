#!/usr/bin/perl
package Channel::HTTP::Func_NTH;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI::URL;
use Channel::HTTP::HTTPUtils qw/http_response/;

my %price_tab = ('82555' => 2);

sub init {
	my ( $proto, $esme ) = @_;
	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if ($esme->{local_addr});
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;
	
	if ($q->param('command') eq 'deliverMessage') {
		handle_new_sms($esme, $heap, $q);
	} elsif ($q->param('command') eq 'deliverReport') {
		handle_new_dr($esme, $heap, $q);
	} else {
		$esme->error(Dumper($q), "Invalid request");
	}
	return http_response('200 OK','',content_type => 'text/plain',charset => 'utf-8');
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	my $req = sender_prepare_request($proto, $esme, $sms);
	
	my $ua = new LWP::UserAgent;
	$ua->timeout(180);
	my $url = url($esme->{url});
	$url->query_form(%$req);
	$esme->{log}->debug($url);
	my $resp = $ua->get($url);

	return sender_handle_response($proto, $esme, $sms, $resp);
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}

###########
# INTERNAL
###########
sub handle_new_sms {
	my ( $esme, $heap, $q, $parsed ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $transaction_id	= $q->param('sessionId'),
	my $short_num		= $q->param('businessNumber');
	my $abonent		= $q->param('msisdn');
	my $text		= $q->param('content'); # keyword ???
	my $operator		= $q->param('operatorCode');


	my $sms = { smsc_id => $esme->{smsc_id},
				transaction_id => $transaction_id,
				num => $short_num,
				abonent => $abonent,
				msg => encode('cp1251',decode('UTF-8',$text)),
				data_coding => 0,
				esm_class => 0,
				link_id => $operator,
			  };

	my $id = $db->insert_sms_into_insms($sms);
	$log->debug(Dumper($sms));
	$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
}

sub handle_new_dr {
	my ( $esme, $heap, $q, $parsed ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $msg_id	   = $q->param('messageId');
	my $status         = $q->param('messageStatus');
	my $statustext     = $q->param('messageStatusText');
	
	my $sms = $esme->{db}->get_sms_by_msg_id($esme->{smsc_id},$msg_id);
	if ($sms) {
		$sms->{outbox_status} = $db->set_sms_report_status($sms->{id},($status == 2 ? 12 : 15),undef,$sms);
		$esme->outbox_status_changed($sms);
		if (($status == 2) and ($sms->{tariff} !~ /^free$/)) {
			$db->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
		}
		$log->info("OUTSMS:$sms->{id} delivery report handled : status $status ($statustext)");
	} else {
		$log->warn("OUTSMS: DR message not found message_id=$msg_id");
	}
}


sub sender_prepare_request {
	my ( $proto, $esme, $sms ) = @_;

	my $msg = ($sms->{data_coding} == 0)
		? $sms->{msg}
		: encode('utf8',decode('UCS-2',$sms->{msg}));
	my $price = $price_tab{$sms->{num}};
	my $req = {
		username => $esme->{system_id},
		password => $esme->{password},
		msisdn => '00'.$sms->{abonent},
		businesNumber => $sms->{num},
		content => $msg,
		price => $price,
		sessionId => $sms->{transaction_id},
		messageRef => $sms->{id},
	};
	return $req;
}

sub sender_handle_response {
	my ( $proto, $esme, $sms, $response ) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
	
	$log->debug($response->as_string());
	if ($response->is_success) {
		my $parsed;
		eval {
			$parsed = XMLin($response->content, KeepRoot => 0);
			$log->debug(Dumper($parsed));
		};

		my $err = $@;
		if ($err) {
			$esme->error("Error while parsing SendSMS response: $@", "Invalid SendSMS response");
		} else {
			my $status = $parsed->{'resultCode'};
			if ($status == 100) {
				$log->info("Message SMS$sms->{id} was delivered to SMSC.");
				my $msg_id = $parsed->{'messageId'};
				$db->set_sms_new_status($sms->{id}, 2, 0, 0, $msg_id, 1);
				$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id}, 1);
				$esme->outbox_status_changed($sms);
				return;
			}
		}
		$log->warn("Message SMS$sms->{id} wasn't accepted by SMSC.\n" . $response->as_string());
		$db->set_sms_new_status($sms->{id}, 255, 0, 255, 0, 1);
		$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id}, 1);
		$esme->outbox_status_changed($sms);
	} else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
			. "Will try to resend in 10 minutes.\n"
			. $response->as_string());
		$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	}
}



1;
