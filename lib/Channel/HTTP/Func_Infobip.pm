#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_Infobip;
use strict;
use Data::Dumper;
use XML::Simple;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);

# �������:
# RECIVED => 2
# ACCEPTD => 11
# DELIVRD => 12
# REJECTD => 13
# EXPIRED => 14
# UNDELIV => 15

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	$heap->{transaction_id} = $q->param('transactionid');
	$heap->{try} = 0;

	if ($q->param('message')) {
		# MO: Push Request
		my $sms = {
			smsc_id        => $esme->{smsc_id},
			abonent        => $q->param('phone'),
			num            => $q->param('shortcode'),
			msg            => encode('cp1251', $q->param('message')),
			data_coding    => 0,
			esm_class      => 0,
			transaction_id => $heap->{transaction_id},
			link_id        => $q->param('mno')
		};

		my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
		if ($id) {
			$log->info("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");

			return { content => http_response(500, 'FAILED') };
		} else {
			$id = $db->insert_sms_into_insms($sms);
			$log->debug(Dumper($sms));
			$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

			return { timer => 5 }; # First try in 5 seconds
		}
	} else {
		# DR: Payment Result � Final Notification
		$log->debug("Action: DR");
		if ($q->param('transactionid')) {
			my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $q->param('transactionid'));
			if ($outsms && $q->param('status')) {
				my $status = ($q->param('status') eq 'success') ? 12 : 13;

				$log->info("Report for message SMS$outsms->{id} was received. Status: ".$q->param('status').".")
					.($q->param('errormessage') and " Reason: ".$q->param('errormessage'));
				$outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status, undef, $outsms);
				$esme->outbox_status_changed($outsms);
				$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1) if ($status == 12);
				$db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
			}	else { 
		 		$log->debug("Not found MT for DR with transaction_id=".$q->param('transactionid'));
		 	}

			return { content => http_response(200, 'OK') };
		} else {
			$esme->warning("Unknown request\n".Dumper($q));
	 		$log->debug("[$heap->{session_id}] Unknown request...");
			return { content => http_response(400, 'Bad Request') };
		}
	}
}

sub handle_timer
{
	my($pkg, $esme, $heap) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	$heap->{try}++;
	my $ret = { timer => 5 }; # Retry in 5 seconds
	$log->debug("Action: MO");
	$log->debug("[$heap->{session_id}] Fetching...");

	# MO
	my $msgs      = $db->get_sms_ready_to_send_tr($esme->{smsc_id}, $heap->{transaction_id});
	my $msg_count = scalar keys(%$msgs);
	$log->debug("$msg_count message(s) fetched.");

	if ($msg_count > 0)	{
		my(undef, $sms)    = each %$msgs;
		$heap->{outsms_id} = $sms->{id};

		my $output = $sms->{msg};

		$log->debug("[$heap->{session_id}]\n$output");

		$db->set_sms_new_status($sms->{id},2,0,0,$heap->{transaction_id});
		$db->set_sms_delivered_status($sms->{id},1);
		$db->redis_report($sms, {});
		$log->info("[$heap->{session_id}] SMS$sms->{id} was delivered to SMSC");
		$ret = { content => http_response(200, $output) };
	}	elsif ($heap->{try} >= 5) { # 5*5+5=30 seconds
		$log->debug("[$heap->{session_id}] Timed out...");
		$ret = { content => http_response(408, "Request Timeout") };
	}
 	return $ret;
}

sub rcv_on_routines {
	my ($pkg, $esme) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $a = $db->{db}->selectall_arrayref('select id from tr_outsms where smsc_id=? and date < subdate(now(), interval 1 minute)',
		undef, $esme->{smsc_id});

	foreach (@$a) {
		my $id = $_->[0];
		$db->set_sms_new_status($id,255,0,0,0);
		$db->set_sms_delivered_status($id,1);
	}
}

1;
__END__

MO Push Request
===============
� phone � Phone number of end user who initiates payment. It is in E.164 format
without plus sign, eg. 491626839553.
� shortcode - Short number that the MO message is sent to.
Centili Payment Notifications
� message � Message text sent in initial MO message. It must include registered
keyword for Partner's service, but also it can contain some other words after the
keyword.
� mno � Mobile Network Operator code defined by Centili. It is in special format and
consists of two parts divided by underscore. First one is two-letter country code
defined in ISO 3166-1, and second one is MNO code defined by Centili, eg.
GR_VODAFONE, CY_MTN.
� transactionid - Unique payment transaction identifier used in Centili Payment
Platform.

Payment Result � Final Notification
===================================
service - Unique identifier of partner's service which is registered inside Centili
Payment Platform.
� transactionid - Unique payment transaction identifier used in Centili Payment
Platform. This parameter is very important and can be used for checking transaction
status in case of end user complaints.
� phone - Phone number of end user who payed. It is in E.164 format without plus
sign, eg. 4366124567.
� enduserprice - Price that the end user has paid in local currency with VAT included.
It is in decimal format, eg. 8.000.
Centili Payment Notifications
� status - Status of the payment. This parameter cab be either "success" or "failed"
and it defines the final result of the transaction. According to this parameter
partners implement their business logic (give content to the end users or not).
� clientid - Presents additional end user identifier at partner side which is sent back
to partner and can be used to connect the payment with the exact user or resource
in partner system. There are different ways how partner can send this parameter to
Centili according to payment initiation type. In case of MO initiation it can be passed
as a additional word after the keyword, eg. TEST 12345. In case of payment
widget it can be passed as a specific parameter of widget initiation.
� errormessage - Description of error in case of failed transaction status, eg. Daily
Limit reached, Insufficient funds, Maximum number of retries reached etc.
� country - The code of country from which end user comes. It is in ISO 3166-1
standard, eg. FR for France, ES for Spain etc.
� mno - Mobile Network Operator which end user belongs to.
� revenue - The net payout received from concrete payment.
� event_type - This parameter describes the payment type or better to say service
type. There are two different service types: One-Time Payment and Subscription
Services. According to those service types the possible values for this parameter
are: one_off (related to One-Time Payments), opt_in, opt_out, recurring_billing
(all related to Subscription Services but describes concrete phase of subscription
lifecycle).
� interval - Subscription interval according to service configuration. This field is
presented only for Subscription Services. Possible values are: daily, weekly,
monthly.
� amount - Quantity of purchased virtual goods. It depends of service configuration
and available packages inside the service.
