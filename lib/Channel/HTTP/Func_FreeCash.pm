#!/usr/bin/perl
################################################################################
# FreeCash
################################################################################
package Channel::HTTP::Func_FreeCash;
use strict;
use warnings;

use Channel::HTTP::PriceToNum qw/load_num_prices sum_to_num/;
use Channel::HTTP::HTTPUtils qw/http_response/;
use Disp::Utils;

use Digest::MD5;
use XML::Simple;
use Data::Dumper;

use URI::Escape;

################################################################################
# Sender part
################################################################################
sub sender_init {
	my ( $proto, $esme ) = @_;
	$esme->{sms_traffic_smsc_id} = Disp::Operator->default_output_operator->{smsc_id};
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	$esme->{log}->info("OUTBOX$sms->{outbox_id} resended through SMSC$esme->{sms_traffic_smsc_id}");
	$esme->{db}->resend_through_smstraffic_with_num($sms->{outbox_id}, $esme->{sms_traffic_smsc_id}, '1121');
	$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
	$esme->{db}->set_sms_delivered_status($sms->{id});
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}

################################################################################
# Main API
################################################################################
sub init {
	my ( $proto, $esme ) = @_;

	$esme->{log}->debug("Init");
	load_num_prices($esme);
}

my %action_handlers = ( 1 => \&action_payment,
						2 => \&action_cancel,
						3 => \&action_check_payment,
						4 => \&action_check_account );

sub make_response {
	my ( $status, $message ) = @_;
	my $params = { date => today(),
				   status => $status };
	$params->{message} = $message if $message;
	my $xml = XMLout({response => $params}, KeepRoot => 1, XMLDecl => 1, NoAttr => 1);
	return http_response('200 OK' , $xml);
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;
	my $params = $q->Vars;

	$esme->{log}->debug("Request: @{[Dumper($q)]})");

	my @sign_params = sort grep { $_ ne 'SIGN' } keys %$params;

# 	my $u = new URI;
# 	$u->query_form( map { $_ => $params->{$_} } @sign_params);
# 	my $what_to_sign = substr $u->path_query, 1;

	my $what_to_sign = join '&', map { $_."=".uri_escape($params->{$_}) } @sign_params;

	$what_to_sign =~ s/%([0-9A-F]{2})/"%".lc($1)/gei;

	$esme->{log}->debug("Reassembled query for signature checking: $what_to_sign");

	my $what_to_md5 = $what_to_sign . $esme->{password};
	$esme->{log}->debug("String for applying MD5: $what_to_md5");

	my $signed = Digest::MD5::md5_hex($what_to_md5);

	if ($q->param('SIGN') eq $signed) {
		my $handler = $action_handlers{$q->param('TYPE')};
		unless ($handler) {
			$esme->error("Invaild signature\nExpected: $signed\nGot     : @{[$q->param('SIGN')]}", "Incorrect request signature");
			return make_response(-3, 'TYPE missing or incorrect');
		}
		return $handler->(@_);
	} else {
		$esme->error("Invaild signature\nExpected: $signed\nGot     : @{[$q->param('SIGN')]}", "Incorrect request signature");
		return make_response(-2, 'Invalid signature');
	}
}

sub make_error_response {
	my ( $esme, $q, $status, $message ) = @_;

	$esme->error("Failed to handle FreeCash request\nFreeCash status: $status\nFreeCash message: $message\nRequest: @{[Dumper(scalar $q->Vars)]}",
				 "Failed to handle FreeCash request" );

	return make_response($status, $message);
}

sub action_payment {
	my ( $proto, $esme, $heap, $q ) = @_;

	return make_error_response($esme, $q, -12, 'Invalid PAY_ID') unless $q->param('PAY_ID') =~ /^\d+$/;

	my $check = $esme->{db}->get_insms_id_by_transaction_id($esme->{smsc_id}, $q->param('PAY_ID'));
	return make_error_response($esme, $q, -13, "Already stored with id $check") if $check;

	my ( $abonent, $msg );
	if ($q->param('ACCOUNT') =~ /^(7\d{10})$/) {
		$abonent = $1;
	} else {
		return make_error_response($esme, $q, -11, 'Cant extract cell phone and text from account');
	}

	$msg = $q->param('SERVICE');

	my $num = sum_to_num($esme, $q->param('PAY_AMOUNT'));

	unless ( $num ) { 
		$esme->error("Request is @{[Dumper(scalar $q->Vars)]}", "Payment ignored - price to num conversion failed");
		return make_response(0, "Payment accepted");
	}

	my $sms = { smsc_id => $esme->{smsc_id},
				num => $num,
				abonent => $abonent,
				msg => $msg,
				data_coding => 0,
				esm_class => 0,
				transaction_id => $q->param('PAY_ID') };

	my $insms_id = $esme->{db}->insert_sms_into_insms($sms);

	return make_response(0, "Payment accepted - tracking number $insms_id");

}

sub action_cancel {
	my ( $proto, $esme, $heap, $q ) = @_;

	return make_error_response($esme, $q, -12, 'Invalid PAY_ID') unless $q->param('PAY_ID') =~ /^\d+$/;
	my $check = $esme->{db}->get_insms_id_by_transaction_id($esme->{smsc_id}, $q->param('PAY_ID'));
	return make_error_response($esme, $q, -21, "Money already spent - tracking number $check") if $check;
	return make_error_response($esme, $q, -23, "No payment with such PAY_ID");
}

sub action_check_payment {
	my ( $proto, $esme, $heap, $q ) = @_;

	return make_error_response($esme, $q, -12, 'Invalid PAY_ID') unless $q->param('PAY_ID') =~ /^\d+$/;

	my $check = $esme->{db}->get_insms_id_by_transaction_id($esme->{smsc_id}, $q->param('PAY_ID'));
	return make_response(0, "Processed - tracking number $check") if $check;

	return make_error_response($esme, $q, -33, 'No payment found with given PAY_ID');
}

sub action_check_account {
	my ( $proto, $esme, $heap, $q ) = @_;

	if ($q->param('ACCOUNT') =~ /^(7\d{10})$/) {
		return make_response(0, 'ACCOUNT OK');
	} else {
		return make_error_response($esme, $q, -41, 'No or incorrect ACCOUNT');
	}
}

1;

__END__


payment
wget -O - 'http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl?TYPE=1&PAY_ID=13412424&ACCOUNT=79265550017/abcdef&DATE=2008-04-17+09:12:00&PAY_AMOUNT=17&SIGN=80cb2c5693f92d50f403ba512b14fa27'

cancel
wget -O - 'http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl?TYPE=2&PAY_ID=13412424&ACCOUNT=79265550017/abcdef&DATE=2008-04-17+09:12:00&PAY_AMOUNT=17&SIGN=6ac77fed86be67f65b07af2ad76db081'

check payment
wget -O - 'http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl?TYPE=3&PAY_ID=13412424&ACCOUNT=79265550017/abcdef&DATE=2008-04-17+09:12:00&PAY_AMOUNT=17&SIGN=dfd6567dbd52987d9f8fb36a3b418205'

check account - valid
wget -O - 'http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl?TYPE=4&PAY_ID=13412424&ACCOUNT=79265550017/abcdef&DATE=2008-04-17+09:12:00&PAY_AMOUNT=17&SIGN=d6df7e40c5e55123e381ce38cf0c6f68'

check account - invalid
wget -O - 'http://devel2.local.alt1.ru/cgi-bin/sms/fastcgi_https_client.pl?TYPE=4&PAY_ID=13412424&ACCOUNT=79265550017&DATE=2008-04-17+09:12:00&PAY_AMOUNT=17&SIGN=c7b470600eb01ecf81bd8e719b47d56c'
