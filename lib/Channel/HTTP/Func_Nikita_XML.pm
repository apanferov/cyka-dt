#!/usr/bin/perl
package Channel::HTTP::Func_Nikita_XML;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use URI::URL;
use LWP::UserAgent;
use HTTP::Request;

use Channel::HTTP::HTTPUtils qw/http_response/;


sub handle_request {
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

    my $xml = $q->param('POSTDATA');
    $log->debug($xml);

    my $ref = XMLin($xml, ForceArray => ['message','delivery-report']);
    $log->debug(Dumper($ref));

    if ($ref->{message}) {
	    foreach (@{ $ref->{message} }){ handle_message($esme,$_) };
    }
    if ($ref->{'delivery-report'}) {
	    handle_delivery_report($esme,$ref->{message});
    }

    my $resp = http_response('200 OK');
    $log->debug("Response: 200 OK");
	return $resp;
}

sub handle_message {
	my ($esme,$ref) = @_;
	my ($db, $log) = ($esme->{db}, $esme->{log});
	my $sms = {
		   smsc_id        => $esme->{smsc_id},
		   num            => $ref->{'short-number'},
		   abonent        => $ref->{phone},
		   msg            => encode('cp1251',$ref->{text}),
		   data_coding    => 0,
		   esm_class      => 0,
		   transaction_id => $ref->{'transaction-id'},
		   link_id        => $ref->{provider},
	};
	$esme->{log}->debug(Dumper($sms));

	my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
        if ($id) {
		$log->warn("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	} else {
		$id = $db->insert_sms_into_insms($sms);
		$log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
}

sub handle_delivery_report {
	my ($esme,$ref) = @_;
	my ($db, $log) = ($esme->{db}, $esme->{log});

}

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_sms {
	my($pkg, $esme, $sms) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});

	my $login = XMLout({login => $esme->{system_id}}, NoAttr => 1, RootName => undef);
	my $password = XMLout({password => $esme->{password}}, NoAttr => 1, RootName => undef);
	my $text = XMLout({text => $sms->{msg}}, NoAttr => 1, RootName => undef);
	my $transaction_id = XMLout({'transaction-id' => $sms->{transaction_id}}, NoAttr => 1, RootName => undef);

	my $header = HTTP::Headers->new;
  	$header->header('content-type' => 'text/xml');

	my $xml = <<EOFXML;
<?xml version="1.0" encoding="cp1251"?>
<response>
<authentication>
$login$password</authentication>
<message>
$transaction_id$text</message>
</response>
EOFXML

  	my $req = HTTP::Request->new('POST', $esme->{url}, $header, $xml);
  	$log->debug($req->as_string());

	my $resp  = $esme->{ua}->request($req);
	my $reply = $resp->content();
	$log->debug($reply);

	if ($resp->code() eq '200') {
		my $ref = XMLin($reply, KeepRoot => 1);
		if ($ref->{status}{code} == 0) {
			$db->set_sms_new_status($sms->{id}, 2, 0, 0, undef, 1);
			$db->set_sms_delivered_status($sms->{id});
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		}
		else {
			$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. ". $resp->as_string());
			$db->set_sms_new_status($sms->{id}, 255, 0, $ref->{status}{code}, undef, 1);
			$db->set_sms_delivered_status($sms->{id});
		}
	}
	else {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. "
				   . "Will try to resend in 10 minutes. "
				   . $resp->as_string());
		$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	}
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}



sub rcv_on_init {
	my ( $proto, $esme ) = @_;
}

sub rcv_on_routines {
	my ( $proto, $esme ) = @_;
}


sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

1;
