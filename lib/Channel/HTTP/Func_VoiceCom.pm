#!/usr/bin/perl
package Channel::HTTP::Func_VoiceCom;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use URI::URL;
use LWP::UserAgent;
use HTTP::Request;

use Channel::HTTP::HTTPUtils qw/http_response/;

my $service_map = {'1098' => 276,
		   '1816' => 504,
		   '1916' => 508,
		   '18423' => 526,
		   '18420' => 526,
		   '1915' => 610,
		   '18422' => 735,
		   '18421' => 753,
		  };

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});
	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $url = url($esme->{url});
	my %req = (	serviceID => $service_map->{$sms->{num}},
				shortcode => $sms->{num},
				msisdn => $sms->{abonent},
				message => $sms->{msg},
				id => $sms->{id},
				timestamp => time,
				op => $sms->{link_id});

	$url->query_form(%req);
	$log->debug($url);
	my $resp = $ua->get($url);
	my $reply = $resp->content();
	$log->debug($reply);

	if ($resp->code() eq '200') {
		if ($reply eq 'SEND_OK') {
			$db->set_sms_new_status($sms->{id},2,0,0,0,1);
			$db->set_sms_delivered_status($sms->{id},0,1);
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		}
		else {
			$db->set_sms_new_status($sms->{id},255,0,255,0,1);
			$db->set_sms_delivered_status($sms->{id},1);
			$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
		}
	}
	else {
       	$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. ($reply)");
		$db->set_sms_new_status($sms->{id},0,600,255,0,1);
		$log->warn("Will try to resend in 10 minutes.");
	}
	return;
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}



sub rcv_on_init {
	my ( $proto, $esme ) = @_;
}

sub rcv_on_routines {
	my ( $proto, $esme ) = @_;
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $sms = {
			   smsc_id => $esme->{smsc_id},
			   num => $q->param('shortcode'),
			   abonent => $q->param('msisdn'),
			   msg => $q->param('msg'),
			   transaction_id => $q->param('id'),
			   data_coding => 0,
			   esm_class => 0,
			   link_id => $q->param('op')
			  };
	my $id = $esme->{db}->insert_sms_into_insms($sms);
	$esme->{log}->debug(Dumper($sms));
	$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	return http_response('200 OK', 'SEND_OK');
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

1;
