#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_PTN;
use strict;
use Data::Dumper;
use XML::Simple;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);

# �������:
# RECIVED => 2
# ACCEPTD => 11
# DELIVRD => 12
# REJECTD => 13
# EXPIRED => 14
# UNDELIV => 15

my $status_tab = {
  '200' => {status_id => 2, http_status => 200, http_type => 'async', delivery_status => 'unprocessed', status_txt => 'Message is received'},
  '201' => {status_id => 11, http_status => undef, http_type => 'async', delivery_status => 'processed', status_txt => 'Message is being processed'},
  '202' => {status_id => 11, http_status => undef, http_type => 'async', delivery_status => 'acknowledged', status_txt => 'Message is acknowledged by operator and is being processed'},
  '203' => {status_id => 12, http_status => undef, http_type => 'async', delivery_status => 'ok', status_txt => 'Message is billed and delivered'},
  '204' => {status_id => 11, http_status => undef, http_type => 'async', delivery_status => 'inprogress', status_txt => 'Message is billed'},
  '205' => {status_id => 11, http_status => undef, http_type => 'async', delivery_status => 'sent', status_txt => 'Message is sent to operator'},
  '404' => {status_id => 13, http_status => 415, http_type => undef, delivery_status => 'error', status_txt => 'DB saving error'},
  '405' => {status_id => 13, http_status => 400, http_type => undef, delivery_status => 'error', status_txt => 'Wrong XML'},
  '406' => {status_id => 13, http_status => 404, http_type => undef, delivery_status => 'error', status_txt => 'Wrong transaction ID'},
  '901' => {status_id => 13, http_status => undef, http_type => 'async', delivery_status => 'error', status_txt => 'Customer has to low credit, the message can not be delivered'},
  '902' => {status_id => 13, http_status => undef, http_type => 'async', delivery_status => 'error', status_txt => 'The message expired and could not be delivered'},
  '907' => {status_id => 13, http_status => undef, http_type => 'async', delivery_status => 'error', status_txt => 'The message is billed and the operator confirms billing, but an error occurred at the operator side when trying to deliver the message to the mobile phone'},
  '912' => {status_id => 13, http_status => undef, http_type => 'async', delivery_status => 'error', status_txt => 'User balance has been reached for the shortcode, and operator denies more overcharged messages in the msisdn in the current period'},
};

my $service_tab = {
  '5' => 1747
};

sub sender_init {
#  my ($proto, $esme) = @_;
} 

sub sender_handle_routines {
#  my ($proto, $esme) = @_;
} 

sub handle_request
{
  my($pkg, $esme, $heap, $q) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});

  my $xml = $q->param('POSTDATA');
  $log->debug($xml);

  my $ref = XMLin($xml, KeepRoot => 1);
  $log->debug(Dumper($ref));

  if ( $ref->{mo} ) {
    # �������� ���
    my $service_id = $service_tab->{ $ref->{mo}{serviceId} };

    my $sms = {
      smsc_id        => $esme->{smsc_id},
      abonent        => $ref->{mo}{subscriberNumber},
      num            => $ref->{mo}{shortNumber}.'-'.$ref->{mo}{tariff},
      msg            => encode('cp1251', $ref->{mo}{messageText}),
      data_coding    => 0,
      esm_class      => 0,
      transaction_id => $ref->{mo}{transactionID}.':'.$ref->{mo}{mcc}.':'.$ref->{mo}{mnc}.':'.$ref->{mo}{tariff}.':'.$ref->{mo}{currency}.':{PTN'.$service_id.'}',
      link_id        => $ref->{mo}{mcc}.$ref->{mo}{mnc} 
    };
  
    my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
    if ($id) {
      $esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
    } else {
      $id = $db->insert_sms_into_insms($sms);
      $log->debug(Dumper($sms));
      $log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
    }

    return http_response('200 OK', '');
  } else  {
    # �������� deliveryReport ��� ���-�� ���
    my $msg_id = $ref->{deliveryReport}{transactionId};
    my $status_id = $status_tab->{ $ref->{deliveryReport}{statusCode} }{status_id};
    my $status_txt = $status_tab->{ $ref->{deliveryReport}{statusCode} }{status_txt};
    my $http_status = $status_tab->{ $ref->{deliveryReport}{statusCode} }{http_status};

    sleep(1); # DR might come before MT saved in outsmsa
    my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $msg_id);
    if ($outsms and ($status_id)) {
      $log->info("Report for message SMS$outsms->{id} was received. Status: $ref->{deliveryReport}{statusCode}."
        . ($status_txt and " Reason: $status_txt"));
      #my ($country) = split(/-/,$outsms->{transaction_id});
      #$db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id},1)
      #  unless (($outsms->{tariff} =~ /^free$/) or ($country ne 'LV') or ($status_id != 12));
      $outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status_id, undef, $outsms);
      $db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
      return http_response((($http_status) ? $http_status : '200').' OK', '');
    } else {
      $esme->warning("Unknown report $status_id\n".Dumper($ref));
      return http_response((($http_status) ? $http_status : '400').' OK', '');
    }
  }
}

sub sender_handle_sms {
  my($pkg, $esme, $sms) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});
  
  my($transaction_id, $mcc, $mnc, $tariff, $currency) = split /:/, $sms->{transaction_id};
  my $msg = ($sms->{data_coding} == 0)
    ? $sms->{msg}
    : encode('utf8', decode('UCS-2', $sms->{msg}));
  $msg = xml_escape($msg);

  my $header = HTTP::Headers->new;
    $header->header('content-type' => 'text/xml');
  	$header->authorization_basic('a1', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

#   my $struct = {
#     currency => $currency,
#     foreignId => 'LV', # !!!!
#     mcc => $mcc,
#     messageText => $msg,
#     mnc => $mnc,
#     tariff => $tariff,
#     shortNumber => $sms->{num},
#     subscriberNumber => $sms->{abonent},
#     transactionID => $sms->{id},
#   };
#   my $xml = XMLout(
#     $struct,
#     RootName => 'mt',
#     NoAttr => 1,
#     KeyAttr => [],
#     XMLDecl => '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
#   );
  my $struct = {
    messageText => $msg,
    transactionID => $transaction_id,
  };
  my $xml = XMLout(
    $struct,
    RootName => 'moReply',
    NoAttr => 1,
    KeyAttr => [],
    XMLDecl => '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
  );

  my $ua = new LWP::UserAgent;
  $ua->timeout(20);

  my $req = HTTP::Request->new('POST', $esme->{url}, $header, $xml);
  $log->debug($req->as_string());

  my $resp  = $esme->{ua}->request($req);
  my $reply = $resp->content();
  $log->debug($reply);

  if ({200=>1,415=>1,400=>1,404=>1}->{$resp->code()}) {
    my $ref = XMLin($reply, KeepRoot => 1);
    if ($ref->{deliveryReport}{statusCode} eq '200') {
      $log->info("Message SMS$sms->{id} was delivered to SMSC.");
      $db->set_sms_new_status($sms->{id}, 2, 0, 0, $ref->{deliveryReport}{transactionId}, 1);
    } else {
      $log->warn("Message SMS$sms->{id} rejected ($ref->{deliveryReport}{statusCode}). \n");
      $db->set_sms_new_status($sms->{id}, 255, 0, $ref->{deliveryReport}{statusCode}, 0, 1);
    }
    #my $ignore_billing = ($country eq 'LV') ? 1 : 0;
    #$sms->{outbox_status} = $db->set_sms_delivered_status($sms->{id},$ignore_billing);
    $db->redis_report($sms, {}) if ($sms->{registered_delivery});
  } else {
    $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. \n"
      . "Will try to resend in 10 minutes.\n"
      . $resp->as_string());
    $db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
  }
}

sub xml_escape {
  my ( $data ) = @_;
  $data =~ s/&/&amp;/sg;
  $data =~ s/</&lt;/sg;
  $data =~ s/>/&gt;/sg;
  $data =~ s/"/&quot;/sg; #"
  return $data;
}

1;
__END__

MO format
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<mo>
    <date>2012-08-15T00:00:00+03:00</date> <!-- Date when message was received from subscriber. -->
    <mcc>0</mcc> <!-- Mobile Country Code. Identified by PTNAggregator mobile country code where message originates from -->
    <mnc>247</mnc> <!-- Mobile Network Code. Identified by PTNAggregator mobile network code where message originates from -->
    <messageText>Test Message</messageText> <!-- Text of the message send by subscriber -->
    <shortNumber>123</shortNumber> <!-- Short number, if applicable -->
    <subscriberNumber>37129355011</subscriberNumber> <!-- Subscribers number -->
    <tariff>100</tariff> <!-- Tariff determined by PTNAggregator, based on MCC, MNC and BE. Given in smallest amount of the currency. Includes VAT -->
    <income>67</income> <!-- Income of the system, determined by PTNAggregator, based on MCC, MNC and BE. Given in smallest amount of the currency. Does not include VAT. -->
    <currency>EUR</currency> <!-- currency defined bay Tariff instance in Aggregator and by Manager, Partner and Coordinator -->
    <serviceID>3342</serviceID> <!-- unique service number inside BE where message is sent, determined by message text -->
    <transactionID>PreudoRandomID111</transactionID> <!-- Automatically generated message id for getting message status/history later -->
</mo>

��� ������ � LMT, 1.4463 ->1.0124, �������� ����� �����.
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<mo>
    <date>2012-09-21T00:00:00+03:00</date>
    <mcc>247</mcc> <!-- ������ -->
    <mnc>1</mnc> <!-- ��� -->
    <messageText>Test Message</messageText>
    <shortNumber>1187</shortNumber>
    <subscriberNumber>37129355011</subscriberNumber>
    <tariff>175</tariff> <!-- 1.75Ls, �������� ��� -->
    <income>122.5</income> <!-- 1.225Ls, �������� ��� -->
    <currency>LVL</currency>
    <serviceID>5</serviceID> <!-- ��� ����� ������ -->
    <transactionID>PreudoRandomID111</transactionID>
</mo>

MT format
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<mt>
   <currency>Eur</currency> <!-- Currency short name -->
   <foreignId>LV</foreignId> <!-- ForeignID - identifier of destination -->
   <mcc>247</mcc> <!-- Mobile Country Code. Identified by PTNAggregator mobile country code where message originates from -->
   <messageText>test</messageText> <!-- Message text -->
   <mnc>1</mnc> <!-- Mobile Network Code. Identified by PTNAggregator mobile network code where message originates from -->
   <tariff>10</tariff> <!-- Tariff determined by PTNAggregator, based on MCC, MNC and BE. Given in smallest ammount of the currency -->
   <shortNumber>1234</shortNumber> <!-- Bussiness Entity number, from which message has been sent -->
   <subscriberNumber>987654321</subscriberNumber> <!-- Subscribers number -->
   <transactionID>random uuid could be here MT</transactionID> <!-- Automatically generated message id for getting message status/histrory later -->
</mt>

DeliveryReport format
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<deliveryReport>
    <deliveryStatus>ok</deliveryStatus> <!-- String status of delivery. Possible values: ok, failed, inprogress -->
    <statusCode>200</statusCode> <!-- Optional field. Should contain error code for corresponding failure rease. Code can be taken from Aspiro document -->
    <transactionId>123</transactionId> <!-- TransactionID will be generated by PTNAggregator for each MO/MT message and given to MO consumer or MT producer. Length = 36 symbols -->
</deliveryReport>
