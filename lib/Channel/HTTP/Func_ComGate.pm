#!/usr/bin/perl
package Channel::HTTP::Func_ComGate;
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
use Encode qw/encode decode/;
use URI;
use LWP::UserAgent;
use HTTP::Request;

use Channel::HTTP::HTTPUtils qw/http_response/;

sub xml_escape {
	my ( $data ) = @_;
	$data =~ s/&/&amp;/sg;
	$data =~ s/</&lt;/sg;
	$data =~ s/>/&gt;/sg;
	$data =~ s/"/&quot;/sg;
	return $data;
}

sub sender_init {
	my ( $proto, $esme ) = @_;
}

sub sender_handle_response {
	my ( $proto, $esme, $sms, $response ) = @_;
	if ($response->is_success) {
		my $parsed;
		eval {
			$parsed = XMLin($response->content, KeepRoot => 1);
		};

		my $err = $@;
		if ($err) {
			$esme->error("Error while parsing SendSMS response: $@", "Invalid SendSMS response");
		} else {
			my $status = $parsed->{methodResponse}{params}{param}{value}{struct}{member}{status}{value}{int};
			if ($status == 200) {
				$esme->{db}->set_sms_new_status($sms->{id},2,0,0,0);
				$esme->{db}->set_sms_delivered_status($sms->{id},1);
				return;
			}
		}
		$esme->{db}->set_sms_new_status($sms->{id},255,0,0,0);
		$esme->{db}->set_sms_delivered_status($sms->{id},1);
	} else {
		$esme->{db}->set_sms_new_status($sms->{id},0,0,0,0);
	}
}

sub sender_prepare_request {
	my ( $proto, $esme, $sms ) = @_;

	my $req = HTTP::Request->new(POST => $esme->{uri});
	$req->content(<<EOF);
<?xml version="1.0"?>
<methodCall>
  <methodName>SendSMS</methodName>
  <params>
	<param>
	  <value><string>$sms->{abonent}</string></value>
	</param>
	<param>
	  <value><string>@{[xml_escape($sms->{msg})]}</string></value>
	</param>
	<param>
	  <value><int>$sms->{transaction_id}</int></value>
	</param>
	<param>
	  <value><int>0</int></value>
	</param>
	<param>
	  <value><int>0</int></value>
	</param>
	<param>
	  <value><string></string></value>
	</param>
  </params>
</methodCall>
EOF

	return $req;
}


sub sender_handle_sms {
	my ( $proto, $esme, $sms ) = @_;

	my $req = sender_prepare_request($proto, $esme, $sms);

	my $ua = new LWP::UserAgent;
	$ua->timeout(20);

	my $resp = $ua->request($req);

	return sender_handle_response($proto, $esme, $sms, $resp);
}

sub sender_handle_routines {
	my ( $proto, $esme ) = @_;
}

my %method_table = ( NewSMS => \&handle_new_sms,
					 NewDLR => \&handle_new_dlr,
				   );

sub handle_new_dlr {
	my ( $esme, $heap, $q, $parsed ) = @_;

	my $eventid = $parsed->{methodCall}{params}{param}[0]{value}{int};
	my $struct = $parsed->{methodCall}{params}{param}[1]{value}{struct}{member};

	my $id		   = $struct->{id}{value}{int};
	my $number	   = $struct->{number}{value}{string};
	my $appnumber  = $struct->{appnumber}{value}{string};
	my $sdate	   = $struct->{sdate}{value}{'dateTime.iso8601'};
	my $text	   = $struct->{text}{value}{string};
	my $ddate	   = $struct->{ddate}{value}{'dateTime.iso8601'};
	my $status	   = $struct->{status}{value}{int};

	if ($eventid !~ /^\d+$/) {
		$esme->error("Invalid eventid : '$eventid'", "Invalid eventid");
		return comgate_response(500, "Invalid eventid");
	}

	if ($id !~ /^\d+$/) {
		$esme->error("Invalid id : '$id'", "Invalid id");
		return comgate_response(500, "Invalid id");
	}

	if ($number !~ /^\+\d+$/) {
		$esme->error("Invalid abonent : '$number'", "Invalid number");
		return comgate_response(500, "Invalid number");
	}

	if ($appnumber !~ /^\d+$/) {
		$esme->error("Invalid abonent : '$appnumber'", "Invalid appnumber");
		return comgate_response(500, "Invalid appnumber");
	}

	if ($status != 0 and $status != 1) {
		$esme->error("Invalid status : '$status'", "Invalid status");
		return comgate_response(500, "Invalid status");
	}

	if ($ddate !~ /^(\d{4})(\d{2})(\d{2})T(\d{2}):(\d{2}):(\d{2})$/) {
		$esme->error("Invalid ddate : '$ddate'", "Invalid ddate");
		return comgate_response(500, "Invalid ddate");
	}

	$ddate = "$1-$2-$3 $4:$5:$6";

	my $sms = $esme->{db}->get_sms_by_msg_id($esme->{smsc_id},$id);
	if ($sms) {
		$esme->{db}->set_sms_report_status($sms->{id},
										   ($status == 0 ? 15 : 12),
										   $ddate,
										   $sms);
		$log->info("SMS$sms->{id} delivery report handled : status $status");
		return comgate_response(200, "Updated SMS$sms->{id} delivery status");
	} else {
		return comgate_response(500, "No such transaction: $id");
	}
}

sub handle_new_sms {
	my ( $esme, $heap, $q, $parsed ) = @_;

	my $transaction_id	= $parsed->{methodCall}{params}{param}[0]{value}{int};
	my $short_num		= $parsed->{methodCall}{params}{param}[1]{value}{string};
	my $abonent			= $parsed->{methodCall}{params}{param}[2]{value}{string};
	my $text			= $parsed->{methodCall}{params}{param}[3]{value}{string};
	my $smsc_date		= $parsed->{methodCall}{params}{param}[4]{value}{'dateTime.iso8601'};
	my $price_level		= $parsed->{methodCall}{params}{param}[5]{value}{int};
	my $operator		= $parsed->{methodCall}{params}{param}[6]{value}{int};
	my $service			= $parsed->{methodCall}{params}{param}[7]{value}{int};
	my $ref_no			= $parsed->{methodCall}{params}{param}[8]{value}{int};
	my $max_no			= $parsed->{methodCall}{params}{param}[9]{value}{int};

	if ($transaction_id !~ /^\d+$/) {
		$esme->error("Invalid transaction_id : '$transaction_id'", "Invalid transaction_id");
		return comgate_response(500, "Invalid id");
	}

	if ($short_num !~ /^\w+$/) {
		$esme->error("Invalid short_num : '$short_num'", "Invalid short_num");
		return comgate_response(500, "Invalid short_num");
	}

	if ($abonent !~ /^\+\d+$/) {
		$esme->error("Invalid abonent : '$abonent'", "Invalid abonent");
		return comgate_response(500, "Invalid abonent");
	}

	if ($price_level !~ /^\d+$/) {
		$esme->error("Invalid price_level : '$price_level'", "Invalid price_level");
		return comgate_response(500, "Invalid price_level");
	}

	if ($operator !~ /^\d+$/) {
		$esme->error("Invalid operator : '$operator'", "Invalid operator");
		return comgate_response(500, "Invalid operator");
	}

	if ($service !~ /^\d+$/) {
		$esme->error("Invalid service : '$service'", "Invalid service");
		return comgate_response(500, "Invalid service");
	}

	if ($ref_no !~ /^\d+$/ or $max_no !~ /^\d+$/ or $ref_no > $max_no ) {
		$esme->error("Invalid sar : '$ref_no'/'$max_no'", "Invalid SAR parameters");
		return comgate_response(500, "Invalid SAR parameters");
	}

	my $sms = { smsc_id => $esme->{smsc_id},
				transaction_id => $transaction_id,
				num => $short_num,
				abonent => $abonent,
				msg => encode('cp1251',decode('UTF-8',$text)),
				data_coding => 0,
				esm_class => 0,
				link_id => "$price_level:$operator:$service",
			  };

	my $id = $esme->{db}->insert_sms_into_insms($sms);
	$esme->{log}->debug(Dumper($sms));
	$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");

	return comgate_response(200, "SMS$id added to processing queue");
}

sub comgate_response {
	my ( $status, $message ) = @_;
	my $xml_params = {};

	my $tmp = [ { member => { name => 'status', value => { int => $status } } } ,
				{ member => { name => 'statusMessage', value => { string => $message } } } ];

	$xml_params->{methodResponse}{params}{param}[0]{value}[0]{struct}[0]{member} = $tmp;

	my $xml = XMLout($xml_params, KeepRoot => 1, NoAttr => 1, XMLDecl => 1);
	return http_response('200 OK', $xml,
						 content_type => 'text/xml',
						 charset => 'cp1251' );
}

sub init {
	my ( $proto, $esme ) = @_;
}

sub handle_request {
	my ( $proto, $esme, $heap, $q ) = @_;

	my $data = $q->param('POSTDATA');
	unless ($data) {
		$esme->error("No postdata in ComGate request", "No postdata in ComGate request");
		return comgate_response(500, "No POSTDATA");
	}

	my $parsed;
	eval {
		$parsed = XMLin($data, KeepRoot => 1);
	};

	$esme->{log}->debug("PARSED REQUEST: " . Dumper($parsed));

	if ($@) {
		$esme->error($@, "Error parsing ComGate XML");
		return comgate_response(500, "Error while parsing XML");
	}

	my $method = $parsed->{methodCall}{methodName};
	unless ($method) {
		$esme->error("Invalid XML-RPC call for ComGate:\n $data", "Invalid XML-RPC call for ComGate");
		return comgate_response(500, "Invalid XML-RPC call for ComGate");
	}

	my $method_handler = $method_table{$method};
	unless ($method_handler) {
		$esme->error("Unknown method '$method'", "Unknown XML-RPC method");
		return comgate_response(500, "Unknown XML-RPC method");
	}

	return $method_handler->($esme, $heap, $q, $parsed);
}

sub handle_timer {
	my ( $proto, $esme, $heap ) = @_;
	$esme->error("Unexpected handle_timer call", "Unexpected handle_timer call");
	return;
}

1;
