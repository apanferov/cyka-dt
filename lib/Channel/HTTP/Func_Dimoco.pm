package Channel::HTTP::Func_Dimoco;
use strict qw(vars);
use Data::Dumper;
use Encode qw(encode decode);
use URI::URL;
use Channel::HTTP::HTTPUtils qw(http_response);
use XML::Simple;

sub handle_request
{
	my($pkg, $esme, $heap, $q) = @_;
	my($db, $log) = ($esme->{db}, $esme->{log});
	$heap->{aa} = 11;
	POE::Kernel->alarm_set('_timer', time + 1);
	return http_response('200 OK', 'OK');

	my $sms = {
		smsc_id        => $esme->{smsc_id},
		num            => $q->param('to'),
		abonent        => $q->param('from'),
		msg            => encode('cp1251', $q->param('message')),
		transaction_id => $q->param('operator').'-'.$q->param('sms-id'),
		link_id        => $q->param('operator'),
		data_coding    => 0,
		esm_class      => 0,
		};

	if (my $id=$db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id})) {
		$esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
	}
	else {
		$esme->{log}->debug(Dumper($sms));
		my $id = $esme->{db}->insert_sms_into_insms_ignored($sms);
		$esme->{log}->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
	}
	#set timer
	POE::Kernel->alarm_set('_timer', time + 1);
	return http_response('200 OK', 'OK');
}

sub handle_timer {
	my($pkg, $esme, $heap) = @_;

	$esme->{log}->debug(Dumper($heap));
	return -1;
}


sub sender_init {}

sub sender_handle_sms {
	my($pkg, $esme, $sms) = @_;
	my ($db,$log) = ($esme->{db},$esme->{log});

	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => $esme->{local_addr})
		if $esme->{local_addr};

	my $ua = new LWP::UserAgent;
	$ua->timeout(30);

	my $url = url($esme->{url});
	my ($operator,$tr_id) = split(/-/,$sms->{transaction_id});
	my %req = (
		'app-id'   => $esme->{system_id},
		password   => $esme->{password},
		message    => encode('utf8', $sms->{msg}),
		to         => $sms->{abonent},
		operator   => $operator,
		tariff     => 'free',
		type       => 'text'
		);

	$url->query_form($url->query_form, %req);
	$log->debug($url);

	my $resp = $ua->get($url);

	my $reply = $resp->content();
	$log->debug("Response: $reply");

        unless (($resp->code() eq '200') and ($reply =~ m/^\s*<\?xml/)) {
		$log->warn("Message SMS$sms->{id} wasn't delivered to SMSC. Will try to resend in 10 minutes.\n". $resp->as_string());
		$db->set_sms_new_status($sms->{id}, 0, 600, 255, 0, 1);
	} else {
		my $ref = XMLin($reply, ForceContent => 0);
		my $status = $ref->{status};

		if ($status eq '0') {
			$db->set_sms_new_status($sms->{id}, 2, 0, 0, $ref->{'msg-id'}, 1);
			$db->set_sms_delivered_status($sms->{id});
			$log->info("Message SMS$sms->{id} was delivered to SMSC.");
		} else {
			$db->set_sms_new_status($sms->{id}, 255, 0, 255, 0, 1);
			$db->set_sms_delivered_status($sms->{id}, 1);
			$log->warn("Message SMS$sms->{id} wasn't accepted by SMSC.\n" . $resp->as_string());
		}
	} #HTTP 200 OK
	return;
}

sub sender_handle_routines { 0 }

1;
__END__
