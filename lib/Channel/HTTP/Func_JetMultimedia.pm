#!/usr/bin/perl
# -*- coding: cp1251 -*-
package Channel::HTTP::Func_JetMultimedia;
use strict;
use Data::Dumper;
use XML::Simple;
use Encode;
use Channel::HTTP::HTTPUtils qw(http_response);

# �������:
# RECIVED => 2
# ACCEPTD => 11
# DELIVRD => 12
# REJECTD => 13
# EXPIRED => 14
# UNDELIV => 15

my $status_tab = {
  0 => {status_id => 12, status_txt => ''},
  1 => {status_id => 13, status_txt => 'login required'},
  2 => {status_id => 13, status_txt => 'password required'},
  3 => {status_id => 13, status_txt => 'mo_id required'},
  4 => {status_id => 13, status_txt => 'IP address not allowed'},
  5 => {status_id => 13, status_txt => 'Unknown exception'},
  6 => {status_id => 13, status_txt => 'mo_id not found'},
  7 => {status_id => 13, status_txt => 'free is missing'},
  8 => {status_id => 13, status_txt => 'Internal API error'},
  9 => {status_id => 13, status_txt => 'Invalid mo_id'},
  10 => {status_id => 13, status_txt => 'Invalid ses_id'},
  11 => {status_id => 13, status_txt => 'mt_text required'},
  12 => {status_id => 13, status_txt => 'Bad end user price'},
  13 => {status_id => 13, status_txt => 'Bad credentials'},
  14 => {status_id => 13, status_txt => 'Message refused by capping system'},
  15 => {status_id => 13, status_txt => 'Unknown error'},
  16 => {status_id => 12, status_txt => 'MT already sent'},
  17 => {status_id => 13, status_txt => 'Client billing problem'},
  18 => {status_id => 13, status_txt => 'Client mobile unreachable'},
  19 => {status_id => 13, status_txt => 'Operator network problem'},
  20 => {status_id => 13, status_txt => 'Operator other error'},
  21 => {status_id => 13, status_txt => 'mt_text too long'},
  22 => {status_id => 13, status_txt => 'function is missing'},
  23 => {status_id => 13, status_txt => 'service_id is required'},
  24 => {status_id => 13, status_txt => 'bad service_id'},
  25 => {status_id => 13, status_txt => 'session_id required'},
  26 => {status_id => 13, status_txt => 'token required'},
  27 => {status_id => 13, status_txt => 'token has expired'},
  28 => {status_id => 13, status_txt => 'bad token'},
  29 => {status_id => 13, status_txt => 'no callback_url defined'},
  30 => {status_id => 13, status_txt => 'operator callback error'},
  31 => {status_id => 13, status_txt => 'subscription_id is missing'},
  32 => {status_id => 13, status_txt => 'invalid subscription_id'},
  33 => {status_id => 13, status_txt => 'operation cancelled'},
  34 => {status_id => 13, status_txt => 'Operation refused'},
  35 => {status_id => 13, status_txt => 'Ip is required'},
  37 => {status_id => 13, status_txt => 'User already subscribe'},
  1051 => {status_id => 13, status_txt => 'Code already used'},
  1052 => {status_id => 13, status_txt => 'Code does not exists'},
  1053 => {status_id => 13, status_txt => 'Tariff does not match'},
};

sub sender_init {
#  my ($proto, $esme) = @_;
} 

sub sender_handle_routines {
#  my ($proto, $esme) = @_;
} 

sub handle_request
{
  my($pkg, $esme, $heap, $q) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});

  #$log->debug(Dumper($q));

  if ( $q->param('phone_number') ) {
    # ��
    my $abonent = substr($q->param('phone_number'), 1) if ($q->param('phone_number') =~ m/^#/);
    $abonent = hex(substr($abonent, 1)) if ($abonent =~ m/^#/);
    my $sms = {
      smsc_id        => $esme->{smsc_id},
      abonent        => $abonent,
      num            => $q->param('shortcode'),
      msg            => encode('cp1251', $q->param('message')),
      data_coding    => 0,
      esm_class      => 0,
      transaction_id => $q->param('mo_id'),
      link_id        => $q->param('mcc_mnc')
    };
  
    my $id = $db->get_insms_id_by_transaction_id($esme->{smsc_id}, $sms->{transaction_id});
    if ($id) {
      $esme->warning("SMS $sms->{transaction_id} was already saved to insms as $id, ignored", "Duplicate sms ignored");
    } else {
      $id = $db->insert_sms_into_insms($sms);
      $log->debug(Dumper($sms));
      $log->info("Message SMS$id was received. From:$sms->{abonent} To:$sms->{num} Text:$sms->{msg}");
    }

    return http_response('200 OK', '');
  } else {
    # SR ��� ��� ���
    my $msg_id = $q->param('mt_id');
    my $error_code = $q->param('error_code');
    my $status_id = $status_tab->{$error_code}{status_id};
    my $status_txt = $status_tab->{$error_code}{status_txt};

    sleep(1); # SR might come before MT saved in outsmsa
    my $outsms = $db->get_sms_by_msg_id($esme->{smsc_id}, $msg_id);
    if ($error_code eq '0' && $outsms) {
      $log->info("Report for message SMS$outsms->{id} was received. Status: $error_code."
        . ($status_txt and " Reason: $status_txt"));
      $outsms->{outbox_status} = $db->set_sms_report_status($outsms->{id}, $status_id, undef, $outsms);
      if ($outsms->{tariff} !~ /^free$/) {
        $db->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1);
      }
      $db->redis_report($outsms, {}) if ($outsms->{registered_delivery});
      return http_response('200 OK', '');
    } else {
      if ($outsms) {
        $esme->warning("Unknown report $error_code\n".Dumper($q));
        return http_response('400 Bad request', '');
      } else {
        $log->warn("OUTSMS: DR message not found message_id=$msg_id");
        return http_response('200 OK', '');
        #$esme->error("MSG$msg_id unknown for SMSC$esme->{smsc_id}", "delivery report for unknown message");
        #return http_response(500, "Delivery report for unknown message");
      }
    }
  }
}

sub sender_handle_sms {
  # �T
  my($pkg, $esme, $sms) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});
  
  my $msg = ($sms->{data_coding} == 0)
    ? $sms->{msg}
    : encode('utf8', decode('UCS-2', $sms->{msg}));
  $msg = xml_escape($msg);

  my $req = {
    login => $esme->{system_id},
    password => $esme->{password},
    mo_id => $sms->{transaction_id},
    mt_text => $msg,
    free => ($sms->{tariff} !~ /^free$/) ? 0 : 1
  };
  $log->debug(Dumper($req));

  my $ua = new LWP::UserAgent;
  $ua->timeout(20);
  my $resp = $ua->post($esme->{url}, $req);
  my $reply = $resp->content();
  my $resp_code = $resp->code();
  $log->debug($reply);
  $log->debug("RespCode = ".$resp_code);

  my $msg_id = get_resp_param($reply, "mt_id");
  my $error_code = get_resp_param($reply, "error_code");

  if ($resp_code eq '200' && $msg_id && $error_code eq '0') {
    $db->set_sms_new_status($sms->{id},2,0,0,$msg_id,1);
    $db->set_sms_delivered_status($sms->{id},1,1);
    $log->info("Message SMS$sms->{id} was delivered to SMSC.");
  } else {
    $db->set_sms_new_status($sms->{id},255,0,255,0,1);
    $db->set_sms_delivered_status($sms->{id},1);
    $log->warn("Message SMS$sms->{id} wasn't delivered to SMSC.");
  }
}

sub get_resp_param {
  my ($string, $param_name) = @_;
  $string .= "\n";
  return unless $string =~ /$param_name:[ ]?(.*)\n/;
  return $1;
}

sub xml_escape {
  my ( $data ) = @_;
  $data =~ s/&/&amp;/sg;
  $data =~ s/</&lt;/sg;
  $data =~ s/>/&gt;/sg;
  $data =~ s/"/&quot;/sg; #"
  return $data;
}

1;
__END__

Example of MO request from Jet Multimedia
POST <Partner MO URL> HTTP/1.1
Host: <hostname>:<port>
Content-Length: <content_length>
Content-Type: application/x-www-form-urlencoded
mo_id=87654321&
phone_number=#21234567890123&
shortcode=81090&
operator_id=31&
mcc_mnc=20810&
keyword=go&
message=go+for+it&end_user_price=3&
end_user_price_currency=EURSending an MT

MT has to be sent to the following URL:
http://billing.virgopass.com/mt.php

Example of MT request from the Partner
Host: <hostname>:<port>
Content-Length: <content_length>
Content-Type: application/x-www-form-urlencoded
login=PartnerLogin&
password=PartnerPassword&
mo_id=87654321&
mt_text=This+is+the+MT+text+the+Partner+wants+to+send+to+the+end-user&
free=0

Receiving a SR
Examples of SR requests from Jet Multimedia
When everything's fine
POST <Partner SR URL> HTTP/1.1
Host: <hostname>:<port>
Content-Length: <content_length>
Content-Type: application/x-www-form-urlencoded
error_code=0&
error_desc=&
mt_id=456781234

When there's an issue
POST <Partner SR URL> HTTP/1.1
Host: <hostname>:<port>
Content-Length: <content_length>
Content-Type: application/x-www-form-urlencoded
error_code=18&
error_desc=Client+mobile+unreachable&
mt_id=456781234

In order to ensure availability of your SR Url a surveillance Probe will send regular request in order to check availability of your plateform
GET <Partner SR URL> HTTP/1.1
Host: <hostname>:<port>
Content-Length: <content_length>
Content-Type: application/x-www-form-urlencoded
event=Monitoring
