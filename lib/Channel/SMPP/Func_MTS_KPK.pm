#!/usr/bin/perl -w
# -*- tab-width: 4; coding: windows-1251-unix -*-
package Channel::SMPP::Func_MTS_KPK;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Math::BigInt;
use Data::Dumper;
use Translit qw/translit_text/;
use Channel::Utils qw(check_limit_pattern);

# Must also be 'Readonly', but module is not installed everywhere
my $TLV_CREDIT_ID = 0x1400;
my $TLV_CREDIT_TOTAL = 0x1401;
my $TLV_CREDIT_REMAINING = 0x1402;
my $TLV_CREDIT_INSTALLMENT = 0x1403;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->{window_limit} = 6; # Count of regions
	$esme->{error_tab} = {
		0x442 => { retry => 5,first => [2,2],other => [5,5] },
		0x58  => { retry => 5,first => [2,2],other => [5,5] },
		0x138a => { retry => 0 },
		0x444 => { retry => 0 },
		0x445 => { retry => 0 },
		0x446 => { retry => 0 },
		0x447 => { retry => 0 },
		0x448 => { retry => 0 },
		0x449 => { retry => 0 },
		0x44a => { retry => 0 },
		0x14 => { retry => 2,first => [10,1,30],other => [300,1,30] },
		0x440 => { retry => 10,first => 1,other => 5 },
		0xa => {retry => 0},
		0xb => {retry => 0},
		0x8 => { retry => 2,first => 30,other => 30 },
		0x77 => {retry => 0},
		0x85 => {retry => 0},
		0x410 => { retry => 0 },
		0x413 => { retry => 0 },
		0x414 => { retry => 0 },
		0x415 => { retry => 10,first => 10,other => 10 },
		0x416 => { retry => 10,first => 10,other => 10 },
		0x441 => { retry => 10,first => 10,other => 10 },
		0x44B => { retry => 0 },
		0x1391 => { retry => 0 },
		0x1392 => { retry => 0 },
		# ������� �� �������
		0x03f2 => { retry => 0 },
		0x03f6 => { retry => 0 },
		0x138b => { retry => 0 },
		0x138c => { retry => 0 },
		0x138d => { retry => 0 },
		0x138e => { retry => 0 },
		0x138f => { retry => 0 },
		_default_ => { retry => 3,first => 3,other => 3 }
	};

	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = ($pdu->{ignore}) ?  0x557 : 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
#	$esme->{error_tab} = {
#		0x442 => { retry => 10,first => [undef,1,undef],other => [undef,1,undef] },
#		0x58  => { retry => 10,first => [undef,1,undef],other => [undef,1,undef] },
#		 _default_ => { retry => 0 }};
	$esme->{error_tab} = {
		0x442 => { retry => 100,first => 0,other => 0 },
		0x58 => { retry => 100,first => 0,other => 0 },
		_default_ => { retry => 0 }};
}

my %final_errors = map { $_ => 1 } ( 0x138B, 0x138C, 0x138F );

sub retry_limit_reached {
    my ( $proto, $esme, $sms, $resp_pdu ) = @_;

    if (not $sms and $resp_pdu) {
        $sms = $esme->{db}{db}->selectrow_hashref(
            "select * from tr_outsms where id = ?", undef, $resp_pdu->{seq},
        );
    }

    return if not $sms;

    if ($sms->{transaction_id} =~ /kpk=\{(\d+)\}/) {
        my $credit_message_id = $1;
        my $data = $esme->{db}{db}->selectrow_hashref(
            "select * from tr_credit_messages where id = ?", undef, $credit_message_id,
        );

        unless ($data) {
            $esme->{log}->error("While processing response for OUTSMS$sms->{id} - CREDIT$credit_message_id not found");
            return;
        }

        my $new_state;
        if ( $data->{credit_type} eq 'initial') {
            $new_state = 'initial_rejected';
        } elsif ( $data->{credit_type} eq 'pending') {
            if ($resp_pdu and $final_errors{$resp_pdu->{status}}) {
                $new_state = 'cancelled';
            } else {
                $new_state = 'rejected';
            }
        } else {
            $esme->{log}->warn(
                "Unexpected tr_credit_messages.credit_type while handling submit_sm_resp",
                "OUTSMS$sms->{id}\nCREDIT$data->{id}\ncredit_type: $data->{credit_type}"
            );
            return;
        }

        $resp_pdu = { } unless defined $resp_pdu;
        $esme->{db}{db}->do(
            "call update_delivered_credit_message(?,?,?,?,?)", undef,
            $data->{id}, $new_state, $data->{credit_total},
            $resp_pdu->{$TLV_CREDIT_REMAINING}, $resp_pdu->{$TLV_CREDIT_INSTALLMENT},
        );

        $esme->{log}->debug("Setting CREDIT$data->{id} state to $new_state due to reached retry limit");
    }
}

sub after_receive
{
	my ( $proto, $esme, $pdu, $sms ) = @_;
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	if (($pdu->{cmd}==5)and(($sms->{esm_class}&0x3C)==0x04)and(!$pdu->{receipted_message_id})) {
		my ($receipted_message_id) = $pdu->{short_message} =~ m/^id:(\d+)\s/;
		my $x = Math::BigInt->new($receipted_message_id);
		($pdu->{receipted_message_id}) = $x->as_hex() =~ /^0x(.*)$/ if ($receipted_message_id);
		$esme->{log}->debug("receipted_message_id filled with $pdu->{receipted_message_id}");
	}

    delete $sms->{link_id};

    my %kpk_allowed_phones = map { $_ => 1 } qw/79856439991 79854149061 79263366614 79120355676 79133790805
           79166800231 79104843112 79151290185 79158320278 79117050756 79160790914
           79101020494 79171014323 79134718765 79133778104 79120311111
           79147064107 79149613927 79146865071 79142049028 79181535026 79108947030/;
    my %kpk_allowed_shortcodes = map { $_ => 1 } qw/4125 4124 7781 4481/;

	if (defined($pdu->{$TLV_CREDIT_ID})) {
#        unless ($kpk_allowed_phones{$sms->{abonent}}) {
#            return reject_credit_message($proto, $esme, $pdu, $sms, "Not for allowed abonent $sms->{abonent}");
#        }
	unless ($kpk_allowed_shortcodes{$sms->{num}}) {
		return reject_credit_message($proto, $esme, $pdu, $sms, "Not for allowed shortcode $sms->{num}");
	}

        unless ($pdu->{$TLV_CREDIT_ID} =~ /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i) {
            return reject_credit_message($proto, $esme, $pdu, $sms, "Wrong CREDIT_ID format - '$pdu->{$TLV_CREDIT_ID}'");
        }

        my ( $credit_message_id ) = $esme->{db}{db}->selectrow_array(
            "select insert_credit_message(?, ?, ?, ?, ?, ?, ?)", undef,
            $sms->{num}, $pdu->{$TLV_CREDIT_ID}, $pdu->{$TLV_CREDIT_TOTAL},
            $pdu->{$TLV_CREDIT_REMAINING}, $pdu->{$TLV_CREDIT_TOTAL} - $pdu->{$TLV_CREDIT_REMAINING},
            $sms->{abonent}, $esme->{smsc_id},
        );

        unless ( $credit_message_id ) {
            return reject_credit_message($proto, $esme, $pdu, $sms, "Saving of additional credit information failed");
        }

        $sms->{transaction_id} = "kpk={$credit_message_id}";
        $esme->{log}->debug("Set 'kontent po karmanu' transaction_id='$sms->{transaction_id}'");
		return 0;
	}

	return 0 if ($sms->{num} =~ m/^(2316|4974|5283)$/);
	$msg = translit_text($msg);
	$sms->{operator_id} = $esme->{db}->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});
	if (
		(($esme->{db}->is_blocked_by_pref($sms->{operator_id}, $msg)) and ($sms->{num} !~ m/^(4974|4462|5283)$/)) or
		($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo')) or
		(($msg =~ /^1/) and ($sms->{num} eq '4016'))
	) {
		$pdu->{ignore} = 1;
		return 1;
	}
	if ((($esme->{smsc_id} == 508) and (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 86400, '^\s*(31|41|51)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 86400, '^\s*(56|57|6543401|6525035|907841|525296|650747|292135|186197|652917)')
	)) or (($esme->{smsc_id} != 508) and (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*24*7, '^\s*(7950118)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*24, '^\s*(x1c|x2c|x3c)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*20, '^\s*(gg|y1|a5|j3|nab|exb|yxy|yy1|yya)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24, '^\s*(79663|22697009|2177822|7986769|1681225|110079255|100154451|10017488|217349|10016359|7982135|7904591)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*20, '^\s*(zz1)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*3, '^\s*(m1|m2)')
	))) {
		$pdu->{ignore} = 1;
		return 1;
	}
	return 0;
}

#sub before_send
#{
#	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
#	if ($sms->{num} eq '770505') {
#		$esme->{db}->set_sms_new_status($sms->{id},0xff,0,0xff);
#		$esme->{db}->set_sms_delivered_status($sms->{id},1);
#		return 0;
#	}
#	return $cmd;
#}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	# BLOCK
	if (($sms->{inbox_id}==0) and (my $errnum=$esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mt'))) {
		$esme->{db}->set_sms_new_status($sms->{id},0xff,undef,$errnum);
		$sms->{outbox_status} = $esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->outbox_status_changed($sms);
		$esme->{log}->info("OUTSMS:$sms->{id} BLOCKED");
		return 0;
	}

	# SKIP
	return 0 unless (allow_mt_send($esme,$sms));
	$esme->{mts_operator_sent}{$sms->{operator_id}} = time() + 60;

    if ($sms->{transaction_id} =~ /kpk=\{(\d+)\}/) {
        my $credit_message_id = $1;
        handle_credit_message_send($proto, $esme, $cmd, $sms, $pdu, $credit_message_id);

        # Override default transaction_id handling, where it is pasted into source_addr field of pdu
        $pdu->{source_addr} = $sms->{num};
    }


	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu,$sms) = @_;

	$esme->{mts_operator_sent}{$sms->{operator_id}} = 0;

	my $rule;
	$rule = {ignore_billing => 1}
		if (($pdu->{status}==0)and is_mt_num($sms->{num}));

    if ($pdu->{$TLV_CREDIT_ID} and $pdu->{status} == 0 ) { # unsuccessful resps are handled in 'retry_limit_reached'
        credit_message_delivered( $esme, $pdu );
    }

	return $rule;
}

sub on_outbox_status {
	my ($proto,$esme,$sms) = @_;
	if (is_mt_num($sms->{num})and($sms->{outbox_status}==2)and not in_shade($esme,$sms)){
		$esme->{db}->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
		$esme->{log}->debug("MSG:$sms->{outbox_id} BILLED");
	}
}

sub credit_message_delivered {
    my ( $esme, $pdu ) = @_;

    my $sms = $esme->{db}{db}->selectrow_hashref(
        "select * from tr_outsms where id = ?", undef, $pdu->{seq},
    );

    return unless $sms;
    return unless $sms->{transaction_id} =~ /kpk=\{(\d+)\}/;
    my $credit_message_id = $1;

    my $data = $esme->{db}{db}->selectrow_hashref(
        "select * from tr_credit_messages where id = ?", undef, $credit_message_id,
    );

    my %state_map = (
        initial => 'initial_delivered',
        pending => 'write_off',
    );

    my $new_state;
    if (exists $state_map{$data->{credit_type}}) {
        $new_state = $state_map{$data->{credit_type}};
        my $installment = sprintf('����� %.02f ���.',
                                  1.18 * $pdu->{$TLV_CREDIT_INSTALLMENT} / 100);
        my $remaining = sprintf('�������� ��������� %.02f ������.',
                                1.18 * $pdu->{$TLV_CREDIT_REMAINING} / 100);
        if ($pdu->{$TLV_CREDIT_REMAINING} == 0) {
            $remaining .= " ������ �� ���������� �� ������� �����������.";
        }
        $esme->{db}->clone_outbox(
            $sms->{outbox_id},
            transaction_id => undef,
            msg => "$installment $remaining ���� *111*456#",
        );
    } else {
        $esme->{log}->warn(
            "Unexpected tr_credit_messages.credit_type while handling submit_sm_resp",
            "OUTSMS$sms->{id}\nCREDIT$data->{id}\ncredit_type: $data->{credit_type}"
        );
        return;
    }

    $esme->{db}{db}->do(
        "call update_delivered_credit_message(?,?,?,?,?)", undef,
        $data->{id}, $new_state, $pdu->{$TLV_CREDIT_TOTAL},
        $pdu->{$TLV_CREDIT_REMAINING}, $pdu->{$TLV_CREDIT_INSTALLMENT},
    );

    $esme->{log}->debug("Setting CREDIT$data->{id} state to $new_state due to successful submit");
    return;
}

sub is_mt_num {
	my $num = shift;
	return  ($num =~ /^77\d{4}$/) ? 1 : 0;
}

sub allow_mt_send {
	my ($esme,$sms) = @_;
	my $now = time();
	return 0 if ($esme->{mts_operator_sent}{$sms->{operator_id}} and ($esme->{mts_operator_sent}{$sms->{operator_id}} > $now));
	if ($sms->{operator_id} == 105) {
		foreach (values(%{$esme->{mts_operator_sent}})) {
			return 0 if ($_ > $now);
		}
	} else {
		return 0 if ($esme->{mts_operator_sent}{105} and ($esme->{mts_operator_sent}{105} > $now));
	}
	return 1;
}

sub in_shade {
	my ($esme,$sms) = @_;
	return 0; # Disable shade on MTS
	return 0 unless (is_mt_num($sms->{num})and($sms->{outbox_status}==2)); # Duplicated condition
	return 0 if ($sms->{num} eq '771053');
	return 0 unless ($sms->{params}{shade_partner} and $sms->{params}{shade_percent});
	$esme->{log}->info("SHADE partner detected. $sms->{params}{subpartner} ($sms->{params}{shade_percent}%) -> $sms->{params}{shade_partner}");
	return 0 unless (int(rand(99)) < $sms->{params}{shade_percent});
	$esme->{log}->info("OUTSMS:$sms->{id} MSG$sms->{outbox_id} SHADED $sms->{params}{shade_partner}");
	$sms->{status} = 255;
	$sms->{outbox_status} = 255;
	$sms->{msg_id} = undef;
	$esme->{db}{db}->do('update tr_outboxa set delivery_status=255 where id=?',undef,$sms->{outbox_id});
	$esme->{db}{db}->do('update tr_outsmsa set status=255,err_num=10119,msg_id=null where id=?',undef,$sms->{id}); #System error ?
	$esme->{db}{db}->do('replace into cyka_payment_ext set id=?,transport_type=99,date=?,service_id=?,abonent=?,operator_id=?,num=?,partner_id=?,cyka_processed=0,test_flag=0,comment=?,is_error=1',undef,$sms->{outbox_id},$sms->{date},$sms->{params}{service_id},$sms->{abonent},$sms->{operator_id},$sms->{num},$sms->{params}{shade_partner},undef);
	return 1;
}

sub reject_credit_message {
	my ( $proto, $esme, $pdu, $sms, $reason ) = @_;

    $esme->{log}->error("Rejected PDU $pdu->{seq} due to '$reason'");

    return 1;
}

sub handle_credit_message_send {
    my ($proto, $esme, $cmd, $sms, $pdu, $credit_message_id) = @_;

    my $data = $esme->{db}{db}->selectrow_hashref(
        "select * from tr_credit_messages where id = ?", undef, $credit_message_id
    );

    unless ($data) {
        $esme->{log}->error("No credit message with id='$credit_message_id'");
        return;
    }

    $esme->{log}->debug("Loaded credit message $credit_message_id details, adding TLV's to PDU");

    $pdu->{$TLV_CREDIT_ID} = $data->{credit_line_id};
    $pdu->{$TLV_CREDIT_INSTALLMENT} =
        $data->{credit_type} eq 'initial' ? $data->{credit_remaining} : $data->{credit_installment};

    return;
}

1;
