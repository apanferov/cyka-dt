#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Sweden;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX qw(pdu_to_sms sms_to_pdu create_test_message process_submit_resp);

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	return 1 if ($sms->{msg} =~ /^stopp?\b/i);
	create_test_message($esme,$sms,
						"Thank you for your payment of 20 SEK. The order is in the second SMS.",
						"SEK2000\x00");
	return 1;
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,$sms->{tariff} || "SEK0\x00");
	return $cmd; # Send SMS
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
