#!/usr/bin/perl -w
package Channel::SMPP::Utils_Utel_Synergy;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Disp::Utils;
use Data::Dumper;
use base 'Exporter';

our @EXPORT = qw/pack_register_service pack_register_service_resp unpack_register_service unpack_register_service_resp/;

sub pack_register_service {
	my ($params) = @_;
	return pack("QZ*CQZ*Z*NCCZ*Z*Q",
			int($params->{request_id}),         # request_id 8 Integer
			$params->{destination_addr},        # destination_addr <=21 C-Octet String
			int($params->{service_state}),      # service_state 1 Integer
			int($params->{service_type_id}),    # service_type_id 8 Integer
			$params->{service_class},           # service_class <=255 C-Octet String
			$params->{service_descr},           # service_descr <=255 C-Octet String
			int($params->{service_cost}),       # service_cost 4 Integer
			int($params->{service_period}),     # service_period 1 Integer
			int($params->{activation_type}),    # activation_type 1 Integer
			$params->{activation_address},      # activation_address <=21 C-Octet String
			$params->{activation_message},      # activation_message <=255 C-Octet String
			int($params->{service_id})          # service_id 8 Integer
		);
}

sub pack_register_service_resp {
	my ($params) = @_;
	return pack("Q",int($params->{service_id}));
}

sub unpack_register_service {
	my ($pdu) = @_;
	($pdu->{request_id},
	 $pdu->{destination_addr},
	 $pdu->{service_state},
	 $pdu->{service_type_id},
	 $pdu->{service_class},
	 $pdu->{service_descr},
	 $pdu->{service_cost},
	 $pdu->{service_period},
	 $pdu->{activation_type},
	 $pdu->{activation_address},
	 $pdu->{activation_message},
	 $pdu->{service_id}) = unpack("QZ*CQZ*Z*NCCZ*Z*Q",$pdu->{data});
 	return 0;
}

sub unpack_register_service_resp {
	my ($pdu) = @_;
	($pdu->{service_id}) = unpack("Q",$pdu->{data});
	return 0;
}

1;
