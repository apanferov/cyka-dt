#!/usr/bin/perl -w
package Channel::SMPP::Func_Poligon;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{transaction_id}) = $pdu->{0x1777} =~ /^(.*)\x00$/;
	($sms->{link_id}) = $pdu->{0x1778} =~ /^(.*)\x00$/;
	return 0;
}

sub on_receive_dr {
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{registered_delivery} = 0 if (($sms->{operator_id} == 159) and ($sms->{sar_segment_seqnum} != 1));
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	delete $pdu->{5122};
	$pdu->{0x1777} = "$sms->{transaction_id}\x00" if ($sms->{transaction_id});
	$pdu->{0x1780} = ($sms->{tariff} eq 'free') ? "\x00" : "\x01";
	return $cmd;
}

1;
