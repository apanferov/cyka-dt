#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Utel_NSS;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Disp::Config;
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{link_id} = get_operator_id($esme, $sms->{abonent});
	return 0;
}

sub get_operator_id {
	my ($esme,$abonent) = @_;
  my ($operator_id) = $esme->{db}{db}->selectrow_array("select operator_id from set_operator_number_pool where locate(number_prefix, ?) = 1 order by length(number_prefix) desc limit 1", undef, $abonent);
  $operator_id = (defined($operator_id) && $operator_id == 106) ? 593 : 595;
#  $operator_id = 106 unless (defined($operator_id));
  return $operator_id;
}

1;

__END__
