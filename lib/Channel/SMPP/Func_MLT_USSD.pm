#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_MLT_USSD;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;


sub on_receive_pdu {
	my ($proto,$esme,$pdu) = @_;
	$pdu->{esm_class} = 0
	  if ($pdu->{cmd} == 0x00000005); # delver_sm
	return 0;
}

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	my ($num,$msg) = $sms->{msg} =~ /^\*(\d*)\*?(.*)\#/;
	$sms->{num} = $num || '0554';
	$sms->{msg} = ($num) ? $msg : $sms->{msg};
	return 1
	  if ($pdu->{message_state} eq "\x08");
	$sms->{ussd_service_op} = 1
	  if ($sms->{ussd_service_op} == 0);
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	delete $pdu->{5122};
	$pdu->{source_addr} = '992907000021';
	$pdu->{service_type} = 'USSD';
	$pdu->{ussd_service_op} = pack('C',$sms->{ussd_service_op})
	  if ($sms->{ussd_service_op});
	$pdu->{data_coding} = 0x48
	  if ($pdu->{data_coding} == 8);
	return $cmd;
}

1;
