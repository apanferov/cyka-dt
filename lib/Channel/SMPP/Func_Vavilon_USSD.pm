#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Vavilon_USSD;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	delete $pdu->{5122};
	$pdu->{service_type} = 'USSD';
	$pdu->{ussd_service_op} = pack('C',$sms->{ussd_service_op})
	  if ($sms->{ussd_service_op});
#	$pdu->{ussd_service_op} = 0x00 if ($sms->{ussd_service_op} == 2);
	$pdu->{ussd_service_op} = "\x20" if ($sms->{ussd_service_op} == 17);
	return $cmd;
}

1;
