#!/usr/bin/perl -w
# -*- tab-width: 4 -*-
package Channel::SMPP::Func_MTS;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Math::BigInt;
use Data::Dumper;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->{error_tab} = {
		0x442 => { retry => 5,first => [2,2],other => [5,5] },
		0x58  => { retry => 5,first => [2,2],other => [5,5] },
		0x138a => { retry => 0 },
		0x444 => { retry => 0 },
		0x445 => { retry => 0 },
		0x446 => { retry => 0 },
		0x447 => { retry => 0 },
		0x448 => { retry => 0 },
		0x449 => { retry => 0 },
		0x44a => { retry => 0 },
		0x14 => { retry => 2,first => [10,1,30],other => [300,1,30] },
		0x440 => { retry => 10,first => 1,other => 5 },
		0xa => {retry => 0},
		0xb => {retry => 0},
		0x8 => { retry => 2,first => 30,other => 30 },
		0x77 => {retry => 0},
		0x85 => {retry => 0},
		0x410 => { retry => 0 },
		0x413 => { retry => 0 },
		0x414 => { retry => 0 },
		0x415 => { retry => 10,first => 10,other => 10 },
		0x416 => { retry => 10,first => 10,other => 10 },
		0x441 => { retry => 10,first => 10,other => 10 },
		0x1391 => { retry => 0 },
		0x1392 => { retry => 0 },
		# ������� �� �������
		0x03f2 => { retry => 0 },
		0x03f6 => { retry => 0 },
		0x138b => { retry => 0 },
		0x138c => { retry => 0 },
		0x138d => { retry => 0 },
		0x138e => { retry => 0 },
		0x138f => { retry => 0 },
		_default_ => { retry => 3,first => 3,other => 3 }
	};


	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = ($pdu->{ignore}) ?  0x557 : 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
}

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	if (($pdu->{cmd}==5)and(($sms->{esm_class}&0x3C)==0x04)and(!$pdu->{receipted_message_id})) {
		my ($receipted_message_id) = $pdu->{short_message} =~ m/^id:(\d+)\s/;
		my $x = Math::BigInt->new($receipted_message_id);
		($pdu->{receipted_message_id}) = $x->as_hex() =~ /^0x(.*)$/ if ($receipted_message_id);
		$esme->{log}->debug("receipted_message_id filled with $pdu->{receipted_message_id}");
	}
	if (defined($pdu->{5120})) {
		$esme->{log}->warn("������� �� ������� 0x1400:$pdu->{5120} 0x1401:$pdu->{5121} 0x1402:$pdu->{5122}");
#		$pdu->{ignore} = 1;
		return 1;
	}
#	if ((($msg =~ m/^\s*49/i) and ($sms->{num} !~ m/^(4974|4462|5283)$/)) or
#		($esme->{db}->is_blocked($sms->{abonent}))) {
#		$pdu->{ignore} = 1;
#		return 1;
#	}
	return 0;
}

#sub before_send
#{
#	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
#	if ($sms->{num} eq '770505') {
#		$esme->{db}->set_sms_new_status($sms->{id},0xff,0,0xff);
#		$esme->{db}->set_sms_delivered_status($sms->{id},1);
#		return 0;
#	}
#	return $cmd;
#}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	# ������ ��������� ����������� ��� ����� � 2 ���/���
	if ($sms->{push_id}) {
#		$esme->update_throttle(1000);
		my $tsl = $esme->throttle_send_limit;
		$esme->update_throttle($tsl-2) if ($tsl > 2);
	}
	$esme->{mts_mt_flag}->{$sms->{id}} = is_mt_num($sms->{num});
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	my $rule;
	my $mts_mt_flag = $esme->{mts_mt_flag}->{$pdu->{seq}};
	$mts_mt_flag = is_mt_num($esme->{db}->get_value("select num from tr_outsms where id=?",undef,$pdu->{seq}))
		unless (defined($mts_mt_flag));
	$rule = {ignore_billing => 1}
		if (($pdu->{status}==0)and($mts_mt_flag));
	delete $esme->{mts_mt_flag}->{$pdu->{seq}};
	return $rule;
}

sub on_receive_dr{
	my ($proto,$esme,$pdu,$s) = @_;
	if (is_mt_num($s->{num})and(($pdu->{message_state} eq "\x02")or($pdu->{short_message}=~/stat:DELIVRD/))){
		$esme->{db}->update_billing($s->{inbox_id}, $s->{outbox_id}, 1);
		$esme->{log}->debug("OUTSMS:$s->{id} BILLED");
	}
}

sub is_mt_num {
	my $num = shift;
	return  ($num =~ /^77\d{4}$/) ? 1 : 0;
}

1;
