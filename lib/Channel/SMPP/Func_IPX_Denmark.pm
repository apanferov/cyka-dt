#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Denmark;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	return 1 if ($sms->{msg} =~ /^stopp?\b/i);
	if ($sms->{num} eq '1609') {
		create_test_message($esme,$sms,
							"Thank you for your payment of 40 DKK. The order is in the second SMS.",
							"DKK4000\x00");
	}
	else {
		create_test_message($esme,$sms,
							"Thank you for your payment of 20 DKK. The order is in the second SMS.",
							"DKK2000\x00");
	}
	return 1;
}

sub on_receive_dr{
	my ($proto,$esme,$pdu,$outsms) = @_;
	if (($pdu->{5889} eq "TDC\x00") or ($pdu->{5889} eq "Telia\x00")) {
		if ($pdu->{message_state} eq "\x02") {
#		if ($pdu->{short_message} =~ m/stat:DELIVRD/) {
			pass_sms_by_tr_id($esme, $outsms->{transaction_id});
		}
	}
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,$sms->{tariff} || "DKK0\x00");
	delete($pdu->{5122});
	my $link_id = $esme->{db}->get_link_id($sms->{abonent},$sms->{smsc_id});
	delete($pdu->{registered_delivery})
	    if (($link_id eq 'Telia') and (not $sms->{tariff}));
	return $cmd; # Send SMS
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
