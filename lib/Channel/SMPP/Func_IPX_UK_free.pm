#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_UK_free;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;

#sub submit_resp {
#	return Channel::SMPP::Func_IPX::submit_resp(@_);
#}

#sub after_receive {
#	return Channel::SMPP::Func_IPX::after_receive(@_);
#}

sub before_send {
	my @params = @_;
	my ($proto,$esme,$cmd,$sms,$pdu) = @params;
	$pdu->{5634} = "GBP0\x00";
	return Channel::SMPP::Func_IPX::before_send(@params);
}

1;
