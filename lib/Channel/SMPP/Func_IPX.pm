#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;
our @EXPORT = qw(pdu_to_sms
				 sms_to_pdu
				 create_test_message
				 process_submit_resp
				 from_cstring
				 to_cstring
				 pass_sms_by_tr_id
				 pass_sms);


#sub on_submit_resp {
sub submit_resp {
	my ($proto,$esme,$pdu) = @_;

	$esme->warning('Negative SUBMIT_SM_RESP',Dumper($pdu))
	  if ($pdu->{status});
	yell(Dumper($pdu),code => 'WARNING',process => $esme->{pr_name},
		 header => "$esme->{pr_name}: Response Code 352",ignore => { log4perl => 1 })
	  if ($pdu->{5638} eq "352\x00");


	if ($pdu->{5642} eq "3\x00") {
		$esme->{db}->do('update tr_outsms set tariff=0 where id=?',undef,$pdu->{seq});
		pass_sms($esme,$pdu->{seq});
		return {_default_ => { retry => 0, first => 0, other => 0 } };
	}
	return {_default_ => { retry => 10, first => 60, other => 60 } }
	  if ($pdu->{5641} eq "true\x00");
	return undef;
}

#sub on_receive_sm
#sub on_receive_dr
sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{transaction_id}) = $pdu->{5894} =~ /^1-(\d*)\x00$/;
	($sms->{link_id}) = $pdu->{5889} =~ /^(.*)\x00$/;
	return 0 if ($sms->{link_id} eq 'Sonera');
	if (($pdu->{cmd}==5)and(($sms->{esm_class}&0x3C)==0x04)) {

	}
	else {
		create_test_message($esme,$sms,'Thanks. The order is in the second SMS.','1');
	}
	return 1;
}

#sub on_send_sm
sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	$pdu->{5633} = "1-$sms->{transaction_id}\x00"
	  if ($sms->{transaction_id});
	$pdu->{5635} = "-1.0\x00";
	return $cmd;
}


# Routines
sub pdu_to_sms {
	my ($pdu,$sms) = @_;
	$sms->{transaction_id} = from_cstring($pdu->{5894});
	$sms->{link_id} = from_cstring($pdu->{5889});
	return undef;
}

sub sms_to_pdu {
	my ($sms,$pdu,$tariff) = @_;
	$pdu->{source_addr} = $sms->{num};
	$pdu->{5633} = to_cstring($sms->{transaction_id})
	  if ($sms->{transaction_id});
	$pdu->{5635} = to_cstring('-1.0');
	$pdu->{5634} = $sms->{tariff};
	$pdu->{5634} = $tariff
	  if ($tariff);
	return undef;
}

sub create_test_message {
	my ($esme,$insms,$text,$tariff) = @_;
	my $msg = { abonent => $insms->{abonent},
				smsc_id => $esme->{smsc_id},
				operator_id => 0,
				num => $insms->{num},
				data_coding => 0,
				esm_class => 0,
				transaction_id => $insms->{transaction_id},
				test_flag => 20,
				tariff => $tariff,
				parts => [$text]};
	$esme->{db}->move_outbox_to_outsms($msg);
	$esme->{log}->info("Testing SMS created.");
	return 0;
}

sub process_submit_resp {
	my ($esme,$pdu,$reject_pass) = @_;

#	$esme->warning('SUBMIT_SM_RESP',Dumper($pdu))
#	  if (($esme->{smsc_id} != 117) and ($esme->{smsc_id} != 118));
	$esme->warning('Response Code 3',Dumper($pdu))
	  if ($pdu->{5638} eq "3\x00");
	$esme->warning('Response Code 352',Dumper($pdu))
	  if ($pdu->{5638} eq "352\x00");

	if ($pdu->{5642} eq "3\x00") { # Billed
		pass_sms($esme,$pdu->{seq})
		  unless ($reject_pass);
		return { retry => 0, first => 0, other => 0 };
	}
	elsif ($pdu->{status} and ($pdu->{5641} eq "true\x00")) { # Temporary error
		return { retry => 10, first => 60, other => 60 }
	}
	elsif ($pdu->{status} == 0x14) { # Message queue full
		return { retry => 0, first => 0, other => 0 };
	}
	return undef;
}

sub pass_sms {
	my ($esme,$outsms_id) = @_;
	my $transaction_id = $esme->{db}->get_value('select transaction_id from tr_outsms where id=?',
												undef,$outsms_id);
	pass_sms_by_tr_id($esme,$transaction_id);
}

sub pass_sms_by_tr_id {
	my ($esme,$tr_id) = @_;
	my $db = $esme->{db};
	my $ignored_id = 
	  $db->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? limit 1',
					 undef,$esme->{smsc_id},$tr_id);
	if ($ignored_id) {
		my $id = $db->move_ignored_to_insms($ignored_id);
		$esme->{log}->info("Message SMS$id was passed to system.");
	}
}

sub from_cstring {
	my ($cstring) = @_;
	my ($text) = $cstring =~ /^(.*)\x00$/;
	return $text;
}

sub to_cstring {
	my ($text) = @_;
	return "$text\x00";
}

1;
