#!/usr/bin/perl -w
package Channel::SMPP::Func_Kartel;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Translit qw/translit_text/;

### ��������� ���������� �������� "44998 40"
sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = ($pdu->{ignore}) ?  0x557 : 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
	routines($proto,$esme);
}

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	$msg = lc(translit_text($msg));
 	
 	foreach (@{$esme->{patterns}}) {
		if ($sms->{msg} =~ m/$_/gi) {
			$pdu->{ignore} = 1;
			return 1;
		}
	}
	if ($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo')) {
		$pdu->{ignore} = 1;
		$sms->{ignore_reason} = 'BlackList';
		return 1;
	}

	return 0;
}

sub routines {
	my ($proto,$esme) = @_;
	my @patterns;
	foreach my $id (1437,1536) {
		my $pattern = $esme->{db}->get_value('select pattern from set_service_t where id=?',undef,$id);
		my $pattern_c;
		eval { $pattern_c = qr/$pattern/i; };
		if ($@) {
			$esme->warning( "Wrong plugin pattern detected for SVC$id");
		} else {
			push(@patterns,$pattern_c);
		}
	}
	$esme->{_patterns} = \@patterns;
	return 600;
}


1;
