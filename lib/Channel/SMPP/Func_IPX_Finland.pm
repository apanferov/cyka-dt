#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Finland;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	return 1 if ($sms->{msg} =~ /^stopp?\b/i);
	return 0 if ($sms->{link_id} eq 'Sonera');
	create_test_message($esme,$sms,
						"Thank you for your payment of 2 EUR. The order is in the second SMS.",
						"EUR200\x00");
	return 1;
}

sub on_receive_dr{
	my ($proto,$esme,$pdu,$outsms) = @_;
	if (($pdu->{5889} eq "DNA\x00") or ($pdu->{5889} eq "Saunalahti\x00") or ($pdu->{5889} eq "Elisa\x00")) {
		if ($pdu->{short_message} =~ m/stat:DELIVRD/) {
			pass_sms_by_tr_id($esme, $outsms->{transaction_id});
		}
	}
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,$sms->{tariff} || "EUR0\x00");
	return $cmd; # Send SMS
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
