#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_MOBIL2;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_init
{
	my ($proto,$esme) = @_;
	$esme->{mobil2_last_push_time} = time();
	return;
}

sub on_receive_sm
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{ussd_service_op} = ($sms->{msg}=~/RENEWSESSION/) ? 1 : 18;
	$sms->{transaction_id} = $sms->{user_message_reference} || undef;
	return 0;
}

sub on_receive_dr
{
	my ($proto,$esme,$pdu,$sms) = @_;
	my $msg_state = unpack('C',$pdu->{message_state});
	if ($msg_state!=2) {
		$esme->{db}->update_blacklist($sms->{abonent},time());
		$esme->{log}->debug("Abonent $sms->{abonent} added to BLACKLIST.");
	}
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	# ������ ��������� ����������� ��� ����� � 1 ���/���
	if ($sms->{push_id}) {
		$esme->update_throttle(1000);
	}
	$pdu->{source_addr} = $sms->{num};
	$pdu->{user_message_reference} = pack('S',$sms->{transaction_id});
	if ($sms->{ussd_service_op}) {
		# USSD ������
		$pdu->{18704} = "\x01" if ($sms->{ussd_service_op} == 17);
		$pdu->{18704} = "\x02" if ($sms->{ussd_service_op} == 32);
		$pdu->{18704} = "\x03" if ($sms->{ussd_service_op} == 33);
	}
	else {
		if ($sms->{tariff}) {
			# ��������������� ������ ���
			$pdu->{18689} = "\x01";
			$pdu->{18691} = uc($sms->{tariff})."\x00";
			$pdu->{short_message} = '';
			$pdu->{message_payload} = ($sms->{msg} =~ /^(cat\d\d?\_\w*):(.*)$/is) ? $2 : $sms->{msg};
		}
		else {
			# ���������� ���
			$pdu->{18689} = "\x00";
		}
		$cmd = Net::SMPP::CMD_data_sm;
	}
	return $cmd;
}

1;
