#!/usr/bin/perl -w
package Channel::SMPP::Func_Megafon_2316;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub on_receive_sm
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{num} = '2316';
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = '79262400751';
	$pdu->{source_addr_ton} = 1;
	$pdu->{source_addr_npi} = 1;
	return $cmd;
}

1;
