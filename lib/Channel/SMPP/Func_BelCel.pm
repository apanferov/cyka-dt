#!/usr/bin/perl -w
package Channel::SMPP::Func_BelCel;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
#	$sms->{data_coding} = 8;
	return 0;
}

sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
#	$pdu->{data_coding} = 4;
#	$pdu->{short_message} = encode('UCS2',decode('cp1251', $sms->{msg}));
	$pdu->{data_coding} = 4 if ($pdu->{data_coding} == 8);
	return $cmd;
}

1;
