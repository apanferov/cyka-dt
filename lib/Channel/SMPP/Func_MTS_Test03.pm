#!/usr/bin/perl -w
package Channel::SMPP::Func_MTS_Test03;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->{error_tab} = {
		0x442 => {
			retry => 5,
			first => [0,60],
			other => [0,120]
		},
		0x57  => {
			retry => 10,
			first => 2,
			other => 5
		},
		0x58  => {
			retry => 5,
			first => [0,60],
			other => [0,120]
		},
		0x14 => {
			retry => 5,
			first => 0,
			other => 0
		},
		0x8 => {
			retry => 10,
			first => 60,
			other => 60
		},
		0xa => {retry => 0},
		0xb => {retry => 0},
		0x77 => {retry => 0},
		0x85 => {retry => 0},
		0x440 => {
			retry => 10,
			first => 60,
			other => 60
		},
		0x441 => {
			retry => 10,
			first => 60,
			other => 60
		},
		0x415 => {
			retry => 10,
			first => 60,
			other => 60
		},
		0x416 => {
			retry => 10,
			first => 60,
			other => 60
		},
		0x3f2 => { retry => 0 },
		0x443 => { retry => 0 },
		0x444 => { retry => 0 },
		0x445 => { retry => 0 },
		0x446 => { retry => 0 },
		0x447 => { retry => 0 },
		0x448 => { retry => 0 },
		0x449 => { retry => 0 },
		0x44a => { retry => 0 },
		0x410 => { retry => 0 },
		0x413 => { retry => 0 },
		0x414 => { retry => 0 },
		0x138a => { retry => 0 },
		0x138b => { retry => 0 },
		0x138c => { retry => 0 },
		0x1391 => { retry => 0 },
		0x1392 => { retry => 0 },
		_default_ => {
			retry => 3,
			first => 3,
			other => 3
		}
	};
}

1;
