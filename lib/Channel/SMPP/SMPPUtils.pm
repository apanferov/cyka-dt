#!/usr/bin/perl
package Channel::SMPP::SMPPUtils;
use strict;
use warnings;
use lib '../../../lib';
use Net::SMPP;
our @EXPORT_OK = qw[decode_submit encode_submit decode_data_sm];

use base qw/Exporter/;

my %default_tab = (
	service_type => '',  # NULL: SMSC defaults, #4> on v4 this is message_class <4#
	source_addr_ton => 0x00, #? not known, see sec 5.2.5
	source_addr_npi => 0x00, #? not known, see sec 5.2.6
	source_addr => '',       ## NULL: not known. You should set this for reply to work.
	dest_addr_ton => 0x00,  #??
	dest_addr_npi => 0x00,  #??
	esm_class => 0x00,      # Default mode (store and forward) and type (5.2.12, p.121)
	protocol_id => 0x00,    ### 0 works for TDMA & CDMA, for GSM set according to GSM 03.40
	priority_flag => 0,     # non-priority/bulk/normal
	schedule_delivery_time => '',  # NULL: immediate delivery
	validity_period => '',  # NULL: SMSC default validity period
	registered_delivery => 0x00,  # no receipt, no ack, no intermed notif
	replace_if_present_flag => 0, # no replacement
	data_coding => 0,       # SMSC default alphabet
	sm_default_msg_id => 0, # Do not use canned message
	short_message => '' );

sub encode_submit {
	my $pdu = shift;
	my %new_pdu = %$pdu;
	foreach (keys (%default_tab)) {
		$new_pdu{$_} = $default_tab{$_} unless defined($new_pdu{$_});
	}
	@_ = (undef,%new_pdu);
	my $data = &Net::SMPP::encode_submit_v34;
	shift;
        $data .= &Net::SMPP::encode_optional_params;
	return $data;
}

sub decode_submit {
        my ( $data ) = @_;
        my $pdu = {data => $data};
        my $mandat_len = Net::SMPP::decode_submit_v34($pdu);
        Net::SMPP::decode_optional_params($pdu, $mandat_len) if $mandat_len < length($pdu->{data});
        return $pdu;
}

sub decode_data_sm {
	my ( $data ) = @_;
	my $pdu = {data => $data};
	my $mandat_len = Net::SMPP::decode_data_sm($pdu);
	Net::SMPP::decode_optional_params($pdu, $mandat_len) if $mandat_len < length($pdu->{data});
	return $pdu;
}

1;
