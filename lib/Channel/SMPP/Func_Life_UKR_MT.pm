#!/usr/bin/perl -w
package Channel::SMPP::Func_Life_UKR_MT;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;


sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	return 0;
}

sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{8208} = is_premium($sms) ? "1\x00" : "0\x00";
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu,$sms) = @_;
	my $rule;
	$rule = {ignore_billing => 1} unless (is_premium($sms));
	return $rule;
}

sub is_premium {
	my $sms = shift;
	return (($sms->{num} =~ /^(46[1-4]0|75[34]0)$/) and ($sms->{tariff} !~ /^free$/i) and ($sms->{sar_segment_seqnum} == $sms->{sar_total_segments}));
}

1;
