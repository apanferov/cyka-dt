#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Norway;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);

	if ($sms->{msg} =~ /^stopp?\b/i) {
	}
	elsif ($sms->{msg} =~ /^(?:hjelp|help|kundeservice|customersupport|customer support|support)$/i) {
		create_test_message($esme,$sms,
			'If you have any questions, feel free to call us 0047 21520088 since 8 a.m. till 5 p.m. or contact us support@alt1.ru', undef);
	}
	else {
		create_test_message($esme,$sms,
			"Thank you for your payment of 16 NOK. The order is in the second SMS.",
			"NOK1600\x00");
	}

	return 1;
}

sub on_receive_dr{
	my ($proto,$esme,$pdu,$outsms) = @_;
	if ($outsms->{tariff} && ($pdu->{5889} eq "NetCom\x00" or $pdu->{5889} eq "Ventelo\x00")) {
		if ($pdu->{short_message} =~ m/stat:DELIVRD/) {
			pass_sms_by_tr_id($esme, $outsms->{transaction_id});
		}
	}
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,$sms->{tariff} || "NOK0\x00");
	$pdu->{5637} = "minimumAge=18\x00";
	my $link_id = $esme->{db}->get_link_id($sms->{abonent},$sms->{smsc_id});
	$pdu->{registered_delivery} = 0
	  if ($link_id eq 'Telenor'); # Telenor Norway does not support DR
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
