#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Infobip;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	if (($sms->{inbox_id}==0) and (my $errnum=$esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mt'))) {
		$esme->{db}->set_sms_new_status($sms->{id},0xff,undef,$errnum);
		$sms->{outbox_status} = $esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->outbox_status_changed($sms);
		$esme->{log}->info("OUTSMS:$sms->{id} BLOCKED");
		return 0;
	}
	return $cmd;
}

sub on_receive_sm {
	return 1;
}


1;
