#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Dimoco;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{transaction_id} = $pdu->{0x1600};
	$sms->{link_id} = $pdu->{0x1602};
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $pdu->{num};
	$pdu->{0x1600} = $sms->{transaction_id};
	$pdu->{0x1601} = '9y1ymM4';
	$pdu->{0x1603} = 'charge' if (is_mt($sms));
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu,$sms) = @_;
	my $rule;
	$rule = {ignore_billing => 1} if (($pdu->{status}==0) and is_mt($sms));
	return $rule;
}

sub on_outbox_status {
	my ($proto,$esme,$sms) = @_;
	if (is_mt($sms) and ($sms->{outbox_status}==12)) {
		$esme->{db}->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
		$esme->{log}->debug("OUTSMS:$sms->{id} BILLED");
	}
}

sub is_mt
{
	my $sms = shift;
        return 1 if ({287=>1,291=>1,292=>1}->{$sms->{operator_id}});
	return 0;
}



1;
