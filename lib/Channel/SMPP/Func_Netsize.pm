#!/usr/bin/perl -w
package Channel::SMPP::Func_Netsize;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{transaction_id}) = $pdu->{0x1405} =~ /^(.*)\x00$/;
	$sms->{link_id} = $pdu->{service_type};
	return 0;
}

sub on_receive_dr{
        my ($proto,$esme,$pdu,$outsms) = @_;
}
										
sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{0x1405} = "$sms->{transaction_id}\x00";
	return $cmd;
}

1;
