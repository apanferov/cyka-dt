#!/usr/bin/perl -w
package Channel::SMPP::Func_Utel_Synergy;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Disp::Utils;
use Data::Dumper;
use Channel::SMPP::Utils_Utel_Synergy;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000201, 'register_service', {run => [1,1,1,1,0]},
		sub {
			my ($self,$pdu) = @_;
			unpack_register_service($pdu);
			my $data = pack_register_service_resp($pdu);
			$self->{smpp}->req_backend(0x80000201,$data,undef,seq => $pdu->{seq});
		});
	$esme->add_pdu_handler(0x80000201, 'register_service_resp', {run => [1,1,1,1,0]},
		sub {
			my ($self,$pdu) = @_;
			my ($db,$log) = ($self->{db},$self->{log});
			$self->dec_wait_resp_count();
			my ($id,$err) = ($pdu->{seq},$pdu->{status});
			unpack_register_service_resp($pdu);
			$self->{log}->debug(Dumper($pdu));
			if ($err==0) {
				$log->info("Service registered (service_id=$pdu->{service_id})");
				$db->set_sms_new_status($id,2,0,$err,undef);
			} else {
				$log->warn("Service not registered (error=$err)");
				$db->set_sms_new_status($id,255,0,$err,undef);
			}
			$db->set_sms_delivered_status($id,1);
		});
}

sub on_receive_pdu {
	my ($proto,$esme,$pdu) = @_;
	unpack_register_service_resp($pdu) if ($pdu->{cmd} == 0x80000201);
	unpack_register_service($pdu) if ($pdu->{cmd} == 0x00000201);
	return 0;
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	if ($sms->{params}{'p.register_service'}) {
		my $params;
		foreach (keys(%{$sms->{params}})) {
			$params->{$1} = $sms->{params}{$_} if ($_ =~ /^p\.(.*)$/);
		}
		$params->{request_id} = $sms->{id};
		$params->{destination_addr} = $sms->{abonent};
		$sms->{pdu} = pack_register_service($params);
		return 0x00000201;
	} else {
		$pdu->{0x1400} = pack("Q",int($sms->{params}{'p.service_id'}));
		return $cmd;
	}
}

1;
