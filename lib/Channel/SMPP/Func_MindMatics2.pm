#!/usr/bin/perl -w
package Channel::SMPP::Func_MindMatics2;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;
use Channel::SMPP::Func_IPX;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	my $db = $esme->{db};
	my $operator = from_cstring($pdu->{5121});
	my $country = from_cstring($pdu->{5122});
	my $tr_id = from_cstring($pdu->{5123});
	my $service_id = from_cstring($pdu->{5124});
	my $price_id = from_cstring($pdu->{5125});
	$sms->{link_id} = "$country-$operator";
	my $transaction_id = "$country-$operator-$tr_id-$service_id-$price_id";
	$sms->{transaction_id} = $transaction_id;

	if ($country==228) { # Genetate free information messages in Switzerland
		if ($sms->{msg} =~ /^\s*info\b/i) {
			create_free_msg($esme,$sms,'SIA "A1: Baltic" www.a1ahelp.net, Customer Care hotline number: 0848548878');
			return 1;
		} elsif ($sms->{msg} =~ /^\s*help\b/i) {
			create_free_msg($esme,$sms,'Customer Care hotline number: 0848548878, e-mail: techsupp@alt1.ru');
			return 1;
		} elsif ($sms->{msg} =~ /^\s*index\b/i) {
			create_free_msg($esme,$sms,'To get the service description please send the request with a short code and a keyword of the service by fax +371 67506186 or at www.a1ahelp.net');
			return 1;
		}
	}

	return 0 if (($country==228) or ($country==232));
	
	create_test_msg($esme,$sms,"Thank you for your payment.");
	return 1;
}

sub on_receive_dr {
	my ($proto,$esme,$pdu,$outsms) = @_;
	if (($pdu->{message_state} eq "\x02") or ($pdu->{short_message} =~ m/stat:DELIVRD/)) {
		if ($outsms->{inbox_id} and $outsms->{outbox_id} and (lc($outsms->{tariff}) ne 'free')) {
			$esme->{db}->update_billing($outsms->{inbox_id}, $outsms->{outbox_id}, 1);
			$esme->{log}->debug("OUTSMS:$outsms->{id} BILLED");
		}
		else {
			pass_sms_by_tr_id($esme, $outsms->{transaction_id});
		}
	}
}


sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	my ($country,$operator,$tr_id,$service_id,$price_id) = $sms->{transaction_id} =~ /^(\d*)-(\d*)-(\d*)-(\d*)-(\d*)$/;
	$pdu->{source_addr} = $sms->{num};
	delete $pdu->{5122};
	$pdu->{5123} = to_cstring($tr_id) if ($tr_id);
	$pdu->{5124} = to_cstring($service_id) if ($service_id);
	$pdu->{5125} = to_cstring($price_id) if ($price_id);
	$pdu->{5126} = to_cstring("0"); 
	$pdu->{5126} = to_cstring("2") if (lc($sms->{tariff}) eq 'free'); # Cancel billing on free tariff
	$pdu->{5126} = to_cstring("1") if (lc($sms->{tariff}) eq 'paid');

	$pdu->{5126} = to_cstring("1") if (($country==228) or ($country==232)); 

	if (($sms->{tariff} eq 'free') and ($country==228)) { # Switzerland
		$pdu->{5126} = to_cstring("1"); # Bill
		$pdu->{5125} = to_cstring("3"); # Price-id
	}
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	my $rule;
	$rule = {ignore_billing => 1} if ($pdu->{status}==0);
	return $rule;
}

sub create_test_msg {
	my ($esme,$insms,$text) = @_;
	my $msg = { abonent => $insms->{abonent},
		    smsc_id => $esme->{smsc_id},
		    operator_id => 0,
		    num => $insms->{num},
		    data_coding => 0,
		    esm_class => 0,
		    transaction_id => $insms->{transaction_id},
		    test_flag => 21,
		    tariff => 'paid',
		    parts => [$text]};
	$esme->{db}->move_outbox_to_outsms($msg);
	$esme->{log}->info("Testing SMS created.");
	return 0;
}

sub create_free_msg {
	my ($esme,$insms,$text) = @_;
	my $msg = { abonent => $insms->{abonent},
	            smsc_id => $esme->{smsc_id},
	            operator_id => 0,
	            num => $insms->{num},
	            data_coding => 0,
	            esm_class => 0,
	            transaction_id => $insms->{transaction_id},
	            test_flag => 21,
	            tariff => 'free',
	            parts => [$text]};
	$esme->{db}->move_outbox_to_outsms($msg);
	$esme->{log}->info("Free information SMS created.");
	return 0;
}

1;
