#!/usr/bin/perl -w
package Channel::SMPP::Func_SMSC_Utel_Synergy;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Disp::Utils;
use Data::Dumper;
use Channel::SMPP::Utils_Utel_Synergy;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000201, 'register_service', {run => [1,1,1,1,1]},
		sub {
			my ($self,$pdu) = @_;
			unpack_register_service($pdu);
			$self->{log}->debug(Dumper($pdu));
			my $data = pack_register_service_resp({service_id => int(rand(0xffffffff))});
			$self->{log}->debug(unpack("H*",$data));
			$self->{smpp}->req_backend(0x80000201,$data,undef,seq => $pdu->{seq});
		});
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;

	return $cmd;
}

1;
