#!/usr/bin/perl -w
package Channel::SMPP::Func_Beeline;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Translit qw/translit_text/;
use Channel::Utils qw/match_num_patterns check_service_patterns check_limit_pattern/;
use Data::Dumper;

my %shade_blacklist = (79651531354 => 1);

### ��������� ���������� �������� "44998 40"
sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = $pdu->{ignore} || 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
	$esme->{error_tab}{0x58} = { retry => 666, first => 0, other => 0 };
	$esme->{window_limit} = 3 * $esme->{output_throttle};
}

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{abonent}, $sms->{transaction_id}) = split(/#/, $sms->{abonent}) if ($sms->{abonent} =~ m/#/);
	($sms->{num}, $sms->{transaction_id}) = split(/#/, $sms->{num}) if ($sms->{num} =~ m/#/);
	$sms->{transaction_id} ||= '';
	return 0 if (($pdu->{esm_class}&0x3C)==0x04); # Delivery Report
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	$msg = lc(translit_text($msg));

	my $source_port = unpack('n', $pdu->{source_port} || '');
	if (my $new_num = is_rebill($sms, $esme, $msg)) {
		my $prefix = $sms->{num}.'|';
		$prefix .= $source_port.'|' if ($source_port);
		$prefix .= $msg.'|'.$sms->{transaction_id};
		$prefix = encode('UCS-2',decode('cp1251', $prefix)) if (($sms->{data_coding} & 12) == 8);
		$sms->{msg} = $prefix;
		$sms->{num} = $new_num;
		return 0;
	}

#!	return 0 if ($sms->{num} =~ m/^(2316|4974|5283|7052\d*|7044|55040?|2535\d*)$/);

	$sms->{operator_id} = $esme->{db}->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});
	if (
		($esme->{db}->is_blocked_by_pref($sms->{operator_id}, $msg)) and ($sms->{num} !~ m/^(4974|4462|5283|7052\d*|7044|55040?|2535\d*)$/) or
		(($msg =~ /^1/) and ($sms->{num} eq '4016'))
	) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'ForbiddenKeyword';
		return 1;
	}
	if ($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo')) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'BlackList';
		return 1;
	}
	if ((($esme->{smsx_id} == 510) and (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 86400, '^\s*(31|41|51)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 86400, '^\s*(56|57)')
	   )) or (($esme->{smsx_id} != 510) and (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*24*7, '^\s*(7950118)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 3, 60*60*24, '^\s*(x1c|x2c|x3c)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 2, 60*60*24, '^\s*(a5|exb|gg|hcc|j3|m1|m2|nab|x1c|x2c|x3c|y1|yxy|yy1|yya)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24, '^\s*(7904591)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*20, '^\s*(zz1)')
	   ))) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'CheckLimitPattern';
		return 1;
	}
	if ($pdu->{callback_num} and ($esme->{smsc_id} == 20) and !{'1111'=>1}->{$sms->{num}}) {
		$sms->{num} = $sms->{num}.'#';
	} elsif ($pdu->{callback_num}) {
		$pdu->{ignore} = 0x66;
		$sms->{ignore_reason} = 'PostponePaymentContent';
		return 1;
	}
	return 0;
}

my $tariff_tab = {'0' => '881000000',
		'101' => '881000101',
		'199' => '881000199',
		'300' => '881000300',
		'400' => '881000400',
		'500' => '881000500',
		'700' => '881000700',
		'800' => '881000800',
		'900' => '881000900',
		'1000' => '881001001',
		'1001' => '881001001',
		'1500' => '881001500',
		'2000' => '881002000',
		'2500' => '881002500',
		'3000' => '881003000',
		'3400' => '881003400',
		'4000' => '881004000',
		'5000' => '881005000',
		'6000' => '881006000',
		'7000' => '881007000',
		'8000' => '881008000',
		'9000' => '881009000',
		'10001' => '881010001',
		'10000' => '881010001',
		'16000' => '881016000',
		'19900' => '881019900',
		'30000' => '881030000' };
my $source_port_tab = {
	'0' => "\x00\x00", 
	'1' => "\x00\x01", # ����������� �������� ����� IVR
	'2' => "\x00\x02", # ���������� �������� ����� IVR
	'3' => "\x00\x03", # ����������� �������� ����� SMS
	'4' => "\x00\x04", # ���������� �������� ����� SMS
	'5' => "\x00\x05", # ����������� �������� ����� WEB
	'6' => "\x00\x06", # ���������� �������� ����� WEB
	'7' => "\x00\x07", # ����������� �������� ����� WAP
	'8' => "\x00\x08", # ���������� �������� ����� WAP
	'9' => "\x00\x09", 
};
my $source_port_tab_7hlp = {
	'IVR' => "\x00\x02", # ���������� �������� ����� IVR
	'SMS' => "\x00\x04", # ���������� �������� ����� SMS
	'WEB' => "\x00\x06", # ���������� �������� ����� WEB
	'WAP' => "\x00\x08", # ���������� �������� ����� WAP
};
sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$sms->{num} =~ s/#$//; # ������� � ���������
	$pdu->{source_addr} = $sms->{num};
	$pdu->{destination_addr} = $sms->{abonent}.'#'.$sms->{transaction_id} if ($sms->{transaction_id});
	if ($sms->{params}{'p.unsubscribe'} and $sms->{params}{'p.service'}) {
#		$pdu->{source_port} = $sms->{params}{'p.type'} eq 'sms' ? "\x00\x04" : "\x00\x06";
		$pdu->{source_port} = $source_port_tab_7hlp->{uc($sms->{params}{'p.type'} || 'SMS')} || "\x00\x04";
		$pdu->{source_addr} = $sms->{params}{'p.service'};
#		$pdu->{short_message} = '';
		$esme->{log}->info("Unsubscribe:".Dumper($pdu));
		return $cmd;
	}
	if ($sms->{params}{'p.forward'}) {
#		$pdu->{source_port} = $sms->{params}{'p.source_port'};
#		$pdu->{source_port} = $source_port_tab->{$sms->{params}{'p.source_port'}} || "\x00\x04";
		$pdu->{source_port} = $source_port_tab->{$sms->{params}{'p.source_port'}} if ( $source_port_tab->{$sms->{params}{'p.source_port'}} );

		$pdu->{destination_addr} = $sms->{params}{'p.source_addr'}.($sms->{params}{'p.transaction_id'} ? '#'.$sms->{params}{'p.transaction_id'} : '');
		$pdu->{source_addr} = $sms->{abonent};

		$esme->{log}->info("Forward:".Dumper($pdu));
		return $cmd;
	}
	if ($sms->{params}{'p.backward'}) {
#		$pdu->{source_port} = $sms->{params}{'p.source_port'};
#		$pdu->{source_port} = "\x00\x06";
#		$pdu->{source_port} = $source_port_tab->{$sms->{params}{'p.source_port'}} || "\x00\x04";
		$pdu->{source_port} = $source_port_tab->{$sms->{params}{'p.source_port'}} if ( $source_port_tab->{$sms->{params}{'p.source_port'}} );

		$pdu->{destination_addr} = $sms->{abonent};
		$pdu->{source_addr} = $sms->{params}{'p.source_addr'}.($sms->{params}{'p.transaction_id'} ? '#'.$sms->{params}{'p.transaction_id'} : '');

		$esme->{log}->info("Backward:".Dumper($pdu));
		return $cmd;
	}
	if (($sms->{inbox_id}==0) and (my $errnum=$esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mt'))) {
		$esme->{db}->set_sms_new_status($sms->{id},0xff,undef,$errnum);
               	$sms->{outbox_status} = $esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->outbox_status_changed($sms);
		$esme->{log}->info("OUTSMS:$sms->{id} BLOCKED");
		return 0;
	}
	if (is_mt_num($sms->{num})) {
		my ($num,$tariff) = get_tariff($sms->{num});
		$pdu->{source_addr} = $num;
		$pdu->{destination_addr} = $sms->{abonent}; # Removes Beeline's bug
#		if (($tariff_tab->{$tariff}) and ($sms->{sar_segment_seqnum}==1)) {
			$pdu->{source_subaddress} = $tariff;
#		} else {
#			$pdu->{source_subaddress} = '881000000';
#		}
	}
	if ($sms->{params}{hidden}) {
		$pdu->{protocol_id} = 0x40;
		$pdu->{short_message} = '';
		delete $pdu->{message_payload};
	}
	return $cmd;
}

sub on_submit_resp {
	my ($proto,$esme,$pdu,$sms) = @_;
	my $rule;
	$rule = {ignore_billing => 1} if (($pdu->{status}==0) and is_mt_num($sms->{num}));
	return $rule;
}


sub on_outbox_status {
	my ($proto,$esme,$sms) = @_;
	if (is_mt_num($sms->{num})and($sms->{outbox_status}==12)and not in_shade($esme,$sms)) {
		$esme->{db}->update_billing($sms->{inbox_id}, $sms->{outbox_id}, 1);
		$esme->{log}->debug("OUTSMS:$sms->{id} BILLED");
	}
}

sub is_mt_num {
	my $num = shift;
	return ($num =~ /^(70520?|7044|55040?|2535)/) ? 1 : 0;
}

sub is_rebill {
	my ($sms, $esme, $msg) = @_;
	if ($sms->{num} =~ /^(2535|5504|7052|9096|9180|9144|9198|9179|2453|2459|8371|9169|9230|9173|9050|9199|9240|8281|8280|9869)\d+$/ || $sms->{num} =~ /^(9096)$/) {
		my $num = $1;
		$num = '9096XX' if ($sms->{num} =~ m/^9096\d\d$/);  # 6 digit numbers
		$num = '9180XX' if ($sms->{num} =~ m/^9180\d\d$/);  # 6 digit numbers
		my $msg_str = $msg;
		$msg_str =~ s/\n|\r//g;
		if (my ($price) = $msg_str =~ /charged\s(([\d\.\,]+))\srub/s) {
			$price =~ s/,/./;
			$price = int($price * 100);
			if ($price) {
				return "$num-$price".'RUR';
			} else {
				$esme->warning("Unknown price for rebill ($price)", Dumper($sms));
				return "$num-".'XXRUR';
			}
		}
		return $num;
	}
	return undef;
}

sub get_tariff
{
	my $num = shift;
	my ($shortcode,$price) = $num =~ /^(70520?|7044|55040?|2535)-?(\d*)$/;
	if ($tariff_tab->{$price}) {
		return $shortcode, $tariff_tab->{$price};
	} else {
		return $shortcode, '881000000';
	}
}

sub in_shade {
	my ($esme,$sms) = @_;
	return 0 if ($esme->{smsc_id} == 510);
	return 0 if ($shade_blacklist{$sms->{abonent}});
	return 0 unless (is_mt_num($sms->{num})and($sms->{outbox_status}==12)); # Duplicated condition
	return 0 if (get_tariff($sms->{num}) eq '881000000');
	return 0 unless ($sms->{params}{shade_partner} and $sms->{params}{shade_percent});
#	$sms->{params}{shade_partner} = 5219 if (($sms->{params}{service_id} == 1650) and ($sms->{params}{subpartner} == 92)); 
	$sms->{params}{shade_partner} = 5219 if (($sms->{params}{service_id} == 1779) and ($sms->{params}{subpartner} == 92)); 
	$esme->{log}->info("SHADE partner detected. $sms->{params}{subpartner} ($sms->{params}{shade_percent}%) -> $sms->{params}{shade_partner}");
	return 0 unless (int(rand(99)) < $sms->{params}{shade_percent});
	eval {
		$esme->{db}{db}->do('insert into cyka_payment_ext set transport_type=99,date=?,service_id=?,abonent=?,operator_id=?,num=?,partner_id=?,cyka_processed=0,test_flag=0,comment=?,is_error=1',undef,$sms->{date},$sms->{params}{service_id},$sms->{abonent},$sms->{operator_id},$sms->{num},$sms->{params}{shade_partner},"MSG:$sms->{outbox_id}");
	};
	return 0 if ($@);
	$esme->{db}{db}->do('update tr_outboxa set delivery_status=13 where id=?',undef,$sms->{outbox_id});
	$esme->{db}{db}->do('update tr_outsmsa set status=13 where id=?',undef,$sms->{id}); # REJECTD
	$sms->{status} = 13;
	$sms->{outbox_status} = 13;
	$esme->{log}->info("OUTSMS:$sms->{id} MSG$sms->{outbox_id} SHADED $sms->{params}{shade_partner}");
	return 1;
}

1;
