#!/usr/bin/perl -w
package Channel::SMPP::Func_Test;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;


sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	return 0;
}

sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
#	$pdu->{source_addr} = "123\x11\x02abc";
#	$pdu->{schedule_delivery_time} = '110330124000000+';
#	$pdu->{schedule_delivery_time} = '000000001000000R';
#	$pdu->{validity_period} = '110330122500000+';
	$pdu->{validity_period} = '000000000500000R';
	if ($sms->{abonent} == 79851354534) {
		$pdu->{protocol_id} = 0x7f;
		$pdu->{data_coding} = 0xf6;
	}
	if ($sms->{num} =~ /^\d{11,20}$/) {
		$pdu->{source_addr_ton} = 1;
		$pdu->{source_addr_npi} = 1;
	}
#	$pdu->{source_addr_ton} = 5;
#	$pdu->{source_addr_npi} = 0;
#	$pdu->{8208} = "1234";
#	$pdu->{registered_delivery} = 0;
	return $cmd;
}

1;
