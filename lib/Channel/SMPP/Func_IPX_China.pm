#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_China;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	return 1 if ($sms->{msg} =~ /^stop\b/i);
	create_test_message($esme,$sms,
						"Thank you for your payment of 3 RMB. The order is in the second SMS.",
						"RMB300\x00");
	return 1;
}

sub on_receive_dr{
        my ($proto,$esme,$pdu,$outsms) = @_;
	if (($pdu->{message_state} eq "\x02") or ($pdu->{short_message} =~ m/stat:DELIVRD/)) {
		pass_sms_by_tr_id($esme, $outsms->{transaction_id});
	}
}
										
sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,$sms->{tariff} || "RMB0\x00");
	$pdu->{5632} = "PrimeConnect\x00" if (!$sms->{tariff});
	return $cmd; # Send SMS
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
