#!/usr/bin/perl -w
package Channel::SMPP::Func_ComGate;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{link_id}) = $pdu->{8528} =~ /^(.*)\x00$/;
	if ($sms->{num} eq '90901') { # MT
		if ($sms->{msg} =~ /^ANO\b/i) {
			pass_sms_by_tr_id($esme,$sms->{abonent});
			return 1;
		} else {
			create_info_message($esme,$sms,"Cena sluzby 299 Kc. Posli ANO pro objednani na $sms->{num}. Vice info 234 718 555 nebo sms HELP na $sms->{num}");
			$sms->{transaction_id} = $sms->{abonent};
			$sms->{num} = '90901299';
			return 1;
		}
	}
	return 0;
}

sub after_receive {
	my ($proto,$self,$pdu,$sms) = @_;
	if (($pdu->{cmd}==5)and(($sms->{esm_class}&0x3C)==0x04)) {
		my ($id,$stat) = $pdu->{short_message} =~ m/^id:(\S+)\s.*stat:(\S+)/;
		$pdu->{message_state} = {DELIVRD => "\x02", ACCEPTD => "\x06", REJECTD => "\x08", EXPIRED => "\x03", UNDELIV => "\x05"}->{$stat};
		$pdu->{receipted_message_id} = $id;
	}
	return 0;
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	$pdu->{service_type} = "P00299" if ($sms->{num} eq '90901299');
	$pdu->{service_type} = "P00000" if (($sms->{num} eq '90901') and ($sms->{test_flag}==30));
	return $cmd;
}

sub create_info_message {
        my ($esme,$insms,$text) = @_;
	my $msg = { abonent => $insms->{abonent},
	            smsc_id => $esme->{smsc_id},
	            operator_id => 0,
	            num => $insms->{num},
	            data_coding => 0,
	            esm_class => 0,
	            test_flag => 30,
		    parts => [$text]};
	$esme->{db}->move_outbox_to_outsms($msg);
	$esme->{log}->info("Info SMS created.");
	return 0;
}

sub pass_sms_by_tr_id {
	my ($esme,$tr_id) = @_;
	my $db = $esme->{db};
	my $ignored_id = $db->get_value('select id from tr_insms_ignored where smsc_id=? and transaction_id=? order by date desc limit 1',undef,$esme->{smsc_id},$tr_id);
	if ($ignored_id) {
		my $id = $db->move_ignored_to_insms($ignored_id);
		$esme->{log}->info("Message SMS$id was passed to system.");
	}
}

1;
