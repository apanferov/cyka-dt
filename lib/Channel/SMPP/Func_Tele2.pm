#!/usr/bin/perl -w
package Channel::SMPP::Func_Tele2;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Data::Dumper;
use Translit qw/translit_text/;
use Channel::Utils qw/match_num_patterns check_service_patterns check_limit_pattern/;

sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = $pdu->{ignore} || 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
}

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	return 0 if (($pdu->{esm_class}&0x3C)==0x04); # Delivery Report
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	$msg = lc(translit_text($msg));
	$sms->{operator_id} = $esme->{db}->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});

	if (($sms->{num} =~ /1121|1131|1151|1161|1899|3121|4015|4016|5013|7781/) and (time < 1398888000)) {
		$pdu->{ignore} = 0x557;
		return 1;
	}

	if (
		$esme->{db}->is_blocked_by_pref($sms->{operator_id}, $msg) or 
		(($msg =~ /^MCN/) and ($sms->{num} ne '1151')) or
		(($msg =~ /^1/) and ($sms->{num} eq '4016'))
	) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'ForbiddenKeyword';
		return 1;
	}
	if ($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo')) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'BlackList';
		return 1;
	}
	if (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24*7, '^\s*(7950118)') or
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24, '^\s*(794784|791265|100176428|10017112|22697009|2177822|7986769|1681225|110079255|100154451|10017488|217349|10016359|79663|7982135|7904591)')
	) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'CheckLimitPattern';
		return 1;
	}
	return 0;
}

1;
