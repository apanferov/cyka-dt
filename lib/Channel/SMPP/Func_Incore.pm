#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Incore;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	my $link = $pdu->{5122};
	my ($num,$tr_id) = split(/#/,$pdu->{destination_addr});
	$sms->{num} = $num;
	$sms->{transaction_id} = "$link-$tr_id";
	$sms->{link_id} = $link;
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	my ($link,$tr_id) = split(/-/,$sms->{transaction_id});
	$link = $link || $esme->{db}->get_link_id_by_op($sms->{operator_id},$sms->{smsc_id});
	$pdu->{5122} = $link;
	$pdu->{source_addr} = "$sms->{num}#$tr_id";
	return $cmd;
}


1;
