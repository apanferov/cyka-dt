#!/usr/bin/perl -w
package Channel::SMPP::Func_Megafon_Free;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{num} = '9950' if ($sms->{num} eq '79262400794');
	$sms->{num} = '8820' if ($sms->{num} eq '79262400775');
	return 0;
}

1;
