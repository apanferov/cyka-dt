#!/usr/bin/perl -w
package Channel::SMPP::Func_A1Systems_push_smsc;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	my ($operator) = $pdu->{5121} =~ /^(.*)\x00$/;
	$sms->{link_id} = $operator;
	return 0;
}

1;
