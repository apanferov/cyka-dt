#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Bite;
use strict;
use warnings;
use base 'Exporter';

use LWP::UserAgent;
use HTTP::Request;
use Data::Dumper;
use Fcntl;
use MIME::Base64;
use XML::Simple;

use Disp::Utils;

use POSIX ":sys_wait_h";

my %num_to_service_id = ( LT => { 1391 => { service_id => 3194, price => '2.7', currency => 'ltl' },
								  1635 => { service_id => 3192, price => '6.7', currency => 'ltl' },
								  1645 => { service_id => 3191, price => '10', currency => 'ltl' },
								  1381 => { service_id => 3190, price => '0.7' , currency => 'ltl' },
								},
						  LV => { 1874 => { service_id => 3114, price => '2.9', currency => 'lvl' },
								  1873 => { service_id => 3113, price => '1.85', currency => 'lvl' },
								  1872 => { service_id => 3112, price => '0.55', currency => 'lvl' },
								  1871 => { service_id => 3111, price => '0.3', currency => 'lvl' },
								}
						);

my $counter = 0;

sub on_init { 
	my ( $proto, $esme ) = @_;

	(@{$esme}{qw/bill_login bill_password url/}) = split /@/, $esme->{url};
	$esme->{log}->debug("Billing info: @{[Dumper([@{$esme}{qw/bill_login bill_password url/}])]}");

}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	if ($sms->{tariff}) {
		$esme->{db}->set_sms_new_status($sms->{id},30,0,0,0);
		delayed_exec {
			$esme->{log}->debug("has forked");
			eval {
				$esme->{db} = $esme->{db}->clone;
				if (bill_account($esme, $sms->{num}, $sms->{abonent})) {
					$esme->{log}->info("OUTSMS:$sms->{id} billed successfully");
					$esme->{db}->do("update tr_outsms set status=0, tariff=null where id=?",undef,$sms->{id});
				}
				else {
					$esme->{log}->info("OUTSMS:$sms->{id} not billed");
					$esme->{db}->set_sms_new_status($sms->{id},254,0,255,0,0);
					$esme->{db}->set_sms_delivered_status($sms->{id},1);
				};
			};
			if ($@) {
				$esme->fatal_error("Forked billing error\nfor OUTSMS @{[Dumper($sms)]}\n$@");
			}
		} 1;
		return 0; # Ignore SMS
	}

	return $cmd; # Send SMS
}

sub on_receive_sm {
	my ( $proto, $esme, $pdu, $sms ) = @_;

	# Rip em all
	while ( waitpid(-1, WNOHANG) > 0 ) {
	}

	$esme->{log}->debug("billing prefork mark");

#     my $flags = fcntl($esme->{smpp}, F_GETFD, 0);
#     fcntl($esme->{smpp}, F_SETFD, $flags & ~FD_CLOEXEC);

	$sms->{transaction_id} = $$ . "-" . time . "-". ($counter++);
	delayed_exec {
		$esme->{log}->debug("has forked");
		eval {
			$esme->{db} = $esme->{db}->clone;
			my $sms_id = $esme->{db}->get_value("select id from tr_insms_ignored where smsc_id = ? and transaction_id = ?", undef,
												$esme->{smsc_id}, $sms->{transaction_id});
			$esme->{log}->debug("Found SMS$sms_id in ignored");
			if (bill_account($esme, $sms->{num}, $sms->{abonent})) {
				$esme->{log}->info("Moving SMS$sms_id from ignored");
				$esme->{db}->move_ignored_to_insms($sms_id);
			};
		};
		if ($@) {
			$esme->fatal_error("Forked billing error\nfor SMS @{[Dumper($sms)]}\n$@");
		}
	} 10;


	return 1;
}


my %error_names = ( -1 => 'System error',
					-2 => 'Insufficient balance',
					-3 => 'Account/service not found',
					-4 => 'Account blocked',
					-5 => 'Account unresolved' );

sub bill_account {
	my ( $esme, $num, $abonent ) = @_;

	my $num_info = $num_to_service_id{$esme->{country}}{$num};

	my $service_id = $num_info->{service_id};
	my $price = $num_info->{price};
	my $currency = $num_info->{currency};

	unless ($price and $currency) {
		$esme->{log}->warn("Prices not found for country '$esme->{country}' and num '$num'");
		return 0;
	}

	unless ($service_id) {
		$esme->{log}->warn("Service_id not found for country '$esme->{country}' and num '$num'");
		return 0;
	}

	my $xml = <<EOF;
<?xml version="1.0"?>
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
				   xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
				   xmlns:xsd="http://www.w3.org/2001/XMLSchema"
				   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
  <SOAP-ENV:Header>
	<provider-service>
	  <service-id>$service_id</service-id>
	</provider-service>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
	<ns6014:billAccount xmlns:ns6014="http://biteapi.metasite.net/billing">
	  <accountId xsi:type="xsd:string">@{[xml_quote($abonent)]}</accountId>
	  <serviceId xsi:type="xsd:int">$service_id</serviceId>
	  <actionId xsi:type="xsd:int">350</actionId>
	  <amount xsi:type="xsd:float">@{[xml_quote($price)]}</amount>
	  <currency xsi:type="xsd:string">@{[xml_quote(uc $currency)]}</currency>
	</ns6014:billAccount>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF

	my $req = HTTP::Request->new('POST', $esme->{url},
								 [ SOAPAction => "http://biteapi.metasite.net/billing/billAccount",
								   Content_type => "text/xml; encoding: utf-8",
								   Authorization => "Basic ".MIME::Base64::encode("$esme->{bill_login}:$esme->{bill_password}"),
								 ],
								 $xml);

	$esme->{log}->debug("Made auth from string '$esme->{bill_login}:$esme->{bill_password}'");
	$esme->{log}->debug("soap request is @{[$req->as_string]}");

	my $ua = new LWP::UserAgent;
	$ua->timeout(90);

	my $resp = $ua->request($req);
# 	my $resp = new HTTP::Response(200, "OK", [], <<EOF);
# <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
#    <soap:Body>
#      <billAccountResponse xmlns="http://biteapi.metasite.net/billing">
#         <transactionId>12354125</transactionId>
#      </billAccountResponse>
#    </soap:Body>
#  </soap:Envelope>
# EOF

	$esme->{log}->debug("billing http response:\n@{[$resp->as_string]}");

	if ($resp->code == 200) {
		my $parsed = XMLin($resp->content, NSExpand => 1);

		$esme->{log}->debug("Parsed billing response: @{[Dumper($parsed)]}");

		if (exists $parsed->{"{http://schemas.xmlsoap.org/soap/envelope/}Body"}) {
			$parsed = $parsed->{"{http://schemas.xmlsoap.org/soap/envelope/}Body"};
		} else {
			$esme->{log}->warn("No SOAP body\nRequest is: @{[$req->as_string]}\nResponse is: @{[$resp->as_string]}");
			return 0;
		}

		if (exists $parsed->{"{http://biteapi.metasite.net/billing}billAccountResponse"}) {
			$parsed = $parsed->{"{http://biteapi.metasite.net/billing}billAccountResponse"};
		} else {
			$esme->{log}->warn("No billAccountResponse in SOAP body\nRequest is: @{[$req->as_string]}\nResponse is: @{[$resp->as_string]}");
			return 0;
		}

		if (exists $parsed->{"{http://biteapi.metasite.net/billing}billAccountReturn"}) {
			$parsed = $parsed->{"{http://biteapi.metasite.net/billing}billAccountReturn"};
		} else {
			$esme->{log}->warn("No billAccountReturn in billAccountResponse body\nRequest is: @{[$req->as_string]}\nResponse is: @{[$resp->as_string]}");
			return 0;
		}

		$esme->{log}->debug("billAccount return value '$parsed'");

		if ($parsed > 0) {
#			$esme->{log}->info("Moving SMS$sms_id from ignored");
#			$esme->{db}->move_ignored_to_insms($sms_id);
			return 1;
		} else {
			$esme->{log}->warn("Billing transaction failed. Deciphered error: '@{[$error_names{$parsed} or '<UNKNOWN>']}'\n@{[$resp->as_string]}");
			return 0;
		}
	} else {
		$esme->{log}->warn("Billing failed with HTTP error\nCode is @{[$resp->code]}\nContent is:\n@{[$resp->content]}");
		return 0;
	}
}

sub soap_return {
    my @ret;
    foreach my $a (@_) {
        my $data;

        if (@$a == 1) {
            $data = SOAP::Data->new(value => $a->[0]);
        }
        if (@$a == 2) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type('');
        }
        if (@$a == 3) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type($a->[2]);
        }
        push @ret, $data;
    }
    return @ret;
}

1;
