#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_SMARTS_Ivanovo;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	return $cmd;
}

sub on_submit_resp
{
	my ($proto,$self,$resp) = @_;
	my $sms = $self->{db}->{db}->selectrow_hashref('select id,abonent,retry_count,UNIX_TIMESTAMP()-UNIX_TIMESTAMP(date) as delay from tr_outsms where id=?',undef,$resp->{seq});
	delete($self->{sended_abonents}->{$sms->{abonent}});
	if ($resp->{status} != 0) {
		if ($sms and ($sms->{retry_count} >= 4) and ($sms->{delay} > 3000)) {
			my $ids = $self->{db}->{db}->selectall_arrayref('select id from tr_outsms where smsc_id=? and abonent=?',undef,$self->{smsc_id},$sms->{abonent});
			my $cnt = 0;
			foreach (@$ids) {
				my $id = $_->[0];
				next if ($id == $sms->{id});
				$self->{db}->set_sms_new_status($id,255,0,0xffffff,0);
				$self->{db}->set_sms_delivered_status($id,1);
				$cnt++;
			}
			$self->{log}->warn("$cnt pdus of $sms->{abonent} deleted from OUTSMS.");
		}
	}
	return undef;
}

1;
