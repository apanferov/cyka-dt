#!/usr/bin/perl -w
package Channel::SMPP::Func_Tele2v4rus;
use strict;
use warnings;
use base 'Exporter';
use LWP::UserAgent;
use HTTP::Request;
use Data::Dumper;
use Fcntl;
use MIME::Base64;
use XML::Simple;
use POSIX ":sys_wait_h";
use Disp::Utils;
use Translit qw/translit_text/;
use Encode qw(encode decode);
use Channel::Utils qw(check_limit_pattern);

my %number_comments = ( 
	176 => {
		user => "K9853856",
#		password => "dhR79PA_eYCJMyg_BNngE6re",
		password => "e5166a5c974852589a93c5b6a8016a4c",
		'1121' => {price=>  "354", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1121;000000;001;001;003;021;                    021999;00;        ;       "},
		'1131' => {price=> "1652", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1131;000000;001;001;003;021;                    021999;00;        ;       "},
		'1141' => {price=> "2714", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1141;000000;001;001;003;021;                    021999;00;        ;       "},
		'1151' => {price=> "4095", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1151;000000;001;001;003;021;                    021999;00;        ;       "},
		'1161' => {price=>"11175", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1161;000000;001;001;003;021;                    021999;00;        ;       "},
		'1899' => {price=> "8968", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1899;000000;001;001;003;021;                    021999;00;        ;       "},
		'3121' => {price=>"7000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                3121;000000;001;001;003;021;                    021999;00;        ;       "},
		'3747' => {price=>"24999", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                3747;000000;001;001;003;021;                    021999;00;        ;       "},
		'4012' => {price=>"20000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4012;000000;001;001;003;021;                    021999;00;        ;       "},
		'4015' => {price=>"10000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4015;000000;001;001;003;021;                    021999;00;        ;       "},
		'4016' => {price=>"15000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4016;000000;001;001;003;021;                    021999;00;        ;       "},
		'4124' => {price=>"25000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4124;000000;001;001;003;021;                    021999;00;        ;       "},
		'4125' => {price=> "4201", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4125;000000;001;001;003;021;                    021999;00;        ;       "},
		'4481' => {price=>"24999", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4481;000000;001;001;003;021;                    021999;00;        ;       "},
		'5013' => {price=> "4484", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                5013;000000;001;001;003;021;                    021999;00;        ;       "},
		'5014' => {price=>"14160", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                5014;000000;001;001;003;021;                    021999;00;        ;       "},
		'5537' => {price=>"20414", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                5537;000000;001;001;003;021;                    021999;00;        ;       "},
		'6681' => {price=>"24999", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                6681;000000;001;001;003;021;                    021999;00;        ;       "},
		'7781' => {price=>"20414", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                7781;000000;001;001;003;021;                    021999;00;        ;       "},
		},
	643 => {
		user => "K9853856",
		password => "e5166a5c974852589a93c5b6a8016a4c",
		'1121' => {price=>  "354", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1121;000000;001;001;003;021;                    021999;00;        ;       "},
		'1131' => {price=> "1652", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1131;000000;001;001;003;021;                    021999;00;        ;       "},
		'1151' => {price=> "4095", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1151;000000;001;001;003;021;                    021999;00;        ;       "},
		'1161' => {price=>"11175", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1161;000000;001;001;003;021;                    021999;00;        ;       "},
		'1899' => {price=> "8968", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                1899;000000;001;001;003;021;                    021999;00;        ;       "},
		'3121' => {price=>"7000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                3121;000000;001;001;003;021;                    021999;00;        ;       "},
		'4015' => {price=>"10000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4015;000000;001;001;003;021;                    021999;00;        ;       "},
		'4016' => {price=>"15000", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                4016;000000;001;001;003;021;                    021999;00;        ;       "},
		'5013' => {price=> "4484", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                5013;000000;001;001;003;021;                    021999;00;        ;       "},
		'7781' => {price=>"20414", currency=>"RUB", cd=>"A1,web-services", ex=>"01;      ;                7781;000000;001;001;003;021;                    021999;00;        ;       "},
		},
	);

my %currency_config = ( SEK => { code => 1 },
			NOK => { code => 2 },
			DKK => { code => 3 },
			EEK => { code => 4 },
			EUF => { code => 5 },
			EUH => { code => 6 },
			EUL => { code => 7 },
			LVL => { code => 8, vat => 21 },
			LTL => { code => 9, vat => 21 },
			EUA => { code => 10 },
			RUB => { code => 11, vat => 18 },
			USD => { code => 12 },
			HRK => { code => 13 },
			CHF => { code => 14 },
	);

my %trans_errs = (
	-32400 => {msg => 'System did not respond correctly',handler => [\&err_reject, 3, 10]},
	1 => {msg => 'ContentProvider does not exist'},
	2 => {msg => 'Amount out of range'},
	3 => {msg => 'Customer does not exist',handler => [\&err_remove_customer, 0, 0]},
	4 => {msg => 'Purchase within time restriction. Customer must not be charged with multiple transactions for the same service'},
	5 => {msg => 'Invalid routing information'},
	6 => {msg => 'To many requests in timeframe',handler => [\&err_reject, 3, 0]},
	7 => {msg => 'To many pending IN requests',handler => [\&err_reject, 3, 0]},
	8 => {msg => 'Customer does not exists in IN',handler => [\&err_remove_customer, 0, 0]},
	9 => {msg => 'Customer account balance to low'},
	10 => {msg => 'IN read timeout during balance check',handler => [\&err_reject, 3, 10]},
	11 => {msg => 'IN read timeout during withdrawal',handler => [\&err_reject, 3, 10]},
	12 => {msg => 'Communiaction error before withdrawal',handler => [\&err_reject, 3, 10]},
	13 => {msg => 'Communiaction error during withdrawal',handler => [\&err_reject, 3, 10]},
	14 => {msg => 'Invalid CHARGE_ACCOUNT'},
	15 => {msg => 'Invalid VAT_PERCENTAGE'},
	16 => {msg => 'Invalid CURRENCY'},
	17 => {msg => 'IN Node not connected'},
	18 => {msg => 'The content provider and customer are of different nationalities'},
	19 => {msg => 'Requested currency differs from provider currency'},
	20 => {msg => 'Invalid provide currency. The currency is not correct'},
	21 => {msg => 'Max number of outstanding CGB requests exceeded',handler => [\&err_reject, 3, 10]},
	22 => {msg => 'Customer has requested barring services for MMS or Content or both MMS and Content'},
	23 => {msg => 'Internal error'},
	24 => {msg => 'Internal error'},
	25 => {msg => 'Internal error'},
	26 => {msg => 'Customer account has either expired, or never been activeated for the content service.',handler => [\&err_remove_customer, 0, 0]},
	27 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	28 => {msg => 'Internal error - payment broker',handler => [\&err_reject, 3, 0]},
	29 => {msg => 'Internal error/timeout',handler => [\&err_reject, 3, 0]},
	30 => {msg => 'Internal error - transaction already open',handler => [\&err_reject, 3, 0]},
	31 => {msg => 'Internal error - transaction busy',handler => [\&err_reject, 3, 0]},
	32 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	33 => {msg => 'Internal error - possible overload',handler => [\&err_reject, 3, 0]},
	35 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	36 => {msg => 'Customer under 16 yrs'},
	37 => {msg => 'Age check failed'},
	38 => {msg => 'Customer under 18 yrs'},
	39 => {msg => 'Customer info'},
	40 => {msg => 'Customer info'},
	41 => {msg => 'Customer info error'},
	42 => {msg => 'Customer info'},
	43 => {msg => 'Internal error',handler => [\&err_reject, 3, 10]},
	44 => {msg => 'Duplicate found',handler => [\&err_duplicate, 0, 0]},
	45 => {msg => 'Duplicate function error',handler => [\&err_reject, 3, 10]},
	46 => {msg => 'Duplicate found',handler => [\&err_duplicate, 0, 0]},
	47 => {msg => 'Incoming queue timeout1',handler => [\&err_reject, 3, 10]},
	48 => {msg => 'Incoming queue timeout2',handler => [\&err_reject, 3, 10]},
	50 => {msg => 'Customer has requested barring service for adult content'},
	51 => {msg => 'Limit for Content exceeded'},
	52 => {msg => 'Limit for MMS exceeded'},
	53 => {msg => 'Limit for Volume exceeded'},
	54 => {msg => 'Customer not Active'},
	# russian transactions specific
	1001 => {msg => 'Unknown MSISDN',handler => [\&err_reject, 3, 0]},
	1002 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	1003 => {msg => 'Not enough credit',handler => [\&err_reject, 3, 0]},
	1004 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	1005 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	1006 => {msg => 'Internal error',handler => [\&err_reject, 3, 0]},
	# X is a status of original transaction.
	'999X' => {msg => 'Duplicate found',handler => [\&err_duplicate, 0, 0]}
);

my %rc_errs = (
	200 => 'Success',
	201 => 'Partial',
	202 => 'Accepted',
	203 => 'Already Done',
	204 => 'Created',
  
	400 => 'UnknownURI',
	401 => 'ReadOnlyURI',
	402 => 'UnknownMethod',
	410 => 'UnknownKey',
	411 => 'ReadonlyKey',
	412 => 'SubspaceNotAllowed',
	413 => 'SubspaceNeeded',
	414 => 'UnknownSubspace',
	415 => 'DuplicateKey',
	416 => 'ValueNotSet',
  
	420 => 'ParameterUnknown',
	421 => 'ParameterNeeded',
	422 => 'ParameterSyntaxError',
	423 => 'ParameterInvalid',
	424 => 'ParameterLengthInvalid',
	425 => 'ParameterIllegalCharacters',
  
	430 => 'AuthenticationFailed',
	431 => 'AuthorizationFailed',
	432 => 'Suspended',
	433 => 'Disabled',
  
	440 => 'ClientNotAuthenticated',
	441 => 'ClientNotAuthorized',
	442 => 'UserNotAuthenticated',
	443 => 'UserNotAuthorized',
	444 => 'NotEncypted',
  
	450 => 'ValueUndefined',
	451 => 'SearchCriteriaTooWide',
	452 => 'ObjectNotFound',
	453 => 'OperationNotAllowed',
  
	#Server Errors	
	500 => 'Unavailable',
	501 => 'Aborted',
	502 => 'ProtocolError',
	503 => 'Cluster',
  
	510 => 'Declined',
  
	520 => 'NotConfigured',
	521 => 'NotImplemented',
  
	530 => 'TransactionFailed: often indicates a syntax error in the XML or incompatibility between XML and wsdl (possible with respect to the defined value). Where this is difficult to resolve, test with the XML from this document, and change only that which needs to be changed.',
	531 => 'OutOfResources',
	532 => 'UnknownSession',
	533 => 'Again',
	534 => 'Timeout',
  
	540 => 'BackendUnavailablePermanently',
	541 => 'BackendUnavailableTemporarily'
	);

sub on_stop {
	my ($proto,$esme) = @_;
	my ($i,$kid) = (0,0);
	while ( ($kid > -1) and ($i < 120) ) {
		$kid = waitpid(-1, WNOHANG);
		$esme->{log}->debug("waitpid=$kid");
		if ($kid == 0) { sleep(1); $i++; }
	}
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	my ($transaction_id) = split(/\-/,$sms->{transaction_id});
	$pdu->{source_addr} = $sms->{num}.'#'.$transaction_id if ($transaction_id);
#	if ($sms->{tariff}) { # MT Premium # Don't use it
#		$esme->{db}->set_sms_new_status($sms->{id},30,0,0,0);
#		delayed_exec {
#			$esme->{log}->debug("has forked");
#			eval {
#				$esme->{db} = $esme->{db}->clone;
#				if (bill_account($esme, $sms->{num}, $sms->{abonent}, $sms->{id} % 2147483647)) {
#					$esme->{log}->info("OUTSMS:$sms->{id} billed successfully");
#					$esme->{db}->do("update tr_outsms set status=0, tariff=null where id=?",undef,$sms->{id});
#				}
#				else {
#					$esme->{log}->info("OUTSMS:$sms->{id} not billed");
#					$esme->{db}->set_sms_new_status($sms->{id},254,0,255,0,0);
#					$esme->{db}->set_sms_delivered_status($sms->{id},1);
#				};
#			};
#			if ($@) {
#				$esme->fatal_error("Forked billing error\nfor OUTSMS @{[Dumper($sms)]}\n$@");
#			}
#		} 1;
#		return 0; # Ignore SMS
#	}

	return $cmd; # Send SMS
}

my $counter = 1;
sub on_receive_sm {
	my ( $proto, $esme, $pdu, $sms ) = @_;
	# Rip all zombies
	while ( waitpid(-1, WNOHANG) > 0 ) { }

	return 0 unless ($number_comments{$esme->{smsc_id}}{$sms->{num}});
	$esme->{log}->debug("CBG payment shortcode $sms->{num} found");
	return 0 if (time() < 1381176000);
#	return 0;

	$sms->{operator_id} = $esme->{db}->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});
	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	$msg = lc(translit_text($msg));
	if ($esme->{db}->is_blocked_by_pref($sms->{operator_id}, $msg) or
		(($msg =~ /^MCN/) and ($sms->{num} ne '1151'))) {
		$pdu->{ignore} = 0x0;
		$sms->{ignore_reason} = 'ForbiddenKeyword';
		return 1;
	}
	if ($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo')) {
		$pdu->{ignore} = 0x0;
		$sms->{ignore_reason} = 'BlackList';
		return 1;
	}
	if (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24*7, '^\s*(7950118)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24, '^\s*(794784|791265|100176428|10017112|22697009|2177822|7986769|1681225|110079255|100154451|10017488|217349|10016359|79663|7982135|7904591)')
	) {
		$pdu->{ignore} = 0x0;
		$sms->{ignore_reason} = 'CheckLimitPattern';
		return 1;
	}

	$esme->{log}->debug("CBG billing prefork mark");
	$sms->{transaction_id} .= "-" . $$ . "-" . time . "-". ($counter++);
	delayed_exec {
		$esme->{log}->debug("CBG has forked");
		eval {
			$esme->{db} = $esme->{db}->clone;
			my $sms_id = $esme->{db}->get_value("select id from tr_insms_ignored where smsc_id = ? and transaction_id = ?", undef, $esme->{smsc_id}, $sms->{transaction_id});
			$esme->{log}->debug("CBG:$sms_id found in ignored");
			if (bill_account($esme, $sms->{num}, $sms->{abonent}, $sms_id)) {
				$esme->{log}->info("CBG:$sms_id billed successfully");
				$esme->{log}->debug("CBG:$sms_id moving from ignored");
				$esme->{db}->move_ignored_to_insms($sms_id);
			} else {
				$esme->{log}->info("CBG:$sms_id not billed");
			}
		};
		if ($@) {
			$esme->fatal_error("CBG forked billing error\nfor SMS @{[Dumper($sms)]}\n$@");
		}
	} 5;
	$pdu->{ignore} = 0x0;
        $sms->{ignore_reason} = 'PaymentWait';
	return 1;
}


sub bill_account {
	my ( $esme, $num, $abonent, $sms_id ) = @_;
	my $smsc_id = $esme->{smsc_id};

#	my ( $price, $currency ) = $esme->{db}->{db}->selectrow_array("select price_in, currency from set_num_price where operator_id = ? and num = ? and date_start <= date(now()) order by date_start desc limit 1",undef,$esme->{operator_id},$num);
	my $price = $number_comments{$smsc_id}{$num}{price};
	my $currency = $number_comments{$smsc_id}{$num}{currency};
	# @{[int($price * 100)]}

	unless ($price and $currency) {
		$esme->fatal_error("CBG:$sms_id Prices not found for num $num and operator $esme->{operator_id}");
		return 0;
	}

	my $vat_str = "";

	my $vat = $currency_config{uc $currency}{vat};
	$vat = &$vat if ref($vat) eq 'CODE';
	my $comment = $number_comments{$smsc_id}{$num}{cd} || $number_comments{$smsc_id}{_default_}{cd};
	my $xtradata = $number_comments{$smsc_id}{$num}{ex} || $number_comments{$smsc_id}{_default_}{ex};

	my $currency_code = $currency_config{uc $currency}{code};

	$vat_str = qq{<T2api:item><T2api:key>VAT</T2api:key><T2api:valueUnsigned>${vat}00</T2api:valueUnsigned></T2api:item>} if $vat;


	my $ua = new LWP::UserAgent;
	$ua->timeout(30);
	my($status, $status0) = (-32400);
	my $retries = 0;
	my $ii = 1;

	do {
		my $tr_id = ($sms_id % 100000000)*10 + $ii;

		my $xml = <<EOF;
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:xsd="http://www.w3.org/1999/XMLSchema"
  xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
  SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Body>
<T2api:Call xmlns:T2api="urn:/T2api/Proto/Soap">
 <T2api:request>
   <T2api:url>CBG</T2api:url>
   <T2api:method>Purchase</T2api:method>
   <T2api:kwargs>
     <T2api:item>
	   <T2api:key>Version</T2api:key>
	   <T2api:valueUnsigned>208</T2api:valueUnsigned>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>ContentType</T2api:key>
	   <T2api:valueUnsigned>2</T2api:valueUnsigned>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>Currency</T2api:key>
	   <T2api:valueUnsigned>$currency_code</T2api:valueUnsigned>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>Amount</T2api:key>
	   <T2api:valueUnsigned>$price</T2api:valueUnsigned>
	 </T2api:item>
     $vat_str
     <T2api:item>
	   <T2api:key>OriginatingCustomerId</T2api:key>
	   <T2api:valueString>00@{[$abonent]}</T2api:valueString>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>username</T2api:key>
	   <T2api:valueString>$number_comments{$smsc_id}{user}</T2api:valueString>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>password</T2api:key>
	   <T2api:valueString>$number_comments{$smsc_id}{password}</T2api:valueString>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>ContentDescription</T2api:key>
	   <T2api:valueString>$comment</T2api:valueString>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>ProviderTransactionId</T2api:key>
	   <T2api:valueUnsigned>$tr_id</T2api:valueUnsigned>
	 </T2api:item>
     <T2api:item>
	   <T2api:key>ReferenceID</T2api:key>
	   <T2api:valueUnsigned>0</T2api:valueUnsigned>
	 </T2api:item>
     <T2api:item>
      <T2api:key>XtraData</T2api:key>
      <T2api:valueString>$xtradata</T2api:valueString>
     </T2api:item>
   </T2api:kwargs>
 </T2api:request>
</T2api:Call></SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF

		my $req = HTTP::Request->new('POST', $esme->{url},
								 [ # SOAPAction => "http://biteapi.metasite.net/billing/billAccount",
								  Content_type => "text/xml; encoding: utf-8",
								  # Authorization => "Basic ".MIME::Base64::encode("$esme->{system_id}:$esme->{password}"),
								 ],
								 $xml);

		$esme->{log}->debug("CBG:$sms_id ($ii) http request:\n@{[$req->as_string]}");
  		my $resp = $ua->request($req);

		$esme->{log}->debug("CBG:$sms_id ($ii) http response:\n@{[$resp->as_string]}");

		eval
		{
			die "CBG:$sms_id ($ii) Erroneous http code: ".$resp->code."\n"
		  		unless ($resp->code == 200);

			my $node = XMLin($resp->content, ForceContent => 0);
			$node = _getnode($node, 'SOAP-ENV:Body/T2api:Response');
		
			my $rc = _getnode($node, 'T2api:rc');
			die "CBG:$sms_id ($ii) Erroneous return code: @{[$rc_errs{$rc} or $rc]}\n"
				if ($rc != 200);

			$node = _getnode($node, 'T2api:data/T2api:item',
				'T2api:key' => 'CBGRESPONSE');

			$status = _getnode($node, 'T2api:valueDict/T2api:item',
				'T2api:key' => 'Status')->{'T2api:valueUnsigned'};

			## While we dont use TransactionId, no need to get it.
			#my $trans_id = _getnode($node, 'T2api:valueDict/T2api:item',
			#	'T2api:key' => 'TransactionId')->{'T2api:valueString'};
		
			die "CBG:$sms_id ($ii) Billing has failed with status: @{[$trans_errs{$status}->{msg} or $status]}\n"
				if ($status);
			$retries = 0;
		};
		if ($@)
		{
			$esme->{log}->warn($@);

			my $key = ($status >= 9990) ? '999X' : $status;

			my $handler = exists $trans_errs{$key} && exists $trans_errs{$key}->{handler}
				? $trans_errs{$key}->{handler}
				: [\&err_reject, 0, 0]; 

			if ($status != $status0)
			{
				$status0 = $status;
				$retries = 0;
			}
			$retries ||= $handler->[1];
			&{ $handler->[0] }($esme, $status, qq/@{[$trans_errs{$key}->{msg} or "Status=$status"]}/, $sms_id);
			sleep($handler->[2]) if ($handler->[2]);
		}
		++$ii; # number of retry
	}
	while (--$retries > 0);

	return !$status;
}

sub soap_return {
    my @ret;
    foreach my $a (@_) {
        my $data;

        if (@$a == 1) {
            $data = SOAP::Data->new(value => $a->[0]);
        }
        if (@$a == 2) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type('');
        }
        if (@$a == 3) {
            $data = SOAP::Data->name($a->[1] => $a->[0]);
            $data->type($a->[2]);
        }
        push @ret, $data;
    }
    return @ret;
}

# transactions error-handlers
sub err_reject
{
}

sub err_remove_customer
{
	my($esme, $status, $msg, $trans_id) = @_;
	$esme->warning($msg,
		'CBG: Remove the A-number immediately from any existing customer database. '
		. 'The A-number should not be resent to CBG unless the user has  done a new request.');
}

sub err_duplicate
{
	my($esme, $status, $msg, $trans_id) = @_;
	$esme->{log}->warn($msg);
	$esme->warning($msg,
	  "CBG: Duplicate transaction found; Status=$status, TransactionId=$trans_id");
}


sub _getnode ($$@)
{
	my($xml, $nodes, $key, $value) = @_;
	
	for my $node (split '/', $nodes)
	{
		$xml = exists $xml->{$node} ? $xml->{$node} : die "$node not found in ($nodes)\n";
	}
	if (defined $key)
	{
		$xml = [$xml] unless (ref($xml) eq 'ARRAY');
		my $found = 0;
		foreach my $h (@$xml)
		{
			if ($h->{$key} eq $value)
				{ $found = 1; $xml = $h; last; }
		}

		die "No such key $value found in ($nodes/$key)\n"
			if (!$found);
	}
	$xml;
}

sub run_fake {
	my ($proto, $esme) = @_;
	while (<STDIN>) {
		my ($id,$abonent,$num) = split /[\t\n]/; 
		$esme->{log}->debug("FAKE: $id $abonent $num");
		my $res = bill_account($esme, $num, $abonent, $id) || 0;
		$esme->{log}->info("FAKEBILL: $id $abonent $num $res");
	}
}

1;
