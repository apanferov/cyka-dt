#!/usr/bin/perl -w
package Channel::SMPP::Func_Mari;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;

sub on_receive_sm
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{msg} = encode('cp1251',decode('koi8-r', $sms->{msg}));
	return 0;
}

1;
