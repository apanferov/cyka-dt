#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_BWC_USSD;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	delete $pdu->{5122};
	$pdu->{service_type} = 'USSD';
	$pdu->{ussd_service_op} = pack('C',$sms->{ussd_service_op})
	  if ($sms->{ussd_service_op});
	return $cmd;
}

1;
