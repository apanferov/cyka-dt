# -*- encoding: utf-8; tab-width: 8 -*-
package Channel::SMPP::Func_MTS_Credit_Testing;
use strict;
use warnings;
use utf8;
use Carp;
use English '-no_match_vars';

# use version; our $VERSION = qv('1.0.0');

use UUID::Tiny;
use Data::Dumper;
use POSIX qw/strftime/;
use List::Util qw/min/;

# Must also be 'Readonly', but module is not installed everywhere
my $TLV_CREDIT_ID = 0x1400;
my $TLV_CREDIT_TOTAL = 0x1401;
my $TLV_CREDIT_REMAINING = 0x1402;
my $TLV_CREDIT_INSTALLMENT = 0x1403;

sub on_init {
	my ( $proto, $esme ) = @_;

	$esme->add_pdu_handler(
            0x00000004, 'submit_sm', {
                run => [1,0,0,0,1],
            },
            sub {
                my ( $self, $pdu ) = @_;
                $self->process_data_pdu($pdu);
                die("CONNECT_ERROR_:Can't write SUBMIT_SM_RESP to socket.") if (!$self->{select}->can_write(0));
                my $status = $pdu->{resp_status} || 0;
                my @add_tlv;
                for my $tlv ($TLV_CREDIT_ID, $TLV_CREDIT_TOTAL, $TLV_CREDIT_INSTALLMENT, $TLV_CREDIT_REMAINING) {
                    push @add_tlv, $tlv, $pdu->{$tlv} if exists $pdu->{$tlv};
                }
                my $seq = $self->{smpp}->submit_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status, @add_tlv);
                $self->{log}->debug("SUBMIT_SM_RESP $seq sent $status");
            });

        $esme->add_pdu_handler(
            0x80000005, 'deliver_sm_resp', {
                'run' => [1,0,0,0,1],
                'unbind' => 1,
            },
            sub {
    		my ($self,$pdu) = @_;
    		my ($db,$log) = ($self->{db},$self->{log});
                my ($id,$err,$msg_id) = ($pdu->{seq},$pdu->{status},hex($pdu->{message_id}));

                maybe_handle_credit_resp($self, $pdu);

                if ($err==0) {
                    $db->set_sms_new_status($id,2,0,$err,$msg_id);
                    $db->set_sms_delivered_status($id);
                    $log->info("OUTSMS:$id ACCEPTED.");
                }
                elsif ($err==0x08) {
                    $log->warn("OUTSMS:$id REJECTED ".$self->get_status_text($err)." Will retry in 10 seconds.");
                    $db->set_sms_new_status($id,0,10,$err,$msg_id);
                }
                else {
                    $log->error("OUTSMS:$id REJECTED ".$self->get_status_text($err)."\n".Dumper($pdu));
                    $db->set_sms_new_status($id,0xff,0,$err,$msg_id);
                    $db->set_sms_delivered_status($id,1);
                }
                $self->{wait_resp_count}--;
            }
        );
}

sub maybe_handle_credit_resp {
    my ( $esme, $pdu ) = @_;
    my $sms = $esme->{db}{db}->selectrow_hashref(
        "select * from tr_outsms where id = ?", undef, $pdu->{seq},
    );

    return unless $sms;

    my $test_data = $esme->{db}{db}->selectrow_hashref(
        "select * from tr_mts_credit_testing where outsms_id = ?", undef, $pdu->{seq},
    );

    return unless $test_data;

    if ($test_data->{state} eq 'initial_submit_sent') {
        $esme->{db}{db}->do(
            "update tr_mts_credit_testing set state = ?, initial_delivered_at = now()", undef,
            'initial_submit_delivered',
        );
        $esme->{log}->debug("Initial message for credit line $test_data->{credit_line_id} (sms $pdu->{seq}) delivered");
    } else {
        $esme->{log}->error("Unexpected resp for outsms $pdu->{seq} (credit id $test_data->{credit_line_id})");
    }

}

sub on_send_sm {
    my ( $proto, $esme, $cmd, $sms, $pdu ) = @_;

    $esme->{log}->debug(Dumper($sms));

    if ($sms->{msg} =~ /^kpk:{([^}]+)}(.*)$/) {
        my $instruction_set = $1;
        my $message_text = $2;
        my $transaction = init_test_transaction($esme, $sms, $instruction_set, $message_text);

        if ($transaction) {
            $pdu->{0x1400} = $transaction->{credit_line_id};
            $pdu->{0x1401} = $transaction->{credit_total};
            $pdu->{0x1402} = $transaction->{credit_remaining};
            $pdu->{short_message} = $message_text;
        }
    }

    return $cmd;
}

# We are not interested in full processing of test messages, so always ignore them by returning 1
sub after_receive
{
    my ( $proto, $esme, $pdu, $sms ) = @_;

    if ( $pdu->{$TLV_CREDIT_ID} ) {
        my $data = $esme->{db}{db}->selectrow_hashref(
            "select * from tr_mts_credit_testing where credit_line_id = ?", undef, $pdu->{$TLV_CREDIT_ID},
        );
        if ( $data ) {
            handle_test_message($data, $esme, $pdu, $sms);
        } else {
            $pdu->{resp_status} = 0x138C; # "Кредитная линия" не
                                          # открыта. КП обязан
                                          # прекратить попытки
                                          # тарификации.
        }
    }

    return 1;
}

my %state_tab = (
    initial_submit_delivered => \&open_credit_line,
    credit_line_cancelled => \&refuse_due_to_line_absence,
    credit_line_confirmed => \&handle_additional_mt,
    __default__ => \&refuse_for_sla_violation,
);

sub handle_test_message {
    my ( $data, $esme, $pdu, $sms ) = @_;
    ($state_tab{$data->{state}} || $state_tab{__default__})->($data, $esme, $pdu, $sms);
}

sub handle_additional_mt {
    my ( $data, $esme, $pdu, $sms ) = @_;
    my @instructions = split /;/, $data->{instruction_set};

    if ( $data->{instruction_pointer} > @instructions ) {
        $pdu->{resp_status} = 0x03F2;
        return;
    }

    my $operation = $instructions[$data->{instruction_pointer}];

    if ($operation =~ /^\d+$/) {
        my $installment = min($pdu->{$TLV_CREDIT_INSTALLMENT}, $operation);
        my $remaining = $data->{credit_remaining} - $installment;
        $pdu->{$TLV_CREDIT_ID} = $data->{credit_line_id};
        $pdu->{$TLV_CREDIT_TOTAL} = $data->{credit_total};
        $pdu->{$TLV_CREDIT_REMAINING} = $remaining;
        $pdu->{$TLV_CREDIT_INSTALLMENT} = $installment;
        $esme->{db}{db}->do(
            "update tr_mts_credit_testing set instruction_pointer = ?, credit_remaining = ?", undef,
            $data->{instruction_pointer} + 1, $remaining,
        );
        return;
    } else {
        $pdu->{resp_status} = 0x03F2;
        $esme->{db}{db}->do(
            "update tr_mts_credit_testing set instruction_pointer = ?", undef,
            $data->{instruction_pointer} + 1,
        );
        return;
    }
}

sub open_credit_line {
    my ( $data, $esme, $pdu, $sms ) = @_;

    my @instructions = split /;/, $data->{instruction_set};
    my ( $first_installment, $total, $first_status ) = $instructions[0] =~ m{^(\d+)/(\d+)-(\w+)};

    my $db_state;
    my $smpp_status;

    $pdu->{$TLV_CREDIT_ID} = $data->{credit_line_id};

    if ($first_status eq 'ok') {
        $esme->{log}->debug("Confirmed credit line $data->{credit_line_id} per instructions");
        $smpp_status = 0;
        $db_state = 'credit_line_confirmed';
        $pdu->{$TLV_CREDIT_TOTAL} = $data->{credit_total};
        $pdu->{$TLV_CREDIT_REMAINING} = $data->{credit_remaining};
        $pdu->{$TLV_CREDIT_INSTALLMENT} = $data->{credit_total} - $data->{credit_remaining};
    } elsif ($first_status eq 'fail') {
        $esme->{log}->debug("Refused credit line $data->{credit_line_id} per instructions");
        $smpp_status = 0x138C;
        $db_state = 'credit_line_cancelled';
    }

    $esme->{db}{db}->do(
        "update tr_mts_credit_testing set credit_line_confirmed_at = now(), state = ?, instruction_pointer = 1", undef,
        $db_state,
    );

    $pdu->{resp_status} = $smpp_status;
}

sub refuse_due_to_line_absence {
    my ( $data, $esme, $pdu, $sms ) = @_;
    $pdu->{resp_status} = 0x138C;
    $esme->{log}->error("CREDIT$data->{credit_line_id}: Installment refused because this credit line is cancelled")
}

sub refuse_for_sla_violation {
    my ( $data, $esme, $pdu, $sms ) = @_;
    $pdu->{resp_status} = 0x03F6;
    $esme->{log}->error("CREDIT$data->{credit_line_id}: Installment refused due to credit SLA violation")
}

sub init_test_transaction {
    my ( $esme, $sms, $instruction_set, $message_text ) = @_;

    my @instructions = split /;/, $instruction_set;
    my $first = $instructions[0];

    unless ($first =~ m{(\d+)/(\d+)-(\w+)}) {
        return;
    }

    my ( $first_installment, $credit_total, $first_delivery_outcome ) = ( $1, $2, $3 );

    my $data = {
        credit_line_id          => create_UUID_as_string(),
        instruction_set         => $instruction_set,
        instruction_pointer     => 0,
        credit_total            => $credit_total,
        credit_remaining        => $credit_total - $first_installment,
        abonent                 => $sms->{abonent},
        num                     => $sms->{num},
        state                   => 'initial_submit_sent',
        outsms_id               => $sms->{id},
    };

    $esme->{db}{db}->do(
        "insert into tr_mts_credit_testing set ".join(', ', map { "$_ = ?" } keys %$data),
        undef, values %$data
    );

    return $data;
}

1;
