#!/usr/bin/perl -w
package Channel::SMPP::Func_MTS_Ural_USSD;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;


sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{ussd_service_op} = unpack('C',$pdu->{ussd_service_op} || '');
	$sms->{transaction_id} = $sms->{user_message_reference} || undef;
	return 0;
}

sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	$pdu->{user_message_reference} = pack('S',$sms->{transaction_id});
	if ($sms->{ussd_service_op}) {
		# USSD ������
		$pdu->{service_type} = 'USSD';
		$pdu->{ussd_service_op} = pack('C',$sms->{ussd_service_op});
	}
	else {
		if ($sms->{tariff}) {
			# ��������������� ������ ���
#			$pdu->{18689} = "\x01";
#			$pdu->{18691} = uc($sms->{tariff})."\x00";
#			$pdu->{short_message} = '';
#			$pdu->{message_payload} = ($sms->{msg} =~ /^(cat\d\d?\_\w*):(.*)$/is) ? $2 : $sms->{msg};
		}
		else {
			# ���������� ���
#			$pdu->{18689} = "\x00";
		}
#		$cmd = Net::SMPP::CMD_data_sm;
	}	
	return $cmd;
}

1;
