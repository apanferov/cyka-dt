#!/usr/bin/perl -w
package Channel::SMPP::Func_A1Systems_push_esme;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Data::Dumper;

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	delete $pdu->{5122};
	$pdu->{5121} = "$sms->{operator_id}\x00");
	return $cmd;
}


1;
