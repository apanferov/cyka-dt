#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Omnitel;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	($sms->{num}) = $pdu->{destination_addr} =~ /^(\d{4})/;
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	if ($sms->{tariff} eq 'free') {
		$esme->{db}->set_sms_new_status($sms->{id},0xff,undef,10010);
		$sms->{outbox_status} = $esme->{db}->set_sms_delivered_status($sms->{id},1);
		$esme->outbox_status_changed($sms);
		$esme->{log}->info("OUTSMS:$sms->{id} BLOCKED");
		return 0;
	}
	return $cmd;
}


1;
