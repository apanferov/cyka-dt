#!/usr/bin/perl -w
# -*- coding: cp1251 -*-
package Channel::SMPP::Func_Svyaznoy;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils qw(yell);
use Data::Dumper;

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{link_id} = $pdu->{5122}; # ��� ���������
	my $tr_id = $pdu->{5152}; # ������������� ���
	my $service_id = $pdu->{5153}; # ������������� �������
	($sms->{num},my $tr_id1) = split(/#/,$pdu->{destination_addr});
	$sms->{transaction_id} = "$sms->{link_id}-$tr_id-$service_id-$tr_id1";
	return 0;
}

sub on_send_sm
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	my ($link_id,$tr_id,$service_id,$tr_id1) = $sms->{transaction_id} =~ /^(\d*)-(\d*)-(\d*)-(\d*)$/;
	$pdu->{5122} = $link_id; # ��� ���������
	$pdu->{5152} = $tr_id; # ������������� ���
	$pdu->{5153} = $service_id; # ������������� �������
	$pdu->{source_addr} = $tr_id1 ? "$sms->{num}#$tr_id1" : $sms->{num};
#	$esme->warning('Wrong encoding is used.',Dumper($pdu))
#	  if ($pdu->{data_coding} != 8);
	return $cmd;
}


1;
