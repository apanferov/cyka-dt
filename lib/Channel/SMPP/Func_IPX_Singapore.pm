#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Singapore;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX qw(pdu_to_sms sms_to_pdu);

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	return 0;
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	sms_to_pdu($sms,$pdu,"SGD0\x00");
	return $cmd;
}


1;
