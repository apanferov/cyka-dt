#!/usr/bin/perl -w
package Channel::SMPP::Func_Lmt;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX;
use Channel::Utils;

my $status_tab = {
		  0 => 12,
		  1 => 13,
		  2 => 13,
		  3 => 13,
		  4 => 13,
		  5 => 14,
		  6 => 13,
		  10 => 13,
		  11 => 13,
		  12 => 13,
		  13 => 13
		 };


sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00010000, 'lmt_report', {run => [1,1,1,1,0]},
	   sub {
		   my ($self, $pdu) = @_;
		   Net::SMPP::decode_optional_params($pdu, 0);
		   $self->debug_pdu($pdu);
		   my $lmt_report_type = unpack("c",$pdu->{5384});
		   $esme->{log}->debug("Report status $lmt_report_type.");
		   if ($lmt_report_type == 0) {
			   my $msg_id = $pdu->{5382}; # lmt_message_id
			   my $tr_id = $self->{db}->get_value('select transaction_id from tr_outsmsa where smsc_id=? and msg_id=? and tariff is not null',
												  undef, $self->{smsc_id}, $msg_id);
			   pass_sms_by_tr_id($self, $tr_id) if ($tr_id);
		   }
		   $self->{smpp}->req_backend(0x80010000,undef,undef,seq => $pdu->{seq});
	   });

}

my $tariff_tab = {'1871' => "\x01\x37", #311
				  '1872' => "\x01\x3D", #317
				  '1873' => "\x01\xF9", #505
				  '1874' => "\x02\x3E"  #574
				 };

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	if ($pdu->{source_addr_ton} == 3) {
		my ($abonent) = split(/\//,$pdu->{5377});
		$esme->{lmt_abonent_tab}{$abonent}=$pdu->{source_addr};
		$sms->{abonent} = $abonent;
	}
	$sms->{transaction_id} = $pdu->{5382}; # lmt_message_id
	my $operator_id = $esme->{db}->get_value('select determine_operator(?,?,null)',
											 undef,$sms->{abonent},$esme->{smsc_id});
	my $text = simple_smpp_decode($sms);

	my $msg = { abonent => $sms->{abonent},
				smsc_id => $esme->{smsc_id},
				operator_id => $operator_id,
				num => $sms->{num},
				data_coding => 0,
				esm_class => 0,
				test_flag => 20,
				transaction_id => $sms->{transaction_id},
				parts => ["Atvainojiet! Jusu pierasijums nebija veiksmigs. Ludzu parbaudiet to un sutiet velreiz! Info www.a1help.ru"]};

	unless (check_service_patterns($esme,$text,942,1522,1634)) {
		$esme->{db}->move_outbox_to_outsms($msg);
		$esme->{log}->info("Wrong query. Information SMS was created.");
		return 1; # Ignore SMS
	}

	$msg->{tariff} = $tariff_tab->{$sms->{num}};
	$msg->{parts} = ["Spasibo. Zakaz vo 2-om SMS."];
	$esme->{db}->move_outbox_to_outsms($msg);
	$esme->{log}->info("Testing SMS was created.");
	return 1; # Ignore SMS
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	$pdu->{5379} = $sms->{tariff} || "\x00\x97"; # lmt_tariff_class
	$pdu->{5380} = ($sms->{tariff}) ? "\x01" : "\x00"; # lmt_more_messages_to_send
	$pdu->{5381} = "\x00"; # lmt_service_desc
	if (my $lmt_addr=$esme->{lmt_abonent_tab}{$sms->{abonent}}) {
		$pdu->{destination_addr} = $lmt_addr;
		$pdu->{dest_addr_ton} = 3;
	}
	return $cmd; # Send SMS
}

sub on_submit_resp {
        my ($proto,$esme,$pdu) = @_;
	if ($pdu->{0x1510}) {
		my $abonent = $esme->{db}->get_value("select abonent from tr_outsms where id=?",undef,$pdu->{seq});
		$esme->{lmt_abonent_tab}{$abonent}=$pdu->{0x1510} if ($abonent);
	}
	if ($pdu->{status}>=0x402) {
		my $rule = {retry => 5, first => 1, other => 60};
		return $rule;
	}
	return undef;
}

1;
