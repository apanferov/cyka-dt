#!/usr/bin/perl -w
# -*- coding: utf-8 -*-
package Channel::SMPP::Func_Velcom;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Disp::Utils;
use Disp::Config;
use Data::Dumper;
use MIME::Lite;
use Net::SMTP_auth;

my $limit_tbl = {};
foreach (qw(2425 4141 4345 2420 1121 9009)) {
	$limit_tbl->{$_}{sms_cnt} = 60;
	$limit_tbl->{$_}{block_sms_cnt} = 60;
}
foreach (qw(2423 1131 4488 3338 7799 4446 2320 1141)) {
	$limit_tbl->{$_}{sms_cnt} = 40;
	$limit_tbl->{$_}{block_sms_cnt} = 60;
}
foreach (qw(1320 2424 3355 3337 3388 1151 4448 2050 3150 7375)) {
	$limit_tbl->{$_}{sms_cnt} = 30;
	$limit_tbl->{$_}{block_sms_cnt} = 50;
};
foreach (qw(3283 4042 3322 3366 2012 3151 2325 2442 3336 3399 4477 6365 5013)) {
	$limit_tbl->{$_}{sms_cnt} = 20;
	$limit_tbl->{$_}{block_sms_cnt} = 30;
};
foreach (qw(3311 1899 4449 2011 3152 2090 1331 2006 3332 3398 5014 4161 8385 1332 9964 5555 3282 4047 3339 7781 4162 1310 3153 1315 9395 3284 4043 3310 9994 4163 8404)) {
	$limit_tbl->{$_}{sms_cnt} = 10;
	$limit_tbl->{$_}{block_sms_cnt} = 20;
};

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
  my($db, $log) = ($esme->{db}, $esme->{log});
	unless (check_limit_for_abonent($db, $log, $sms->{abonent}, $sms->{num})) {
		$sms->{transaction_id} = '#fraud#';
	}
	return 0;
}

sub on_send_sm {
	my ($proto, $esme, $cmd, $sms, $pdu) = @_;
	$pdu->{source_addr} = $sms->{num};
	return $cmd;
}

sub check_limit_for_abonent {
	my ($db, $log, $abonent, $num) = @_;
	my $prefix = 'Velcom';
	my $time_30 = 30*60; # 30 min
	my $time_60 = 60*60; # 60 min
	my $sms_cnt_limit = $limit_tbl->{$num}{sms_cnt} || 10; # 10 sms
	my $block_sms_cnt_limit = $limit_tbl->{$num}{block_sms_cnt} || 60; # 60 sms
	my $sms_30_cnt = 0;
	my $sms_60_cnt = 0;

	my $key = $prefix.'A'.$abonent.'N'.$num;

	$db->redis_connect unless ($db->{redis});

	# Добавляем новый элемент в конец
	my $sms_date = time();
	eval {
		$db->{redis}->rpush($key, $sms_date);
	};
	if ($@) {
		$db->redis_connect;
		$db->{redis}->rpush($key, $sms_date);
	}

	# Удаляем по данному абоненту старые sms
	# Считаем оставшиеся
	my $sms_cnt = $db->{redis}->llen($key);
	my @sms_list = $db->{redis}->lrange($key,0,$sms_cnt);
	foreach my $sms_date (@sms_list) {
		if (time() - $sms_date > $time_60) {
			$db->{redis}->ltrim($key,1,-1);
		} else {
			$sms_60_cnt++;
			unless (time() - $sms_date > $time_30) {
				$sms_30_cnt++;
			}
		}
	}

	# Проверяем кол-во, если > $sms_30_cnt/$sms_60_cnt и последнее отправление было > $time_30/$time_60 минут назад, отправляем allert
	if (($sms_30_cnt >= $sms_cnt_limit) || ($sms_60_cnt >= $sms_cnt_limit)) {
		$key = $prefix.'LastSend'.'A'.$abonent.'N'.$num;
		my $last_send = $db->{redis}->get($key) || 0;
		if (($sms_30_cnt >= $sms_cnt_limit) && (time() - $last_send > $time_30)) {
			send_mail($log, "На к.н. '$num' - $abonent отправил $sms_cnt_limit сообщений менее, чем за 30 минут.");
			$db->{redis}->set($key,time());
		} elsif (($sms_60_cnt >= $sms_cnt_limit) && (time() - $last_send > $time_60)) {
			send_mail($log, "На к.н. '$num' - $abonent отправил $sms_cnt_limit сообщений менее, чем за 60 минут.");
			$db->{redis}->set($key,time());
		}
	}

	# Проверяем на блокировку
	$key = $prefix.'BlockedTime'.'A'.$abonent.'N'.$num;
	my $blocked_time = $db->{redis}->get($key) || 0;
	unless (time() - $blocked_time > 60*60*24) {
		return 0;
	}
	if ($sms_60_cnt >= $block_sms_cnt_limit) {
#		send_mail($log, "'$num' - $abonent: Блокировка на 24 часа. (\$sms_60_cnt = $sms_60_cnt, \$block_sms_cnt_limit = $block_sms_cnt_limit)");
		$db->{redis}->set($key,time());
		return 0;
	}

  return 1;
}

sub send_mail {
	my ($log, $msg_txt) = @_;

	# email
	my $smtp = Net::SMTP_auth->new('mail.alt1.ru');
	if (!$smtp->auth('LOGIN', 'support', '4r5t6y7U')) {
		$log->warn("SMTP authentication failed.");
		$smtp->quit;
	} else {
		$smtp->mail('support@alt1.ru');
		$smtp->to('CreditControl@velcom.by', 'a.panferov@alt1.ru', 'v.islamov@alt1.ru');
#		$smtp->to('a.panferov@alt1.ru', 'v.islamov@alt1.ru');
		$smtp->data();
		$smtp->datasend("From: support\@alt1.ru\n");
		$smtp->datasend("To: CreditControl\@velcom.by\n");
		$smtp->datasend("Subject: Превышение лимита запросов абонента\n");
		$smtp->datasend("\n");
		$smtp->datasend("$msg_txt\n");
		$smtp->dataend();
		$smtp->quit;
	}
}

1;

__END__
