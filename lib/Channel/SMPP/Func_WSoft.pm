#!/usr/bin/perl -w
package Channel::SMPP::Func_WSoft;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;
	$sms->{transaction_id} = $pdu->{5120};
	($sms->{link_id}) = split(/[\/#]/,$pdu->{5120});
	return 0;
}

sub before_send
{
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	delete $pdu->{5122};
	$pdu->{source_addr} = $sms->{num};
	$pdu->{5120} = $sms->{transaction_id};
	return $cmd;
}

1;
