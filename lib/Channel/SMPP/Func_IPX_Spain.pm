#!/usr/bin/perl -w
package Channel::SMPP::Func_IPX_Spain;
use strict;
use warnings;
use base 'Exporter';
use Net::SMPP;
use Channel::SMPP::Func_IPX qw(pdu_to_sms sms_to_pdu create_test_message process_submit_resp);

sub on_receive_sm {
	my ($proto,$esme,$pdu,$sms) = @_;
	pdu_to_sms($pdu,$sms);
	if (($sms->{link_id} eq 'MOVISTAR') or
		($sms->{link_id} eq 'Euskaltel') or
		($sms->{link_id} eq 'R')) {
		return 0; # MO-billed SMS
	}
	create_test_message($esme,$sms,
						"Thank you for your payment of 1.20 EUR. The order is in the second SMS.",
						"EUR1392\x00");
	return 1; # Ignore SMS
}

sub on_receive_dr{
	my ($proto,$esme,$pdu,$outsms) = @_;
	if (($pdu->{5889} eq "Yoigo\x00")) {
		if ($pdu->{short_message} =~ m/stat:DELIVRD/) {
			Channel::SMPP::Func_IPX::pass_sms_by_tr_id($esme, $outsms->{transaction_id});
		}
	}
}

sub on_send_sm {
	my ($proto,$esme,$cmd,$sms,$pdu) = @_;
	my $link_id = $esme->{db}->get_link_id($sms->{abonent},$sms->{smsc_id});
	if (($link_id eq 'MOVISTAR') or
		($link_id eq 'Euskaltel') or
		($link_id eq 'R') or
		$sms->{tariff}) {
		sms_to_pdu($sms,$pdu,$sms->{tariff} || "EUR0\x00");
		$pdu->{registered_delivery} = 1 if ($link_id eq 'Yoigo');
		return $cmd; # Send SMS
	}
	else {
		$esme->{db}->do("update tr_outsms set smsc_id=124, msg=concat(?,msg) where id=?",
						undef,"\x{1b}\x{3c}FreeMsg\x{1b}\x{3e} ",$sms->{id});
		return 0; # Don't send SMS
	}
}

sub on_submit_resp {
	my ($proto,$esme,$pdu) = @_;
	return process_submit_resp($esme,$pdu);
}

1;
