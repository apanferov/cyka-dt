#!/usr/bin/perl -w
package Channel::SMPP::Func_Megafon;
use strict;
use warnings;
use Encode qw(encode decode);
use base 'Exporter';
use Net::SMPP;
use Data::Dumper;
use Translit qw/translit_text/;
use Channel::Utils qw(check_limit_pattern);

my %blocked_nums = (194 => {'aaa' => 1}, #Moscow
		    196 => {'aaa' => 1}, #North-West
		    9   => {'aaa' => 1}, #North Kavkaz
		    192 => {'aaa' => 1}, # Far East
		    1131 => {'aaa' => 1},
		    3121 => {'aaa' => 1},
		    4012 => {'aaa' => 1},
		    5013 => {'aaa' => 1},
		    5537 => {'aaa' => 1},
		    6681 => {'aaa' => 1},
		    7781 => {'aaa' => 1},
	    );

sub on_init {
	my ($proto,$esme) = @_;
	$esme->add_pdu_handler(0x00000005, 'deliver_sm', {'run'=>[1,0,1,1,0], 'unbind'=>[1,0,1,1,0],},
						   sub {
							   my ($self,$pdu) = @_;
							   $self->{last_received_sms_time} = time();
							   $self->process_data_pdu($pdu);
							   die("CONNECT_ERROR_:Can't write DELIVER_SM_RESP to socket.") if (!$self->{select}->can_write(0));
							   my $status = $pdu->{ignore} || 0;
							   my $seq = $self->{smpp}->deliver_sm_resp(message_id => '',seq => $pdu->{seq}, status => $status);
							   $self->{log}->debug("DELIVER_SM_RESP $seq sent $status");
						   });
}

sub after_receive
{
	my ($proto,$esme,$pdu,$sms) = @_;

	return 0 if ($sms->{num} =~ m/^(2316|4974|5283)$/);

	my $msg = (($sms->{data_coding} & 12) == 8) ? encode('cp1251',decode('UCS-2', $sms->{msg})) : $sms->{msg};
	$msg = lc(translit_text($msg));
	$sms->{operator_id} = $esme->{db}->determine_operator($sms->{abonent}, $sms->{smsc_id}, $sms->{link_id});
	if ($esme->{db}->is_blocked_by_pref($sms->{operator_id}, $msg)
			or ($esme->{db}->is_blocked($sms->{abonent},$sms->{num},'mo'))
			or ($blocked_nums{$esme->{smsc_id}}{$sms->{num}})) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'ForbiddenKeyword';
		return 1;
	}
	if (
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24*7, '^\s*(7950118)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*24, '^\s*(x1c|x2c|x3c|15351|157|159|165|167|253330|692|111 33|12|315|333|37|51(?!818|98[01])|xyz|yzx|zxy|nac|100176428|10017112|794784|791265|22697009|2177822|7986769|1681225|110079255|100154451|10017488|217349|10016359|79663|7982135)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*20, '^\s*(gg|y1|a5|j3|nab|exb|yxy|yy1|yya)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*20, '^\s*(zz1)') ||
		check_limit_pattern($esme, $msg, $sms->{abonent}, 1, 60*60*3, '^\s*(m1|m2)')
	) {
		$pdu->{ignore} = 0x557;
		$sms->{ignore_reason} = 'CheckLimitPattern';
		return 1;
	}
	return 0;
}

1;
