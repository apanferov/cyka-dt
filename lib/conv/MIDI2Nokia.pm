package IMMO::Converter::MIDI2Nokia ;

use lib '/home/projects/lib';
use Data::Dumper;
use IMMO::Converter::RTConverter;

use base 'IMMO::Converter';
use constant MAX_LENGTH =>512 ; 
use constant MAX_PART_LENGTH =>128 ; 

sub new {

		my $proto = shift;
		my $class = ref($proto) || $proto;
		my $self  = $class->SUPER::new();

		$self; 
  
}

sub doTranslate {

my $self = shift;
my $ret = 1 ; 
my $data=shift;
	 $data = pack("H*",$data ) if $data=~/^4D54/ ;  

my $RTConverter = new IMMO::Converter::RTConverter;


$RTConverter->clear();
$RTConverter->set_max_filesize(&MAX_LENGTH);
$RTConverter->set_compact();  
$RTConverter->set_inputformat("MIDI");
$RTConverter->set_outformat("NOKIA");
 

 $self->{result} = [2, uc(unpack("H*", $RTConverter->convert($data))) ] ;   

 $ret;

} 


1; 