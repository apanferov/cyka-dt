package IMMO::Converter::RTDebug;
use Time::HiRes qw (gettimeofday);

	sub new {
		my $this = { };
		bless($this,'IMMO::Converter::RTDebug');

		$this->{loaded_time_start}=0;
		$this->{debug}=1;
		#$this->{debug_log}='/var/log/sender/rtconverter.debug.log';
		
		$this;
	}


  sub _loaded_time_start {

   my $this = shift;  
		  $this->{loaded_time_start}=time();
	
	} 	
	sub _error($str)
	{
		my ($this,$str)=@_;
		$this->_debug($str);
		print "\n---------------------------\n  Error: ".$str."\n---------------------------\n";
		return 0;
	}

	# ������ � ���/stdout �������� �������
	sub _debug($str)
	{
		my ($this,$str)=@_;
		if( $this->{debug_log} || $this->{debug}) {
			if(! $str) {
				$debug_str = "\n";
			}else {
				$time_end = $this->getmicrotime();
				$time = int(($time_end - $this->_loaded_time_start)*1000)/1000;
				$time=~/'([0-9]+)\.([0-9]+)'/;
				$str_time = sprintf("%04d", $1).".".sprintf("%04d", $2);
				($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime();
				$debug_str = "$year-$mon-$mday $hour:$min:$sec"."|".$str_time."|".$str."\n";
			}
		}

		# ������ � ���
		open F, ">>".$this->{debug_log} || die "Open log file ".$this->{debug_log}." failed : $!";
		print F $debug_str;
		close F ;

		if($this->{debug} ) {
			print $debug_str;
		}
	}

	sub getmicrotime()
	{
		my ($this)=@_;
#		list($usec, $sec) = explode(" ", microtime());
#		return ((float)$usec + (float)$sec);
		$t= gettimeofday;
		return $t;
	}

return 1;