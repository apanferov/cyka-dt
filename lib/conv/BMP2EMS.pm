package IMMO::Converter::BMP2EMS ;

use lib '/home/projects/lib';
use Data::Dumper;
use File::Temp qw/ :mktemp /;
use IMMO::SMSTools qw(wrap_text); 
use IMMO::XML::Writer::FakeIO;
use Image::Magick; 

use base 'IMMO::Converter';
use constant CMD=> 'ringtonetools -intype bmp -outtype ems ';
use constant TEMPFILE=> '/tmp/ringtonetools' ;

use constant PACKETLEN=> 140    ;
use constant HEADERLEN=> 6  ;  # ����� UDH, ��� ��������, �����, ������ = ( ����� ������, ������, ������ ) ; 
use constant CONCATLEN => 5  ; # 00, �����, ref number, ����� , ����� ���� 
use constant WRAPPING => 224;

use constant IEL => 
						 { 
						 
						  'variable_picture' => '12',
							'concat_short_message_8bit' =>'00',  
							
						 
						 }  ; 


sub new {

		my $proto = shift;
		my $class = ref($proto) || $proto;
		my $self  = $class->SUPER::new();


		$self; 
  
}



sub doTranslate {

my $self = shift;
my $ret = 1 ; 
my $data = shift; 
my $result; 
my $name; 
my 	$stream	; 

 ($name, $data)  = ($1, $2) if  $data =~/(.*?)::(.*?)/;  

 my $image=Image::Magick->new(magick=>'bmp');

		return -1 unless $image;
		
		$image->BlobToImage($data =~/^BM/ ? $data  : pack("H*",$data ) ); 
		$image->Negate;
    $image->Set("magick" => "mono");
		
my $width = $image->Get('columns'); 
my $height = $image->Get('height'); 

my ($fh, $filetmp) = mkstemp(TEMPFILE . ".XXXXXX");
    $image->Write("wbmp:$filetmp");
   	$fh->close();
		
		open ($fh ,"<", $filetmp);

		$stream.=$_ while (<$fh>) ; 	

   	$fh->close();
		unlink($filetmp);

    $stream = substr(  uc(unpack("H*", $stream))  ,8 );
		
		
	 				 	 
my  $stream_length = length ($stream) / 2  ; 


   if ( $stream_length < PACKETLEN - HEADERLEN ) { 
	 
	   $result  = ev ($stream_length + HEADERLEN -1 ). &IEL->{variable_picture}. ev ($stream_length + HEADERLEN - 2 ).'00'.ev( $width / 8 ) .ev($length) . $stream  ; 
	 } 
	 else 
	 {
			
		 my $ref= ( time() % 255 ) + 1 ;
		 my @parts =  split(/\n/, wrap_text( $stream, WRAPPING + 1 )) ;
		 my $num_of_parts = @parts ;  

		 
		 my $substr = $width *  WRAPPING /   (length ($stream) * 4 ) ; 
		 my @ret; 


		 
#		 print Dumper(split(/\n/, wrap_text( $stream, length ($stream) /$height  + 1 )));
		 
		 foreach (split(/\n/, wrap_text( $stream, length ($stream) /$height  + 1 )) ) 
		 {
		 		for ( my $i=0; $i <= $num_of_parts - 2 ; $i++)
				{
				    $ret[$i].=substr($_, $i * $substr , $substr );  
				}
				    $ret[$num_of_parts - 1].=substr($_, ($num_of_parts - 1 ) * $substr );  
		 }  
		 
#     print Dumper(@ret);
		 
		 

#		  map { print length($_ ),"=\n" } @ret;
			
		 
		 
		 for ( my $i=1 ; $i <= scalar(@ret) ; $i++ )   {
		 
			  
	
	   my $tmp =  '00'.'03'.ev($ref).ev( scalar(@ret) ).ev($i);
			  $tmp .=  '1301'.ev(scalar(@ret)) if $i==1; # UPI
			  $tmp .= &IEL->{'variable_picture'}.ev( 3 + length($ret[$i-1])/2  ).'00'. ev( length($ret[$i-1]) /  56 ) .ev($height) .$ret[$i-1] ;
				$tmp =  ev(length ($tmp) / 2  ) . $tmp;
				
				$tmp.='::' unless $i == @ret ; 				  
				
				$result  .=$tmp;  
		 
		 }
	 
	 } 

 defined $result ? $self->setResult( [7, $result] ) : $ret=-1;


$ret; 
}



sub ev {
  sprintf("%02X",shift);
}

 
1; 