package IMMO::Converter::Nokia ;

use lib '/home/projects/lib';
use IMMO::SMS::OTA::Bitmap;
use GSM::SMS::OTA::RTTTL;
use Data::Dumper;

use base 'IMMO::Converter';

my $handlers =  { 

   'image/bmp' => 
	 
	 			   { 'image/vnd.nok-oplogo' =>
				   
				   	   sub {
					     
						 return [1, OTABitmap_makestream(OTABitmap_fromblob(shift)) ] ; 
						 	
					   },						
				   
				   	  'image/vnd.nokia.picture' =>						  
				   	   sub {
					     
						 return [3, OTABitmap_makestream(OTABitmap_fromblob(shift)) ]  ; 
						 	
					   }						
					  
				    },
					
		'audio/x-rtttl'=>
					
					{
					  'application/vnd.nokia.ringing-tone' => 

					  sub {
					  
					    [2,OTARTTTL_makestream(shift) ] ; 
					  } 
		
		
					}			

   };


sub new {

		my $proto = shift;
		my $class = ref($proto) || $proto;
		my $self  = $class->SUPER::new();

    
		$self->{handlers} = $handlers ; 

		$self; 
  
}

sub doTranslate {

my $self = shift;
my $ret = 1 ; 

 $ret =  & { $self->{handlers}->{ $self->{supplier}->{source} }->{ $self->{supplier}->{target} } } (shift);

 $ret==-1 ? $self->{supplier}->error("This is an error") : $self->{result} = $ret;   

 $ret;
} 

 
1;