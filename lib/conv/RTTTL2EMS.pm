package IMMO::Converter::RTTTL2EMS ;

use lib '/home/projects/lib';
use Data::Dumper;
use IMMO::Converter::RTConverter;

use base 'IMMO::Converter';
use constant MAX_LENGTH =>128 ; 
use constant MAX_PART_LENGTH =>128 ; 

sub new {

		my $proto = shift;
		my $class = ref($proto) || $proto;
		my $self  = $class->SUPER::new();

		$self; 
  
}

sub doTranslate {

my $self = shift;
my $ret = 1 ; 

my $RTConverter = new IMMO::Converter::RTConverter;


$RTConverter->clear();
$RTConverter->set_max_filesize(&MAX_LENGTH);
$RTConverter->set_compact();  
$RTConverter->set_inputformat("RTTTL");
$RTConverter->set_outformat("IMELODY");
 

 $self->{result} = [7, $self->_imy_pieces2ems($self->_imelody2pieces($RTConverter->convert(shift))) ] ;   

 $ret;

} 

=item
	function _midimono2ems($input,
	$name = 'tone'#�������� �������
	)
	{
		if (!$name) $name='tone';
		$iMelody = $this->_midimono2imelody($input, $name);
		return $this->_imy_pieces2ems($this->_imelody2pieces($iMelody));
	}
=cut


sub  _imy_pieces2ems
#������ ������ iMelody ��� �������� ����� EMS (128 ����)
{

my $self= shift; 
my $arr_ref_imelody = shift; 
my $melody_len = length(join('',@$arr_ref_imelody));

my $upi_len = 3;
my $cat16_len = 6;
my $add_len = 4;
my $max_len = &MAX_PART_LENGTH;

my $EMS_data;

 if ( @$arr_ref_imelody > 1 )
 {
			my $catRef  = (time() % 65535 ) + 1 ;
			my $UPI = '1301'.ev( @$arr_ref_imelody );
			my @aTPUD; 
			for (my $i=0; $i < @$arr_ref_imelody; $i++)
			{
				my $catEI = '0804' . ev($catRef) . ev(scalar(@$arr_ref_imelody)) . ev($i+1);
				my $mLen = length($arr_ref_imelody->[$i]);
				my $mEI = '0C'.ev($mLen+1).'00'.uc(unpack("H*",$arr_ref_imelody->[$i]));
				
				my $data = $i ? $catEI . $mEI : $catEI . $UPI . $mEI; 

				push @aTPUD, ev( length($data)/2 ).$data;
			}
			$EMS_data = join ('::',@aTPUD);
 }
 else
 {
		 $EMS_data = ev($melody_len+3).'0C'.ev($melody_len+1).'00'.uc($arr_ref_imelody->[0]);
 }

 return $EMS_data;

}

	
sub  _imelody2pieces
{
# ����������� ���������� ���������� �� ������ ���������� RTConverter

my $self  = shift; 
my $iMelody  = shift;

my $max_len = &MAX_PART_LENGTH;
my $upi_len = 3;
my $cat16_len = 6;
my  $add_len = 4; 


		#preg_match("'BEGIN.*?MELODY:'si", $iMelody, $r_Head);
		my @r_Head = $iMelody=~/(BEGIN.*?MELODY:)/si;

		#$iMelodyHead = trim($r_Head[0]);
		my $iMelodyHead = trim($r_Head[0]);

		#preg_match("'MELODY:(.*?)END'si", $iMelody, $r_Notes);
		my @r_Notes = $iMelody=~/MELODY:(.*?)END/si; 

		
		#$strNotes = preg_replace("'[\s]+'si", "", $r_Notes[1]);

		 $r_Notes[0]=~s/\s+//sig ;
		my $strNotes = $r_Notes[0];
		
		print "=strNotes=$strNotes=\n\n";
		my @aNotes   = $self->_imelody2array($strNotes);

		print Dumper(@aNotes);

		my $part = 0;
		my @arr_iMelody;

		$arr_iMelody[$part] = $iMelodyHead;

		my $iMelodyFoot = "\nEND:IMELODY";

		# ����� ������� �������� iMelody
		my $segm_len = $max_len - $upi_len - $cat16_len - $add_len;
		for(my $i=0; $i<scalar(@aNotes); $i++)
		{
			#���� �� ������, �� ��������� ������� ������� � �������� ���������
			if (length($arr_iMelody[$part].$aNotes[$i].$iMelodyFoot) < $segm_len)
			{
			   $arr_iMelody[$part] .=	$aNotes[$i];
			}	 
			else
			{
				$arr_iMelody[$part] .= $iMelodyFoot;
				#//���� ���������� ��� � ���������� ��������
				$arr_iMelody[++$part] = $iMelodyHead.$aNotes[$i];
				#//����������� ��������� ��� UPI
				$segm_len = $max_len - $cat16_len - $add_len;
			}
		}
		#//������� ��������� �������
		$arr_iMelody[$part] .=	$iMelodyFoot;
		
		print "=\n";
		print Dumper(@arr_iMelody);
		print "=\n";

		return \@arr_iMelody;
}



sub _imelody2array {
#������ - ������ ���������� � ������� iMelody {<silence>|<note>|<led>|<vib>|<repeat>|<volume>|<backlight>}+
my $self=shift; 
my $iMelody=shift;
my @aNotes;
my $strMelody = $iMelody;
my $regexp = '[[:space:]]*(ledoff|ledon|viboff|vibon|backon|backoff|([*][0-8])?[&#]?[cdefgabh][0-5][.:;]?|r[0-5][.:;]?|V[+-]|V[0-9]{1,2}|[(][^()]*[)])';
		while(length($strMelody)>0  && defined ( my @parts = 	$strMelody=~/^($regexp)(.*)$/	) )
		{
			
			$parts[0]=~s/\s+(.*?)\s+/$1/o;
			$parts[3]=~s/\s+(.*?)\s+/$1/o;

			push @aNotes , trim($parts[0]);
			$strMelody = $parts[3];
		}
		return @aNotes;
}


sub ev {
  sprintf("%02X",shift);
}

sub trim {
my $item = shift; 
	 $item=~s/^\s{1,}(.*?)\s{1,}$/$1/;
	 
	 $item; 
 
}

1; 