package IMMO::Converter::RTConverter;
use lib "/home/projects/lib";
use URI::Escape qw(uri_escape);
use MIME::Base64;
use LWP;
use HTTP::Request::Common qw(POST);

#use URI:Escape;

use base 'IMMO::Converter::RTDebug';

1; 
	sub new 
	{
    my $class = shift;
     
		my $this = $class->SUPER::new(); 

		bless $this, ref $class || $class ;

		$this->{converter_url} = "http://varda.mtu.immo:8080/rtconverter/rtconverter?";
		$this->{_out_format}="";
		$this->{_input_format}="";
		$this->{_input_filename}="";
		$this->{_options}="";
		$this->{_options_compact}="";
		$this->{_actions}="";
		$this->{_volume}="";
		$this->{_song_name}="";
		$this->{_limit_duration}="";
		$this->{_max_filesize}="";
		$this->{_loaded_time_start}="";
		$this->{_truncate_chanels}="";
		$this->{debug} = true;
		$this->{show_converter_path} = false;
		$this->{_loaded_time_start} = $this->getmicrotime();
		return $this;
	}

	sub clear 
	{
		$this->{_out_format}        = undef;
		$this->{_input_format}      = undef;
		$this->{_input_filename}    = undef;
		$this->{_options}           = undef;
		$this->{_actions}           = undef;
		$this->{_volume}            = undef;
		$this->{_song_name}         = undef;
		$this->{_limit_duration}    = undef;
		$this->{_max_filesize}      = undef;
		$this->{_loaded_time_start} = undef;

		return true;
	}

	sub set_outformat
	{
		my ($this,$format)=@_;
		$this->{_out_format} = $format;
	}

	sub set_inputformat
	{
		my ($this,$format)=@_;
		$this->{_input_format} = $format;
	}


	sub set_inputfilename
	{
		my ($this,$filename)=@_;
		$this->{_input_filename} = $filename;
	}

	sub set_volume
	{
		my ($this,$volume)=@_;
		$this->{_volume} = "VO".$volume;

		return true;
	}

	sub set_song_name
	{
		my ($this,$song_name)=@_;
		$this->{_song_name} = "SN".$this->w1251_to_lat($song_name);
		return true;
	}

	sub set_compact
	{
		my ($this)=@_;
		$this->{_options_compact} = "COMPACT";
		return true;
	}

	sub set_limit_duration
	{
		my ($this,$milliseconds)=@_;
		$this->{_limit_duration} = "LT".$milliseconds;
		return true;
	}

	sub set_truncate_chanels
	 {

		my ($this,$chanels)=@_;
		$this->{_truncate_chanels} = "TC".$chanels;
		return true;
	}

	sub set_limit_notes {

		my ($this,$chanels)=@_;
		$this->{_truncate_chanels} = "LI".$chanels;
		return true;
	}

	sub set_removing_silence {

		my ($this)=@_;
		$this->{_removing_silence} = "TR";
		return true;
	}

	sub set_max_filesize
	{
		my ($this,$bytes)=@_;
		$this->{_max_filesize} = "LS".$bytes;
		return true;
	}

	sub convert
	{
		my ($this,$input, $output_file, $input_type)=@_;


		if($input_type eq "file") {
			$this->_debug("������ �� �������������� �������: $input => $output_file");
		}else {
			$this->_debug("������ �� �������������� �������");
		}

		if($input_type eq "file") {

			if(! -e $input) {return $this->_error("����������� ������� �� ����������: \"$input\"");}

			unless ( defined ($in_melody_data=$this->_get_filedata($input))  ) 
			{
				return $this->_error("���� ����: $input");
			}
			
			
			print "in_melody_data=$in_melody_data\n";
			# ���������� � �������� �����������
			$this->_debug("������������ post-�������");

			$file_name = $this->{_input_filename};
			if(!$file_name) {
				$file_name = $this->_get_filename($input);
			}
		}else {
			$file_name = $this->{_song_name};
			$in_melody_data = $input;
		}

		$convert_url = $this->{converter_url}."filename=".uri_escape($file_name)."&ringtone=".  uri_escape( encode_base64($in_melody_data,'' ) )."&outformat=". uri_escape($this->{_out_format})."&options=".   uri_escape($this->_get_options)."&actions=".uri_escape($this->_get_actions);		

		if($this->{_input_format}) {
			$convert_url .= "&format=".uri_escape($this->{_input_format});
			}


		if($this->{show_converter_path} eq true) {
			print "\n".$convert_url."\n";
		}
		# ���������� � �������� �����������
		$this->_debug("���������� � �������� �����������");
		if($post_str = $this->HTTP_Post($convert_url)) {
		;
		} else {
			$this->_debug("������ ���������� � ��������");
		}

		$response_code = unpack("H*", substr($post_str, 0, 1));
		$response_msg  = substr($post_str, 2, length($post_str));

		if($response_code eq "00") {
			$this->_debug("00 - ������ �������� �������, ����� ������: ".length($post_str));
		}
		elsif ($response_code eq "FF") {
			return $this->_error("FF - ������: ".$response_msg);
		}
		else    {
			return $this->_error("Unknown response! ".$response_msg);
		}

		if(!$output_file) {
		
			return $response_msg;
		}else {

			open F, ">".$output_file or return $this->_error("���������� ������� ���� ��� ������ ������� $output_file $! ");
			print F $response_msg;
			close F;

		}
		$this->_debug("������� ������� ����������������");
 
		return 1;
	}

	sub get_hex_data
  {
		my ($this,$binary_string)=@_;
		return uc(unpack('H',$binary_string));
	}

	sub _get_actions {
		my ($this)=@_;

		if($this->{_volume} ne "")          {   push @r_actions , $this->{_volume};}
		if($this->{_song_name} ne "")        {  push @r_actions , $this->{_song_name};}
		if($this->{_limit_duration} ne "") {	push @r_actions , $this->{_limit_duration};}
		if($this->{_max_filesize} ne "")    {   push @r_actions , $this->{_max_filesize};}
		if($this->{_truncate_chanels} ne "") {  push @r_actions , $this->{_truncate_chanels};}
		if($this->{_removing_silence} ne "")  { push @r_actions , $this->{_removing_silence};}

		return join("|", @r_actions);
	}

	sub  _get_options {
		my ($this)=@_;
		if($this->{_options_compact} ne "") {push @r_options, $this->{_options_compact};}
		return join("|", @r_options);
	}

	sub _get_filename
	{
		my ($this,$str)=@_;
#TODO
#		if(preg_match("'([^/]+)$'si", $str, $str_a)) {
#			if($str_a[1]) {return $str_a[1];}
#		}
	return $str;
	}

	sub HTTP_Post {
		my ($this,$URL)=@_;
		my $ua=LWP::UserAgent->new;
		$ua->agent("to rtconverter");
		
		$URL=~/(.*)\?(.*)/;

		 my $request=POST ($1,
									Content => $2
 								);
								
		print $request->as_string; 								

		my $response=$ua->request($request);

		print $response->as_string; 								

		die $response->message unless $response->is_success;

		my $content=$response->content;
		return $content;
	}

	# windows-1251 to translit
	sub w1251_to_lat
	{
		my ($this,$s)=@_;
		@pattern = ("'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'","'�'" );
		@replacement = ("a", "b", "v", "g", "d", "e", "yo", "zh", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sch","\"", "y", "'", "e'", "yu", "ya","A", "B", "V", 'G', "D", "E", "Yo", "Zh", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "Ch", "Sh", "Sch","~", "Y", "'", "E'", "Ju", "Ya" );
#		my $s=~/$pattern/$replacement/s;
		return $s;
	}

	sub _get_filedata
	{
		my ($this,$file)=@_;
		my $str_out = "";
		open F, "<$file" or return $this->_error("���������� ������� ����: \"$file\"");
		while (<F>) {
			$str_out .= $_;
		}
		close F;

		return $str_out;
	}

