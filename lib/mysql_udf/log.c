#include <mysql.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <string.h>


#include <sys/types.h>
#include <sys/stat.h>

// create function asg_repl_log returns integer soname 'libasg_log_udf.so';

#define LOG_DIR "/gate/asg5/var/repl"
#define LOG_FILE LOG_DIR "/repl-%04d-%02d-%02d-GMT.log"

#define LOG_NAME_SIZE (sizeof(LOG_FILE) + 128)

static pthread_mutex_t LOCK_write = PTHREAD_MUTEX_INITIALIZER;
static FILE* log_file = 0;
static int log_file_day = -1;

long long asg_repl_log(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error);
my_bool asg_repl_log_init(UDF_INIT *initid, UDF_ARGS *args, char *message);

int need_reopen_file()
{

  struct tm lt;
  time_t ts = time(0);
  gmtime_r(&ts, &lt);

  if ( log_file_day == lt.tm_mday ) {
	return 0;
  }

  return 1;
}

int reopen_file() 
{
  struct tm lt;
  char tmp[LOG_NAME_SIZE];
  mode_t old_umask;

  time_t ts = time(0);
  gmtime_r(&ts, &lt);

  if ( log_file ) {
	fclose(log_file);
  }

  snprintf(tmp, LOG_NAME_SIZE-1, LOG_FILE, lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday);

  old_umask = umask(0);
  log_file = fopen(tmp, "a");
  umask(old_umask);

  if ( ! log_file ) {
	return 0;
  }

  log_file_day = lt.tm_mday;

  return 1;
}

long long asg_repl_log(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
  char tmp[256];
  struct tm lt;

  time_t ts = time(0);
  gmtime_r(&ts, &lt);

  // XXX - writes are atomic up to 512 bytes, maybe remove mutexes?
  (void)pthread_mutex_lock(&LOCK_write);

  fprintf(log_file,
		  "%04d-%02d-%02d %02d:%02d:%02d\t",
		  lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec);

  fwrite(args->args[0], args->lengths[0], 1, log_file);
  fputc('\t', log_file);
  fwrite(args->args[1], args->lengths[1], 1, log_file);
  fputc('\t', log_file);
  fwrite(args->args[2], args->lengths[2], 1, log_file);
  fputc('\n', log_file);

  // fflush is not fsync!
  fflush(log_file);

  (void)pthread_mutex_unlock(&LOCK_write);

  return 0;
}

my_bool asg_repl_log_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
  int reopen_result = 1;

  if (args->arg_count != 3) {
	strcpy(message,"asg_repl_log() requires three arguments");
    return 1;
  }

  initid->maybe_null = 1;

  if ( need_reopen_file() ) {
	(void)pthread_mutex_lock(&LOCK_write);
	if ( need_reopen_file() ) {
	  reopen_result = reopen_file();
	}
	(void)pthread_mutex_unlock(&LOCK_write);
  }

  if ( ! reopen_result ) {
	strcpy(message,"asg_repl_log() cant reopen log file");
    return 1;
  }


  args->arg_type[0] = STRING_RESULT;
  args->arg_type[1] = STRING_RESULT;
  args->arg_type[2] = STRING_RESULT;

  return 0;
}

/* void test_logger() */
/* { */
/*   UDF_INIT init; */
/*   UDF_ARGS args; */

/*   char tmp[1024]; */
/*   enum Item_result arg_type[3]; */
/*   char *args_a[] = { "tr_inboxa", "update", "inbox_id = 1343 and outbox_id = 75335" }; */
/*   unsigned long lengths[] = { 9, 6, 37}; */

/*   args.args = args_a; */
/*   args.arg_type = arg_type; */
/*   args.lengths = lengths; */
/*   args.arg_count = 3; */

/*   asg_repl_log_init(&init, &args, tmp); */
/*   asg_repl_log(&init, &args, 0, 0); */
/* } */

/* int main(int argc, char **argv) */
/* { */
/*   int i; */

/*   // speedcheck - approx 1 mln in second at devel2 */
/*   /\* for (i = 0; i < 10000000; i++) { *\/ */
/*   /\* 	(void)need_reopen_file(); *\/ */
/*   /\* } *\/ */
/*   // reopen_file(); */
/*   test_logger(); */
/* } */

/* /\* */



/* *\/ */
