#!/usr/bin/perl -w
use strict;
use warnings;
use Data::Dumper;
use MIME::Parser;
use MIME::QuotedPrint;
use Encode qw/decode encode/;
use URI;
use URI::URL;
use LWP::UserAgent;
use HTTP::Request;

my ($yell_from,$yell_to) = ('notify@sms.agms.com.ua','v.islamov@alt1.ru');
my $target = 'http://gateasg.alt1.ru/sms.exe';

my $contents;
while(<STDIN>) {$contents.=$_;};
my %params = process_mail($contents);
print Dumper(\%params);
my $ua = LWP::UserAgent->new(agent => 'email-script');
$ua->timeout(10);
my $url = url($target);
$url->query_form(%params);
print "$url\n";
my $resp = $ua->get($url);
my $reply = $resp->as_string();
print $reply;
if ($resp->code() eq '200') {
} else {
	yell('Connection error', "URL: $target\nParams: ".Dumper(\%params)."Response: $reply");
}

sub process_mail {
	my ( $contents ) = @_;

	my $m = new MIME::Parser;
	$m->decode_headers(1);
	$m->output_to_core(1);

	my $e = $m->parse_data($contents);

	chomp($params{from} = $e->get('from'));
	chomp($params{to} = $e->get('to'));
	chomp($params{'message-id'} = $e->get('message-id'));
	chomp($params{date} = $e->get('date'));

	chomp(my $ct = $e->get('content-type'));
	chomp(my $te = $e->get('content-transfer-encoding'));;

	chomp(my $msg = join "\n", map { chomp $_; $_ } @{$e->body});

	if ($te eq 'quoted-printable') {
	    $msg = decode_qp($msg);
	} elsif ($te eq '8bit') {
	    # Do nothing
	} else {
	    _yell("Unknown transfer encoding: $te",
		 header => "Unknown transfer-encoding");
	    return;
	}

	if ($ct eq 'text/plain; charset=us-ascii') {
	    # Do nothing
	} elsif ($ct eq 'text/plain; charset=koi8-r') {
	    $msg = encode('cp1251', decode('koi8-r', $msg));
	} elsif ($ct eq 'text/plain; charset=koi8-u') {
	    $msg = encode('cp1251', decode('koi8-u', $msg));
	} else {
	    _yell("Unknown content-type: $ct",
		 header => 'Cannot handle unknown content-type in e-mail');
	    return;
	}

	$params{msg} = $msg;
	return %params;
}

sub yell {
	my ( $subject, $body ) = @_;
	
	my $rc = open my $sm, "|-", "/usr/lib/sendmail", "-f", $yell_from, "-t"
        	or die "SM failed: $!";

print $sm <<EOF;
From: $yell_from
To: $yell_to
Subject: $subject
Content-Type: text/plain; charset=koi8-r
Content-Transfer-Encoding: quoted-printable
Mime-Version: 1.0

$body
EOF

	close $sm;
}

1;




