#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use MIME::QuotedPrint;
use Encode qw/encode decode/;
use CGI qw/:standard/;


my $query = new CGI;
my $params = $query->Vars;
$params->{body} = encode_qp(encode('koi8-r', decode('cp1251',$params->{msg})));
$params->{envelope_from} = $params->{from};

my $rc = open my $sm, "|-", "/usr/lib/sendmail", "-f", $params->{envelope_from}, "-t"
	or die "SM failed: $!";

print $sm <<EOF;
From: $params->{from}
To: $params->{to}
Subject: $params->{subject}
Content-Type: text/plain; charset=koi8-r
Content-Transfer-Encoding: quoted-printable
Mime-Version: 1.0

$params->{body}
EOF

close $sm;

print $query->header(-type => 'text/html', -charset => 'cp1251');
print 'OK';

1;
