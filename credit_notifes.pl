#!/usr/bin/perl
# -*- tab-width: 4 -*-
package main;
use strict;
use warnings;

use FindBin;
use lib "$FindBin::RealBin/../lib";
use English '-no_match_vars';
use Carp;

use Config::Tiny;
use Disp::Config qw(param);
use Disp::Utils qw(yell);

use Translit;

use LWP::UserAgent;
use URI;

use utf8;
use Encode;

use Log::Log4perl qw/get_logger/;
use Data::Dumper;

use constant GET_ALL_NOTIFIES => 10;
use constant NEW_STATUS => 'new';
use constant RESTART_STATUS => 'restart';
use constant MAX_TRY_CNT => 5;
use constant MAX_TRY_TIME => 1800;
use constant UA_TIMEOUT => 3;

Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

my %log_conf = (
		"log4perl.rootLogger" => "INFO, Logfile",
		"log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
		"log4perl.appender.Logfile.recreate" => 1,
		"log4perl.appender.Logfile.recreate_check_interval" => 300,
		"log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
		"log4perl.appender.Logfile.umask" => "0000",
		"log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/credit_notification.log',
		"log4perl.appender.Logfile.layout" => 'PatternLayout',
		"log4perl.appender.Logfile.layout.ConversionPattern" => '[%d{DATE}] {%P} %p %m%n'
		);

Log::Log4perl->init( \%log_conf);
my $log	= get_logger("");
my $ua	= LWP::UserAgent->new();
# Работаем только с utf8.
#Config->dbh->do('set names utf8');

$log->info('Start...');
# Берем 5 новых нотификаций.
my $new_notification = Config->dbh->selectall_arrayref("SELECT n.transaction_id, notify_id credit_installment_id, credit_type, credit_total, credit_remaining, credit_installment, `status`, next_date, i.msg, i.num, i.operator_id, i.abonent user_id, i.date, i.test_flag, i.transport_count, b.service_id, o.name operator, s.gmt_offset, r.uri, r.notify_email, n.retry_cnt
FROM tr_credit_notifies n 
JOIN tr_inboxa i ON n.transaction_id = i.id
JOIN tr_billing b ON b.inbox_id = i.id AND b.dir = 1
JOIN set_operator o ON o.id = i.operator_id
JOIN set_smsc s ON s.id = i.smsc_id
JOIN set_service r ON r.id = b.service_id
WHERE `status` = ? LIMIT ?", {Slice=>{}}, NEW_STATUS, GET_ALL_NOTIFIES) || [];

$log->info('Load: ' . scalar(@$new_notification) . " new notification");

my $retry_limit = GET_ALL_NOTIFIES - scalar(@$new_notification);

# Берем (Все - Новые) повторов.  
my $retry_notification = Config->dbh->selectall_arrayref("SELECT n.transaction_id, notify_id credit_installment_id, credit_type, credit_total, credit_remaining, credit_installment, `status`, next_date, i.msg, i.num, i.operator_id, i.abonent, i.date, i.test_flag, i.transport_count, b.service_id, o.name, s.gmt_offset, r.uri, r.notify_email, n.retry_cnt
FROM tr_credit_notifies n 
JOIN tr_inboxa i ON n.transaction_id = i.id
JOIN tr_billing b ON b.inbox_id = i.id AND b.dir = 1
JOIN set_operator o ON o.id = i.operator_id
JOIN set_smsc s ON s.id = i.smsc_id
JOIN set_service r ON r.id = b.service_id
WHERE `status` = ? and next_date < NOW() LIMIT ?", {Slice=>{}}, RESTART_STATUS, $retry_limit) || [];

$log->info('Load: ' . scalar(@$new_notification) . " retry notification");

for my $notification ( @$new_notification, @$retry_notification  ) {
	my $label = '[' . $notification->{transaction_id} . ':' . $notification->{credit_installment_id} . ']';

	$notification->{msg_trans} = Translit::translit_text($notification->{msg});
	notification2log($notification, $label);

	# Не передаются следующие параметры.
	# op, region, op_transaction_id
	my $notify_email 	= delete($notification->{notyfy_email});
	my $retry_cnt		= delete($notification->{retry_cnt});
	
	my $uri = URI->new(delete($notification->{uri}));
	$uri->query_form($notification);
	my $url = $uri->as_string();
	$log->warn( $label . ' Url:' . $url);
	my $res = '';
	eval {
		local $SIG{ALRM} = sub { die "LWP::UserAgent timeout" };
		alarm UA_TIMEOUT;
		$res	= $ua->get($url);
		alarm 0;

		};
		if ($@) {
			yell('Notification send timeout', contact_group => 'yell_credit_notification', header => '[credit notify] Timeout');	
			set_retry( $notification->{credit_installment_id}, $retry_cnt, $label );
		}
		else {
			if ( $res->is_success ) {
				my $error = parse_ansver($res->decoded_content, $label, $notification->{credit_installment_id}, $retry_cnt, $notify_email);  # or whatever
				unless ( $error ) {
					set_ok( $notification->{credit_installment_id}, $label );
				}
			}
			else {
				$log->error($label . ' Error: ' . $res->status_line);
				yell($res->status_line, contact_group => 'yell_credit_notification', header => '[credit notify] Wrong HTTP answer');	
				set_retry( $notification->{credit_installment_id}, $retry_cnt, $label );
			}
		}
}

sub notification2log {
	my $notification 	= shift;
	my $label			= shift;

	my $info = $label . 'Info ' . "\n";
	for my $name ( keys %$notification ) {
		unless ($notification->{$name}) { $notification->{$name} = ''};
		$info .= $name . ' = "' . $notification->{$name} . '"' . " ";
	}

	$log->warn($info);
}

sub parse_ansver {
	my $ansver 					= shift;
	my $label					= shift;
	my $notify_id				= shift;
	my $retry_cnt 				= shift || 1;
	my $notify_email			= shift;
	
	$log->warn( $label . ' Ansver: ' . $ansver);	
	my $error = 0;
	unless ( $ansver =~ /^ok$/) {
		if ( $notify_email ) {
			yell($ansver, contact_group => 'yell_credit_notification', header => '[credit notify] Wrong answer', cc => $notify_email);	
		}
		else {
			yell($ansver, contact_group => 'yell_credit_notification', header => '[credit notify] Wrong answer');	
		}
		
		set_retry( $notify_id, $retry_cnt, $label );
		$error = 1;
	}
	return $error;
}

sub set_ok {
	my $notify_id				= shift;
	my $label					= shift;

	if ( $notify_id ) {
		eval{Config->dbh->do('delete from tr_credit_notifies where notify_id = ?', undef, $notify_id)};
		if ( $@ ) {
			yell($@, contact_group => 'yell_credit_notification', header => '[credit notify] Cant delete notification!');	
			$log->error( $label . ' Problem remove: ' . $@);
		}
		else {
			$log->warn( $label . ' notification remove.');
		}
	}
	else {
		yell('Empty notify_id', contact_group => 'yell_credit_notification', header => '[credit notify] Cant delete notification!');
		$log->error( $label . "Problem remove empty notify_id");
	}
}


sub set_retry {
	my $notify_id				= shift;
	my $try_cnt					= shift || 1;
	my $label					= shift;

	my $try2time	= {
					'1' => 20,
					'2' => 40, 
					'3' => 600,
					'4' => 600,
					'5' => 600
						};

		if ( $notify_id) {
		my $retry_time = $try2time->{$try_cnt} ||  MAX_TRY_TIME;
		$log->warn( 'UPDATE tr_credit_notifies SET `status` = ' . RESTART_STATUS . ' , retry_cnt = retry_cnt + 1, next_date = DATE_ADD(NOW(), INTERVAL ' . $retry_time . '  SECOND)  where notify_id =' . $notify_id);
		eval{Config->dbh->do('UPDATE tr_credit_notifies SET `status` = ?, retry_cnt = retry_cnt + 1, next_date = DATE_ADD(NOW(), INTERVAL ?  SECOND)  where notify_id = ?', undef, RESTART_STATUS, $retry_time, $notify_id)};
		if ( $@ ) {
			yell($@, contact_group => 'yell_credit_notification', header => '[credit notify] Cant update notification!');
			$log->error( $label . ' Problem: update' . $@);
		}
		else {
			$log->warn( $label . ' notification update.');
		}
	}
	else {
		yell('Empty notify_id', contact_group => 'yell_credit_notification', header => '[credit notify] Cant update notification!');
		$log->error( $label . "Problem update empty notify_id");
	}
}

