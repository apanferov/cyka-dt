-- MySQL dump 10.10
--
-- Host: localhost    Database: alt4
-- ------------------------------------------------------
-- Server version	5.0.18-standard

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a`
--

DROP TABLE IF EXISTS `a`;
CREATE TABLE `a` (
  `b` varchar(255) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `access_map`
--

DROP TABLE IF EXISTS `access_map`;
CREATE TABLE `access_map` (
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `service_id` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`abonent`,`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `asg_errors`
--

DROP TABLE IF EXISTS `asg_errors`;
CREATE TABLE `asg_errors` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `err_type` int(11) unsigned default NULL,
  `err_num` int(11) unsigned default NULL,
  `err_subtype` varchar(20) default NULL,
  `err_text` varchar(200) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `err_type_err_num` (`err_type`,`err_num`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
CREATE TABLE `billing` (
  `inbox_id` int(11) unsigned NOT NULL default '0',
  `outbox_id` int(11) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `service_id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(10) unsigned NOT NULL,
  `operator_id` int(10) unsigned default NULL,
  `num` varchar(20) NOT NULL,
  `insms_count` tinyint(3) unsigned NOT NULL default '0',
  `outsms_count` tinyint(3) unsigned NOT NULL default '0',
  `bill_in` varchar(45) default NULL,
  `bill_out` varchar(45) default NULL,
  `push_id` int(11) unsigned default NULL,
  `dir` tinyint(3) unsigned NOT NULL default '0',
  `partner_id` int(11) default NULL,
  `legal_owner_id` int(11) default NULL,
  `category` varchar(20) default NULL,
  PRIMARY KEY  (`inbox_id`,`outbox_id`),
  KEY `outbox_id` (`outbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `billing_log`
--

DROP TABLE IF EXISTS `billing_log`;
CREATE TABLE `billing_log` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `description` varchar(256) NOT NULL default '',
  `time_stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Table structure for table `billing_plug_service`
--

DROP TABLE IF EXISTS `billing_plug_service`;
CREATE TABLE `billing_plug_service` (
  `date` date NOT NULL default '0000-00-00',
  `service_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL default '',
  `insms_count` int(11) NOT NULL default '0',
  `outsms_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`date`,`service_id`,`operator_id`,`num`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `billing_price`
--

DROP TABLE IF EXISTS `billing_price`;
CREATE TABLE `billing_price` (
  `date` date NOT NULL default '0000-00-00',
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `price_in` double NOT NULL default '0',
  `price_out` double NOT NULL default '0',
  `currency` char(3) NOT NULL default 'rur',
  PRIMARY KEY  (`date`,`operator_id`,`num`),
  KEY `FK_billing_price_operator_id` (`operator_id`),
  CONSTRAINT `FK_billing_price_operator_id` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `billinga`
--

DROP TABLE IF EXISTS `billinga`;
CREATE TABLE `billinga` (
  `inbox_id` int(11) unsigned NOT NULL default '0',
  `outbox_id` int(11) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `service_id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(10) unsigned NOT NULL,
  `operator_id` int(10) unsigned default NULL,
  `num` varchar(20) NOT NULL,
  `insms_count` tinyint(3) unsigned NOT NULL default '0',
  `outsms_count` tinyint(3) unsigned NOT NULL default '0',
  `bill_in` varchar(45) default NULL,
  `bill_out` varchar(45) default NULL,
  `push_id` int(11) unsigned default NULL,
  `dir` tinyint(3) unsigned NOT NULL default '0',
  `partner_id` int(11) default NULL,
  `legal_owner_id` int(11) default NULL,
  `category` varchar(20) default NULL,
  PRIMARY KEY  (`inbox_id`,`outbox_id`),
  KEY `idx_billinga_service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `consumption_filter_legal_owner`
--

DROP TABLE IF EXISTS `consumption_filter_legal_owner`;
CREATE TABLE `consumption_filter_legal_owner` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumption_rule_id` int(11) NOT NULL,
  `legal_owner_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_consumption_filter_legal_owner_1` (`money_consumption_rule_id`),
  KEY `legal_owner_id` (`legal_owner_id`,`category`),
  CONSTRAINT `FK_consumption_filter_legal_owner_1` FOREIGN KEY (`money_consumption_rule_id`) REFERENCES `money_consumption_rule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `consumption_filter_num`
--

DROP TABLE IF EXISTS `consumption_filter_num`;
CREATE TABLE `consumption_filter_num` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumption_rule_id` int(11) NOT NULL,
  `num` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_consumption_filter_num_1` (`money_consumption_rule_id`),
  KEY `num` (`num`),
  CONSTRAINT `FK_consumption_filter_num_1` FOREIGN KEY (`money_consumption_rule_id`) REFERENCES `money_consumption_rule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `consumption_filter_operator`
--

DROP TABLE IF EXISTS `consumption_filter_operator`;
CREATE TABLE `consumption_filter_operator` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumption_rule_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_consumption_filter_operator_1` (`money_consumption_rule_id`),
  KEY `operator_id` (`operator_id`),
  CONSTRAINT `FK_consumption_filter_operator_1` FOREIGN KEY (`money_consumption_rule_id`) REFERENCES `money_consumption_rule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `consumption_filter_partner`
--

DROP TABLE IF EXISTS `consumption_filter_partner`;
CREATE TABLE `consumption_filter_partner` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumption_rule_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_consumption_filter_partner_1` (`money_consumption_rule_id`),
  KEY `partner_id` (`partner_id`),
  CONSTRAINT `FK_consumption_filter_partner_1` FOREIGN KEY (`money_consumption_rule_id`) REFERENCES `money_consumption_rule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `consumption_filter_service`
--

DROP TABLE IF EXISTS `consumption_filter_service`;
CREATE TABLE `consumption_filter_service` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumption_rule_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_consumption_filter_service_1` (`money_consumption_rule_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `FK_consumption_filter_service_1` FOREIGN KEY (`money_consumption_rule_id`) REFERENCES `money_consumption_rule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `current_chain`
--

DROP TABLE IF EXISTS `current_chain`;
CREATE TABLE `current_chain` (
  `money_consumer_id` int(11) NOT NULL default '0',
  `money_consumption_rule_id` int(11) NOT NULL default '0',
  `k_price_abonent` double NOT NULL default '0',
  `k_price_rest` double NOT NULL default '0',
  `b_price_fixed` double NOT NULL default '0',
  `priority` int(11) default NULL,
  `currency` char(3) NOT NULL default 'rur',
  `money_consumer_currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `debug`
--

DROP TABLE IF EXISTS `debug`;
CREATE TABLE `debug` (
  `id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `category` varchar(20) default NULL,
  `msg` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `error_log`
--

DROP TABLE IF EXISTS `error_log`;
CREATE TABLE `error_log` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL,
  `message` mediumtext NOT NULL,
  `process_name` varchar(100) NOT NULL,
  `code` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `errsms`
--

DROP TABLE IF EXISTS `errsms`;
CREATE TABLE `errsms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `inbox_id` int(11) unsigned default '0',
  `errcode` varchar(20) NOT NULL default '',
  `errmessage` mediumtext NOT NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `push_id` int(11) unsigned default NULL,
  `response_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_errsms_inbox_id` (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `exchange_rate`
--

DROP TABLE IF EXISTS `exchange_rate`;
CREATE TABLE `exchange_rate` (
  `date` date NOT NULL,
  `currency` char(3) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY  (`date`,`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `fraud_abonent`
--

DROP TABLE IF EXISTS `fraud_abonent`;
CREATE TABLE `fraud_abonent` (
  `abonent` varchar(20) NOT NULL,
  `money_total` double NOT NULL default '0',
  `is_fraud` tinyint(1) NOT NULL default '0',
  `in_whitelist` tinyint(1) NOT NULL default '0',
  `operator_id` int(11) NOT NULL,
  `currency` char(3) NOT NULL default 'rur',
  `operator_fraud_limit` double NOT NULL default '0',
  PRIMARY KEY  (`abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
CREATE TABLE `inbox` (
  `id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL,
  `operator_id` int(11) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `msg` mediumtext,
  `msg_type` varchar(30) NOT NULL default 'sms',
  `transport_type` varchar(30) NOT NULL default 'sms',
  `transaction_id` bigint(20) unsigned default NULL,
  `transport_ids` varchar(100) default NULL,
  `transport_count` tinyint(3) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `test_flag` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_smsc_id` (`smsc_id`),
  CONSTRAINT `fk_smsc_id` FOREIGN KEY (`smsc_id`) REFERENCES `smsc_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `inboxa`
--

DROP TABLE IF EXISTS `inboxa`;
CREATE TABLE `inboxa` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL,
  `operator_id` int(11) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `msg` mediumtext,
  `msg_type` varchar(30) NOT NULL default 'sms',
  `transport_type` varchar(30) NOT NULL default 'sms',
  `transaction_id` bigint(20) unsigned default NULL,
  `transport_ids` varchar(100) default NULL,
  `transport_count` tinyint(3) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `test_flag` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_inboxa_smsc_id` (`smsc_id`),
  KEY `idx_inboxa_operator_id` (`operator_id`),
  KEY `idx_inboxa_num` (`num`),
  KEY `idx_inboxa_abonent` (`abonent`),
  KEY `idx_inboxa_date` (`date`),
  CONSTRAINT `fk_inboxa_smsc_id` FOREIGN KEY (`smsc_id`) REFERENCES `smsc_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `inmmsa`
--

DROP TABLE IF EXISTS `inmmsa`;
CREATE TABLE `inmmsa` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(11) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `transaction_id` varchar(20) default NULL,
  `content` blob,
  `data` blob NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_inmmsa_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `inmmsa_data`
--

DROP TABLE IF EXISTS `inmmsa_data`;
CREATE TABLE `inmmsa_data` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `inmmsa_id` int(11) unsigned NOT NULL default '0',
  `content_id` varchar(50) default NULL,
  `content_type` varchar(50) default NULL,
  `filename` varchar(100) default NULL,
  `data` blob,
  PRIMARY KEY  (`id`),
  KEY `idx_inmmsa_data_inmmsa_id` (`inmmsa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `insms`
--

DROP TABLE IF EXISTS `insms`;
CREATE TABLE `insms` (
  `id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(11) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `msg` varchar(255) default NULL,
  `data_coding` tinyint(3) unsigned NOT NULL default '0',
  `esm_class` tinyint(3) unsigned NOT NULL default '0',
  `transaction_id` bigint(20) unsigned default NULL,
  `ussd_service_op` tinyint(3) unsigned default NULL,
  `sar_msg_ref_num` smallint(6) unsigned default NULL,
  `sar_total_segments` tinyint(3) unsigned default NULL,
  `sar_segment_seqnum` tinyint(3) unsigned default NULL,
  `user_message_reference` smallint(6) unsigned default NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `pdu` blob,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `insmsa`
--

DROP TABLE IF EXISTS `insmsa`;
CREATE TABLE `insmsa` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `msg` varchar(255) default NULL,
  `data_coding` tinyint(3) unsigned NOT NULL default '0',
  `esm_class` tinyint(3) unsigned NOT NULL default '0',
  `transaction_id` bigint(20) unsigned default NULL,
  `ussd_service_op` tinyint(3) unsigned default NULL,
  `sar_msg_ref_num` smallint(6) unsigned default NULL,
  `sar_total_segments` tinyint(3) unsigned default NULL,
  `sar_segment_seqnum` tinyint(3) unsigned default NULL,
  `user_message_reference` smallint(6) unsigned default NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `pdu` blob,
  PRIMARY KEY  (`id`),
  KEY `idx_insmsa_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `legal_entity`
--

DROP TABLE IF EXISTS `legal_entity`;
CREATE TABLE `legal_entity` (
  `id` int(11) NOT NULL auto_increment,
  `fio` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `icq` varchar(30) default NULL,
  `ras_account` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `cor_account` varchar(255) NOT NULL,
  `bik` varchar(255) NOT NULL,
  `card_number` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `legal_owner`
--

DROP TABLE IF EXISTS `legal_owner`;
CREATE TABLE `legal_owner` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumer_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!50001 DROP VIEW IF EXISTS `manager`*/;
/*!50001 CREATE TABLE `manager` (
  `id` int(11) NOT NULL default '0',
  `legal_entity_id` int(11) NOT NULL default '0' COMMENT '����������� ����',
  `fio` varchar(255) NOT NULL default '' COMMENT '���',
  `phone` varchar(30) NOT NULL default '' COMMENT '�������',
  `email` varchar(255) NOT NULL default '' COMMENT 'EMAIL',
  `icq` varchar(30) default NULL COMMENT 'ICQ',
  `wap_url` varchar(255) default NULL,
  `web_url` varchar(255) default NULL,
  `login` varchar(30) NOT NULL default '',
  `password` varchar(30) default NULL,
  `contract_number` varchar(30) default NULL COMMENT '�����, ���� ��������',
  `payment_way` varchar(10) default NULL,
  `payment_comment` text,
  `payment_account` varchar(255) default NULL,
  `bank_name` varchar(255) default NULL,
  `bank_ras_account` varchar(30) default NULL,
  `bank_cor_account` varchar(30) default NULL,
  `bank_bik` varchar(30) default NULL,
  `comment` text,
  `name` varchar(255) NOT NULL default '',
  `reg_date` datetime default NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `activate_date` datetime default NULL,
  `manager_id` int(10) unsigned default NULL,
  `money` double NOT NULL default '0',
  `money_fraud` double NOT NULL default '0',
  `currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `manager_t`
--

DROP TABLE IF EXISTS `manager_t`;
CREATE TABLE `manager_t` (
  `id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  CONSTRAINT `FK_manager_t_1` FOREIGN KEY (`id`) REFERENCES `money_consumer_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `money_consumer`
--

DROP TABLE IF EXISTS `money_consumer`;
/*!50001 DROP VIEW IF EXISTS `money_consumer`*/;
/*!50001 CREATE TABLE `money_consumer` (
  `id` int(11) NOT NULL default '0',
  `legal_entity_id` int(11) NOT NULL default '0' COMMENT '����������� ����',
  `fio` varchar(255) NOT NULL default '' COMMENT '���',
  `phone` varchar(30) NOT NULL default '' COMMENT '�������',
  `email` varchar(255) NOT NULL default '' COMMENT 'EMAIL',
  `icq` varchar(30) default NULL COMMENT 'ICQ',
  `wap_url` varchar(255) default NULL,
  `web_url` varchar(255) default NULL,
  `login` varchar(30) NOT NULL default '',
  `password` varchar(30) default NULL,
  `contract_number` varchar(30) default NULL COMMENT '�����, ���� ��������',
  `payment_way` varchar(10) default NULL,
  `payment_comment` text,
  `payment_account` varchar(255) default NULL,
  `bank_name` varchar(255) default NULL,
  `bank_ras_account` varchar(30) default NULL,
  `bank_cor_account` varchar(30) default NULL,
  `bank_bik` varchar(30) default NULL,
  `comment` text,
  `name` varchar(255) NOT NULL default '',
  `reg_date` datetime default NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `activate_date` datetime default NULL,
  `_deleted` tinyint(1) unsigned NOT NULL default '0',
  `_deleted_date` datetime default NULL,
  `manager_id` int(10) unsigned default NULL,
  `money` double NOT NULL default '0',
  `money_fraud` double NOT NULL default '0',
  `currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `money_consumer_t`
--

DROP TABLE IF EXISTS `money_consumer_t`;
CREATE TABLE `money_consumer_t` (
  `id` int(11) NOT NULL auto_increment,
  `legal_entity_id` int(11) NOT NULL default '0' COMMENT '����������� ����',
  `fio` varchar(255) NOT NULL default '' COMMENT '���',
  `phone` varchar(30) NOT NULL default '' COMMENT '�������',
  `email` varchar(255) NOT NULL default '' COMMENT 'EMAIL',
  `icq` varchar(30) default NULL COMMENT 'ICQ',
  `wap_url` varchar(255) default NULL,
  `web_url` varchar(255) default NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(30) default NULL,
  `contract_number` varchar(30) default NULL COMMENT '�����, ���� ��������',
  `payment_way` varchar(10) default NULL,
  `payment_comment` text,
  `payment_account` varchar(255) default NULL,
  `bank_name` varchar(255) default NULL,
  `bank_ras_account` varchar(30) default NULL,
  `bank_cor_account` varchar(30) default NULL,
  `bank_bik` varchar(30) default NULL,
  `comment` text,
  `name` varchar(255) NOT NULL default '',
  `reg_date` datetime default NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `activate_date` datetime default NULL,
  `_deleted` tinyint(1) unsigned NOT NULL default '0',
  `_deleted_date` datetime default NULL,
  `manager_id` int(10) unsigned default '0',
  `money` double NOT NULL default '0',
  `money_fraud` double NOT NULL default '0',
  `currency` char(3) NOT NULL default 'rur',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `login_idx` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='����� ����, ���������� ������ � �';

--
-- Table structure for table `money_consumption_rule`
--

DROP TABLE IF EXISTS `money_consumption_rule`;
CREATE TABLE `money_consumption_rule` (
  `id` int(11) NOT NULL auto_increment,
  `money_consumer_id` int(11) NOT NULL,
  `filter_on_operator` tinyint(1) NOT NULL default '0',
  `filter_on_partner` tinyint(1) NOT NULL default '0',
  `filter_on_legal_owner` tinyint(1) NOT NULL default '0',
  `filter_on_service` tinyint(1) NOT NULL default '0',
  `filter_on_num` tinyint(1) NOT NULL default '0',
  `test_percentage` double NOT NULL default '0',
  `priority` int(11) default NULL,
  `comment` text,
  `k_price_abonent` double NOT NULL default '0',
  `k_price_rest` double NOT NULL default '0',
  `b_price_fixed` double NOT NULL default '0',
  `plugin_id` int(10) unsigned default NULL,
  `currency` char(3) NOT NULL default 'rur',
  PRIMARY KEY  (`id`),
  KEY `idx_mcr_plugin` (`plugin_id`),
  KEY `FK_money_consumption_rule_2` (`money_consumer_id`),
  CONSTRAINT `FK_money_consumption_rule_1` FOREIGN KEY (`plugin_id`) REFERENCES `plugin_cyka_t` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_money_consumption_rule_2` FOREIGN KEY (`money_consumer_id`) REFERENCES `money_consumer_t` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 1177600 kB; (`plugin_id`) REFER `alt4/plugin_t`';

--
-- Table structure for table `num_map`
--

DROP TABLE IF EXISTS `num_map`;
CREATE TABLE `num_map` (
  `abonent` varchar(20) NOT NULL default '',
  `transport_type` varchar(30) NOT NULL,
  `smsc_id` int(11) unsigned NOT NULL,
  `operator_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`abonent`,`transport_type`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `num_price`
--

DROP TABLE IF EXISTS `num_price`;
CREATE TABLE `num_price` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `date_start` date NOT NULL,
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `price_in` decimal(10,2) unsigned default NULL COMMENT 'incomming sms price for for abonent',
  `price_out` decimal(10,2) unsigned default NULL COMMENT 'outcomming sms price for alt1',
  `estimated_income` decimal(10,5) NOT NULL default '0.00000',
  `currency` char(3) NOT NULL default 'rur',
  `nds` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Index_3` (`date_start`,`operator_id`,`num`),
  KEY `Index` (`date_start`,`operator_id`,`num`),
  KEY `FK_num_price_1` (`operator_id`),
  CONSTRAINT `FK_num_price_1` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 1128448 kB; (`plug_id`) REFER `alt4/plug_t`(`id';

--
-- Table structure for table `operator`
--

DROP TABLE IF EXISTS `operator`;
/*!50001 DROP VIEW IF EXISTS `operator`*/;
/*!50001 CREATE TABLE `operator` (
  `id` int(11) unsigned NOT NULL default '0',
  `parent_id` int(10) unsigned default NULL,
  `name` varchar(255) NOT NULL default '',
  `level` int(10) unsigned NOT NULL default '0',
  `billing_rule` varchar(45) NOT NULL default '',
  `cnt` bigint(21) NOT NULL default '0',
  `koef` double NOT NULL default '0',
  `fraud_limit` double default NULL,
  `fraud_currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `operator_number_pool`
--

DROP TABLE IF EXISTS `operator_number_pool`;
CREATE TABLE `operator_number_pool` (
  `id` int(11) NOT NULL auto_increment,
  `number_prefix` varchar(20) NOT NULL,
  `operator_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `operator_number_pool_number_prefix_idx` (`number_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `operator_t`
--

DROP TABLE IF EXISTS `operator_t`;
CREATE TABLE `operator_t` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `_deleted` tinyint(3) unsigned NOT NULL default '0',
  `_deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `parent_id` int(10) unsigned default NULL,
  `level` int(10) unsigned NOT NULL default '0',
  `billing_rule` varchar(45) NOT NULL default 'default',
  `koef` double NOT NULL default '0.5',
  `fraud_limit` double default NULL,
  `fraud_currency` char(3) NOT NULL default 'rur',
  PRIMARY KEY  (`id`),
  KEY `is_deleted` (`_deleted`),
  KEY `Index_3` (`parent_id`),
  CONSTRAINT `FK_operator_t_1` FOREIGN KEY (`parent_id`) REFERENCES `operator_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `outbox`
--

DROP TABLE IF EXISTS `outbox`;
CREATE TABLE `outbox` (
  `id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL,
  `operator_id` int(11) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL,
  `msg` mediumtext,
  `content_type` varchar(100) NOT NULL default 'text/plain',
  `target_type` varchar(100) default NULL,
  `transport_type` varchar(30) NOT NULL default 'sms',
  `transaction_id` bigint(20) unsigned default NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `inbox_id` int(11) unsigned default '0',
  `push_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `outbox_id` (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `outboxa`
--

DROP TABLE IF EXISTS `outboxa`;
CREATE TABLE `outboxa` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(10) unsigned NOT NULL,
  `msg` mediumtext,
  `num` varchar(20) NOT NULL,
  `content_type` varchar(100) NOT NULL default '',
  `target_type` varchar(100) default NULL,
  `transport_type` varchar(30) NOT NULL default 'sms',
  `transaction_id` bigint(20) unsigned default NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `inbox_id` int(11) unsigned default '0',
  `push_id` int(11) unsigned default NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `operator_id` int(11) unsigned NOT NULL default '0',
  KEY `idx_outboxa_inbox_id` (`inbox_id`),
  KEY `idx_outboxa_num` (`num`),
  KEY `idx_outboxa_abonent` (`abonent`),
  KEY `idx_outboxa_date` (`date`),
  KEY `idx_outboxa_smsc_id` (`smsc_id`),
  KEY `idx_outboxa_operator_id` (`operator_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `outcode`
--

DROP TABLE IF EXISTS `outcode`;
CREATE TABLE `outcode` (
  `name` varchar(20) NOT NULL,
  `service_id` int(11) unsigned NOT NULL,
  `operator_id` int(11) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `smsc_id` int(11) unsigned default NULL,
  `id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`id`),
  KEY `idx_outcode_service_id` (`service_id`),
  KEY `idx_outcode_operator_id` (`operator_id`),
  KEY `idx_outcode_num` (`num`),
  CONSTRAINT `outcode_fk1` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`),
  CONSTRAINT `outcode_fk` FOREIGN KEY (`service_id`) REFERENCES `service_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 11264 kB; (`operator_id`) REFER `alt4/operator_';

--
-- Table structure for table `outsms`
--

DROP TABLE IF EXISTS `outsms`;
CREATE TABLE `outsms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(11) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `msg` varchar(255) default NULL,
  `data_coding` tinyint(3) unsigned NOT NULL default '0',
  `esm_class` tinyint(3) unsigned NOT NULL default '0',
  `transaction_id` bigint(20) unsigned default NULL,
  `ussd_service_op` tinyint(3) unsigned default NULL,
  `outbox_id` int(11) unsigned default '0',
  `inbox_id` int(11) unsigned default '0',
  `status` int(10) unsigned NOT NULL default '0',
  `err_num` int(11) unsigned NOT NULL default '0',
  `msg_id` int(11) unsigned default NULL,
  `delivery_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `pdu` blob,
  PRIMARY KEY  (`id`),
  KEY `idx_outsms_smsc_id_num` (`smsc_id`,`num`),
  KEY `idx_outsms_inbox_id` (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `outsmsa`
--

DROP TABLE IF EXISTS `outsmsa`;
CREATE TABLE `outsmsa` (
  `id` int(11) unsigned NOT NULL default '0',
  `abonent` bigint(20) unsigned NOT NULL default '0',
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(11) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `msg` varchar(255) default NULL,
  `data_coding` tinyint(3) unsigned NOT NULL default '0',
  `esm_class` tinyint(3) NOT NULL default '0',
  `transaction_id` bigint(20) unsigned default NULL,
  `ussd_service_op` tinyint(3) unsigned default NULL,
  `outbox_id` int(11) unsigned default '0',
  `inbox_id` int(11) unsigned default '0',
  `status` int(11) unsigned NOT NULL default '0',
  `err_num` int(11) unsigned NOT NULL default '0',
  `msg_id` int(11) unsigned default NULL,
  `delivery_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `pdu` blob,
  PRIMARY KEY  (`id`),
  KEY `idx_outsmsa_date` (`date`),
  KEY `idx_outsmsa_outbox_id` (`outbox_id`),
  KEY `idx_outsmsa_inbox_id` (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 2746368 kB';

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!50001 DROP VIEW IF EXISTS `partner`*/;
/*!50001 CREATE TABLE `partner` (
  `id` int(11) NOT NULL default '0',
  `partner_agent_id` int(11) NOT NULL default '0',
  `partner_manager_id` int(11) NOT NULL default '0',
  `legal_entity_id` int(11) NOT NULL default '0' COMMENT '����������� ����',
  `login` varchar(30) NOT NULL default '',
  `password` varchar(30) default NULL,
  `fio` varchar(255) NOT NULL default '' COMMENT '���',
  `phone` varchar(30) NOT NULL default '' COMMENT '�������',
  `email` varchar(255) NOT NULL default '' COMMENT 'EMAIL',
  `icq` varchar(30) default NULL COMMENT 'ICQ',
  `wap_url` varchar(255) default NULL,
  `web_url` varchar(255) default NULL,
  `contract_number` varchar(30) default NULL COMMENT '�����, ���� ��������',
  `payment_way` varchar(10) default NULL,
  `payment_comment` text,
  `payment_account` varchar(255) default NULL,
  `bank_name` varchar(255) default NULL,
  `bank_ras_account` varchar(30) default NULL,
  `bank_cor_account` varchar(30) default NULL,
  `bank_bik` varchar(30) default NULL,
  `comment` text,
  `name` varchar(255) NOT NULL default '',
  `reg_date` datetime default NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `activate_date` datetime default NULL,
  `_deleted` tinyint(1) unsigned NOT NULL default '0',
  `_deleted_date` datetime default NULL,
  `manager_id` int(10) unsigned default NULL,
  `money` double NOT NULL default '0',
  `money_fraud` double NOT NULL default '0',
  `currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `partner_agent`
--

DROP TABLE IF EXISTS `partner_agent`;
/*!50001 DROP VIEW IF EXISTS `partner_agent`*/;
/*!50001 CREATE TABLE `partner_agent` (
  `id` int(11) NOT NULL default '0',
  `legal_entity_id` int(11) NOT NULL default '0' COMMENT '����������� ����',
  `fio` varchar(255) NOT NULL default '' COMMENT '���',
  `phone` varchar(30) NOT NULL default '' COMMENT '�������',
  `email` varchar(255) NOT NULL default '' COMMENT 'EMAIL',
  `icq` varchar(30) default NULL COMMENT 'ICQ',
  `wap_url` varchar(255) default NULL,
  `web_url` varchar(255) default NULL,
  `login` varchar(30) NOT NULL default '',
  `password` varchar(30) default NULL,
  `contract_number` varchar(30) default NULL COMMENT '�����, ���� ��������',
  `payment_way` varchar(10) default NULL,
  `payment_comment` text,
  `payment_account` varchar(255) default NULL,
  `bank_name` varchar(255) default NULL,
  `bank_ras_account` varchar(30) default NULL,
  `bank_cor_account` varchar(30) default NULL,
  `bank_bik` varchar(30) default NULL,
  `comment` text,
  `name` varchar(255) NOT NULL default '',
  `reg_date` datetime default NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `activate_date` datetime default NULL,
  `manager_id` int(10) unsigned default NULL,
  `money` double NOT NULL default '0',
  `money_fraud` double NOT NULL default '0',
  `currency` char(3) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `partner_agent_t`
--

DROP TABLE IF EXISTS `partner_agent_t`;
CREATE TABLE `partner_agent_t` (
  `id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  CONSTRAINT `FK_partner_agent_t_1` FOREIGN KEY (`id`) REFERENCES `money_consumer_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `partner_t`
--

DROP TABLE IF EXISTS `partner_t`;
CREATE TABLE `partner_t` (
  `id` int(11) NOT NULL default '0',
  `partner_agent_id` int(11) NOT NULL default '0',
  `partner_manager_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `FK_partner_t_2` (`partner_agent_id`),
  KEY `FK_partner_t_3` (`partner_manager_id`),
  CONSTRAINT `FK_partner_t_3` FOREIGN KEY (`partner_manager_id`) REFERENCES `manager_t` (`id`),
  CONSTRAINT `FK_partner_t_1` FOREIGN KEY (`id`) REFERENCES `money_consumer_t` (`id`),
  CONSTRAINT `FK_partner_t_2` FOREIGN KEY (`partner_agent_id`) REFERENCES `partner_agent_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 92160 kB';

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `money_consumer_id` int(11) NOT NULL default '0',
  `k_price_abonent` double NOT NULL default '0',
  `k_price_rest` double NOT NULL default '0',
  `b_price_fixed` double NOT NULL default '0',
  `price_abonent` double default NULL,
  `price_rest` double default NULL,
  `chain_position` int(10) unsigned NOT NULL default '0',
  `chain_terminated` tinyint(3) unsigned NOT NULL default '0',
  `money_consumption_rule_id` int(10) unsigned NOT NULL default '0' COMMENT '������ ��� ������� � ������ ���������������� (�� ����� ������) �������',
  `inbox_id` int(10) unsigned NOT NULL default '0',
  `money` double NOT NULL default '0',
  `is_fraud` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `Index_2` (`money_consumer_id`),
  KEY `date` (`date`),
  KEY `inbox_id` (`inbox_id`),
  CONSTRAINT `FK_payment_1` FOREIGN KEY (`money_consumer_id`) REFERENCES `money_consumer_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `payment_ways`
--

DROP TABLE IF EXISTS `payment_ways`;
CREATE TABLE `payment_ways` (
  `id` varchar(10) NOT NULL default 'none',
  `name` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `plugin`
--

DROP TABLE IF EXISTS `plugin`;
/*!50001 DROP VIEW IF EXISTS `plugin`*/;
/*!50001 CREATE TABLE `plugin` (
  `id` int(10) unsigned NOT NULL default '0',
  `pattern` varchar(100) default NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `priority` int(11) unsigned NOT NULL default '0',
  `is_default_num` tinyint(3) unsigned NOT NULL default '0',
  `is_default_operator` tinyint(3) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL default '0',
  `num` varchar(20) NOT NULL default '',
  `msg_type` varchar(100) default NULL,
  `ignore_partner_from_service` tinyint(3) unsigned NOT NULL default '0',
  `partner_id` int(11) unsigned default NULL,
  `active` tinyint(3) unsigned NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `plugin_cyka_t`
--

DROP TABLE IF EXISTS `plugin_cyka_t`;
CREATE TABLE `plugin_cyka_t` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `service_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL default '1',
  `k_price_abonent_p` double NOT NULL default '0',
  `k_price_rest_p` double NOT NULL default '0',
  `b_price_fixed_p` double NOT NULL default '0',
  `k_price_abonent_pp` double NOT NULL default '0',
  `k_price_rest_pp` double NOT NULL default '0',
  `b_price_fixed_pp` double NOT NULL default '0',
  `k_price_abonent_ma1` double NOT NULL default '0',
  `k_price_rest_ma1` double NOT NULL default '0',
  `b_price_fixed_ma1` double NOT NULL default '0',
  `currency` char(3) NOT NULL default 'rur',
  PRIMARY KEY  (`id`),
  KEY `idx_plugin_cyka__service_id` (`service_id`),
  KEY `idx_plugin_cyka_partner_id` (`partner_id`),
  KEY `idx_plugin_cyka_operator_id` (`operator_id`),
  KEY `idx_plugin_cyka_num` (`num`),
  CONSTRAINT `fk_plugin_cyka_operator` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`),
  CONSTRAINT `fk_plugin_cyka_partner` FOREIGN KEY (`partner_id`) REFERENCES `partner_t` (`id`),
  CONSTRAINT `fk_plugin_cyka_service` FOREIGN KEY (`service_id`) REFERENCES `service_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

/*!50003 SET @OLD_SQL_MODE=@@SQL_MODE*/;
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE TRIGGER `trg_plugin_insert_after` AFTER INSERT ON `plugin_cyka_t` FOR EACH ROW CALL regen_consumption_rule(NEW.id) */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE TRIGGER `trg_plugin_update_after` AFTER UPDATE ON `plugin_cyka_t` FOR EACH ROW CALL regen_consumption_rule(NEW.id) */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

--
-- Table structure for table `plugin_t`
--

DROP TABLE IF EXISTS `plugin_t`;
CREATE TABLE `plugin_t` (
  `id` int(10) unsigned NOT NULL,
  `pattern` varchar(100) default NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `priority` int(11) unsigned NOT NULL default '0',
  `is_default_num` tinyint(3) unsigned NOT NULL default '0',
  `is_default_operator` tinyint(3) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `msg_type` varchar(100) default NULL,
  `ignore_partner_from_service` tinyint(3) unsigned NOT NULL default '0',
  `partner_id` int(11) unsigned default NULL,
  `active` tinyint(3) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `idx_plugin_service_id` (`service_id`),
  KEY `idx_plugin_partner_id` (`partner_id`),
  KEY `idx_plugin_operator_id` (`operator_id`),
  CONSTRAINT `fk_plugin_operator` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`),
  CONSTRAINT `fk_plugin_service` FOREIGN KEY (`service_id`) REFERENCES `service_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `process_state_log`
--

DROP TABLE IF EXISTS `process_state_log`;
CREATE TABLE `process_state_log` (
  `date` datetime NOT NULL,
  `pr_name` varchar(30) NOT NULL,
  `pr_state` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `pushboxa`
--

DROP TABLE IF EXISTS `pushboxa`;
CREATE TABLE `pushboxa` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `service_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `ip_addr` varchar(70) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!50001 DROP VIEW IF EXISTS `service`*/;
/*!50001 CREATE TABLE `service` (
  `id` int(11) unsigned NOT NULL default '0',
  `name` varchar(200) NOT NULL default '',
  `description` mediumtext NOT NULL,
  `sgroup_id` int(11) unsigned NOT NULL default '0',
  `uri` varchar(255) NOT NULL default '',
  `email` varchar(45) NOT NULL default '',
  `groupname` varchar(45) NOT NULL default '',
  `fullname` varchar(247) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `service_t`
--

DROP TABLE IF EXISTS `service_t`;
CREATE TABLE `service_t` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(200) NOT NULL default '',
  `description` mediumtext NOT NULL,
  `sgroup_id` int(11) unsigned NOT NULL default '0',
  `_deleted` tinyint(3) unsigned NOT NULL default '0',
  `_deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `uri` varchar(255) NOT NULL default '',
  `email` varchar(45) NOT NULL default 'i.alekseeva@alt1.ru',
  PRIMARY KEY  (`id`),
  KEY `idx_service_sgroup_id` (`sgroup_id`),
  CONSTRAINT `fk_service_sgroup` FOREIGN KEY (`sgroup_id`) REFERENCES `sgroup_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `sgroup`
--

DROP TABLE IF EXISTS `sgroup`;
/*!50001 DROP VIEW IF EXISTS `sgroup`*/;
/*!50001 CREATE TABLE `sgroup` (
  `id` int(11) unsigned NOT NULL default '0',
  `name` varchar(45) NOT NULL default '',
  `descr` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `sgroup_t`
--

DROP TABLE IF EXISTS `sgroup_t`;
CREATE TABLE `sgroup_t` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL default '',
  `descr` mediumtext NOT NULL,
  `_deleted` tinyint(3) unsigned NOT NULL default '0',
  `_deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `smsc`
--

DROP TABLE IF EXISTS `smsc`;
/*!50001 DROP VIEW IF EXISTS `smsc`*/;
/*!50001 CREATE TABLE `smsc` (
  `id` int(11) unsigned NOT NULL default '0',
  `is_active` tinyint(3) unsigned NOT NULL default '0',
  `default_operator_id` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `host` varchar(100) NOT NULL default '',
  `port` int(11) unsigned NOT NULL default '0',
  `system_id` varchar(20) NOT NULL default '',
  `password` varchar(20) NOT NULL default '',
  `system_type` varchar(20) default NULL,
  `smpp_version` tinyint(3) unsigned NOT NULL default '0',
  `interface_version` tinyint(3) unsigned NOT NULL default '0',
  `addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `src_addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `src_addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `dest_addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `dest_addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `addr_range` varchar(20) default NULL,
  `smpp_mode` tinyint(3) unsigned NOT NULL default '0',
  `sync` tinyint(3) unsigned NOT NULL default '0',
  `debug_level` varchar(10) default NULL,
  `rebind_timeout` int(11) NOT NULL default '0',
  `elink_timeout` int(11) NOT NULL default '0',
  `registered_delivery` tinyint(3) unsigned NOT NULL default '0',
  `use7bit` tinyint(3) unsigned NOT NULL default '0',
  `gmt_offset` tinyint(3) NOT NULL default '0',
  `description` varchar(200) default NULL,
  `_deleted` tinyint(3) unsigned NOT NULL default '0',
  `_deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Table structure for table `smsc_expected_traffic`
--

DROP TABLE IF EXISTS `smsc_expected_traffic`;
CREATE TABLE `smsc_expected_traffic` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `smsc_id` int(11) unsigned NOT NULL default '0',
  `hour` int(11) unsigned NOT NULL default '0',
  `cnt` int(11) unsigned default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_smsc_expected_traffic_smsc_id` (`smsc_id`,`hour`),
  CONSTRAINT `fk_smsc_expected_traffic_smsc_id` FOREIGN KEY (`smsc_id`) REFERENCES `smsc_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `smsc_t`
--

DROP TABLE IF EXISTS `smsc_t`;
CREATE TABLE `smsc_t` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `is_active` tinyint(3) unsigned NOT NULL default '0',
  `default_operator_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL default '''',
  `host` varchar(100) NOT NULL default '',
  `port` int(11) unsigned NOT NULL default '0',
  `system_id` varchar(20) NOT NULL default '',
  `password` varchar(20) NOT NULL default '',
  `system_type` varchar(20) default NULL,
  `smpp_version` tinyint(3) unsigned NOT NULL default '52',
  `interface_version` tinyint(3) unsigned NOT NULL default '52',
  `addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `src_addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `src_addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `dest_addr_ton` tinyint(3) unsigned NOT NULL default '0',
  `dest_addr_npi` tinyint(3) unsigned NOT NULL default '0',
  `addr_range` varchar(20) default NULL,
  `smpp_mode` tinyint(3) unsigned NOT NULL default '0',
  `sync` tinyint(3) unsigned NOT NULL default '0',
  `debug_level` varchar(10) default 'INFO',
  `rebind_timeout` int(11) NOT NULL default '10',
  `elink_timeout` int(11) NOT NULL default '60',
  `registered_delivery` tinyint(3) unsigned NOT NULL default '0',
  `use7bit` tinyint(3) unsigned NOT NULL default '0',
  `gmt_offset` tinyint(3) NOT NULL default '3',
  `description` varchar(200) default NULL,
  `_deleted` tinyint(3) unsigned NOT NULL default '0',
  `_deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `idx_smsc_t_operator_id` (`default_operator_id`),
  CONSTRAINT `smsc_t_ibfk_1` FOREIGN KEY (`default_operator_id`) REFERENCES `operator_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='InnoDB free: 37888 kB; (`default_operator_id`) REFER `alt4/o';

--
-- Table structure for table `sresponse`
--

DROP TABLE IF EXISTS `sresponse`;
CREATE TABLE `sresponse` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `response` mediumtext NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `service_id` int(11) unsigned NOT NULL default '0',
  `request_url` mediumtext,
  `type` varchar(20) NOT NULL default '',
  `orig_id` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_sresponce_orig_id` (`orig_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Table structure for table `view_payment`
--

DROP TABLE IF EXISTS `view_payment`;
/*!50001 DROP VIEW IF EXISTS `view_payment`*/;
/*!50001 CREATE TABLE `view_payment` (
  `id` int(10) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `money_consumer_id` int(11) NOT NULL default '0',
  `money_consumer_name` varchar(255) NOT NULL default '',
  `money_consumer_fio` varchar(255) NOT NULL default '' COMMENT '���',
  `money_consumer_active` tinyint(1) unsigned NOT NULL default '0',
  `k_price_abonent` double NOT NULL default '0',
  `k_price_rest` double NOT NULL default '0',
  `b_price_fixed` double NOT NULL default '0',
  `price_abonent` double default NULL,
  `price_rest` double default NULL,
  `chain_position` int(10) unsigned NOT NULL default '0',
  `chain_terminated` tinyint(3) unsigned NOT NULL default '0',
  `money_consumption_rule_id` int(10) unsigned NOT NULL default '0' COMMENT '������ ��� ������� � ������ ���������������� (�� ����� ������) �������',
  `inbox_id` int(10) unsigned NOT NULL default '0',
  `money` double NOT NULL default '0',
  `is_fraud` tinyint(1) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251*/;

--
-- Dumping routines for database 'alt4'
--
DELIMITER ;;
/*!50003 DROP FUNCTION IF EXISTS `determine_operator` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE FUNCTION `determine_operator`(_abonent bigINT, _smsc_id INTEGER) RETURNS int(11)
BEGIN
  DECLARE _operator_id integer;

  select operator_id into _operator_id from operator_number_pool
    where locate(number_prefix, _abonent) = 1 order by length(number_prefix) desc limit 1;

  if _operator_id is null then
    select default_operator_id into _operator_id from smsc
      where id=_smsc_id;
  end if;

  return _operator_id;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP FUNCTION IF EXISTS `from_rur` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE FUNCTION `from_rur`(_curr CHAR(3), summ DOUBLE) RETURNS double
BEGIN
 DECLARE _rate DOUBLE;
 IF _curr='rur' then
    return summ;
 else
    SELECT price INTO _rate FROM exchange_rate WHERE currency = _curr and date = date(now());
    IF _rate IS NULL THEN
        call warn(concat("Exchange rate for currency '",_curr,"' is not given at day '", date(now()),"'"));
        return 0;
    ELSE
        return summ/_rate;
    END IF;
 end if;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP FUNCTION IF EXISTS `get_rur` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE FUNCTION `get_rur`(_curr CHAR(3), summ DOUBLE) RETURNS double
BEGIN
  DECLARE _rate DOUBLE;
  IF _curr='rur' then
    return summ;
 else
    SELECT price INTO _rate FROM exchange_rate WHERE currency = _curr and date = date(now());
    IF _rate IS NULL THEN
        call warn(concat("Exchange rate for currency '",_curr,"' is not given at day '", date(now()),"'"));
        return 0;
    ELSE
        return summ * _rate;
    END IF;
 end if;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `archive_billing` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `archive_billing`()
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE reference_date DATE;
	DECLARE date_var DATE;
	DECLARE cur1 CURSOR FOR SELECT distinct date(date) from billing where date(date) <= reference_date;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	START TRANSACTION;
	select date(date_sub(now(), INTERVAL 1 DAY)) INTO reference_date;
	insert into billinga select * from billing where date(date) <= reference_date;

	OPEN cur1;
	REPEAT
		FETCH cur1 INTO date_var;
		IF NOT done THEN
			CALL billing_calc__plug_service(date_var);
		END IF;
	UNTIL done END REPEAT;
	delete from billing where date(date) <= reference_date;
	COMMIT;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `billing_calc__plug_service` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `billing_calc__plug_service`(on_date DATE)
BEGIN

DELETE from billing_plug_service where date = on_date;
insert into billing_plug_service select on_date, service_id, abonent, num, sum(insms_count), sum(outsms_count) from billinga where date(date) = on_date group by abonent, num, service_id;

END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `billing_get_price` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `billing_get_price`(_operator_id INTEGER UNSIGNED,
									_num VARCHAR(20),
									_date date,
									OUT _price_in DOUBLE,
									OUT _price_out DOUBLE,
                  OUT _currency CHAR(3),
                  OUT _nds BOOL)
BEGIN
	-- LOG --
--	INSERT INTO billing_log
--      SET billing_log.description=
--            CONCAT("CALL billing_get_price with OPERATOR_ID = ",
--                   _operator_id,
--                   ", NUM = ",
--                   _num,
--                   ", DATE = ",
--                   _date
--                  );
	-- BEGIN TRANSACTION --
	START TRANSACTION;

	SELECT num_price.price_in,
         num_price.price_out,
         num_price.currency,
         num_price.nds
	INTO _price_in, _price_out, _currency, _nds
	FROM num_price
	WHERE num_price.date_start<=_date AND num_price.operator_id=_operator_id AND num_price.num=_num
	ORDER BY num_price.date_start DESC LIMIT 1;
	
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
--	INSERT INTO billing_log
--      SET billing_log.description=
--            CONCAT("COMPLETE CALL billing_get_price with OPERATOR_ID = ",
--                   _operator_id,
--                   ", NUM = ",
--                   _num,
--                   ", DATE = ",
--                   _date,
--				   ", PRICE_IN = ",
--				   _price_in,
--				   ", PRICE_OUT = ",
--				   _price_out,
--				   ", CYRRENCY = ",
--				   _currency,
--				   ", NDS = ",
--				   _nds
--                  );
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `count_billing_price` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `count_billing_price`()
BEGIN
	-- DECLARE SECTION --
	DECLARE _month INTEGER UNSIGNED;
	DECLARE _year INTEGER UNSIGNED;
	DECLARE _billing_rule VARCHAR(45);

	DECLARE _operator_id INTEGER UNSIGNED;
	DECLARE _cur_done INTEGER;
	DECLARE _cur CURSOR FOR SELECT DISTINCT num_price.operator_id
							FROM num_price;
	
	-- SET DEFAULT VALUES --
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _cur_done = 1;
	SET _month = month(now());
	SET _year = year(now());

	-- LOG --
	INSERT INTO billing_log SET billing_log.description="CALL count_billing_price FOR ALL OPERATORS.";

	-- BEGIN TRANSACTION --
	START TRANSACTION;

	-- INSERT NEW RECORDS FOR ALL `NUM`--
	OPEN _cur;
	cur_loop: LOOP
		SET _cur_done = 0;
		FETCH _cur INTO _operator_id;
    	IF (_cur_done) THEN LEAVE cur_loop;
		END IF;
   		
		-- RETRIEVING NAME FOR FUNCTION --
		SET _billing_rule = NULL;

		SELECT operator_t.billing_rule 
			INTO _billing_rule 
			FROM operator_t 
			WHERE operator_t.id=_operator_id;
		
		-- CALL billing_rule --
		IF (_billing_rule = "count_billing_price__default")
		THEN CALL count_billing_price__default(_operator_id, _month, _year);
		ELSE
			IF (_billing_rule = "count_billing_price__mts")
			THEN CALL count_billing_price__mts(_operator_id, _month, _year);
			ELSE
				IF (_billing_rule = "count_billing_price__beeline")
				THEN CALL count_billing_price__beeline(_operator_id, _month, _year);
				ELSE
					IF (_billing_rule = "count_billing_price__megafon")
					THEN CALL count_billing_price__megafon(_operator_id, _month, _year);
					ELSE
						-- LOG --
						INSERT INTO billing_log SET billing_log.description=
							CONCAT("ERROR inside count_billing_price: unknown billing_rule '",
								   _billing_rule,
								   "' FOR OPERATOR '",
								   _operator_id,
								   "'.");
					END IF;
				END IF;
			END IF;
		END IF;
	END LOOP;
	CLOSE _cur;
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
	INSERT INTO billing_log SET billing_log.description="COMPLETE CALL count_billing_price FOR ALL OPERATORS.";

END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `count_billing_price__beeline` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `count_billing_price__beeline`(_operator_id INTEGER UNSIGNED,
												_month INTEGER UNSIGNED,
												_year INTEGER UNSIGNED)
BEGIN
	-- DECLARE SECTION --
	DECLARE _date DATE;
	DECLARE _num VARCHAR(20);
	DECLARE _price_in DOUBLE;
	DECLARE _price_out DOUBLE;
	DECLARE _currency CHAR(3);
	DECLARE _nds BOOL;
	DECLARE _no_exit BOOL;
	DECLARE _koef DOUBLE;
	DECLARE _count_sms_out INTEGER;

	DECLARE _cur_done INTEGER;
	DECLARE _cur CURSOR FOR SELECT DISTINCT num_price.num
							FROM num_price
							WHERE num_price.operator_id=_operator_id;

	-- SET DEFAULT VALUES --
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _cur_done = 1;
	SET _count_sms_out = 0;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("CALL count_billing_price__beeline with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );

	-- BEGIN TRANSACTION --
	START TRANSACTION;
	
	-- COUNT TRAFFIC --
	SELECT sum(billing_plug_service.outsms_count)
	INTO _count_sms_out
	FROM billing_plug_service
  WHERE billing_plug_service.operator_id=_operator_id AND
        month(billing_plug_service.date)=_month AND
        year(billing_plug_service.date)=_year;
	
	-- SET KOEFFICIENT --
	IF (_count_sms_out<=25000)
	THEN SET _koef=0.04;
	ELSE
		IF (_count_sms_out<=75000)
		THEN SET _koef=0.03;
		ELSE
			IF (_count_sms_out<=200000)
			THEN SET _koef=0.02;
			ELSE SET _koef=0.01;
			END IF;
		END IF;
	END IF;

	-- CLEAN PREVIOUS RECORDS --
	DELETE FROM billing_price
	WHERE billing_price.operator_id=_operator_id AND month(billing_price.date)=_month AND year(billing_price.date)=_year;

	-- INSERT NEW RECORDS FOR ALL `NUM`--
	OPEN _cur;
	cur_loop: LOOP
		SET _cur_done = 0;
		FETCH _cur INTO _num;
	    IF (_cur_done) THEN LEAVE cur_loop;
			END IF;
	    
		SET _no_exit = TRUE;
		SET _price_in = 0;
		SET _price_out = 0;
		SET _currency = 'rur';
		SELECT STR_TO_DATE(CONCAT(_year,"-",_month,"-",1),"%Y-%m-%d") INTO _date;

		-- INSERT NEW RECORDS --
		while_loop: WHILE (_no_exit) DO
			-- GET PRICES AND CYRRENCY --
		    CALL billing_get_price(_operator_id, _num, _date, _price_in, _price_out, _currency, _nds);
 	
			-- COUNT NEW PRICES --
	    IF (_nds)
			THEN
				IF (_num = "1121")
				THEN
					SET _price_in = _price_in * 0.55;
		  	ELSE
					SET _price_in = _price_in * 0.7;
		  	END IF;
				SET _price_out = _price_out * _koef;
			ELSE
				IF (_num = "1121")
				THEN
					SET _price_in = _price_in * 0.55;
		  	ELSE
					SET _price_in = _price_in * 0.7;
		  	END IF;
				SET _price_out = _price_out * _koef;
			END IF;

      IF (_price_in IS NULL) THEN SET _price_in=0; END IF;
      IF (_price_out IS NULL) THEN SET _price_out=0; END IF;

      -- INSERT NEW RECORD --
	 		INSERT INTO billing_price
    			SET billing_price.date=_date,
	  				billing_price.operator_id=_operator_id,
	  	  			billing_price.num=_num,
		  		  	billing_price.price_in=_price_in,
					billing_price.price_out=_price_out,
  				 	billing_price.currency=_currency;
			SELECT date_add(_date, INTERVAL 1 DAY) INTO _date;
    		IF (month(_date)>_month)
  			THEN
		  		SET _no_exit=FALSE;
		  		LEAVE while_loop;
  			END IF;
    	END WHILE;
	END LOOP;
	CLOSE _cur;
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("COMPLETE CALL count_billing_price__beeline with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `count_billing_price__default` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `count_billing_price__default`(_operator_id INTEGER UNSIGNED,
												_month INTEGER UNSIGNED,
												_year INTEGER UNSIGNED)
BEGIN
	-- DECLARE SECTION --
	DECLARE _date DATE;
	DECLARE _num VARCHAR(20);
	DECLARE _koef DOUBLE;
	DECLARE _price_in DOUBLE;
	DECLARE _price_out DOUBLE;
	DECLARE _currency CHAR(3);
	DECLARE _nds BOOL;
	DECLARE _no_exit BOOL;
	
	DECLARE _cur_done INTEGER;
	DECLARE _cur CURSOR FOR SELECT DISTINCT num_price.num
							FROM num_price
							WHERE num_price.operator_id=_operator_id;
	
	-- SET DEFAULT VALUES --
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _cur_done = 1;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("CALL count_billing_price__default with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );

	-- TABLE operator_t MUST HAS RECORD, OTHERWISE _koef = 0.3 --
	SET _koef = 0.3;
	SELECT operator_t.koef INTO _koef FROM operator_t WHERE operator_t.id=_operator_id;

	-- BEGIN TRANSACTION --
	START TRANSACTION;

	-- CLEAN PREVIOUS RECORDS --
	DELETE FROM billing_price
	WHERE billing_price.operator_id=_operator_id AND month(billing_price.date)=_month AND year(billing_price.date)=_year;

	-- INSERT NEW RECORDS FOR ALL `NUM`--
	OPEN _cur;
	cur_loop: LOOP
		SET _cur_done = 0;
		FETCH _cur INTO _num;
    	IF (_cur_done) THEN LEAVE cur_loop;
		END IF;
   	
     	SET _no_exit = TRUE;
		SET _price_in = 0;
		SET _price_out = 0;
		SET _currency = 'rur';
		SELECT STR_TO_DATE(CONCAT(_year,"-",_month,"-",1),"%Y-%m-%d") INTO _date;
		
		-- INSERT NEW RECORDS --
		while_loop: WHILE (_no_exit) DO
			-- GET PRICES AND CYRRENCY --
			CALL billing_get_price(_operator_id, _num, _date, _price_in, _price_out, _currency, _nds);
			-- COUNT NEW PRICES --
			IF (_nds)
			THEN
				SET _price_in = _price_in * _koef;
				-- SET _price_out = _price_out * _koef;
		  	ELSE
				SET _price_in = _price_in * _koef;
				-- SET _price_out = _price_out * _koef;
			END IF;
	
      IF (_price_in IS NULL) THEN SET _price_in=0; END IF;
      IF (_price_out IS NULL) THEN SET _price_out=0; END IF;

        -- INSERT NEW RECORD --
 		  	INSERT INTO billing_price
				SET billing_price.date=_date,
	  				billing_price.operator_id=_operator_id,
		  	  		billing_price.num=_num,
			  	  	billing_price.price_in=_price_in,
					billing_price.price_out=_price_out,
  		 			billing_price.currency=_currency;
	
		    SELECT date_add(_date, INTERVAL 1 DAY) INTO _date;

			IF (month(_date)>_month)
  			THEN
	  			SET _no_exit=FALSE;
			  	LEAVE while_loop;
  			END IF;
	    END WHILE;
	END LOOP;
	CLOSE _cur;
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("COMPLETE CALL count_billing_price__default with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `count_billing_price__megafon` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `count_billing_price__megafon`(_operator_id INTEGER UNSIGNED,
												_month INTEGER UNSIGNED,
												_year INTEGER UNSIGNED)
BEGIN
	-- DECLARE SECTION --
	DECLARE _date DATE;
	DECLARE _num VARCHAR(20);
	DECLARE _currency CHAR(3);
	DECLARE _nds BOOL;
	DECLARE _no_exit BOOL;

	DECLARE _price_in DOUBLE;
	DECLARE _price_out DOUBLE;
	DECLARE _insms_count INTEGER;
	DECLARE _outsms_count INTEGER;
	DECLARE _p DOUBLE; -- ����������� ������� ���������� ���������� ������� ��� �������� � ��������� ������� �� ������� �� ��������� �������
	DECLARE _ef DOUBLE;
	DECLARE _g DOUBLE; -- ������� ����������� �������������� ����������	

	DECLARE _cur_done INTEGER;
	DECLARE _cur CURSOR FOR SELECT DISTINCT num_price.num
							FROM num_price
							WHERE num_price.operator_id=_operator_id;

	-- SET DEFAULT VALUES --
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _cur_done = 1;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("CALL count_billing_price__megafon with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );

	-- BEGIN TRANSACTION --
	START TRANSACTION;
	
	-- CLEAN PREVIOUS RECORDS --
	DELETE FROM billing_price
	WHERE billing_price.operator_id=_operator_id AND month(billing_price.date)=_month AND year(billing_price.date)=_year;

	-- INSERT NEW RECORDS FOR ALL SHORT NUMBERS --
	OPEN _cur;
	cur_loop: LOOP
		SET _cur_done = 0;
		FETCH _cur INTO _num;
	    
		IF (_cur_done) 
		THEN LEAVE cur_loop;
		END IF;
	    
		SET _no_exit = TRUE;
		SET _price_in = 0;
		SET _price_out = 0;
		SET _insms_count = 0;
		SET _outsms_count = 0;
		SET _currency = 'rur';
		SELECT STR_TO_DATE(CONCAT(_year,"-",_month,"-",1),"%Y-%m-%d") INTO _date;

		-- COUNT TRAFFIC FOR SHORT NUMBER --
		SELECT sum(billing_plug_service.insms_count), sum(billing_plug_service.outsms_count) 
		INTO _insms_count, _outsms_count 
		FROM billing_plug_service 
	  	WHERE billing_plug_service.operator_id=_operator_id AND 
			  billing_plug_service.num=_num AND 
    		  month(billing_plug_service.date)=_month AND 
			  year(billing_plug_service.date)=_year;

		-- INSERT NEW RECORDS --
		while_loop: WHILE (_no_exit) DO
			SET _p = 0;
			SET _g = 0;
			SET _ef = 0;
			-- GET PRICES AND CYRRENCY --
			CALL billing_get_price(_operator_id, _num, _date, _price_in, _price_out, _currency, _nds);
 	
			-- ����������� ������� ���������� ���������� ������� ��� �������� � ��������� ������� �� ������� �� ��������� ������� --
			SET _ef = (_price_in * _insms_count) - (_price_out * (_insms_count + _outsms_count));
			SET _p = _ef / _insms_count;

			-- ������� ����������� �������������� ���������� --
			IF (_p<=0.05)
			THEN SET _g=0.95;
			ELSE
				IF (_p<=0.1)
				THEN SET _g=0.85;
				ELSE
					IF (_p<=0.5)
					THEN SET _g=0.75;
					ELSE
						IF (_p<=1.0)
						THEN SET _g=0.65;
						ELSE
							IF (_p<=1.5)
							THEN SET _g=0.60;
							ELSE
								IF (_p<=2.5)
								THEN SET _g=0.55;
								ELSE set _g=0.5;
								END IF;
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;

			-- COUNT _koef
			IF (_nds)
			THEN
				SET _price_in = ( _price_in - _price_out ) * _g;
				SET _price_out = _price_out * _g;
			ELSE -- IF (_nds)
				SET _price_in = ( _price_in - _price_out ) * _g;
				SET _price_out = _price_out * _g;
			END IF; -- IF (_nds)

      IF (_price_in IS NULL) THEN SET _price_in=0; END IF;
      IF (_price_out IS NULL) THEN SET _price_out=0; END IF;

      -- INSERT NEW RECORD --
	 		INSERT INTO billing_price
			SET billing_price.date=_date,
	  			billing_price.operator_id=_operator_id,
	  	  		billing_price.num=_num,
		  	  	billing_price.price_in=_price_in,
				billing_price.price_out=_price_out,
  			 	billing_price.currency=_currency;
			
			SELECT date_add(_date, INTERVAL 1 DAY) INTO _date;
			IF (month(_date)>_month)
  			THEN
		  		SET _no_exit=FALSE;
		  		LEAVE while_loop;
  			END IF;
    	END WHILE;
	END LOOP;
	CLOSE _cur;
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("COMPLETE CALL count_billing_price__megafon with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `count_billing_price__mts` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `count_billing_price__mts`(_operator_id INTEGER UNSIGNED,
											_month INTEGER UNSIGNED,
											_year INTEGER UNSIGNED)
BEGIN
	-- DECLARE SECTION --
	DECLARE _date DATE;
	DECLARE _num VARCHAR(20);
	DECLARE _price_in DOUBLE;
	DECLARE _price_out DOUBLE;
	DECLARE _currency CHAR(3);
	DECLARE _nds BOOL;
	DECLARE _no_exit BOOL;
	DECLARE _koef DOUBLE;
	DECLARE _insms_count INTEGER;
	DECLARE _insms_full_price DOUBLE;

	DECLARE _cur_done INTEGER;
	DECLARE _cur CURSOR FOR SELECT DISTINCT num_price.num
							FROM num_price
							WHERE num_price.operator_id=_operator_id;

	-- SET DEFAULT VALUES --
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _cur_done = 1;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("CALL count_billing_price__mts with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );

	-- BEGIN TRANSACTION --
	START TRANSACTION;
	
	-- CLEAN PREVIOUS RECORDS --
	DELETE FROM billing_price
	WHERE billing_price.operator_id=_operator_id AND month(billing_price.date)=_month AND year(billing_price.date)=_year;

	-- INSERT NEW RECORDS FOR ALL `NUM`--
	OPEN _cur;
	cur_loop: LOOP
		SET _cur_done = 0;
		FETCH _cur INTO _num;
	    IF (_cur_done) THEN LEAVE cur_loop;
			END IF;
	    
		SET _no_exit = TRUE;
		SET _price_in = 0;
		SET _price_out = 0;
		SET _currency = 'rur';
		SELECT STR_TO_DATE(CONCAT(_year,"-",_month,"-",1),"%Y-%m-%d") INTO _date;

		-- INSERT NEW RECORDS --
		while_loop: WHILE (_no_exit) DO
			-- GET PRICES AND CYRRENCY --
		    CALL billing_get_price(_operator_id, _num, _date, _price_in, _price_out, _currency, _nds);

			-- COUNT TRAFFIC --
			SELECT sum(billing_plug_service.insms_count) 
			INTO _insms_count 
			FROM billing_plug_service 
			WHERE billing_plug_service.operator_id=_operator_id AND 
				  billing_plug_service.num=_num AND 
				  month(billing_plug_service.date)=_month AND 
				  year(billing_plug_service.date)=_year;

			-- COUNT TRAFFIC * PRICE_IN --
			SET _insms_full_price = _insms_count * _price_in;
			
			-- COUNT koefficient --
			IF (_insms_full_price<50000)
			THEN SET _koef = 0.55;
				IF (_insms_full_price<100000)
				THEN SET _koef = 0.58;
				ELSE
					IF (_insms_full_price<250000)
					THEN SET _koef = 0.61;
					ELSE
						IF (_insms_full_price<500000)
						THEN SET _koef = 0.63;
						ELSE
							IF (_insms_full_price<750000)
							THEN SET _koef = 0.65;
							ELSE
								IF (_insms_full_price<1000000)
								THEN SET _koef = 0.67;
								ELSE
									IF (_insms_full_price<1500000)
									THEN SET _koef = 0.69;
									ELSE
										IF (_insms_full_price<2000000)
										THEN SET _koef = 0.7;
										ELSE
											IF (_insms_full_price<2500000)
											THEN SET _koef = 0.71;
											ELSE SET _koef = 0.72;
											END IF;
										END IF;
									END IF;
								END IF;
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
 			
			-- COUNT NEW PRICES --
	    	IF (_nds)
			THEN
				SET _price_in = _price_in * _koef;
				-- SET _price_out = _price_out * _koef;
			ELSE
				SET _price_in = _price_in * _koef;
				-- SET _price_out = _price_out * _koef;
			END IF;

      IF (_price_in IS NULL) THEN SET _price_in=0; END IF;
      IF (_price_out IS NULL) THEN SET _price_out=0; END IF;

      -- INSERT NEW RECORD --
	 		INSERT INTO billing_price
    			SET billing_price.date=_date,
	  				billing_price.operator_id=_operator_id,
	  	  			billing_price.num=_num,
		  		  	billing_price.price_in=_price_in,
					billing_price.price_out=_price_out,
  				 	billing_price.currency=_currency;
			SELECT date_add(_date, INTERVAL 1 DAY) INTO _date;
    		IF (month(_date)>_month)
  			THEN
		  		SET _no_exit=FALSE;
		  		LEAVE while_loop;
  			END IF;
    	END WHILE;
	END LOOP;
	CLOSE _cur;
	-- END TRANSACTION --
	COMMIT;

	-- LOG --
	INSERT INTO billing_log
      SET billing_log.description=
            CONCAT("COMPLETE CALL count_billing_price__mts with OPERATOR_ID = ",
                   _operator_id,
                   ", MONTH = ",
                   _month,
                   ", YEAR = ",
                   _year
                  );
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `get_prices` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `get_prices`(_op_id INTEGER, _num VARCHAR(20), OUT _price_abon REAL, OUT _price_rest REAL)
BEGIN
		SELECT get_rur(currency, price_in), get_rur(currency, estimated_income)
			   INTO _price_abon, _price_rest
			   FROM num_price
			   WHERE operator_id = _op_id and num = _num and date_start <= date(now())
			   ORDER BY date_start 
			   LIMIT 1;
		IF _price_abon IS NULL THEN
		   call warn(concat("No abonent price for opeartor '",_op_id,"' and num '",_num,"', defaulting to '0'"));
		   SET _price_abon = 0;
		END IF;
		IF _price_rest IS NULL THEN
		   call warn(concat("No estimated for opeartor '",_op_id,"' and num '",_num,"', defaulting to '0'"));
		   SET _price_rest = 0;
		END IF;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `process_cyka_batch` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `process_cyka_batch`()
test: BEGIN
    DECLARE _max_inbox_processed INTEGER;
    DECLARE _smsc_id, _inbox_id, _insms_count, _operator_id, _partner_id, _legal_owner_id, _service_id INTEGER;
    DECLARE _num, _abonent, _price_category VARCHAR(20);
    DECLARE _date DATETIME;
    DECLARE _price_abonent, _price_rest_initial, _price_rest_current REAL;
    DECLARE _k_price_abonent, _k_price_rest, _b_price_fixed REAL;
    DECLARE _money_consumption_rule_id, _money_consumer_id INTEGER;
    DECLARE _current_deduction REAL;
    DECLARE _position INTEGER;
    DECLARE _priority INTEGER;
    DECLARE cursor_done INTEGER;
	DECLARE _currency VARCHAR(3);
	DECLARE _is_fraud TINYINT;
    DECLARE batch_cursor CURSOR FOR
            SELECT date, smsc_id, inbox_id, insms_count, operator_id, num, partner_id, legal_owner_id, service_id, abonent, is_fraud, category
            FROM current_batch order by inbox_id;
    DECLARE chain_cursor CURSOR FOR
            SELECT money_consumer_id, money_consumption_rule_id, k_price_abonent, 
                   k_price_rest, get_rur(currency, b_price_fixed), priority, money_consumer_currency FROM current_chain ORDER BY priority DESC;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done = 1;
    SELECT max(inbox_id) INTO _max_inbox_processed from payment;
    IF _max_inbox_processed IS NULL THEN
       SET _max_inbox_processed = 0;
    END IF;
    
    DROP TABLE IF EXISTS current_batch;
    CREATE TABLE current_batch select date, smsc_id, inbox_id, insms_count, b.operator_id, b.num, partner_id, legal_owner_id, service_id, b.abonent, fa.is_fraud as is_fraud, b.category as category
		   from billing b left join fraud_abonent fa on b.abonent = fa.abonent
           where inbox_id > _max_inbox_processed and dir = 1 ORDER by inbox_id LIMIT 10;
    OPEN batch_cursor;
    batch_loop: LOOP
            SET cursor_done = 0;
            FETCH batch_cursor INTO _date, _smsc_id, _inbox_id, _insms_count, _operator_id, _num, _partner_id, _legal_owner_id, _service_id, _abonent, _is_fraud, _price_category;
            IF cursor_done THEN LEAVE batch_loop; END IF;
            CALL get_prices(_operator_id, _num, _price_abonent, _price_rest_initial);
			IF _price_abonent = 0 and _price_rest_initial = 0 THEN
			   call warn(concat("Prices is zero for operator '", _operator_id, "' and num '", _num, "' - not building consumer chain"));
			   ITERATE batch_loop;
			END IF;
            SET _price_rest_current = _price_rest_initial;
            DROP TABLE IF EXISTS current_chain;
            CREATE TABLE current_chain AS
                   select c.id as money_consumer_id, cr.id as money_consumption_rule_id, cr.k_price_abonent, 
                   cr.k_price_rest, cr.b_price_fixed, cr.priority, cr.currency as currency, c.currency as money_consumer_currency
                             FROM money_consumer c
                             join money_consumption_rule cr on c.id = cr.money_consumer_id
                             left join consumption_filter_operator fo on if(cr.filter_on_operator, fo.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_num  fn on if(cr.filter_on_num, fn.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_partner  fp on if(cr.filter_on_partner, fp.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_legal_owner fl on if(cr.filter_on_legal_owner, fl.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_service fs on if(cr.filter_on_service, fs.money_consumption_rule_id = cr.id, null)
                   where c.active = 1 and
                         if(cr.filter_on_operator, fo.operator_id = _operator_id, 1) and
                         if(cr.filter_on_partner, fp.partner_id = _partner_id, 1) and
                         if(cr.filter_on_partner, fn.num = _num, 1) and
                         if(cr.filter_on_legal_owner, fl.legal_owner_id = _legal_owner_id and fl.category = _price_category, 1) and
                         if(cr.filter_on_service, fs.service_id = _service_id, 1);
            SET _position = 1;
            OPEN chain_cursor;
            chain_loop: LOOP
                        SET cursor_done = 0;
                        FETCH chain_cursor INTO  _money_consumer_id, _money_consumption_rule_id,
                              _k_price_abonent, _k_price_rest, _b_price_fixed, _priority, _currency;
                        IF cursor_done THEN LEAVE chain_loop; END IF;
                        SET _current_deduction = _k_price_abonent * _price_abonent + _k_price_rest * _price_rest_current + _b_price_fixed;
                        IF ( (_price_rest_current - _current_deduction) / _price_rest_initial) < 0.05 THEN
                           LEAVE chain_loop;
                        END IF;
                        INSERT INTO payment (date, money_consumer_id, k_price_abonent, k_price_rest, b_price_fixed, chain_position, chain_terminated, money_consumption_rule_id, inbox_id, money, price_abonent, price_rest, is_fraud)
                               VALUES (_date, _money_consumer_id, _k_price_abonent, _k_price_rest, _b_price_fixed, _position, 0, _money_consumption_rule_id, _inbox_id, _current_deduction, _price_abonent, _price_rest_current, _is_fraud);
						IF _is_fraud = 1 THEN
					       UPDATE money_consumer_t SET money_fraud = money_fraud + from_rur(_currency, _current_deduction) WHERE id = _money_consumer_id;
						ELSE
					       UPDATE money_consumer_t SET money = money + from_rur(_currency, _current_deduction) WHERE id = _money_consumer_id;
						END IF;								
                        SET _price_rest_current = _price_rest_current - _current_deduction;
                        SET _position = _position + 1;
            END LOOP;
            CLOSE chain_cursor;
    
    END LOOP;
    
    DROP TABLE current_batch;
    
    
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `regen_consumption_rule` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `regen_consumption_rule`(_id INTEGER)
BEGIN
        DECLARE _rule_id, _partner_id, _operator_id, _service_id INTEGER;
        DECLARE _num VARCHAR(20);
        DECLARE _active TINYINT;
        DECLARE _k_rest_p, _k_abon_p, _b_fix_p REAL;
        DECLARE _k_rest_pp, _k_abon_pp, _b_fix_pp REAL;
        DECLARE _k_rest_ma1, _k_abon_ma1, _b_fix_ma1 REAL;
		DECLARE _manager_id, _agent_id INTEGER;
		DECLARE _currency VARCHAR(3);
        
        DELETE FROM money_consumption_rule WHERE plugin_id = _id;
        
        SELECT partner_id, operator_id, num, service_id, active, currency,
			   			   k_price_abonent_p, k_price_rest_p, b_price_fixed_p,
			   			   k_price_abonent_pp, k_price_rest_pp, b_price_fixed_pp,
						   k_price_abonent_ma1, k_price_rest_ma1, b_price_fixed_ma1
               INTO _partner_id, _operator_id, _num, _service_id, _active, _currency,
			   _k_abon_p, _k_rest_p, _b_fix_p,
			   _k_abon_pp, _k_rest_pp, _b_fix_pp,
			   _k_abon_ma1, _k_rest_ma1, _b_fix_ma1
               from plugin_cyka_t WHERE id = _id;
	    
	    SELECT partner_agent_id, partner_manager_id INTO _agent_id, _manager_id
			   FROM partner_t WHERE id = _partner_id;
	    INSERT INTO a values("A2");
        IF  _active = 1 THEN
		   INSERT INTO a values("ACTIVE");
		   
           INSERT INTO money_consumption_rule
                     (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                     filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                     VALUES (_partner_id, 1, 1, 1, 1, 30, _id, _k_abon_p, _k_rest_p, _b_fix_p, _currency);
           SELECT last_insert_id() INTO _rule_id;
           INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
           INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
           INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
           INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);
		   
		   IF _agent_id IS NOT NULL THEN
               INSERT INTO money_consumption_rule
                         (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                         filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                         VALUES (_agent_id, 1, 1, 1, 1, 40, _id, _k_abon_pp, _k_rest_pp, _b_fix_pp, _currency);
               SELECT last_insert_id() INTO _rule_id;
               INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
               INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
               INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
               INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);
   		   END IF;
		   
		   IF _manager_id IS NOT NULL THEN
               INSERT INTO money_consumption_rule
                         (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                         filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                         VALUES (_manager_id, 1, 1, 1, 1, 50, _id, _k_abon_ma1, _k_rest_ma1, _b_fix_ma1, _currency);
               SELECT last_insert_id() INTO _rule_id;
               INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
               INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
               INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
               INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);
           END IF; 
        END IF;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `update_abonent_fraud` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `update_abonent_fraud`(_oper INTEGER, _abonent VARCHAR(20), _summ DOUBLE, _limit DOUBLE, _currency CHAR(3))
BEGIN
        INSERT INTO fraud_abonent (abonent, money_total, is_fraud, in_whitelist, operator_id, currency, operator_fraud_limit)
            VALUES (_abonent, _summ, IF(_summ > _limit, 1, 0), 0, _oper, _currency, _limit)
        ON DUPLICATE KEY UPDATE money_total = if(currency = _currency, money_total + _summ, _summ), currency = _currency, operator_fraud_limit = _limit,
           is_fraud = if(money_total > _limit and in_whitelist <> 1, 1, 0);
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
/*!50003 DROP PROCEDURE IF EXISTS `warn` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE PROCEDURE `warn`(_msg TEXT)
INSERT INTO debug (date, category, msg) values(now(), 'warn', _msg) */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
DELIMITER ;

--
-- View structure for view `manager`
--

/*!50001 DROP TABLE IF EXISTS `manager`*/;
/*!50001 DROP VIEW IF EXISTS `manager`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `manager` AS select `money_consumer_t`.`id` AS `id`,`money_consumer_t`.`legal_entity_id` AS `legal_entity_id`,`money_consumer_t`.`fio` AS `fio`,`money_consumer_t`.`phone` AS `phone`,`money_consumer_t`.`email` AS `email`,`money_consumer_t`.`icq` AS `icq`,`money_consumer_t`.`wap_url` AS `wap_url`,`money_consumer_t`.`web_url` AS `web_url`,`money_consumer_t`.`login` AS `login`,`money_consumer_t`.`password` AS `password`,`money_consumer_t`.`contract_number` AS `contract_number`,`money_consumer_t`.`payment_way` AS `payment_way`,`money_consumer_t`.`payment_comment` AS `payment_comment`,`money_consumer_t`.`payment_account` AS `payment_account`,`money_consumer_t`.`bank_name` AS `bank_name`,`money_consumer_t`.`bank_ras_account` AS `bank_ras_account`,`money_consumer_t`.`bank_cor_account` AS `bank_cor_account`,`money_consumer_t`.`bank_bik` AS `bank_bik`,`money_consumer_t`.`comment` AS `comment`,`money_consumer_t`.`name` AS `name`,`money_consumer_t`.`reg_date` AS `reg_date`,`money_consumer_t`.`active` AS `active`,`money_consumer_t`.`activate_date` AS `activate_date`,`money_consumer_t`.`manager_id` AS `manager_id`,`money_consumer_t`.`money` AS `money`,`money_consumer_t`.`money_fraud` AS `money_fraud`,`money_consumer_t`.`currency` AS `currency` from (`money_consumer_t` join `manager_t` on((`money_consumer_t`.`id` = `manager_t`.`id`))) where (`money_consumer_t`.`_deleted` <> 1)*/;

--
-- View structure for view `money_consumer`
--

/*!50001 DROP TABLE IF EXISTS `money_consumer`*/;
/*!50001 DROP VIEW IF EXISTS `money_consumer`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `money_consumer` AS select `money_consumer_t`.`id` AS `id`,`money_consumer_t`.`legal_entity_id` AS `legal_entity_id`,`money_consumer_t`.`fio` AS `fio`,`money_consumer_t`.`phone` AS `phone`,`money_consumer_t`.`email` AS `email`,`money_consumer_t`.`icq` AS `icq`,`money_consumer_t`.`wap_url` AS `wap_url`,`money_consumer_t`.`web_url` AS `web_url`,`money_consumer_t`.`login` AS `login`,`money_consumer_t`.`password` AS `password`,`money_consumer_t`.`contract_number` AS `contract_number`,`money_consumer_t`.`payment_way` AS `payment_way`,`money_consumer_t`.`payment_comment` AS `payment_comment`,`money_consumer_t`.`payment_account` AS `payment_account`,`money_consumer_t`.`bank_name` AS `bank_name`,`money_consumer_t`.`bank_ras_account` AS `bank_ras_account`,`money_consumer_t`.`bank_cor_account` AS `bank_cor_account`,`money_consumer_t`.`bank_bik` AS `bank_bik`,`money_consumer_t`.`comment` AS `comment`,`money_consumer_t`.`name` AS `name`,`money_consumer_t`.`reg_date` AS `reg_date`,`money_consumer_t`.`active` AS `active`,`money_consumer_t`.`activate_date` AS `activate_date`,`money_consumer_t`.`_deleted` AS `_deleted`,`money_consumer_t`.`_deleted_date` AS `_deleted_date`,`money_consumer_t`.`manager_id` AS `manager_id`,`money_consumer_t`.`money` AS `money`,`money_consumer_t`.`money_fraud` AS `money_fraud`,`money_consumer_t`.`currency` AS `currency` from `money_consumer_t` where (`money_consumer_t`.`_deleted` <> 1)*/;

--
-- View structure for view `operator`
--

/*!50001 DROP TABLE IF EXISTS `operator`*/;
/*!50001 DROP VIEW IF EXISTS `operator`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `operator` AS select `o`.`id` AS `id`,`o`.`parent_id` AS `parent_id`,`o`.`name` AS `name`,`o`.`level` AS `level`,`o`.`billing_rule` AS `billing_rule`,count(`o2`.`id`) AS `cnt`,`o`.`koef` AS `koef`,`o`.`fraud_limit` AS `fraud_limit`,`o`.`fraud_currency` AS `fraud_currency` from (`operator_t` `o` left join `operator_t` `o2` on((`o2`.`parent_id` = `o`.`id`))) where (`o`.`_deleted` <> 1) group by `o`.`id`*/;

--
-- View structure for view `partner`
--

/*!50001 DROP TABLE IF EXISTS `partner`*/;
/*!50001 DROP VIEW IF EXISTS `partner`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `partner` AS select `partner_t`.`id` AS `id`,`partner_t`.`partner_agent_id` AS `partner_agent_id`,`partner_t`.`partner_manager_id` AS `partner_manager_id`,`money_consumer_t`.`legal_entity_id` AS `legal_entity_id`,`money_consumer_t`.`login` AS `login`,`money_consumer_t`.`password` AS `password`,`money_consumer_t`.`fio` AS `fio`,`money_consumer_t`.`phone` AS `phone`,`money_consumer_t`.`email` AS `email`,`money_consumer_t`.`icq` AS `icq`,`money_consumer_t`.`wap_url` AS `wap_url`,`money_consumer_t`.`web_url` AS `web_url`,`money_consumer_t`.`contract_number` AS `contract_number`,`money_consumer_t`.`payment_way` AS `payment_way`,`money_consumer_t`.`payment_comment` AS `payment_comment`,`money_consumer_t`.`payment_account` AS `payment_account`,`money_consumer_t`.`bank_name` AS `bank_name`,`money_consumer_t`.`bank_ras_account` AS `bank_ras_account`,`money_consumer_t`.`bank_cor_account` AS `bank_cor_account`,`money_consumer_t`.`bank_bik` AS `bank_bik`,`money_consumer_t`.`comment` AS `comment`,`money_consumer_t`.`name` AS `name`,`money_consumer_t`.`reg_date` AS `reg_date`,`money_consumer_t`.`active` AS `active`,`money_consumer_t`.`activate_date` AS `activate_date`,`money_consumer_t`.`_deleted` AS `_deleted`,`money_consumer_t`.`_deleted_date` AS `_deleted_date`,`money_consumer_t`.`manager_id` AS `manager_id`,`money_consumer_t`.`money` AS `money`,`money_consumer_t`.`money_fraud` AS `money_fraud`,`money_consumer_t`.`currency` AS `currency` from (`partner_t` join `money_consumer_t` on((`partner_t`.`id` = `money_consumer_t`.`id`))) where (`money_consumer_t`.`_deleted` <> 1)*/;

--
-- View structure for view `partner_agent`
--

/*!50001 DROP TABLE IF EXISTS `partner_agent`*/;
/*!50001 DROP VIEW IF EXISTS `partner_agent`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `partner_agent` AS select `money_consumer_t`.`id` AS `id`,`money_consumer_t`.`legal_entity_id` AS `legal_entity_id`,`money_consumer_t`.`fio` AS `fio`,`money_consumer_t`.`phone` AS `phone`,`money_consumer_t`.`email` AS `email`,`money_consumer_t`.`icq` AS `icq`,`money_consumer_t`.`wap_url` AS `wap_url`,`money_consumer_t`.`web_url` AS `web_url`,`money_consumer_t`.`login` AS `login`,`money_consumer_t`.`password` AS `password`,`money_consumer_t`.`contract_number` AS `contract_number`,`money_consumer_t`.`payment_way` AS `payment_way`,`money_consumer_t`.`payment_comment` AS `payment_comment`,`money_consumer_t`.`payment_account` AS `payment_account`,`money_consumer_t`.`bank_name` AS `bank_name`,`money_consumer_t`.`bank_ras_account` AS `bank_ras_account`,`money_consumer_t`.`bank_cor_account` AS `bank_cor_account`,`money_consumer_t`.`bank_bik` AS `bank_bik`,`money_consumer_t`.`comment` AS `comment`,`money_consumer_t`.`name` AS `name`,`money_consumer_t`.`reg_date` AS `reg_date`,`money_consumer_t`.`active` AS `active`,`money_consumer_t`.`activate_date` AS `activate_date`,`money_consumer_t`.`manager_id` AS `manager_id`,`money_consumer_t`.`money` AS `money`,`money_consumer_t`.`money_fraud` AS `money_fraud`,`money_consumer_t`.`currency` AS `currency` from (`money_consumer_t` join `partner_agent_t` on((`money_consumer_t`.`id` = `partner_agent_t`.`id`))) where (`money_consumer_t`.`_deleted` <> 1)*/;

--
-- View structure for view `plugin`
--

/*!50001 DROP TABLE IF EXISTS `plugin`*/;
/*!50001 DROP VIEW IF EXISTS `plugin`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `plugin` AS select `plugin_t`.`id` AS `id`,`plugin_t`.`pattern` AS `pattern`,`plugin_t`.`service_id` AS `service_id`,`plugin_t`.`priority` AS `priority`,`plugin_t`.`is_default_num` AS `is_default_num`,`plugin_t`.`is_default_operator` AS `is_default_operator`,`plugin_t`.`operator_id` AS `operator_id`,`plugin_t`.`num` AS `num`,`plugin_t`.`msg_type` AS `msg_type`,`plugin_t`.`ignore_partner_from_service` AS `ignore_partner_from_service`,`plugin_t`.`partner_id` AS `partner_id`,`plugin_t`.`active` AS `active` from `plugin_t` where (`plugin_t`.`active` = 1)*/;

--
-- View structure for view `service`
--

/*!50001 DROP TABLE IF EXISTS `service`*/;
/*!50001 DROP VIEW IF EXISTS `service`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `service` AS select `s`.`id` AS `id`,`s`.`name` AS `name`,`s`.`description` AS `description`,`s`.`sgroup_id` AS `sgroup_id`,`s`.`uri` AS `uri`,`s`.`email` AS `email`,`g`.`name` AS `groupname`,concat(`g`.`name`,_cp1251'::',`s`.`name`) AS `fullname` from (`service_t` `s` join `sgroup` `g` on((`g`.`id` = `s`.`sgroup_id`)))*/;

--
-- View structure for view `sgroup`
--

/*!50001 DROP TABLE IF EXISTS `sgroup`*/;
/*!50001 DROP VIEW IF EXISTS `sgroup`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`asg`@`%` SQL SECURITY DEFINER VIEW `sgroup` AS select `sgroup_t`.`id` AS `id`,`sgroup_t`.`name` AS `name`,`sgroup_t`.`descr` AS `descr` from `sgroup_t` where (`sgroup_t`.`_deleted` = 0)*/;

--
-- View structure for view `smsc`
--

/*!50001 DROP TABLE IF EXISTS `smsc`*/;
/*!50001 DROP VIEW IF EXISTS `smsc`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `smsc` AS select `smsc_t`.`id` AS `id`,`smsc_t`.`is_active` AS `is_active`,`smsc_t`.`default_operator_id` AS `default_operator_id`,`smsc_t`.`name` AS `name`,`smsc_t`.`host` AS `host`,`smsc_t`.`port` AS `port`,`smsc_t`.`system_id` AS `system_id`,`smsc_t`.`password` AS `password`,`smsc_t`.`system_type` AS `system_type`,`smsc_t`.`smpp_version` AS `smpp_version`,`smsc_t`.`interface_version` AS `interface_version`,`smsc_t`.`addr_ton` AS `addr_ton`,`smsc_t`.`addr_npi` AS `addr_npi`,`smsc_t`.`src_addr_ton` AS `src_addr_ton`,`smsc_t`.`src_addr_npi` AS `src_addr_npi`,`smsc_t`.`dest_addr_ton` AS `dest_addr_ton`,`smsc_t`.`dest_addr_npi` AS `dest_addr_npi`,`smsc_t`.`addr_range` AS `addr_range`,`smsc_t`.`smpp_mode` AS `smpp_mode`,`smsc_t`.`sync` AS `sync`,`smsc_t`.`debug_level` AS `debug_level`,`smsc_t`.`rebind_timeout` AS `rebind_timeout`,`smsc_t`.`elink_timeout` AS `elink_timeout`,`smsc_t`.`registered_delivery` AS `registered_delivery`,`smsc_t`.`use7bit` AS `use7bit`,`smsc_t`.`gmt_offset` AS `gmt_offset`,`smsc_t`.`description` AS `description`,`smsc_t`.`_deleted` AS `_deleted`,`smsc_t`.`_deleted_datetime` AS `_deleted_datetime` from `smsc_t` where (`smsc_t`.`_deleted` <> 1)*/;

--
-- View structure for view `view_payment`
--

/*!50001 DROP TABLE IF EXISTS `view_payment`*/;
/*!50001 DROP VIEW IF EXISTS `view_payment`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_payment` AS select `p`.`id` AS `id`,`p`.`date` AS `date`,`p`.`money_consumer_id` AS `money_consumer_id`,`c`.`name` AS `money_consumer_name`,`c`.`fio` AS `money_consumer_fio`,`c`.`active` AS `money_consumer_active`,`p`.`k_price_abonent` AS `k_price_abonent`,`p`.`k_price_rest` AS `k_price_rest`,`p`.`b_price_fixed` AS `b_price_fixed`,`p`.`price_abonent` AS `price_abonent`,`p`.`price_rest` AS `price_rest`,`p`.`chain_position` AS `chain_position`,`p`.`chain_terminated` AS `chain_terminated`,`p`.`money_consumption_rule_id` AS `money_consumption_rule_id`,`p`.`inbox_id` AS `inbox_id`,`p`.`money` AS `money`,`p`.`is_fraud` AS `is_fraud` from (`payment` `p` join `money_consumer_t` `c` on((`p`.`money_consumer_id` = `c`.`id`)))*/;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

