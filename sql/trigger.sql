drop table if exists tr_replication;
create table tr_replication(
  id int(10) unsigned NOT NULL auto_increment,
  tbl varchar(128),
  op enum('INSERT','UPDATE','DELETE'), 
  cond varchar(128),	
  PRIMARY KEY(id)
);

DROP TRIGGER tr_billing_insert;

DELIMITER |
create trigger tr_billing_insert after insert on billingf for each row 
BEGIN
  insert into tr_replication set id=NULL,
  tbl='billingf',
  op='INSERT',
  cond=concat('inbox_id=',new.inbox_id,' and outbox_id=',new.outbox_id);
END;
|

DROP TRIGGER tr_billing_update
|

create trigger tr_billing_update after update on billingf for each row 
BEGIN
  insert into tr_replication set id=NULL,
  tbl='billingf',
  op='UPDATE',
  cond=concat('inbox_id=',new.inbox_id,' and outbox_id=',new.outbox_id);
END;
|

DELIMITER ;

