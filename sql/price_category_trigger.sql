DELIMITER $$
DROP PROCEDURE IF EXISTS regen_legal_owner_consumption$$
CREATE PROCEDURE regen_legal_owner_consumption()
BEGIN
DECLARE _price_category VARCHAR(45);
DECLARE _legal_owner_id INTEGER;
DECLARE _k_price_abonent_p, _k_price_rest_p, _b_price_fixed_p REAL;
DECLARE _currency_p CHAR(3);

DECLARE _legal_owner_agent_id INTEGER;
DECLARE _k_price_abonent_pl, _k_price_rest_pl, _b_price_fixed_pl REAL;
DECLARE _currency_pl CHAR(3);

DECLARE _mcr_id INTEGER;
DECLARE cursor_done INTEGER;

DECLARE regen_cursor CURSOR FOR
		SELECT category_name, legal_owner_id, k_price_abonent_p, k_price_rest_p, b_price_fixed_p, currency_p, legal_owner_agent_id, k_price_abonent_pl, k_price_rest_pl, b_price_fixed_pl, currency_pl FROM price_category_regen_tmp;

DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done = 1;

DELETE FROM money_consumption_rule where filter_on_legal_owner = 1;

DROP TABLE IF EXISTS price_category_regen_tmp;
CREATE TABLE price_category_regen_tmp AS select p.name as category_name , l.id as legal_owner_id, p.k_price_abonent as k_price_abonent_p, p.k_price_rest as k_price_rest_p, p.b_price_fixed as b_price_fixed_p, p.currency as currency_p, l.legal_owner_agent_id, l.k_price_abonent_pl, l.k_price_rest_pl, l.b_price_fixed_pl, l.currency_pl from legal_owner l, price_category p where l.login = 'generic_content_owner';

OPEN regen_cursor;

regen_loop: LOOP
			SET cursor_done = 0;
			FETCH regen_cursor INTO _price_category,
				  _legal_owner_id, _k_price_abonent_p, _k_price_rest_p, _b_price_fixed_p, _currency_p,
				  _legal_owner_agent_id, _k_price_abonent_pl, _k_price_rest_pl, _b_price_fixed_pl, _currency_pl;

            IF cursor_done THEN LEAVE regen_loop; END IF;

			INSERT INTO money_consumption_rule
				   (money_consumer_id, filter_on_legal_owner, priority, k_price_abonent, k_price_rest, b_price_fixed, currency)
 				   VALUES (_legal_owner_id, 1, 90, _k_price_abonent_p, _k_price_rest_p, _b_price_fixed_p, _currency_p);

			SELECT last_insert_id() into _mcr_id;
            INSERT INTO consumption_filter_legal_owner (id, money_consumption_rule_id, legal_owner_id, category)
				   VALUES (null, _mcr_id, _legal_owner_id, _price_category);

            IF _legal_owner_agent_id IS NOT NULL THEN
			   INSERT INTO money_consumption_rule
				   (money_consumer_id, filter_on_legal_owner, priority, k_price_abonent, k_price_rest, b_price_fixed, currency)
 				   VALUES (_legal_owner_agent_id, 1, 80, _k_price_abonent_pl, _k_price_rest_pl, _b_price_fixed_pl, _currency_pl);

			    SELECT last_insert_id() into _mcr_id;
            	INSERT INTO consumption_filter_legal_owner (id, money_consumption_rule_id, legal_owner_id, category)
				   VALUES (null, _mcr_id, _legal_owner_id, _price_category);

			END IF;
			END LOOP;
END $$

DELIMITER ;
