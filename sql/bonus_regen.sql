DROP PROCEDURE IF EXISTS regen_bonus_rules;
DELIMITER $$
CREATE PROCEDURE regen_bonus_rules()
BEGIN
		DECLARE _bonus_consumer_id INTEGER;
		DECLARE _bonus_action_id INTEGER;
 		DECLARE cursor_done INTEGER;

		DECLARE _num VARCHAR(20);
		DECLARE _points, _operator_id INTEGER;
		DECLARE _rule_id INTEGER;

		DECLARE bonus_num_cursor CURSOR FOR
				(SELECT null, num, points FROM bonus_num)
				UNION
				(SELECT id, num, points FROM bonus_action
					   WHERE active = 1
					   		 and current_date() between date_start and date_end);

 		DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done = 1;

		SELECT id INTO _bonus_consumer_id FROM money_consumer WHERE login = 'a1_bonus_consumer' LIMIT 1;
		DELETE FROM money_consumption_rule WHERE money_consumer_id = _bonus_consumer_id;

		OPEN bonus_num_cursor;
		bonus_num_loop: LOOP
						SET cursor_done = 0;
						FETCH bonus_num_cursor INTO _bonus_action_id, _num, _points;
						IF cursor_done THEN LEAVE bonus_num_loop; END IF;

						INSERT INTO money_consumption_rule SET
							   money_consumer_id = _bonus_consumer_id,
							   filter_on_num = 1,
							   filter_on_operator = if(_bonus_action_id IS NOT NULL, 1, 0),
							   priority = 50,
							   b_price_fixed = _points,
							   currency = 'a1c';

						SELECT last_insert_id() INTO _rule_id;

						INSERT INTO consumption_filter_num SET 
						   	   money_consumption_rule_id = _rule_id,
							   num = _num;

						IF _bonus_action_id IS NOT NULL THEN
						   INSERT INTO consumption_filter_operator
						   		  SELECT null, _rule_id, operator_id FROM bonus_operator
								  		 WHERE bonus_action_id = _bonus_action_id;
						   		  
						END IF;

        END LOOP;

END$$

DROP PROCEDURE IF EXISTS process_bonus_payments $$
CREATE PROCEDURE process_bonus_payments()
BEGIN
		DECLARE _date, _old_date DATETIME;
		DECLARE _bonus_consumer_id INTEGER;

		SELECT id INTO _bonus_consumer_id FROM money_consumer WHERE login = 'a1_bonus_consumer' LIMIT 1;
		SELECT now() INTO _date;
		SELECT `date` INTO _old_date FROM timestamps WHERE name = 'bonus_to_abonent';
		IF _old_date IS NULL THEN
		   set _old_date = '2000-01-01';
		END IF;

		DROP TABLE IF EXISTS tmp_bonus_payments;
		CREATE TABLE tmp_bonus_payments AS 
			   SELECT billing.abonent, sum(payment.consumer_money) as consumer_money
			   FROM  payment JOIN billing on payment.inbox_id = billing.inbox_id
			   WHERE billing.dir = 1 AND
			   		 payment._timestamp BETWEEN _old_date and _date AND
					 payment.money_consumer_id = _bonus_consumer_id
			   GROUP BY 1;

        INSERT INTO bonus_abonent (abonent, points)
               SELECT DISTINCT tmp_bonus_payments.abonent, 0
               FROM tmp_bonus_payments LEFT JOIN bonus_abonent ON tmp_bonus_payments.abonent = bonus_abonent.abonent
                    WHERE bonus_abonent.abonent IS NULL;

        UPDATE bonus_abonent, tmp_bonus_payments
               SET bonus_abonent.points = bonus_abonent.points + tmp_bonus_payments.consumer_money
               WHERE bonus_abonent.abonent = tmp_bonus_payments.abonent;

        REPLACE timestamps SET `name` = 'bonus_to_abonent', `date` = _date;
END$$

DELIMITER ;

