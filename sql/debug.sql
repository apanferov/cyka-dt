DROP PROCEDURE IF EXISTS warn;
CREATE PROCEDURE warn(_msg TEXT)
	   INSERT INTO debug (date, category, msg) values(now(), 'warn', _msg);

DROP PROCEDURE IF EXISTS data_error;
CREATE PROCEDURE data_error(_msg TEXT)
	   INSERT INTO debug (date, category, msg) values(now(), 'data_error', _msg);