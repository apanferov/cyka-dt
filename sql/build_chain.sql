DELIMITER $$

DROP PROCEDURE IF EXISTS `alt4`.`build_chain`$$
CREATE PROCEDURE  `alt4`.`build_chain`(_oper INTEGER, _num VARCHAR(20), _part INTEGER, _owner INTEGER, _svc INTEGER)
select * from money_consumer c
       join money_consumption_rule cr on c.id = cr.money_consumer_id
       left join consumption_filter_operator fo on if(cr.filter_on_operator, fo.money_consumption_rule_id = cr.id, null)
       left join consumption_filter_num  fn on if(cr.filter_on_num, fn.money_consumption_rule_id = cr.id, null)
       left join consumption_filter_partner  fp on if(cr.filter_on_partner, fp.money_consumption_rule_id = cr.id, null)
       left join consumption_filter_legal_owner fl on if(cr.filter_on_legal_owner, fl.money_consumption_rule_id = cr.id, null)
       left join consumption_filter_service fs on if(cr.filter_on_service, fs.money_consumption_rule_id = cr.id, null)
where 
    if(cr.filter_on_operator, fo.operator_id = _oper, 1) and
    if(cr.filter_on_partner, fp.partner_id = _part, 1) and
    if(cr.filter_on_partner, fn.num = _num, 1) and
    if(cr.filter_on_legal_owner, fl.legal_owner_id = _owner, 1) and
    if(cr.filter_on_service, fs.service_id = _svc, 1)
order by cr.priority 
$$

DELIMITER ;
