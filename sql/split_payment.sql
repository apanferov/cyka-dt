DELIMITER $$

DROP FUNCTION IF EXISTS split_payment $$
CREATE FUNCTION split_payment (_inbox_id INTEGER,_payment_id INTEGER,_legal_owner_id INTEGER,_price_name varchar(100)) RETURNS INTEGER
BEGIN
    DECLARE _money,_price_abonent,_price_rest,_provider_income REAL;
    DECLARE _chain_position INTEGER;
    
    DECLARE _k_price_abonent_p,_k_price_rest_p,_k_price_abonent_pp REAL;
    DECLARE _b_price_fixed_p, _b_price_fixed_pp REAL;
    
    DECLARE _legal_owner_agent_id,_generic_owner_id INTEGER;
    DECLARE _is_fraud INTEGER;
    
    DECLARE _pmoney,_ppmoney REAL;
    DECLARE _currency_p,_currency_pp char(3);
    
    DECLARE _res_id INTEGER;
    
    select money,price_abonent,price_rest,chain_position,is_fraud,money_consumer_id,provider_income into _money,_price_abonent,_price_rest,_chain_position,_is_fraud,_generic_owner_id,_provider_income 
	from payment where id=_payment_id;
    	
    select k_price_abonent,k_price_rest, get_rur(currency,b_price_fixed) into _k_price_abonent_p,_k_price_rest_p, _b_price_fixed_p from price_category where 
	name=_price_name;   

    select k_price_abonent_pl,get_rur(currency_pl,b_price_fixed_pl),legal_owner_agent_id,currency,currency_pl into _k_price_abonent_pp, _b_price_fixed_pp,_legal_owner_agent_id,_currency_p,_currency_pp from legal_owner where 
	id=_legal_owner_id;   



	-- money for legal_owner
	set _pmoney=_k_price_abonent_p * _price_abonent + _k_price_rest_p * _price_rest +  _b_price_fixed_p;


	IF _pmoney IS NULL THEN
	   SET _pmoney = 0;
	END IF;

	if _money >= _pmoney then
		update money_consumer_t set 
			money = money+if(_is_fraud,0,from_rur(currency,_pmoney)),
			money_fraud = money_fraud+if(_is_fraud,from_rur(currency,_pmoney),0) 
			where id=_legal_owner_id;

		update money_consumer_t set 
			money = money-if(_is_fraud,0,from_rur(currency, _pmoney)),
			money_fraud = money_fraud-if(_is_fraud,from_rur(currency, _pmoney),0) 
			where id=_generic_owner_id;

		update payment set chain_position=chain_position+1 where 
		    inbox_id=_inbox_id and chain_position>=_chain_position;

		-- legal owner;
		insert into payment set
		id=NULL,
		date=now(),
	  	money_consumer_id = _legal_owner_id,
	  	k_price_abonent = _k_price_abonent_p,
	  	k_price_rest = _k_price_rest_p,
	    b_price_fixed = _b_price_fixed_p,
	    
	    price_abonent=_price_abonent,
	    price_rest=_price_rest,
	
	    chain_position=_chain_position,
	    chain_terminated=0, -- ?
	    
	    money_consumption_rule_id=0,
	    inbox_id=_inbox_id,
	    
	    money= _pmoney,
	    is_fraud = _is_fraud,
	    consumer_currency = _currency_p ,
	    provider_income = _provider_income + _money - _pmoney,
	    consumer_money = if(is_fraud,0,from_rur(_currency_p,_pmoney)),
	    consumer_money_fraud = if(is_fraud,from_rur(_currency_p,_pmoney),0);
	
		--
		select 	last_insert_id() into _res_id;	
	
		
	    -- old generic legal owner
		update payment set 
			money= money - _pmoney,
			consumer_money_fraud = if(is_fraud,from_rur(consumer_currency,money),0), 
			consumer_money = if(is_fraud,0,from_rur(consumer_currency,money))
			where id=_payment_id;
			
		return _res_id;
	END IF;	
	-- call payment_debug('no more money for inbox_id=');
	return -1;
END $$

DELIMITER ;