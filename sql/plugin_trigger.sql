DROP PROCEDURE IF EXISTS regen_consumption_rule;
DELIMITER $$
CREATE PROCEDURE regen_consumption_rule(_id INTEGER)
BEGIN
        DECLARE _rule_id, _partner_id, _operator_id, _service_id INTEGER;
        DECLARE _num VARCHAR(20);
        DECLARE _active TINYINT;
        DECLARE _k_rest_p, _k_abon_p, _b_fix_p REAL;
        DECLARE _k_rest_pp, _k_abon_pp, _b_fix_pp REAL;
        DECLARE _k_rest_ma1, _k_abon_ma1, _b_fix_ma1 REAL;
		DECLARE _manager_id, _agent_id INTEGER;
		DECLARE _currency VARCHAR(3);
		DECLARE _filter_on_operator TINYINT;

        -- Cleanup old, CASCADE FK must clean other tables
        DELETE FROM money_consumption_rule WHERE plugin_id = _id;

        -- Fetch data from new row in plugin_cyka_t
        SELECT partner_id, operator_id, num, service_id, active, currency,
			   			   k_price_abonent_p, k_price_rest_p, b_price_fixed_p,
			   			   k_price_abonent_pp, k_price_rest_pp, b_price_fixed_pp,
						   k_price_abonent_ma1, k_price_rest_ma1, b_price_fixed_ma1
               INTO _partner_id, _operator_id, _num, _service_id, _active, _currency,
			   _k_abon_p, _k_rest_p, _b_fix_p,
			   _k_abon_pp, _k_rest_pp, _b_fix_pp,
			   _k_abon_ma1, _k_rest_ma1, _b_fix_ma1
               from plugin_cyka_t WHERE id = _id;

	    IF _operator_id = 1 THEN
		   SET _filter_on_operator = 0;
		ELSE
		   SET _filter_on_operator = 1;
		END IF;

	    -- Fetch additional partner data
	    SELECT partner_agent_id, partner_manager_id INTO _agent_id, _manager_id
			   FROM partner_t WHERE id = _partner_id;

        IF  _active = 1 THEN
		   -- partner consumption
           INSERT INTO money_consumption_rule
                     (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                     filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                     VALUES (_partner_id, _filter_on_operator, 1, 1, 1, 70, _id, _k_abon_p, _k_rest_p, _b_fix_p, _currency);
           SELECT last_insert_id() INTO _rule_id;
		   IF _filter_on_operator = 1 THEN
           	  INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
		   END IF;
           INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
           INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
           INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);

		   -- partner agent consumption
		   IF _agent_id IS NOT NULL THEN
               INSERT INTO money_consumption_rule
                         (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                         filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                         VALUES (_agent_id, 1, 1, 1, 1, 60, _id, _k_abon_pp, _k_rest_pp, _b_fix_pp, _currency);
               SELECT last_insert_id() INTO _rule_id;
    		   IF _filter_on_operator = 1 THEN
 			   	  INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
 			   END IF;
               INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
               INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
               INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);
   		   END IF;

		   -- manager consumption
		   IF _manager_id IS NOT NULL THEN
               INSERT INTO money_consumption_rule
                         (money_consumer_id, filter_on_operator, filter_on_num, filter_on_partner,
                         filter_on_service, priority, plugin_id, k_price_abonent, k_price_rest, b_price_fixed, currency)
                         VALUES (_manager_id, 1, 1, 1, 1, 40, _id, _k_abon_ma1, _k_rest_ma1, _b_fix_ma1, _currency);
               SELECT last_insert_id() INTO _rule_id;
    		   IF _filter_on_operator = 1 THEN
 			   	  INSERT INTO consumption_filter_operator (money_consumption_rule_id, operator_id) VALUES (_rule_id, _operator_id);
 			   END IF;
               INSERT INTO consumption_filter_service  (money_consumption_rule_id, service_id)  VALUES (_rule_id, _service_id);
               INSERT INTO consumption_filter_partner (money_consumption_rule_id, partner_id)  VALUES (_rule_id, _partner_id);
               INSERT INTO consumption_filter_num     (money_consumption_rule_id, num)         VALUES (_rule_id, _num);
           END IF; 
        END IF;
END$$

DELIMITER ;
