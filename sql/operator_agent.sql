DELIMITER $$
DROP PROCEDURE IF EXISTS regen_operator_agent_consumption $$
CREATE PROCEDURE regen_operator_agent_consumption ()
BEGIN
    DECLARE cursor_done TINYINT;
	DECLARE _agent_id, _operator_id, _rule_id INTEGER;
	DECLARE _currency CHAR(3);
	DECLARE _k_abon, _k_rest, _b_fixed REAL;

	DECLARE agent_cursor CURSOR FOR
 			SELECT id, k_price_abonent_po, k_price_rest_po, b_price_fixed_po, currency_po, operator_id 
 				   FROM operator_agent WHERE active = 1 AND operator_id IS NOT NULL;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done = 1;

	DELETE FROM money_consumption_rule WHERE money_consumer_id IN (SELECT id FROM operator_agent);

 	OPEN agent_cursor;
 	agent_loop: LOOP
 				SET cursor_done = 0;
 				FETCH agent_cursor INTO _agent_id, _k_abon, _k_rest, _b_fixed, _currency, _operator_id;
 				IF cursor_done THEN LEAVE agent_loop; END IF;

 				INSERT INTO money_consumption_rule SET
					   money_consumer_id = _agent_id,
 					   filter_on_operator = 1,
 					   priority = 100,
					   k_price_abonent = _k_abon,
					   k_price_rest = _k_rest,
 					   b_price_fixed = _b_fixed,
 					   currency = _currency;

				SELECT last_insert_id() INTO _rule_id;

 				INSERT INTO consumption_filter_operator SET 
 					   money_consumption_rule_id = _rule_id,
 					   operator_id = _operator_id;
    END LOOP;
END $$
DELIMITER ;

