DELIMITER $$

DROP PROCEDURE IF EXISTS `restart_sms`$$
CREATE PROCEDURE restart_sms(_inbox_id INTEGER)
BEGIN
	   DELETE FROM billing  WHERE inbox_id = _inbox_id AND dir = 1;
	   DELETE FROM billinga WHERE inbox_id = _inbox_id AND dir = 1;

	   UPDATE money_consumer_t, payment
	   		  SET money_consumer_t.money = money_consumer_t.money - payment.consumer_money,
			  	  money_consumer_t.money_fraud = money_consumer_t.money_fraud - payment.consumer_money_fraud
			WHERE money_consumer_t.id = payment.money_consumer_id AND payment.inbox_id = _inbox_id;

	   -- XXX fraud

	   DELETE FROM payment WHERE inbox_id = _inbox_id;

	   INSERT INTO inbox SELECT * from inboxa WHERE id = _inbox_id;
END$$

DELIMITER ;