DROP TABLE IF EXISTS `alt4`.`plugin_cyka_t`;
CREATE TABLE  `alt4`.`plugin_cyka_t` (
  `id` int(10) unsigned NOT NULL,
  `service_id` int(11) unsigned NOT NULL default '0',
  `operator_id` int(10) unsigned NOT NULL,
  `num` varchar(20) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL default '1',

  `k_price_abonent_p` double NOT NULL default '0',
  `k_price_rest_p` double NOT NULL default '0',
  `b_price_fixed_p` double NOT NULL default '0',

  `k_price_abonent_pp` double NOT NULL default '0',
  `k_price_rest_pp` double NOT NULL default '0',
  `b_price_fixed_pp` double NOT NULL default '0',

  `k_price_abonent_ma1` double NOT NULL default '0',
  `k_price_rest_ma1` double NOT NULL default '0',
  `b_price_fixed_ma1` double NOT NULL default '0',

  PRIMARY KEY  (`id`),
  KEY `idx_plugin_cyka__service_id` (`service_id`),
  KEY `idx_plugin_cyka_partner_id` (`partner_id`),
  KEY `idx_plugin_cyka_operator_id` (`operator_id`),
  KEY `idx_plugin_cyka_num` (`num`),

  CONSTRAINT `fk_plugin_cyka_operator` FOREIGN KEY (`operator_id`) REFERENCES `operator_t` (`id`),
  CONSTRAINT `fk_plugin_cyka_service` FOREIGN KEY (`service_id`) REFERENCES `service_t` (`id`),
  CONSTRAINT `fk_plugin_cyka_partner` FOREIGN KEY (`partner_id`) REFERENCES `partner_t` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

CREATE TRIGGER trg_plugin_insert_after AFTER INSERT ON plugin_cyka_t
FOR EACH ROW CALL regen_consumption_rule(NEW.id);

CREATE TRIGGER trg_plugin_update_after AFTER UPDATE ON plugin_cyka_t
FOR EACH ROW CALL regen_consumption_rule(NEW.id);
