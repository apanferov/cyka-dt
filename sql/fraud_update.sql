DROP PROCEDURE IF EXISTS update_abonent_fraud;
DELIMITER $$
CREATE PROCEDURE update_abonent_fraud(_oper INTEGER, _abonent VARCHAR(20), _summ DOUBLE, _limit DOUBLE, _currency CHAR(3))
BEGIN
        INSERT INTO fraud_abonent (abonent, money_total, is_fraud, in_whitelist, operator_id, currency, operator_fraud_limit)
            VALUES (_abonent, _summ, IF(_summ > _limit, 1, 0), 0, _oper, _currency, _limit)
        ON DUPLICATE KEY UPDATE money_total = if(currency = _currency, money_total + _summ, _summ), currency = _currency, operator_fraud_limit = _limit,
           is_fraud = if(money_total > _limit and in_whitelist <> 1, 1, 0);
END $$
DELIMITER ;
