DELIMITER $$

SELECT 'Creating get_prices procedure' AS ' '$$

DROP PROCEDURE IF EXISTS `get_prices`$$
CREATE PROCEDURE get_prices(_op_id INTEGER, _num VARCHAR(20), OUT _price_abon REAL, OUT _price_rest REAL, OUT _currency CHAR(3), OUT _fraud_limit REAL)
BEGIN
		SELECT r.price * n.price_in, r.price * n.estimated_income, o.fraud_currency, o.fraud_limit
			   INTO _price_abon, _price_rest, _currency, _fraud_limit
			   FROM num_price n
			   		left join exchange_rate r ON r.currency = n.currency AND r.date = now()
			   			 join operator o on o.id = _op_id
			   WHERE operator_id = _op_id AND num = _num AND date_start <= date(now())
			   ORDER BY date_start DESC
			   LIMIT 1;

		IF _price_abon IS NULL THEN
		   call warn(concat("No abonent price for operator '",_op_id,"' and num '",_num,"', defaulting to '0'"));
		   SET _price_abon = 0;
		END IF;

		IF _price_rest IS NULL THEN
		   call warn(concat("No estimated for opeartor '",_op_id,"' and num '",_num,"', defaulting to '0'"));
		   SET _price_rest = 0;
		END IF;
END $$

SELECT 'Creating cyka_bill_record procedure' AS ' '$$
DROP PROCEDURE IF EXISTS `cyka_bill_record`$$
CREATE PROCEDURE cyka_bill_record(_inbox_id INTEGER)
proc_body: BEGIN
    DECLARE _smsc_id, _insms_count, _operator_id, _partner_id, _legal_owner_id, _service_id INTEGER;
    DECLARE _num, _abonent, _price_category VARCHAR(20);
    DECLARE _date DATETIME;
    DECLARE _price_abonent, _price_rest_initial, _price_rest_current, _price_rest_after_deduction REAL;
    DECLARE _k_price_abonent, _k_price_rest, _b_price_fixed REAL;
    DECLARE _money_consumption_rule_id, _money_consumer_id INTEGER;
    DECLARE _current_deduction REAL;
    DECLARE _position INTEGER;
    DECLARE _priority INTEGER;
    DECLARE cursor_done INTEGER;
	DECLARE _currency VARCHAR(3);
	DECLARE _is_fraud TINYINT;
	DECLARE _fraud_limit REAL;
	DECLARE _fraud_currency CHAR(3);
	DECLARE _consumer_money, _consumer_money_fraud, _current_deduction_in_consumer_currency REAL;
	DECLARE _tmp_real REAL;

    DECLARE chain_cursor CURSOR FOR
            SELECT money_consumer_id, money_consumption_rule_id, k_price_abonent, 
                   k_price_rest, get_rur(currency, b_price_fixed), priority, money_consumer_currency FROM current_chain ORDER BY priority DESC;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done = 1;

    -- start transaction
    select date, smsc_id, insms_count, b.operator_id, b.num, partner_id, legal_owner_id, service_id, b.abonent, b.category as category
      INTO _date, _smsc_id, _insms_count, _operator_id, _num, _partner_id, _legal_owner_id, _service_id, _abonent, _price_category
		   from billing b left join fraud_abonent fa on b.abonent = fa.abonent
		   where inbox_id = _inbox_id and dir = 1 LIMIT 1;

    CALL get_prices(_operator_id, _num, _price_abonent, _price_rest_initial, _fraud_currency, _fraud_limit);

	-- fraud detection must be first
	SET _is_fraud = 0;
	IF _fraud_currency IS NOT NULL and _fraud_limit IS NOT NULL and _fraud_limit <> 0 THEN
	    SELECT from_rur(_fraud_currency, _price_abonent)
			   INTO _tmp_real;

        IF _tmp_real IS NULL THEN
		   CALL data_error(concat("Exchange rate for '", _fraud_currency, "' is not set"));
		   SET _tmp_real = 0;
		END IF;

        INSERT INTO fraud_abonent
			   SET abonent = _abonent,
			   	   money_total = _tmp_real,
				   is_fraud = if(_tmp_real > _fraud_limit, 1, 0),
				   in_whitelist = 0,
				   operator_id = _operator_id,
				   currency = _fraud_currency,
				   operator_fraud_limit = _fraud_limit
		  ON DUPLICATE KEY UPDATE
		  	 	    money_total = if(currency = _fraud_currency, money_total + _tmp_real, _tmp_real),
					currency = _fraud_currency,
					operator_fraud_limit = _fraud_limit,
				   	is_fraud = if( money_total > _fraud_limit and in_whitelist <> 1, 1, 0);

		  SELECT is_fraud INTO _is_fraud FROM fraud_abonent WHERE abonent = _abonent;
	END IF;

	IF _price_abonent = 0 or _price_rest_initial = 0 THEN
	   call data_error(concat("Some prices is zero for operator '", _operator_id, "' and num '", _num, "' - not building consumer chain"));
	   LEAVE proc_body;
	END IF;

	IF _price_abonent IS NULL or _price_rest_initial IS NULL THEN
	   call warn("Shit happens - prices is null!");
	   LEAVE proc_body;
	END IF;

    SET _price_rest_current = _price_rest_initial * _insms_count;
	SET _price_abonent = _price_abonent * _insms_count;

    DROP TABLE IF EXISTS current_chain;
    CREATE TABLE current_chain AS
                   select c.id as money_consumer_id, cr.id as money_consumption_rule_id, cr.k_price_abonent, 
                   cr.k_price_rest, cr.b_price_fixed, cr.priority, cr.currency as currency, c.currency as money_consumer_currency
                             FROM money_consumer c
                             join money_consumption_rule cr on c.id = cr.money_consumer_id
                             left join consumption_filter_operator fo on if(cr.filter_on_operator, fo.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_num  fn on if(cr.filter_on_num, fn.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_partner  fp on if(cr.filter_on_partner, fp.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_legal_owner fl on if(cr.filter_on_legal_owner, fl.money_consumption_rule_id = cr.id, null)
                             left join consumption_filter_service fs on if(cr.filter_on_service, fs.money_consumption_rule_id = cr.id, null)
                   where c.active = 1 and
                         if(cr.filter_on_operator, fo.operator_id = _operator_id, 1) and
                         if(cr.filter_on_partner, fp.partner_id = _partner_id, 1) and
                         if(cr.filter_on_num, fn.num = _num, 1) and
                         if(cr.filter_on_legal_owner, fl.legal_owner_id = _legal_owner_id and fl.category = _price_category, 1) and
                         if(cr.filter_on_service, fs.service_id = _service_id, 1);

    SET _position = 1;

 	OPEN chain_cursor;
 	chain_loop: LOOP
						SET cursor_done = 0;
                        FETCH chain_cursor INTO  _money_consumer_id, _money_consumption_rule_id,
                              _k_price_abonent, _k_price_rest, _b_price_fixed, _priority, _currency;
                        IF cursor_done THEN LEAVE chain_loop; END IF;

                        SET _current_deduction = _k_price_abonent * _price_abonent + _k_price_rest * _price_rest_current + _b_price_fixed;
						SET _current_deduction_in_consumer_currency = from_rur(_currency, _current_deduction);
						SET _price_rest_after_deduction = _price_rest_current - _current_deduction;

						IF _is_fraud = 1 THEN
						   SET _consumer_money_fraud = _current_deduction_in_consumer_currency;
						   SET _consumer_money = 0;
						ELSE
						   SET _consumer_money = _current_deduction_in_consumer_currency;
						   SET _consumer_money_fraud = 0;
						END IF;								

						INSERT INTO payment SET date = _date,
 							   money_consumer_id = _money_consumer_id,
 							   k_price_abonent = _k_price_abonent,
 							   k_price_rest = _k_price_rest,
 							   b_price_fixed = _b_price_fixed,
 							   chain_position = _position,
 							   chain_terminated = 0,
 							   money_consumption_rule_id = _money_consumption_rule_id,
							   inbox_id = _inbox_id,
							   money = _current_deduction,
							   price_abonent = _price_abonent,
							   price_rest = _price_rest_current,
							   is_fraud = _is_fraud,
							   consumer_currency = _currency,
							   consumer_money = _consumer_money,
							   consumer_money_fraud = _consumer_money_fraud,
							   provider_income = _price_rest_after_deduction;

					    UPDATE money_consumer_t
							   SET money_fraud = money_fraud + _consumer_money_fraud, money = money + _consumer_money
							   WHERE id = _money_consumer_id;

                        SET _price_rest_current = _price_rest_after_deduction;
                        SET _position = _position + 1;
            END LOOP;
            CLOSE chain_cursor;

	-- DROP TABLE current_batch;
    -- commit transaction
END $$

DELIMITER ;

-- ALTER TABLE payment ADD COLUMN consumer_currency CHAR(3) NOT NULL DEFAULT 0;
-- ALTER TABLE payment ADD COLUMN provider_income REAL NOT NULL DEFAULT 0;
-- ALTER TABLE payment ADD COLUMN consumer_money REAL NOT NULL DEFAULT 0;
-- ALTER TABLE payment ADD COLUMN consumer_money_fraud REAL NOT NULL DEFAULT 0;
