#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;

my %service_groups = ( 19 => { name => 'RB',
							   email => 'a.lebedeff@alt1.ru, a.safonov@alt1.ru, admin@smsproxy.ru,  as4czx12az1qw4a5a@russianbilling.com',
#							   email => 'a.lebedeff@alt1.ru',
							 },
					   16 => { name => 'MP',
							   email => 'a.lebedeff@alt1.ru, a.safonov@alt1.ru, denis@mobilefuture.ru',
#							   email => 'a.lebedeff@alt1.ru',
							 },
					 );

sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

	my $report_data = {};

	money_sender($report_data);
	series_sender($report_data);

	while (my ($k, $v) = each %$report_data) {
		my $msg = "";

		$msg .= join "\n", map { (my $abonent = $_->{abonent}) =~ s/^./X/; "$abonent\t$_->{usd}\t$_->{date}"} @{$v->{abonents}};
		$msg .= "\n";
		$msg .= join "\n", map { "$_->{series}XXX\t$_->{usd}" } @{$v->{series}};

		sendmail( to => $service_groups{$k}{email},
				  subject => "$service_groups{$k}{name} report for $v->{abonents}[0]{date}",
				  from => 'fraud@gateasg.alt1.ru',
				  content_type => 'text/plain',
				  body => $msg );
	}
}

sub money_sender {
	my ($report_data) = @_;

	my $services = selectall_hash("set_service_t", "id");

	# print Dumper($services);

	my $dbh = Config->dbh;

	my $sth = $dbh->prepare("select * from abonent_money_daily where date = date(date_sub(now(), interval 1 day)) and usd > 30");

	$sth->execute;
	while (defined(my $hr = $sth->fetchrow_hashref)) {

		my $sgroup = $services->{last_service($hr->{abonent})}{sgroup_id};

		if ($service_groups{$sgroup}) {
			my ($age) = $dbh->selectrow_array("select unix_timestamp(now()) - unix_timestamp(min(date)) from tr_inboxa where abonent = ?",
											  undef, $hr->{abonent});
			if ($age < 60 * 60 * 24 * 8) {
				my $tmp = Config->dbh->selectrow_hashref("select * from tr_inboxa where abonent = ? order by id desc limit 1", undef,
														 $hr->{abonent});

				$hr->{inbox} = $tmp;
				$hr->{msg} = substr $tmp->{msg}, 0, 4;

				push @{$report_data->{$sgroup}{abonents}}, $hr;
			}
		}
	}

}

sub series_sender {
	my ( $report_data ) = @_;
	my ( $week ) = Config->dbh->selectrow_array("select date_format(date_sub(now(), interval weekday(now()) day), '%Y%m%d')");

	my $sth = Config->dbh->prepare("select * from abonent_series_weekly where week = ? and usd > 100 and uniq > 4 and min_age < 60 * 60 * 24 * 8");
	my $services = selectall_hash("set_service_t", "id");

	$sth->execute($week);
	while (defined(my $hr = $sth->fetchrow_hashref)) {
		my ($sid) = Config->dbh->selectrow_array("select service_id from tr_billing where inbox_id = ? and dir = 1", undef, $hr->{last_inbox_id});
		my $sgroup = $services->{$sid}{sgroup_id};

		# print Dumper($hr, $sgroup);
		if ($service_groups{$sgroup}) {
			$hr->{inbox} = Config->dbh->selectrow_hashref("select * from tr_inboxa where id = ?", undef, $hr->{last_inbox_id});
			$hr->{msg} = substr $hr->{inbox}{msg}, 0, 4;
			push @{$report_data->{$sgroup}{series}}, $hr;
		}
	}

}

sub last_service {
	my ( $abonent ) = @_;

	my ( $last ) = Config->dbh->selectrow_array("select service_id from tr_billing where abonent = ? and date > date(date_sub(now(), interval 1 day)) order by date desc limit 1", undef, $abonent);
	return $last;
}



main();
