#!/usr/bin/perl
use strict;
use locale;
use CGI;

sub main {
	my $q = new CGI;
	print $q->header(-type => 'text/plain', -charset => 'koi8-r');

	my $dest = $q->param('user_id');
	my $msg = $q->param('msg');

	if (length $msg > 50) {
		$msg =~ s/^(.{25}).*(.{25})$/$1...$2/;
	}

	print <<EOF;
status: reply
bill-incoming: echoincoming
bill-outgoing: echooutgoing
destination: $dest
transaction-id: 12
service-id: 1

ECHO: $msg
EOF

}



main();



