#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use Data::Dumper;

print "truncate set_osmp_range;\n";

while (<>) {
    my %m = /(\w+)="(\w+)"/gi;
    if ( $m{to} and $m{from} and $m{prv}) {
	my $from = delete $m{from};
	my $to = delete $m{to};
	my $prv = delete $m{prv};

	my $rest = join ",", map { "$_ = '$m{$_}'"} keys %m;
	$rest = "," . $rest if $rest;

	print "insert into set_osmp_range set range_from = '$from', range_to = '$to', prv = '$prv'$rest;\n";

    }

}