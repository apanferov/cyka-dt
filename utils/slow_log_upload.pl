#!/usr/bin/perl

use strict;
use warnings;

while (<>) {
    # 2007/03/18 03:12:57 INFO GOT RESPONSE FOR DOWN96519/MSG50541942/SVC493/DOWNLOAD_TIME(0.085967)
    if (/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}) INFO GOT RESPONSE FOR DOWN(\d+)\/MSG(\d+)\/SVC(\d+)\/DOWNLOAD_TIME\(((?:\d|\.)+)\)/) {
	print "insert into tr_service_time_log values ( $9, $10, '$1-$2-$3', 'success', $8);\n";
    }

    # 2007/03/18 03:21:30 ERROR TRANSPORT ERROR FOR DOWN96910/MSG50538636/ERROR(TIMEOUT)/DOWN_TIME(20.08264))
    if (/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}) ERROR TRANSPORT ERROR FOR DOWN(\d+)\/MSG(\d+)\/ERROR\(([^)]+)\)\/DOWN_TIME\(((?:\d|\.)+)\)\/SVC(\d+)/) {
	print "insert into tr_service_time_log values ( $11, $10, '$1-$2-$3', '$9', $8);\n";
    }
}

__END__

update tr_service_time_log, tr_billing set
tr_service_time_log.service_id = tr_billing.service_id where
tr_service_time_log.service_id = 0 and tr_service_time_log.msg_id =
tr_billing.inbox_id and tr_billing.outbox_id = 0;
