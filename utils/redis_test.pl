#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper; $Data::Dumper::Terse = 1;
use lib '../lib';

use Redis;

my $redis = Redis->new();
my $key = 'mks_2316';
my $k = {};
foreach ($redis->hvals($key)) {
	$k->{$_}++;
}
print Dumper($k);
print join(',', sort keys %{$k})."\n";


print "Done\n";

__END__
