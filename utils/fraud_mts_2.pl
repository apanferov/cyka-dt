#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use Spreadsheet::WriteExcel;
use MIME::Lite;
use IO::Scalar;

my $days_back = $ARGV[1];
my $kp = $ARGV[0];
my $operators = {'a1' => '105,128,138,175,176,177,178,179,180,181', 'mks' => '534'}->{$kp};
my $to = {'a1'  => 'v.islamov@alt1.ru,e.chumakov@alt1.ru,a.safonov@alt1.ru,a.sotnikov@alt1.ru,p.shabanova@alt1.ru,i.kulakov@alt1.ru',
       	  'mks' => 'v.islamov@alt1.ru,e.chumakov@alt1.ru,ms@moscomsvjaz.ru,ag@moscomsvjaz.ru'
  	   }->{$kp};
die 'usage: perl fraud_mts_2.pl <kp name> <days back>' unless ($days_back and $operators and $to);

my $from_date = POSIX::strftime("%Y-%m-%d", localtime(time()-86400*$days_back));
my $to_date = POSIX::strftime("%Y-%m-%d", localtime(time()-86400));
Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();
my $sts = $db->{db}->selectall_arrayref("select distinct abonent from cyka_payment force index (idx_operator_id_num) where date between '$from_date' and '$to_date 23:59:59' and operator_id in ($operators) order by 1");

my $data = "msisdn\n";

foreach (@$sts) {
	$data .= "$_->[0]\n"
}

my $msg = MIME::Lite->new( From => 'asg-info@alt1.ru',
	To => $to,
#						   To => 'v.islamov@alt1.ru,A.Safonov@alt1.ru,E.Chumakov@alt1.ru,a.sotnikov@alt1.ru',
						   Subject => "MTS abonents from $from_date to $to_date",
						   Type => 'text/plain',
						   Data => "MTS abonents from $from_date to $to_date");
$msg->attach( Type => 'text/comma-separated-values',
			  Encoding => 'base64',
			  Data => $data,
			  Filename => "report_$kp.csv");
raw_sendmail($msg->as_string);

