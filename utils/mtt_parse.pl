#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
use HTML::Parser;
use FindBin;
use Log::Log4perl;


use lib "$FindBin::RealBin/../lib";
use Disp::Config;

my $dbh;


=begin sql

CREATE TABLE set_mtt1_region
(id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(255) NOT NULL,
 UNIQUE KEY (name));

CREATE TABLE set_mtt1_operator
(id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(255) NOT NULL,
 cuka_operator_id INTEGER REFERENCES set_operator_t(id),
 UNIQUE KEY (name));

CREATE TABLE set_mtt1_base
(id INTEGER PRIMARY KEY AUTO_INCREMENT,
 num_start VARCHAR(20) NOT NULL,
 num_end VARCHAR(20) NOT NULL,
 date_mtt DATE NOT NULL,
 date_parse DATE NOT NULL,
 operator_id INTEGER NOT NULL,
 region_id INTEGER NOT NULL,
 INDEX(num_start, num_end)
);

=cut
sub insert_base_record {
	my ( $rec ) = @_;
	die "Invalid record".Dumper($rec) unless $rec->{def} =~ /^(\d+)-(\d+)$/;
	my ( $ds, $de ) = ( $1, $2 );

	die "Invalid date". Dumper($rec) unless $rec->{date} =~ /^(\d{4})\.(\d{2})\.(\d{2})$/;

	my $date = "$1-$2-$3";

	eval {
		$dbh->begin_work;
		$dbh->do("insert into set_mtt1_base (num_start, num_end, date_mtt, operator_id, region_id, date_parse) values (?, ?, ?, ?, ?, date(now()))", undef,
				 $ds, $de, $date, ensure_operator($rec->{operator}), ensure_region($rec->{region}) );
	};
	if ( $@ ) {
		$dbh->rollback;
	} else {
		$dbh->commit;
	}
}

sub ensure_db_row {
    my ( $table, $field, $value ) = @_;

    $value =~ s/^(\s|\xA0)+//;
    $value =~ s/(\s|\xA0)+$//;

    my ( $id ) = $dbh->selectrow_array("select id from $table where $field = ?", undef, $value);

    unless ( $id ) {
        $dbh->do("insert into $table ($field) values (?)", undef, $value);
        $id = $dbh->{"mysql_insertid"};
    }
    return $id;
}

sub ensure_operator {
	my ( $name ) = @_;
	return ensure_db_row( set_mtt1_operator=> name => $name );
}

sub ensure_region {
	my ( $name ) = @_;
	return ensure_db_row( set_mtt1_region => name => $name);
}


my @mtt;
my $state = 'none';
my $current;

sub start {
	my ( $tag, $attrs ) = @_;

	if ( $tag eq 'tr' ) {
		$state = 'tr';
		$current = { };
		return;
	}

	if ( $state eq 'tr' and $tag eq 'td' and exists($attrs->{abbr})) {
		$state = "td_1";
		$current->{operator} = $attrs->{abbr};
		return;
	}

	if ( $state eq 'td_1' and $tag eq 'td' and exists($attrs->{abbr})) {
		$state = "td_2";
		$current->{region} = $attrs->{abbr};
		return;
	}

	if ( $state eq 'td_2' and $tag eq 'td' and exists($attrs->{abbr})) {
		$state = "td_3";
		return;
	}

	if ( $state eq 'td_3' and $tag eq 'td' and exists($attrs->{abbr})) {
		$state = 'td_4';
		$current->{def} = $attrs->{abbr};
		return;
	}

	if ( $state eq 'td_4' and $tag eq 'br') {
		$state = 'td_5';
		return;
	}

	if ( $state eq 'td_5' and $tag eq 'td' and exists($attrs->{abbr})) {
		$current->{date} = $attrs->{abbr};
		push @mtt, $current;
	}

	$state = 'none';
}

my $p = HTML::Parser->new( api_version => 3,
						   start_h => [\&start, 'tagname, attr'] );

$p->parse_file("/tmp/mtt.html");


Log::Log4perl->init("../etc/log.conf");
Config::init(config_file => "../etc/asg.conf");
$dbh = Config->dbh;

foreach ( @mtt ) {
	insert_base_record($_);
# 	search_clause($_);
}


sub search_clause {
	my ( $rec ) = @_;
	die "Invalid record".Dumper($rec) unless $rec->{def} =~ /^(\d+)-(\d+)$/;
	my ( $ds, $de ) = ( $1, $2 );

    print "or (abonent between 7$ds and 7$de) ";
}
