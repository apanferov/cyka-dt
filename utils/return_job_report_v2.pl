#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use Spreadsheet::WriteExcel;
use MIME::Lite;
use IO::Scalar;

my $id = $ARGV[0];
my $price_abonent = ($ARGV[1] && ($ARGV[1] eq 'nonds')) ? 'r.price_abonent/1.18' : 'r.price_abonent';
unless ($id) {
        print "Usage: return_job_report_v2.pl <job_id> [nonds]\n";
        exit 0;
}

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();

my $sts = $db->{db}->selectall_arrayref(<<EOF,undef,$id);
select
l.id,
l.abonent,
l.num,
l.data,
if(s.id in (1010,1646),'���������_RUR',if(r.partner_id=0,null,p.name)) as partner,
group_concat(distinct s.fullname separator ',') as services,
sum($price_abonent) as sum,
group_concat(distinct cast(if(r.inbox_id,r.inbox_id,null) as char) separator ',') as mo,
group_concat(distinct cast(if(not r.inbox_id,r.push_id,null) as char) separator ',') as mt
from
cyka_return_list l
left join cyka_return_t r on l.job_id=r.job_id and l.id=r.list_id
left join set_cyka_partner p on p.id=r.partner_id
left join set_service s on r.service_id=s.id
where l.job_id=?
group by 1,5;
EOF

my $data = "";
my $wbfh = new IO::Scalar \$data;
my $workbook = Spreadsheet::WriteExcel->new($wbfh);
my $worksheet = $workbook->add_worksheet(decode('cp1251','��������'));
$worksheet->write_string(0, 0, decode('cp1251','ID'));
$worksheet->write_string(0, 1, decode('cp1251','�������'));
$worksheet->write_string(0, 2, decode('cp1251','��'));
$worksheet->write_string(0, 3, decode('cp1251','����� ��������'));

$worksheet->write_string(0, 4, decode('cp1251','�������'));
$worksheet->write_string(0, 5, decode('cp1251','�������'));
$worksheet->write_string(0, 6, decode('cp1251','����� �� ����'));
$worksheet->write_string(0, 7, decode('cp1251','MO'));
$worksheet->write_string(0, 8, decode('cp1251','MT'));
$worksheet->set_column('A:A', 5);
$worksheet->set_column('B:B', 15);
$worksheet->set_column('C:C', 10);
$worksheet->set_column('D:D', 10);
$worksheet->set_column('E:E', 20);
$worksheet->set_column('F:F', 20);
$worksheet->set_column('G:G', 10);
$worksheet->set_column('H:H', 40);
$worksheet->set_column('I:I', 40);


my $h = {};
my $row = 1;
my $start_row = 1;
my $last = $sts->[0];
push(@$sts,[0]);
my $format = $workbook->add_format(valign  => 'vcenter', align => 'left');
foreach (@$sts) {
	$worksheet->write_string($row, 0, $_->[0]);
	$worksheet->write_string($row, 1, $_->[1]);
	$worksheet->write_string($row, 2, $_->[2]);
	$worksheet->write_string($row, 3, $_->[3]);
#	if ($last->[0] != $_->[0]) {
#		if (($row-1) == $start_row) {
#			$worksheet->write_string($start_row, 0, $last->[0]);
#			$worksheet->write_string($start_row, 1, $last->[1]);
#			$worksheet->write_string($start_row, 2, $last->[2]);
#			$worksheet->write_string($start_row, 3, $last->[3]);
#		} else {
#			$worksheet->merge_range($start_row,0,$row-1,0, $last->[0], $format);
#			$worksheet->merge_range($start_row,1,$row-1,1, $last->[1], $format);
#			$worksheet->merge_range($start_row,2,$row-1,2, $last->[2], $format);
#			$worksheet->merge_range($start_row,3,$row-1,3, $last->[3], $format);
#		}
#		$start_row = $row;
#	}

	$worksheet->write_string($row, 4, decode('cp1251',$_->[4]));
	$worksheet->write_string($row, 5, decode('cp1251',$_->[5]));
	$worksheet->write_string($row, 6, $_->[6]);
	$worksheet->write_string($row, 7, $_->[7]);
	$worksheet->write_string($row, 8, $_->[8]);
	$row++;
	@$last = @$_;
	
}

$workbook->close;
my $msg = MIME::Lite->new( From => 'v.islamov@alt1.ru',
						   To => 'v.islamov@alt1.ru',
						   Subject => "Returns report V2 (Job $id)",
						   Type => 'text/plain',
						   Data => "Report");
$msg->attach( Type => 'application/vnd.ms-excel',
			  Encoding => 'base64',
			  Data => $data,
			  Filename => "returns_report_v2_$id.xls");
raw_sendmail($msg->as_string);



