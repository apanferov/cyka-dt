#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use Spreadsheet::WriteExcel;
use MIME::Lite;
use IO::Scalar;

my $id = $ARGV[0];
my $price_abonent = ($ARGV[1] && ($ARGV[1] eq 'nonds')) ? 'r.price_abonent/1.18' : 'r.price_abonent';
unless ($id) {
        print "Usage: return_job_report.pl <job_id> [nonds]\n";
        exit 0;
}

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();

my $sts = $db->{db}->selectall_arrayref(<<EOF,undef,$id,$id,$id,$id,$id,$id);
select
min(if(q.job_id=?,q.id,null)) as id,
q.abonent as msisdn,
q.num as shortcode,

round(sum(if(q.job_id=?, q.data, 0)),2) as return_sum,
round(sum(if(q.job_id<?, q.data, 0)),2) as previous_return_sum,
group_concat(distinct if(q.job_id<?,cast(q.date_end as char),null) separator ',') as return_dates,
round(if(sum(q.sum) is null,0,sum(q.sum)),2) as payment_sum,
round(if(sum(q.sum) is null,0,sum(q.sum)) - sum(if(q.job_id<=?, q.data, 0)),2) as difference,
group_concat(distinct q.partners separator ',') as partners

from
 (select
 l.job_id,
 l.id,
 l.abonent,
 l.num,
 replace(l.data,',','.') as data,
 l.date_end,
 sum($price_abonent) as sum,
 group_concat(distinct if(r.service_id in (1010,1644,1646),'���������_RUR',if(r.partner_id=0,null,p.name)) separator ',') as partners
 from cyka_return_list l
 left join cyka_return_t r on l.job_id=r.job_id and l.id=r.list_id
 left join set_cyka_partner p on p.id=r.partner_id
 group by 1,2,3,4,5,6) q
group by 2,3
having sum(if(q.job_id=?,1,0))>0
order by 1;
EOF

my $data = "";
my $wbfh = new IO::Scalar \$data;
my $workbook = Spreadsheet::WriteExcel->new($wbfh);
my $worksheet = $workbook->add_worksheet(decode('cp1251','��������'));
$worksheet->write_string(0, 0, decode('cp1251','ID'));
$worksheet->write_string(0, 1, decode('cp1251','�������'));
$worksheet->write_string(0, 2, decode('cp1251','��'));
$worksheet->write_string(0, 3, decode('cp1251','����� �������� (A)'));
$worksheet->write_string(0, 4, decode('cp1251','����� ���������� ��������� (B)'));
$worksheet->write_string(0, 5, decode('cp1251','���� ���������� ���������'));
$worksheet->write_string(0, 6, decode('cp1251','����� �������� �� ���� (C)'));
$worksheet->write_string(0, 7, decode('cp1251','������� (C-A-B)'));
$worksheet->write_string(0, 8, decode('cp1251','��������'));
$worksheet->set_column('A:A', 5);
$worksheet->set_column('B:B', 15);
$worksheet->set_column('C:C', 10);
$worksheet->set_column('D:D', 10);
$worksheet->set_column('E:E', 10);
$worksheet->set_column('F:F', 20);
$worksheet->set_column('G:G', 10);
$worksheet->set_column('H:H', 10);
$worksheet->set_column('I:I', 40);


my $h = {};
my $row = 1;
foreach (@$sts) {
	my ($return_sum,$presious_return_sum) = @$_;
	$worksheet->write_string($row, 0, $_->[0]);
	$worksheet->write_string($row, 1, $_->[1]);
	$worksheet->write_string($row, 2, $_->[2]);
	$worksheet->write_string($row, 3, $_->[3]);
	$worksheet->write_string($row, 4, $_->[4]);
	$worksheet->write_string($row, 5, $_->[5]);
	$worksheet->write_string($row, 6, $_->[6]);
	$worksheet->write_string($row, 7, $_->[7]);
	$worksheet->write_string($row, 8,  decode('cp1251',$_->[8]));
	$row++;
}

$workbook->close;
my $msg = MIME::Lite->new( From => 'v.islamov@alt1.ru',
						   To => 'v.islamov@alt1.ru',
						   Subject => "Returns report (Job $id)",
						   Type => 'text/plain',
						   Data => "Report");
$msg->attach( Type => 'application/vnd.ms-excel',
			  Encoding => 'base64',
			  Data => $data,
			  Filename => "returns_report_$id.xls");
raw_sendmail($msg->as_string);



