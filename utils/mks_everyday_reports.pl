#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use MIME::Lite;
use IO::Scalar;

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();

sub notify_about_the_traffic_everyday_changing {
	my $query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds),0) as operator_income_percent,
  ifnull(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds),0) as a1_income_percent,
  ifnull(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds),0) as a1_clean_income_percent,

  ifnull(sum(t1.operator_income__rur_without_nds),0) as operator_income_cnt,
  ifnull(sum(t1.a1_income__rur_without_nds),0) as a1_income_cnt,
  ifnull(sum(t1.a1_clean_income__rur_without_nds),0) as a1_clean_income_cnt
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
    GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
    GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_operator op
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  op.id=t1.operator_id and
  op.parent_id = 4 AND
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	my $query1 = "SELECT op.fullname as operator_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	my $query2 = "SELECT ".$query;
  my $rt = $db->{db}->selectall_arrayref($query1, { Slice => {} });
	my $all = $db->{db}->selectrow_hashref($query2, { Slice => {} });
	my $d1 = POSIX::strftime("%Y-%m-%d", localtime(time()-1*86400));
#	my $d2 = POSIX::strftime("%Y-%m-%d", localtime(time()-2*86400));
	my $res = "<h3>���������� ����� �� ���������� ������� � ������� ���<br>�� $d1</h3>\n";
	$res .= "<h4>�� ����������</h4>\n";
	$res .= "<table border=1>
		<tr class=header>
			<td>��������</td>
			<td>% �� ����</td>
			<td>��������� ������ ���������, %</td>
			<td>����� ���������, RUB</td>
			<td>����� ���, RUB</td>
			<td>����� ��� � ������ ��������� �������, RUB</td>
	 	</tr>\n";
	if ($rt) {
    my $class = "";
		$class = " style='color:red'" if ($all->{operator_income_percent} <= -2);
		$class = " style='color:blue'" if ($all->{operator_income_percent} >= 2);
		$res .=
      "<tr class='dark' align='right'>".
			"  <td align='left'><b>����� �� ���� ����������</b></td>".
			"  <td width='10%'><b>100.000</b></td>".
			"  <td width='10%'$class><b>".(($all->{operator_income_percent} > 0) ? '+' : '').sprintf('%.3f', $all->{operator_income_percent})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{operator_income_cnt})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{a1_income_cnt})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{a1_clean_income_cnt})."</b></td>".
			"</tr>\n";
    foreach my $row (@$rt) {
      my $class = "";
			$class = " style='color:red'" if ($row->{operator_income_percent} <= -2);
			$class = " style='color:blue'" if ($row->{operator_income_percent} >= 2);
  		$res .=
        "<tr align='right'>".
				"  <td align='left'><b>$row->{operator_name}</b></td>".
				"  <td><b>".(($all->{'a1_clean_income__rur_without_nds'} == 0) ? 'undef' : sprintf('%.3f', $row->{'a1_clean_income__rur_without_nds'} * 100 / $all->{'a1_clean_income__rur_without_nds'}))."</b></td>".
				"  <td$class><b>".(($row->{operator_income_percent} > 0) ? '+' : '').sprintf('%.3f', $row->{operator_income_percent})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{operator_income_cnt})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{a1_income_cnt})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{a1_clean_income_cnt})."</b></td>".
				"</tr>\n";
		}
	}
	$res .= "</table>\n";

  return $res;
}

sub notify_about_the_detail_traffic_everyday_changing {
	my $query =
" sum(t2.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds,
  ifnull(100 * ( sum(t1.operator_income__rur_without_nds) - sum(t2.operator_income__rur_without_nds) ) / sum(t2.operator_income__rur_without_nds),0) as operator_income_percent,
  ifnull(100 * ( sum(t1.a1_income__rur_without_nds) - sum(t2.a1_income__rur_without_nds) ) / sum(t2.a1_income__rur_without_nds),0) as a1_income_percent,
  ifnull(100 * ( sum(t1.a1_clean_income__rur_without_nds) - sum(t2.a1_clean_income__rur_without_nds) ) / sum(t2.a1_clean_income__rur_without_nds),0) as a1_clean_income_percent,

  ifnull(sum(t1.operator_income__rur_without_nds),0) as operator_income_cnt,
  ifnull(sum(t1.a1_income__rur_without_nds),0) as a1_income_cnt,
  ifnull(sum(t1.a1_clean_income__rur_without_nds),0) as a1_clean_income_cnt
FROM
  (
     SELECT
      t11.date as date,
      t11.service_id as service_id,
      t11.operator_id as operator_id,
      sum(t11.insms_count) as insms_count,
      sum(t11.outsms_count) as outsms_count,
      sum(t11.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t11.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t11.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 1 DAY)
      ) t11
    GROUP BY t11.service_id,t11.date,t11.operator_id
  ) t1,
  (
    SELECT
      t22.date as date,
      t22.service_id as service_id,
      t22.operator_id as operator_id,
      sum(t22.insms_count) as insms_count,
      sum(t22.outsms_count) as outsms_count,
      sum(t22.operator_income__rur_without_nds) as operator_income__rur_without_nds,
      sum(t22.a1_income__rur_without_nds) as a1_income__rur_without_nds,
      sum(t22.a1_clean_income__rur_without_nds) as a1_clean_income__rur_without_nds
    FROM
      (
        SELECT *
        FROM traffic_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
        UNION
        SELECT *
        FROM traffic_ext_furl_price
        WHERE date=DATE_SUB(CURDATE() ,INTERVAL 2 DAY)
      ) t22
    GROUP BY t22.service_id,t22.date,t22.operator_id
  ) t2,
  set_operator o,
  set_service_t op,
  set_sgroup_t sg
WHERE
  t2.date=DATE_SUB(t1.date,INTERVAL 1 DAY) AND
  o.id=t1.operator_id AND
  o.parent_id = 4 AND
  op.id=t1.service_id and
  op.sgroup_id=sg.id and
  t1.operator_id=t2.operator_id AND
  t1.service_id=t2.service_id";

	my $query1 = "SELECT sg.name as service_name,".$query." GROUP BY 1 ORDER BY 2 DESC";
	my $query2 = "SELECT ".$query;
  my $rt = $db->{db}->selectall_arrayref($query1, { Slice => {} });
	my $all = $db->{db}->selectrow_hashref($query2, { Slice => {} });
#	my $d1 = POSIX::strftime("%Y-%m-%d", localtime(time()-1*86400));
#	my $d2 = POSIX::strftime("%Y-%m-%d", localtime(time()-2*86400));
#	$res .= "<h3>���������� ����� �� ���������� ������� � ������� �� ������� ��������<h3><h4>�� $d1 � ��������� � $d2</h4><br/>\n";
	my $res = "<h4>�� ������� ��������</h4>\n";
	$res .= "<table border=1>
		<tr class=header>
			<td>������ ��������</td>
			<td>% �� ����</td>
			<td>��������� ������ ���������, %</td>
			<td>����� ���������, RUB</td>
			<td>����� ���, RUB</td>
			<td>����� ��� � ������ ��������� �������, RUB</td>
	 	</tr>\n";
	if ($rt) {
    my $class = "";
		$class = " style='color:red'" if ($all->{operator_income_percent} <= -2);
		$class = " style='color:blue'" if ($all->{operator_income_percent} >= 2);
		$res .=
      "<tr class='dark' align='right'>".
			"  <td align='left'><b>����� �� ���� ��������</b></td>".
			"  <td width='10%'><b>100.000</b></td>".
			"  <td width='10%'$class><b>".(($all->{operator_income_percent} > 0) ? '+' : '').sprintf('%.3f', $all->{operator_income_percent})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{operator_income_cnt})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{a1_income_cnt})."</b></td>".
			"  <td width='16%'><b>".sprintf('%.2f', $all->{a1_clean_income_cnt})."</b></td>".
			"</tr>\n";
    foreach my $row (@$rt) {
      my $class = "";
			$class = " style='color:red'" if ($row->{operator_income_percent} <= -2);
			$class = " style='color:blue'" if ($row->{operator_income_percent} >= 2);
  		$res .=
        "<tr align='right'>".
				"  <td align='left'><b>$row->{service_name}</b></td>".
				"  <td><b>".(($all->{'a1_clean_income__rur_without_nds'} == 0) ? 'undef' : sprintf('%.3f', $row->{'a1_clean_income__rur_without_nds'} * 100 / $all->{'a1_clean_income__rur_without_nds'}))."</b></td>".
				"  <td$class><b>".(($row->{operator_income_percent} > 0) ? '+' : '').sprintf('%.3f', $row->{operator_income_percent})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{operator_income_cnt})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{a1_income_cnt})."</b></td>".
				"  <td><b>".sprintf('%.2f', $row->{a1_clean_income_cnt})."</b></td>".
				"</tr>\n";
		}
	}
	$res .= "</table>\n";

  return $res;
}

my $data = "<html>
	<head>
		<title></title>
		<style>
			body
			{
				color: #000;
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 10px;
			}
			.header
			{
				background-color:#ccc;
			}
		</style>
	</head>
	<body>\n";
$data .= notify_about_the_traffic_everyday_changing();
$data .= notify_about_the_detail_traffic_everyday_changing();
$data .= "</body>\n";

my $msg = MIME::Lite->new(
  From => 'cyka_info@alt1.ru',
  To => 'k.safonov@alt1.ru,d.tsedrik@alt1.ru,m.shabanov@alt1.ru,v.islamov@alt1.ru,a.panferov@alt1.ru',
  Subject => "���������� ����� �� ���������� ������� � ������� ���.",
  Type => 'text/html; charset=windows-1251',
  Data => $data
  );
raw_sendmail($msg->as_string);

__END__
