#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use MIME::Lite;
use IO::Scalar;

my $from_date = POSIX::strftime("%Y-%m-%d", localtime(time()-2*86400));
my $from_date_s = POSIX::strftime("%d-%m-%Y", localtime(time()-2*86400));
my $to_date = "$from_date 23:59:59";

Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();
my $sql = "select p.name as name,p.comment as comment,round(sum(c.partner_income_ex),2) as money,sum(c.count) as kol,c.partner_currency as currency from cyka_payment_furl c left join set_cyka_partner p on c.partner_id = p.id where c.date between '$from_date' and '$to_date' and c.test_flag=0 and p.id in (5134,5116,5147,5171,5169,5177,5167) group by 1,5 having money > 0 order by 1";

my $sts = $db->{db}->selectall_arrayref($sql);

my $data = "";
foreach (@$sts) {
	my ($name,$comment,$money,$count,$currency) = @$_;
	my ($wm) = $comment =~ /(R\d+)/i;
	$money =~ s/\./\,/g;
	$data.="$wm;$money;$from_date_s �� ������� ($name)\n";
}

my $msg = MIME::Lite->new( From => 'Billing<billing@unitech.lv>',
						   To => 'pay@unitech.lv',
#						   To => 'v.islamov@alt1.ru',
						   Subject => "Partner's every day report $from_date",
						   Type => 'text/plain',
						   Data => "Partner's every day report");
$msg->attach( Type => 'application/vnd.ms-excel',
			  Encoding => 'base64',
			  Data => $data,
			  Filename => "payment $from_date.csv");
raw_sendmail($msg->as_string);



