#!/usr/bin/perl
use strict;
use warnings;
use locale;
use encoding 'UTF-8';
use FindBin;
use lib "$FindBin::RealBin/../lib";

use CGI::Carp qw/fatalsToBrowser/;
use CGI;
use Log::Log4perl;
use Data::Dumper;
use POSIX qw/strftime/;
use Spreadsheet::WriteExcel::Big;

use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;

my %status_names = ( 2 => 'Принято SMSC',
					 255 => 'Не доставлено',
					 total => 'Всего СМС',
					 11 => 'Принято SMSC (отчёт)',
					 12 => 'Уверенная доставка',
					 13 => 'Отклонено',
					 14 => 'Устарело',
					 15 => 'Не доставлено (отчёт)'
				   );

my @status_order = qw/2 11 12 13 14 15 255 total/;

Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

my $distribution_id = $ARGV[0];

die "Must specify distribution id" unless $distribution_id =~ /^\d+$/;

my $fn = "distribution_$distribution_id.xls";
$fn = '-' if $ARGV[1];

my $workbook = Spreadsheet::WriteExcel::Big->new($fn);

my $worksheet;

my %status_count;
my %abonent_count;
my %def_count;

my $sql = "select l.abonent, o.status from tr_distribution_list l join
tr_outsmsa o on o.outbox_id = l.outbox_id where distribution_id = ?";

my $sth = Config->dbh->prepare($sql);
$sth->execute($distribution_id);

while (my($abonent, $status) = $sth->fetchrow_array) {
	$status_count{$status}++;
	$status_count{total}++;

	$abonent_count{$abonent}{$status}++;
	$abonent_count{$abonent}{total}++;

	if ( $abonent =~ /^7(\d{3})/ ) {
		$def_count{$1}{$status}++;
		$def_count{$1}{total}++;
	}
}

# print Dumper(\%def_count);

# Сводная статистика
warn "Total\n";
$worksheet =  $workbook->add_worksheet("Общее");
$worksheet->set_column('A:A', 16);

for (my $i = 0; $i <= $#status_order; $i++) {
	$worksheet->write_string($i, 0, $status_names{$status_order[$i]});
	$worksheet->write($i, 1, $status_count{$status_order[$i]});
}


# Статистика по абонентам
my $batch_num = 0;
my $current_batch_count = 0;

for my $abonent (sort { $a <=> $b } keys %abonent_count) {

	if ($batch_num == 0 or $current_batch_count > 65000 ) {
		$batch_num++;
		$current_batch_count = 0;

		warn "Abonent - $batch_num\n";

		$worksheet =  $workbook->add_worksheet("По абонентам - $batch_num");
		$worksheet->write_string(0, 0, "Номер абонента");
		$worksheet->write_string(0, 1, "Статус доставки");
		$worksheet->write_string(0, 2, "Количество отправленных СМС");

		$worksheet->set_column('A:A', 20);
		$worksheet->set_column('B:B', 23);
		$worksheet->set_column('C:C', 28);
	}

	for (sort grep { $_ ne 'total' } keys %{$abonent_count{$abonent}}) {
		$current_batch_count++;
		$worksheet->write_string($current_batch_count, 0, $abonent);
		$worksheet->write_string($current_batch_count, 1, $status_names{$_});
		$worksheet->write($current_batch_count, 2, $abonent_count{$abonent}{$_});
	}
}

# По DEF-кодам
warn "DEF\n";
my @column_index = qw/A B C D E F G H I J K L M N O P Q/;
$current_batch_count = 0;

$worksheet =  $workbook->add_worksheet("По кодам");

$worksheet->write_string(0, 0, "Первые 3 цифры номера");
$worksheet->set_column('A:A', 22);

for (my $i = 0; $i <= $#status_order; $i++) {
	$worksheet->write_string(0, $i+1, $status_names{$status_order[$i]});
	$worksheet->set_column($column_index[$i+1].':'.$column_index[$i+1], 22);
}

for my $def (sort { $a <=> $b } keys %def_count ) {
	$current_batch_count++;

	$worksheet->write_string($current_batch_count, 0, $def);
	for (my $i = 0; $i <= $#status_order; $i++) {
		$worksheet->write($current_batch_count, $i+1, $def_count{$def}{$status_order[$i]} || 0);
	}
}

$workbook->close;