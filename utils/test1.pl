#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::Config;
use Disp::Utils;
use Disp::AltDB;
use Data::Dumper;
use IO::File;
use Time::HiRes qw(gettimeofday tv_interval usleep);

Config::init(config_file => '../etc/asg.conf', skip_db_connect => 1);
my $db = new Disp::AltDB();

my $f = new IO::File("test_data",'r');

my $t0 = [gettimeofday];
while (my $line = $f->getline) {
	$line =~ /^(\d+)\t(\d+)$/;
	$db->get_value('select trusted_level(?)',undef,$1);
}
my $elapsed = tv_interval($t0);
print "$elapsed\n";
$f->close;

exit;

