#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my $path = $ARGV[0];

my $inventory;

{
	local $/;
	local @ARGV = ( "$path/_darcs/inventory" );
	$inventory = <>;
}

my @inventory_patches = grep {$_ !~ /^[\s\n]*$/} split /\[|\]|\]\n\[/, $inventory;
my %inventory;


$inventory{"[$_]"} = 1 for @inventory_patches;

opendir my $dh, "$path/_darcs/patches/";

while (local $_ = readdir $dh) {
	if (/^\d{14}-\w{5}-\w{40}\.gz$/) {
		my $filename = $_;

		open my $fh, "-|", "zcat", "$path/_darcs/patches/$filename";

		my $info = "";
		while (local $_ = <$fh>) {
			$info .= $_;
			last if /\]/;
		}

		$info =~ s/\].*$/]/s;

		if ($inventory{$info}) {
			print $filename . "\n";
		}
	}
}

# foreach my $ip (@inventory_patches) {
# 	my ( $desc, $author_and_ts ) = split /\n/, $ip;
# 	my ( $author, $ts ) = split /\*\*/, $author_and_ts;

	


# }
