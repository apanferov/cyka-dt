#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use POSIX;
use Data::Dumper;


sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

	my ($division, $src_table, $dst_table) = @ARGV[0,1,2];

	my ( $min_id ) = Config->dbh->selectrow_array("select min(id) from $src_table");
	my ( $max_id ) = Config->dbh->selectrow_array("select max(id) from $src_table");


	my %diff;
	for my $tab ($src_table, $dst_table) {
		$diff{$_}++ for map { $_->{Field} } @{Config->dbh->selectall_arrayref("desc $tab", {Slice=>{}})};
	}
	my @common_columns = grep { $diff{$_} == 2 } keys %diff;

	my $min_div = POSIX::floor($min_id / $division);
	my $max_div = POSIX::floor($max_id / $division);

	for (my $i = $max_div;$i >= $min_div; $i--) {
		print "insert ignore into $dst_table(@{[join(', ', @common_columns)]}) select @{[join(', ', @common_columns)]} from $src_table where id between @{[$i*$division]} and @{[($i+1)*$division]};\nselect $i, $min_div, $max_div, sleep(120);\n";
		# last;
	}
}

main();
