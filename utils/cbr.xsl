<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='text'/>
<xsl:template match="/">
<xsl:for-each select="//tr[@bgcolor='#ffffff']">
replace into exchange_rate set date=now(), currency=lower('<xsl:value-of select='substring(td[2],3)'/>'),price=<xsl:value-of select='translate(td[5],",",".")'/> / <xsl:value-of select='td[3]'/>;
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
