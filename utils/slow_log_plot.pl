#!/usr/bin/perl
use strict;
use warnings;
use Time::Local;

while ( <> ) {
    # 2007/03/18 03:12:57 INFO GOT RESPONSE FOR DOWN96519/MSG50541942/SVC493/DOWNLOAD_TIME(0.085967)
    if (/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}) INFO GOT RESPONSE FOR DOWN(\d+)\/MSG(\d+)\/SVC(\d+)\/DOWNLOAD_TIME\(((?:\d|\.)+)\)/) {
		my $tm = timelocal $6, $5, $4, $3, $2 -1 , $1;
		$tm -= 1191096000;
		$tm /= 86400;
		print "$tm\t$10\n";
    }

    # 2007/03/18 03:21:30 ERROR TRANSPORT ERROR FOR DOWN96910/MSG50538636/ERROR(TIMEOUT)/DOWN_TIME(20.08264))
    if (/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}) ERROR TRANSPORT ERROR FOR DOWN(\d+)\/MSG(\d+)\/ERROR\(([^)]+)\)\/DOWN_TIME\(((?:\d|\.)+)\)\/SVC(\d+)/) {
		my $tm = timelocal $6, $5, $4, $3, $2 - 1, $1;
		$tm -= 1191096000;
		$tm /= 86400;
		print "$tm\t$10\n";
    }
}
