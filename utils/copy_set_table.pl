$table = shift @ARGV;
$select_condition = shift @ARGV;
@new_data = @ARGV;

unless ($table and $select_condition and scalar @new_data) {
	print "Usage: copy_set_table.pl <table_name> <select_condition> <new assignment> <new assignment>...\n";
	exit;
}

$new_data=join(',',@new_data);
$delete_condition=join(' and ',@new_data);

print <<EOF;
drop table if exists set_tmppp;
create temporary table set_tmppp select * from $table where $select_condition;
alter table set_tmppp modify column id int;
update set_tmppp set id=null;
update set_tmppp set $new_data;
delete from $table where $delete_condition;
insert into $table select * from set_tmppp;
drop table set_tmppp;
EOF
