#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI;
use Encode;
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny;
use Spreadsheet::WriteExcel;
use MIME::Lite;
use IO::Scalar;


Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();

my $smsc = $db->{db}->selectall_hashref("select id,fullname from set_smsc where in_commerce",'id');
my $sts = [];
Disp::AltDB::dbh(undef, sub {
		my $tmp = $_[0]->{db}->selectall_arrayref("select unix_timestamp(date) as xdate,pr_name,pr_state from tr_process_state_log where date between subdate(date(now()),interval 7 day) and date(now()) and (pr_state='RUN' or pr_state_old='RUN') order by 1");
		push @$sts, $_ foreach (@$tmp);
	});

my $data = "";
my $wbfh = new IO::Scalar \$data;
my $workbook = Spreadsheet::WriteExcel->new($wbfh);
my $worksheet = $workbook->add_worksheet(decode('cp1251','������'));
$worksheet->write_string(0, 0, decode('cp1251','�'));
$worksheet->write_string(0, 1, decode('cp1251','��'));
$worksheet->write_string(0, 2, decode('cp1251','��������'));
$worksheet->write_string(0, 3, decode('cp1251','������'));
$worksheet->set_column('A:A', 20);
$worksheet->set_column('B:B', 20);
$worksheet->set_column('C:C', 10);
$worksheet->set_column('D:D', 40);

my $h = {};
my $row = 1;
foreach (sort {$a->[0] <=> $b->[0]}@$sts) {
	my ($time,$name,$state) = @$_;
	if ($name =~ m/^[a-zA-Z_]*(\d+)$/i) {
        my $id = $1;
		if (($state eq 'RUN') and $h->{$id}{state} and ($h->{$id}{state} ne 'RUN')) {
			$h->{$id}{start} = $time;
			if ($h->{$id}{stop}
				and ($h->{$id}{start} > $h->{$id}{stop} + 3600)
				and exists($smsc->{$id})) {
				$worksheet->write_string($row, 0, POSIX::strftime("%D %T", localtime($h->{$id}{stop})));
                $worksheet->write_string($row, 1, POSIX::strftime("%D %T", localtime($h->{$id}{start})));
                $worksheet->write_string($row, 2, int(($h->{$id}{start}-$h->{$id}{stop})/3600).decode('cp1251'," ���(��)"));
                $worksheet->write_string($row, 3, decode('cp1251',"�� ���� ����� � $smsc->{$id}{fullname}"));
				$row++;
			}
		}
		elsif ($state ne 'RUN') {
			$h->{$id}{stop} = $time;
		}
		$h->{$id}{state} = $state;
	}
}

$workbook->close;
my $msg = MIME::Lite->new( From => 'a.lebedeff@alt1.ru',
						   To => 'v.islamov@alt1.ru',
						   Subject => "Process status report",
						   Type => 'text/plain',
						   Data => "Process status report");
$msg->attach( Type => 'application/vnd.ms-excel',
			  Encoding => 'base64',
			  Data => $data,
			  Filename => 'report.xls');
raw_sendmail($msg->as_string);



