#!/usr/bin/perl
use strict;
use locale;
use File::Find;
use Test::Harness;
use Data::Dumper;

sub wanted;

my @files;

File::Find::find({wanted => \&wanted}, $ARGV[0] || '.');
$ENV{PERL5LIB} = '../lib';
use lib '../lib';
runtests(sort @files);

exit;

sub wanted {
    my ($dev,$ino,$mode,$nlink,$uid,$gid);

    return unless (($dev,$ino,$mode,$nlink,$uid,$gid) = lstat($_));
	return unless -f _;
    return unless /^.*\.t\z/s;
	return if $File::Find::name =~ /_darcs/;

	push @files, $File::Find::name;
}



