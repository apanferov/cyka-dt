#!/bin/sh
DATE=`perl -MDate::Manip -e "print UnixDate(DateCalc('today','- 1 day'), '%Y-%m-%d')"`
cd /home/asg/utils/ && time ./make_database_cutoff.pl ../etc/cutoff.conf ../etc/db_war_alt5_DANGER.conf ../etc/db_dev_alt5_new.conf $DATE $DATE && mv /home/asg/backup/current.sql /home/asg/backup/backup-$DATE.sql && bzip2 /home/asg/backup/backup-$DATE.sql
