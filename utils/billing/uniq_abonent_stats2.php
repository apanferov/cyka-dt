<?php
ini_set('display_errors',1);
$htdocss = realpath(dirname(__FILE__).'/../../htdocss') . '/';
chdir($htdocss);
include $htdocss.'config.php';
include $htdocss.'db.php';
chdir(dirname(__FILE__));

function nm($num) {
  return $num<0 ? 0 : $num;
}

//$DB = $ARDB;
$link = new db($DB);
$mysql2 = "mysql -h172.16.200.39 -P3306 --user=$DB[user] --password=$DB[password] $DB[name] ";

$service_ids = array(427  => array(427),
                     206  => array(206),
                     119  => array(119),
                     1282 => array(1282,645),
                     1231 => array(1231,17,398),
                     1230 => array(1230,21,392),
                     1229 => array(1229,25),
                     1228 => array(1228,24),
                     1227 => array(1227,23),
                     1226 => array(1226,18,401),
                     1225 => array(1225,13,393),
                     1224 => array(1224,22,399),
                     1222 => array(1222,12,397),
                     1221 => array(1221,58),
                     1220 => array(1220,34,395),
                     1219 => array(1219,14,394),
                     1218 => array(1218,11,405),
                     1217 => array(1217,19),
                     930  => array(930,33,404,866,1223),
                     20   => array(20),
                    );
$operator_ids = array(106);
$string_operator_ids = join(',',$operator_ids);
$months = array();
$date = '2007-01-01';
$lastmonth = '';
while ($date < '2009-03-01') {
  if ($date < '2009-01-01') {
    $months[] = $lastmonth = $date;
    $date = db::selectField("SELECT DATE_ADD('$date',INTERVAL 1 MONTH)");
    continue;
  }
  $end = db::selectField("SELECT DATE_SUB(DATE_ADD('$date',INTERVAL 1 MONTH),INTERVAL 1 SECOND)");

  echo "processing $date\t$end\n";

  #1 UNIQUE USERS BY MONTH
  foreach ($service_ids as $serv => $ids) {
    db::query("SELECT 1");
    echo "make $serv for $date\n";
    $ids = join(',',$ids);
    `$mysql2 -e "SELECT distinct(abonent) FROM tr_billing WHERE date between '$date' and '$end' and service_id in ($ids) and operator_id in ($string_operator_ids)" > part1_{$serv}_{$date}`;
    `sort part1_{$serv}_{$date} -o part1_{$serv}_{$date}`;
    /*if ($months) {
      # CALC UNIQUE USERS BY ALL PREVIOUS MONTHS
      $name = "part1_{$serv}_";
      $names = "";
      foreach ($months as $month)
        $names .= " ".$name.$month;
      `cat $names | sort -u > part3help_{$serv}_$date`;
    } else {
      `echo "abonent" > part3help_{$serv}_$date`;
    }*/
  }
  if ($months) {
    # CALC UNIQUE USERS BY ALL SERVICES IN PREVIOUS MONTHS
    `cat part1_*_$lastmonth | sort -u > part2help_$date`;
    # CALC UNIQUE USERS BY ALL PREVIOUS MONTHS
    $name = "part1_*_";
    $names = "";
    foreach ($months as $month)
      $names .= " ".$name.$month;
    `cat $names | sort -u > part3help_$date`;
  } else {
    `echo "abonent" > part2help_$date`;
    `echo "abonent" > part3help_$date`;
  }


  $months[] = $lastmonth = $date;
  $date = db::selectField("SELECT DATE_ADD('$date',INTERVAL 1 MONTH)");
}

print "-------------------------------------------\n";
#1
foreach ($service_ids as $serv => $ids) {
  print "$serv\t";
  foreach ($months as $month) {
    print nm(trim(`cat part1_{$serv}_$month | wc -l`)-1)."\t";
  }
  //print (trim(`cat part1_{$serv}_* | sort -u | wc -l`)-1)."\t";
  print "\n";
}
print "All\t";
foreach ($months as $month) {
  print nm(trim(`cat part1_*_$month | sort -u | wc -l`)-1)."\t";
}
print "\n";
print "-------------------------------------------\n";
#2
foreach ($service_ids as $serv => $ids) {
  print "$serv\t";
  $lastmonth = '';
  foreach ($months as $month) {
    if ($lastmonth)
      print trim(`diff part1_{$serv}_$month part2help_$month | grep '<' | awk '{print $2}' | wc -l`)."\t";
    else
      print nm(trim(`cat part1_{$serv}_$month | wc -l`)-1)."\t";
    $lastmonth = $month;
  }
  print "\n";
}
print "All\t";
$lastmonth = '';
foreach ($months as $month) {
  if ($lastmonth) {
    `cat part1_*_$month | sort -u > tmp_1`;
    print trim(`diff tmp_1 part2help_$month | grep '<' | awk '{print $2}' | wc -l`)."\t";
  } else {
    print nm(trim(`cat part1_*_$month | sort -u | wc -l`)-1)."\t";
  }
  $lastmonth = $month;
}
`rm tmp_1`;
print "\n";
print "-------------------------------------------\n";
#3
foreach ($service_ids as $serv => $ids) {
  print "$serv\t";
  $lastmonth = '';
  foreach ($months as $month) {
    if ($lastmonth)
      print trim(`diff part1_{$serv}_$month part3help_$month | grep '<' | awk '{print $2}' | wc -l`)."\t";
    else
      print nm(trim(`cat part1_{$serv}_$month | wc -l`)-1)."\t";
    $lastmonth = $month;
  }
  print "\n";
}
print "All\t";
$lastmonth = '';
foreach ($months as $month) {
  if ($lastmonth) {
    `cat part1_*_$month | sort -u > tmp_1`;
    print trim(`diff tmp_1 part3help_$month | grep '<' | awk '{print $2}' | wc -l`)."\t";
  } else {
    print nm(trim(`cat part1_*_$month | sort -u | wc -l`)-1)."\t";
  }
  $lastmonth = $month;
}
`rm tmp_1`;
print "\n";
?>
