#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper; $Data::Dumper::Terse = 1;
use lib '../lib';

use HTTP::Request;
use LWP::UserAgent;
use URI::URL;
use JSON;

my $abonent = 79208718597;
my $resp = get_operator($abonent);
if (ref($resp) eq "HASH") {
	print $abonent.': '.Dumper($resp)."\n";
} else {
	print $abonent.': '.$resp."\n";
}

print "Done\n";

sub get_operator {
	my ($abonent) = @_;
	my $url = "http://178.63.66.211:9998/get_operator";
	my $user = 'qqq';
	my $pass = 'qqq';

	if (length($abonent) == 10) {
		$abonent = "7".$abonent;
	} elsif (length($abonent) == 11) {
		$abonent = "7".substr($abonent, 1);
	} else {
		return 0;
	}

	$url = url($url);
	my %req = (
		number => $abonent,
		# parent_id => 0,
	);
	$url->query_form(%req);

	# GET
	my $ua = LWP::UserAgent->new;
	$ua->timeout(5); # Ожидаем ответ (сек)
	my $request = HTTP::Request->new;
	$request->method("GET");
	$request->uri($url);
	$request->authorization_basic($user, $pass);
	my $result = '';
	eval {
		my $response = $ua->request($request);
		$result .= $response->content()."\n";
		$result = decode_json($response->content());
	};
	if ($@) {
		$result .= $@;
	}

	return $result;
}

__END__
