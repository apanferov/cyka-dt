#!/usr/bin/perl
# -*- encoding: cp1251 -*-
use strict;
use locale;
use lib '../lib';
use Config::Tiny;
use Disp::Config;
use Data::Dumper;
use DBI;

sub eachp(&@);
sub open_db($);

# ��������� ��������� ��������� ������ =)
unless (eachp { -f } @ARGV[0,1,2] and
		eachp { /^\d{4}-\d{2}-\d{2}/ } @ARGV[3,4]) {
	die "Usage: $0 cutoff.conf src_db.conf dst_db.conf cutoff_start_date cutoff_end_date";
}

my ( $c_cf, $in, $out ) = map { Config::Tiny->read($_)->{_} } @ARGV[0,1,2];

# ��������� ��������� ������ ������ � ��������� ���� ������.
if (lc($out->{host}) ne 'localhost') {
	die "Upload allowed only to localhost";
}

# ����������� � ����� ������
my ( $in_db ) = open_db $in;


my @cutoff_tables = split /\s+/, $c_cf->{cutoff_tables};
my @ignore_tables = split /\s+/, $c_cf->{ignore_tables};

my $d1 = $ARGV[3];
my $d2 = "$ARGV[4] 23:59:59";

my $tables = $in_db->selectall_arrayref("select * from information_schema.tables where table_schema = ? and table_type = 'BASE TABLE'",
										{ Slice => {}}, $in->{name});

$in_db->disconnect;

my %tables = map { $_->{TABLE_NAME} => $_ } @$tables;

# � ������� ����� ���� ����������� ����� �������, �� �� ������ �������
# ������ ������ �� ������� ������������.
@cutoff_tables = grep { exists $tables{lc $_} } @cutoff_tables;

# ���������� ������ �� �������� ������
delete $tables{$_} for @ignore_tables;

# ������ �� ���������� ������ ������������ ������ �������
delete $tables{lc $_} for @cutoff_tables;

print "-- Cutoff: ".join(", ", @cutoff_tables)."\n";
print "-- Ignore: ".join(", ", @ignore_tables)."\n";
print "-- Other : ".join(", ", keys %tables)."\n";

# ��������� ������������ ����������� ����� ��� ��������������� ���������
open OLDOUT, ">&STDOUT";
select(OLDOUT);
$| = 1;

# � � �������� ������������ ������ ���������� mysql � ���� ����������.
# open STDOUT, "|-", "mysql", "--host=$out->{host}", "--user=$out->{username}",
#   "--password=$out->{password}", "--force", $out->{name}
#   or die "can't open mysql: $!";

# open STDOUT, "> dump.out";
open STDOUT, "| tee $c_cf->{tee_file} | mysql --host=$out->{host} --user=$out->{username} --password=$out->{password} --force $out->{name}"
  or die "can't open mysql: $!";

select STDOUT;
$| = 1;

# ������ ��������� ���� ������
print OLDOUT "<h1>dumping database structure</h1>\n";
system("../bin/mysqldump_filter", "--default-character-set=cp1251", "--host=$in->{host}", "--quick", "--single-transaction", "--user=$in->{username}", "--password=$in->{password}", "-R", "--skip-triggers", "-d", $in->{name});

# ������ �� ��������� ���������� ������
print OLDOUT "<h1>dumping data from full-copy tables</h1>\n";
system("../bin/mysqldump_filter", "--default-character-set=cp1251", "--host=$in->{host}", "--quick", "--single-transaction", "--user=$in->{username}", "--password=$in->{password}", "-t", "--skip-triggers", $in->{name}, keys %tables)
  if keys %tables;

# � ���������� ������
print OLDOUT "<h1>dumping data from cut-off tables</h1>\n";
system("../bin/mysqldump_filter", "--default-character-set=cp1251", "--host=$in->{host}", "--quick", "--single-transaction", "--user=$in->{username}", "--password=$in->{password}", "-t", "--skip-triggers", "--where=date between '$d1' and '$d2'", $in->{name}, @cutoff_tables)
  if @cutoff_tables;

# � ������ ������ ����������� ��������.
print OLDOUT "<h1>dumping triggers</h1>\n";
print "DELIMITER \$\n";

$in_db = open_db $in;
my $triggers = $in_db->selectall_arrayref("select * from information_schema.triggers where trigger_schema = ?", {Slice => {}}, $in->{name});

foreach my $trigger (@$triggers) {
	print "DROP TRIGGER `$trigger->{TRIGGER_NAME}` \$\n";
	print "CREATE TRIGGER `$trigger->{TRIGGER_NAME}` $trigger->{ACTION_TIMING} $trigger->{EVENT_MANIPULATION} on `$trigger->{EVENT_OBJECT_TABLE}` FOR EACH ROW $trigger->{ACTION_STATEMENT}\$\n";
}

print "DELIMITER ;\n";

close STDOUT;
exit;

sub eachp(&@) {
	my ( $func, @arr ) = @_;

	foreach (@arr) {
		return 0 unless $func->();
	}
	return 1;
}

sub open_db($) {
	my ( $in ) = @_;

	my $in_db = DBI->connect("DBI:mysql:database=$in->{name};host=$in->{host}",
							 $in->{username}, $in->{password},
							 { RaiseError => 1 });
	return $in_db;
}

