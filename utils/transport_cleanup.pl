#!/usr/bin/perl
use lib '../lib';
use strict;
use locale;
use Disp::Config;
use Log::Log4perl;
use Date::Manip;
use IO::File;
use File::Basename;
use ConfigTinyWrapper;
use DBI;
use Data::Dumper;
use Disp::AltDB;


sub main {
	Log::Log4perl->init("../etc/log.conf");
	Config::init(config_file => "../etc/asg.conf");

	my $date = $ARGV[0];

	unless ($date) {
		$date = UnixDate(DateCalc(ParseDate("now"), "-1 weeks"), '%Y-%m-%d');
		print "Date not speciefied on command line, using date 1 weeks in past: $date\n";
	}

	my @tables = split /\s+/, Config->param( cleanup_tables => 'transport');

	unless (valid_date($date)) {
		print "Invalid date: $date\n";
		return;
	}

	print "Cleaning up on date $date\n";

	my @db_transport = map { Disp::AltDB->new($_) } values %{Config->section('db_transport')};
	my $db_big = Disp::AltDB->new(Config->section('database'));

	print "Connected to databases successfully\n";

	foreach my $table (@tables) {
		print "Checking table $table\n";

		my $c1 = 0;
		for (@db_transport) {
			$_->reconnect;
			$c1 += record_count($_->{db}, $table, $date)
		}

		$db_big->reconnect;
		my $c2 = record_count($db_big->{db}, $table, $date);

		print "=> TR: $c1, BIG: $c2\n";

		if ( $c1 == $c2) {
			print "=> Count matches, doing cleanup\n";

			my @sth;
			push @sth, $_->{db}->prepare("delete from $table where date between ? and ? limit 1000")
				for (@db_transport);

			print "=> Cleaning table: $table\n";
			my $cnt = delete_records(\@sth, day_start($date), day_end($date));
			print "=> Affected $cnt rows\n";

		} elsif ( $c1 !=0 ){
			my $id_field = (lc($table) eq 'tr_billing') ? 'concat(inbox_id,",",outbox_id) as id' : 'id';
			my $big_ids = $db_big->{db}->selectall_hashref("select $id_field from $table where date between ? and ?",'id',undef,day_start($date), day_end($date));
			my $tr_ids;
			for (@db_transport) {
				my $s = $_->{db}->selectall_hashref("select $id_field from $table where date between ? and ?",'id',undef,day_start($date), day_end($date));
				@$tr_ids{keys %$s} = values %$s;
			}
			print "=> Count mismatch, ignoring data for that day\n";
			print "=> Exist only on BIG:\n";
			for (keys %$big_ids) {
				print "B> $_\n" if (! exists($tr_ids->{$_}));
			}
			print "=> Exist only on TR:\n";
			for (keys %$tr_ids) {
				if (! exists($big_ids->{$_})) {
					my $condition = (lc($table) eq 'tr_billing') ? sprintf('inbox_id=%s and outbox_id=%s',split(/,/,$_)) : "id=$_";
					print "/*TRSQL*/ do asg_repl_log('$table','REPLACE','$condition');\n";
				}
			}

		} else {
			print "=> Count mismatch, ignoring data for that day\n";
		}

		my %other_dates;
		for my $db_transport (@db_transport) {
			my $sth = $db_transport->{db}->prepare("select date(date), count(*) from $table where date < ? group by 1");
			$sth->execute($date);
			while (my ($d, $c) = $sth->fetchrow_array) {
				$other_dates{$d} += $c;
			}
		}

		my $tmp;
		for (sort keys %other_dates) {
			unless ($tmp) {
				$tmp = 1;
				print "=> Also has following data for dates preciding $date\n";
			}
			print "===> $_ = $other_dates{$_}\n";
		}
	}

	@tables = split /\s+/, Config->param( cleanup2_tables => 'transport');
	foreach my $str (@tables) {
		my($tbl, $dt) = split /:/, $str;
		my($table, $field) = split /\./, $tbl;
		$field ||= 'date';

		$date = UnixDate(DateCalc(ParseDate("now"), "-$dt days"), '%Y-%m-%d') if $dt;

		unless (valid_date($date)) {
			print "Invalid date: $date\n";
			return;
		}

		my @sth = ();
		push @sth, $_->{db}->prepare("delete from $table where $field < ? limit 1000")
			for (@db_transport);

		print "=> Cleaning table: $table, older $date\n";
		my $cnt = delete_records(\@sth, day_start($date));
		printf "=> Affected $cnt rows\n";
	}
}

sub delete_records {
	my $sth_r = shift;
	my $cnt = 0;
	while (1)
	{
		my $deleted = 0;
		for my $sth (@$sth_r)
		{
			$deleted += $sth->execute(@_);
		}
		return $cnt unless $deleted;

		$cnt += $deleted;
		select(undef, undef, undef, 0.25);
	}
}

sub valid_date {
	my ( $date ) = @_;

	return 0 unless $date =~ /^\d{4}-\d{2}-\d{2}$/;

	# check that date is at least 2 weeks in past
	return 1;
}

sub record_count {
	my ( $dbh, $table, $date ) = @_;

	my ( $cnt ) = $dbh->selectrow_array("select count(*) from $table where date between ? and ?", undef,
										day_start($date),
										day_end($date));
	return $cnt;
}

sub day_start {
	my ( $date ) = @_;
	UnixDate(ParseDate($date), '%Y-%m-%d 00:00:00');
}

sub day_end {
	my ( $date ) = @_;
	UnixDate(ParseDate($date), '%Y-%m-%d 23:59:59');
}

main();
