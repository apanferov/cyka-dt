#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;

my %svc_names;
my %op_names;


sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

	my %op_stat;
	my %svc_stat;
	my %ab_stat;

	my $first_date;
	my $last_date;

	while (<>) {

		m|(\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2})|;
		my $date = $1;

		unless ( $first_date ) {
			$first_date = $date;
		}

		$last_date = $date;

		if (/IGNORE (BLACKLIST|FRAUD) MSG(\d+): FROM ABONENT(\d+) VIA OPERATOR(\d+) NUM(\d+) TO SERVICE(\d+)/) {
			my ( $type, $id, $ab, $op, $num, $svc ) = ( $1, $2, $3, $4, $5, $6 );

			$op_stat{$op}{$type}++;
			$svc_stat{$svc}{$type}++;
			$ab_stat{$ab}{$type}++;
			$op_stat{$op}{total}++;
			$svc_stat{$svc}{total}++;
			$ab_stat{$ab}{total}++;
		}
	}

	my @out;

	push @out, "<h1>Fraud stats from $first_date to $last_date</h1>";

	if (keys %svc_stat) {
		push @out, "<h1>Service stats</h1>";
		push @out, "<table border=1>";
		push @out, "<tr><td>Service</td><td>FRAUD</td><td>BLACKLIST</td><td>total</td></tr>";
		for my $sid (keys %svc_stat) {
			my $sv = sname($sid);
			my $fr = $svc_stat{$sid}{FRAUD} || '-';
			my $bl = $svc_stat{$sid}{BLACKLIST} || '-';
			my $to = $svc_stat{$sid}{total} || '-';
			push @out, "<tr><td>$sv</td><td>$fr</td><td>$bl</td><td>$to</td></tr>";
		}
		push @out, "</table>";
	}

	if (keys %op_stat) {
		push @out, "<h1>Operator stats</h1>";
		push @out, "<table border=1>";
		push @out, "<tr><td>Operator</td><td>FRAUD</td><td>BLACKLIST</td><td>total</td></tr>";
		for my $sid (keys %op_stat) {
			my $sv = oname($sid);
			my $fr = $op_stat{$sid}{FRAUD} || '-';
			my $bl = $op_stat{$sid}{BLACKLIST} || '-';
			my $to = $op_stat{$sid}{total} || '-';
			push @out, "<tr><td>$sv</td><td>$fr</td><td>$bl</td><td>$to</td></tr>";
		}
		push @out, "</table>";
	}

	sendmail( content_type => 'text/html; charset=windows-1251',
			  to => Config->param('yell_email_fraud'),
			  from => Config->param('yell_from'),
			  subject => 'fraud aggregated report',
			  body => join("\n", @out) );

}
main;

sub sname {
	my ( $id ) = @_;

	unless ($svc_names{$id}) {
		($svc_names{$id}) = Config->dbh->selectrow_array("select name from set_service where id = ?", undef, $id);
	}

	return $svc_names{$id};
}

sub oname {
	my ( $id ) = @_;
	unless ($op_names{$id}) {
		($op_names{$id}) = Config->dbh->selectrow_array("select name from set_operator_t where id = ?", undef, $id);
	}

	return $op_names{$id};
}
