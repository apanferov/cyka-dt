#!/usr/bin/perl
use strict;
use locale;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Log::Log4perl;
use Config::Tiny;
use Getopt::Long;
use Data::Dumper;

use Disp::Config;
use Disp::Mysql;

sub main {
    local $| = 1;
    Log::Log4perl->init("../etc/log.conf");
    Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf",
		 skip_db_connect => 1);

    my $mode = "";
    unless (GetOptions( 'mode=s' => \$mode)) {
	die "Failed options parsing";
    }

    my %mode_table = ( set => \&transfer_set_tables,
		       auto => \&dump_auto_increment,
		       map => \&transfer_num_map,
		       queue => \&transfer_queues,
		     );

    my $sub = $mode_table{$mode};

    unless ($sub) {
	die "No valid mode given\n";
    }

    my ($from_dbh, $from_cfg) = Config->dbh_config_maker($ARGV[0]);
    my ($dest_dbh, $dest_cfg) = Config->dbh_config_maker($ARGV[1]);



    print STDERR "Source DB: $from_cfg->{name} \@ $from_cfg->{host}\n";
    print STDERR "Destination DB: $dest_cfg->{name} \@ $dest_cfg->{host}\n";
    print STDERR "Do '$mode' action? [y/n]? ";
    my $answer = <STDIN>;

    if ($answer =~ /^y/) {
	print STDERR "Doing '$mode' action\n";
	$sub->($from_dbh, $from_cfg, $dest_dbh, $dest_cfg);
    }

    # transfer_set_tables($from_dbh, $from_cfg, $dest_cfg);
    # transfer_num_map($from_dbh, $from_cfg, $dest_dbh, $dest_cfg);
    # copy_auto_increment($from_dbh, $dest_dbh, $exact_auto_increment);

    #     transfer_queues($from_dbh, $dest_dbh)
    #       if $transfer_queues;
}
main();

sub transfer_set_tables {
    my ( $from_dbh, $from_cfg, $to_dbh, $to_cfg ) = @_;

    do_dump($from_cfg, $to_cfg, [ "--routines", "--triggers" ],
	    @{set_tables_list($from_dbh, $from_cfg->{name})});

}

sub set_tables_list {
    my ( $dbh, $db_name ) = @_;
    my $st = $dbh->prepare(q{select table_name from information_schema.tables where table_schema like ? and table_type in ('BASE TABLE', 'VIEW') and table_name like 'set_%'});
    $st->execute($db_name);

    my @ret;
    while (my($tab) = $st->fetchrow_array) {
	push @ret, $tab;
    }
    return \@ret;
}

sub transfer_num_map {
    my ( $from_dbh, $from_cfg, $to_dbh, $to_cfg ) = @_;

    do_dump($from_cfg, $to_cfg, [],
	    qw[tr_num_map tr_link_map]);
}

sub dump_auto_increment {
    my ( $from_dbh, $from_cfg, $to_dbh, $to_cfg ) = @_;

    my $from_inc = get_auto_increment_tables($from_dbh, $from_cfg->{name});
    my $to_inc = get_auto_increment_tables($to_dbh, $to_cfg->{name});

    my %increments;

    foreach my $tab_info (@$from_inc) {
	$increments{$tab_info->[0]} = $tab_info->[1];
    }

    foreach my $tab_info (@$to_inc) {
	if ( $tab_info->[1] >= $increments{$tab_info->[0]} ) {
	    print STDERR "Dest $tab_info->[0] has auto_increment greater than source, ignored\n";
	    delete $increments{$tab_info->[0]};
	}
    }

    while (my($k,$v) = each %increments) {
	print "alter table $k auto_increment = $v;\n";
    }
}

sub get_auto_increment_tables {
    my ( $dbh, $db ) = @_;

    return $dbh->selectall_arrayref("select table_name, auto_increment from information_schema.tables where table_schema = ? and auto_increment > 0",
				    undef, $db);

}

sub do_dump {
    my ( $from, $to, $opts, @tables ) = @_;

    local $| = 1;

    my @from_exec = ( Config->param("mysqldump"),
		      "--default-character-set=cp1251",
		      "--single-transaction",
		      "--quick",
		      "--user=$from->{username}",
		      "--password=$from->{password}",
		      "--host=$from->{host}",
		      "--add-drop-table",
		      @$opts,
		      $from->{name},
		      @tables );

    my @to_exec = ( Config->param("mysql"),
		    "--force",
		    "--user=$to->{username}",
		    "--password=$to->{password}",
		    "--database=$to->{name}",
		    "--host=$to->{host}" );

    print STDERR "Will exec: ".join(" ", @from_exec)." | ".join(" ", @to_exec)."\n";

#    exit 1;

    open my $from, "-|", @from_exec or die "From dump open failed";
    open my $to,   "|-", @to_exec or die "To dump open failed";

    while (<$from>) {
	print $to $_;
	print STDERR ".";
    }
    print STDERR "\n";
}

sub transfer_queues {
    my ( $from_dbh, $from_cfg, $to_dbh, $to_cfg ) = @_;
    do_dump($from_cfg, $to_cfg, [],
	    qw[tr_insms tr_inbox tr_inbox_restart tr_outbox tr_outsms]);
}
