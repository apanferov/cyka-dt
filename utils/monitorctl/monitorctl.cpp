#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
using namespace std;

enum { RELOAD = 1, LIST, RESTART_CHILD };

int main(int argc, char **argv) {
  int c;
  int mode = 0;
  string k_opt;
  while ( 1 ) {
	int option_index = 0; 
	static struct option long_options[] = {
	  { "reload", 0, 0, 'r' },
	  { "list", 0, 0, 'l' },
	  { "restart-child", 1, 0, 'k' } };
	c = getopt_long(argc, argv, "rlk:", long_options, &option_index);
	if ( c == -1 )
	  break;
	switch ( c ) {
	case 'r':
	  mode = RELOAD;
	  break;
	case 'l':
	  mode = LIST;
	  break;
	case 'k':
	  mode = RESTART_CHILD;
	  k_opt = optarg;
	  break;
	}
  }

  if ( ! mode ) {
	cout << "Usage: monitorctl -r|-l|-k <name>" << endl;
	cout << "   -r       --reload                  Send SIGHUP to monitor" << endl;
	cout << "   -l       --list                    List running childs" << endl;
	cout << "   -k       --restart-child <name>    Send SIGTERM to child '<name>'" << endl;
	exit(1);
  }

  if (mode == RELOAD ) {
	ifstream pid(PID_FILE);
	if ( ! pid ) {
	  cout << "ERR: No pid file" << endl;
	  exit(1);
	}

	int i;
	pid >> i;
	cout << "Signalling " << i << (kill(i, SIGHUP) ? " failed" : " succeeded" )<< endl;
	return 0;
  }

  map<string, int> child_map;
  ifstream status(STATUS_FILE);

  if ( ! status ) {
	cout << "ERR: No status file" << endl;
	exit(1);
  }

  int update_time;

  status >> update_time;
  if ( time(0) > update_time + 10  ) {
	cout << "ERROR: Stalled status, from " << update_time << ", now is " << time(0) << endl;
	return 1;
  }

  while ( 1 ) {
	string name,child_status,tmp,time_running;
	int child_pid;
	status >> name;
	status >> child_status;
	status >> tmp;
	status >> time_running; // throw away
	if ( name == "" ) 
	  break;
	if ( child_status == "RUNNING" ) {
	  child_map[name] = atoi(tmp.c_str());
	}
  }

  if ( mode == LIST ) {
	for (map<string,int>::const_iterator i = child_map.begin(); i != child_map.end(); i++ ) {
	  cout << i->first << endl;
	}
	return 0;
  }

  if ( mode == RESTART_CHILD ) {
	map<string,int>::const_iterator i = child_map.find(k_opt);
	if ( i == child_map.end() ) {
	  cout << "ERR: No such child " << k_opt << endl;
	  return 1;
	} 
	cout << "Signalling " << k_opt << " with TERM "
		 << (kill(i->second, SIGTERM) ? "failed" : "successful" ) << endl;
  }
}
