#!/usr/bin/perl
use strict;
use locale;
use lib '/gate/asg5/lib';
use Disp::MonitorCtl;
use Data::Dumper;
use Getopt::Long;

my $mc = new_aggregated Disp::MonitorCtl
  ( { pid => "/gate/asg5/var/run/monitor-transport.pid",
	  status => "/gate/asg5/var/log/monitor-transport.status" },
	{ pid => "/gate/asg5/var/run/monitor-billing.pid",
	  status => "/gate/asg5/var/log/monitor-billing.status" } );

my ($get_list, $reload, $restart_child);

my $result = GetOptions( "list" => \$get_list,
						 "reload" => \$reload,
						 "k|restart-child=s" => \$restart_child);

if ($get_list) {
	print join "\n", map { $_->{name} } @{$mc->list};
	exit;
}

if ($reload) {
	$mc->reload;
	print "All monitors signalled";
	exit;
}

if ($restart_child) {
	if ($mc->kill_child($restart_child)) {
		print "Child $restart_child signalled\n";
	} else {
		print "Child $restart_child signalling failed\n";
	}
	exit;
}
