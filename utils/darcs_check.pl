#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my @hosts = ('roasg@alt1-dt.alt1.ru', 'roasg@alt1-dt2.alt1.ru', 'roasg@alt1-dt3.alt1.ru', 'roasg@devel2.local.alt1.ru');
my $email = 'a.lebedeff@alt1.ru, v.islamov@alt1.ru, e.tihonov@alt1.ru, a.arhipov@alt1.ru, m.fokin@alt1.ru';

use FindBin;
use lib "$FindBin::RealBin/../lib";

use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;

my @out;

sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf", skip_db_connect => 1);

	my %patches;

	push @out, "<html><head><title>DARCS STATS</title></head><body>";

	check_patches(\%patches, $_) for @hosts;
	print_patch_diff(\@hosts, \%patches);
	check_commit($_) for @hosts;

	push @out, "</body></html>";

	sendmail (content_type => "text/html",
			  to => $email,
			  from => 'roasg@devel2.local.alt1.ru',
			  subject => 'darcs stats',
			  body => join "\n", @out );
}

main();

sub check_patches {
	my ( $patches, $hostname ) = @_;

	local $_;

	open(my $fh, "-|", "ssh", "-i", "/home/roasg/.ssh/id_dsa", $hostname, "perl", "/gate/asg5/utils/darcs_patchset.pl", "/gate/asg5/");

	while (<$fh>) {
		next unless /^\d{14}.*\.gz$/;
		chomp;
		$patches->{$_}{total}++;
		$patches->{$_}{$hostname} = 1;
	}

}

sub print_patch_diff {
	my ( $hosts, $patches ) = @_;

	my $host_count = @$hosts;
	my $first_pass = 1;

	foreach my $patch ( sort grep { $patches->{$_}{total} < $host_count } keys %$patches ) {
		if ($first_pass) {
			$first_pass = 0;
			push @out, "<h1>Patch info</h1>";
			push @out, "<table border='1'>";
			push @out, "<tr><td>Patch info</td>" . join("", map {"<td>$_</td>"} @$hosts ) . "</tr>";
		}
		delete $patches->{$patch}{total};

		my ($arbitary_host) = (keys %{$patches->{$patch}});

		my ( $patch_comment, $patch_author, $patch_date  ) = get_patch_info($arbitary_host, $patch);

		push @out, "<tr>";
		push @out, "<td>$patch_comment<br>$patch_date<br>$patch_author</td>";
		for my $host (@$hosts) {
			my $color = "";
			$color = "bgcolor='red'" unless $patches->{$patch}{$host};
			push @out, "<td $color>&nbsp;</td>";
		}
		push @out, "</tr>";
	}
	unless ($first_pass) {
		push @out, "</table>";
	}
}

sub get_patch_info {
	my ( $hostname, $patch ) = @_;
	open (my $fh, "-|", "ssh", "-i", "/home/roasg/.ssh/id_dsa", $hostname, "zcat", "/gate/asg5/_darcs/patches/$patch");

	chomp(my $comment = <$fh>);
	chomp(my $author_and_date = <$fh>);

	$comment =~ s/^\[//;

	my ($author, $date) = $author_and_date =~ m/^(.*)\*\*(\d{14})/;

	my @date = ($date =~ m/\d{2}/g);

	$date = "$date[0]$date[1]-$date[2]-$date[3] $date[4]:$date[5]:$date[6]";

	return ( $comment, $author, $date );
}

sub check_commit {
	my ( $hostname ) = @_;

	local $_;

	open(my $fh, "-|", "ssh", "-i", "/home/roasg/.ssh/id_dsa", $hostname, "sh", "/gate/asg5/utils/darcs_check.sh");

	my $first_pass = 1;

	while (<$fh>) {
		if ($first_pass) {
			push @out, "<h1>Uncomitted info for $hostname</h1>";
			push @out, "<table border='1'><tr><td>File</td><td>Hunks</td></tr>";
			$first_pass = 0;
		}

		if (/^\s+(\d+)\s+(.*)$/) {
			push @out, "<tr><td>$2</td><td>$1</td></tr>";
		} else {
			push @out, "<tr><td bgcolor='red' rowspan='2'>Invalid line: $_</td></tr>";
		}
	}
	unless ($first_pass) {
		push @out, "</table>";
	}
}
