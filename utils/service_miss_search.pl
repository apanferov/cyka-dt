#!/usr/bin/perl
use strict;
use warnings;
use CGI::Carp qw/fatalsToBrowser/;
use CGI;
use lib '../lib';
use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;
use Log::Log4perl;
use Data::Dumper;
use POSIX qw/strftime/;
use Translit qw/translit_text/;

Config::init(config_file => '../etc/asg.conf');
Log::Log4perl::init('../etc/log.conf');

my $sth = Config->dbh->prepare("select * from tr_inboxa where num = 1121 and date > '2008-10-20' order by date");
$sth->execute;

while (defined(my $hr = $sth->fetchrow_hashref)) {
    my $trans = translit_text($hr->{msg});

    if ($trans =~ /^p[0-9m]/i) {
	my ( $service_id ) = Config->dbh->selectrow_array("select service_id from tr_billing where inbox_id = ? and outbox_id = 0 and dir = 1", undef,
							  $hr->{id});
	print "$hr->{id}\t$hr->{abonent}\t$hr->{date}\t$service_id\t$hr->{msg}\n";
    }
}



