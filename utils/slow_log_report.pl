#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Encode;
use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use Spreadsheet::WriteExcel;
use MIME::Lite;
use IO::Scalar;


sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

	Config->dbh->do("truncate tr_service_time_log");
	Config->dbh->do("set names utf8");

	my $log_name = Config->param("system_root")."/var/log/old/log-message.log.1.bz2";
	my ( $first_date, $last_date );

	foreach (keys %{Config->section('db_transport')}) {
		print $_."\n";

		open my $log, "ssh -i /home/roasg/.ssh/id_dsa roasg\@$_ bzcat $log_name|" or die "Can't open logfile $log_name for parsing";

		while (<$log>) {
			if (/^\[(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2})/) {
				unless ($first_date) {
					$first_date = $1;
				}
				$last_date = $1;
			}

			# 2007/03/18 03:12:57 INFO GOT RESPONSE FOR DOWN96519/MSG50541942/SVC493/DOWNLOAD_TIME(0.085967)
			if (/^\[(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}).* INFO GOT RESPONSE FOR DOWN(\d+)\/MSG(\d+)\/SVC(\d+)\/DOWNLOAD_TIME\(((?:\d|\.|,)+)\)/) {
				my $time = $10;
				$time =~ tr/,/./;
				Config->dbh->do("insert into tr_service_time_log values ( $9, $time, '$1-$2-$3', 'success', $8)");
			}

			# 2007/03/18 03:21:30 ERROR TRANSPORT ERROR FOR DOWN96910/MSG50538636/ERROR(TIMEOUT)/DOWN_TIME(20.08264))
			if (/^\[(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}).* ERROR TRANSPORT ERROR FOR DOWN(\d+)\/MSG(\d+)\/ERROR\(([^)]+)\)\/DOWN_TIME\(((?:\d|\.|,)+)\)\/SVC(\d+)/) {
				my $time = $10;
				$time =~ tr/,/./;
				Config->dbh->do("insert into tr_service_time_log values ( $11, $time, '$1-$2-$3', '$9', $8)");
			}
		}
	}

	my $sql = "select s.id, s.name, t.reason, min(t.time),
	           max(t.time), avg(t.time), sum(t.time) as sumt, count(*)
	           from tr_service_time_log t join set_service_t s on
	           t.service_id = s.id group by s.id, t.reason order by
	           sumt desc";

	my $sth = Config->dbh->prepare($sql);
	$sth->execute;

	my $data = "";

	my $wbfh = new IO::Scalar \$data;
	# my $workbook = Spreadsheet::WriteExcel->new("/tmp/slow_log_report.xls");
	my $workbook = Spreadsheet::WriteExcel->new($wbfh);

	# Add a worksheet
	my $worksheet = $workbook->add_worksheet(decode('cp1251','�������� ������ ��������'));

	$worksheet->write_string(0, 0, decode("UTF-8", "Log from $first_date to $last_date"));
	$worksheet->write_string(1, 0, decode("UTF-8", "service_id"));
	$worksheet->write_string(1, 1, decode("UTF-8", "Сервис"));
	$worksheet->write_string(1, 2, decode("UTF-8", "Причина"));
	$worksheet->write_string(1, 3, decode("UTF-8", "Мин. время"));
	$worksheet->write_string(1, 4, decode("UTF-8", "Макс. время"));
	$worksheet->write_string(1, 5, decode("UTF-8", "Среднее время"));
	$worksheet->write_string(1, 6, decode("UTF-8", "Сумм. время"));
	$worksheet->write_string(1, 7, decode("UTF-8", "Кол-во запросов"));


	$worksheet->set_column('A:A', 10);
	$worksheet->set_column('B:B', 35);
	$worksheet->set_column('C:C', 10);
	$worksheet->set_column('D:D', 10);
	$worksheet->set_column('E:E', 11);
	$worksheet->set_column('F:F', 14);
	$worksheet->set_column('G:G', 12);
	$worksheet->set_column('H:H', 15);

	my $row = 2;
	while (my($sid, $sname, $reason, $mint, $maxt, $avgt, $sumt, $count) = $sth->fetchrow_array) {
		$worksheet->write($row, 0, $sid);
		$worksheet->write_string($row, 1, decode("UTF-8", $sname));
		$worksheet->write_string($row, 2, decode("UTF-8", $reason));
		$worksheet->write($row, 3, $mint);
		$worksheet->write($row, 4, $maxt);
		$worksheet->write($row, 5, $avgt);
		$worksheet->write($row, 6, $sumt);
		$worksheet->write($row, 7, $count);

		$row++;
	}

	$workbook->close;

	my $msg = MIME::Lite->new( From => 'a.lebedeff@alt1.ru',
							   To => 'v.islamov@alt1.ru',
							   Subject => "Slowlog from $first_date to $last_date",
							   Type => 'text/plain',
							   Data => "Weekly slow-log report");

	$msg->attach( Type => 'application/vnd.ms-excel',
				  Encoding => 'base64',
				  Data => $data,
				  # Path => "/tmp/slow_log_report.xls",
				  Filename => 'slow_log.xls');

	raw_sendmail($msg->as_string);
}

main();
