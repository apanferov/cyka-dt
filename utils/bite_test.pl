#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Data::Dumper;


use Disp::Utils;
use Disp::Config;
use Log::Log4perl qw/get_logger/;

use Channel::SMPP::Func_Bite;
use Disp::SMPP_SMSClient;

sub main {
	Log::Log4perl->init("$FindBin::RealBin/../etc/log.conf");
	Config::init(config_file => "$FindBin::RealBin/../etc/asg.conf");

	my $esme = new Disp::SMPP_SMSClient(143, 'DEBUG', 3);

	my $sms = Config->dbh->selectrow_hashref("select * from tr_insms_ignored where id = 1430367");

	# $sms->{abonent} = '3712219983';

	Channel::SMPP::Func_Bite::bill_account($esme, $sms, 1430367);
}

main();
