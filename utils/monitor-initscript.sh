#!/bin/sh

# chkconfig: 2345 99 1
# description: Dispatcher monitor
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/mysql/bin:/usr/local/apache2/bin:/opt/bin
USER=asg
NAME=monitor-transport.pl
DAEMON=/gate/asg5/bin/monitor-transport.pl
WORKDIR=/gate/asg5/bin
PID=/gate/asg5/var/run/monitor-transport.pid
TIMEOUT=10

REAL_USER=`id -nu`
if [ $REAL_USER = $USER ] ; then
	SU_CMD=
else
	SU_CMD="su $USER -c"
fi

check_pid()
{
	if test ! -r $1 ; then 
		PID_VALUE=0
		return 1
	fi
	PID_VALUE=`cat $1`
	if kill -0 $PID_VALUE ; then
		return 0
	else
		rm $PID
		PID_VALUE=0
		return 1
	fi
}

start()
{
	echo -n "Starting $NAME: "
	if check_pid $PID ; then
		echo "already running"
	else
		pushd . > /dev/null
		if cd $WORKDIR &> /dev/null; then
			if $SU_CMD $DAEMON ; then 
				echo "done."
			else
				echo "failed."
			fi
		else
			echo "failed cd to workdir"
		fi
		popd > /dev/null
	fi
}

stop()
{
	echo -n "Stopping $NAME: "
	if check_pid $PID ; then
		kill $PID_VALUE
		QUIT_FLAG=0
		START_TIME=`date '+%s'`
		END_TIME=`expr $START_TIME + $TIMEOUT`
		while test $QUIT_FLAG -eq 0 ; do
			sleep 1
			if check_pid $PID ; then
				CUR_TIME=`date '+%s'`
				if test $CUR_TIME -gt $END_TIME ; then
					echo "wait timed out."
					QUIT_FLAG=1
				fi
			else
				QUIT_FLAG=1
				echo "done."
			fi	
		done
	else
		echo "no pid found"
	fi
}

reload() {
	echo -n "Reloading $NAME: "
	if check_pid $PID ; then
		kill -HUP $PID_VALUE
		echo "done."
	else
		echo "nothing to reload"
	fi
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		$0 stop
		$0 start
		;;
	reload)
		reload
		;;
	*)
		echo "Usage: $0 {start|stop|restart|reload}"
		;;
esac
