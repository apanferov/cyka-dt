#!/usr/bin/perl

my @ppid=(`cat /gate/asg5/var/run/monitor-transport.pid`);

while(my $ppid = shift @ppid) {
  my @pid = split("\n", `ps -opid= --ppid $ppid`);
  for my $pid (@pid) {
    system("/bin/kill -INT $pid");
    push @ppid, $pid;
  }
}
