#!/usr/bin/perl
# -*- coding: windows-1251-unix -*-
use strict;
use warnings;
use lib '../lib';
use DBI; 
use Data::Dumper;
use Disp::AltDB;
use Disp::Config qw(param);
use Disp::Utils;
use Config::Tiny; 
#use Net::SFTP::Foreign;
use Net::SCP::Expect;
use Getopt::Long;
use Cwd;

# Конфигурация
my $cfg = {
	week => undef, # Неделя undef = <прошедшая>, формат 'yyyy-mm-dd'
	service_id => 1813, # Сервис !Error::Фрод для Velcom
	help => 0, # Как использовать
};
GetOptions(
	\%$cfg,
	'week|weekday|w|d=s',
	'help|h'
) or usage();
usage() if ($cfg->{help});

sub usage {
	print
		"Usage: $0 [options]\n".
		"  -h, --help                 this help message\n".
		"  -d, -w, --week, --weekday  the day of report week, format 'yyyy-mm-dd'\n";
	exit;
}

my $sftp_host = '77.74.32.130';
my $sftp_port = 22;
my $sftp_user = 'lexmedia';
#my $sftp_pass = '3ayEV8@mz67';
my $sftp_pass = 'ca2QAMv@cksq';

my $scpe = Net::SCP::Expect->new(
	host => $sftp_host, 
	user => $sftp_user, 
	password => $sftp_pass, 
	port => $sftp_port
);

## devel2.alt1.local
# my $db_host = "192.168.252.15";
# my $db_name = "alt5";
# my $db_user = "asg";
# my $db_pass = "GisurmAms4";

## bill1.alt1.ru
# my $db_host = "bill1.alt1.ru";
# my $db_name = "alt5";
# my $db_user = "asg";
# my $db_pass = "9atUQ704Mbq4WTJ";

# bill2.alt1.ru
my $db_host = "bill2.alt1.ru";
my $db_name = "alt5";
my $db_user = "asg";
my $db_pass = "9atUQ704Mbq4WTJ";

my $dbh = DBI->connect(
	"DBI:mysql:$db_name;host=$db_host",
	$db_user,
	$db_pass
);
$dbh->do("SET NAMES 'cp1251'");
$dbh->do("SET character_set_client=cp1251");  

#Config::init(config_file => "../etc/asg.conf", skip_db_connect => 0);
#my $db = new Disp::AltDB(); 

my $date_week;
my $date_week_end;
if (defined($cfg->{week})) {
	$date_week = $dbh->selectrow_array("select date_sub('$cfg->{week}', interval (weekday('$cfg->{week}') + 7 - 7) day)");
	$date_week_end = $dbh->selectrow_array("select date_sub('$cfg->{week}', interval (weekday('$cfg->{week}') + 1 - 7) day)");
} else {
	$date_week = $dbh->selectrow_array("select date_sub(curdate(), interval (weekday(curdate()) + 7) day)");
	$date_week_end = $dbh->selectrow_array("select date_sub(curdate(), interval (weekday(curdate()) + 1) day)");
}
$cfg->{week_start} = $date_week;
$cfg->{week_end} = $date_week_end;

print Dumper($cfg);

my $query = <<EOF;
select b.abonent, b.num, if(b.dir = 1, 'O', 'I') as direction, b.date
from tr_billing b
where
b.date >= '$cfg->{week_start}' and b.date <= '$cfg->{week_end} 23:59:59' and
b.service_id = $cfg->{service_id} and
b.num in ('5013', '5014')

union all

select i.abonent, i.num, if(1 = 1, 'O', 'I') as direction, i.date
from tr_inboxa i
where
i.id = (
select rt.inbox_id
from cyka_return_job rj
left join cyka_return_t rt on rt.job_id = rj.id
where
('$cfg->{week_start}' <= rj.date_start and rj.date_start <= '$cfg->{week_end} 23:59:59') or
('$cfg->{week_start}' <= rj.date_end and rj.date_end <= '$cfg->{week_end} 23:59:59') or
(rj.date_start <= '$cfg->{week_start}' and '$cfg->{week_end} 23:59:59' <= rj.date_end)
) and
i.operator_id = 205
EOF
#print $query;
my $rt = $dbh->selectall_arrayref($query, { Slice => {} }); 
#print Dumper($rt);

my $data = {};
if (@$rt) {
	my $cwd  = Cwd::cwd(); 

	foreach my $row (@$rt) {
		my ($y, $m, $d, $hour, $min, $sec) = $row->{date} =~ /^(\d+)\-(\d+)\-(\d+)\s(\d+):(\d+):(\d+)$/;
		$row->{date} = "$d.$m.$y $hour:$min:$sec";
		push(@{$data->{$row->{num}}}, $row);
	}

	my ($y, $m, $d) = $cfg->{week_end} =~ /^(\d+)\-(\d+)\-(\d+)$/;

	foreach my $num (keys %$data) {
		my $filename = "lexmedia_".$num."_$d$m$y.txt";

		open(F,"> $cwd/$filename");
		my $old_point = select(F);
#		print Dumper($data);
		foreach my $row (@{$data->{$num}}) {
		 	print "$row->{abonent};$row->{num};$row->{direction};$row->{date};;\n";
		}
		select($old_point);
		close(F);

		$scpe->scp("$cwd/$filename", "/");
		print "File name: '$cwd/$filename' - ok\n";
	}

	print "Ok: data is successfully uploaded.\n";
} else {
	print "Error: No data for selected period.\n";
}

print "Done\n"; 

__END__
 
scp ./mobil2bel_1813_140608.txt  lexmedia@77.74.32.130:/
