#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Data::Dumper;
use Disp::Config;
#use Disp::AltDB;
use Replication::Position;

Config::init(config_file => '../etc/asg.conf', skip_db_connect => 0);

our $dir = '../var/repl';
my $load_position_try_cnt = 10; # (шт)
my $dif_pos = 1024*100; # (байт) 100 Кб
my $dif_time = 60*10; # (сек) 10 мин

#for my $i (1..1000) {
foreach my $dest_host (split(/\s*,\s*/, Config->param('destination_hosts', 'replication'))) {
	my $dest_db_conf = (Config->param($dest_host, 'db_transport') || Config->param($dest_host, 'db_billing'));
#	print $dest_db_conf->{host}."\n";

	my ($filename, $pos) = load_position($dest_db_conf->{host}.".pos");
	my $size = (stat("$dir/$filename"))[7] || 0; # Размер файла (байты)
	my $date_last_modified = (stat("$dir/$filename"))[9] || 0; # Дата последнего изменения файла
#	print "$date_last_modified\n";
#	print time()."\n";
	unless (
		$size - $pos < $dif_pos &&  # Позиция репликации в файле не должна отставать больше чем на $dif_pos_and_size (байт)
		$size > 0 && # Размер файла репликации должен быть > 0 (байт)
		time() - $date_last_modified < $dif_time # Время изменения файла не старше $dif_time (сек) от текущего
	) {
		print "0";
		print "filename=$filename,pos=$pos,size=$size,pos_dif=".($size - $pos).",time_dif=".(time() - $date_last_modified)."\n";
		exit(0);
	}
}
print "1";
exit(1);
#print "$i\n";
#sleep(1);
#}

sub load_position {
	my ($position_filename) = @_;
	# Try load from position file
#	print "$dir/$position_filename\n";

	my ($filename, $pos);
	my $terminated = 0;
	my $try_cnt = $load_position_try_cnt;
	while (!$terminated) {
		eval {
			my $rep_pos = new Replication::Position("$dir/$position_filename");
			($filename, $pos) = $rep_pos->load_position;
		};
		if ($@ || !$filename) {
			$try_cnt--;
			$terminated = 1 if ($try_cnt <= 0);
			sleep(1);
		} else {
#			print "try_cnt=".($load_position_try_cnt - $try_cnt)."\n";
			$terminated = 1;
		}
	}

	# Try find earliest replication file in directory
	unless ($filename and (-f "$dir/$filename")) {
		($filename) = sort {$a cmp $b} glob("$dir/repl-????-??-??-GMT.log") ;
		$filename ||= '';
		($filename) = $filename =~ /(repl-\d{4}-\d{2}-\d{2}-GMT\.log)/;
	}
#	# Set defaults
#	$filename ||= "repl-2008-10-01-GMT.log";
	return $filename, $pos;
}

__END__

Алгоритм:
1) Узнаем какие сервера обновляются и через какие файлы по конфигурации /gate/asg5/etc/asg.cfg
2) По каждому серверу принимаем решение(работает/нет) в зависимости от
- позиция репликации в файле не должна отставать больше чем на 100 Кб
- размер файла репликации должен быть > 0
- время изменения файла не старше 10 мин от текущего
