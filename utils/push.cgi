#!/usr/bin/perl
use strict;
use warnings;
use CGI::Carp qw/fatalsToBrowser/;
use CGI;
use lib '../lib';
use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;
use Log::Log4perl;
use Data::Dumper;
use POSIX qw/strftime/;


use Time::HiRes;


Config::init(config_file => '../etc/asg.conf');
Log::Log4perl::init('../etc/log.conf');

eval {
#    warn "$$ START ".Time::HiRes::time;
	main();
#    warn "$$ END ".Time::HiRes::time;
};
yell("PUSH: $@",code => 'FATAL',process => 'PUSH',
	header => "PUSH: $@",ignore => { log4perl => 1 }) if ($@);
exit;

sub main {
	my $q = new CGI;
	print $q->header(-type => 'text/html', -charset => 'cp1251');

	my ( $msg, $sid ) = ($q->param('msg'),$q->param('service_id'));

	if (defined($msg) and length($msg) and ($sid =~ /^\d+$/)) {
		
		# ��� ���������� 
#		if ( #($ENV{REMOTE_ADDR} eq '192.168.252.71') or 
#			  #($ENV{REMOTE_ADDR} eq '62.105.145.210') or 
#			($ENV{REMOTE_ADDR} eq '195.54.25.99')) { 
#			my $rsp = parse_response($msg);
#			if ($rsp->{result}) {
#				foreach my $sec (@{$rsp->{sections}}) {
#					foreach my $dest (@{$sec->{destinations}}) {
#						my $op = 28;
#						$op = 6  if ($dest eq '73519000138');
#						Config->dbh->do("replace into tr_num_map values(?,'sms',?,determine_operator(?,?,null))", undef, $dest, $op, $dest, $op) if ($dest);
#						#print $dest;
#					}
#				}
#			}
#		}
		# ��� ���������� 
		
#        warn "$$ CREATION START ".Time::HiRes::time;
		my $sm = new Disp::ServiceManager({this_is_push => 1}); #+-vlad
#        warn "$$ CREATION END ".Time::HiRes::time;

		# XXX check service auth here
		my $error;

#        warn "$$ PUSHBOX INSERT START ".Time::HiRes::time;
		my $push_id = sql_insert_with_id("tr_pushboxa", [ qw /service_id ip_addr date/],
										 [ $sid, $ENV{REMOTE_ADDR}, strftime('%Y-%m-%d %H:%M:%S',
																			 localtime())] );
#        warn "$$ PUSHBOX INSERT END ".Time::HiRes::time;

#        warn "$$ PUSHBOX HANDLE START ".Time::HiRes::time;
		$sm->handle_service_response( service => { id => $sid },
									  orig => { id => $push_id },
									  response => $msg,
									  orig_type => 'push',
									  error_cb => sub { $error .= "$_[0]\n" } );
#        warn "$$ PUSHBOX HANDLE END ".Time::HiRes::time;
		print $error ? "<pre>$error</pre>" : "$push_id";


		my @cc;

		my $svc = $sm->{set_service}{$sid};
		if ( $svc ) {
		    push @cc, $svc->{email};
		    my $mgr = $sm->{set_cyka_manager}{$svc->{manager_id}};
		    if ( $mgr ) {
			push @cc,  $mgr->{email};
		    }
		}
		my $cc = join ", ", @cc;
		yell("PUSH: Please update push url for service '".$sm->describe_service($sid)."'\nNew url is http://gateasg.alt1.ru/cgi-bin/push.pl",
		     header => 'PLEASE UPDATE YOUR PUSH-CALLING SCRIPTS',
		     process => 'PUSH',
		     code => 'NOTICE',
		     cc => $cc);

#		yell("PUSH: $error",code => 'ERROR',process => 'PUSH',
#			header => "PUSH: $error",ignore => { log4perl => 1 }) if ($error);
	} elsif (!($ENV{REMOTE_ADDR} eq '195.54.25.99')){
		print <<EOF;
		<form method=post>
			<input type=text name=service_id><br><textarea name=msg rows=20 cols=60>status : notice
bill-incoming: zero
bill-outgoing: zero
destination:

</textarea><br><input type=submit>
        </form>
EOF
	}
}
