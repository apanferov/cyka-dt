#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::SMPP_SMSClient;
use Data::Dumper;

my $smsc_id = $ARGV[0];
unless ($smsc_id) {
	print "Usage: fake_sms_client.pl <smsc_id>\n";
	exit 0;
}

my $client;
Config::init(config_file => "../etc/asg.conf", skip_db_connect => 1);
my $db = new Disp::AltDB();
my $cnf = $db->get_smsc_configuration($smsc_id);
my %types = ( transceiver => 3,
              receiver_transmitter => 2
      );
$client = new Disp::SMPP_SMSClient($smsc_id,$cnf->{debug_level},$types{$cnf->{client_type}});
exit $client->run(1);
