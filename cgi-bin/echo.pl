#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8887/;
use POSIX qw/strftime/;

Disp::FastCgi::run(\&main,
				   sub {
				   });

sub main {
  my $q = shift;

  print $q->header(-type => 'text/plain', -charset => 'cp1251');
  print "status: reply\n\n>".$q->param('msg');
}

