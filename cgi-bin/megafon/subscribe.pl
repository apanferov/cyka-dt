#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../../lib";
use CGI;
use Disp::Config;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use LWP::UserAgent;
use URI;
use URI::URL;

Log::Log4perl->init("$FindBin::Bin/../../etc/proxy_log.conf");
Config::init(config_file => "$FindBin::Bin/../../etc/asg.conf");

main();
exit;

sub main {
	my $req = new CGI;
	my $params = $req->Vars;
	my $log = get_logger("");
	$log->info("PT GOT SUBSCRIBE $req->{_binarin_query_string}");
	my $uri = "http://partner2.jumpit.ru/col/mds4/partner/web_tmp/";

	my $ua = new LWP::UserAgent;
	$ua->timeout(10);
	my $url = url($uri);
	$url->query_form(%$params);
	my $resp = $ua->get($url);
	$log->info("MF SENT SUBSCRIBE ".$url->as_string);
	$log->info("MF RESPONSE SUBSCRIBE\n".$resp->as_string);
	
	my $result = $resp->as_string;
	$result =~ s/HTTP\/[\d\.]+\s/Status: /;
	print $result;
}
