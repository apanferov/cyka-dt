#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::RealBin/../../lib";
use CGI;
use Disp::Config;
use Disp::AltDB;
use Log::Log4perl qw/get_logger/;
use Data::Dumper;
use LWP::UserAgent;
use URI;
use URI::URL;
use POSIX;

Log::Log4perl->init("$FindBin::Bin/../../etc/proxy_log.conf");
Config::init(config_file => "$FindBin::Bin/../../etc/asg.conf");

my %black_list = (
	79859680162 => 1
	);

my %service_tab = (
	tutdieta => {partner_id => 5147, service_id => 1689, num => '7052-1500'},
	vksmedias => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	odnmedias => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	sexyrusavi => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	sexyrusclip => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	sexyrusmpeg  => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	socimedias => {partner_id => 5147, service_id => 1689, num => '7052-1500'},
	socmedias => {partner_id => 5147, service_id => 1689, num => '7052-1500'},
	bigfun2012 => {partner_id => 5147, service_id => 1689, num => '7052-1500'},
	russiaxxxvideo => {partner_id => 5147, service_id => 1689, num => '7052-1500'},
	newzter => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	anonymizer => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	windefend => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	redhataa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	bestruvidzaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	blacktubezaaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	sexnovostiaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	hotxxxvideoaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	adulthubzaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	redxtubeaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	exgirlsaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	xxvideoaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	anonimservice2 => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	vkmediaget => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	goroscopeaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	fellowship => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	safeguard => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	webhelper => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	mediadostupno => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	banvidaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	ruadultportalorg => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	combatwars => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	rueliteclubcom => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	muznovostiaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	drochtube => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	ruinfoportalorg => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	homevideopageaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	gayvidspageaa => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	'3girlspageaa' => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	hidemyip => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	protectinfo => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	kvntv => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	unknowny => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	megarelaxxx => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	anonymousaccess => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	securenetprod => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	infodefender => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	officedostup => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	sbornix2 => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	ihelperorg => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	hauberk => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	beregisvoe => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	ivantiru => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	ka4aem => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	basecontent => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	completeanonymity => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	failload => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	myhelperinet => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	myhelpinet => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	smehtvnet => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	smehtvorg => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	zipmtus => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	cyberprotect => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	audiolit => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	helperinternetainfo => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	internetpomoshinfo => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	myinetpomoshinfo => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	myinetpomosh => {partner_id => 5147, service_id => 1689, num => '7052-2000'},
	);

main();
exit;

sub main {
	my $req = new CGI;
	my $params = $req->Vars;
	my $log = get_logger("");
	$log->info("MF GOT NOTIFY $req->{_binarin_query_string}");
#	$log->debug("PARAMS ".Dumper($params));

	unless ( cyka_payment_save_shaded($params,$log) ) {
		my $uri = "http://gateasg.alt1.ru/cgi-bin/push_status.pl";
#		my $uri = "http://mtx.beta.platinote.com/megafon.php";
		my $ua = new LWP::UserAgent;
		$ua->timeout(30);
		my $url = url($uri);
		$url->query_form(%$params);
		my $resp = $ua->get($url);
		$log->info("PT SENT NOTIFY ".$url->as_string);
		$log->info("PT RESPONSE NOTIFY\n".$resp->as_string);
		my $result = $resp->as_string;
		$result =~ s/HTTP\/[\d\.]+\s/Status: /;
		print $result;
	} else {
		print "Status: 200 OK\nConnection: close\nServer: nginx/0.8.54\nContent-Type: text/html; charset=utf-8\n\nok"
	}
}
	
sub cyka_payment_save_shaded {
	my ($params,$log) = @_;
	my $shade_percent = 5;
	my $shade_partner = 5175;
	my $shaded = 0;
	if ($params->{status} and $params->{status} =~ /^new|prolong/ and my $service = $service_tab{$params->{serviceid}}) {
		my $date = $params->{'time'} || POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime);
		$date =~ s/\,/ /;
		my $data = { 
			transport_type => 3,
			date => $date,
			service_id => $service->{service_id},
			abonent =>$params->{msisdn},
			operator_id => 516,
			num => $service->{num},
			partner_id => $service->{partner_id},
			cyka_processed => 0,
			test_flag => 0,
			comment => "$params->{status} $params->{transaction}",
			is_error => 1,
		};
		
		if ($shade_partner and (!$black_list{$params->{msisdn}}) and (int(rand(100)) < $shade_percent) and ($params->{status} =~ /^prolong/)) {
			$data->{partner_id} = $shade_partner;
			$log->info("MF SHADED NOTIFY ($shade_percent\%) -> $shade_partner");
			$shaded = 1;
		}		
		
		my @fields = qw/transport_type date service_id abonent operator_id num partner_id cyka_processed test_flag comment is_error/;
		my $query = "replace into cyka_payment_ext (".join(", ", @fields).") values (".join(", ", ('?') x (@fields)).")";
		my @values = map { $data->{$_} } @fields;
		$log->debug("CYKA SAVE ".Dumper($data));
		eval {
			Config->dbh->do($query, undef, @values);
		};
		$log->warn("CYKA SAVE ERROR $@") if ($@);
	}
	return $shaded;
}
