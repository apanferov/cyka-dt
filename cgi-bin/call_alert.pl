#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8886/;
use Data::Dumper;
use Disp::Config;
use Disp::AltDB;
use LWP::UserAgent;
use JSON;
use URI::URL;

my %mac_tab = {};
my $provider_tab = {
	A => 1,
	M => 2,
	T => 3,
};

Disp::FastCgi::run(
	\&main,
	\&init,
	undef,
	{processes => 1}
);

sub init {
	Config::init(config_file => '../etc/asg.conf');
}

sub main {
	my $q = shift;
	my %params = $q->Vars;
	my $db = Config->dbh;

	print $q->header(-type => 'text/html', -charset => 'cp1251');

	my $abonent = '';
	my $sip = '';
	my $cc_operator_id;
	my $action_id;
	my $mac;

	($sip, $abonent) = ($params{remote} =~ /^sip:(.*)-(\d+)@.*/i) if ($params{remote});
	$cc_operator_id = $params{active_user} || '';
	$action_id = $params{action_id} || '';
	$mac = $params{mac} || '';

	# print "<pre>";
	# print "ip = $ENV{REMOTE_ADDR}\n";
	# print "sip = $sip\n";
	# print "ord(sip) = ".ord($sip)."\n";
	# print "ord(provider_tab) = (".join(',', keys(%$provider_tab)).")\n";
	# print "provider_id = ".$provider_tab->{$sip}."\n";
	# print "abonent = $abonent\n";
	# print "cc_operator_id = $cc_operator_id\n";
	# print "action_id = $action_id\n";
	# print Dumper(\%params);
	# print "</pre>";

	$action_id ||= 0;
	if ($mac) { 
		$mac_tab{$mac}{cc_operator_id} = $cc_operator_id if ($cc_operator_id);
		$mac_tab{$mac}{abonent} = $abonent if ($abonent);
		$mac_tab{$mac}{sip} = $sip if ($sip);
		$cc_operator_id ||= $mac_tab{$mac}{cc_operator_id} if ($mac_tab{$mac}{cc_operator_id});
		$abonent ||= $mac_tab{$mac}{abonent} if ($mac_tab{$mac}{abonent});
		$sip ||= $mac_tab{$mac}{sip} if ($mac_tab{$mac}{sip});
	}

	if ($action_id && $abonent) {
		$abonent =~ s/^7//g;
		$db->do('insert into tr_call_alert (date, cc_operator_id, abonent, action_id, ip, params, orgcode, provider_id) values (now(), ?, ?, ?, ?, ?, ?, ?)', undef, $cc_operator_id, '7'.$abonent, $action_id, $ENV{REMOTE_ADDR}, Dumper(\%params), get_operator_code($abonent), $provider_tab->{$sip});
		print "OK\n";
	} else {
		$action_id ||= 0;
		$abonent ||= '';
		$abonent =~ s/^7//g;
		$db->do('insert into tr_call_alert (date, cc_operator_id, abonent, action_id, ip, params, orgcode, provider_id) values (now(), ?, ?, ?, ?, ?, ?, ?)', undef, $cc_operator_id, $abonent ? '7'.$abonent : '', $action_id, $ENV{REMOTE_ADDR}, Dumper(\%params), get_operator_code($abonent), $provider_tab->{$sip});
		print "BAD_PARAM\n";
	}
}

sub get_operator_code {
	my ($abonent) = @_;
	return 'null' unless $abonent;
	my $resp_params = {};

	# my $url = url("http://dt1.local:9998/get_operator");
	my $http_user = 'qqq';
	my $http_password = 'qqq';
	my $url = url('http://'.$http_user.':'.$http_password.'@dt1.local:9998/get_operator');
	my %req = (
		number => $abonent,
	);
	$url->query_form(%req);

	# GET
	my $ua = LWP::UserAgent->new;
	$ua->timeout(5); # ������� ����� (���)
	$ua->env_proxy;
	my $resp = $ua->get($url);
	my $resp_data = $resp->as_string;
	if ($resp->code == 200) {
		eval {
			my ($resp_header, $resp_str) = split("\n\n", $resp_data);
			$resp_params = decode_json($resp_str);
		};
		if ($@) { 
			return 'null';
		} else {
			return $resp_params->{orgcode} || $resp_params->{error};
		}
	} else {
		return 'null';
	}		
}

__END__

http://gateasg.a1-content.ru/cgi-bin/call_alert.pl?remote=sip:T-9175683609@192.168.10.11&active_user=108&action_id=1

IncomingCall=
http://gateasg.a1-content.ru/cgi-bin/call_alert.pl?remote=$remote&active_user=$active_user&action_id=1

������ �����=
http://gateasg.a1-content.ru/cgi-bin/call_alert.pl?remote=$remote&active_user=$active_user&action_id=2

������ ��������=
http://gateasg.a1-content.ru/cgi-bin/call_alert.pl?remote=$remote&active_user=$active_user&action_id=3

CREATE DEFINER = 'asg'@'%' TRIGGER `tr_call_alert_insert` AFTER INSERT ON `tr_call_alert`
FOR EACH ROW
BEGIN
	do asg_repl_log('tr_call_alert', 'REPLACE', concat('id=', new.id));
END;
CREATE DEFINER = 'asg'@'%' TRIGGER `tr_call_alert_update` AFTER UPDATE ON `tr_call_alert`
FOR EACH ROW
BEGIN
	do asg_repl_log('tr_call_alert', 'REPLACE', concat('id=', new.id));
END;

$mac --> current MAC address of the phone
$ip --> current IP address of the phone
$model --> the phone model
$firmware --> current firmware version of the phone
$active_url --> the SIP URI of the active outgoing identity
$active_user --> the user part of the SIP URI for the active outgoing identity
$active_host --> the host part of the SIP URI for the active outgoing identity
$local --> the SIP URI of the local identity (your phone's number)
$remote --> the SIP URI of the remote identity (the other party's phone number)
$display_local --> used to display your phone's display name
$display_remote --> used to display the other party's display name
$call_id --> the call-id of the active call

http://gateasg.a1-content.ru/cgi-bin/call_alert.pl?mac=$mac&ip=$ip&model=$model&firmware=$firmware&active_url=$active_url&active_user=$active_user&active_host=$active_host&local=$local&remote=$remote&display_local=$display_local&display_remote=$display_remote&call_id=$call_id&action_id=0

$VAR1 = {
		  'display_local' => '',
		  'active_url' => '',
		  'model' => 'SIP-T20P',
		  'action_id' => '3',
		  'display_remote' => '',
		  'firmware' => '9.73.0.40',
		  'remote' => '',
		  'mac' => '001565344ab4',
		  'call_id' => '',
		  'ip' => '192.168.10.181',
		  'local' => '',
		  'active_host' => '',
		  'active_user' => ''
		};
$VAR1 = {
		  'mac' => '001565344ab4',
		  'remote' => '',
		  'display_remote' => '',
		  'firmware' => '9.73.0.40',
		  'display_local' => '',
		  'active_url' => '',
		  'action_id' => '2',
		  'model' => 'SIP-T20P',
		  'local' => '',
		  'active_host' => '',
		  'active_user' => '',
		  'ip' => '192.168.10.181',
		  'call_id' => ''
		};
$VAR1 = {
		  'display_remote' => 'A1:',
		  'firmware' => '9.73.0.40',
		  'active_url' => 'sip:101@192.168.10.11',
		  'display_local' => '101',
		  'action_id' => '4',
		  'model' => 'SIP-T20P',
		  'mac' => '001565344ab4',
		  'remote' => 'sip:A-9051173006@192.168.10.11',
		  'call_id' => '260',
		  'ip' => '192.168.10.181',
		  'active_user' => '101',
		  'local' => 'sip:101@192.168.10.11',
		  'active_host' => '192.168.10.11'
		};
$VAR1 = {
		  'mac' => '001565344ab4',
		  'remote' => 'sip:A-9051173006@192.168.10.11',
		  'display_remote' => 'A1:',
		  'firmware' => '9.73.0.40',
		  'active_url' => 'sip:101@192.168.10.11',
		  'display_local' => '101',
		  'action_id' => '1',
		  'model' => 'SIP-T20P',
		  'active_user' => '101',
		  'active_host' => '192.168.10.11',
		  'local' => 'sip:101@192.168.10.11',
		  'ip' => '192.168.10.181',
		  'call_id' => '260'
		};

-----

$VAR1 = {
		  'action_id' => '1',
		  'remote' => 'sip:A-9673907792@192.168.10.11',
		  'mac' => '0015653f3912',
		  'model' => 'SIP-T20P',
		  'active_url' => 'sip:104@192.168.10.11',
		  'display_local' => '104',
		  'firmware' => '9.73.0.40',
		  'display_remote' => 'A1:',
		  'local' => 'sip:104@192.168.10.11',
		  'active_user' => '104',
		  'active_host' => '192.168.10.11',
		  'call_id' => '307',
		  'ip' => '192.168.10.119'
		};
$VAR1 = {
		  'action_id' => '2',
		  'active_host' => '',
		  'local' => '',
		  'active_user' => '',
		  'ip' => '192.168.10.119',
		  'call_id' => '',
		  'remote' => '',
		  'mac' => '0015653f3912',
		  'display_local' => '',
		  'active_url' => '',
		  'model' => 'SIP-T20P',
		  'display_remote' => '',
		  'firmware' => '9.73.0.40'
		};
$VAR1 = {
		  'action_id' => '4',
		  'display_local' => '104',
		  'active_url' => 'sip:104@192.168.10.11',
		  'model' => 'SIP-T20P',
		  'display_remote' => 'A1:',
		  'firmware' => '9.73.0.40',
		  'remote' => 'sip:A-9673907792@192.168.10.11',
		  'mac' => '0015653f3912',
		  'call_id' => '307',
		  'ip' => '192.168.10.119',
		  'active_host' => '192.168.10.11',
		  'local' => 'sip:104@192.168.10.11',
		  'active_user' => '104'
		};
$VAR1 = {
		  'action_id' => '3',
		  'local' => '',
		  'active_user' => '',
		  'active_host' => '',
		  'ip' => '192.168.10.181',
		  'call_id' => '',
		  'remote' => '',
		  'mac' => '001565344ab4',
		  'model' => 'SIP-T20P',
		  'active_url' => '',
		  'display_local' => '',
		  'firmware' => '9.73.0.40',
		  'display_remote' => ''
		};

$VAR1 = {
		  'mac' => '001565344ab4',
		  'remote' => 'sip:A-9051173006@192.168.10.11',
		  'display_remote' => 'A1:',
		  'firmware' => '9.73.0.40',
		  'active_url' => 'sip:101@192.168.10.11',
		  'display_local' => '101',
		  'action_id' => '1',
		  'model' => 'SIP-T20P',
		  'active_user' => '101',
		  'active_host' => '192.168.10.11',
		  'local' => 'sip:101@192.168.10.11',
		  'ip' => '192.168.10.181',
		  'call_id' => '260'
		};

ALTER TABLE `tr_call_alert` DROP INDEX `abonent`;
ALTER TABLE `tr_call_alert` ADD INDEX `abonent` (`abonent`, `cc_operator_id`);

$VAR1 = {
		  'call_id' => '',
		  'mac' => '0015653f3912',
		  'remote' => '',
		  'display_local' => '',
		  'model' => 'SIP-T20P',
		  'active_user' => '',
		  'firmware' => '9.73.0.40',
		  'local' => '',
		  'active_url' => '',
		  'display_remote' => '',
		  'ip' => '192.168.10.119',
		  'action_id' => '3',
		  'active_host' => ''
		};
$VAR1 = {
		  'model' => 'SIP-T20P',
		  'display_local' => '104',
		  'remote' => 'sip:A-79787885913@192.168.10.11',
		  'mac' => '0015653f3912',
		  'call_id' => '2840',
		  'action_id' => '4',
		  'ip' => '192.168.10.119',
		  'active_host' => '192.168.10.11',
		  'active_url' => 'sip:104@192.168.10.11',
		  'display_remote' => 'A1:',
		  'local' => 'sip:104@192.168.10.11',
		  'active_user' => '104',
		  'firmware' => '9.73.0.40'
		};
