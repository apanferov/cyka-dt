#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8889/;
use Disp::Config;
use Disp::MonitorCtl;
use Data::Dumper;

my %dispatch_table = ( get_log_tail => [ \&get_log_tail, "get_log_tail=<abonent_number>[&lines=<number_of_lines(default 100)>]" ],
					   monitor_list => [ \&monitor_list, "monitor_list=1" ],
					   monitor_kill => [ \&monitor_kill, "monitor_kill=<process_name>" ],
					 );

my $mc;

Disp::FastCgi::run(\&main,
				   sub {
					   Config::init(config_file => '../etc/asg.conf',
									skip_db_connect => 1 );
					   my $dir = Config->param('system_root')."/var/log/";
					   $mc = new_aggregated Disp::MonitorCtl
						 ( { pid => "$dir/monitor-transport.pid",
							 status => "$dir/monitor-transport.status" },
						   { pid => "$dir/monitor-billing.pid",
							 status => "$dir/monitor-billing.status" } ); });

sub get_log_tail {
	my $q = shift;
	my $name = $q->param('get_log_tail');
	my $lines = $q->param('lines');

	$lines = 100 unless ($lines||0) > 0;
	if ($name !~ /^[-_0-9a-z]+$/i) {
		print "Incorrect log name";
		return;
	}

	my $s;
	open TAIL, "-|", '/usr/bin/tail', '-n', $lines, Config->param('system_root')."/var/log/$name.log";
	while (sysread TAIL, $s, 4096) {
		print $s;
	}
	close TAIL;
	return;
}

sub monitor_list {
	my $q = shift;
	$mc->print_long_list(\*STDOUT);
}

sub monitor_kill {
	my $q = shift;
	my $name = $q->param('monitor_kill');
	if ($mc->kill_child($name, $q->param('sig') )) {
		print "Child $name successfully signalled";
	} else {
		print "Child $name signalling failed";
	}
}

sub main {
	my $q = shift;
	print $q->header(-type => 'text/plain', -charset => 'cp1251');
	foreach (keys %dispatch_table) {
		if ($q->param($_)) {
			$dispatch_table{$_}[0]->($q);
			return;
		}
	}
	print join "\n", map { $_->[1] } values %dispatch_table;
}
