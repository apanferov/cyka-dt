#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8881/;
use Disp::Config;
use Data::Dumper;
use POSIX qw/strftime/;
use Disp::AltDB;
use XML::Simple;

my @db=();
Disp::FastCgi::run(\&main,
				   sub {
					  Config::init(config_file => '../etc/asg.conf');
#					  Log::Log4perl::init('../etc/log.conf');
					});

sub main {
  my $q = shift;

  my $operation = $q->param('operation') || '';
  if ($operation =~ /^partner_services$/i) {
    my $list=Config->dbh->selectall_arrayref("select distinct p.service_id,s.name from set_plugin p left join set_service s on s.id=p.service_id where p.partner_id=? order by 2",undef,$q->param('partner_id'));
    print $q->header(-type => 'text/plain', -charset => 'cp1251');
    print "$_->[0] $_->[1]\n" foreach (@$list);
  } elsif ($operation =~ /^service_stat$/i) {
    my $list=Config->dbh->selectall_arrayref("select date,sum(count),sum(partner_income) from cyka_payment_furl where date between ? and ? and service_id=? and partner_id=? group by 1",undef,$q->param('start_date'),$q->param('end_date'),$q->param('service_id'),$q->param('partner_id'));
    print $q->header(-type => 'text/plain', -charset => 'cp1251');
    print "$_->[0] $_->[1] $_->[2]\n" foreach (@$list);
#  } elsif ($operation =~ /^partner_stat$/i) {
   } elsif (($operation =~ /^partner_stat$/i) and ($q->param('partner_id') =~ /^[\d\,]+$/)) {
      my $list=Config->dbh->selectall_arrayref("select date,sum(count),sum(partner_income),sum(price_rest) from cyka_payment_furl where date between ? and ? and partner_id in (".$q->param('partner_id').") group by 1",undef,$q->param('start_date'),$q->param('end_date'));
    print $q->header(-type => 'text/plain', -charset => 'cp1251');
    print "$_->[0] $_->[1] $_->[2] $_->[3]\n" foreach (@$list);
  } elsif ($operation =~ /^partner_statistics$/i) {
     my $list=Config->dbh->selectall_arrayref("select p.id,c.partner_currency,p.name,p.comment,round(sum(c.partner_income_ex),2) from cyka_payment_furl c left join set_cyka_partner p on c.partner_id = p.id where c.date between ? and ? and c.partner_id=? and c.test_flag=0 group by 1,2,3,4",undef,,$q->param('start_date'),$q->param('end_date'),$q->param('partner_id'));
     my $struct;
     foreach (@$list) {
	my ($id,$currency,$name,$comment,$amount) = @$_;
	$struct->{partner}{$id}{name} = $name;
	$struct->{partner}{$id}{comment} = $comment;
	$struct->{partner}{$id}{amount}{$currency}{content} = $amount;
	my ($wmr) = $comment =~ /(r\d+)\b/i;
	$struct->{partner}{$id}{wmr} = $wmr;
     }
     print $q->header(-type => 'application/xml', -charset => 'cp1251');
     print XMLout($struct,
	     RootName => 'stat',
	     AttrIndent => 1,
	     KeyAttr => {partner => 'id', amount => 'currency'},
	     XMLDecl => '<?xml version="1.0" encoding="windows-1251"?>'
    );
  } elsif (($operation =~ /^partner_connections$/i) and $q->param('login') and $q->param('password')) {
    my ( $partner_id ) = Config->dbh->selectrow_array("select id from set_cyka_partner where login=? and password=?",
							undef,$q->param('login'),$q->param('password'));
    if ( $partner_id ) {
	my $list=Config->dbh->selectall_hashref("select
pl.id,
c.id country_id,c.name as country_name,
o.id operator_id,o.name operator_name,c.mcc,o.mnc,
pl.num,if(pr.nds,round(pr.price_in/1.18,4),pr.price_in) price_in,
round(if(rnp.nds,rnp.estimated_income/1.18,rnp.estimated_income)*e_r.price,4) as estimated_income,rnp.currency,rnp.mt,
round(if(pr.nds,pr.price_in_a1/1.18,pr.price_in_a1)*e_r.price,4) as income_a1,pr.currency,
pl.service_id, pl.pattern,
round(cpl.b_price_fixed_p,4) as payout, cpl.currency as payout_currency
from set_plugin pl
left join set_cyka_plugin_t cpl on pl.service_id=cpl.service_id and pl.operator_id=cpl.operator_id and pl.num=cpl.num
inner join set_real_num_price rnp on pl.operator_id=rnp.operator_id and pl.num=rnp.num
inner join traffic_price pr on pl.operator_id=pr.operator_id and pl.num=pr.num and pr.date=curdate()
inner join set_operator o on pl.operator_id=o.id
left join set_country_t c on o.country_id=c.id
left join set_exchange_rate e_r on e_r.currency = pr.currency and e_r.date=curdate() 
where pl.partner_id=?",
	'id',undef,$partner_id);
	my $show_income = ($q->param('show_income') and ({3064 => 1}->{$partner_id}));
	my $struct;
	foreach (values(%$list)) {
		$struct->{country}{$_->{country_id}}{name}=$_->{country_name};
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{name}=$_->{operator_name};
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{mcc}=$_->{mcc};
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{mnc}=$_->{mnc};
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{price}=$_->{price_in}.uc($_->{currency});
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{estimated_income}=$_->{estimated_income}.'RUR' if ($show_income);
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{income}=$_->{income_a1}.'RUR' if ($show_income);
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{billing}=($_->{mt} ? 'mt' : 'mo');
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{service}{$_->{service_id}}{content}=$_->{pattern};
		$struct->{country}{$_->{country_id}}{operator}{$_->{operator_id}}{shortcode}{$_->{num}}{service}{$_->{service_id}}{payout}=$_->{payout}.uc($_->{payout_currency}) if ($_->{payout});
	}
	print $q->header(-type => 'application/xml', -charset => 'cp1251');
	print XMLout($struct,
		RootName => 'cyka',
		AttrIndent => 1,
		KeyAttr => {country => 'id', operator => 'id', shortcode => 'shortcode', service => 'id'},
		XMLDecl => '<?xml version="1.0" encoding="cp1251"?>');
    } else {
	 print $q->header(-type => 'text/plain', -charset => 'cp1251');
	print "Unknown partner";
    }
  }

}

