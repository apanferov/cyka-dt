#!/usr/bin/perl
#######################################################
#  
#  HTTP SMS Client v1
#  
#######################################################

use strict;
use warnings;
use lib '../../lib';
use Disp::HTTP_SMSClient;

my $query = new CGI;

Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
my	$client = new Disp::HTTP_SMSClient($query);
$client->run();



