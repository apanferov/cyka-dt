#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8919/;
use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;

use Data::Dumper;
use LWP::UserAgent;
use HTTP::Request::Common qw/POST/;
use Log::Log4perl qw/get_logger/;
use POSIX qw/strftime/;

my $log;
Disp::FastCgi::run(\&main,
                   sub {
                       Config::init(config_file => '../etc/asg.conf');

                       my %log_conf = (
                           "log4perl.rootLogger" => "DEBUG, Logfile",
                           "log4perl.appender.Logfile" => "Log::Log4perl::Appender::File",
                           "log4perl.appender.Logfile.recreate" => 1,
                           "log4perl.appender.Logfile.recreate_check_interval" => 300,
                           "log4perl.appender.Logfile.recreate_check_signal" => 'USR1',
                           "log4perl.appender.Logfile.umask" => "0000",
                           "log4perl.appender.Logfile.filename" => Config->param('system_root').'/var/log/dev_mts_proxy.log',
                           "log4perl.appender.Logfile.layout" => 'PatternLayout',
                           "log4perl.appender.Logfile.layout.ConversionPattern" => '[%d{DATE}] {%P} %p %m%n'
                       );

                       Log::Log4perl->init( \%log_conf);
                       $log = get_logger("");
                   });

sub main {
    my $q = shift;

    my $req = POST('https://p.a1pay.ru/mts_mc/',
                   Content_type => 'application/xml',
                   Content => $q->param('POSTDATA'));

    $log->debug("REQ: " . $req->as_string);

    my $ua = LWP::UserAgent->new;
    my $resp = $ua->request($req);

    $resp = $resp->as_string;
    $resp =~ s/^[^\r\n]+[\r\n]+//;

    $log->debug("RESP: " . $resp);
    print $resp;
}
