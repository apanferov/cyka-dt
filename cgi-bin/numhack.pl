#!/usr/bin/perl
use strict;
use warnings;
use CGI::Carp qw/fatalsToBrowser/;
use CGI;
use lib '../lib';
use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;
use Log::Log4perl;
use Data::Dumper;
use POSIX qw/strftime/;

Config::init(config_file => '../etc/asg.conf');
Log::Log4perl::init('../etc/log.conf');

main();
exit;

sub main {
	my $q = new CGI;
	print $q->header(-type => 'text/html', -charset => 'cp1251');

	my $abonent = $q->param('abonent') || "";
	my $smsc = $q->param('smsc') || "";

	if ($abonent and $smsc) {
#vlad		my ( $tmp ) = Config->dbh->selectrow_array("select smsc_id from tr_num_map where abonent = ?", undef, $abonent);
#vlad		if ($tmp) {
#vlad			print "<p>Already exists</p>";
#vlad		} else {
#vlad			Config->dbh->do("insert into tr_num_map values(?,'sms',?,determine_operator(?,?,null))", undef, $abonent, $smsc, $abonent, $smsc);
			Config->dbh->do("replace into tr_num_map values(?,'sms',?,determine_operator(?,?,null))", undef, $abonent, $smsc, $abonent, $smsc);
			print "<p>Updated</p>";
#vlad		}
	} else {
		print <<EOF;
<form method=get>
abonent: <input type=text name=abonent value="$abonent"><br>
smsc_id: <input type=text name=smsc value="$smsc"><br>
<input type=submit>
</form>
EOF
	}
}
