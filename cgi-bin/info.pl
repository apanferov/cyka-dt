#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8888/;
use Disp::Config;
use Disp::Operator;
use Disp::Utils;
use Log::Log4perl qw(get_logger);
use Data::Dumper;
use POSIX qw/strftime/;
use Disp::AltDB;
use Disp::IpCheck;

my $sh;
my $ip_cheker;
my @db=();
Disp::FastCgi::run(\&main,
				   sub {
             Config::init(config_file => '../etc/asg.conf');

             $ip_cheker = Disp::IpCheck->new();
				   },
		undef,
		{processes => 4});

sub main {
  my $q = shift;

	unless ($ip_cheker->check_all_ip($ENV{REMOTE_ADDR})) {
		http_response(403, "Forbidden");
		return;
	}

  my $header = $q->header(-type => 'text/plain', -charset => 'cp1251');

  if ($q->param('push_delivery_status') and my($push_id) = $q->param('push_delivery_status') =~ /^\s*(\d+)\s*$/) {
		my $status;
    my $sid;
		Disp::AltDB::dbh(undef, sub {
			($status, $sid) = $_[0]->{db}->selectrow_array("select delivery_status, service_id from tr_outboxa where push_id = ? and registered_delivery > 0",
				undef, $push_id);
			defined($status) and do { 1; }
			});
		unless (defined($sid) && $ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $sid)) {
			http_response(403, "Forbidden");
			return;
		}
    print $header;
		printf "PUSH$push_id:%s\n", status_name($status);
  } elsif ($q->param('outbox_delivery_status') and my($outbox_id) = $q->param('outbox_delivery_status') =~ /^\s*(\d+)\s*$/) {
	  my $status;
    my $sid;
	  Disp::AltDB::dbh(undef, sub {
			  ($status, $sid) = $_[0]->{db}->selectrow_array("select delivery_status, service_id from tr_outboxa where id=?",undef, $outbox_id);
			  defined($status) and do { 1; }
		  });
		unless (defined($sid) && $ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $sid)) {
			http_response(403, "Forbidden");
			return;
		}
    print $header;
	  printf "OUTBOX$outbox_id:%s\n", status_name($status);
  } elsif ($q->param('inbox_delivery_status') and my($inbox_id) = $q->param('inbox_delivery_status') =~ /^\s*(\d+)\s*$/) {
		my $status;
    my $sid;
		Disp::AltDB::dbh(undef, sub {
			($status, $sid) = $_[0]->{db}->selectrow_array("select delivery_status, service_id from tr_outboxa where inbox_id = ? and registered_delivery > 0",
				undef, $inbox_id);
			defined($status) and do { 1; }
			});
		unless (defined($sid) && $ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $sid)) {
			http_response(403, "Forbidden");
			return;
		}
    print $header;
	  print "INBOX$inbox_id:", status_name($status), "\n";
  } elsif ($q->param('operation') =~ /^new_session$/i
		   and (my $abonent) = $q->param('msisdn') =~ /^(\d+)$/
		   and my $num = $q->param('shortcode')
		   and (my $end_date) = $q->param('end_date') =~ /^([\d\s\-\:]+)$/
		   and (my $service_id) = $q->param('service_id') =~ /^(\d+)$/
		  ) {
				unless ($ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $service_id)) {
					http_response(403, "Forbidden");
					return;
				}

	  my $keyword = $q->param('keyword') || '';

    print $header;
	  my ( $id ) = Config->dbh->selectrow_array("select update_session(null,null,now(),?,?,?,?,?)",undef,
												$abonent,$num,$end_date,$service_id,$keyword);
	  print $id;
   } elsif ($q->param('operation') =~ /^payment$/i
	and (my $abonent1) = $q->param('msisdn') =~ /^(\d+)$/
	and my $num1 = $q->param('shortcode')
	and (my $date1) = $q->param('date') =~ /^([\d\s\-\:]+)$/
	and (my $service_id1) = $q->param('service_id') =~ /^(\d+)$/
	and (my $operator_id1) = $q->param('operator_id') =~ /^(\d+)$/
	and (my $partner_id1) = $q->param('partner_id') =~ /^(\d+)$/
	and (my $transport_type1) = $q->param('transport_type') =~ /^(\d+)$/
	) {
		unless ($ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $service_id1)) {
			http_response(403, "Forbidden");
			return;
		}

		my $comment = $q->param('comment');
		my ( $id ) = Config->dbh->selectrow_array('select id from cyka_payment_ext where date=? and service_id=? and num=? and abonent=?',undef,$date1,$service_id1,$num1,$abonent1);
		unless ($id) {
			Config->dbh->begin_work;
			eval {
				Config->dbh->do('insert into cyka_payment_ext set transport_type=?,date=?,service_id=?,abonent=?,operator_id=?,num=?,partner_id=?,cyka_processed=0,test_flag=0,comment=?,is_error=1',undef,$transport_type1,$date1,$service_id1,$abonent1,$operator_id1,$num1,$partner_id1,$comment);
				( $id ) = Config->dbh->selectrow_array('select last_insert_id()');
				Config->dbh->commit;
			};
			if ($@) {
				Config->dbh->rollback;
				http_response(500, "Internal server error");
				return;
			}
		}
		print $header;
		print $id;
  } else {
	    print $header;
	    print <<EOF;
push_delivery_status=<push_id>
inbox_delivery_status=<inbox_id>
outbox_delivery_status=<outbox_id>
EOF
	}
}


sub status_name {
	my ( $status ) = @_;
	return "UNKNOWN" unless (defined($status));
	my $status_text = {     0 => "PROCESS",
				2 => "ACCEPTED",
				10 => "ACKNOWLEDGED",
				11 => "ACCEPTED",
				12 => "DELIVERED",
				13 => "UNDELIVERED",
				14 => "UNDELIVERED",
				15 => "UNDELIVERED",
				127 => "REJECTED",
				255 => "REJECTED"
			}->{$status} || "UNKNOWN";
	return $status_text;
}

sub status_name2 {
  my ( $status ) = @_;

  my $status_text = "UNKNOWN";
  if (defined $status) {
    if (( $status == 2 ) or ( $status == 11 )) {
      $status_text = "ACCEPTED";
    } elsif ($status == 12) {
      $status_text = "DELIVERED";
    } elsif ($status == 0) {
      $status_text = "PROCESS";
    } else {
      $status_text = "REJECTED";
    }
  }
  return $status_text;
}

sub get_osmp_prv {
    my ( $abonent ) = @_;

    $abonent =~ s/^7//;
    my $data = Config->dbh->selectrow_hashref("select * from set_osmp_range where ? between range_from and range_to", undef, $abonent);

    if ($data) {
	print "STATUS:FOUND\n";
	print "PRV:$data->{prv}\n";
	print "REGION:$data->{region}\n" if $data->{region};
	print "PRIORITY:$data->{priority}\n" if $data->{priority};
    } else {
	print "STATUS:NOT_FOUND\n";
    }
}

sub get_mtt_operators {
	my $ops = Config->dbh->selectall_arrayref("select id, name from set_mtt1_operator", {Slice => {}});

	for (@$ops) {
		print "$_->{id}: $_->{name}\n";
	}
}

sub get_mtt_regions {
	my $regs = Config->dbh->selectall_arrayref("select id, name from set_mtt1_region", {Slice => {}});

	for (@$regs) {
		print "$_->{id}: $_->{name}\n";
	}
}

sub mtt_map_lookup {
	my ( $abonent, $map ) = @_;

	$abonent =~ s/^7//;

	my ( $operator_id, $region_id, $operator_name, $region_name ) =
	  Config->dbh->selectrow_array("select o.id as operator_id, r.id as region_id, o.name as operator, r.name as region from set_mtt1_base b join set_mtt1_operator o on b.operator_id = o.id join set_mtt1_region r on b.region_id = r.id where ? between num_start and num_end limit 1", undef,
								   $abonent);

	unless ($operator_id) {
		print "STATUS:NOT_FOUND\n";
		return;
	}

	my ( $map_value ) = Config->dbh->selectrow_array("select map_value from set_mtt1_mapping where operator_id = ? and region_id = ? and map_name = ?", undef,
													 $operator_id, $region_id, $map);

	if ($map_value) {
		print "STATUS:OK\n";
		print "MAPPING:$map_value\n";
	} else {
		print "STATUS:NO_MAPPING\n";
	}

	print "OPERATOR_NAME: $operator_name\n";
	print "REGION_NAME: $region_name\n";
	print "MTT_REGION_ID: $region_id\n";
	print "MTT_OPERATOR_ID: $operator_id\n";
}

sub mtt_lookup {
	my ( $abonent ) = @_;

	$abonent =~ s/^7//;

	if ( my ( $operator_id, $operator, $region ) =
		 Config->dbh->selectrow_array
		 ("select o.cuka_operator_id as operator_id, o.name as operator, r.name as region from set_mtt1_base b join set_mtt1_operator o on b.operator_id = o.id join set_mtt1_region r on b.region_id = r.id where ? between num_start and num_end limit 1", undef,
		  $abonent) ) {
		$operator_id ||= "NULL";
		print "OPERATOR_ID: $operator_id\n";
		print "OPERATOR_NAME: $operator\n";
		print "REGION_NAME: $region\n";
	} else {
		print "NOT FOUND\n";
	}
}

sub http_response {
        my($code, $body) = @_;
        print "Status: $code\r\nContent-type: text/html; charset=utf-8\r\n\r\n$body";
}
