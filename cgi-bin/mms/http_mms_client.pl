#!/usr/bin/perl
#######################################################
#  
#  MMS Client v1
#  
#######################################################

use strict;
use warnings;
use lib '../../lib';
use Disp::HTTP_MMSClient;
use Disp::Utils qw(yell);
use Data::Dumper;




our $proc_name = 'mrcv_all';
our $raw_input;
eval {
	# Loading configuration
	Config::init(config_file => "../../etc/asg.conf", skip_db_connect => 1);
#	Log::Log4perl::init('../../etc/log.conf');

	my	$client = new Disp::HTTP_MMSClient();
	$client->parse();
};

if ($@) {
	yell("$@\n$raw_input\n".Dumper(\%ENV),code => 'FATAL_ERROR',process => $proc_name,
		header => "$proc_name: $@");
}
