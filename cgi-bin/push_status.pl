#!/usr/bin/perl
use strict;
use warnings;
use CGI;
use lib '../lib';
use Disp::Config;
use Disp::AltDB;
use Log::Log4perl;
use Data::Dumper;

Config::init(config_file => '../etc/asg.conf', skip_db_connect => 1);

main();
exit;

sub main {
	my $db = new Disp::AltDB();
	my $q = new CGI;
	print $q->header(-type => 'text/html', -charset => 'cp1251');

	my $push_id = $q->param('push_id');
	unless ($push_id) {
		print 'UNKNOWN'; exit;
	}
		
	my ($outbox_id,$billing_count) = $db->{db}->selectrow_array('select outbox_id,outsms_count from tr_billing where push_id=? limit 1',undef,$push_id);

    unless ( $outbox_id ) { 
	  print "UNROUTABLE_IGNORED"; exit;
    }

#	if (!$billing_count) {
		if ($outbox_id) {
			my $sms_id = $db->get_value('select id from tr_outsms where outbox_id=? limit 1',undef,$outbox_id);
			if (!$sms_id) {
				my $sms_status = $db->get_value('select status from tr_outsmsa where outbox_id=? limit 1',undef,$outbox_id);		
				if ($sms_status==2)  {
					print 'ACCEPTED'; exit;
				}
				elsif ($sms_status!=12)  {
					print 'REJECTED'; exit;
				}
				print 'DELIVERED'; exit;
			}
			else {
				print 'PROCESS'; exit;
			}
		}
#	}
#	else {
#		print 'DELIVERED'; exit;
#	}
	print 'UNKNOWN';
}
