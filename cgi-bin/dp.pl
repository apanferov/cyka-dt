use lib ('../lib');
use Disp::Config;
use Disp::Operator;
use Disp::Utils;
use Log::Log4perl ('get_logger');
use Data::Dumper;
use POSIX ('strftime');
use Disp::AltDB;
use warnings;
use strict 'refs';
(my(@db) = ());
Disp::FastCgi::run((\&main), sub {
    Config::init('config_file', '../etc/asg.conf');
}
);
sub main {
    use warnings;
    use strict 'refs';
    (my $q = shift(@_));
    if (($q->param('distribution_report') and (my($dist_id) = ($q->param('distribution_report') =~ /^(\d+)$/)))) {
        print($q->header((-'type'), 'application/vnd.xls', (-'content_disposition'), "attachment; filename=distribution_$dist_id.xls"));
        print(`perl /gate/asg5/utils/distribution_report.pl $dist_id stdout`);
        return;
    }
    print($q->header((-'type'), 'text/plain', (-'charset'), 'cp1251'));
    if (($q->param('push_delivery_status') and (my($push_id) = ($q->param('push_delivery_status') =~ /^\s*(\d+)\s*$/)))) {
        my($status);
        &Disp::AltDB::dbh(undef, sub {
            (($status) = $_[0]{'db'}->selectrow_array('select delivery_status from tr_outboxa where push_id = ? and registered_delivery > 0', undef, $push_id));
            if (defined($status)) {
                do {
                    1;
                };
            }
        }
        );
        printf("PUSH${push_id}:%s\n", status_name($status));
    }
    elsif ((my($inbox_id) = ($q->param('inbox_delivery_status') =~ /^\s*(\d+)\s*$/))) {
        my($status);
        &Disp::AltDB::dbh(undef, sub {
            (($status) = $_[0]{'db'}->selectrow_array('select delivery_status from tr_outboxa where inbox_id = ? and registered_delivery > 0', undef, $inbox_id));
            if (defined($status)) {
                do {
                    1;
                };
            }
        }
        );
        print("INBOX${inbox_id}:", status_name($status), "\n");
    }
    elsif (((((($q->param('operation') =~ /^new_session$/i) and (my($abonent) = ($q->param('msisdn') =~ /^(\d+)$/))) and (my $num = $q->param('shortcode'))) and (my($end_date) = ($q->param('end_date') =~ /^([\d\s\-:]+)$/))) and (my($service_id) = ($q->param('service_id') =~ /^(\d+)$/)))) {
        (my $keyword = ($q->param('keyword') || ''));
        (my($id) = 'Config'->dbh->selectrow_array('select update_session(null,null,now(),?,?,?,?,?)', undef, $abonent, $num, $end_date, $service_id, $keyword));
        print($id);
    }
    elsif ($q->param('get_operator')) {
        (my $op = 'Disp::Operator'->new);
        (my($oper, $smsc) = $op->determine_operator($q->param('get_operator')));
        print($oper);
    }
    elsif ($q->param('get_operator_mtt')) {
        mtt_lookup($q->param('get_operator_mtt'));
    }
    elsif ($q->param('get_mtt_mapping')) {
        mtt_map_lookup($q->param('get_mtt_mapping'), $q->param('map'));
    }
    elsif ($q->param('get_mtt_operators')) {
        get_mtt_operators();
    }
    elsif ($q->param('get_mtt_regions')) {
        get_mtt_regions();
    }
    elsif ($q->param('get_osmp_prv')) {
        get_osmp_prv($q->param('get_osmp_prv'));
    }
    else {
        print("get_operator=<abonent_number>\nget_operator_mtt=<abonent_number>\nget_mtt_mapping=<abonent_number>&map=<mapping_name>\nget_mtt_operators=1\nget_mtt_regions=1\nget_osmp_prv=<abonent_number>\npush_delivery_status=<push_id>\ninbox_delivery_status=<inbox_id>\n");
    }
}
sub status_name {
    use warnings;
    use strict 'refs';
    (my($status) = @_);
    (my $status_text = 'UNKNOWN');
    if (defined($status)) {
        if ((($status == 2) or ($status == 11))) {
            ($status_text = 'ACCEPTED');
        }
        elsif (($status == 12)) {
            ($status_text = 'DELIVERED');
        }
        elsif (($status == 0)) {
            ($status_text = 'PROCESS');
        }
        else {
            ($status_text = 'REJECTED');
        }
    }
    return($status_text);
}
sub get_osmp_prv {
    use warnings;
    use strict 'refs';
    (my($abonent) = @_);
    ($abonent =~ s/^7//);
    (my $data = 'Config'->dbh->selectrow_hashref('select * from set_osmp_range where ? between range_from and range_to', undef, $abonent));
    if ($data) {
        print("STATUS:FOUND\n");
        print("PRV:$$data{'prv'}\n");
        ($$data{'region'} and print("REGION:$$data{'region'}\n"));
        ($$data{'priority'} and print("PRIORITY:$$data{'priority'}\n"));
    }
    else {
        print("STATUS:NOT_FOUND\n");
    }
}
sub get_mtt_operators {
    use warnings;
    use strict 'refs';
    (my $ops = 'Config'->dbh->selectall_arrayref('select id, name from set_mtt1_operator', {'Slice', {}}));
    foreach $_ (@$ops) {
        print("$$_{'id'}: $$_{'name'}\n");
    }
}
sub get_mtt_regions {
    use warnings;
    use strict 'refs';
    (my $regs = 'Config'->dbh->selectall_arrayref('select id, name from set_mtt1_region', {'Slice', {}}));
    foreach $_ (@$regs) {
        print("$$_{'id'}: $$_{'name'}\n");
    }
}
sub mtt_map_lookup {
    use warnings;
    use strict 'refs';
    (my($abonent, $map) = @_);
    ($abonent =~ s/^7//);
    (my($operator_id, $region_id, $operator_name, $region_name) = 'Config'->dbh->selectrow_array('select o.id as operator_id, r.id as region_id, o.name as operator, r.name as region from set_mtt1_base b join set_mtt1_operator o on b.operator_id = o.id join set_mtt1_region r on b.region_id = r.id where ? between num_start and num_end limit 1', undef, $abonent));
    unless ($operator_id) {
        print("STATUS:NOT_FOUND\n");
        return;
    }
    (my($map_value) = 'Config'->dbh->selectrow_array('select map_value from set_mtt1_mapping where operator_id = ? and region_id = ? and map_name = ?', undef, $operator_id, $region_id, $map));
    if ($map_value) {
        print("STATUS:OK\n");
        print("MAPPING:$map_value\n");
    }
    else {
        print("STATUS:NO_MAPPING\n");
    }
    print("OPERATOR_NAME: $operator_name\n");
    print("REGION_NAME: $region_name\n");
    print("MTT_REGION_ID: $region_id\n");
    print("MTT_OPERATOR_ID: $operator_id\n");
}
sub mtt_lookup {
    use warnings;
    use strict 'refs';
    (my($abonent) = @_);
    ($abonent =~ s/^7//);
    if ((my($operator_id, $operator, $region) = 'Config'->dbh->selectrow_array('select o.cuka_operator_id as operator_id, o.name as operator, r.name as region from set_mtt1_base b join set_mtt1_operator o on b.operator_id = o.id join set_mtt1_region r on b.region_id = r.id where ? between num_start and num_end limit 1', undef, $abonent))) {
        ($operator_id ||= 'NULL');
        print("OPERATOR_ID: $operator_id\n");
        print("OPERATOR_NAME: $operator\n");
        print("REGION_NAME: $region\n");
    }
    else {
        print("NOT FOUND\n");
    }
}
