#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8890/;
use Disp::Config;
use Disp::ServiceManager;
use Disp::Utils;
use Data::Dumper;
use Log::Log4perl qw(get_logger);
use Disp::AltDB;
use Time::HiRes qw(gettimeofday tv_interval);
use Disp::IpCheck;

my $sm;
my $sh;
my $request_count = 0;
my $html_form = <<EOF;
<form method=post>
<input type=text name=service_id>
<br>
<textarea name=msg rows=20 cols=60>status: notice
bill-incoming: zero
bill-outgoing: zero
destination:

</textarea>
<br>
<input type=submit>
</form>
EOF
my $ip_cheker;

Disp::FastCgi::run(\&main,
        sub {
                Config::init(config_file => '../etc/asg.conf');
                Log::Log4perl::init('../etc/log.conf');

                $sm = new Disp::ServiceManager('push' => 1);
                my %limits = (0 => Config->param('push_limit', 'push') || 100);
                while (my($id,$r) = each %{$sm->services})
                        { $limits{$id} = $r->{push_limit}; }
                $sh = new Shared(limits => \%limits);

                $ip_cheker = Disp::IpCheck->new();
        },
	undef,
	{processes => 4});

sub main {
        my $q = shift;
	my $t0 = [gettimeofday];
        # CGI.pm understands utf8, so param() can return back utf8 content.
        # we use param_fetch() to get raw content.
        my ($msg, $sid, $old_type) = ($q->param_fetch('msg')->[0], $q->param('service_id'), 1);
	($msg, $sid, $old_type) = ('a', $q->param('service-id'), 0) if ($q->param('service-id'));

        my $log = Log::Log4perl::get_logger('push');
	$log->debug("Request from $ENV{REMOTE_ADDR}"); 

	if ($ip_cheker->check_service_ip($ENV{REMOTE_ADDR}, $sid)) {
		$log->debug("Access granted");
  } else {
		$log->debug("Access denied");
		http_response(403, "Forbidden");
		return;
  }

        unless (defined($msg) and length($msg) and ($sid =~ /^\d+$/)) {
            $ENV{REMOTE_ADDR} eq '195.54.25.99' || http_response(200, $html_form);
            return;
        }

        unless ($sm->services()->{$sid}) {
            http_response(200, "<pre>Service$sid not found</pre>");
            return;
        }

        if ($sh->limitExceeded($sid)) {
	    my $interval = sprintf("% 4dms",int(1000*tv_interval($t0)));
	    $log->info("Blocked SVC$sid [$ENV{REMOTE_ADDR}] ($interval)");
            http_response(503, "<pre>Service push limit exceeded</pre>");
            return;
        }

#       if ($sh->limitExceeded) {
#	       http_response(503, "<pre>Push limit exceeded</pre>");
#	       return;
#       }

        my $error;
	my $push_id;

	if ($old_type) {
	        Config->dbh->do('insert into tr_pushboxa (service_id, ip_addr, date) values (?, inet_aton(?), now())',
                        undef, $sid, $ENV{REMOTE_ADDR});
        	$push_id = Config->dbh->selectrow_array("select last_insert_id()");
        	$sm->handle_service_response(
            		service   => { id => $sid },
            		orig      => { id => $push_id },
            		response  => $msg,
            		orig_type => 'push',
            		error_cb  => sub { $error .= "$_[0]\n" }
	        );
	} else {
		my %params = $q->Vars;
		$params{status} ||= 'notice';
		$push_id = $sm->handle_service_response_simple(
			service   => { id => $sid },
			orig      => { id => 0 },
			response  => $q->query_string(),
			section   => \%params,
			orig_type => 'push',
			error_cb  => sub { $error .= "$_[0]\n" }
		);
	}
	if ($request_count >= 100) {
		my($dbases) = Disp::AltDB::db_transports;
		$dbases->{$_}{db}->disconnect foreach (keys(%$dbases)); 
#		Config->dbh->disconnect;
		$request_count = 0;
	}
	$request_count++;
	my $interval = sprintf("% 4dms",int(1000*tv_interval($t0)));
	$log->info("Processed $push_id SVC$sid [$ENV{REMOTE_ADDR}] ($interval)");
        http_response(200, $error ? "<pre>$error</pre>" : "$push_id");
}

sub http_response {
        my($code, $body) = @_;
        print "Status: $code\r\nContent-type: text/html; charset=utf-8\r\n\r\n$body";
}

package Shared;
use strict;
use Redis;
use Time::HiRes qw(gettimeofday tv_interval);

sub new {
	my($class,%params) = (shift, @_);

	my $r;
	my $limits;
	my $log = Log::Log4perl::get_logger('push');

	eval {
		$limits = delete $params{limits};

		$r = Redis->new(encoding => undef);
		# init shared data
		$r->set('push_lock', 0);
		$log->info('LIMIT:INITIALIZED');
	}; #no limits, but push will be fine
	$log->error($@) if ($@);
	bless { redis => $r, limits => $limits, log => $log }, $class;
}

sub limitExceeded {
	my($self, $id) = @_;
	$id ||= 0;
	my $result=0;

	eval {
		my $r = $self->{redis};
		my $limit = $self->{limits}{$id} || $self->{limits}{0};

		$self->lock;
		my $count = $r->hincrby('push_counters', $id, 1);
		if ($limit and ($count > $limit)) {
#			$self->lock;
			my $interval = tv_interval([unpack('LL', pack('H*',$r->hget('push_timers', $id)))]);
			$self->{log}->debug("interval=$interval");
			if ($interval < 1) {
				$self->{log}->warn("LIMIT:FAIL service=$id request=$count/$limit interval=$interval");
				$result = 1;
			} else {
				$count = 1;
				$r->hset('push_timers', $id, unpack('H*',pack('LL', gettimeofday)));
				$r->hset('push_counters', $id, $count);
			}
#			$self->unlock;
		}
		$self->unlock;
		$self->{log}->debug("LIMIT:OK service=$id request=$count/$limit") unless ($result);

	};
	$self->{log}->error($@) if ($@);
	return $result;
}

sub lock {
	my $self = shift;
	while ($self->{redis}->incr('push_lock') > 1) {}
}

sub unlock {
	my $self = shift;
	$self->{redis}->set('push_lock', 0);
}


1;
