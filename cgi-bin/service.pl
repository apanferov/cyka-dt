#!/usr/bin/perl
use strict;
use warnings;
use lib '../lib';
use Disp::FastCgi qw/127.0.0.1:8787/;
##use Disp::Config;
##use Disp::Utils;
use Data::Dumper;
use Log::Log4perl;

# Disp::FastCgi::run(\&main, sub {
# 	##Config::init(config_file => '../etc/asg.conf');
# 	##Log::Log4perl::init('../etc/log.conf');
# 	},
# 	undef,
# 	{processes => 10},
# );
Disp::FastCgi::run(\&main, sub {
	##Config::init(config_file => '../etc/asg.conf');
	##Log::Log4perl::init('../etc/log.conf');
	},
);

sub main {
	my $q = shift;
	my $pkg = "Service::" . $q->param('service_id');
	eval "require $pkg; print ${pkg}::handle(\$q)";
	if ($@) {
		require Service::not_found;
		print Service::not_found::handle($q);
	}
}
